<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Maps extends CI_Controller

{
  function __construct()
  {
    $this->data['pagetitle'] = 'Map';
    $this->data['smalltitle'] = 'Map';
    $this->data['breadcrumb'] = 'Map';
    parent::__construct($this->data);
    $this->load->library('formloader');
    $this->load->library('formloader1');
    $this->load->model('Agenda_model');
    $this->template->set_template('front_template');
    $this->load->model('Event_template_model');
    $this->load->model('Cms_model');
    $this->load->model('Event_model');
    $this->load->model('Setting_model');
    $this->load->model('Speaker_model');
    $this->load->model('Profile_model');
    $this->load->model('Message_model');
    $this->load->model('Map_model');
    $user = $this->session->userdata('current_user');
    $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
    $eventid = $event_val[0]['Id'];
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);
    if (!in_array('10', $menu_list))
    {
      echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
    }
    if ($this->data['pagetitle'] == "Map")
    {
      $title = "Maps";
    }
    $eventname = $this->Event_model->get_all_event_name();
    if (in_array($this->uri->segment(3) , $eventname))
    {
      if ($user != '')
      {
        $logged_in_user_id = $user[0]->User_id;
        $parameters = $this->uri->uri_to_assoc(1);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
        $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
        if ($cnt == 1)
        {
          $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
          $this->data['notes_list'] = $notes_list;
        }
        else
        {
          $event_type = $event_templates[0]['Event_type'];
          if ($event_type == 3)
          {
            $this->session->unset_userdata('current_user');
            $this->session->unset_userdata('invalid_cred');
            $this->session->sess_destroy();
          }
          else
          {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain = $this->uri->segment(3);
            $acc_name = $this->uri->segment(2);
            echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
          }
        }
      }
    }
    else
    {
      $Subdomain = $this->uri->segment(3);
      $flag = 1;
      echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
    }
  }
  public function index($acc_name= NULL, $Subdomain = NULL, $intFormId = NULL)

  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $user = $this->session->userdata('current_user');
    if (!empty($user[0]->Id))
    {
      $req_mod = $this->router->fetch_class();
      if ($req_mod == "Map")
      {
        $req_mod = "Maps";
      }
      $menu_id = $this->Event_model->get_menu_id($req_mod);
      $current_date = date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
    }
    $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting'] = $notificationsetting;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $this->data['fb_login_data'] = $fb_login_data;
    $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
    $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
    $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
    $this->data['sign_form_data'] = $res1;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    for ($i = 0; $i < count($menu_list); $i++)
    {
      if ('Maps' == $menu_list[$i]['pagetitle'])
      {
        $mid = $menu_list[$i]['id'];
      }
    }
    $this->data['menu_id'] = $mid;
    $this->data['menu_list'] = $menu_list;
    $eid = $event_templates[0]['Id'];
    $res = $this->Agenda_model->getforms($eid, $mid);
    $this->data['form_data'] = $res;
    $array_temp_past = $this->input->post();
    if (!empty($array_temp_past))
    {
      $aj = json_encode($this->input->post());
      $formdata = array(
        'f_id' => $intFormId,
        'm_id' => $mid,
        'user_id' => $user[0]->Id,
        'event_id' => $eid,
        'json_submit_data' => $aj
      );
      $this->Agenda_model->formsinsert($formdata);
      echo '<script>window.location.href="' . base_url() . 'Maps/' . $acc_name . '/' . $Subdomain . '"</script>';
      exit;
    }
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $map = $this->Event_template_model->get_map_list($Subdomain);
    $this->data['map'] = $map;
    $this->data['Subdomain'] = $Subdomain;
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/index', $this->data, true);
        $this->template->write_view('footer', 'Maps/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/index', $this->data, true);
        $this->template->write_view('footer', 'Maps/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3')
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] = $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('10', $event_templates[0]['Id']);
      if ($isforcelogin['is_force_login'] == '1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/index', $this->data, true);
        $this->template->write_view('footer', 'Maps/footer', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/index', $this->data, true);
        $this->template->write_view('footer', 'Maps/footer', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }
  public function View($acc_name= NULL, $Subdomain = NULL, $id = null)

  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $this->data['fb_login_data'] = $fb_login_data;
    $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
    $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
    $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $this->data['Subdomain'] = $Subdomain;
    $map = $this->Event_template_model->get_map($Subdomain, $id);
    if (!empty($map))
    {
      $view_map = $this->Event_template_model->get_map($Subdomain, $id);
      $this->data['view_map'] = $view_map;
      $this->data['image_mapping'] = $this->Map_model->get_map_image_data_list($id);
    }
    $user = $this->session->userdata('current_user');
    $category_list = $this->Agenda_model->get_all_agenda_category_list($event_templates[0]['Id']);
    $agenda_category = $this->Event_template_model->get_agenda_category_id_by_user($event_templates[0]['Id']);
    $primary_agenda = $this->Event_template_model->get_primary_category_id($event_templates[0]['Id']);
    if (count($category_list) == 1)
    {
      $cid = $category_list[0]['cid'];
    }
    else
    {
      if (!empty($agenda_category[0]['agenda_category_id']))
      {
        $cid = $agenda_category[0]['agenda_category_id'];
      }
      else
      {
        $cid = $primary_agenda[0]['Id'];
      }
    }
    $this->data['user_agenda_ids'] = $this->Event_model->get_user_assign_agenda_id($cid);
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/view', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/view', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3')
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] = $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('10', $event_templates[0]['Id']);
      if ($isforcelogin['is_force_login'] == '1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/view', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'Maps/view', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }
  public function Map_list($Subdomain = NULL, $id = null)

  {
    $Subdomain = $this->uri->segment(2);
    $id = $this->uri->segment(4);
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $this->data['Subdomain'] = $Subdomain;
    $map = $this->Event_template_model->get_map($Subdomain, $id);
    if (!empty($map))
    {
      $view_map = $this->Event_template_model->get_map($Subdomain, $id);
      $this->data['view_map'] = $view_map;
    }
    $user = $this->session->userdata('current_user');
    if (!empty($user))
    {
      $this->template->write_view('content', 'Maps/map_list', $this->data, true);
      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
      $this->template->write_view('header', 'frontend_files/header', $this->data, true);
      $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
      $this->template->write_view('js', 'frontend_files/js', $this->data, true);
      $this->template->render();
    }
    else
    {
      $this->template->write_view('css', 'frontend_files/css', $this->data, true);
      $this->template->write_view('header', 'frontend_files/header', $this->data, true);
      $this->template->write_view('content', 'registration/index', $this->data, true);
      $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
      $this->template->write_view('js', 'frontend_files/js', $this->data, true);
      $this->template->render();
    }
  }
}