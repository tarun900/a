<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Doccategory extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Doc';
        $this->data['smalltitle'] = 'Doc Details';
        $this->data['page_edit_title'] = 'edit';
        $this->data['breadcrumb'] = 'Doc';
        parent::__construct($this->data);
        $this->load->model('Doccategory_model');
        $this->load->model('Setting_model');
        $this->load->model('Event_model');
        $this->load->model('Profile_model');
        $this->load->library('session');
        $this->load->library('upload');
        $user = $this->session->userdata('current_user');
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
    }
    public function index($id=NULL)

    {
        $docscategory = $this->Doccategory_model->get_doccategory_list(null, $id);
        $this->data['docscategory'] = $docscategory;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['event_id'] = $id;
        // print_r($users); exit();
        $this->template->write_view('css', 'doccategory/css', $this->data, true);
        $this->template->write_view('content', 'doccategory/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'doccategory/js', $this->data, true);
        $this->template->render();
    }
    public function edit($id = '0', $eventid=NULL)

    {
        $this->data['docscategory'] = $this->Doccategory_model->get_doccategory_list($id);
        $event = $this->Event_model->get_admin_event($eventid);
        $this->data['event'] = $event[0];
        $this->data['event_id'] = $eventid;
        //          echo "<pre>";
        //          print_r($this->data['docscategory'] );
        //          exit;
        if ($this->input->post())
        {
            if (!empty($_FILES['images']['name']))
            {
                $v = str_replace(' ', '', $_FILES['images']['name']);
                $tempname = explode('.', $v);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $_POST['image'] = $images_file;
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png',
                    "max_size" => '2048',
                    "max_width" => '2048',
                    "max_height" => '2048'
                ));
                if (!$this->upload->do_multi_upload("images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', $error['error']);
                }
            }
            if (!empty($_FILES['coverimages']['name']))
            {
                $v = str_replace(' ', '', $_FILES['coverimages']['name']);
                $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                $tempname = explode('.', $v);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $_POST['coverimages'] = $images_file;
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '100000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("coverimages"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', "Document Cover images added wrong " . $error['error']);
                    redirect("Document/index/" . $eventid);
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "idval")
                {
                    $k = strtolower($k);
                    $array_add[$k] = $v;
                }
                else
                {
                    $idval = $v;
                }
            }
            $array_add['Event_id'] = $eventid;
            $user = $this->Doccategory_model->edit_doccategory($idval, $array_add);
            $this->session->set_flashdata('doccategoy_data', 'Added');
            redirect("Document/index/" . $eventid);
        }
        $this->template->write_view('css', 'doccategory/add_css', $this->data, true);
        $this->template->write_view('content', 'doccategory/edit', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'doccategory/add_js', $this->data, true);
        $this->template->render();
    }
    public function add($eventid=NULL)

    {
        $event = $this->Event_model->get_admin_event($eventid);
        $this->data['event'] = $event[0];
        $this->data['event_id'] = $eventid;
        if ($this->input->post())
        {
            if (!empty($_FILES['images']['name']))
            {
                $v = str_replace(' ', '', $_FILES['images']['name']);
                $tempname = explode('.', $v);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $_POST['image'] = $images_file;
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png',
                    "max_size" => '2048',
                    "max_width" => '2048',
                    "max_height" => '2048'
                ));
                if (!$this->upload->do_multi_upload("images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', $error['error']);
                }
            }
            if (!empty($_FILES['coverimages']['name']))
            {
                $v = str_replace(' ', '', $_FILES['coverimages']['name']);
                $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                $tempname = explode('.', $v);
                $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $images_file = $tempname_imagename . "." . $tempname[1];
                $_POST['coverimages'] = $images_file;
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => 'gif|jpg|png|jpeg',
                    "max_size" => '100000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("coverimages"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', "Document Cover images added wrong " . $error['error']);
                    redirect("Document/index/" . $eventid);
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval")
                {
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            $array_add['Event_id'] = $eventid;
            $user = $this->Doccategory_model->add_doccategory($array_add);
            $this->session->set_flashdata('doccategoy_data', 'Added');
            redirect("Document/index/" . $eventid);
        }
        $this->template->write_view('css', 'doccategory/add_css', $this->data, true);
        $this->template->write_view('content', 'doccategory/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'doccategory/add_js', $this->data, true);
        $this->template->render();
    }
    public function delete($id=NULL, $eventid=NULL)

    {
        $user = $this->Doccategory_model->delete_doccategory($id);
        $this->session->set_flashdata('doccategoy_data', 'Deleted');
        redirect("Document/index/" . $eventid);
    }
}