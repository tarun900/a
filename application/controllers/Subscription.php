<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscription extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'Subscription';
		$this->data['smalltitle'] = 'Event Subscription';
		$this->data['breadcrumb'] = 'Subscription';
        $this->data['page_edit_title'] = 'edit';    
		parent::__construct($this->data);
		
		$this->load->model('Subscription_model');
        $this->load->model('event_model');
	}

	public function index()                 
	{                       		         		
		$subscription = $this->Subscription_model->get_subscription_list();

		$this->data['Subscription'] = $subscription;
			
		$this->template->write_view('css', 'subscription/css', $this->data , true);
		$this->template->write_view('content', 'subscription/index', $this->data , true);
		$this->template->write_view('js', 'subscription/js', $this->data , true);
		$this->template->render();
	}

	public function add()
	{
            if($this->input->post())
            {
                $subscription = $this->Subscription_model->add_subscription($this->input->post());
                $this->session->set_flashdata('subscription_data', 'Added');
                redirect('Subscription');
            }
            
            $event = $this->event_model->get_event_list();
            $this->data['Event'] = $event;
	
            $this->template->write_view('css', 'subscription/add_css', $this->data , true);
            $this->template->write_view('content', 'subscription/add', $this->data , true);
            $this->template->write_view('js', 'subscription/add_js', $this->data , true);
            $this->template->render();
	}
        
        public function edit($id = NULL)
        {
            if($this->input->post())
            {
                $subscription = $this->Subscription_model->update_subscription($this->input->post(),$id);
                $this->session->set_flashdata('subscription_data', 'Updated');
                redirect('Subscription');
            }
            
            $subscription = $this->Subscription_model->get_subscription_list($id);
            $this->data['Subscription'] = $subscription;
            
            $event = $this->event_model->get_event_list();
            $this->data['Event'] = $event;
	
            $this->template->write_view('css', 'subscription/add_css', $this->data , true);
            $this->template->write_view('content', 'subscription/edit', $this->data , true);
            $this->template->write_view('js', 'subscription/add_js', $this->data , true);
            $this->template->render();
        }
        
        public function delete($id = NULL)
        {
            
            $subscription = $this->Subscription_model->delete_subscription($id);
            $this->session->set_flashdata('subscription_data', 'Deleted');
            redirect('Subscription');
        }
        
        public function checksubscription()
        {            
            if($this->input->post())
            {
                $subscription = $this->Subscription_model->checksubscription($this->input->post('sname'),$this->input->post('idval'));
               
                if($subscription)
                {
                    echo "error###subscription alerady exist. Please chose another subscription.";
                }
                else
                {
                    echo "success###";
                }
            }
            exit;
        }
}
