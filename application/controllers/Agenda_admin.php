<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Agenda_admin extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Agenda';
        $this->data['smalltitle'] = 'Add multiple Agendas and Sessions into your App.';
        $this->data['breadcrumb'] = 'Agenda';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        ini_set('auto_detect_line_endings', true);
        $this->load->model('Event_model');
        $this->load->model('Agenda_model');
        $this->load->model('Menu_model');
        $eventid = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column_1($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        // echo $module;die;
        if (!in_array('1', $module))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            $cnt = $this->Agenda_model->check_auth("Agenda", $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('1', $menu_list))
        {
            $this->load->model('Agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Map_model');
            $this->load->model('Speaker_model');
            $this->load->model('Event_template_model');
            $this->load->model('Profile_model');
            $this->load->library('upload');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="' . base_url() . 'forbidden/' . '"</script>';
        }
    }
    public function index($id = NULL)

    {
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
            $this->data['category_list'] = $category_list;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['lock_agenda'] = $this->Event_model->get_lock_agenda_status($event[0]['Organisor_id']);
        $time_format = $this->Event_model->getTimeFormat($id);
        $this->data['time_format'] = $time_format;
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 1);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 1);
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 1);
        $this->data['module_group_list'] = $module_group_list;
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->template->write_view('css', 'agenda_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/agenda_category_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/js', true);
        $this->template->render();
    }
    public function agenda_index($id = NULL, $cid = NULL)

    {
        if ($this->input->post())
        {
            if (!empty($_FILES['Images']['name'][0]))
            {
                $v = str_replace(' ', '', $_FILES['Images']['name']);
                if (file_exists("./assets/user_files/" . $v)) $Images = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                else $Images = $v;
                $this->upload->initialize(array(
                    "file_name" => $Images,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => '*',
                    "max_size" => '10000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("Images"))
                {
                    $error = array(
                        'session_type_error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', "For Menu Images, " . $error['error']);
                    redirect(base_url() . 'Agenda_admin/agenda_index/' . $id . '/' . $cid);
                }
            }
            if (!empty($Images))
            {
                $menu_data['agenda_array']['img'] = $Images;
            }
            if ($this->input->post('is_feture_product') != '')
            {
                $menu_data['agenda_array']['is_feture_product'] = '1';
            }
            else
            {
                $menu_data['agenda_array']['is_feture_product'] = '0';
            }
            $menu_data['agenda_array']['agenda_id'] = $this->input->post('agenda_id');
            $menu_data['agenda_array']['event_id'] = $this->input->post('event_id');
            $menu_data['agenda_array']['title'] = $this->input->post('title');
            $menu_data['agenda_array']['img_view'] = $this->input->post('img_view');
            $agenda_id = $this->Menu_model->add_menu($menu_data);
            redirect(base_url() . 'Agenda_admin/agenda_index/' . $id . '/' . $cid);
        }
        $category_menu_data = $this->Event_model->get_agenda_category_menu_data($id, $cid);
        $this->data['category_menu_data'] = $category_menu_data;
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
            $this->data['agenda_list'] = $agenda_list;
        }
        $this->data['category_data'] = $this->Agenda_model->get_agenda_category_by_id($cid);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['acid'] = $cid;
        $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
        $this->data['agenda_list'] = $agenda_list;
        /*echo '<pre>';
        print_r($agenda_list);
        exit;*/
        $this->load->model('Attendee_model');
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $this->data['reg_tickets'] = $this->Agenda_model->get_reg_tickets($id);
        $session_types = $this->Agenda_model->get_session_type($id);
        $this->data['session_types'] = $session_types;
        $time_format = $this->Event_model->getTimeFormat($id);
        $this->data['time_format'] = $time_format;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 1);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['event_id'] = $id;
        $this->data['acid'] = $cid;
        $this->template->write_view('css', 'agenda_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/js', true);
        $this->template->render();
    }
    public function getmaparea($id = NULL)

    {
        $mid = $this->input->post('mid');
        $maparea = $this->Map_model->getarea($id, $mid);
        echo $maparea[0]['area'];
    }
    public function add_existing_agenda($eid = NULL, $cid = NULL)

    {
        $addagenda['category_id'] = $cid;
        $addagenda['agenda_id'] = $this->input->post('agenda_id');
        $this->Agenda_model->add_existing_agenda($addagenda);
        $this->session->set_flashdata('category_success', 'Session Add Successfully');
        redirect(base_url() . 'Agenda_admin/agenda_index/' . $eid . '/' . $cid);
    }
    public function add_caterogy($eid = NULL)

    {
        $Event_id = $eid;
        if ($this->input->post())
        {
            $acid = $this->input->post('category_id');
            $data['category_name'] = $this->input->post('agenda_category_name');
            $welcome_screen = $this->input->post('welcome_screen');
            if(empty(strip_tags($welcome_screen)))
            {
            	$welcome_screen = '';
            }
            $data['welcome_screen'] = $welcome_screen;
            $getcate = $this->Agenda_model->get_agenda_category_by_name($data['category_name'], $Event_id, $acid);
            if (count($getcate) > 0)
            {
                $this->session->set_flashdata('category_error', 'Agenda Name already exist. Please choose another Category Name.');
                redirect("Agenda_admin/agenda_index/" . $Event_id);
            }
            else
            {
                if (empty($acid))
                {
                    $data['created_date'] = date("Y-m-d H:i:s");
                    $category_id = $this->Agenda_model->add_agenda_category($data, $Event_id);
                }
                else
                {
                    $category_id = $this->Agenda_model->update_agenda_category($data, $acid);
                }
            }
            $this->session->set_flashdata('category_success', 'Agenda Save Successfully');
            redirect("Agenda_admin/agenda_index/" . $Event_id . '/' . $category_id);
        }
    }
    public function check_agenda_cateory($eventid = NULL)

    {
        $cname = $this->input->post('Category_name');
        $cid = $this->input->post('cid');
        $getcate = $this->Agenda_model->get_agenda_category_by_name($cname, $eventid, $cid);
        if (count($getcate) > 0)
        {
            echo "error###Category Name already exist. Please choose another Category Name.";
            die;
        }
        else
        {
            echo "success###";
            die;
        }
    }
    public function delete_agenda_category($eventid = NULL, $cid = NULL)

    {
        $this->Agenda_model->delete_agenda_category_by_id($cid, $eventid);
        $this->session->set_flashdata('category_data', 'Deleted');
        redirect("Agenda_admin/index/" . $eventid);
    }
    public function add($id = NULL, $cid = NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $types = $this->Agenda_model->get_session_type($id);
        $this->data['type_data'] = $types;
        $map_list = $this->Map_model->get_maps($id);
        $this->data['map_list'] = $map_list;
        $presentation_list = $this->Agenda_model->getAllpresentationByEventId($id);
        $this->data['presentation_list'] = $presentation_list;
        $surveys_list = $this->Agenda_model->get_all_surveys_category_by_event($id);
        $this->data['surveys_list'] = $surveys_list;
        $doc_list = $this->Agenda_model->getAllDocumentonByEventId($id);
        $this->data['doc_list'] = $doc_list;
        $types = $this->Agenda_model->get_session_type($id);
        $this->data['type_data'] = $types;
        $speakers = $this->Speaker_model->get_speaker_list($id);
        $this->data['speakers'] = $speakers;
        $qasession_list = $this->Agenda_model->get_all_qasession_by_event($id);
        $this->data['qasession_list'] = $qasession_list;
        if ($this->input->post())
        {
            $event = $this->Event_model->get_admin_event($id);
            if (!empty($_FILES['session_images']))
            {
                $tempname = explode('.', $_FILES['session_images']['name']);
                $tempname_imagename = str_replace(' ', '_', $tempname[0]) . uniqid();
                $images_file = $tempname_imagename . "." . $tempname[1];
                $this->upload->initialize(array(
                    "file_name" => $images_file,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => '*',
                    "max_size" => '0',
                    "max_width" => '0',
                    "max_height" => '0'
                ));
                if ($this->upload->do_upload("session_images"))
                {
                    $data['agenda_array']['session_image'] = $images_file;
                }
            }
            $data['agenda_array']['Organisor_id'] = $logged_in_user_id;
            $data['agenda_array']['Event_id'] = $Event_id;
            $data['agenda_array']['Start_date'] = date('Y-m-d', strtotime($this->input->post('Start_date')));
            $Start_time = $this->input->post('Start_time');
            $Start_time = date('H:i:s', strtotime($Start_time));
            $data['agenda_array']['Start_time'] = $Start_time;
            $data['agenda_array']['End_date'] = date('Y-m-d', strtotime($this->input->post('End_date')));
            $data['agenda_array']['checking_datetime'] = $this->input->post('checking_datetime');
            $End_time = $this->input->post('End_time');
            $End_time = date('H:i:s', strtotime($End_time));
            $data['agenda_array']['End_time'] = $End_time;
            /* if(!empty($event[0]['Event_time_zone'])){
            if(strpos($event[0]['Event_time_zone'],"-")==true)
            {
            $arr=explode("-", $event[0]['Event_time_zone']);
            $offset=explode(":",$arr[1]);
            if(!in_array("30",$offset))
            {
            $intoffset=$offset[0]*3600;
            }
            else
            {
            $intoffset=$offset[0].'.5';
            $intoffset=$intoffset*3600;
            }
            $intNew = abs($intoffset);
            $StartDateTime = date('Y-m-d H:i:s',strtotime($data['agenda_array']['Start_date']." ".$data['agenda_array']['Start_time']) - $intNew);
            $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['agenda_array']['End_date']." ".$data['agenda_array']['End_time']) - $intNew);
            }
            else
            {
            $arr=explode("+", $event[0]['Event_time_zone']);
            $offset=explode(":",$arr[1]);
            if(!in_array("30",$offset))
            {
            $intoffset=$offset[0]*3600;
            }
            else
            {
            $intoffset=$offset[0].'.5';
            $intoffset=$intoffset*3600;
            }
            $intNew = abs($intoffset);
            $StartDateTime = date('Y-m-d H:i:s',strtotime($data['agenda_array']['Start_date']." ".$data['agenda_array']['Start_time']) + $intNew);
            $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['agenda_array']['End_date']." ".$data['agenda_array']['End_time']) + $intNew);
            }
            $data['agenda_array']['Start_date']=date('Y-m-d', strtotime($StartDateTime));
            $data['agenda_array']['Start_time'] = date('H:i:s', strtotime($StartDateTime));
            $data['agenda_array']['End_time']= date('H:i:s', strtotime($EndDateTime));
            $data['agenda_array']['End_date']=date('Y-m-d', strtotime($EndDateTime));
            }*/
            $data['agenda_array']['Heading'] = $this->input->post('Heading');
            $data['agenda_array']['Types'] = $this->input->post('Types');
            if ($this->input->post('Types') == 'Other')
            {
                $data['agenda_array']['other_types'] = $this->input->post('other_types');
            }
            $data['agenda_array']['short_desc'] = $this->input->post('short_desc');
            $data['agenda_array']['description'] = $this->input->post('description');
            $data['agenda_array']['Address_map'] = $this->input->post('Address_map');
            $data['agenda_array']['Agenda_status'] = $this->input->post('Agenda_status');
            $data['agenda_array']['Maximum_People'] = $this->input->post('Maximum_People');
            $data['agenda_array']['custom_speaker_name'] = $this->input->post('custom_speaker_name');
            $data['agenda_array']['custom_location'] = $this->input->post('custom_location');
            if ($this->input->post('show_places_remaining') == '1')
            {
                $data['agenda_array']['show_places_remaining'] = $this->input->post('show_places_remaining');
            }
            else
            {
                $data['agenda_array']['show_places_remaining'] = '0';
            }
            if ($this->input->post('show_checking_in') == '1')
            {
                $data['agenda_array']['show_checking_in'] = $this->input->post('show_checking_in');
            }
            else
            {
                $data['agenda_array']['show_checking_in'] = '0';
            }
            if ($this->input->post('show_rating') == '1')
            {
                $data['agenda_array']['show_rating'] = $this->input->post('show_rating');
            }
            else
            {
                $data['agenda_array']['show_rating'] = '0';
            }
            if ($this->input->post('allow_clashing') == '1')
            {
                $data['agenda_array']['allow_clashing'] = $this->input->post('allow_clashing');
            }
            else
            {
                $data['agenda_array']['allow_clashing'] = '0';
            }
            if ($this->input->post('Speaker_id') != '' && $this->input->post('Speaker_id') != 'Select Speakers')
            {
                $data['agenda_array']['Speaker_id'] = implode(',', $this->input->post('Speaker_id'));
            }
            else
            {
                $data['agenda_array']['Speaker_id'] = NULL;
            }
            if ($this->input->post('doc_id') == "New")
            {
                $data['agenda_array']['document_id'] = "";
            }
            else
            {
                $data['agenda_array']['document_id'] = $this->input->post('doc_id');
            }
            $data['agenda_array']['presentation_id'] = $this->input->post('presentation_id');
            $data['agenda_array']['survey_id'] = $this->input->post('survey_id');
            if (empty($this->input->post('qasession_id')))
            {
                $data['agenda_array']['qasession_id'] = NULL;
            }
            else
            {
                $data['agenda_array']['qasession_id'] = $this->input->post('qasession_id');
            }
            if ($this->input->post('allow_comments') == '1')
            {
                $data['agenda_array']['allow_comments'] = $this->input->post('allow_comments');
            }
            else
            {
                $data['agenda_array']['allow_comments'] = '0';
            }
            $data['agenda_array']['sort_order'] = $this->input->post('sort_order');
            $agenda_id = $this->Agenda_model->add_agenda($data, $Event_id, $cid);
            if ($this->input->post('allow_notification') == '1')
            {
                $session_noti_data['event_id'] = $Event_id;
                $session_noti_data['agenda_id'] = $agenda_id;
                $session_noti_data['title'] = $this->input->post('notification_title');
                $session_noti_data['messages'] = $this->input->post('notification_msg');
                $session_noti_data['notify_datetime'] = $this->input->post('notify_datetime');
                $this->Agenda_model->add_session_notification($session_noti_data);
            }
            $this->session->set_flashdata('agenda_data', 'Added');
            redirect("Agenda_admin/agenda_index/" . $id . '/' . $cid);
        }
        if ($this->data['user']->Role_name == 'Client')
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
            $agenda_list = $this->Agenda_model->get_agenda_list($id);
            $this->data['agenda_list'] = $agenda_list;
            $speakers = $this->Speaker_model->get_speaker_list($id);
            $this->data['speakers'] = $speakers;
        }
        /*sort order*/
        $addSortOrderArr = array();
        $sortorderlist = $this->Agenda_model->get_all_sortorder($id);
        foreach($sortorderlist as $key => $value)
        {
            if ($value['sort_order'] != '')
            {
                $addSortOrderArr[] = $value['sort_order'];
            }
        }
        $sortOrderRange = range(1, 350);
        $arr1 = array_diff($sortOrderRange, $addSortOrderArr);
        $arr2 = array_diff($addSortOrderArr, $sortOrderRange);
        $this->data['sort_order'] = array_merge($arr1, $arr2);
        $this->data['Event_id'] = $Event_id;
        /*sort order*/
        $agenda_list = $this->Agenda_model->get_all_agenda_list_for_add_existing($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'agenda_admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/add_js', $this->data, true);
        $this->template->render();
    }
    public function edit($id = NULL, $agenda_id = NULL, $cid = NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        /*  $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role;*/
        $map_list = $this->Map_model->get_maps($id);
        $this->data['map_list'] = $map_list;
        $speakers = $this->Speaker_model->get_speaker_list($id);
        $this->data['speakers'] = $speakers;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $types = $this->Agenda_model->get_session_type($id);
        $this->data['type_data'] = $types;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $presentation_list = $this->Agenda_model->getAllpresentationByEventId($Event_id);
        $this->data['presentation_list'] = $presentation_list;
        $surveys_list = $this->Agenda_model->get_all_surveys_category_by_event($id);
        $this->data['surveys_list'] = $surveys_list;
        $qasession_list = $this->Agenda_model->get_all_qasession_by_event($Event_id);
        $this->data['qasession_list'] = $qasession_list;
        $doc_list = $this->Agenda_model->getAllDocumentonByEventId($Event_id);
        $this->data['doc_list'] = $doc_list;
        if ($this->data['page_edit_title'] = 'edit')
        {
            if ($id == NULL || $id == '')
            {
                redirect('Agenda_admin');
            }
            if ($this->input->post())
            {
                $agenda_list = $this->Agenda_model->get_agenda_list($id);
                if (!empty($_FILES['session_images']))
                {
                    $tempname = explode('.', $_FILES['session_images']['name']);
                    $tempname_imagename = str_replace(' ', '_', $tempname[0]) . uniqid();
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => '*',
                        "max_size" => '0',
                        "max_width" => '0',
                        "max_height" => '0'
                    ));
                    if ($this->upload->do_upload("session_images"))
                    {
                        $data['agenda_array']['session_image'] = $images_file;
                    }
                }
                $data['agenda_array']['Id'] = $this->uri->segment(4);
                $data['agenda_array']['Organisor_id'] = $logged_in_user_id;
                $data['agenda_array']['Event_id'] = $Event_id;
                $data['agenda_array']['Start_date'] = date('Y-m-d', strtotime($this->input->post('Start_date')));
                $Start_time = $this->input->post('Start_time');
                $Start_time = date('H:i:s', strtotime($Start_time));
                $data['agenda_array']['Start_time'] = $Start_time;
                $data['agenda_array']['End_date'] = date('Y-m-d', strtotime($this->input->post('End_date')));
                $data['agenda_array']['checking_datetime'] = $this->input->post('checking_datetime');
                $End_time = $this->input->post('End_time');
                $End_time = date('H:i:s', strtotime($End_time));
                $data['agenda_array']['End_time'] = $End_time;
                /*if(!empty($event[0]['Event_time_zone'])){
                $inputsdate=date('Y-m-d H:i:s',strtotime($data['agenda_array']['Start_date']." ".$data['agenda_array']['Start_time']));
                $savesdate=date('Y-m-d H:i:s',strtotime($agenda_list[0]['Start_date']." ".$agenda_list[0]['Start_time']));
                $inputedate=date('Y-m-d H:i:s',strtotime($data['agenda_array']['End_date']." ".$data['agenda_array']['End_time']));
                $saveedate=date('Y-m-d H:i:s',strtotime($agenda_list[0]['End_date']." ".$agenda_list[0]['End_time']));
                if(strpos($event[0]['Event_time_zone'],"-")==true)
                {
                $arr=explode("-", $event[0]['Event_time_zone']);
                $offset=explode(":",$arr[1]);
                if(!in_array("30",$offset))
                {
                $intoffset=$offset[0]*3600;
                }
                else
                {
                $intoffset=$offset[0].'.5';
                $intoffset=$intoffset*3600;
                }
                $intNew = abs($intoffset);
                if($inputsdate!=$savesdate){
                $StartDateTime = date('Y-m-d H:i:s',strtotime($data['agenda_array']['Start_date']." ".$data['agenda_array']['Start_time']) - $intNew);
                }
                if($inputedate!=$saveedate){
                $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['agenda_array']['End_date']." ".$data['agenda_array']['End_time']) - $intNew);
                }
                }
                else
                {
                $arr=explode("+", $event[0]['Event_time_zone']);
                $offset=explode(":",$arr[1]);
                if(!in_array("30",$offset))
                {
                $intoffset=$offset[0]*3600;
                }
                else
                {
                $intoffset=$offset[0].'.5';
                $intoffset=$intoffset*3600;
                }
                $intNew = abs($intoffset);
                if($inputsdate!=$savesdate){
                $StartDateTime = date('Y-m-d H:i:s',strtotime($data['agenda_array']['Start_date']." ".$data['agenda_array']['Start_time']) + $intNew);
                }
                if($inputedate!=$saveedate){
                $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['agenda_array']['End_date']." ".$data['agenda_array']['End_time']) + $intNew);
                }
                }
                if($inputsdate!=$savesdate){
                $data['agenda_array']['Start_date']=date('Y-m-d', strtotime($StartDateTime));
                $data['agenda_array']['Start_time'] = date('H:i:s', strtotime($StartDateTime));
                }
                if($inputedate!=$saveedate){
                $data['agenda_array']['End_time']= date('H:i:s', strtotime($EndDateTime));
                $data['agenda_array']['End_date']=date('Y-m-d', strtotime($EndDateTime));
                }
                }*/
                $data['agenda_array']['Heading'] = $this->input->post('Heading');
                $data['agenda_array']['Types'] = $this->input->post('Types');
                if ($this->input->post('Types') == 'Other')
                {
                    $data['agenda_array']['other_types'] = $this->input->post('other_types');
                }
                // echo $this->input->post('doc_id');die;
                $data['agenda_array']['document_id'] = $this->input->post('doc_id');
                $data['agenda_array']['short_desc'] = $this->input->post('short_desc');
                $data['agenda_array']['presentation_id'] = $this->input->post('presentation_id');
                $data['agenda_array']['description'] = $this->input->post('description');
                $data['agenda_array']['Address_map'] = $this->input->post('Address_map');
                $data['agenda_array']['Agenda_status'] = $this->input->post('Agenda_status');
                $data['agenda_array']['Maximum_People'] = $this->input->post('Maximum_People');
                $data['agenda_array']['custom_speaker_name'] = $this->input->post('custom_speaker_name');
                $data['agenda_array']['custom_location'] = $this->input->post('custom_location');
                if ($this->input->post('show_places_remaining') == '1')
                {
                    $data['agenda_array']['show_places_remaining'] = $this->input->post('show_places_remaining');
                }
                else
                {
                    $data['agenda_array']['show_places_remaining'] = '0';
                }
                if ($this->input->post('show_checking_in') == '1')
                {
                    $data['agenda_array']['show_checking_in'] = $this->input->post('show_checking_in');
                }
                else
                {
                    $data['agenda_array']['show_checking_in'] = '0';
                }
                if ($this->input->post('show_rating') == '1')
                {
                    $data['agenda_array']['show_rating'] = $this->input->post('show_rating');
                }
                else
                {
                    $data['agenda_array']['show_rating'] = '0';
                }
                if ($this->input->post('allow_clashing') == '1')
                {
                    $data['agenda_array']['allow_clashing'] = $this->input->post('allow_clashing');
                }
                else
                {
                    $data['agenda_array']['allow_clashing'] = '0';
                }
                if ($this->input->post('allow_comments') == '1')
                {
                    $data['agenda_array']['allow_comments'] = $this->input->post('allow_comments');
                }
                else
                {
                    $data['agenda_array']['allow_comments'] = '0';
                }
                if (!empty($this->input->post('Speaker_id')))
                {
                    $data['agenda_array']['Speaker_id'] = implode(',', $this->input->post('Speaker_id'));
                }
                else
                {
                    $data['agenda_array']['Speaker_id'] = NULL;
                }
                if (empty($this->input->post('qasession_id')))
                {
                    $data['agenda_array']['qasession_id'] = NULL;
                }
                else
                {
                    $data['agenda_array']['qasession_id'] = $this->input->post('qasession_id');
                }
                $data['agenda_array']['survey_id'] = $this->input->post('survey_id');
                $data['agenda_array']['sort_order'] = $this->input->post('sort_order');
                $this->Agenda_model->update_agenda($data, $agenda_id, $Event_id);
                if ($this->input->post('allow_notification') == '1')
                {
                    $session_noti_data['title'] = $this->input->post('notification_title');
                    $session_noti_data['messages'] = $this->input->post('notification_msg');
                    $session_noti_data['notify_datetime'] = $this->input->post('notify_datetime');
                    if (!empty($agenda_list[0]['notify_id']))
                    {
                        $this->Agenda_model->update_session_notification($session_noti_data, $Event_id, $agenda_id);
                    }
                    else
                    {
                        $session_noti_data['event_id'] = $Event_id;
                        $session_noti_data['agenda_id'] = $agenda_id;
                        $this->Agenda_model->add_session_notification($session_noti_data);
                    }
                }
                $this->session->set_flashdata('agenda_data', 'Updated');
                redirect("Agenda_admin/agenda_index/" . $id . '/' . $cid);
            }
            /*sort order*/
            $addSortOrderArr = array();
            $sortorderlist = $this->Agenda_model->get_all_sortorder($id, $agenda_id);
            foreach($sortorderlist as $key => $value)
            {
                if ($value['sort_order'] != '')
                {
                    $addSortOrderArr[] = $value['sort_order'];
                }
            }
            $sortOrderRange = range(1, 350);
            $arr1 = array_diff($sortOrderRange, $addSortOrderArr);
            $arr2 = array_diff($addSortOrderArr, $sortOrderRange);
            $this->data['sort_order'] = array_merge($arr1, $arr2);
            $this->data['Event_id'] = $Event_id;
            /*sort order*/
            $user = $this->session->userdata('current_user');
            $roleid = $user[0]->Role_id;
            $event_id = $id;
            $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
            $this->data['users_role'] = $user_role;
            $this->session->set_userdata($data);
            $this->template->write_view('css', 'agenda_admin/add_css', $this->data, true);
            $this->template->write_view('content', 'agenda_admin/edit', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $total_permission = $this->Agenda_model->get_permission_list();
                $this->data['total_permission'] = $total_permission;
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'agenda_admin/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function delete($Event_id = NULL, $id = NULL, $cid = NULL)

    {
        if ($this->data['user']->Role_name == 'Client')
        {
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
            $agenda_list = $this->Agenda_model->get_agenda_list($id);
            $this->data['agenda_list'] = $agenda_list[0];
        }
        $agenda = $this->Agenda_model->delete_agenda($id, $cid, $Event_id);
        $this->session->set_flashdata('agenda_data', 'Deleted');
        redirect("Agenda_admin/agenda_index/" . $Event_id . '/' . $cid);
    }
    public function saveshowoption($id = NULL)

    {
        if ($this->input->post('show_session_by_time') == 1)
        {
            $show_session['event_array']['show_session_by_time'] = $this->input->post('show_session_by_time');
        }
        else
        {
            $show_session['event_array']['show_session_by_time'] = '0';
        }
        $show_session['event_array']['Id'] = $id;
        $this->Event_model->update_admin_event($show_session);
        redirect("Agenda_admin/index/" . $id);
    }
    public function saveshowpendingoption($id = NULL)

    {
        if ($this->input->post('on_pending_agenda') == 1)
        {
            $show_session['event_array']['on_pending_agenda'] = $this->input->post('on_pending_agenda');
        }
        else
        {
            $show_session['event_array']['on_pending_agenda'] = '0';
        }
        $show_session['event_array']['Id'] = $id;
        $this->Event_model->update_admin_event($show_session);
        redirect("Agenda_admin/index/" . $id);
    }
    public function show_check_in_time($id = NULL, $cid = NULL)

    {
        echo $this->Agenda_model->show_check_in_time_by_category($id, $cid);
        die;
    }
    public function checkvalid_typename($eid = NULL)

    {
        $type_name = $this->input->post('type_name');
        $type_id = $this->input->post('type_id');
        $type = $this->Agenda_model->check_exists_type_name($eid, $type_name, $type_id);
        if (count($type) <= 0)
        {
            echo 'TRUE';
        }
        else
        {
            echo 'FALSE';
        }
        die;
    }
    public function save_new_types($eid = NULL, $cid = NULL)

    {
        $type_name = $this->input->post('type_name');
        $type_id = $this->input->post('type_id');
        $type = $this->Agenda_model->check_exists_type_name($eid, $type_name, $type_id);
        if (count($type) <= 0)
        {
            if (empty($type_id))
            {
                $addtype_id = $this->Agenda_model->add_session_types($eid, $type_name);
            }
            else
            {
                $updatetype_id = $this->Agenda_model->update_session_types($type_id, $type_name);
            }
            $this->session->set_flashdata('agenda_data', "Type Save");
        }
        else
        {
            $this->session->set_flashdata('session_type_error', "Session Type Already Exists Please Enter Another Type");
        }
        redirect(base_url() . 'Agenda_admin/agenda_index/' . $eid . '/' . $cid);
    }
    public function ajax_save_new_types($eid = NULL)

    {
        $type_name = $this->input->post('type_name');
        $type = $this->Agenda_model->check_exists_type_name($eid, $type_name, NULL);
        if (count($type) <= 0)
        {
            $addtype_id = $this->Agenda_model->add_session_types($eid, $type_name);
            echo "success###" . $addtype_id;
        }
        else
        {
            echo "error###Session Type Already Exists Please Enter Another Type";
        }
        die;
    }
    public function delete_session_type($eid = NULL, $tid = NULL, $cid = NULL)

    {
        $this->Agenda_model->remove_session_type($tid, $eid);
        $this->session->set_flashdata('agenda_data', "Type Deleted");
        redirect(base_url() . 'Agenda_admin/agenda_index/' . $eid . '/' . $cid);
    }
    public function mass_upload_page($id = NULL, $cid = NULL)

    {
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
            $this->data['agenda_list'] = $agenda_list;
        }
        $this->data['category_data'] = $this->Agenda_model->get_agenda_category_by_id($cid);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['acid'] = $cid;
        $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
        $this->data['agenda_list'] = $agenda_list;
        $time_format = $this->Event_model->getTimeFormat($id);
        $this->data['time_format'] = $time_format;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 1);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['acid'] = $cid;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'agenda_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/mass_add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/js', true);
        $this->template->render();
    }
    public function download_template_csv($id = NULL)

    {
        $this->load->helper('download');
        $filename = "Session_demo.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Start Date";
        $header[] = "Start Time";
        $header[] = "End Date";
        $header[] = "End Time";
        $header[] = "Time to allow Check In";
        $header[] = "Session Name";
        $header[] = "Session Type";
        $header[] = "Speaker Email";
        $header[] = "Map Code";
        $header[] = "Description";
        $header[] = "Maximum People";
        $header[] = "Show Number Of Places Remaining";
        $header[] = "Activate Checking In";
        $header[] = "Activate Session Ratings";
        $header[] = "Custom Speaker Name";
        $header[] = "Custom Location";
        $header[] = "Allow Users to save another session which clashes with this session";
        $header[] = "Allow Users to Comment on Sessions";
        $header[] = "Send Notification to Attendees who have saved this session";
        $header[] = "Notification Title";
        $header[] = "Notification Message";
        $header[] = "Time to send notificationÃ¢â‚¬â€¹";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $data['Start Date'] = date('Y-m-d');
        $data['Start Time'] = date('H:i:s');
        $data['End Date'] = date('Y-m-d');
        $data['End Time'] = date('H:i:s');
        $data['Time to allow Check In'] = date('Y-m-d H:i:s');
        $data['Session Name'] = "Sesstion name";
        $data['Session Type'] = "Sesstion Type";
        $data['Speaker_id'] = "for e.g(a@g.com,b@g.com)";
        $data['Address_map'] = "Map Code";
        $data['Description'] = "Sesstion Description";
        $data['Maximum People'] = "15";
        $data['Show Number Of Places Remaining'] = "for e.g(0,1)";
        $data['Activate Checking In'] = "for e.g(0,1)";
        $data['Activate Session Ratings'] = "for e.g(0,1)";
        $data['Custom Speaker Name'] = "name";
        $data['Custom Location'] = "location";
        $data['allow clashes'] = "for e.g(0,1)";
        $data['allow Comments'] = "for e.g(0,1)";
        $data['allow Notification'] = "for e.g(0,1)";
        $data['Notification Title'] = "Notification Title";
        $data['Notification Message'] = "Notification Message";
        $data['Time to send Notification'] = date('Y-m-d H:i:s');
        fputcsv($fp, $data);
    }
    public function upload_csv_session($id = NULL, $cid = NULL)

    {
        $event = $this->Event_model->get_admin_event($id);
        if (pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION) == "csv")
        {
            $file = $_FILES['csv']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            while (($datacsv = fgetcsv($handle, 0, ",")) !== FALSE)
            {
                if ($find_header != '0' && count($datacsv) > 1)
                {
                    $data['agenda_array']['Organisor_id'] = $event[0]['Organisor_id'];
                    $data['agenda_array']['Event_id'] = $id;
                    $data['agenda_array']['Start_date'] = date('Y-m-d', strtotime($datacsv[0]));
                    $data['agenda_array']['Start_time'] = date('H:i:s', strtotime($datacsv[1]));
                    $data['agenda_array']['End_date'] = date('Y-m-d', strtotime($datacsv[2]));
                    $data['agenda_array']['End_time'] = date('H:i:s', strtotime($datacsv[3]));
                    if (!empty($datacsv[4]))
                    {
                        $data['agenda_array']['checking_datetime'] = date('Y-m-d H:i:s', strtotime($datacsv[4]));
                    }
                    $data['agenda_array']['Heading'] = $datacsv[5];
                    $check = $this->Agenda_model->check_exists_type_name($id, $datacsv[6]);
                    if (count($check))
                    {
                        $type_id = $check[0]['type_id'];
                    }
                    else
                    {
                        $type_id = $this->Agenda_model->add_session_types($id, $datacsv[6]);
                    }
                    $data['agenda_array']['Types'] = $type_id;
                    if (!empty($datacsv[7]))
                    {
                        $s_email = explode(",", $datacsv[7]);
                        $s_ids = array();
                        foreach($s_email as $key1 => $value1)
                        {
                            $s_ids[$key1] = $this->Agenda_model->get_speaker_id_by_email($value1, $id);
                        }
                        $data['agenda_array']['Speaker_id'] = implode(",", $s_ids);
                    }
                    if (!empty($datacsv[8]))
                    {
                        $data['agenda_array']['Address_map'] = $this->Agenda_model->get_map_id_by_map_code($datacsv[8], $id);
                    }
                    $data['agenda_array']['Agenda_status'] = '1';
                    $data['agenda_array']['description'] = $datacsv[9];
                    $data['agenda_array']['Maximum_People'] = $datacsv[10];
                    $data['agenda_array']['show_places_remaining'] = $datacsv[11];
                    $data['agenda_array']['show_checking_in'] = $datacsv[12];
                    $data['agenda_array']['show_rating'] = $datacsv[13];
                    $data['agenda_array']['custom_speaker_name'] = $datacsv[14];
                    $data['agenda_array']['custom_location'] = $datacsv[15];
                    $data['agenda_array']['allow_clashing'] = $datacsv[16];
                    $data['agenda_array']['allow_comments'] = $datacsv[17];
                    $agenda_id = $this->Agenda_model->add_agenda($data, $id, $cid);
                    if ($datacsv[18] == '1')
                    {
                        $session_noti_data['event_id'] = $id;
                        $session_noti_data['agenda_id'] = $agenda_id;
                        $session_noti_data['title'] = $datacsv[19];
                        $session_noti_data['messages'] = $datacsv[20];
                        $session_noti_data['notify_datetime'] = date('Y-m-d H:i:s', strtotime($datacsv[21]));
                        $this->Agenda_model->add_session_notification($session_noti_data);
                    }
                }
                $find_header++;
            }
            $this->session->set_flashdata('agenda_data', 'Added');
            redirect(base_url() . 'Agenda_admin/agenda_index/' . $id . '/' . $cid);
        }
        else
        {
            $this->session->set_flashdata('session_error_data', ' please Upload csv Files.');
            redirect(base_url() . 'Agenda_admin/mass_upload_page/' . $id . '/' . $cid);
        }
    }
    public function make_it_primary_agenda_categories($eid = NULL, $acid = NULL)

    {
        $this->Agenda_model->make_primary_agenda_categories($eid, $acid);
        $this->session->set_flashdata('category_data', 'Updated');
        redirect("Agenda_admin/index/" . $eid);
    }
    public function duplicate_session($eid = NULL)

    {
        $agenda_list = $this->Agenda_model->agenda_id_listby_session_id($this->input->post('session_id'));
        $category_data = $this->Agenda_model->get_agenda_category_by_id($this->input->post('session_id'));
        $data['category_name'] = $this->input->post('session_Title');
        $getcate = $this->Agenda_model->get_agenda_category_by_name($data['category_name'], $eid, NULL);
        if (count($getcate) > 0)
        {
            $this->session->set_flashdata('category_error', 'Agenda Name already exist. Please choose another Agenda Name.');
            redirect("Agenda_admin/index/" . $eid);
        }
        else
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $data['show_check_in_time'] = $category_data[0]['show_check_in_time'];
            $category_id = $this->Agenda_model->add_agenda_category($data, $eid);
        }
        array_walk_recursive($agenda_list, array(
            $this,
            'update_somthing'
        ) , array(
            'matchkey' => 'category_id',
            'newval' => $category_id
        ));
        $this->Event_model->save_data_databasetables('agenda_category_relation', $agenda_list);
        $this->session->set_flashdata('category_data', ' Added ');
        redirect(base_url() . 'Agenda_admin/index/' . $eid);
    }
    public function update_somthing(&$item = NULL, $key = NULL, $arr = NULL)

    {
        if ($key == $arr['matchkey']) $item = $arr['newval'];
    }
    public function save_show_agenda_column_option($eid = NULL)

    {
        if ($this->input->post('show_agenda_place_left_column') == 1)
        {
            $show_session['event_array']['show_agenda_place_left_column'] = $this->input->post('show_agenda_place_left_column');
        }
        else
        {
            $show_session['event_array']['show_agenda_place_left_column'] = '0';
        }
        if ($this->input->post('show_agenda_speaker_column') == 1)
        {
            $show_session['event_array']['show_agenda_speaker_column'] = $this->input->post('show_agenda_speaker_column');
        }
        else
        {
            $show_session['event_array']['show_agenda_speaker_column'] = '0';
        }
        if ($this->input->post('show_agenda_location_column') == 1)
        {
            $show_session['event_array']['show_agenda_location_column'] = $this->input->post('show_agenda_location_column');
        }
        else
        {
            $show_session['event_array']['show_agenda_location_column'] = '0';
        }
        $show_session['event_array']['Id'] = $eid;
        $this->Event_model->update_admin_event($show_session);
        redirect("Agenda_admin/index/" . $eid);
    }
    /*public function get_all_comment_in_agenda($eid)
    {
    $this->load->helper('download');
    $this->db->select('u.Firstname,u.Lastname,u.Email,u.Title,u.Company_name,a.Heading,ac.comments')->from('agenda_comments ac');
    $this->db->join('agenda a','ac.agenda_id=a.Id','left');
    $this->db->join('user u','ac.user_id=u.Id');
    $this->db->where('ac.event_id',$eid);
    $this->db->order_by('a.Id');
    $res=$this->db->get()->result_array();
    // echo "<pre>";print_r($res);die;
    $filename = "Session_comment.csv";
    $fp = fopen('php://output', 'w');
    $header[] = "First Name";
    $header[] = "Last Name";
    $header[] = "Email";
    $header[] = "Title";
    $header[] = "Company";
    $header[] = "Agenda Name";
    $header[] = "Comment";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    fputcsv($fp, $header);
    foreach ($res as $key => $value) {
    fputcsv($fp, $value);
    }
    }*/
    public function delete_session_image($eid = NULL, $agenda_id = NULL, $acid = NULL)

    {
        $agenda_list = $this->Agenda_model->get_agenda_list($eid);
        unlink('./assets/user_files/' . $agenda_list[0]['session_image']);
        $this->Agenda_model->delete_agenda_images($eid, $agenda_id);
        redirect(base_url() . 'Agenda_admin/edit/' . $eid . '/' . $agenda_id . '/' . $acid);
    }
    public function assign_agenda_ticket($eid = NULL, $cid = NULL)

    {
        foreach($this->input->post('agenda_id') as $key => $value)
        {
            $this->Agenda_model->assign_ticket_to_agenda($value, $this->input->post('ticket_type'));
        }
        echo "Success###";
        die;
    }
    public function export_agenda_rating($eid = NULL)

    {
        $res = $this->Agenda_model->get_export_session_rating_users($eid);
        $filename = "Session_rating.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Session Heading";
        $header[] = "Start Time";
        $header[] = "End Time";
        $header[] = "Start Date";
        $header[] = "End Date";
        $header[] = "Rating Given";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Title";
        $header[] = "Company Name";
        $header[] = "Email";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($res as $key => $value)
        {
            $sdata['Heading'] = $value['Heading'];
            $sdata['Start_time'] = $value['Start_time'];
            $sdata['End_time'] = $value['End_time'];
            $sdata['Start_date'] = $value['Start_date'];
            $sdata['End_date'] = $value['End_date'];
            $sdata['rating'] = $value['rating'];
            $sdata['Firstname'] = ucfirst($value['Firstname']);
            $sdata['Lastname'] = ucfirst($value['Lastname']);
            $sdata['Title'] = $value['Title'];
            $sdata['Company Name'] = $value['Company_name'];
            $sdata['Email'] = $value['Email'];
            fputcsv($fp, $sdata);
        }
    }
    public function export_agenda_comment($eid = NULL)

    {
        $res = $this->Agenda_model->get_export_session_comment_users($eid);
        $filename = "Session_comment.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Session Heading";
        $header[] = "Start Time";
        $header[] = "End Time";
        $header[] = "Start Date";
        $header[] = "End Date";
        $header[] = "Comment Given";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Title";
        $header[] = "Company Name";
        $header[] = "Email";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($res as $key => $value)
        {
            $sdata['Heading'] = $value['Heading'];
            $sdata['Start_time'] = $value['Start_time'];
            $sdata['End_time'] = $value['End_time'];
            $sdata['Start_date'] = $value['Start_date'];
            $sdata['End_date'] = $value['End_date'];
            $sdata['comments'] = $value['comments'];
            $sdata['Firstname'] = ucfirst($value['Firstname']);
            $sdata['Lastname'] = ucfirst($value['Lastname']);
            $sdata['Title'] = $value['Title'];
            $sdata['Company Name'] = $value['Company_name'];
            $sdata['Email'] = $value['Email'];
            fputcsv($fp, $sdata);
        }
    }
    public function order_session_type($id = NULL, $cid = NULL)

    {
        if ($this->input->post())
        {
            foreach($this->input->post('type_ids') as $key => $value)
            {
                $this->Agenda_model->save_session_type_order_number($value, $key + 1);
            }
            $this->session->set_flashdata('agenda_data', "Type Order Save");
            redirect(base_url() . 'Agenda_admin/agenda_index/' . $id . '/' . $cid);
        }
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;
            $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
            $this->data['agenda_list'] = $agenda_list;
        }
        $this->data['category_data'] = $this->Agenda_model->get_agenda_category_by_id($cid);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['acid'] = $cid;
        $agenda_list = $this->Agenda_model->get_all_agenda_list($id, $cid);
        $this->data['agenda_list'] = $agenda_list;
        $time_format = $this->Event_model->getTimeFormat($id);
        $this->data['time_format'] = $time_format;
        $session_types = $this->Agenda_model->get_session_type($id);
        $this->data['session_types'] = $session_types;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 1);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['acid'] = $cid;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'agenda_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/order_sessiontype_views', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/js', true);
        $this->template->render();
    }
    public function saveshowallagendaoption($eid = NULL)

    {
        if ($this->input->post('allow_show_all_agenda') == 1)
        {
            $show_all_agenda['event_array']['allow_show_all_agenda'] = $this->input->post('allow_show_all_agenda');
        }
        else
        {
            $show_all_agenda['event_array']['allow_show_all_agenda'] = '0';
        }
        $show_all_agenda['event_array']['Id'] = $eid;
        $this->Event_model->update_admin_event($show_all_agenda);
        redirect("Agenda_admin/index/" . $eid);
    }
    public function saveagendasort($eid = NULL)

    {
        if ($this->input->post('agenda_sort') == 1)
        {
            $agenda_sort['event_array']['agenda_sort'] = $this->input->post('agenda_sort');
        }
        else
        {
            $agenda_sort['event_array']['agenda_sort'] = '0';
        }
        $agenda_sort['event_array']['Id'] = $eid;
        $this->Event_model->update_admin_event($agenda_sort);
        redirect("Agenda_admin/index/" . $eid);
    }
    public function add_group($id = NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 1;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, NULL);
            redirect(base_url() . 'Agenda_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/add_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_group($id = NULL, $mgid = NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_group_data($mgid, $id, 1);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 1;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, $mgid);
            redirect(base_url() . 'Agenda_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/edit_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_group($event_id = NULL, $group_id = NULL)

    {
        $this->Event_template_model->delete_group($group_id, $event_id);
        $this->session->set_flashdata('map_data', 'Group Deleted');
        redirect(base_url() . 'Agenda_admin/index/' . $event_id);
    }
    public function view_session_comments($id = NULL, $agenda_id = NULL, $cid = NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $this->data['agenda_data'] = $this->Agenda_model->get_agenda_and_comments($agenda_id, $id);
        $user = $this->session->userdata('current_user');
        $this->data['user'] = $user;
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->session->set_userdata($data);
        $this->data['menu_data'] = $this->Event_model->geteventmenu($id, 1);
        $this->data['menu_toal_data'] = $this->Event_model->get_total_menu($id);
        $this->template->write_view('css', 'agenda_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/session_comments', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'agenda_admin/js', true);
        $this->template->render();
    }
    public function import_agenda($id)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'agenda_admin/import_agenda', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function save_import_agenda($id = NULL)

    {
        $event_data = $this->Event_model->get_event_accesskey_id($id, $this->input->post('access_key'));
        /*if(empty($event_data))
        {
        $this->session->set_flashdata('csv_flash_message', 'Access key is wrong.');
        redirect('agenda_admin/import_agenda/'.$id);
        }*/
        if ($this->input->post('file-type') == 'JSON')
        {
            $data['json_url'] = $this->input->post('json_url');
            $data['import_type'] = 'agenda';
            $data['event_id'] = $id;
            $data['file_type'] = '1';
            $this->Event_model->saveEventCSVImport($data);
            $event_data = $this->Event_model->get_event_accesskey_id($id, $this->input->post('access_key'));
            $saveCSVData = $this->Event_model->saveJSONDataAgenda($data['json_url'], $event_data['Id'], $event_data['Organisor_id']);
            $this->session->set_flashdata('csv_flash_message', 'JSON Imported Successfully.');
            redirect(base_url() . 'Agenda_admin/import_agenda/' . $id);
        }
        else
        {
            session_start();
            $filepath = $this->input->post('csv_url');
            $contents = file_get_contents($this->input->post('csv_url'));
            if (strlen($contents))
            {
                $_SESSION['access_key'] = $this->input->post('access_key');
                $_SESSION['csv_url'] = $this->input->post('csv_url');
                $handle = fopen($filepath, "r");
                $find_header = 0;
                while (($datacsv[] = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                }
                $keys = array_shift($datacsv);
                foreach($datacsv as $key => $value)
                {
                    $csv[] = array_combine($keys, $value);
                }
                $nomatch = array();
                $csvtest = array_filter($csv);
                $this->data['import_csv_data'] = $csvtest;
                $_SESSION['save_import_attendees'] = $csvtest;
                $this->data['keysdata'] = $keys;
                foreach($csv as $key => $value)
                {
                    $keysarr = array_keys($value);
                    $fixedc = array(
                        'Start_date',
                        'Start_time',
                        'End_date',
                        'End_time',
                        'Heading',
                        'Types',
                        'description',
                        'session_image',
                        'custom_location',
                        'custom_speaker_name',
                        'session_code',
                        'agenda_name'
                    );
                    for ($i = 0; $i < count($keysarr); $i++)
                    {
                        if (!in_array($keysarr[$i], $fixedc))
                        {
                            $kk = $keysarr[$i];
                            if (!in_array($kk, $nomatch))
                            {
                                $nomatch[] = $kk;
                            }
                        }
                    }
                }
            }
            else
            {
                $this->session->set_flashdata('csv_flash_message', 'CSV file not found.');
                redirect('agenda_admin/import_agenda/' . $id);
            }
            $this->data['nomatch'] = count($nomatch);
            $this->data['Event_id'] = $id;
            $statelist = $this->Profile_model->statelist();
            $this->data['Statelist'] = $statelist;
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
            $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
            $this->data['category_list'] = $category_list;
            $agenda_list = $this->Agenda_model->get_agenda_list($id);
            $this->data['agenda_list'] = $agenda_list;
            $this->template->write_view('css', 'admin/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'agenda_admin/maping_import_agenda', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
            $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
            $this->template->render();
        }
    }
    public function download_agenda_template_csv($eid = NULL)

    {
        $this->load->helper('download');
        $filename = "import_agenda.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Start_date";
        $header[] = "Start_time";
        $header[] = "End_date";
        $header[] = "End_time";
        $header[] = "Heading";
        $header[] = "Types";
        $header[] = "description";
        $header[] = "session_image";
        $header[] = "custom_location";
        $header[] = "custom_speaker_name";
        $header[] = "session_code";
        $header[] = "agenda_name";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $custom_speaker_name = array(
            "Kay",
            "Joe",
            "Susan",
            "Frank"
        );
        for ($i = 1; $i <= 1; $i++)
        {
            $data['Start_date'] = "2018-05-28";
            $data['Start_time'] = "12:10:00";
            $data['End_date'] = "2018-05-28";
            $data['End_time'] = "12:30:00";
            $data['Heading'] = "Welcome session";
            $data['Types'] = "Type";
            $data['description'] = "Description";
            $data['session_image'] = "";
            $data['custom_location'] = "Hall " . rand('1', '6');
            $data['custom_speaker_name'] = $custom_speaker_name[array_rand($custom_speaker_name) ];
            $data['session_code'] = "sccdr" . $i;
            $data['agenda_name'] = "Agenda 1";
            fputcsv($fp, $data);
        }
    }
    public function download_import_agenda_json()

    {
        error_reporting(E_ALL);
        $this->load->helper('download');
        $filename = "import_agenda.json";
        $fp = fopen('php://output', 'w');
        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename=' . $filename);
        $custom_speaker_name = array(
            "Kay",
            "Joe",
            "Susan",
            "Frank"
        );
        for ($i = 1; $i <= 1; $i++)
        {
            $data['Start_date'] = "2018-05-28";
            $data['Start_time'] = "12:10:00";
            $data['End_date'] = "2018-05-28";
            $data['End_time'] = "12:30:00";
            $data['Heading'] = "Welcome session";
            $data['Types'] = "Type";
            $data['description'] = "Description";
            $data['session_image'] = "";
            $data['custom_location'] = "Hall " . rand('1', '6');
            $data['custom_speaker_name'] = $custom_speaker_name[array_rand($custom_speaker_name) ];
            $data['session_code'] = "sccdr" . $i;
            $data['agenda_name'] = "Agenda 1";
            $agenda[] = $data;
        }
        $data = json_encode($agenda);
        fputs($fp, $data);
    }
    public function show_import_agenda_json()

    {
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
    {
        "Start_date": "<font color=green><b>(String)</b></font> Start Date if Agenda <font color=red><b>**Required**</b></font>",
        "Start_time": "<font color=green><b>(String)</b></font> Start time of Agenda <font color=red><b>**Required**</b></font>",
        "End_date": "<font color=green><b>(String)</b></font> End date of Agenda <font color=red><b>**Required**</b></font>",
        "End_time": "<font color=green><b>(String)</b></font> End time of Agenda <font color=red><b>**Required**</b></font>",
        "Heading": "<font color=green><b>(String)</b></font> Heading <font color=red><b>**Required**</b></font>", 
        "Types": "<font color=green><b>(String)</b></font> Types <font color=red><b>**Required**</b></font>",
        "description": "<font color=green><b>(String)</b></font> description",
        "session_image": "<font color=green><b>(String)</b></font> session_image"
        "custom_location": "<font color=green><b>(String)</b></font> Custom Location",
        "custom_speaker_name": "<font color=green><b>(String)</b></font> Custom Speaker Name",
        "session_code": "<font color=green><b>(String)</b></font> Session Code <font color=red><b>**Required**</b></font>",
        "agenda_name": "<font color=green><b>(String)</b></font> Name of Agenda Category <font color=red><b>**Required**</b></font>"
    },
    {
        "Start_date": "2018-05-28",
        "Start_time": "12:10:00",
        "End_date": "2018-05-28",
        "End_time": "12:30:00",
        "Heading": "Welcome session",
        "Types": "Type",
        "description": "Description",
        "session_image": "Image URL",
        "custom_location": "Hall 1",
        "custom_speaker_name": "John Doe",
        "session_code": "sccdr1",
        "agenda_name": "Agenda 1"
    }
]';
        exit();
        error_reporting(E_ALL);
        header('Content-type: application/json');
        for ($i = 0; $i < 2; $i++)
        {
            if ($i == 0)
            {
                $data['Start_date'] = "(String) Start Date if Agenda";
                $data['Start_time'] = "(String) Start time of Agenda";
                $data['End_date'] = "(String) End date of Agenda";
                $data['End_time'] = "(String) End time of Agenda **Required**";
                $data['Heading'] = "(String) Heading";
                $data['Types'] = "(String) Types";
                $data['description'] = "(String) description";
                $data['session_image'] = "(String) session_image";
                $data['custom_location'] = "(String) Custom Location";
                $data['custom_speaker_name'] = "(String) Custom Speaker Name";
                $data['session_code'] = "(String) Session Code";
                $data['agenda_name'] = "(String) Name of Agenda Category";
            }
            else
            {
                $data['Start_date'] = "2018-05-28";
                $data['Start_time'] = "12:10:00";
                $data['End_date'] = "2018-05-28";
                $data['End_time'] = "12:30:00";
                $data['Heading'] = "Welcome session";
                $data['Types'] = "Type";
                $data['description'] = "Description";
                $data['session_image'] = "";
                $data['custom_location'] = "Hall 1";
                $data['custom_speaker_name'] = "John Doe";
                $data['session_code'] = "sccdr1";
                $data['agenda_name'] = "Agenda 1";
            }
            $fin[$i] = $data;
        }
        echo json_encode($fin, JSON_PRETTY_PRINT);
    }
    public function upload_import_agenda($eventid = NULL)

    {
        session_start();
        $key = array_keys($this->input->post());
        $postval = array_values($this->input->post());
        $postarr = array_combine($key, $postval);
        asort($postarr);
        $saveEventCSV['event_id'] = $eventid;
        $saveEventCSV['import_type'] = 'agenda';
        $saveEventCSV['csv_url'] = $_SESSION['csv_url'];
        $saveEventCSV['csv_mapping'] = json_encode($postarr);
        $csvSave = $this->Event_model->saveEventCSVImport($saveEventCSV);
        $access_key = $_SESSION['access_key'];
        $event_data = $this->Event_model->get_event_accesskey_id($eventid, $access_key);
        $saveCSVData = $this->Event_model->saveCSVDataAgenda($saveEventCSV['csv_url'], $event_data['Id'], $event_data['Organisor_id']);
        unset($_SESSION['save_import_attendees']);
        unset($_SESSION['access_key']);
        unset($_SESSION['csv_url']);
        $this->session->set_flashdata('csv_flash_message', 'CSV Import Successfully.');
        redirect(base_url() . 'Agenda_admin/import_agenda/' . $eventid);
    }
    public function downloadDeepLink($event_id = NULL, $id = NULL)

    {
        $this->Agenda_model->downloadDeepLink($event_id, $id);
    }
}