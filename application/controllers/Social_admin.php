<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Social_admin extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Social Media Links';
          $this->data['smalltitle'] = 'Add your social media links';
          $this->data['breadcrumb'] = 'Social Media Links';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('17',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Social Media Links")
               {
                    $title = "Social";
               }

               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
             
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('17',$menu_list))
          {
               $this->load->model('Social_model');
               $this->load->model('Agenda_model');
               $this->load->model('Event_template_model');
               $this->load->model('Setting_model');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id = NULL)
     {

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;


          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $social = $this->Social_model->get_social_list($id);
          $this->data['social'] = $social;

          $menudata = $this->Event_model->geteventmenu($id, 17);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'social_admin/index', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('js', 'social_admin/js', $this->data, true);
          $this->template->render();
     }

     public function add($id = NULL)
     {
          $Event_id = $id;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $logged_in_user_id = $user[0]->Id;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          if ($this->input->post())
          {
               $data['social_array']['Organisor_id'] = $logged_in_user_id;
               $data['social_array']['Event_id'] = $Event_id;
               $data['social_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['social_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['social_array']['instagram_url'] = $this->input->post('instagram_url');
               $data['social_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['social_array']['pinterest_url'] = $this->input->post('pinterest_url');
               $data['social_array']['youtube_url'] = $this->input->post('youtube_url');

               $social_id = $this->Social_model->add_social($data);
               $this->session->set_flashdata('social_data', 'Added');
               redirect("Social_admin/index/" . $id);
          }

          $this->template->write_view('css', 'admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'social_admin/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'social_admin/js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = NULL,$sid = NULL)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $social = $this->Social_model->get_social_list($id,$sid);
          $this->data['social'] = $social;

          if ($id == NULL || $id == '')
          {
               redirect('Social_admin');
          }
          if ($this->input->post())
          {

               $data['social_array']['Organisor_id'] = $logged_in_user_id;
               $data['social_array']['Event_id'] = $Event_id;
               $data['social_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['social_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['social_array']['instagram_url'] = $this->input->post('instagram_url');
               $data['social_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['social_array']['pinterest_url'] = $this->input->post('pinterest_url');
               $data['social_array']['youtube_url'] = $this->input->post('youtube_url');

               $this->Social_model->update_social($data);
               $this->session->set_flashdata('social_data', 'Updated');
               redirect("Social_admin/index/" . $id);
          }

          $this->session->set_userdata($data);
          $this->template->write_view('css', 'admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'social_admin/edit', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'social_admin/js', $this->data, true);
          $this->template->render();
     }

     public function delete($Event_id = NULL,$id = NULL)
     {
        
          $social_list = $this->Social_model->get_social_list($id);
          $this->data['social_list'] = $social_list[0];

          $social = $this->Social_model->delete_social($id);
          $this->session->set_flashdata('social_data', 'Deleted');
          redirect("Social_admin/index/" . $Event_id);
     }

}
