<?php
if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Public_messages_hub extends CI_Controller
{

	function __construct()
	{
    	$this->data['pagetitle'] = 'Attendee';
		$this->data['smalltitle'] = 'Attendee';
		$this->data['breadcrumb'] = 'Attendee';
		parent::__construct($this->data);
		ini_set('memory_limit','1024M');
		$this->load->library('formloader');
		$this->load->library('formloader1');
		$this->template->set_template('front_template');
		$this->load->model('Event_template_model');
		$this->load->model('Cms_model');
		$this->load->model('Event_model');
		$this->load->model('Setting_model');
		$this->load->model('Speaker_model');
		$this->load->model('Message_model');
		$this->load->model('Notes_admin_model');
		$this->load->model('Profile_model');
		$this->load->model('Agenda_model');
		$org=$this->Event_model->get_organizer_by_acc_name($this->uri->segment(2));
        $oid=$org[0]['Id'];
		if(empty($oid))
		{
			$org=$this->Event_model->get_organizer_by_acc_name($this->uri->segment(3));
        	$oid=$org[0]['Id'];
		}
		$hub_active=$this->Event_model->get_hub_active_by_organizer($oid); 
      	if($hub_active!='1')
      	{
      		redirect(base_url().'Forbidden/');
      	}
	}
	public function index($acc_name = NULL)
	{
		$acc['acc_name'] =  $acc_name;
        $this->session->set_userdata($acc);
		$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
		$hubdesing=$this->Event_model->get_hub_event($org_id);
		$view_chats1 = $this->Message_model->view_hangouts_public_msg_by_organizer($org_id);
		$this->data['view_chats1']=$view_chats1;
		$cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
		$this->data['hubdesing']=$hubdesing;
		$event_templates[0]['Logo_images']=$hubdesing[0]['hub_logo_images'];
		$event_templates[0]['attendees_active']=$hubdesing[0]['attendees_active'];
		$event_templates[0]['public_messages_active']=$hubdesing[0]['public_messages_active'];
		$event_templates[0]['Img_view']='0';
		$event_templates[0]['Event_name']=$hubdesing[0]['hub_name'];
		$event_templates[0]['Subdomain']=$hubevent[0]['Subdomain'];
		$event_templates[0]['menu_text_color']='#FFFFFF';
		$event_templates[0]['menu_background_color']='#6b6b6b';
		$event_templates[0]['menu_hover_background_color']='#b5b5b5';
		$event_templates[0]['Top_text_color']=$hubdesing[0]['hub_top_bar_text_color'];
		$event_templates[0]['Top_background_color']=$hubdesing[0]['hub_top_bar_backgroundcolor'];
		$event_templates[0]['huburl']=base_url().'hub/'.$acc_name;
		$this->data['event_templates']=$event_templates;
		$this->template->write_view('css', 'frontend_files/css', $this->data, true);
		$this->template->write_view('header', 'frontend_files/header', $this->data, true);
		$this->template->write_view('js', 'frontend_files/js', $this->data, true);
		$this->template->write_view('content', 'Public_messages_hub/chatspublic', $this->data, true);
		$this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
	}
	public function loadmore($acc_name = NULL,$start = NULL)
	{
		$acc['acc_name'] =  $acc_name;
        $this->session->set_userdata($acc);
		$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
		$hubdesing=$this->Event_model->get_hub_event($org_id);
		$cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
		$view_chats1 = $this->Message_model->view_hangouts_public_msg_by_organizer($org_id,$start);
		$this->data['hubdesing']=$hubdesing;
		$event_templates[0]['Logo_images']=$hubdesing[0]['hub_logo_images'];
		$event_templates[0]['attendees_active']=$hubdesing[0]['attendees_active'];
		$event_templates[0]['public_messages_active']=$hubdesing[0]['public_messages_active'];
		$event_templates[0]['Img_view']='0';
		$event_templates[0]['Event_name']=$hubdesing[0]['hub_name'];
		$event_templates[0]['Subdomain']=$hubevent[0]['Subdomain'];
		$event_templates[0]['menu_text_color']='#FFFFFF';
		$event_templates[0]['menu_background_color']='#6b6b6b';
		$event_templates[0]['menu_hover_background_color']='#b5b5b5';
		$event_templates[0]['Top_text_color']=$hubdesing[0]['hub_top_bar_text_color'];
		$event_templates[0]['Top_background_color']=$hubdesing[0]['hub_top_bar_backgroundcolor'];
		$event_templates[0]['huburl']=base_url().'hub/'.$acc_name;
		$this->data['event_templates']=$event_templates;
		foreach ($view_chats1 as $key => $value)
       {
            if (!empty($value['Id']))
            {
                echo "<div class='message_container'>";
                if ($value['Sender_id'] == $user[0]->Id)
                {
                    echo "<div class='msg_edit-view-box'>";
                    echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                    echo "</div>";
                    echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                    echo "Delete";
                    echo "</div>";
                    echo "</div>";
                }
                echo "<div class='msg_main_body'>";
                echo "<div class='message_img'>";
                if ($value['Sender_id'] == $user[0]->Id && $value['Senderlogo']!="")
                {
                    echo "<img src='" . base_url() . "/assets/user_files/" . $value['Senderlogo'] . "'>";
                }
                else if ($value['Receiver_id'] == $user[0]->Id && $value['Receiverlogo']!="")
                {
                    echo "<img src='" . base_url() . "/assets/user_files/" . $value['Receiverlogo'] . "'>";
                }
                else
                {
                    echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                }
                echo "</div>";
                echo "<div class='msg_fromname'>";
                $t=time().$key;
                echo '<a href="#" class="tooltip_data">';
                echo ucfirst($value['Sendername']);
                echo '</a>';
				echo "</div>";
				if (!empty($value['Receiver_id']))
				{
				  echo "<div class='msg_with'>";
				  echo "with";
				  echo "</div>";
				  echo "<div class='msg_toname'>";
				  $t=time().$key;
				  echo '<a href="#" class="tooltip_data">';
				  echo ucfirst($value['Recivername']);
				  echo '</a>';                                               
				  echo "</div>";
				}
				echo "</div>";
				echo "<div class='msg_date'>";
				echo $this->get_timeago(strtotime($value['Time']));
				echo "</div>";
				echo "<div class='msg_message'>";
				echo $value['Message'];
				echo "</div>";
				$img_data = json_decode($value['image']);
				foreach ($img_data as $kimg => $valimg)
				{
				  echo "<div class='msg_photo'>";
				  echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
				  echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
				  echo "</a>";
				  echo "</div>";
				}
				if ($type == '0')
				{
				  echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
				}
				else
				{
				  echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
				}
				echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
				<form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Photos_hub/upload_commentimag/".$acc_name."/". $value['Id']."'>
				<div class='comment_message_img'>";
				if ($user[0]->Logo!="")
				{
				  echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
				}
				else
				{
				  echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
				}
				echo "</div>";
				if ($type == '0')
				{
				  echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
				}
				else
				{
				  echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
				}   
				echo "<div class='photo_view_icon'>     
				<div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
				<input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
				<input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
				<ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
				</ul>
				<div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
				if ($type == '0')
				{
				  echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
				}
				else
				{
				  echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . "," . $type . ");' />";
				}
				echo "</form>
				<div class='comment_data' id='comment_conten" . $value['Id'] . "'>";
				if (!empty($view_chats1[$key]['comment']))
				{
				  	$view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
				  	$i = 0;
				  	$flag = false;
				  	foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
				  	{
				       if ($i > 3)
				       {
				            $classadded = 'comment_msg_hide';
				       }
				       else
				       {
				            $classadded = '';
				       }

				       echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
				       if ($cval['Logo'] != "")
				       {
				            echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
				       }
				       else
				       {
				            echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
				       }

				       echo "</div>
				            <div class='comment_wrapper'>        
				            <div class='comment_username'>
				                 " . ucfirst($cval['user_name']) . "
				            </div>
				            <div class='comment_text'>
				                 " . $cval['comment'] . "
				            </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

				       if ($cval['image'] != "")
				       {
				            $image_comment = json_decode($cval['image']);
				            echo "<div class='msg_photo'>";
				            echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
				            echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
				            echo "</a>";
				            echo "</div>";
				       }
				       if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
				       {
				            echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
				       }
				       echo "</div>";


				       if ($i > 3 && $flag == false)
				       {
				            $flag = true;
				            echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
				       }

				       $i++;
				  	}
				}
				echo "</div></div>";
				echo "</div>";
            }
       }
       exit;
	}
	function get_timeago($ptime = NULL)
	{
	    $estimate_time = time() - $ptime;
	    if ($estimate_time < 1)
	    {
	        return '1 second ago';
	    }
	    $condition = array(
		    12 * 30 * 24 * 60 * 60 => 'year',
		    30 * 24 * 60 * 60 => 'month',
		    24 * 60 * 60 => 'day',
		    60 * 60 => 'hour',
		    60 => 'minute',
		    1 => 'second'
	    );
      	foreach ($condition as $secs => $str)
      	{
           $d = $estimate_time / $secs;

           if ($d >= 1)
           {
                $r = round($d);
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
           }
	    }
	}
	public function chatspublic($acc_name = NULL)
	{ 
		$acc['acc_name'] =  $acc_name;
        $this->session->set_userdata($acc);
		$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
		$this->load->model('User_model');
		$user = $this->session->userdata('current_user');
		$cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
		$lid = $user[0]->Id;
		$userdata=$this->Event_model->get_user_by_hub_event_by_user_id($org_id,$lid);
		$dataevents = $this->Event_template_model->get_event_template_by_id_list($userdata[0]['Subdomain']);
		if ($this->input->post())
		{
		   $imgdataarray = $this->input->post('unpublished_photo');
		   if (!empty($imgdataarray))
		   {
		        $imag_photos = json_encode($imgdataarray);
		   }
		   else
		   {
		        $imag_photos = NULL;
		   }
		   if ($this->input->post('Receiver_id') != "")
		   {
		      $resiver = $this->input->post('Receiver_id');
		   }
		   else
		   {
		        $resiver = NULL;
		   }
		   $data1 = array(
	           'Message' => $this->input->post('Message'),
	           'Sender_id' => $lid,
	           'Receiver_id' => $resiver,
	           'Parent' => '0',
	           'Event_id' => $userdata[0]['eventid'],
	           'Time' => date("Y-m-d H:i:s"),
	           'image' => $imag_photos,
	           'ispublic' => $this->input->post('ispublic')
	        );
		   $current_date=date('Y/m/d');
		   foreach ($resiver as $key => $value) {
		        $this->Message_model->add_msg_hit($lid,$current_date,$value); 
		   }
		   $this->Message_model->send_grp_speaker_message($data1);
		   if ($this->input->post('ispublic') == '1')
		   {
		        $dataevents = $this->Event_template_model->get_event_template_by_id_list($userdata[0]['Subdomain']);
		        $view_chats1 = $this->Message_model->view_hangouts_public_msg_by_organizer($org_id);
		   }
		   else
		   {
		       if(!empty($resiver))
		       {
		           $strRec = @implode(',',$resiver);
		           if(!empty($strRec))
		           {
		               $arrUser = $this->User_model->getUserFromIds($strRec);
		               if(!empty($arrUser))
		               {
		                   foreach($arrUser as $intKey=>$strValue)
		                   {
		                       $strMsg = $this->input->post('Message');
		                       $strMsg .= "<br/>";
		                       $strMsg .= "Event Link : ".  base_url()."Events/".$userdata[0]['Subdomain'];
		                       $this->email->from($user[0]->Email, $user[0]->Firstname);
		                       $this->email->to($strValue['Email']);
		                       $this->email->subject("Private Message");
		                       $this->email->set_mailtype("html");
		                       $this->email->message($strMsg);    
		                       $this->email->send();
		                   }
		               }
		           }
		       }
		   }
		   $string = '';
		   $user = $this->session->userdata('current_user');
		   $lid = $user[0]->Id;
		   echo '<div style="padding-top: 20px;">
		   <div id="messages">';
		   if ($this->input->post('ispublic') == '1')
		   {
		        echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Public Posts <br/></h3>';
		   }
		   else
		   {
		        echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
		   }

		   foreach ($view_chats1 as $key => $value)
		   {
		        echo "<div class='message_container'>";

		        if ($value['Sender_id'] == $user[0]->Id)
		        {
		             echo "<div class='msg_edit-view-box'>";
		             echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
		             echo "</div>";
		             echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
		             echo "Delete";
		             echo "</div>";
		             echo "</div>";
		        }

		        echo "<div class='msg_main_body'>";
		        echo "<div class='message_img'>";
		        if ($value['Senderlogo'] != "")
		        {
		             echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
		        }
		        else
		        {
		             echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
		        }

		        echo "</div>";
		        echo "<div class='msg_fromname'>";
		        $t=time().$key;
		        echo '<a href="#" class="tooltip_data">';
		        echo ucfirst($value['Sendername']);
		        echo '</a>';
		        echo "</div>";
		        if (!empty($value['Receiver_id']))
		        {
		             echo "<div class='msg_with'>";
		             echo "with";
		             echo "</div>";
		             echo "<div class='msg_toname'>";
		             $t=time().$key;
		             echo '<a href="#" class="tooltip_data">';
		             echo ucfirst($value['Recivername']);
		             echo '</a>';                                               
		             echo "</div>";
		        }
		        echo "</div>";
		        echo "<div class='msg_date'>";
		        echo $this->get_timeago(strtotime($value['Time']));
		        echo "</div>";
		        echo "<div class='msg_message'>";
		        echo $value['Message'];
		        echo "</div>";

		        $img_data = json_decode($value['image']);
		        foreach ($img_data as $kimg => $valimg)
		        {
		             echo "<div class='msg_photo'>";
		             echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
		             echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
		             echo "</a>";
		             echo "</div>";
		        }
		        
		        if($this->input->post('ispublic') == '1')
		        {
		               echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
		        }
		        else
		        {
		              echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
		        }
		        
		        echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
		        <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Photos_hub/upload_commentimag/".$acc_name."/". $value['Id'] . "'>
		        <div class='comment_message_img'>";
		        if ($user[0]->Logo!="")
		        {
		             echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
		        }
		        else
		        {
		             echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
		        }
		        echo "</div>";

		        if($this->input->post('ispublic') == '1')
		        {
		               echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
		        }
		        else
		        {
		             echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
		        }
		        
		        echo "<div class='photo_view_icon'>     
		        <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
		        <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
		        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
		        <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
		        </ul>
		        <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
		           if($this->input->post('ispublic') == '1')
		           {
		                  echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
		           }
		           else
		           {
		                 echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",0);' />";
		           }
		                                 
		        echo "</form>
		        <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

		        if (!empty($view_chats1[$key]['comment']))
		        {
		             $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
		             $i = 0;
		             $flag = false;
		             foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
		             {
		                  if ($i > 3)
		                  {
		                       $classadded = 'comment_msg_hide';
		                  }
		                  else
		                  {
		                       $classadded = '';
		                  }

		                  echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
		                  if ($cval['Logo'] != "")
		                  {
		                       echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
		                  }
		                  else
		                  {
		                       echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
		                  }

		                  echo "</div>
		                  <div class='comment_wrapper'>        
		                  <div class='comment_username'>
		                       " . ucfirst($cval['user_name']) . "
		                  </div>
		                  <div class='comment_text'>
		                       " . $cval['comment'] . "
		                  </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

		                  if ($cval['image'] != "")
		                  {
		                       $image_comment = json_decode($cval['image']);
		                       echo "<div class='msg_photo'>";
		                       echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
		                       echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
		                       echo "</a>";
		                       echo "</div>";
		                  }
		                  if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
		                  {
		                       echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
		                  }
		                  echo "</div>";

		                  if ($i > 3 && $flag == false)
		                  {
		                       $flag = true;
		                       echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
		                  }

		                  $i++;
		             }
		        }
		        echo "</div></div>";
		        echo "</div>";
		   }
		   echo "</div></div>";
		   exit;
		}
	}
	public function commentaddpublic($acc_name = NULL)
	{
		$acc['acc_name'] =  $acc_name;
        $this->session->set_userdata($acc);
		$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
		$user = $this->session->userdata('current_user');
		$cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
		$lid = $user[0]->Id;
		$userdata=$this->Event_model->get_user_by_hub_event_by_user_id($org_id,$lid);
		if ($this->input->post())
		{
		   $imgdataarray = $this->input->post('unpublished_commentphoto');
		   if (!empty($imgdataarray))
		   {
		        $imag_photos = json_encode($imgdataarray);
		   }
		   else
		   {
		        $imag_photos = NULL;
		   }
		   $array_add['comment'] = trim($this->input->post('comment'));
		   $array_add['msg_id'] = $this->input->post('msg_id');
		   $array_add['image'] = $imag_photos;
		   $array_add['user_id'] = $lid;
		   $array_add['Time'] = date("Y-m-d H:i:s");
		   $msg_id=$this->input->post('msg_id');
		   $sender_id=$this->Message_model->get_senderid($msg_id);
		   $current_date=date('Y/m/d');
		   $this->Message_model->add_comment_hit($lid,$current_date,$sender_id); 
		   $dataevents = $this->Event_template_model->get_event_template_by_id_list($userdata[0]['Subdomain']);
		   $comment_id = $this->Message_model->add_comment($userdata[0]['eventid'],$array_add); 
		   $view_chats1 = $this->Message_model->view_hangouts_public_msg_by_organizer($org_id,$start,$this->input->post('msg_id'));
		   if(count($view_chats1) > 1)
           {
              for ($j=0; $j <=count($view_chats1); $j++) { 
                if($view_chats1[$j]['Id']==$this->input->post('msg_id'))
                {
                    $view_chats1[0]=$view_chats1[$j];
                    break;  
                }
              }
           }
		   if (!empty($view_chats1[0]['comment']))
		   {
		        $i = 0;
		        $flag = false;
		        $view_chats1[0]['comment'] = array_reverse($view_chats1[0]['comment']);
		        foreach ($view_chats1[0]['comment'] as $ckey => $cval)
		        {
		            if ($i > 3)
		            {
		                $classadded = 'comment_msg_hide';
		            }
		            else
		            {
		                $classadded = '';
		            }
		            echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
		            if ($cval['Logo'] != "")
		            {
		                echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
		            }
		            else
		            {
		                echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
		            }
		            echo "</div><div class='comment_wrapper'><div class='comment_username'>" . ucfirst($cval['user_name']) . "</div><div class='comment_text'>" . $cval['comment'] . "</div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";
		            if ($cval['image'] != "")
		            {
		                $image_comment = json_decode($cval['image']);
	                  	echo "<div class='msg_photo'>";
	                  	echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
	                  	echo "<img src='" . base_url() . "/assets/user_files/" . $image_comment[0] . "'>";
	                  	echo "</a>";
	                  	echo "</div>";
		            }
		            if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
		            {
		                echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",this)'>&nbsp;</button>";
		            }
		            echo "</div>";
		            if ($i > 3 && $flag == false)
		            {
		                $flag = true;
		                echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
		            }
		            $i++;
		        }
		   }
		}
		exit;
	}
	public function delete_message($acc_name = NULL,$id = NULL)
    {
    	$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
        echo "333";
        $chats = $this->Message_model->delete_message($id);
        exit;
    }
    public function delete_comment($acc_name = NULL,$id = NULL)
    {
    	$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
        $org_id=$org[0]['Id'];
        echo "333";
        $chats = $this->Message_model->delete_comment($id);
        exit;
    }
}
?>