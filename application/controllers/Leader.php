<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Leader extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Leader';
        $this->data['smalltitle'] = 'Leader';
        $this->data['breadcrumb'] = 'Leader';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->model('Agenda_model');
        $this->load->library('formloader1');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Notes_admin_model');
        $this->load->model('Message_model');
        // $this->load->model('Exibitor_model');
        $this->load->model('Profile_model');
        $user = $this->session->userdata('current_user');
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (!in_array('8', $menu_list))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $eventname = $this->Event_model->get_all_event_name();
        if (in_array($this->uri->segment(3) , $eventname))
        {
            if ($user != '')
            {
                $req_mod = $this->router->fetch_class();
                $menu_id = $this->Event_model->get_menu_id($req_mod);
                $current_date = date('Y/m/d');
                $logged_in_user_id = $user[0]->User_id;
                $this->Event_model->add_view_hit($logged_in_user_id, $current_date, $menu_id);
                $parameters = $this->uri->uri_to_assoc(1);
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                /*if(!empty($roledata))
                {
                $roleid = $roledata[0]->Role_id;
                $eventid = $event_templates[0]['Id'];
                $rolename = $roledata[0]->Name;
                $cnt = 0;
                $req_mod = ucfirst($this->router->fetch_class());
                // $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename, $eventid);
                $cnt = 1;
                }
                else
                {
                $cnt=0;
                }*/
                $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                if ($cnt == 1)
                {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                    $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type = $event_templates[0]['Event_type'];
                    if ($event_type == 3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain = $this->uri->segment(3);
                        $acc_name = $this->uri->segment(2);
                        echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
                    }
                }
            }
        }
        else
        {
            $Subdomain = $this->uri->segment(3);
            $flag = 1;
            echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
        }
        // echo $cnt;
    }
    public function index($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $this->data['sign_form_data'] = $res1;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        //          if ($user[0]->Name == 'Client')
        //          {
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        /*echo "<pre>";
        print_r($menu_list);die;*/
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Leader' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_list'] = $menu_list;
        $this->data['menu_id'] = $mid;
        //          }
        //          else
        //          {
        //               $menu_list = $this->Event_template_model->get_menu_list($roleid,$event_templates[0]['Id']);
        //               $this->data['menu_list'] = $menu_list;
        //          }
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        /* $photo = $this->Event_template_model->get_photos($Subdomain);
        $this->data['photo'] = $photo; */
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $this->data['Subdomain'] = $Subdomain;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            // redirect('Leader/demo','refresh');
            echo '<script>window.location.href="' . base_url() . 'Leader/' . $acc_name . '/' . $Subdomain . '"</script>';
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Leader/index', $this->data, true);
                $this->template->write_view('footer', 'Leader/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Leader/index', $this->data, true);
                $this->template->write_view('footer', 'Leader/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $this->template->write_view('content', 'Leader/index', $this->data, true);
            $this->template->write_view('footer', 'Leader/footer', $this->data, true);
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
}