<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class QA extends CI_Controller
{
  function __construct()
  {
    $this->data['pagetitle'] = 'Q&A';
    $this->data['smalltitle'] = 'Q&A';
    $this->data['breadcrumb'] = 'Q&A';
    parent::__construct($this->data);
    $this->load->library('formloader');
    $this->load->library('formloader1');
    $this->load->model('Agenda_model');
    $this->template->set_template('front_template');
    $this->load->model('Event_template_model');
    $this->load->model('Cms_model');
    $this->load->model('Event_model');
    $this->load->model('Setting_model');
    $this->load->model('Speaker_model');
    $this->load->model('Notes_admin_model');
    $this->load->model('Profile_model');
    $this->load->model('Message_model');
    $this->load->model('Qa_model');
    $user = $this->session->userdata('current_user');
    $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
    $eventid = $event_val[0]['Id'];
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);
    if(!in_array('50',$menu_list))
    {
      redirect('forbidden');
    }
    $eventname=$this->Event_model->get_all_event_name();
    if(in_array($this->uri->segment(3),$eventname))
    {
      if ($user != '')
      {
        $logged_in_user_id=$user[0]->Id;
        $parameters = $this->uri->uri_to_assoc(1);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
        if(!empty($roledata))
        {
          $roleid = $roledata[0]->Role_id;
          $eventid = $event_templates[0]['Id'];
          $rolename = $roledata[0]->Name;
          $cnt = 0;
          $req_mod = ucfirst($this->router->fetch_class());
          $cnt = 1;
        }
        else
        {
          $cnt=0;
        }

        if ($cnt == 1)
        {
          $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
          $this->data['notes_list'] = $notes_list;
        }
        else
        {
          $event_type=$event_templates[0]['Event_type'];
          if($event_type==3)
          {
            $this->session->unset_userdata('current_user');
            $this->session->unset_userdata('invalid_cred');
            $this->session->sess_destroy();
          }
          else
          {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain=$this->uri->segment(3);
            $acc_name=$this->uri->segment(2);
            echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
          }
        }
      }
    }
    else
    {
      $parameters = $this->uri->uri_to_assoc(1);
      redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
    }
  }

  public function index($acc_name,$Subdomain = NULL,$intFormId=NULL)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $user = $this->session->userdata('current_user');
    if(!empty($user[0]->Id))
    {
      $current_date=date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id,$current_date,'50',$event_templates[0]['Id']);
    }

    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],null);
    $this->data['qa_session']=$qa_session;

    $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting']=$notificationsetting;

    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $this->data['fb_login_data'] = $fb_login_data;
    $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);

    $this->data['sign_form_data']=$res1;

    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;

    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;

    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;

    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;


    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;

    $this->data['Subdomain'] = $Subdomain;

    for($i=0;$i<count($menu_list);$i++)
    {
      if('QA'==$menu_list[$i]['pagetitle'])
      {
        $mid=$menu_list[$i]['id'];
      }
    }
    $this->data['menu_id']=$mid;
    $eid=$event_templates[0]['Id'];
    $res=$this->Agenda_model->getforms($eid,$mid);
    $this->data['form_data']=$res;

    $array_temp_past = $this->input->post();
    if(!empty($array_temp_past))
    {
      $aj=json_encode($this->input->post());
      $formdata=array('f_id' =>$intFormId,
      'm_id'=>$mid,
      'user_id'=>$user[0]->Id,
      'event_id'=>$eid,
      'json_submit_data'=>$aj
      );
      $this->Agenda_model->formsinsert($formdata);
      echo '<script>window.location.href="'.base_url().'QA/'.$acc_name.'/'.$Subdomain.'"</script>';
      exit;
    }

    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] =  $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('50',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render(); 
  }
  public function view($acc_name,$Subdomain,$sid)
  {
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;

    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],$sid);
    $this->data['qa_session']=$qa_session;

    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $this->data['Subdomain'] = $Subdomain;
    $this->data['sid']=$sid;
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/viewsession', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/viewsession', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] =  $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('50',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/viewsession', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/viewsession', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }
  public function send_message($acc_name,$Subdomain,$sid)
  {
    $user = $this->session->userdata('current_user');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],$sid);
    $data['qa_session_id']=$sid;
    if($this->input->post('anonymous_user')!='1' && !empty($user[0]->Id))
    {
      $sender_id=$user[0]->Id;
    }
    else
    {
      $sender_id=NULL;
    }
    $modertor_id=$this->Event_model->get_user_all_moderator($event_templates[0]['Id'],$qa_session[0]['Moderator_speaker_id']);
    if(count($modertor_id) > 0)
    {
      $mids=array_column_1($modertor_id,'moderator_id');
    }
    else
    {
      $mids=array($qa_session[0]['Moderator_speaker_id']);
    }
    $resiver=array_unique($mids);
    foreach ($resiver as $key => $value) 
    {
      $data1 = array(
        'Message' => $this->input->post('Message'),
        'Sender_id' => $sender_id,
        'Receiver_id' => $value,
        'Parent' => '0',
        'Event_id' => $event_templates[0]['Id'],
        'Time' => date("Y-m-d H:i:s"),
        'image' => NULL,
        'org_msg_receiver_id'=>$qa_session[0]['Moderator_speaker_id'],
        'ispublic' => '0',
        'qasession_id' => $sid
      );
      $this->Message_model->send_speaker_message($data1);
    }
    echo "success";die;
  }
}