<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C404 extends CI_Controller {

	public function index()
	{	
		$url = explode('/', $this->uri->uri_string);

		if($this->router->directory=='')
			$url[0] = ucfirst($url[0]);
		else
			$url[1] = ucfirst($url[1]);

		$url = implode('/', $url);
		
		
		if($this->input->server('REQUEST_METHOD') == 'POST')
		{
			echo '<form id="myForm" action="'.base_url().$url.'" method="post">';

		    foreach ($_POST as $a => $b) {
		        echo '<input type="hidden" name="'.htmlentities($a).'" value="'.htmlentities($b).'">';
		    }

			echo '</form>
			<script type="text/javascript">
			    document.getElementById("myForm").submit();
			</script>';
		}
		else
		{
			redirect(base_url().$url);
		}


		echo $url;exit;

	}

}