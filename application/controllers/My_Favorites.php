<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class My_Favorites extends CI_Controller

{
	function __construct()
	{
		$this->data['pagetitle'] = 'My Favorites';
		$this->data['smalltitle'] = 'My Favorites';
		$this->data['breadcrumb'] = 'My Favorites';
		parent::__construct($this->data);
		$this->load->library('formloader');
		$this->load->library('formloader1');
		$this->template->set_template('front_template');
		$this->load->model('Event_template_model');
		$this->load->model('Cms_model');
		$this->load->model('Event_model');
		$this->load->model('Setting_model');
		$this->load->model('Speaker_model');
		$this->load->model('Message_model');
		$this->load->model('Notes_admin_model');
		$this->load->model('Profile_model');
		$this->load->model('Agenda_model');
		$user = $this->session->userdata('current_user');
		$event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
		$eventid = $event_val[0]['Id'];
		$event = $this->Event_model->get_module_event($eventid);
		$menu_list = explode(',', $event[0]['checkbox_values']);
		if (!in_array('49', $menu_list))
		{
			echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
		}
		$eventname = $this->Event_model->get_all_event_name();
		if (in_array($this->uri->segment(3) , $eventname))
		{
			if ($user != '')
			{
				$parameters = $this->uri->uri_to_assoc(1);
				$event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
				$roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
				$logged_in_user_id = $user[0]->Id;
				$cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
				if ($cnt == 1)
				{
					$notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
					$this->data['notes_list'] = $notes_list;
				}
				else
				{
					$event_type = $event_templates[0]['Event_type'];
					if ($event_type == 3)
					{
						$this->session->unset_userdata('current_user');
						$this->session->unset_userdata('invalid_cred');
						$this->session->sess_destroy();
					}
					else
					{
						$parameters = $this->uri->uri_to_assoc(1);
						$Subdomain = $this->uri->segment(3);
						$acc_name = $this->uri->segment(2);
						echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
					}
				}
			}
		}
		else
		{
			$parameters = $this->uri->uri_to_assoc(1);
			$Subdomain = $this->uri->segment(3);
			$flag = 1;
			echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
		}
	}
	public function index($acc_name= NULL, $Subdomain= NULL, $intFormId = NULL)

	{
		$event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
		$this->data['event_templates'] = $event_templates;
		$user = $this->session->userdata('current_user');
		if (!empty($user[0]->Id))
		{
			$current_date = date('Y/m/d');
			$this->Event_model->add_view_hit($user[0]->Id, $current_date, '49', $event_templates[0]['Id']);
		}
		$my_favorites = $this->Event_template_model->get_all_favorites_contect_by_event_user($event_templates[0]['Id'], $user[0]->Id);
		$this->data['my_favorites'] = $my_favorites;
		$my_favorites_user = $this->Event_template_model->get_all_favorites_user_with_type_by_event_user($event_templates[0]['Id'], $user[0]->Id);
		$this->data['my_favorites_user'] = $my_favorites_user;
		$advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
		$this->data['advertisement_images'] = $advertisement_images;
		$menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
		$this->data['menu'] = $menu;
		$notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
		$this->data['notify_msg'] = $notifiy_msg;
		$menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
		for ($i = 0; $i < count($menu_list); $i++)
		{
			if ('My_Favorites' == $menu_list[$i]['pagetitle'])
			{
				$mid = $menu_list[$i]['id'];
			}
		}
		$this->data['menu_id'] = $mid;
		$this->data['menu_list'] = $menu_list;
		$eid = $event_templates[0]['Id'];
		$res = $this->Agenda_model->getforms($eid, $mid);
		$this->data['form_data'] = $res;
		$array_temp_past = $this->input->post();
		if (!empty($array_temp_past))
		{
			$aj = json_encode($this->input->post());
			$formdata = array(
				'f_id' => $intFormId,
				'm_id' => $mid,
				'user_id' => $user[0]->Id,
				'event_id' => $eid,
				'json_submit_data' => $aj
			);
			$this->Agenda_model->formsinsert($formdata);
			redirect(base_url() . 'My_Favorites/' . $acc_name . '/' . $Subdomain);
			// echo '<script>window.location.href="'.base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'"</script>';
			exit;
		}
		$cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
		$this->data['cms_menu'] = $cmsmenu;
		$notes_list = $this->Event_template_model->get_notes($Subdomain);
		$this->data['notes_list'] = $notes_list;
		$this->data['Subdomain'] = $Subdomain;
		$user = $this->session->userdata('current_user');
		$this->template->write_view('css', 'frontend_files/css', $this->data, true);
		$this->template->write_view('header', 'frontend_files/header', $this->data, true);
		$this->template->write_view('js', 'frontend_files/js', $this->data, true);
		if ($event_templates[0]['Event_type'] == '1')
		{
			if (empty($user))
			{
				$this->template->write_view('content', 'registration/index', $this->data, true);
			}
			else
			{
				$this->template->write_view('content', 'my_favorites/index', $this->data, true);
				$this->template->write_view('footer', 'my_favorites/footer', $this->data, true);
			}
		}
		elseif ($event_templates[0]['Event_type'] == '2')
		{
			if (empty($user))
			{
				$this->template->write_view('content', 'registration/index', $this->data, true);
			}
			else
			{
				$this->template->write_view('content', 'my_favorites/index', $this->data, true);
				$this->template->write_view('footer', 'my_favorites/footer', $this->data, true);
			}
		}
		elseif ($event_templates[0]['Event_type'] == '3')
		{
			$this->session->unset_userdata('acc_name');
			$acc['acc_name'] = $acc_name;
			$this->session->set_userdata($acc);
			$isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('49', $event_templates[0]['Id']);
			if ($isforcelogin['is_force_login'] == '1' && empty($user))
			{
				$this->template->write_view('content', 'registration/force_user_login', $this->data, true);
			}
			else
			{
				$this->template->write_view('content', 'my_favorites/index', $this->data, true);
				$this->template->write_view('footer', 'my_favorites/footer', $this->data, true);
			}
		}
		else
		{
			if (empty($user))
			{
				$this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
			}
			else
			{
				$this->template->write_view('content', 'my_favorites/index', $this->data, true);
				$this->template->write_view('footer', 'my_favorites/footer', $this->data, true);
			}
		}
		$this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
		$this->template->render();
	}
	public function save_myfavorites($acc_name= NULL, $Subdomain= NULL, $uid= NULL)

	{
		$user = $this->session->userdata('current_user');
		$event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
		if (!empty($user[0]->Id))
		{
			$mf_data['event_id'] = $event_templates[0]['Id'];
			$mf_data['user_id'] = $user[0]->Id;
			$mf_data['module_id'] = $this->input->post('module_id');
			$mf_data['module_type'] = $this->input->post('module_type');
			$process = $this->Event_template_model->myfavorites_save($mf_data);
			echo "success###" . $process;
			die;
		}
		else
		{
			echo "error###";
			die;
		}
	}
}
?>