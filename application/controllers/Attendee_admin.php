<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Attendee_admin extends FrontendController
{

    public function __construct()
    {
        $this->data['pagetitle'] = 'Your Attendees';
        $this->data['smalltitle'] = 'Upload or individually add your attendee list on this page and send invite emails.';
        $this->data['page_edit_title'] = 'edit';
        $this->data['breadcrumb'] = 'Your Attendees';
        parent::__construct($this->data);
        $this->load->model('Event_model');
        ini_set('memory_limit', "1024M");
        ini_set('auto_detect_line_endings', true);
        $this->load->model('Formbuilder_model');
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Event_template_model');
        $this->load->library('upload');
        $this->load->library('email');
        $user = $this->session->userdata('current_user');
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column_1($user_events, 'Event_id'));
        $eventid = $this->uri->segment(3);
        if (!in_array($eventid, $user_events)) {
            redirect('Forbidden');
        }
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('2', $module)) {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);

        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata)) {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == "Your Attendees") {
                $title = "Attendee";
            }

            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        } else {
            $cnt = 0;
        }

        if (!empty($user[1]['event_id_selected'])) {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1) {
            $this->load->model('Attendee_model');
            $this->load->model('Agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Profile_model');
            $this->load->library('session');
            $this->load->model('Event_template_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        } else {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
    }

    public function index($id, $limit = 10, $start = 0)
    {
        /*if($id == '1012' || $id == '634')
        {
        redirect(base_url().'attendee_admin/attendee_list/'.$id);
        exit;
        }*/

        $keyword = '';
        if ($this->input->post('keyword')) {
            $keyword = $this->input->post('keyword');
        }
        $this->data['keyword'] = $keyword;
        $this->load->library('pagination');
        $limit_per_page = $limit;
        $this->data['limit_per_page'] = $limit_per_page;
        /*if($limit == 10)
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        else*/
        $start_index = $start;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $group_name = $this->Attendee_model->get_attendee_group_list($user[0]->Organisor_id);
        // echo "<pre>"; print_r($group_name); exit();
        $this->data['attendee_group'] = $group_name;
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();

        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User') {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;

            // $attendees = $this->Attendee_model->get_attendee_list();
            // $this->data['Attendees'] = $attendees;
            $attendees = $this->Attendee_model->get_attendee_list_pagination($id, $limit_per_page, $start_index, $keyword);
            $this->data['Attendees'] = $attendees;
            //$attendees = $this->Attendee_model->get_attendee_list_pagination(NULL,$limit_per_page,$start_index);
            //$this->data['Attendees'] = $attendees;
        }

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $meeting_locations = $this->Event_template_model->get_all_meeting_locations_by_event($id);
        $this->data['meeting_locations'] = $meeting_locations;
        /*$this->data['registration_data']=$this->Attendee_model->get_registration_screen_data($id,null);*/
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $this->data['keysdata'] = $this->Attendee_model->get_all_custom_modules($id);

        if ($id != '1362') {
            $Meeting_Attendees = $this->Attendee_model->get_attendee_list($id);
        }

        $this->data['Meeting_Attendees'] = $Meeting_Attendees;

        $attendees = $this->Attendee_model->get_attendee_list_pagination($id, $limit_per_page, $start_index, $keyword);
        $this->data['Attendees'] = $attendees;

        if ($id == '1114') {
            $this->data['registerd_attendee'] = $this->Attendee_model->getRegisterdUserIds($id);
        }

        $total_records = $this->Attendee_model->get_attendee_count($id, $keyword);
        $config['base_url'] = base_url() . 'Attendee_admin/index/' . $id . '/' . $limit;
        $config['total_rows'] = $total_records;
        $this->data['total_rows'] = $total_records;
        $this->data['start'] = $start;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 5;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();

        $this->data['moderator'] = $this->Attendee_model->get_moderator_list_by_event($id);
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->data['skipcsvdata'] = $csvdata;
        /*$badges_data=$this->Attendee_model->get_badges_values_by_event($id);
        $this->data['badges_data']=$badges_data;*/
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '1');
        $this->data['csvdata'] = $csvdata;
        $view_data = $this->Attendee_model->get_all_view_by_event($id);
        $this->data['view_data'] = $view_data;
        $authorized_user = $this->Attendee_model->get_authorized_user_data($id);
        $this->data['auth_user_list'] = $authorized_user;
        $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
        $this->data['agenda_invited_list'] = $agenda_invited_list;
        $arrSingupForm = $this->Formbuilder_model->get_singup_form_list($id);
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        if (!empty($arrSingupForm)) {
            $this->data['arrSingupForm'] = $arrSingupForm;
        }
        if ($this->input->post('invites')) {
            $string = "";

            foreach ($this->input->post('invites') as $key => $value) {
                $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
                foreach ($agenda_invited_list as $key1 => $values1) {
                    $arraydata = array();
                    if ($values1['Emailid'] == $value) {
                        $arraydata = $values1;
                        break;
                    }
                }
                $msg = $this->input->post('msg');
                $sent_from = $this->input->post('sent_from');
                if ($this->input->post('send_link') == '1') {
                    $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '';
                    $tes = urlencode(base64_encode($value));
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                    $msg1 .= ' <br/><br/> ' . 'Invitation Link:  ' . $event_link . '/' . $tes . '';
                } else {
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                }
                $msg1 .= "<br/><br/>" . $msg;
                $sub = $this->input->post('subject1');
                $sendername;
                if ($this->input->post('sent_name') != "") {
                    $sendername = $this->input->post('sent_name');
                } else {
                    $sendername = 'Event Management';
                }
                $sent_from = 'invite@allintheloop.com';
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($sent_from, $sendername);
                $this->email->to($value);
                $this->email->subject($sub);
                $this->email->message($msg1);
                $this->email->send();

            }
            $this->session->set_flashdata('attendee_data', 'Invitation Resend');
            redirect('Attendee_admin/index/' . $id . '/1');
        }

        $this->data['Event_id'] = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;

        $menudata = $this->Event_model->geteventmenu($id, 2);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);

        $this->data['get_booked_meetings_data'] = $this->Attendee_model->get_booked_meetings($id);
        $where['status'] = '1';
        if ($event[0]['auto_allocate_meeting_location']) {
            $this->data['pending'] = $this->Attendee_model->get_booked_meetings($id, $where);
        }

        $meeting_dates = file_get_contents(base_url() . 'native_single_fcm/attendee/getRequestMeetingDate?event_id=' . $id . '&user_id=' . $user[0]->Id);
        $this->data['meeting_dates'] = json_decode($meeting_dates, true)['meeting_dates'];

        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->data['categorie_data'] = $this->Attendee_model->get_attendee_categories_by_event($id, null);
        $this->data['attendee_categories'] = $this->Attendee_model->get_attendee_categories_by_event($id);
        $this->data['filters'] = $this->Attendee_model->getFilters($id);
        // ld($this->data['filters']);
        //$this->data['H_images'] = $menudata[0]->H_images;
        //$this->data['B_images'] = $menudata[0]->B_images;
        //$this->data['F_images'] = $menudata[0]->F_images;

        /* $meeting_dates = file_get_contents(base_url().'native_single_fcm/attendee/ getRequestMeetingDate?event_id='.$id.'&user_id='.$user[0]->Id);
        $meeting_dates = json_decode($meeting_dates,true);
        $this->data['meeting_dates'] = $meeting_dates['meeting_dates'];*/
        $news_event = $this->Event_model->getNewsMasterEvent();
        $is_news_event = $news_event[0]['Id'] == $id ? '1' : '0';
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        if ($is_news_event) {
            $this->template->write_view('content', 'attendee_admin/index_news', $this->data, true);
        } else {
            $this->template->write_view('content', 'attendee_admin/index', $this->data, true);
        }

        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        if (!empty($arrSingupForm)) {
            $this->template->write_view('js', 'admin/edit_js', $this->data, true);
        } else {
            $this->template->write_view('js', 'admin/add_js', $this->data, true);
        }
        $this->template->render();
    }

    public function assign_location()
    {
        $eventid = $this->uri->segment(3);
        $id = $_POST['id'];
        $location = $_POST['locmodel_meeting_location'];
        if (!empty($id) && !empty($location)) {
            $is_update = $this->Attendee_model->assign_location($id, $location);
            if ($is_update) {

                $this->session->set_flashdata('location_assigned', 'Location Assign Successfully');
            } else {
                $this->session->set_flashdata('location_not_assigned', 'Location Not Assign some error was Occured');
            }
        }

        redirect('Attendee_admin/index/' . $eventid . '#allocate_location');
    }

    public function assign_agenda_category($eventid)
    {
        /*error_reporting(E_ALL);
        ini_set('display_errors', '1');*/

        $view_id = $this->input->post('attendee_view_id');
        $acid = $this->input->post('agenda_category_id');
        $attendees_id = $this->input->post('attendees_id');
        $user_group = $this->input->post('user_group');
        $invite_attendees_id = $this->input->post('invite_attendees_id');
        $temp_attendees_id = $this->input->post('temp_attendees_id');
        $attendee_categories = $this->input->post('attendee_categories');
        if (!empty($acid)) {
            foreach ($attendees_id as $key => $value) {
                $relation['attendee_id'] = $value;
                $relation['agenda_category_id '] = $acid;
                $relation['event_id'] = $eventid;
                $this->Attendee_model->add_agenda_relation($relation);
            }
            if (count($invite_attendees_id) > 0) {
                $this->Attendee_model->update_assign_agenda_invite_attendee($invite_attendees_id, $eventid, $acid);
            }
            if (count($temp_attendees_id) > 0) {
                $this->Attendee_model->update_assign_agenda_temp_attendee($temp_attendees_id, $eventid, $acid);
            }
        }
        if (!empty($view_id)) {
            foreach ($attendees_id as $key => $value) {
                $view_data['Attendee_id'] = $value;
                $view_data['views_id'] = $view_id;
                $this->Attendee_model->assign_view_id($view_data, $eventid);
            }
        }
        if (!empty($user_group)) {
            foreach ($attendees_id as $key => $value) {
                $insert_data['Attendee_id'] = $value;
                $insert_data['group_id'] = $user_group;
                $this->Attendee_model->assign_group($insert_data, $eventid);
            }
        }
        if (!empty($attendee_categories)) {
            foreach ($attendees_id as $key => $value) {
                $insert_data['user_id'] = $value;
                $insert_data['keyword'] = $attendee_categories;
                $this->Attendee_model->assign_attendee_categories($insert_data, $eventid);
            }
        }
        $this->session->set_flashdata('attendee_agenda_assign_data', 'Agenda Assign Successfully.');
        echo "success###" . base_url() . 'Attendee_admin/index/' . $eventid;
    }
    public function getJsonData($intEventId = null)
    {
        $arrForm = $this->Formbuilder_model->get_singup_form_list($intEventId);
        echo $arrForm[0]['json_data'];
    }
    public function delete_form($event_id)
    {
        $this->Attendee_model->delete_signup_form($event_id);
        $this->session->set_flashdata('formbuild_data', ' Deleted');
        redirect('Attendee_admin/index/' . $event_id);
    }
    public function edit_form($intEventId = null)
    {
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;

        $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
        $this->data['menu_toal_data'] = $menu_toal_data;

        $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
        $this->data['cms_list'] = $cms_list;

        $event = $this->Event_model->get_admin_event($intEventId);
        $this->data['event'] = $event[0];

        $arrForm = $this->Formbuilder_model->get_form_list($intEventId);
        $this->data['arrForm'] = $arrForm;

        if ($this->data['page_edit_title'] == 'edit') {
            if ($intEventId == null || $intEventId == '') {
                redirect('Event');
            }
            if ($this->input->post()) {
                $arrUpdate = array();
                $arrUpdate['event_id'] = $intEventId;
                $arrUpdate['id'] = $intFormId;
                $arrUpdate['json_data'] = $_POST['formData'];
                $advertising_id = $this->Formbuilder_model->update_form($arrUpdate);
                $this->session->set_flashdata('formbuild_data', 'Updated');
                redirect("Formbuilder/index/" . $intEventId);
            }
            $this->template->write_view('css', 'formbuilder/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'formbuilder/edit', $this->data, true);

            if ($this->data['user']->Role_name == 'User') {
                $total_permission = $this->advertising_model->get_permission_list();
                $this->data['total_permission'] = $total_permission;

                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            } else {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }

            $this->template->write_view('js', 'formbuilder/edit_js', $this->data, true);
            $this->template->render();
        }
    }

    public function singup($id)
    {
        $arrInsert['event_id'] = $id;
        $arrInsert['json_data'] = $_POST['formData'];
        $advertising_id = $this->Formbuilder_model->add_signup_form($arrInsert, $id);
        $this->session->set_flashdata('formbuild_data', 'Added');
    }
    public function edit($id = '0')
    {
        $this->data['attendees'] = $this->Attendee_model->get_attendee_list($id);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'admin/add_js', $this->data, true);
        $this->template->render();
    }
    public function add($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;

        $this->db->select('Subdomain,Event_type');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $this->data['email_template'] = $this->Setting_model->email_template($id);

        $res = $query->result_array();
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        if ($this->input->post()) {
            $string = " ";
            if ($this->input->post()) {
                $attandedata = $this->Attendee_model->get_invited_attendee_bymail($this->input->post('email_address'), $id);
                $attandeuser = $this->Attendee_model->get_user_bymail($this->input->post('email_address'), $id);
                if (count($attandedata) <= 0 && count($attandeuser) <= 0) {
                    $sent_from = $this->input->post('sent_from');
                    $msg1 = "Hello " . $this->input->post('first_name') . " " . $this->input->post('last_name') . '<br><br>';
                    $event_type = $res[0]['Event_type'];
                    $length = 6;
                    $chars = '1234567890';
                    $chars_length = (strlen($chars) - 1);
                    $string = $chars{rand(0, $chars_length)};
                    for ($i = 1; $i < $length; $i = strlen($string)) {
                        $r = $chars{rand(0, $chars_length)};
                        if ($r != $string{$i - 1}) {
                            $string .= $r;
                        }

                    }
                    $tes1 = urlencode(base64_encode($string));
                    if ($this->input->post('send_link') == '1') {
                        if ($event_type == '1') {
                            $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '/passwordset';
                            $tes = urlencode(base64_encode($this->input->post('email_address')));
                            $msg1 .= 'Registration Link: Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">click here</a> to set your password and access the app.<br><br>';
                        } else {
                            $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '';
                            $tes = urlencode(base64_encode($this->input->post('email_address')));
                            $msg1 .= 'Registration Link: Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">Click here</a> to access the app.<br/><br/>';
                        }
                    }
                    if ($this->input->post('send_link') == '0') {
                        $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '/attendee_registration_screen';

                        $msg1 .= 'Registration Link: Please <a href="' . $event_link . '">Click here</a> to Register in the app.<br/><br/>';
                    }
                    $msg1 .= $this->input->post('msg');
                    $sub = $this->input->post('subject1');
                    if ($sub == '') {
                        $sub = $email_template[0]['Subject'];
                    }
                    $sendername;
                    if ($this->input->post('sent_name') != "") {
                        $sendername = $this->input->post('sent_name');
                    } else {
                        $sendername = 'Event Management';
                    }
                    $sent_from = 'invite@allintheloop.com';
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_port'] = '25';
                    $config['smtp_user'] = 'invite@allintheloop.com';
                    $config['smtp_pass'] = 'xHi$&h9M)x9m';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from($sent_from, $sendername);
                    $this->email->to($this->input->post('email_address'));
                    $this->email->subject($sub);
                    $this->email->message($msg1);
                    $this->email->send();
                    if ($this->input->post('send_link') != '0') {
                        if ($this->input->post('send_link') == '1') {
                            $this->Attendee_model->addinvite_attendee($this->input->post('email_address'), $id, $this->input->post('first_name'), $this->input->post('last_name'), "", "", $string, $this->input->post('session_id'));
                        } else {
                            $cs['Emailid'] = trim($this->input->post('email_address'));
                            $cs['firstname'] = $this->input->post('first_name');
                            $cs['lastname'] = $this->input->post('last_name');
                            $cs['Event_id'] = $id;
                            $cs['skip_to_invite'] = '1';
                            $cs['agenda_id'] = $this->input->post('session_id');
                            $this->Attendee_model->addinvite_attendee_in_temp($cs);
                        }
                    }
                    $this->session->set_flashdata('attendee_data', 'Send Invitation');
                }
            }
            redirect('Attendee_admin/index/' . $id);
        }

        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        if (count($csvdata) > 0) {
            $this->data['csvdata'] = $csvdata;
            $this->data['keysdata'] = $this->Attendee_model->get_all_custom_modules($id);
            $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
            $this->data['category_list'] = $category_list;
            $this->template->write_view('content', 'attendee_admin/agenda_add', $this->data, true);
        } else {
            $this->template->write_view('content', 'attendee_admin/add', $this->data, true);
        }
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }

    public function getEmailTemplate($event_id)
    {
        $email_template_id = $this->input->post('Slug');
        $template = $this->Setting_model->front_email_template($email_template_id, $event_id);
        echo $template[0]['Content'];

    }
    public function delete($Event_id, $id)
    {
        $attendee = $this->Attendee_model->delete_attendee($id, $Event_id);
        $this->session->set_flashdata('attendee_data', 'Deleted');
        //redirect('Attendee_admin/index/' . $Event_id);
    }

    public function delete_mass_upload($event_id)
    {
        $ids = $this->input->post('attendees_id');
        $attendee = $this->Attendee_model->delete_mass_attendee($ids, $event_id);
        $this->session->set_flashdata('attendee_data', 'Deleted');
    }

    public function download_csv()
    {
        $this->load->helper('download');
        $filename = "attendee.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Emailaddress";
        $header[] = "firstname";
        $header[] = "lastname";
        $header[] = "Company";
        $header[] = "Title";

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);

        $data['Emailaddress'] = "attendee@allintheloop.com";
        $data['firstname'] = "Lawrence";
        $data['lastname'] = "Gill";
        $data['Company'] = "AITL";
        $data['Title'] = "Business Development Manager";
        fputcsv($fp, $data);
    }
    public function add_hub_attendees($eid)
    {
        $user = $this->session->userdata('current_user');
        $group_member = $this->Attendee_model->get_all_group_member_by_group_id($this->input->post('attendee_group_id'));
        foreach ($group_member as $key => $value) {
            $count_attendee = $this->Event_model->get_all_attendee_count($eid);
            $org_id = $this->Event_model->get_org_id_by_event($eid);
            $org_email = $this->Event_model->getOrgEmail($org_id);
            $iseventfreetrial = $this->Event_model->check_event_free_trial_account_by_org_email($org_email);
            if (count($iseventfreetrial) <= 0 || $count_attendee < 3) {
                $this->Attendee_model->add_hub_attendees_in_event($eid, $user[0]->Organisor_id, $value['user_id']);
            } else {
                $this->session->set_flashdata('org_limit_attendee', 'You can only add up to 3 Attendee Profiles until you Launch Your App.');
                redirect('Attendee/index/' . $eid);
            }
        }
        $this->session->set_flashdata('attendee_data', 'Add');
        redirect('Attendee_admin/index/' . $eid);
    }
    public function save_attendee_notes($eid)
    {
        $data['User_id'] = $this->input->post('User_id');
        $data['Heading'] = $this->input->post('Heading');
        $data['Description'] = $this->input->post('Description');
        $this->Attendee_model->save_notes_in_attendee($eid, $data);
        $this->session->set_flashdata('attendee_data', 'Notes Save');
        redirect('Attendee_admin/index/' . $eid);
    }
    public function getnotesdata($eid)
    {
        $note = $this->Attendee_model->get_note_data_note_id($this->input->post('notes_id'), $this->input->post('User_id'));
        echo json_encode($note);die;
    }
    public function savecsvdata($id)
    {
        session_start();
        $customkey = $this->Attendee_model->get_all_custom_modules($id);
        if (!empty($_FILES['csv']['tmp_name'])) {
            $file = $_FILES['csv']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            while (($datacsv[] = fgetcsv($handle, 1000, ",")) !== false) {}
            $keys = array_shift($datacsv);
            foreach ($datacsv as $key => $value) {
                $csv[] = array_combine($keys, $value);
            }
            $nomatch = array();
            $csvtest = array_filter($csv);
            $this->data['csvdatatest'] = $csvtest;
            $_SESSION['svdatatest'] = $csvtest;
            $this->data['keysdata'] = $keys;
            foreach ($csv as $key => $value) {
                $keysarr = array_keys($value);
                $fixedc = array('Emailaddress', 'firstname', 'lastname', 'Company', 'Title');
                for ($i = 0; $i < count($keysarr); $i++) {
                    if (!in_array($keysarr[$i], $fixedc)) {
                        if (!in_array($keysarr[$i], array_column_1($customkey, 'column_name'))) {
                            $kk = $keysarr[$i];
                            if (!in_array($kk, $nomatch)) {
                                $nomatch[] = $kk;
                            }
                            unset($value[$kk]);
                        }
                    }
                }
            }
        }
        $this->data['nomatch'] = count($nomatch);
        $this->data['customkey'] = $customkey;
        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/match', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function match_upload_data($eventid)
    {
        session_start();
        $customkey = $this->Attendee_model->get_all_custom_modules($eventid);
        $fixedc = array('Emailaddress', 'firstname', 'lastname', 'Company', 'Title');
        $key = array_merge($fixedc, array_column_1($customkey, 'column_name'));
        $skip_to_invite = '0';
        $postval = array_values($this->input->post());
        $postarr = array_combine($key, $postval);
        asort($postarr);
        $csv = $_SESSION['svdatatest'];
        $array_keys = array_values($postarr);
        $array_values = array_keys($postarr);
        foreach ($csv as $key => $value) {
            ksort($value);
            foreach ($value as $key1 => $value1) {
                if (!in_array($key1, $array_keys)) {
                    unset($value[$key1]);
                }
            }
            $value = array_combine($array_values, array_values($value));
            if (!empty($value['Emailaddress'])) {
                $attandedata = $this->Attendee_model->get_invited_attendee_bymail($value['Emailaddress'], $eventid);
                $attandeuser = $this->Attendee_model->get_user_bymail($value['Emailaddress'], $eventid);
                if (count($attandedata) <= 0 && count($attandeuser) <= 0) {
                    $cs['Emailid'] = trim($value['Emailaddress']);
                    unset($value['Emailaddress']);
                    $cs['firstname'] = $value['firstname'];
                    unset($value['firstname']);
                    $cs['lastname'] = $value['lastname'];
                    unset($value['lastname']);
                    $cs['Event_id'] = $eventid;
                    $cs['Company_name'] = $value['Company'];
                    unset($value['Company']);
                    $cs['Title'] = $value['Title'];
                    unset($value['Title']);
                    $cs['skip_to_invite'] = $skip_to_invite;
                    $cs['extra_column'] = json_encode($value);
                    $this->Attendee_model->addinvite_attendee_in_temp($cs);
                }
            }
        }
        unset($_SESSION['svdatatest']);
        redirect(base_url() . 'Attendee_admin/add/' . $eventid);
    }
    public function assign_agenda_temp_attendee($eventid)
    {
        $acid = $this->input->post('agenda_category_id');
        $attendees_id = $this->input->post('attendees_id');
        foreach ($attendees_id as $key => $value) {
            $temp_data['Emailid'] = $value;
            $temp_data['Event_id'] = $eventid;
            $temp_data['agenda_id'] = $acid;
            $this->Attendee_model->addinvite_attendee_in_temp($temp_data);
        }
        $this->session->set_flashdata('attendee_agenda_assign_data', 'Agenda Assign Successfully.');
        echo "success###" . base_url() . 'Attendee_admin/add/' . $eventid;
    }
    public function send_invitation($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->db->select('Subdomain,Event_type');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $this->data['email_template'] = $this->Setting_model->email_template($id);
        $res = $query->result_array();
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        if ($this->input->post()) {
            $string = " ";
            if ($this->input->post()) {
                $attandedata = $this->Attendee_model->get_invited_attendee_bymail($this->input->post('email_address'), $id);
                $attandeuser = $this->Attendee_model->get_user_bymail($this->input->post('email_address'), $id);
                if (count($attandedata) <= 0 && count($attandeuser) <= 0) {
                    $sent_from = $this->input->post('sent_from');
                    $msg1 = "Hello " . $this->input->post('first_name') . " " . $this->input->post('last_name') . '<br><br>';
                    $event_type = $res[0]['Event_type'];
                    $length = 6;
                    $chars = '1234567890';
                    $chars_length = (strlen($chars) - 1);
                    $string = $chars{rand(0, $chars_length)};
                    for ($i = 1; $i < $length; $i = strlen($string)) {
                        $r = $chars{rand(0, $chars_length)};
                        if ($r != $string{$i - 1}) {
                            $string .= $r;
                        }

                    }
                    $tes1 = urlencode(base64_encode($string));
                    if ($this->input->post('send_link') == '1') {
                        if ($event_type == '1') {
                            $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '/passwordset';
                            $tes = urlencode(base64_encode($this->input->post('email_address')));
                            $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">click here</a> to set your password and access the app.<br/><br/>';
                        } else {
                            $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '';
                            $tes = urlencode(base64_encode($this->input->post('email_address')));
                            $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">Click here</a> to access the app.<br><br>';
                        }
                    }
                    if ($this->input->post('send_link') == '0') {
                        $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '/attendee_registration_screen';

                        $msg1 .= 'Registration Link: Please <a href="' . $event_link . '">Click here</a> to Register in the app.<br/><br/>';
                    }
                    $msg1 .= $this->input->post('msg');
                    $sub = $this->input->post('subject1');
                    if ($sub == '') {
                        $sub = $email_template[0]['Subject'];
                    }
                    $sendername;
                    if ($this->input->post('sent_name') != "") {
                        $sendername = $this->input->post('sent_name');
                    } else {
                        $sendername = 'Event Management';
                    }
                    $sent_from = 'invite@allintheloop.com';
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_port'] = '25';
                    $config['smtp_user'] = 'invite@allintheloop.com';
                    $config['smtp_pass'] = 'xHi$&h9M)x9m';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from($sent_from, $sendername);
                    $this->email->to($this->input->post('email_address'));
                    $this->email->subject($sub);
                    $this->email->message($msg1);
                    $this->email->send();
                    if ($this->input->post('send_link') != '0') {
                        if ($this->input->post('send_link') == '1') {
                            $this->Attendee_model->addinvite_attendee($this->input->post('email_address'), $id, $this->input->post('first_name'), $this->input->post('last_name'), "", "", $string, $this->input->post('session_id'));
                        } else {
                            $cs['Emailid'] = trim($this->input->post('email_address'));
                            $cs['firstname'] = $this->input->post('first_name');
                            $cs['lastname'] = $this->input->post('last_name');
                            $cs['Event_id'] = $id;
                            $cs['skip_to_invite'] = '1';
                            $cs['agenda_id'] = $this->input->post('session_id');
                            $this->Attendee_model->addinvite_attendee_in_temp($cs);
                        }
                    }
                    $this->session->set_flashdata('attendee_data', 'Send Invitation');
                }
            }
            redirect('Attendee_admin/index/' . $id);
        }

        $this->data['Event_id'] = $id;

        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;

        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/send_invitionadd', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function send_invition_multiple_attendee($id)
    {
        $event = $this->Event_model->get_admin_event($id);
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        foreach ($csvdata as $key => $value) {
            $msg1 = "Hello " . $value['firstname'] . " " . $value['lastname'] . '<br><br>';
            $sent_from = $this->input->post('sent_from');
            $event_type = $event[0]['Event_type'];
            $length = 6;
            $chars = '1234567890';
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1}) {
                    $string .= $r;
                }

            }
            $tes1 = urlencode(base64_encode($string));
            if ($this->input->post('send_link_in_multiple') == '1') {
                if ($event_type == '1') {
                    $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '/passwordset';
                    $tes = urlencode(base64_encode($value['Emailid']));
                    $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">click here</a> to set your password and access the app.<br/><br/>';
                } else {
                    $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '';
                    $tes = urlencode(base64_encode($value['Emailid']));
                    $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '">Click here</a>to access the app.<br/><br/>';
                }
            }
            $msg1 .= $this->input->post('msg');
            $sub = $this->input->post('subject');
            if ($sub == '') {
                $sub = $email_template[0]['Subject'];
            }
            $sendername;
            if ($this->input->post('sent_name') != "") {
                $sendername = $this->input->post('sent_name');
            } else {
                $sendername = 'Event Management';
            }
            $savedata['firstname'] = $value['firstname'];
            $savedata['lastname'] = $value['lastname'];
            $savedata['Emailid'] = trim($value['Emailid']);
            $savedata['Event_id'] = $id;
            $savedata['Status'] = '0';
            $savedata['role_status'] = '0';
            $savedata['link_code'] = $string;
            if (!empty($value['Company_name'])) {
                $savedata['Company_name'] = $value['Company_name'];
            }

            if (!empty($value['Title'])) {
                $savedata['Title'] = $value['Title'];
            }

            if (!empty($value['agenda_id'])) {
                $savedata['agenda_id'] = $value['agenda_id'];
            }

            $savedata['extra_column'] = $value['extra_column'];
            if ($this->input->post('send_link_in_multiple') == '1') {
                $attendee_id = $this->Attendee_model->add_invitaion__attendees($savedata);
            } else {
                $attendee_id = '1';
            }
            if (!empty($attendee_id)) {

                $sent_from = 'invite@allintheloop.com';
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($sent_from, $sendername);
                $this->email->to($value['Emailid']);
                $this->email->subject($sub);
                $this->email->message($msg1);
                $this->email->send();
            }
        }
        $temp_id = array_column_1($csvdata, 'temp_id');
        $sti['skip_to_invite'] = '1';
        $this->Attendee_model->update_skip_to_invite_status($temp_id, $id, $sti);
        $this->session->set_flashdata('attendee_data', 'Send Invitation');
        redirect(base_url() . 'Attendee_admin/index/' . $id);
    }
    public function savecoumnname($id)
    {
        $cid = $this->input->post('column_id');
        $cnm['column_name'] = $this->input->post('column_name');
        $intRequire = $this->input->post('crequire');
        if ($intRequire == 1) {
            $cnm['crequire'] = '1';
        } else {
            $cnm['crequire'] = '0';
        }
        if (!empty($this->input->post('delete_btn'))) {
            $this->Attendee_model->delete_custom_column($id, $cid);
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        } else {
            if (!empty($cid)) {
                $this->Attendee_model->update_column_name($cnm, $cid, $id);
                redirect(base_url() . 'Attendee_admin/index/' . $id);
            } else {
                $cnm['event_id'] = $id;
                $this->Attendee_model->add_column_name($cnm);
                redirect(base_url() . 'Attendee_admin/index/' . $id);
            }
        }
    }
    public function download_data_in_csv($id)
    {
        $keysdata = $this->Attendee_model->get_all_custom_modules($id);
        $registerdUsers = $this->Attendee_model->getRegisterdUserIds($id);
        $attendees = $this->Attendee_model->get_attendee_list($id);
        $maxrow = 1;
        foreach ($attendees as $key => $value) {
            $saveagenda = $this->Attendee_model->get_save_session_list_by_user($value['uid'], $id);
            if (count($saveagenda) > $maxrow) {
                $maxrow = count($saveagenda);
            }
            $attendees[$key]['save_session'] = $saveagenda;
        }
        $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '1');
        $get_sinupform = $this->Attendee_model->get_event_signup_form_by_event_id($id);
        $signup_form_fields = json_decode($get_sinupform[0]['json_data'], true);
        $filename = "attedee_user_detail.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "First Name";
        $header[] = "Last Name";
        $header[] = "Email";
        $header[] = "Company_name";
        $header[] = "Title";
        $header[] = "Status";
        $header[] = "Registration Date";
        foreach ($keysdata as $key => $value) {
            $header[] = ucfirst($value['column_name']);
        }
        foreach ($signup_form_fields['fields'] as $key => $value) {
            $header[] = ucfirst($value['title']);
        }
        for ($i = 1; $i <= $maxrow; $i++) {
            $header[] = "Session Name";
        }
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach ($attendees as $key => $value) {
            $data['First Name'] = $value['Firstname'];
            $data['Last Name'] = $value['Lastname'];
            $data['Email'] = $value['Email'];
            $data['Company_name'] = $value['Company_name'];
            $data['Title'] = $value['Title'];
            if ($id == '1114') {
                $data['Status'] = in_array($value['Id'], $registerdUsers) ? "Registered" : "Invited";
            } else {
                $data['Status'] = "Registered";
            }

            $data['Registration Date'] = $value['reg_date'];
            for ($j = 0; $j < count($keysdata); $j++) {
                $custom = json_decode($value['extra_column'], true);
                $keynm = $keysdata[$j]['column_name'];
                if (!empty($custom[$keynm])) {
                    $data[$j] = is_array($custom[$keynm]) ? implode(',', $custom[$keynm]) : $custom[$keynm];
                } else {
                    $data[$j] = '';
                }
            }
            $singupdata = json_decode($value['json_data'], true);
            foreach ($signup_form_fields['fields'] as $skey => $svalue) {
                $fieldname = str_replace(' ', '_', strtolower($svalue['title']));
                if (!empty($singupdata[$fieldname])) {
                    $data[$fieldname] = $singupdata[$fieldname];
                } else {
                    $data[$fieldname] = '';
                }
            }
            foreach ($value['save_session'] as $akey => $avalue) {
                $data['Session Name' . $akey] = ucfirst($avalue['Heading']);
            }
            fputcsv($fp, $data);
            unset($data);
        }
        foreach ($agenda_invited_list as $key => $value) {
            if ($value['Status'] != '1') {
                $data['Name'] = $value['firstname'] . ' ' . $value['lastname'];
                $data['Email'] = $value['Emailid'];
                $data['Company_name'] = $value['Company_name'];
                $data['Title'] = $value['Title'];
                $data['Status'] = "Invited";
                for ($j = 0; $j < count($keysdata); $j++) {
                    $custom = json_decode($value['extra_column'], true);
                    $keynm = $keysdata[$j]['column_name'];
                    $data[$j] = $custom[$keynm];
                }
                fputcsv($fp, $data);
            }
        }
        foreach ($csvdata as $key => $value) {
            $data['Name'] = $value['firstname'] . ' ' . $value['lastname'];
            $data['Email'] = $value['Emailid'];
            $data['Company_name'] = $value['Company_name'];
            $data['Title'] = $value['Title'];
            $data['Status'] = "Inactive";
            for ($j = 0; $j < count($keysdata); $j++) {
                $custom = json_decode($value['extra_column'], true);
                $keynm = $keysdata[$j]['column_name'];
                $data[$j] = $custom[$keynm];
            }
            fputcsv($fp, $data);
        }
    }
    public function get_usersave_session($eid, $uid)
    {
        $saveagenda = $this->Attendee_model->get_save_session_list_by_user($uid, $eid);
        echo json_encode($saveagenda);die;
    }

    public function getuserextra_info($id, $uid)
    {
        $extra_info = $this->Attendee_model->get_extra_info_user($id, $uid, $this->input->post('user_type'));
        $keysdata = $this->Attendee_model->get_all_custom_modules($id);
        $einfo = json_decode($extra_info[0]['extra_column'], true);
        $send_array = array();
        for ($j = 0; $j < count($keysdata); $j++) {
            $kid = $keysdata[$j]['column_id'];
            $keynm = $keysdata[$j]['column_name'];
            $send_array[$kid] = $einfo[$keynm];
        }
        echo json_encode($send_array);die;
    }
    public function extra_info_save($id)
    {
        $keysdata = $this->Attendee_model->get_all_custom_modules($id);
        $post_arr = $this->input->post();
        $uid = $post_arr['extra_user_id'];
        unset($post_arr['extra_user_id']);
        $user_type = $post_arr['user_type'];
        unset($post_arr['user_type']);
        $key_name = array_column_1($keysdata, 'column_name');
        $extra_array = json_encode(array_filter(array_combine(array_values($key_name), array_values($post_arr))));
        $this->Attendee_model->update_extra_information_by_user($id, $uid, $extra_array, $user_type);
        redirect(base_url() . 'Attendee_admin/index/' . $id);
    }
    public function skip_to_invite_attendee($id)
    {
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        if ($this->input->post('skip_to_invite') == '1') {
            $temp_id = array_column_1($csvdata, 'temp_id');
            $sti['skip_to_invite'] = '1';
            $this->Attendee_model->update_skip_to_invite_status($temp_id, $id, $sti);
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        } else {
            redirect(base_url() . 'Attendee_admin/send_invitation/' . $id);
        }
    }
    public function add_attendee_without_invite($id)
    {
        $count_attendee = $this->Event_model->get_all_attendee_count($id);
        $org_id = $this->Event_model->get_org_id_by_event($id);
        $org_email = $this->Event_model->getOrgEmail($org_id);
        $iseventfreetrial = $this->Event_model->check_event_free_trial_account_by_org_email($org_email);
        if (count($iseventfreetrial) <= 0 || $count_attendee < 3) {

            $event = $this->Event_model->get_admin_event($id);
            $acc = $this->Event_template_model->getAccname($id);
            $acc_name = $acc[0]['acc_name'];
            $user['Firstname'] = $this->input->post('Firstname');
            $user['Lastname'] = $this->input->post('Lastname');
            $user['Email'] = trim($this->input->post('email_attendee'));
            if ($id == '447') {
                $this->load->library('RC4');
                $rc4 = new RC4();
                $user['Password'] = $rc4->encrypt($this->input->post('password'));
            } else {
                $user['Password'] = md5($this->input->post('password'));
            }
            $user['Created_date'] = date('Y-m-d h:i:s');
            $user['Active'] = '1';
            $user['Organisor_id'] = $event[0]['Organisor_id'];
            $this->Attendee_model->add_attendee_without_invite_as_user($id, $user);
            $slug = "WELCOME_MSG";
            $em_template = $this->Setting_model->front_email_template($slug, $id);
            $name = $this->input->post('Firstname');
            $msg = html_entity_decode($em_template[0]['Content']);
            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $patterns[2] = '/{{email}}/';
            $patterns[3] = '/{{link}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $this->input->post('password');
            $replacements[2] = $this->input->post('email_attendee');
            $msg = preg_replace($patterns, $replacements, $msg);
            $msg .= "Email: " . $this->input->post('email_attendee') . "<br/>";
            $msg .= "Password: " . $this->input->post('password') . "<br/>";
            $msg .= "Login Link: " . base_url() . 'app/' . $acc_name . '/' . $event[0]['Subdomain'] . "<br/>";
            if ($em_template['Subject'] == "") {
                $subject = "WELCOME_MSG";
            } else {
                $subject = $em_template['Subject'];
            }
            /*$sent_from='invite@allintheloop.com';
        $config['protocol']   = 'smtp';
        $config['smtp_host']  = 'localhost';
        $config['smtp_port']  = '25';
        $config['smtp_user']  = 'invite@allintheloop.com';
        $config['smtp_pass']  = 'xHi$&h9M)x9m';
        $config['mailtype'] = 'html';
        $this->email->from($sent_from, 'Allintheloop');
        $this->email->to($this->input->post('email_attendee'));
        $this->email->subject($subject);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();*/
        } else {
            $this->session->set_flashdata('org_limit_attendee', 'You can only add up to 3 Attendee Profiles until you Launch Your App.');
        }
        redirect(base_url() . 'Attendee_admin/index/' . $id);
    }
    public function save_attendee_ids($id)
    {
        $this->session->set_userdata('invite_attendees_id', $this->input->post('invite_attendees_id'));
        $this->session->set_userdata('temp_attendees_id', $this->input->post('temp_attendees_id'));
    }
    public function send_email_msg_sceen($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->db->select('Subdomain,Event_type');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $this->data['email_template'] = $this->Setting_model->email_template($id);
        $res = $query->result_array();
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;

        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/send_email_to_invited', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function send_mail_to_invite_attendee($eid)
    {
        $event = $this->Event_model->get_admin_event($eid);
        $acc = $this->Event_template_model->getAccname($eid);
        $acc_name = $acc[0]['acc_name'];
        $invite_attendees_id = $this->session->userdata('invite_attendees_id');
        $temp_attendees_id = $this->session->userdata('temp_attendees_id');
        $temp_attendees_data = $this->Attendee_model->gettempinvited_attendee_by_ids($eid, $temp_attendees_id);
        $invite_attendees_data = $this->Attendee_model->getinvited_attendee_by_id($eid, $invite_attendees_id);
        $sent_from = $this->input->post('sent_from');
        $queuemail = $this->Attendee_model->get_queue_mail_list_by_event($eid);
        $qmkey = 1;
        $event_type = $event[0]['Event_type'];
        foreach ($invite_attendees_data as $key => $value) {
            $msg1 = "Hello " . $value['firstname'] . " " . $value['lastname'] . '<br><br>';
            $tes = urlencode(base64_encode($value['Emailid']));
            if (empty($value['link_code'])) {
                $length = 6;
                $chars = '1234567890';
                $chars_length = (strlen($chars) - 1);
                $string = $chars{rand(0, $chars_length)};
                for ($i = 1; $i < $length; $i = strlen($string)) {
                    $r = $chars{rand(0, $chars_length)};
                    if ($r != $string{$i - 1}) {
                        $string .= $r;
                    }

                }
                $this->update_assign_linkcode_invite_attendee($value['Id'], $eid, $string);
                $tes1 = urlencode(base64_encode($string));
            } else {
                $tes1 = urlencode(base64_encode($value['link_code']));
            }
            //$tes1=urlencode(base64_encode($tes1));
            if ($event_type == '1') {
                $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '/passwordset/' . $tes . '/' . $tes1;
                $msg1 .= 'Registration Link : Please <a href="' . $event_link . '">click here</a> to set your password and access the app.<br/><br/>';
            } else {
                $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '/' . $tes;
                $msg1 .= 'Registration Link : Please <a href="' . $event_link . '">click here</a>to access the app.<br/><br/>';
            }
            $msg1 .= $this->input->post('msg');
            if (count($queuemail) <= 0 && $qmkey <= 30) {
                $qmkey++;
                $sent_from = 'invite@allintheloop.com';
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($sent_from, $this->input->post('sent_name'));
                $this->email->to($value['Emailid']);
                $this->email->subject($this->input->post('subject'));
                $this->email->message($msg1);
                $this->email->send();
            } else {
                $msgadd['event_id'] = $eid;
                $msgadd['from_name'] = $this->input->post('sent_name');
                $msgadd['to_email'] = $value['Emailid'];
                $msgadd['subject'] = $this->input->post('subject');
                $msgadd['message_body'] = $msg1;
                $this->Attendee_model->add_msg_queue_mail($msgadd);
            }
        }
        foreach ($temp_attendees_data as $key => $value) {
            $msg1 = "Hello " . $value['firstname'] . " " . $value['lastname'] . '<br><br>';
            $event_type = $event[0]['Event_type'];
            $length = 6;
            $chars = '1234567890';
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1}) {
                    $string .= $r;
                }

            }
            $tes1 = urlencode(base64_encode($string));
            if ($event_type == '1') {
                $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '/passwordset';
                $tes = urlencode(base64_encode($value['Emailid']));
                $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '/' . $tes1 . '">click here</a> to set your password and access the app.<br/><br/>';
            } else {
                $event_link = base_url() . 'app/' . $acc_name . "/" . $event[0]['Subdomain'] . '';
                $tes = urlencode(base64_encode($value['Emailid']));
                $msg1 .= 'Registration Link : Please <a href="' . $event_link . '/' . $tes . '">Click here</a> to access the app<br/><br/>';
            }
            $msg1 .= $this->input->post('msg');
            $sub = $this->input->post('subject');
            if ($sub == '') {
                $sub = $email_template[0]['Subject'];
            }
            $sendername;
            if ($this->input->post('sent_name') != "") {
                $sendername = $this->input->post('sent_name');
            } else {
                $sendername = 'Event Management';
            }
            $savedata['firstname'] = $value['firstname'];
            $savedata['lastname'] = $value['lastname'];
            $savedata['Emailid'] = $value['Emailid'];
            $savedata['Event_id'] = $eid;
            $savedata['Status'] = '0';
            $savedata['role_status'] = '0';
            $savedata['link_code'] = $string;
            if (!empty($value['Company_name'])) {
                $savedata['Company_name'] = $value['Company_name'];
            }

            if (!empty($value['Title'])) {
                $savedata['Title'] = $value['Title'];
            }

            if (!empty($value['agenda_id'])) {
                $savedata['agenda_id'] = $value['agenda_id'];
            }

            $savedata['extra_column'] = $value['extra_column'];
            $attendee_id = $this->Attendee_model->add_invitaion__attendees($savedata);
            if (!empty($attendee_id)) {
                if (count($queuemail) <= 0 && $qmkey <= 30) {
                    $sent_from = 'invite@allintheloop.com';
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_port'] = '25';
                    $config['smtp_user'] = 'invite@allintheloop.com';
                    $config['smtp_pass'] = 'xHi$&h9M)x9m';
                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->from($sent_from, $sendername);
                    $this->email->to($value['Emailid']);
                    $this->email->subject($sub);
                    $this->email->message($msg1);
                    $this->email->send();
                } else {
                    $msgadd['event_id'] = $eid;
                    $msgadd['from_name'] = $sendername;
                    $msgadd['to_email'] = $value['Emailid'];
                    $msgadd['subject'] = $sub;
                    $msgadd['message_body'] = $msg1;
                    $this->Attendee_model->add_msg_queue_mail($msgadd);
                }
            }
        }
        $this->session->unset_userdata('invite_attendees_id');
        $this->session->unset_userdata('temp_attendees_id');
        redirect(base_url() . 'Attendee_admin/index/' . $eid);
    }
    public function saveshowateendee($id)
    {
        if ($this->input->post('show_attendee_menu') == 1) {
            $show_attendee['event_array']['show_attendee_menu'] = $this->input->post('show_attendee_menu');
        } else {
            $show_attendee['event_array']['show_attendee_menu'] = '0';
        }
        $show_attendee['event_array']['Id'] = $id;
        $this->Event_model->update_admin_event($show_attendee);
        redirect("Attendee_admin/index/" . $id);
    }
    public function delete_invited_attendee($eid, $iid)
    {
        $attendee = $this->Attendee_model->delete_invited_attendee($iid, $eid);
        $this->session->set_flashdata('Attendee_data', 'Deleted');
        //redirect('   ' . $eid);
    }
    public function delete_inactive_attendee($eid, $temp_id)
    {
        $attendee = $this->Attendee_model->delete_inactive_attendee($temp_id, $eid);
        $this->session->set_flashdata('Attendee_data', 'Deleted');
        // redirect('Attendee_admin/index/' . $eid);
    }
    public function save_check_in_attendee($id)
    {
        $attendee_id = $this->input->post('attendee_id');
        $this->Attendee_model->attendee_check_in_save($id, $attendee_id);
        echo "success###";
    }
    public function add_view_role($id)
    {

        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $event_id = $id;

        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);

        $this->data['users_role'] = $user_role;
        $this->data['email_template'] = $this->Setting_model->email_template($id);

        $acc = $this->Event_template_model->getAccname($id);

        if ($this->input->post()) {
            $arr_view['view_name'] = $this->input->post('view_name');
            $arr_view['view_modules'] = implode(",", $this->input->post('view_modules'));
            $arr_view['view_custom_modules'] = implode(",", $this->input->post('view_custom_modules'));
            $arr_view['event_id'] = $id;
            $arr_view['view_type'] = "0";
            $arr_view['created_date'] = date('Y-m-d H:i:s');
            $this->Attendee_model->add_view_data($arr_view);
            $this->session->set_flashdata('attendee_view_data', 'Attendee View Add Successfully.');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }

        $acc_name = $acc[0]['acc_name'];
        $role_menu = $this->Event_model->geteventmenu_rolemanagement($id);
        $this->data['role_menu'] = $role_menu;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $this->data['Event_id'] = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add_role_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function edit_view_role($id, $vid)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $logged_in_user_id = $user[0]->Id;
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $view_data = $this->Attendee_model->get_all_view_by_event($id, $vid);
        $this->data['view_data'] = $view_data;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['email_template'] = $this->Setting_model->email_template($id);
        $acc = $this->Event_template_model->getAccname($id);
        if ($this->input->post()) {
            $arr_view['view_name'] = $this->input->post('view_name');
            $arr_view['view_modules'] = implode(",", $this->input->post('view_modules'));
            $arr_view['view_custom_modules'] = implode(",", $this->input->post('view_custom_modules'));
            $this->Attendee_model->edit_view_data($vid, $id, $arr_view);
            $this->session->set_flashdata('attendee_view_data', 'Attendee View Edit Successfully.');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }
        $acc_name = $acc[0]['acc_name'];
        $role_menu = $this->Event_model->geteventmenu_rolemanagement($id);
        $this->data['role_menu'] = $role_menu;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $this->data['Event_id'] = $id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/edit_role_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function delete_view($id, $vid)
    {
        $this->Attendee_model->delete_view_data($id, $vid);
        $this->session->set_flashdata('attendee_view_data', 'Attendee View Delete Successfully.');
        //redirect(base_url().'Attendee_admin/index/'.$id);
    }
    public function checkemail_by_attendee($id)
    {
        $attandeuser = $this->Attendee_model->get_user_bymail($this->input->post('email'), $id);
        if (count($attandeuser) > 0) {
            echo "error###Email already exist. Please choose another email.";
        } else {
            echo "success###";
        }die;
    }

    public function saveallow_setting_for_attendee($eid)
    {
        if ($this->input->post('attendee_hide_request_meeting') == 1) {
            $show_attendee['event_array']['attendee_hide_request_meeting'] = $this->input->post('attendee_hide_request_meeting');
        } else {
            $show_attendee['event_array']['attendee_hide_request_meeting'] = '0';
        }
        if ($this->input->post('auto_allocate_meeting_location') == 1) {
            $show_attendee['event_array']['auto_allocate_meeting_location'] = $this->input->post('auto_allocate_meeting_location');
        } else {
            $show_attendee['event_array']['auto_allocate_meeting_location'] = '0';
        }
        $show_attendee['event_array']['meeting_time_slot'] = $this->input->post('meeting_time_slot');
        $show_attendee['event_array']['meeting_start_time'] = $this->input->post('meeting_start_time');
        $show_attendee['event_array']['meeting_end_time'] = $this->input->post('meeting_end_time');
        $show_attendee['event_array']['max_meeting'] = $this->input->post('max_meeting');
        $show_attendee['event_array']['Id'] = $eid;
        $this->Event_model->update_admin_event($show_attendee);
        redirect("Attendee_admin/index/" . $eid);
    }
    public function add_authorized_email($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add_authorized_email', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function download_template_csv($eid)
    {
        $custom = $this->Attendee_model->get_all_custom_modules($eid);
        $this->load->helper('download');
        $filename = "authorized_user.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Title";
        $header[] = "Company Name";
        foreach ($custom as $key => $value) {
            $header[] = $value['column_name'];
        }
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $data['Firstname'] = "test";
        $data['Lastname'] = "testing";
        $data['Email'] = "testing@gmail.com";
        $data['Title'] = "Developer";
        $data['Company Name'] = "test pvt ltd";
        fputcsv($fp, $data);
    }
    public function upload_authorized_user($eid)
    {
        $custom = $this->Attendee_model->get_all_custom_modules($eid);
        $event = $this->Event_model->get_admin_event($eid);
        if (pathinfo($_FILES['csv_files']['name'], PATHINFO_EXTENSION) == "csv") {
            $file = $_FILES['csv_files']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            while (($datacsv = fgetcsv($handle, 0, ",")) !== false) {
                if ($find_header != '0' && !empty($datacsv[2])) {
                    $data['firstname'] = $datacsv[0];
                    $data['lastname'] = $datacsv[1];
                    $data['Email'] = $datacsv[2];
                    $data['title'] = $datacsv[3];
                    $data['company_name'] = $datacsv[4];
                    $extra_arr = array();
                    $i = 5;
                    foreach ($custom as $key => $value) {
                        if (!empty($datacsv[$i])) {
                            $extra_arr[$value['column_name']] = $datacsv[$i];
                        }
                        $i++;
                    }
                    $data['Event_id'] = $eid;
                    if (count($extra_arr) > 0) {
                        $data['extra_column'] = json_encode($extra_arr);
                    }
                    $data['email_type'] = '0';
                    $this->Attendee_model->save_authorized_user_data($data);
                }
                $find_header++;
            }
            $this->session->set_flashdata('attendee_view_data', 'Authorized Emails Added Successfully.');
            redirect(base_url() . 'Attendee_admin/index/' . $eid);
        } else {
            $this->session->set_flashdata('upload_error_data', ' please Upload csv Files.');
            redirect(base_url() . 'Attendee_admin/add_authorized_email/' . $eid);
        }
    }
    public function edit_authorized_user($id, $aid)
    {
        $auth_user_data = $this->Attendee_model->get_edit_authorized_user_data_by_id($id, $aid);
        $this->data['auth_user_data'] = $auth_user_data;
        $custom = $this->Attendee_model->get_all_custom_modules($id);
        $this->data['custom'] = $custom;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['event_id'] = $id;
        if ($this->input->post()) {
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['Email'] = $this->input->post('Email');
            $data['title'] = $this->input->post('title');
            $data['company_name'] = $this->input->post('company_name');
            $input_arr = $this->input->post();
            $extra_arr = array();
            foreach ($custom as $key => $value) {
                $key = str_ireplace(" ", "_", $value['column_name']);
                if (!empty($input_arr[$key])) {
                    $extra_arr[$value['column_name']] = $input_arr[$key];
                }
                $i++;
            }
            $data['extra_column'] = json_encode($extra_arr);
            $update = $this->Attendee_model->update_authorized_user_data_by_id($data, $id, $aid);
            if ($update) {
                $this->session->set_flashdata('attendee_view_data', 'Authorized User Updated Successfully.');
                redirect(base_url() . 'Attendee_admin/index/' . $id);
            } else {
                $this->session->set_flashdata('authorized_error_data', 'Email Address Already exists Please Choose Another Email Address.');
                redirect(base_url() . 'Attendee_admin/edit_authorized_user/' . $id . '/' . $aid);
            }
        }
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/edit_authorized_email', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function check_exists_authuseremail($eid, $aid)
    {
        $check = $this->Attendee_model->check_already_exists_email_in_authorized($this->input->post('Email'), $eid, $aid);
        if (count($check) > 0) {
            echo "error###Email Address Already exists Please Choose Another Email Address.";
        } else {
            echo "success###";
        }
        die;
    }
    public function delete_authorized_user($eid, $aid)
    {
        $this->Attendee_model->delete_authorized_email($eid, $aid);
        $this->session->set_flashdata('attendee_view_data', 'Authorized User Deleted Successfully.');
        //redirect(base_url().'Attendee_admin/index/'.$eid);
    }
    public function save_check_in_authorized_user($eid)
    {
        $authorized_id = $this->input->post('authorized_id');
        $this->Attendee_model->Authorized_check_in_save($eid, $authorized_id);
        echo "success###";
    }
    public function Authorized_assign_agenda_category($eid)
    {
        $auth_user_id = $this->input->post('auth_user_id');
        $agrnda_id = $this->input->post('agenda_category_id');
        $views_id = $this->input->post('views_id');
        $this->Attendee_model->authorized_user_assign_agenda($eid, $auth_user_id, $agrnda_id, $views_id);
        echo "success###" . base_url() . 'Attendee_admin/index/' . $eid;
    }

    public function assign_moderator_to_attendee($eid)
    {
        $rel_data['event_id'] = $eid;
        $rel_data['user_id'] = $this->input->post('user_id');
        $this->Attendee_model->save_attendee_moderator_relation($rel_data, $this->input->post('moderator_user_id'));
        $this->session->set_flashdata('attendee_view_data', 'Moderator Assign Successfully.');
        redirect("Attendee_admin/index/" . $eid . '#allow_metting');
    }
    public function send_attndee_registration_mailpdf($eid)
    {
        $event = $this->Event_model->get_admin_event($id);
        $badgesdata = $this->Attendee_model->get_badges_values_by_event($eid);
        $type_color = json_decode($badgesdata[0]['attendee_type_color'], true);
        $color = !empty($type_color[$this->input->post('attendee_type')]) ? $type_color[$this->input->post('attendee_type')] : '#000000';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/TCPDF-master/config/tcpdf_config.php';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/TCPDF-master/tcpdf.php';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'localhost';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'invite@allintheloop.com';
        $config['smtp_pass'] = 'xHi$&h9M)x9m';
        $config['_encoding'] = 'base64';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        if ($badgesdata[0]['code_type'] != '1') {
            $this->load->library('Ciqrcode');
        } else {
            $this->load->library('zend');
            //load in folder Zend
            $this->zend->load('Zend/Barcode');
        }
        foreach ($this->input->post('attendees_id') as $key => $value) {
            $attendee_user = $this->Event_model->get_user_detail_by_user_id($value);
            if (count($attendee_user) > 0) {
                $filesname = "badges_files.pdf";
                if ($badgesdata[0]['code_type'] != '1') {
                    //$params['data'] = $attendee_user[0]['Email'].'##'.rand('111111','999999');
                    $params['data'] = $eid . '-' . $attendee_user[0]['Id'];
                    $params['level'] = 'M';
                    $params['size'] = 2;
                    $params['savename'] = 'tes.png';
                    $this->ciqrcode->generate($params);
                } else {
                    $imageResource = Zend_Barcode::factory('code128', 'image', array('text' => $eid . '-' . $attendee_user[0]['Id'], 'drawText' => false, 'barHeight' => 50, 'factor' => 3), array('barHeight' => 50, 'factor' => 3))->draw();
                    imagepng($imageResource, './assets/user_files/Barcode.png');
                }
                show_errors();
                if ($eid == '1367') {
                    $pdf = new TCPDF('l', PDF_UNIT, 'A5', true, 'UTF-8', false);
                    //case 'A5' : {$pf = array(  419.528,  595.276); break;}
                    $pdf->SetTitle('Badge Design Final - Visitor');
                    $pdf->SetLeftMargin(2);
                    $pdf->SetRightMargin(5);
                    $pdf->SetTopMargin(5);
                    $pdf->SetFooterMargin(5);
                    $pdf->AddPage();
                    $pdf->SetAutoPageBreak(false, 0);
                    $pdf->setPrintFooter(false);

                    $htm = '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:0px solid white;margin-top:0px;">
                    <tr>
                        <td width="49%" align="left">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th width="100%" align="left" style="vertical-align:top;">';
                    if (!empty($badgesdata[0]['badges_logo_images'])) {
                        $htm .= '<img width="524" height="220" src="' . base_url() . 'assets/badges_files/' . $badgesdata[0]['badges_logo_images'] . '"/>';
                    }
                    $htm .= '</th>
                            </tr>
                            <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <font size="22"><span style="text-align:center;"><b>' . ucfirst($attendee_user[0]['Firstname']) . '</b></span></font>
                                <font size="22"><span style="text-align:center;"><b>' . ucfirst($attendee_user[0]['Lastname']) . '</b></span></font>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:10" height="10">&nbsp;</td>
                            </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <span style="text-align:center; padding-top:20px; font-size:20px;">' . ucfirst($attendee_user[0]['Company_name']) . '</span>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>

                        <tr>
                            <th width="100%" align="center" style="margin-top:2%;float:right;">';
                    if ($badgesdata[0]['code_type'] != '1') {
                        $htm .= '<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                    } else {
                        $htm .= '<img style="width:200px; height:50px; float:right;" src="' . base_url() . 'assets/user_files/Barcode.png"/>';
                    }

                    $htm .= '</th>
                        </tr>
                        <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="96%">
                                <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png">
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="2%"></td>
                <td width="49%" align="right">
                <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th width="100%" align="left" style="vertical-align:top;">';
                    if (!empty($badgesdata[0]['badges_logo_images'])) {
                        $htm .= '<img width="524" height="220" src="' . base_url() . 'assets/badges_files/' . $badgesdata[0]['badges_logo_images'] . '"/>';
                    }
                    $htm .= '</th>
                         </tr>
                        <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <font size="22"><span style="text-align:center;"><b>' . ucfirst($attendee_user[0]['Firstname']) . '</b></span></font>
                                <font size="22"><span style="text-align:center;"><b>' . ucfirst($attendee_user[0]['Lastname']) . '</b></span></font>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:10" height="10">&nbsp;</td>
                            </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <span style="text-align:center; padding-top:20px; font-size:20px;">' . ucfirst($attendee_user[0]['Company_name']) . '</span>
                            </td>
                        </tr>
                            <tr>
                                <td style="height:20" height="20">&nbsp;</td>
                            </tr>

                        <tr>
                            <th width="100%" align="center" style="margin-top:2%;float:right;">';
                    if ($badgesdata[0]['code_type'] != '1') {
                        $htm .= '<img style="width:60px; height:60px; float:right" src="tes.png"/>';
                    } else {
                        $htm .= '<img style="width:200px; height:50px; float:right;" src="' . base_url() . 'assets/user_files/Barcode.png"/>';
                    }

                    $htm .= '</th>
                        </tr>
                        <tr>
                            <td style="height:20" height="20">&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="96%">
                                <img width="300" height="80" src="https://www.allintheloop.net/assets/images/template2/hostedby.png">
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            </table>';
                    $pdf->writeHTML($htm);

                } else {

                    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
                    $pdf->SetTitle('Badge Design Final - Visitor');
                    $pdf->AddPage();
                    $htm = '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
	                                    <tr>
	                                        <th colspan="4" height="10"></th>
	                                    </tr>
	                                    <tr>
	                                        <th width="2%"></th>
	                                        <th width="58%" align="left" style="vertical-align:top;">';
                    if (!empty($badgesdata[0]['badges_logo_images'])) {
                        $htm .= '<img width="504" height="158" src="' . base_url() . 'assets/badges_files/' . $badgesdata[0]['badges_logo_images'] . '"/>';
                    }
                    $htm .= '</th>
	                                        <th width="38%" align="right" style="margin-top:5%;">';
                    if ($badgesdata[0]['code_type'] != '1') {
                        $htm .= '<img src="tes.png"/>';
                    } else {
                        $htm .= '<img src="' . base_url() . 'assets/user_files/Barcode.png"/>';
                    }
                    $htm .= '</th>
	                                        <th width="2%" align="right"></th>
	                                    </tr>
	                                    <tr style="padding-bottom:15px;">
	                                        <th width="3%"></th>
	                                        <th width="97%" colspan="3">
	                                            <font size="18"><b>' . ucfirst($attendee_user[0]['Firstname']) . '</b></font><br>
	                                            <font size="14"><b>' . ucfirst($attendee_user[0]['Lastname']) . '</b></font><br><br>
	                                            <font size="10"><i>' . ucfirst($attendee_user[0]['Company_name']) . '</i></font><br>
	                                            <font size="10">' . ucfirst($attendee_user[0]['Title']) . '</font><br>
	                                            <font size="10"> Badge Number : ' . $eid . '-' . $attendee_user[0]['Id'] . '</font>
	                                        </th>
	                                    </tr>
	                                    <tr>
	                                        <th colspan="4" height="20"></th>
	                                    </tr>
	                                    <tr style="">
	                                        <th colspan="4" width="100%" style="background-color:' . $color . ';color:#fff; text-align:center; vertical-align:center;">' . strtoupper($this->input->post('attendee_type')) . '</th>
	                                    </tr>
	                                </table>
	                            </td>
	                            <td width="50%"><table width="100%" style="border:0.5px solid black;" border="0" cellspacing="0" cellpadding="0">
	                                    <tr>
	                                        <th colspan="4" height="10"></th>
	                                    </tr>
	                                    <tr>
	                                        <th width="2%"></th>
	                                        <th width="58%" align="left" style="vertical-align:top;">';
                    if (!empty($badgesdata[0]['badges_logo_images'])) {
                        $htm .= '<img width="504" height="158" src="' . base_url() . 'assets/badges_files/' . $badgesdata[0]['badges_logo_images'] . '"/>';
                    }
                    $htm .= '</th>
	                                        <th width="38%" align="right" style="margin-top:5%;">';
                    if ($badgesdata[0]['code_type'] != '1') {
                        $htm .= '<img src="tes.png"/>';
                    } else {
                        $htm .= '<img src="' . base_url() . 'assets/user_files/Barcode.png"/>';
                    }
                    $htm .= '</th>
	                                        <th width="2%" align="right"></th>
	                                    </tr>
	                                    <tr style="padding-bottom:15px;">
	                                        <th width="3%"></th>
	                                        <th width="97%" colspan="3">
	                                            <font size="18"><b>' . ucfirst($attendee_user[0]['Firstname']) . '</b></font><br>
	                                            <font size="14"><b>' . ucfirst($attendee_user[0]['Lastname']) . '</b></font><br><br>
	                                            <font size="10"><i>' . ucfirst($attendee_user[0]['Company_name']) . '</i></font><br>
	                                            <font size="10">' . ucfirst($attendee_user[0]['Title']) . '</font><br>
	                                            <font size="10"> Badge Number : ' . $eid . '-' . $attendee_user[0]['Id'] . '</font>
	                                        </th>
	                                    </tr>
	                                    <tr>
	                                        <th colspan="4" height="20"></th>
	                                    </tr>
	                                    <tr style="">
	                                        <th colspan="4" width="100%" style="background-color:' . $color . ';color:#fff; text-align:center; vertical-align:center;">' . strtoupper($this->input->post('attendee_type')) . '</th>
	                                    </tr>
	                                </table>
	                            </td>
	                        </tr>
	                        </table>';
                    $pdf->writeHTML($htm);
                }

                if ($eid == 1802 || $eid == 1511) {
                    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, "pt", 'A6', true, 'UTF-8', false);
                    $pdf->SetTitle('Badge Design');

                    //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $pdf->AddPage();
                    $pdf->SetFontSize(22);

                    $htm = '<table style="text-align:center;" align="center">
                                 <tbody>
                                        <tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr>
                                      <tr>
                                           <td ><b>' . ucfirst($attendee_user[0]['Firstname']) . '</b></td>
                                      </tr>

                                      <tr >
                                           <td ><p><b>' . ucfirst($attendee_user[0]['Lastname']) . '</b></p></td>
                                      </tr>

                                      <tr >
                                           <td ><span></span><small class="cmpnyname">' . ucfirst($attendee_user[0]['Company_name']) . '</small></td>
                                      </tr>
                                      <tr>
                                           <td>';
                    if ($badgesdata[0]['code_type'] != '1') {
                        $htm .= '<img style="width:75px; height:75px; margin-top:30px;" src="tes.png"/>';
                    } else {
                        $htm .= '<img style="width:200px; height:50px; " src="' . base_url() . 'assets/user_files/Barcode.png"/>';
                    }
                    $htm .= '</td>
                                      </tr>
                                 </tbody>
                            </table>';

                    // echo $htm;exit;
                    $pdf->writeHTML($htm, true, false, false, false, 'C');

                    //$pdf->writeHTML($htm);
                }
                file_put_contents('./assets/user_files/' . $filesname, $pdf->Output($filesname, 'S'));

                unset($htm);
                $Subject = !empty($badgesdata[0]['subject']) ? $badgesdata[0]['subject'] : "Welcome Message";
                $sender_name = !empty($badgesdata[0]['sender_name']) ? $badgesdata[0]['sender_name'] : $event[0]['Event_name'];
                $msg = html_entity_decode($badgesdata[0]['email_body']);
                $patterns = array();
                $patterns[0] = '/{{name}}/';
                $replacements = array();
                $replacements[0] = ucfirst($attendee_user[0]['Firstname']) . ' ' . $attendee_user[0]['Lastname'];
                $msg = preg_replace($patterns, $replacements, $msg);
                $this->email->from('invite@allintheloop.com', $sender_name);
                //$this->email->to($attendee_user[0]['Email']);
                /*if($eid == 1511)
                $this->email->to('rashmi@elsner.in');
                else*/
                $this->email->to($attendee_user[0]['Email']);
                $this->email->subject($Subject);
                $this->email->message($msg);
                $this->email->attach('./assets/user_files/' . $filesname);
                $this->email->send();
                $this->email->clear(true);
                unlink('./assets/user_files/' . $filesname);
                unset($pdf);
                if ($badgesdata[0]['code_type'] == '1') {
                    unlink('./assets/user_files/Barcode.png');
                }
            }
        }
    }

    public function save_meeting_locations($eid)
    {
        $ml_data['event_id'] = $eid;
        $ml_data['location'] = $this->input->post('locations');
        $this->Attendee_model->save_meeting_locations_in_event($ml_data, $this->input->post('locations_id'));
        $this->session->set_flashdata('attendee_view_data', 'Meeting Locations Save Successfully...');
        redirect("Attendee_admin/index/" . $eid . '#metting_location');
    }
    public function delete_meetinglocations($eid, $lid)
    {
        $this->Attendee_model->delete_meeting_locations_in_event($eid, $lid);
        $this->session->set_flashdata('attendee_view_data', 'Meeting Locations Deleted Successfully...');
        //redirect("Attendee_admin/index/".$eid);
    }
    public function add_mass_attendee($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/mass_attendee_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function download_attendee_template_csv($eid)
    {
        $custom = $this->Attendee_model->get_all_custom_modules($eid);
        $this->load->helper('download');
        $filename = "mass_attendee.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Title";
        $header[] = "Company Name";
        $header[] = "Profile Images";
        $header[] = "Keywords";
        foreach ($custom as $key => $value) {
            $header[] = $value['column_name'];
        }
        $header[] = "Password";
        $header[] = "Attendee Group";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);
        $data['Firstname'] = "test";
        $data['Lastname'] = "testing";
        $data['Email'] = "testing@gmail.com";
        $data['Title'] = "Developer";
        $data['Company Name'] = "test pvt ltd";
        $data['Profile Images'] = "i.e(https://)";
        $data['Keywords'] = "sales";
        $data['Password'] = "123456";
        $data['Attendee Group'] = "Group";
        fputcsv($fp, $data);
    }
    public function upload_mass_attendee($eid)
    {
        ini_set('max_execution_time', 0);
        $custom = $this->Attendee_model->get_all_custom_modules($eid);
        $attendee_group = $this->Attendee_model->get_user_group($eid);
        $event = $this->Event_model->get_admin_event($eid);
        $org_id = $event[0]['Organisor_id'];
        if (pathinfo($_FILES['csv_files']['name'], PATHINFO_EXTENSION) == "csv") {
            $file = $_FILES['csv_files']['tmp_name'];
            $handle = fopen($file, "r");
            $find_header = 0;
            while (($datacsv = fgetcsv($handle, 0, ",")) !== false) {
                if ($find_header != '0' && !empty($datacsv[2])) {
                    $data['firstname'] = $datacsv[0];
                    $data['lastname'] = $datacsv[1];
                    $data['Email'] = $datacsv[2];
                    $data['title'] = $datacsv[3];
                    $data['company_name'] = $datacsv[4];
                    $data['keywords'] = $datacsv[6];

                    if (!empty($datacsv[5])) {
                        $images = "profilelogo" . uniqid() . ".png";
                        copy($datacsv[5], "./assets/user_files/" . $images);
                        $data['Logo'] = $images;
                    }
                    $extra_arr = array();
                    $i = 7;
                    foreach ($custom as $key => $value) {
                        if (!empty($datacsv[$i])) {
                            $extra_arr[$value['column_name']] = $datacsv[$i];
                        }
                        $i++;
                    }
                    if (count($extra_arr) > 0) {
                        $data['extra_column'] = json_encode($extra_arr);
                    }
                    $pass = $i++;
                    $group = $i++;
                    if ($datacsv[$pass]) {
                        $data['Password'] = md5($datacsv[$pass]);
                    }

                    if ($datacsv[$group]) {
                        $key = array_search($datacsv[$group], array_column_1($attendee_group, 'group_name'));
                        $data['group_id'] = $attendee_group[$key]['id'];
                    }
                    // exec('/usr/local/bin/php index.php Attendee_admin addAttendeeUpload '.$data.' '.$eid.' '.$org_id.' > /dev/null &');
                    $this->Attendee_model->add_mass_attendee_without_invite_as_user($data, $eid, $event[0]['Organisor_id']);
                    unset($data);
                }
                $find_header++;
            }
            $this->session->set_flashdata('attendee_view_data', 'Attendee Users Added Successfully.');
            redirect(base_url() . 'Attendee_admin/index/' . $eid);
        } else {
            $this->session->set_flashdata('upload_error_data', ' please Upload csv Files.');
            redirect(base_url() . 'Attendee_admin/add_mass_attendee/' . $eid);
        }
    }
    public function addAttendeeUpload($data, $event_id, $org_id)
    {
        $this->Attendee_model->add_mass_attendee_without_invite_as_user($data, $event_id, $org_id);
    }
    public function add_group($id)
    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        $logged_in_user_id = $user[0]->Id;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        if ($this->input->post()) {
            $insert['group_name'] = $this->input->post('group_name');
            $insert['event_id'] = $id;
            $insert['created_date'] = date('Y-m-d h:i:s');
            $insert['send_message'] = ($this->input->post('send_message')) ? '1' : '0';
            $insert['send_request'] = ($this->input->post('send_request')) ? '1' : '0';
            $insert['max_meeting_limit'] = $this->input->post('max_meeting_limit') ?: '0';
            $group_id = $this->Attendee_model->add_user_group($insert);
            if ($this->input->post('Status') == '1') {
                $i = 0;
                while ($i < 6) {
                    $col = $this->input->post('columns' . $i);
                    $query = $this->input->post('operator' . $i);
                    $search = $this->input->post('search_values' . $i);
                    if (!empty($col) && !empty($query) && !empty($search)) {
                        if ($query == 'start_with') {
                            $where[] = "(u." . $col . " like '%" . $search . "')";
                        } elseif ($query == 'end_with') {
                            $where[] = "(u." . $col . " like '" . $search . "%')";
                        } elseif ($query == 'contains') {
                            $where[] = "(u." . $col . " like '%" . $search . "%')";
                        } else {
                            $where[] = "(u." . $col . " " . $query . " '" . $search . "')";
                        }
                    }
                    $i++;
                }
                $where = implode('AND', $where);

                $this->Attendee_model->get_attendee_id($id, $where, $group_id);
            }
            redirect('Attendee_admin/index/' . $id);
        }
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add_group_views', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    public function add_group_permisson($event_id)
    {
        if ($this->input->post()) {
            $group_ids = explode(',', $this->input->post('group_ids'));
            foreach ($group_ids as $value) {
                $where['id'] = $value;
                $where['event_id'] = $event_id;
                $update['permitted_group'] = implode(',', $_POST[$value]);
                $this->Attendee_model->add_group_permisson($where, $update);
            }
        }
        redirect(base_url() . 'Attendee_admin/index/' . $event_id);
    }
    public function delete_group($event_id, $id)
    {
        $this->Attendee_model->delete_group($id, $event_id);
        $this->session->set_flashdata('attendee_view_data', 'Group Delete Successfully.');
        //redirect(base_url().'Attendee_admin/index/'.$event_id);
    }
    public function update_group($event_id)
    {
        $id = $this->input->post('id');
        $update['group_name'] = $this->input->post('group_name');
        $update['send_message'] = ($this->input->post('send_message')) ? '1' : '0';
        $update['send_request'] = ($this->input->post('send_request')) ? '1' : '0';
        $update['max_meeting_limit'] = $this->input->post('max_meeting_limit') ?: '0';
        $update['permitted_group'] = implode(',', $this->input->post('permitted_group'));
        $this->Attendee_model->update_group($update, $id);
        $this->session->set_flashdata('attendee_view_data', 'Group Updated Successfully.');
        redirect(base_url() . 'Attendee_admin/index/' . $event_id);
    }
    public function save_email_session($id)
    {
        $custom = $this->Attendee_model->get_all_custom_modules($id);
        $event = $this->Event_model->get_admin_event($id);
        if ($this->input->post()) {
            if (pathinfo($_FILES['csv_files']['name'], PATHINFO_EXTENSION) == "csv") {
                $file = $_FILES['csv_files']['tmp_name'];
                $handle = fopen($file, "r");
                $find_header = 0;
                while (($datacsv = fgetcsv($handle, 0, ",")) !== false) {
                    if ($find_header != '0') {
                        $data['firstname'] = $datacsv[1];
                        $data['lastname'] = $datacsv[2];
                        $data['Email'] = $datacsv[3];
                        $data['title'] = $datacsv[5];
                        $data['company_name'] = $datacsv[4];
                        /*if(!empty($datacsv[5]))
                        {
                        $images="profilelogo".uniqid().".png";
                        copy($datacsv[5],"./assets/user_files/".$images);
                        $data['Logo']=$images;
                        }*/
                        $extra_arr = array();
                        /*$i=6;
                        foreach ($custom as $key => $value) {
                        if(!empty($datacsv[$i])){
                        $extra_arr[$value['column_name']]=$datacsv[$i];
                        }
                        $i++;
                        }
                        if(count($extra_arr) > 0)
                        {*/
                        $extra_arr['choose agenda'] = $datacsv[0];
                        $extra_arr['gender'] = $datacsv[6];
                        $data['extra_column'] = json_encode($extra_arr);
                        //}
                        $this->Attendee_model->add_mass_attendee_without_invite_as_user($data, $id, $event[0]['Organisor_id']);

                        $this->Attendee_model->direct_assign_agenda_category(trim($datacsv[3]), trim($datacsv[21]), $id);
                        if (!empty(trim($datacsv[7]))) {
                            $this->Attendee_model->save_email_agenda(trim($datacsv[3]), trim($datacsv[7]), $id);
                        }
                    }
                    $find_header++;
                }
            }
            $this->session->set_flashdata('upload_success_data', 'Attendee Users Session Saved Successfully.');
            redirect(base_url() . 'Attendee_admin/save_email_session/' . $id);
        }
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['user_list'] = $users;
        $Organisor_id = $this->Event_model->get_org_id_by_event($id);
        $logged_in_user_id = $user[0]->Id;
        $this->data['event'] = $event[0];
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/save_email_session_views', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }
    // make event_attendee extra_column json
    /* public function temp_attendee_extra_column($event_id)
    {
    $this->Attendee_model->add_extra_column_temp($event_id);
    }*/

    public function attendee_list($id)
    {
        $this->load->library('pagination');

        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $group_name = $this->Attendee_model->get_attendee_group_list($user[0]->Organisor_id);
        $this->data['attendee_group'] = $group_name;
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();

        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User') {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;

            $attendees = $this->Attendee_model->get_attendee_list_pagination(null, $limit_per_page, $start_index);
            $this->data['Attendees'] = $attendees;
        }

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $meeting_locations = $this->Event_template_model->get_all_meeting_locations_by_event($id);
        $this->data['meeting_locations'] = $meeting_locations;
        /*$this->data['registration_data']=$this->Attendee_model->get_registration_screen_data($id,null);*/
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $this->data['keysdata'] = $this->Attendee_model->get_all_custom_modules($id);
        $attendees = $this->Attendee_model->get_attendee_list_pagination($id, $limit_per_page, $start_index);

        $this->data['Attendees'] = $attendees;
        $total_records = $this->Attendee_model->get_attendee_count($id);
        $config['base_url'] = base_url() . 'attendee_admin/attendee_list/' . $id;
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();

        $this->data['moderator'] = $this->Attendee_model->get_moderator_list_by_event($id);
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->data['skipcsvdata'] = $csvdata;
        /*$badges_data=$this->Attendee_model->get_badges_values_by_event($id);
        $this->data['badges_data']=$badges_data;*/
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '1');
        $this->data['csvdata'] = $csvdata;
        $view_data = $this->Attendee_model->get_all_view_by_event($id);
        $this->data['view_data'] = $view_data;
        $authorized_user = $this->Attendee_model->get_authorized_user_data($id);
        $this->data['auth_user_list'] = $authorized_user;
        $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
        $this->data['agenda_invited_list'] = $agenda_invited_list;
        $arrSingupForm = $this->Formbuilder_model->get_singup_form_list($id);
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        if (!empty($arrSingupForm)) {
            $this->data['arrSingupForm'] = $arrSingupForm;
        }
        if ($this->input->post()) {
            $string = "";

            foreach ($this->input->post('invites') as $key => $value) {
                $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
                foreach ($agenda_invited_list as $key1 => $values1) {
                    $arraydata = array();
                    if ($values1['Emailid'] == $value) {
                        $arraydata = $values1;
                        break;
                    }
                }
                $msg = $this->input->post('msg');
                $sent_from = $this->input->post('sent_from');
                if ($this->input->post('send_link') == '1') {
                    $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '';
                    $tes = urlencode(base64_encode($value));
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                    $msg1 .= ' <br/><br/> ' . 'Invitation Link:  ' . $event_link . '/' . $tes . '';
                } else {
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                }
                $msg1 .= "<br/><br/>" . $msg;
                $sub = $this->input->post('subject1');
                $sendername;
                if ($this->input->post('sent_name') != "") {
                    $sendername = $this->input->post('sent_name');
                } else {
                    $sendername = 'Event Management';
                }
                $sent_from = 'invite@allintheloop.com';
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($sent_from, $sendername);
                $this->email->to($value);
                $this->email->subject($sub);
                $this->email->message($msg1);
                $this->email->send();

            }
            $this->session->set_flashdata('attendee_data', 'Invitation Resend');
            redirect('Attendee_admin/index/' . $id . '/1');
        }

        $this->data['Event_id'] = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;

        $menudata = $this->Event_model->geteventmenu($id, 2);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);

        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        //$this->data['H_images'] = $menudata[0]->H_images;
        //$this->data['B_images'] = $menudata[0]->B_images;
        //$this->data['F_images'] = $menudata[0]->F_images;

        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/index_test', $this->data, true);

        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        if (!empty($arrSingupForm)) {
            $this->template->write_view('js', 'admin/edit_js', $this->data, true);
        } else {
            $this->template->write_view('js', 'admin/add_js', $this->data, true);
        }
        $this->template->render();
    }
    public function add_attendee_categorie($id) //Monday 26 March 2018 06:06:28 PM IST

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post()) {
            $categorie_data['category'] = $this->input->post('categorie_name');
            $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
            $categorie_data['event_id'] = $id;
            $categorie_data['menu_id'] = '2';
            $categorie_data['updated_date'] = date('Y-m-d H:i:s');
            $categorie_data['created_date'] = date('Y-m-d H:i:s');
            $this->Attendee_model->add_attendee_categorie($categorie_data);
            $this->session->set_flashdata('attendee_data', 'Categorie Added');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add_attendee_categorie', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
        $this->template->render();
    }
    public function attendee_categories_edit($id, $cid) //Monday 26 March 2018 06:06:36 PM IST

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post()) {

            $categorie_data['category'] = $this->input->post('categorie_name');
            $categorie_data['categorie_keywords'] = $this->input->post('Short_desc');
            $categorie_data['updated_date'] = date('Y-m-d H:i:s');

            $this->Attendee_model->update_attendee_categorie($categorie_data, $cid);
            $this->session->set_flashdata('attendee_data', ' Categorie Updated');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }
        $this->data['categorie_data'] = $this->Attendee_model->get_attendee_categories_by_event($id, $cid);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'exibitor/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/edit_attendee_categorie', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
        $this->template->render();
    }
    public function delete_attendee_categorie($id, $cid) //Monday 26 March 2018 06:06:41 PM IST

    {
        $this->Attendee_model->delete_attendee_categorie($cid, $id);
        $this->session->set_flashdata('attendee_data', 'Categorie Deleted');
        //redirect(base_url().'Attendee_admin/index/'.$id);
    }
    public function delete_meeting($event_id, $id)
    {
        $this->Attendee_model->delete_meeting($event_id, $id);
        $this->session->set_flashdata('attendee_data', 'Meeting Deleted');
        // redirect(base_url().'Attendee_admin/index/'.$event_id);
    }

    public function download_meeting_data($id)
    {
        $meeting_data = $this->Attendee_model->get_booked_meetings($id);
        $filename = "attedee_meetings.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Sender";
        $header[] = "Reciever";
        $header[] = "Location";
        $header[] = "Date";
        $header[] = "Time";
        $header[] = "Status";

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach ($meeting_data as $key => $value) {

            $data['Sender'] = $value['sender'];
            $data['Reciever'] = $value['reciever'];
            $data['Location'] = $value['location'];
            $data['Date'] = $value['date'];
            $data['Time'] = $value['time'];
            if ($value['status'] == '1') {
                $data['Status'] = "Accepted";
            } elseif ($value['status'] == '2') {
                $data['Status'] = "Rejected";
            } else {
                $data['Status'] = "Pending";
            }
            fputcsv($fp, $data);
            unset($data);
        }
    }
    public function index_temp($id)
    {
        if ($id == '1012' || $id == '634') {
            redirect(base_url() . 'Attendee_admin/attendee_list/' . $id);
            exit;
        }
        $keyword = '';
        if ($this->input->post('keyword')) {
            $keyword = $this->input->post('keyword');
        }
        $this->data['keyword'] = $keyword;
        $this->load->library('pagination');
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $event = $this->Event_model->get_admin_event($id);
        $this->data['Event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $group_name = $this->Attendee_model->get_attendee_group_list($user[0]->Organisor_id);
        $this->data['attendee_group'] = $group_name;
        $this->db->select('Subdomain');
        $this->db->from('event');
        $this->db->where('Id', $id);
        $query = $this->db->get();
        $res = $query->result_array();

        $orid = $this->data['user']->Id;
        if ($this->data['user']->Role_name == 'User') {
            $total_permission = $this->Agenda_model->get_permission_list();
            $this->data['total_permission'] = $total_permission;

            // $attendees = $this->Attendee_model->get_attendee_list();
            // $this->data['Attendees'] = $attendees;

            $attendees = $this->Attendee_model->get_attendee_list_pagination(null, $limit_per_page, $start_index, $keyword);
            $this->data['Attendees'] = $attendees;
        }

        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $meeting_locations = $this->Event_template_model->get_all_meeting_locations_by_event($id);
        $this->data['meeting_locations'] = $meeting_locations;
        /*$this->data['registration_data']=$this->Attendee_model->get_registration_screen_data($id,null);*/
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $this->data['keysdata'] = $this->Attendee_model->get_all_custom_modules($id);

        // $attendees = $this->Attendee_model->get_attendee_list($id);
        // $this->data['Attendees'] = $attendees;

        $attendees = $this->Attendee_model->get_attendee_list_pagination($id, $limit_per_page, $start_index, $keyword);
        $this->data['Attendees'] = $attendees;

        $total_records = $this->Attendee_model->get_attendee_count($id, $keyword);
        $config['base_url'] = base_url() . 'attendee_admin/index_temp/' . $id;
        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();

        $this->data['moderator'] = $this->Attendee_model->get_moderator_list_by_event($id);
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '0');
        $this->data['skipcsvdata'] = $csvdata;
        /*$badges_data=$this->Attendee_model->get_badges_values_by_event($id);
        $this->data['badges_data']=$badges_data;*/
        $csvdata = $this->Attendee_model->get_all_csv_data_by_event($id, '1');
        $this->data['csvdata'] = $csvdata;
        $view_data = $this->Attendee_model->get_all_view_by_event($id);
        $this->data['view_data'] = $view_data;
        $authorized_user = $this->Attendee_model->get_authorized_user_data($id);
        $this->data['auth_user_list'] = $authorized_user;
        $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
        $this->data['agenda_invited_list'] = $agenda_invited_list;
        $arrSingupForm = $this->Formbuilder_model->get_singup_form_list($id);
        $acc = $this->Event_template_model->getAccname($id);
        $acc_name = $acc[0]['acc_name'];
        if (!empty($arrSingupForm)) {
            $this->data['arrSingupForm'] = $arrSingupForm;
        }
        if ($this->input->post('invites')) {
            $string = "";

            foreach ($this->input->post('invites') as $key => $value) {
                $agenda_invited_list = $this->Attendee_model->get_invited_attendee($id);
                foreach ($agenda_invited_list as $key1 => $values1) {
                    $arraydata = array();
                    if ($values1['Emailid'] == $value) {
                        $arraydata = $values1;
                        break;
                    }
                }
                $msg = $this->input->post('msg');
                $sent_from = $this->input->post('sent_from');
                if ($this->input->post('send_link') == '1') {
                    $event_link = base_url() . 'app/' . $acc_name . "/" . $res[0]['Subdomain'] . '';
                    $tes = urlencode(base64_encode($value));
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                    $msg1 .= ' <br/><br/> ' . 'Invitation Link:  ' . $event_link . '/' . $tes . '';
                } else {
                    $msg1 = 'Hi ' . $arraydata['firstname'] . " " . $arraydata['lastname'] . ', <br/> <br/> ';
                }
                $msg1 .= "<br/><br/>" . $msg;
                $sub = $this->input->post('subject1');
                $sendername;
                if ($this->input->post('sent_name') != "") {
                    $sendername = $this->input->post('sent_name');
                } else {
                    $sendername = 'Event Management';
                }
                $sent_from = 'invite@allintheloop.com';
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'localhost';
                $config['smtp_port'] = '25';
                $config['smtp_user'] = 'invite@allintheloop.com';
                $config['smtp_pass'] = 'xHi$&h9M)x9m';
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->from($sent_from, $sendername);
                $this->email->to($value);
                $this->email->subject($sub);
                $this->email->message($msg1);
                $this->email->send();

            }
            $this->session->set_flashdata('attendee_data', 'Invitation Resend');
            redirect('Attendee_admin/index_temp/' . $id . '/1');
        }

        $this->data['Event_id'] = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;

        $menudata = $this->Event_model->geteventmenu($id, 2);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);

        $this->data['get_booked_meetings_data'] = $this->Attendee_model->get_booked_meetings($id);

        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->data['categorie_data'] = $this->Attendee_model->get_attendee_categories_by_event($id, null);
        $this->data['attendee_categories'] = $this->Attendee_model->get_attendee_categories_by_event($id);

        //$this->data['H_images'] = $menudata[0]->H_images;
        //$this->data['B_images'] = $menudata[0]->B_images;
        //$this->data['F_images'] = $menudata[0]->F_images;

        /* $meeting_dates = file_get_contents(base_url().'native_single_fcm/attendee/getRequestMeetingDate?event_id='.$id.'&user_id='.$user[0]->Id);
        $meeting_dates = json_decode($meeting_dates,true);
        $this->data['meeting_dates'] = $meeting_dates['meeting_dates'];*/
        $this->template->write_view('css', 'admin/css', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/index22', $this->data, true);

        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        if (!empty($arrSingupForm)) {
            $this->template->write_view('js', 'admin/edit_js', $this->data, true);
        } else {
            $this->template->write_view('js', 'admin/add_js', $this->data, true);
        }
        $this->template->render();
    }
    public function get_meeting_time($id)
    {

        $time1 = file_get_contents(base_url() . 'native_single_fcm/attendee/getRequestMeetingTime?event_id=' . $id . '&date=' . $_POST['meeting_date'] . '&user_id=' . $_POST['sender_attendee']);
        $time1 = json_decode($time1, true)['meeting_time'];

        $time2 = file_get_contents(base_url() . 'native_single_fcm/attendee/getRequestMeetingTime?event_id=' . $id . '&date=' . $_POST['meeting_date'] . '&user_id=' . $_POST['reciever_attendee']);
        $time2 = json_decode($time2, true)['meeting_time'];

        $time = array_intersect($time1, $time2);

        $str = "<option value=''>Select Time</option>";
        foreach ($time as $key => $value) {
            $str .= "<option value='" . $value . "'>$value</option>";
        }
        echo $str;
    }
    public function get_meeting_location($id)
    {
        $location = file_get_contents(base_url() . 'native_single_fcm/attendee/getRequestMeetingLocation?event_id=' . $id . '&date=' . $_POST['meeting_date'] . '&user_id=' . $_POST['sender_attendee'] . '&time=' . $_POST['meeting_time']);
        $location = json_decode($location, true)['meeting_location'];
        $str = "<option value=''>Select Location</option>";
        foreach ($location as $key => $value) {
            $str .= "<option value='" . $value . "'>$value</option>";
        }
        echo $str;
    }

    public function get_meeting_location1()
    {
        $event_id = $this->input->get_post('event_id');
        $date = $this->input->get_post('meeting_date');
        $time = $this->input->get_post('meeting_time');

        $location = $this->Attendee_model->getfreelocation($event_id, $date, $time);
        $str = "<option value=''>Select Location</option>";
        foreach ($location as $key => $value) {
            $str .= "<option value='" . $value . "'>$value</option>";
        }
        echo $str;
    }

    public function save_request($id)
    {
        error_reporting(E_ALL);
        $data['event_id'] = $id;
        $data['recever_attendee_id'] = $_POST['reciever_attendee'];
        $data['attendee_id'] = $_POST['sender_attendee'];
        $date_format = $this->Attendee_model->checkEventDateFormat($id);
        $date = $_POST['meeting_date'];
        if ($date_format[0]['date_format'] == '0') {
            $date = DateTime::createFromFormat('d-m-Y', $date);
        } else {
            $date = DateTime::createFromFormat('m-d-Y', $date);
        }
        $data['date'] = $date->format('Y-m-d');
        $data['time'] = $_POST['meeting_time'];
        $data['location'] = $_POST['meeting_location'];
        $data['status'] = '1';
        $data['created_datetime'] = date('Y-m-d H:i:s');
        $this->Attendee_model->SaveRequest($data);
        redirect('Attendee_admin/index/' . $id);
    }
    public function import_attendees($id)
    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $org = $user[0]->Organisor_id;
        $org_email = $this->Event_model->getOrgEmail($org);
        $this->data['org_email'] = $org_email;
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['Event_id'] = $id;
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
        $this->data['category_list'] = $category_list;
        $agenda_list = $this->Agenda_model->get_agenda_list($id);
        $this->data['agenda_list'] = $agenda_list;

        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/import_attendees', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
        $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
        $this->template->render();
    }

    public function download_import_attendees_csv()
    {
        $this->load->helper('download');
        $filename = "import_attendees.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Company_name";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Email";
        $header[] = "Password";
        $header[] = "Salutation";
        $header[] = "Title";

        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, $header);

        for ($i = 1; $i <= 1; $i++) {
            $data['Company_name'] = "AITL" . $i;
            $data['Firstname'] = "John" . $i;
            $data['Lastname'] = "Deo" . $i;
            $data['Email'] = "john" . $i . "@deo.com";
            $data['Password'] = $i;
            $data['Salutation'] = "Test" . $i;
            $data['Title'] = "Test Title" . $i;
            fputcsv($fp, $data);
        }
        /*$data['Company_name'] = "AITL";
    $data['Firstname'] = "John";
    $data['Lastname'] = "Deo";
    $data['Email'] = "john@deo.com";
    $data['Password'] = "123456";
    $data['Salutation'] = "Test";
    $data['Title'] = "Test Title";
    fputcsv($fp, $data);*/

    }

    public function save_import_attendees($id)
    {

        $event_data = $this->Event_model->get_event_accesskey_id($id, $this->input->post('access_key'));
        if (empty($event_data)) {
            $this->session->set_flashdata('csv_flash_message', 'Access key is wrong.');
            redirect('Attendee_admin/import_attendees/' . $id);
        }

        if ($this->input->post('file-type') == 'JSON') {
            $data['json_url'] = $this->input->post('json_url');
            $data['import_type'] = 'attendees';
            $data['event_id'] = $id;
            $data['file_type'] = '1';
            $this->Event_model->saveEventCSVImport($data);
            $event_data = $this->Event_model->get_event_accesskey_id($id, $this->input->post('access_key'));
            $saveCSVData = $this->Event_model->saveJSONData($data['json_url'], $event_data['Id'], $event_data['Organisor_id']);
            $this->session->set_flashdata('csv_flash_message', 'JSON Imported Successfully.');
            redirect(base_url() . 'Attendee_admin/import_attendees/' . $id);
        } else {
            session_start();
            $filepath = $this->input->post('csv_url');
            $contents = file_get_contents($this->input->post('csv_url'));
            if (strlen($contents)) {
                $_SESSION['access_key'] = $this->input->post('access_key');
                $_SESSION['csv_url'] = $this->input->post('csv_url');

                $handle = fopen($filepath, "r");
                $find_header = 0;
                while (($datacsv[] = fgetcsv($handle, 1000, ",")) !== false) {}
                $keys = array_shift($datacsv);
                foreach ($datacsv as $key => $value) {
                    $csv[] = array_combine($keys, $value);
                }
                $nomatch = array();
                $csvtest = array_filter($csv);
                $this->data['import_csv_data'] = $csvtest;
                $_SESSION['save_import_attendees'] = $csvtest;
                $this->data['keysdata'] = $keys;
                foreach ($csv as $key => $value) {
                    $keysarr = array_keys($value);
                    $fixedc = array('Company_name', 'Firstname', 'Lastname', 'Email', 'Password', 'Salutation', 'Title');
                    for ($i = 0; $i < count($keysarr); $i++) {
                        if (!in_array($keysarr[$i], $fixedc)) {
                            $kk = $keysarr[$i];
                            if (!in_array($kk, $nomatch)) {
                                $nomatch[] = $kk;
                            }
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('csv_flash_message', 'CSV file not found.');
                redirect('Attendee_admin/import_attendees/' . $id);
            }
            $this->data['nomatch'] = count($nomatch);
            $this->data['Event_id'] = $id;
            $statelist = $this->Profile_model->statelist();
            $this->data['Statelist'] = $statelist;
            $event = $this->Event_model->get_admin_event($id);
            $this->data['event'] = $event[0];
            $category_list = $this->Agenda_model->get_all_agenda_category_list($id);
            $this->data['category_list'] = $category_list;
            $agenda_list = $this->Agenda_model->get_agenda_list($id);
            $this->data['agenda_list'] = $agenda_list;
            $this->template->write_view('css', 'admin/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'attendee_admin/maping_import_attendees', $this->data, true);
            if ($this->data['user']->Role_name == 'User') {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            } else {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'attendee_admin/add_js', $this->data, true);
            $this->template->write_view('js', 'attendee_admin/js', $this->data, true);
            $this->template->render();
        }
    }

    public function upload_import_attendees($eventid)
    {
        session_start();
        $key = array_keys($this->input->post());
        $postval = array_values($this->input->post());
        $postarr = array_combine($key, $postval);
        asort($postarr);

        $saveEventCSV['event_id'] = $eventid;
        $saveEventCSV['import_type'] = 'attendees';
        $saveEventCSV['csv_url'] = $_SESSION['csv_url'];
        $saveEventCSV['csv_mapping'] = json_encode($postarr);

        $csvSave = $this->Event_model->saveEventCSVImport($saveEventCSV);

        $access_key = $_SESSION['access_key'];
        $event_data = $this->Event_model->get_event_accesskey_id($eventid, $access_key);
        // lq();
        $saveCSVData = $this->Event_model->saveCSVData($saveEventCSV['csv_url'], $event_data['Id'], $event_data['Organisor_id']);

        /*$csv = $_SESSION['save_import_attendees'];
        $access_key = $_SESSION['access_key'];
        $array_keys = array_values($postarr);
        $array_values = array_keys($postarr);

        foreach ($csv as $key => $value)
        {
        ksort($value);
        foreach ($value as $key1 => $value1)
        {
        if(!in_array($key1, $array_keys))
        {
        unset($value[$key1]);
        }
        }
        $value = array_combine($array_values, array_values($value));

        if(!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
        {
        $event_data = $this->Event_model->get_event_accesskey_id($eventid,$access_key);
        $Event_id = $event_data['Id'];
        $Organisor_id = $event_data['Organisor_id'];
        $Role_id = 4;

        $emailCheck = $this->Event_model->checkUserEmail($value['Email']);
        if(empty($emailCheck))
        {
        $add_user['Company_name'] = $value['Company_name'];
        $add_user['Firstname'] = $value['Firstname'];
        $add_user['Lastname'] = $value['Lastname'];
        $add_user['Email'] = $value['Email'];
        $add_user['Password'] = md5($value['Password']);
        $add_user['Salutation'] = $value['Salutation'];
        $add_user['Title'] = $value['Title'];
        $add_user['Organisor_id'] = $Organisor_id;
        $add_user['created_date'] = date('Y-m-d H:i:s');

        $User_id = $this->Event_model->addAttendeeUserOpenApi($add_user);

        $add_relation_event_user['Event_id'] = $Event_id;
        $add_relation_event_user['User_id'] = $User_id;
        $add_relation_event_user['Organisor_id'] = $Organisor_id;
        $add_relation_event_user['Role_id'] = $Role_id;

        $relationEventUser = $this->Event_model->addRelationEventUser($add_relation_event_user);
        }
        else
        {
        $update_user['Company_name'] = $value['Company_name'];
        $update_user['Firstname'] = $value['Firstname'];
        $update_user['Lastname'] = $value['Lastname'];
        $update_user['Email'] = $value['Email'];
        if(!empty($value['Password']))
        {
        $update_user['Password'] = md5($value['Password']);
        }
        $update_user['Salutation'] = $value['Salutation'];
        $update_user['Title'] = $value['Title'];
        $update_user['Organisor_id'] = $Organisor_id;
        $update_user['updated_date'] = date('Y-m-d H:i:s');
        $updateUser = $this->Event_model->updateAttendeeUserOpenApi($emailCheck['Id'],$update_user);
        }
        }
        }*/
        unset($_SESSION['save_import_attendees']);
        unset($_SESSION['access_key']);
        unset($_SESSION['csv_url']);
        $this->session->set_flashdata('csv_flash_message', 'CSV Import Successfully.');
        redirect(base_url() . 'Attendee_admin/import_attendees/' . $eventid);
    }
    /* Import Attendees functions end*/
    public function download_import_attendees_json()
    {
        error_reporting(E_ALL);
        $this->load->helper('download');
        $filename = "import_attendees.json";
        $fp = fopen('php://output', 'w');

        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename=' . $filename);

        for ($i = 1; $i <= 1; $i++) {
            $data['Company_name'] = "AITL" . $i;
            $data['Firstname'] = "John" . $i;
            $data['Lastname'] = "Deo" . $i;
            $data['Email'] = "john" . $i . "@deo.com";
            $data['Password'] = $i;
            $data['Salutation'] = "Test" . $i;
            $data['Title'] = "Test Title" . $i;
            $final[] = $data;
        }
        $data = json_encode($final);
        fputs($fp, $data);
    }
    public function show_import_attendee_json()
    {
        echo "<pre><h3>First Object is Defination and Second Object is sample Data</h3>";
        echo '[
            {
                "Company_name": "<font color=green><b>(String)</b></font> Company Name",
                "Firstname": "<font color=green><b>(String)</b></font> Firstname of Attendee",
                "Lastname": "<font color=green><b>(String)</b></font> Lastname of Attendee",
                "Email": "<font color=green><b>(String)</b></font> Email of Attendee <font color=red><b>**Required**</b></font>",
                "Password": "<font color=green><b>(String)</b></font> Password",
                "Salutation": "<font color=green><b>(String)</b></font> Salutation",
                "Title": "<font color=green><b>(String)</b></font> Title"
            },
            {
                "Company_name": "ABC Pvt. Ltd.",
                "Firstname": "John",
                "Lastname": "Deo",
                "Email": "john@deo.com",
                "Password": "123456",
                "Salutation": "Mr.",
                "Title": "CEO"
            }
        ]';exit();
        error_reporting(E_ALL);
        header('Content-type: application/json');

        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) {
                $data['Company_name'] = "(String) Company Name";
                $data['Firstname'] = "(String) Firstname of Attendee";
                $data['Lastname'] = "(String) Lastname of Attendee";
                $data['Email'] = "(String) Email of Attendee **Required**";
                $data['Password'] = "(String) Password";
                $data['Salutation'] = "(String) Salutation";
                $data['Title'] = "(String) Title";
            } else {
                $data['Company_name'] = "ABC Pvt. Ltd.";
                $data['Firstname'] = "John";
                $data['Lastname'] = "Deo";
                $data['Email'] = "john@deo.com";
                $data['Password'] = "123456";
                $data['Salutation'] = "Mr.";
                $data['Title'] = "CEO";
            }
            $fin[$i] = $data;
        }
        echo json_encode($fin, JSON_PRETTY_PRINT);
    }
    public function add_attendee_filter($id) //8/13/2018 6:42:43 PM

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        if ($this->input->post()) {
            $data['custom_column'] = $this->input->post('custom_column');
            $data['category_id'] = $this->input->post('attendee_categories') ?: null;
            $data['label'] = $this->input->post('label');
            $data['event_id'] = $id;
            $this->Attendee_model->addFilter($data);
            $this->session->set_flashdata('attendee_data', 'Filter Added');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['custom_column'] = $this->Attendee_model->getCustomColumn($id);
        $this->data['attendee_categories'] = $this->Attendee_model->get_attendee_categories_by_event($id);
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/add_attendee_filter', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
        $this->template->render();
    }
    public function edit_attendee_filter($id, $filter_id) //8/13/2018 6:42:43 PM

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['filter'] = $this->Attendee_model->getFilter($id, $filter_id);
        if ($this->input->post()) {
            $data['custom_column'] = $this->input->post('custom_column');
            $data['category_id'] = $this->input->post('attendee_categories') ?: null;
            $data['label'] = $this->input->post('label');
            $data['event_id'] = $id;
            $data['tags'] = $this->input->post('tags');
            $data['remove_tag'] = array_diff(explode(',', $this->data['filter']['tags']), explode(',', $this->input->post('tags')));
            $this->Attendee_model->updateFilter($data, $filter_id);
            $this->session->set_flashdata('attendee_data', 'Filter Added');
            redirect(base_url() . 'Attendee_admin/index/' . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $this->data['custom_column'] = $this->Attendee_model->getCustomColumn($id);
        $this->data['attendee_categories'] = $this->Attendee_model->get_attendee_categories_by_event($id);
        $this->session->set_userdata($data);
        $this->template->write_view('css', 'attendee_admin/add_css', $this->data, true);
        $this->template->write_view('content', 'attendee_admin/edit_attendee_filter', $this->data, true);
        if ($this->data['user']->Role_name == 'User') {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        } else {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'exibitor/add_js', $this->data, true);
        $this->template->render();
    }
    public function deleteFilter($event_id, $id)
    {
        $this->Attendee_model->deleteFilter($event_id, $id);
        $this->session->set_flashdata('attendee_view_data', 'Attendee Filter Deleted Successfully...');
    }
}
