<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Attendee extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Attendee';
        $this->data['smalltitle'] = 'Attendee';
        $this->data['breadcrumb'] = 'Attendee';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->library('formloader1');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Message_model');
        $this->load->model('Notes_admin_model');
        $this->load->model('Profile_model');
        $this->load->model('Agenda_model');
        $user = $this->session->userdata('current_user');
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (!in_array('2', $menu_list))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $eventname = $this->Event_model->get_all_event_name();
        if (in_array($this->uri->segment(3) , $eventname))
        {
            if ($user != '')
            {
                $parameters = $this->uri->uri_to_assoc(1);
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                $logged_in_user_id = $user[0]->User_id;
                $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                if ($cnt == 1)
                {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                    $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type = $event_templates[0]['Event_type'];
                    if ($event_type == 3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain = $this->uri->segment(3);
                        $acc_name = $this->uri->segment(2);
                        echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
                    }
                }
            }
        }
        else
        {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain = $this->uri->segment(3);
            $flag = 1;
            echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
        }
    }
    public function index($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            if ($req_mod == "Attendee")
            {
                $req_mod = "Attendees";
            }
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $this->data['sign_form_data'] = $res1;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Attendee' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $my_favorites = $this->Event_template_model->get_my_favorites_user_by_event_menu_id($eid, $user[0]->Id, '2');
            $this->data['my_favorites'] = $my_favorites;
        }
        $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
        $this->data['attendees'] = $attendees;
        $attendees_mycontact = $this->Event_template_model->get_attendee_my_contact_user($Subdomain);
        $this->data['attendees_mycontact'] = $attendees_mycontact;
        $this->data['Subdomain'] = $Subdomain;
        $intFormBuild = $this->input->post('hdnFormBuild');
        if ($intFormBuild == 1)
        {
            unset($_POST['hdnFormBuild']);
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            echo '<script>window.location.href="' . base_url() . 'Attendee/' . $acc_name . "/" . $Subdomain . '"</script>';
            exit;
        }
        if ($this->input->post())
        {
            $data1 = array(
                'Message' => $this->input->post('Message') ,
                'Sender_id' => $lid,
                'Receiver_id' => $this->uri->segment(3) ,
                'Parent' => '0',
                'Time' => date("H:i:s") ,
                'ispublic' => $this->input->post('ispublic')
            );
            $this->Message_model->send_speaker_message($data1);
            $Sid = $this->uri->segment(3);
            $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
            $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);
            $string = '';
            $string.= '<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with ' . $view_chats1[0]['Recivername'] . '<br/></span>';
            $user = $this->session->userdata('current_user');
            $lid = $user[0]->Id;
            foreach($view_chats1 as $key => $value)
            {
                if ($value['Sender_id'] == $lid)
                {
                    echo '<strong>' . $value['Recivername'] . '</strong>';
                    echo '<br/>';
                    echo $value['Message'];
                    echo '<br/>';
                    echo $value['Time'];
                    echo '<br/>';
                    echo '<br/>';
                }
                else
                {
                    echo '<strong>' . $value['Sendername'] . '</strong>';
                    echo '<br/>';
                    echo $value['Message'];
                    echo '<br/>';
                    echo $value['Time'];
                    echo '<br/>';
                    echo '<br/>';
                }
            }
            echo $string;
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Attendee/index', $this->data, true);
                $this->template->write_view('footer', 'Attendee/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Attendee/index', $this->data, true);
                $this->template->write_view('footer', 'Attendee/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('2', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Attendee/index', $this->data, true);
                $this->template->write_view('footer', 'Attendee/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Attendee/index', $this->data, true);
                $this->template->write_view('footer', 'Attendee/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function View($acc_name, $Subdomain = NULL, $id = null)

    {
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $meeting_locations = $this->Event_template_model->get_all_meeting_locations_by_event($event_templates[0]['Id']);
        $this->data['meeting_locations'] = $meeting_locations;
        $gamification_point = $this->Event_template_model->get_gamification_point_by_user_id($event_templates[0]['Id'], $id);
        $this->data['gamification_point'] = $gamification_point;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['subdomain'] = $Subdomain;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['sp_id'] = $id;
        $this->data['Sid'] = $id;
        $this->data['allow_contact'] = $this->Event_model->get_allow_contact_me_option_by_user_id($id, $event_templates[0]['Id']);
        $user = $this->session->userdata('current_user');
        $this->data['show_share_contact'] = $this->Event_template_model->get_share_conact($event_templates[0]['Id'], $user[0]->Id, $id);
        $this->data['pending_share_contact'] = $this->Event_template_model->get_pending_share_conact($event_templates[0]['Id'], $id);
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                // $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $id);
                $this->data['view_chats1'] = $view_chats1;
                // $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                // $this->data['cms_menu'] = $cmsmenu;
                $test = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $user_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;
                $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['social_url'] = $social_url;
                if (!empty($test))
                {
                    $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
                }
                else
                {
                    $user_url = $this->Event_template_model->get_attendee_user_url($Subdomain, $id);
                    if ($user[0]->Rid == '4')
                    {
                        $user_url[0]['message_permisson'] = $this->Event_template_model->check_message_permisson($id, $user[0]->Id, $event_templates[0]['Id']);
                    }
                    else
                    {
                        $user_url[0]['message_permisson'] == '1';
                    }
                    $this->data['user_url'] = $user_url;
                }
                $this->template->write_view('content', 'Attendee/user', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                // $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $id);
                $this->data['view_chats1'] = $view_chats1;
                // $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                // $this->data['cms_menu'] = $cmsmenu;
                $test = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $user_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;
                $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['social_url'] = $social_url;
                if (!empty($test))
                {
                    $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
                }
                else
                {
                    $user_url = $this->Event_template_model->get_attendee_user_url($Subdomain, $id);
                    if ($user[0]->Rid == '4')
                    {
                        $user_url[0]['message_permisson'] = $this->Event_template_model->check_message_permisson($id, $user[0]->Id, $event_templates[0]['Id']);
                    }
                    else
                    {
                        $user_url[0]['message_permisson'] == '1';
                    }
                    $this->data['user_url'] = $user_url;
                }
                $this->template->write_view('content', 'Attendee/user', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('2', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                if (!empty($user))
                {
                    // $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                    $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $id);
                    $this->data['view_chats1'] = $view_chats1;
                }
                $test = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $user_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;
                $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['social_url'] = $social_url;
                if (!empty($test))
                {
                    $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
                }
                else
                {
                    $user_url = $this->Event_template_model->get_attendee_user_url($Subdomain, $id);
                    if ($user[0]->Rid == '4')
                    {
                        $user_url[0]['message_permisson'] = $this->Event_template_model->check_message_permisson($id, $user[0]->Id, $event_templates[0]['Id']);
                    }
                    else
                    {
                        $user_url[0]['message_permisson'] == '1';
                    }
                    $this->data['user_url'] = $user_url;
                }
                $this->template->write_view('content', 'Attendee/user', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                // $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $id);
                $this->data['view_chats1'] = $view_chats1;
                // $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                // $this->data['cms_menu'] = $cmsmenu;
                $test = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $user_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;
                $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                $this->data['social_url'] = $social_url;
                if (!empty($test))
                {
                    $social_url = $this->Event_template_model->get_attendee_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
                }
                else
                {
                    $user_url = $this->Event_template_model->get_attendee_user_url($Subdomain, $id);
                    if ($user[0]->Rid == '4')
                    {
                        $user_url[0]['message_permisson'] = $this->Event_template_model->check_message_permisson($id, $user[0]->Id, $event_templates[0]['Id']);
                    }
                    else
                    {
                        $user_url[0]['message_permisson'] == '1';
                    }
                    $this->data['user_url'] = $user_url;
                }
                $this->template->write_view('content', 'Attendee/user', $this->data, true);
            }
        }
        $this->template->render();
    }
    public function share_contact_info($acc_name=NULL, $Subdomain=NULL, $uid=NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $share_data = array(
            'event_id' => $event_templates[0]['Id'],
            'from_id' => $user[0]->Id,
            'to_id' => $uid,
            'approval_status' => '0',
        );
        $shar_id = $this->Event_template_model->share_contact_infomation($share_data);
        $url = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/update_approval_status/' . $shar_id;
        $Message = "<a href='" . $url . "'>" . ucfirst($user[0]->Firstname) . " " . $user[0]->Lastname . " has shared their contact details with you. Tap here to share your contact details with " . ucfirst($user[0]->Firstname) . " " . $user[0]->Lastname . " to reveal their details</a>";
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $user[0]->Id,
            'Receiver_id' => $uid,
            'Event_id' => $event_templates[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $user[0]->Id,
            'msg_type' => '1'
        );
        $this->Message_model->send_speaker_message($data1);
        redirect(base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '#content');
    }
    public function confirm_user_request($acc_name=NULL, $Subdomain=NULL, $uid=NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $share_data = array(
            'event_id' => $event_templates[0]['Id'],
            'from_id' => $this->input->post('from_id') ,
            'to_id' => $this->input->post('to_id') ,
            'approval_status' => '1',
        );
        $shar_id = $this->Event_template_model->share_contact_infomation($share_data);
        $pending_share_contact = $this->Event_template_model->get_pending_share_conact($event_templates[0]['Id'], $this->input->post('to_id'));
        echo "success###" . count($pending_share_contact);
        die;
    }
    public function reject_user_request($acc_name=NULL, $Subdomain=NULL, $uid=NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $share_data = array(
            'event_id' => $event_templates[0]['Id'],
            'from_id' => $this->input->post('from_id') ,
            'to_id' => $this->input->post('to_id') ,
            'approval_status' => '2',
        );
        $shar_id = $this->Event_template_model->share_contact_infomation($share_data);
        $pending_share_contact = $this->Event_template_model->get_pending_share_conact($event_templates[0]['Id'], $this->input->post('to_id'));
        echo "success###" . count($pending_share_contact);
        die;
    }
    public function update_approval_status($acc_name=NULL, $Subdomain=NULL, $sid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $share_contact = $this->Event_template_model->get_share_contact_share_id($sid);
        if ($user[0]->Id == $share_contact[0]['to_id'])
        {
            $share_data = array(
                'event_id' => $share_contact[0]['event_id'],
                'from_id' => $share_contact[0]['from_id'],
                'to_id' => $share_contact[0]['to_id'],
                'approval_status' => '1',
            );
            $shar_id = $this->Event_template_model->share_contact_infomation($share_data);
        }
        redirect(base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '#content');
    }
    public function checpendingkmetting($acc_name=NULL, $Subdomain=NULL)

    {
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $attendees_metting = $this->Agenda_model->get_all_pending_metting_by_attendee($event_templates[0]['Id']);
        $this->data['attendees_metting'] = $attendees_metting;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['Subdomain'] = $Subdomain;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->write_view('content', 'Attendee/checkmetting_view', $this->data, true);
        $this->template->render();
    }
    public function request_meeting($acc_name=NULL, $Subdomain=NULL, $uid=NULL)

    {
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $location = $this->input->post('location');
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $overlapping = $this->Agenda_model->check_overlapping_meeting_with_agenda($user[0]->Id, $event[0]['Id'], $date, $time);
        if (count($overlapping) > 0 && $this->input->post('clash_check') == '1')
        {
            echo "Info###This will clash with " . $overlapping[0]['Heading'] . "- would you like to proceed?";
            die;
        }
        else
        {
            $validmeetinglocation = true;
            if (!empty($this->input->post()))
            {
                $checkdata = $this->input->post();
                $checkdata['event_id'] = $event[0]['Id'];
                $checkvalid = $this->Agenda_model->check_meeting_location_clash_with_other($checkdata, $user[0]->Id, $uid);
                $validmeetinglocation = $checkvalid['status'];
            }
            if ($validmeetinglocation)
            {
                $moderator = $this->Event_template_model->get_moderator_by_user($uid, $event[0]['Id']);
                if (count($moderator) > 0)
                {
                    $metting['moderator_id'] = $moderator['moderator_id'];
                    $metting['recipient_attendee_id'] = $uid;
                }
                else
                {
                    $metting['recever_attendee_id'] = $uid;
                }
                $metting['attendee_id'] = $user[0]->Id;
                $metting['event_id'] = $event[0]['Id'];
                $metting['location'] = $location;
                $metting['date'] = $date;
                $metting['time'] = $time;
                $metting['status'] = '0';
                $metting_id = $this->Agenda_model->add_metting_with_exibitor($metting);
                $reminder_data['event_id'] = $event[0]['Id'];
                $reminder_data['user_id'] = $user[0]->Id;
                $reminder_data['modules_id'] = '0';
                $reminder_data['msid'] = $metting_id;
                $reminder_data['reminder_time'] = '15';
                $reminder_data['read_status'] = '0';
                $reminder_data['created_date'] = date('Y-m-d H:i:s');
                if (count($moderator) > 0)
                {
                    $recipient_user = $this->Event_model->get_user_detail_by_user_id($uid);
                    $url = base_url() . 'Speakers/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
                    $Message = "<a href='" . $url . "'>" . ucfirst($user[0]->Firstname) . " " . $user[0]->Lastname . " has requested a Meeting with " . ucfirst($recipient_user[0]['Firstname']) . " " . $recipient_user[0]['Lastname'] . " at " . $time . " on " . $date . " Tap here to accept or reject the meeting </a>";
                    $uid = $moderator['moderator_id'];
                }
                else
                {
                    $url = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
                    $Message = "<a href='" . $url . "'>" . ucfirst($user[0]->Firstname) . " " . $user[0]->Lastname . " has requested a Meeting with you at " . $time . " on " . $date . " Tap here to accept or reject the meeting </a>";
                }
                $data1 = array(
                    'Message' => $Message,
                    'Sender_id' => $user[0]->Id,
                    'Receiver_id' => $uid,
                    'Event_id' => $event[0]['Id'],
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s") ,
                    'ispublic' => '0',
                    'msg_creator_id' => $user[0]->Id,
                    'msg_type' => '3'
                );
                $this->Message_model->send_speaker_message($data1);
                echo "success###";
                die;
            }
            else
            {
                echo "Error###" . $checkvalid['msg'];
                die;
            }
        }
    }
    public function changemettingstatus($acc_name=NULL, $Subdomain=NULL, $mid=NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $metting_data = $this->Agenda_model->change_metting_status_by_metting_id($mid, $this->input->post('status'));
        $user_url = $this->Event_model->get_user_detail_by_user_id($metting_data[0]['recever_attendee_id']);
        if ($metting_data[0]['status'] == '2')
        {
            if (empty($this->input->post('reject_msg')))
            {
                $Message = ucfirst($user_url[0]['Firstname']) . " " . $user_url[0]['Lastname'] . " has denied your Meeting request due to a clash. Please try another time.";
            }
            else
            {
                $Message = ucfirst($user_url[0]['Firstname']) . " " . $user_url[0]['Lastname'] . " has rejected your meeting request - " . $this->input->post('reject_msg');
            }
        }
        else if ($metting_data[0]['status'] == '1')
        {
            $Message = ucfirst($user_url[0]['Firstname']) . " " . $user_url[0]['Lastname'] . " Have Accepted Your Meeting Request.";
        }
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $metting_data[0]['recever_attendee_id'],
            'Receiver_id' => $metting_data[0]['attendee_id'],
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $ex_uid
        );
        $this->Message_model->send_speaker_message($data1);
        echo "Success###";
        die;
    }
    public function suggest_new_datetime($acc_name=NULL, $Subdomain=NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $mid = $this->input->post('meeting_id_textbox');
        $meeting_data = $this->Agenda_model->get_meeting_data_by_meeting_id($mid);
        $user_url = $this->Event_model->get_user_detail_by_user_id($meeting_data[0]['recever_attendee_id']);
        $Message = ucfirst($user_url[0]['Firstname']) . " " . $user_url[0]['Lastname'] . " is unable to meet with you at the time you specified. " . ucfirst($user_url[0]['Firstname']) . " " . $user_url[0]['Lastname'] . " has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";
        $time = $this->input->post('time');
        foreach($this->input->post('date') as $key => $value)
        {
            $date = date('Y-m-d H:i', strtotime($value . ' ' . $time[$key]));
            $this->Agenda_model->add_suggest_new_datetime($event[0]['Id'], $date, $mid, $meeting_data[0]['attendee_id'], NULL, $meeting_data[0]['recever_attendee_id']);
            $link = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/changemeetingdate/' . strtotime($date) . '/' . $mid;
            $Message.= "<a href='" . $link . "'>" . $date . "</a><br/>";
        }
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $user[0]->Id,
            'Receiver_id' => $meeting_data[0]['attendee_id'],
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $user[0]->Id,
            'msg_type' => '4'
        );
        $this->Message_model->send_speaker_message($data1);
        redirect(base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting');
    }
    public function changemeetingdate($acc_name=NULL, $Subdomain=NULL, $datetime=NULL, $mid=NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $meeting_data = $this->Agenda_model->get_meeting_data_by_meeting_id($mid);
        $user = $this->session->userdata('current_user');
        if ($user[0]->Id == $meeting_data[0]['attendee_id'])
        {
            $m_da['date'] = date('Y-m-d', $datetime);
            $m_da['time'] = date('H:i', $datetime);
            $m_da['status'] = '1';
            $this->Agenda_model->update_datetime_in_meeting_by_meeting_id($m_da, $mid);
            $this->Agenda_model->delete_suggest_meeting_date($event[0]['Id'], $meeting_data[0]['attendee_id'], null, $meeting_data[0]['recever_attendee_id']);
        }
        $Message = $meeting_data[0]['Firstname'] . " " . $meeting_data[0]['Lastname'] . " at " . $meeting_data[0]['Company_name'] . " as accepted your new suggested time of " . date('Y-m-d H:i', $datetime) . ". This meeting has now been booked";
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $user[0]->Id,
            'Receiver_id' => $meeting_data[0]['recever_attendee_id'],
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $user[0]->Id
        );
        $this->Message_model->send_speaker_message($data1);
        redirect(base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_user_agenda');
    }
}