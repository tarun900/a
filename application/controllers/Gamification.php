<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Gamification extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Gamification';
        $this->data['smalltitle'] = 'Gamification';
        $this->data['breadcrumb'] = 'Gamification';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->library('formloader');
        $this->template->set_template('front_template');
        $this->load->library('formloader');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Message_model');
        $this->load->model('gamification_model');
        $user = $this->session->userdata('current_user');
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (!in_array('52', $menu_list))
        {
            redirect('Forbidden');
        }
        $eventname = $this->Event_model->get_all_event_name();
        if (in_array($this->uri->segment(3) , $eventname))
        {
            if ($user != '')
            {
                $logged_in_user_id = $user[0]->Id;
                $parameters = $this->uri->uri_to_assoc(1);
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                if (!empty($roledata))
                {
                    $roleid = $roledata[0]->Role_id;
                    $eventid = $event_templates[0]['Id'];
                    $rolename = $roledata[0]->Name;
                    $cnt = 0;
                    $req_mod = ucfirst($this->router->fetch_class());
                    $cnt = 1;
                }
                else
                {
                    $cnt = 0;
                }
                if ($cnt == 1)
                {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
                    $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type = $event_templates[0]['Event_type'];
                    if ($event_type == 3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain = $this->uri->segment(3);
                        $acc_name = $this->uri->segment(2);
                        echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
                    }
                }
            }
        }
        else
        {
            $parameters = $this->uri->uri_to_assoc(1);
            redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
        }
    }
    public function index($acc_name=NULL, $Subdomain = NULL, $intFormId = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, '50', $event_templates[0]['Id']);
        }
        $get_game_header = $this->gamification_model->get_game_header($event_templates[0]['Id'], null);
        $this->data['get_game_header'] = $get_game_header;
        $get_all_rank_type = $this->gamification_model->get_all_rank_type($event_templates[0]['Id'], null);
        $this->data['get_all_rank_type'] = $get_all_rank_type;
        $get_all_rank_color_by_event = $this->gamification_model->get_all_rank_color_by_event($event_templates[0]['Id'], null);
        $this->data['get_all_rank_color_by_event'] = $get_all_rank_color_by_event;
        $get_leaderboard_data = $this->gamification_model->get_leaderboard($event_templates[0]['Id'], null);
        $this->data['get_leaderboard_data'] = $get_leaderboard_data;
        /*echo "<pre>";
        print_r($this->data['get_leaderboard_data']);
        exit();*/
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $this->data['sign_form_data'] = $res1;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['menu_id'] = '52';
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, '52');
        $this->data['form_data'] = $res;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => '50',
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            echo '<script>window.location.href="' . base_url() . 'Gamification/' . $acc_name . '/' . $Subdomain . '"</script>';
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Gamification/index', $this->data, true);
                $this->template->write_view('footer', 'Gamification/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Gamification/index', $this->data, true);
                $this->template->write_view('footer', 'Gamification/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('50', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Gamification/index', $this->data, true);
                $this->template->write_view('footer', 'Gamification/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Gamification/index', $this->data, true);
                $this->template->write_view('footer', 'Gamification/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
}