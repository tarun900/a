<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Photos extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Photos';
          $this->data['smalltitle'] = 'Photos';
          $this->data['breadcrumb'] = 'Photos';
          parent::__construct($this->data);
          ini_set('memory_limit', '1024M');
          $this->load->library('formloader');
          $this->load->model('Agenda_model');
          $this->load->library('formloader1');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Notes_admin_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $this->load->model('Photos_model');
          $user = $this->session->userdata('current_user');
          $eventname=$this->Event_model->get_all_event_name();
          if(!in_array($this->uri->segment(3),$eventname))
          {
               $Subdomain=$this->uri->segment(3);
               $flag=1;
               redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);
          }
     }

     public function index($acc_name = NULL,$Subdomain = NULL,$intFormId=NULL)
     {
        if($_GET['action']==1)
        {
			$singupclick['singupclick']='1';
          $this->session->set_userdata($singupclick);
          redirect(base_url()."Photos/".$acc_name."/".$Subdomain);
        }
        if(!$this->input->post())
        {
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('11',$menu_list))
          {
            redirect(base_url().'Forbidden/');
          }
          if ($user != '')
          {
                 $req_mod = $this->router->fetch_class();
                 $menu_id=$this->Event_model->get_menu_id($req_mod);
                 $current_date=date('Y/m/d');
                 $logged_in_user_id=$user[0]->User_id;
                 $this->Event_model->add_view_hit($logged_in_user_id,$current_date,$menu_id,$eventid); 
              
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                 $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                 if ($cnt == 1)
                 {
                      $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                      $this->data['notes_list'] = $notes_list;
                 }
                 else
                 {
                      $event_type=$event_templates[0]['Event_type'];
                      if($event_type==3)
                      {
                          $this->session->unset_userdata('current_user');
                          $this->session->unset_userdata('invalid_cred');
                          $this->session->sess_destroy();
                      }
                      else
                      {
                          $parameters = $this->uri->uri_to_assoc(1);
                          $Subdomain=$this->uri->segment(3);
                          $acc_name=$this->uri->segment(2);
                          redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                      }
                 }
            }
        }
          $user = $this->session->userdata('current_user');
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          for($i=0;$i<count($menu_list);$i++)
          {
                if('Photos'==$menu_list[$i]['pagetitle'])
                {
                     $mid=$menu_list[$i]['id'];
                }
          }
          $this->data['menu_id']=$mid;
          $this->data['menu_list'] = $menu_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
               $this->data['fb_login_data'] = $fb_login_data;
               $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          /* $cms_menu_details = $this->Cms_model->get_cms_page($event_templates[0]['Id'],$menuid);
            $this->data['cms_menu_details'] = $cms_menu_details; */

          $this->data['subdomain'] = $Subdomain;
          $this->data['Subdomain'] = $Subdomain;
          $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
          $this->data['speakers'] = $speakers;

          $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
          $this->data['attendees'] = $attendees;
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $view_chats1 = $this->Photos_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;

          if(!empty($user))
          {
               $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
               $this->data['notify_msg'] = $notifiy_msg;

               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $orid = $this->data['user']->Id;
               $user = $this->session->userdata('current_user');

               $lid = $user[0]->Id;

               if ($this->input->post())
               {
                    $imgdataarray = $this->input->post('unpublished_photo');
                    if (!empty($imgdataarray))
                    {
                         $imag_photos = json_encode($imgdataarray);
                    }
                    else
                    {
                         $imag_photos = NULL;
                    }

                    if ($this->uri->segment(3) != "")
                    {
                         $resiver = $this->uri->segment(3);
                    }
                    else
                    {
                         $resiver = NULL;
                    }

                    $data1 = array(
                            'Message' => $this->input->post('Message'),
                            'Sender_id' => $lid,
                            'Receiver_id' => $resiver,
                            'Parent' => '0',
                            'Time' => date("Y-m-d H:i:s"),
                            'image' => $imag_photos,
                            'ispublic' => $this->input->post('ispublic')
                    );
                    $this->Photos_model->send_speaker_message($data1);
                    $Sid = $this->uri->segment(3);
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Photos_model->view_hangouts_public_msg($dataevents[0]['Id']);
                    $string = '';
                    $user = $this->session->userdata('current_user');
                    $lid = $user[0]->Id;
                    echo '<div style="padding-top: 20px;">
                    <div id="messages">';
                    foreach ($view_chats1 as $key => $value)
                    {
                         echo "<div class='message_container'>";
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "'>";
                         echo "test";
                         echo "</div>";
                         echo "</div>";
                         echo "<div class='msg_main_body'>";
                         echo "<div class='message_img'>";
                         if ($value['Senderlogo'] != "")
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                         }
                         else
                         {
                              echo '<img src="' . base_url() . '/assets/user_files/avatar-1-small1416918047.jpg" >';
                         }

                          echo "</div>";
                          echo "<div class='msg_fromname'>";
                          $t=time().$key;
                          echo '<a href="#" class="tooltip_data">';
                          echo ucfirst($value['Sendername']);
                          echo '</a>';
                          echo "</div>";
                          if (!empty($value['Receiver_id']))
                          {
                              echo "<div class='msg_with'>";
                              echo "with";
                              echo "</div>";
                              echo "<div class='msg_toname'>";
                              $t=time().$key;
                              echo '<a href="#" class="tooltip_data">';
                              echo ucfirst($value['Recivername']);
                              echo '</a>';
                              echo "</div>";
                          }
                          echo "</div>";
                          echo "<div class='msg_date'>";
                          echo $this->get_timeago(strtotime($value['Time']));
                          echo "</div>";
                          $img_data = json_decode($value['image']);
                          foreach ($img_data as $kimg => $valimg)
                          {
                              echo "<div class='msg_photo photos_feed'>";
                              echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '" >';
                              echo "</div>";
                          }
                          echo "<div class='like'><a href='' class='like_btn'><i class='fa fa-thumbs-up'></i>Like</a></div>";
                          echo "<div class='like blue_like'><a href='' class='like_btn blue_btn'><i class='fa fa-thumbs-up'></i>Like</a></div>";
                          echo "<div class='toggle_comment msg_toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                          echo "<div class='comment_panel' id='slidepanel" . $value['Id'] . "'>
                          <form action='' method='post' name='form" . $value['Id'] . "' id='form" . $value['Id'] . "'>
                          <div class='comment_message_img'>";
                          if ($user[0]->Logo!="")
                          {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                          }
                          else
                          {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                          }
                          echo "</div><textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea><br />
                                        <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                                        <input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ");' />
                                   </form>
                                   <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                          if (!empty($view_chats1[$key]['comment']))
                          {
                              $i = 0;
                              $flag = false;
                              $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                              foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                              {

                                   if ($i > 3)
                                   {
                                        $classadded = 'comment_msg_hide';
                                   }
                                   else
                                   {
                                        $classadded = '';
                                   }


                                   echo "<div class='comment_container " . $classadded . "'>  
                                           <div class='comment_message_img'>";
                                   if ($cval['Logo'] != "")
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                                   }
                                   else
                                   {
                                        echo "<img src='" . base_url() . "/assets/user_files/avatar-1-small1416918047.jpg'>";
                                   }

                                   echo "</div>
                                              <div class='comment_text'>
                                             " . $cval['comment'] . "
                                            </div>";
                                   ?>
                                   <?php echo timeAgo(strtotime($cval['Time'])); ?>
                                   <?php echo "</div>";
                                   if ($i > 3 && $flag == false)
                                   {
                                        $flag = true;
                                        echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                                   }
                                   $i++;
                              }
                         }
                         echo "</div>";
                         echo "</div>";
                         echo "</div>";
                    }
                    echo "</div></div>";
                    exit;
               }
            }
            
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          if ($event_templates[0]['Event_type'] == '1')
          {
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                  $this->template->write_view('content', 'Photos/uploadphoto', $this->data, true);
              }
          }
          elseif ($event_templates[0]['Event_type'] == '2')
          {
  
              if (empty($user))
              {
                  $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                 $this->template->write_view('content', 'Photos/uploadphoto', $this->data, true);
              }
          }
          elseif ($event_templates[0]['Event_type'] == '3') 
          {
            $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('11',$event_templates[0]['Id']);
            if($isforcelogin['is_force_login']=='1' && empty($user))
            {
              $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
              if($this->session->userdata('singupclick')=='1')
              {
                $this->session->unset_userdata('singupclick');
                $this->template->write_view('content', 'registration/index', $this->data, true);
              }
              else
              {
                $this->session->unset_userdata('acc_name');
                $acc['acc_name'] =  $acc_name;
                $this->session->set_userdata($acc);
                $this->template->write_view('content', 'Photos/uploadphoto', $this->data, true);
              }
            }
          }
          else
          {
            if (empty($user))
            {
              $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
              $this->template->write_view('content', 'Photos/uploadphoto', $this->data, true);
            }
          }
          $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
          $this->template->render(); 
     }
     public function dislike($acc_name = NULL,$Subdomain = null)
     {
        $user = $this->session->userdata('current_user');
        $post_id=$this->input->post('post_id');
        $this->Photos_model->update_feed_like($post_id,$user[0]->Id);
        $arr_user=$this->Photos_model->get_userid_by_post($post_id);
        $total_like=count($arr_user);
        $a="<a href='#' class='like_btn'  onclick='like(".$post_id.")'><i class='fa fa-thumbs-up'></i>Like</a>";
        $j_data=json_encode(array('total_like'=>$total_like,'post_id'=>$post_id,'status'=>'dislike','html'=>$a));
        echo $j_data;         
     }
     public function like($acc_name = NULL,$Subdomain = null)
     {
        $user = $this->session->userdata('current_user');
        $post_id=$this->input->post('post_id');
        $this->Photos_model->update_feed_like($post_id,$user[0]->Id);
        $arr_user=$this->Photos_model->get_userid_by_post($post_id);
        $total_like=count($arr_user);
        $a='<a href="#"  class="like_btn blue_btn" onclick="unlike('.$post_id.')"><i class="fa fa-thumbs-up"></i>Like</a>';
        $j_data=json_encode(array('total_like'=>$total_like,'post_id'=>$post_id,'status'=>'dislike','html'=>$a));
        echo $j_data;
     }
     public function chatspublic($acc_name = NULL,$Subdomain = null)
     {
          $this->load->model('User_model');
          $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);

          $view_chats1 = $this->Photos_model->view_hangouts_public_msg($dataevents[0]['Id']);
          $this->data['view_chats1'] = $view_chats1;

          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_photo');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               if ($this->input->post('Receiver_id') != "")
               {
                    $resiver = $this->input->post('Receiver_id');
               }
               else
               {
                    $resiver = NULL;
               }

               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $resiver,
                       'Parent' => '0',
                       'Event_id' => $dataevents[0]['Id'],
                       'Time' => date("Y-m-d H:i:s"),
                       'image' => $imag_photos,
                       'ispublic' => $this->input->post('ispublic')
               );

               $current_date=date('Y/m/d');
               foreach ($resiver as $key => $value) {
                    $this->Photos_model->add_msg_hit($lid,$current_date,$value); 
               }
               $this->Photos_model->send_grp_speaker_message($data1);
               if ($this->input->post('ispublic') == '1')
               {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    $view_chats1 = $this->Photos_model->view_hangouts_public_msg($dataevents[0]['Id']);
               }
               else
               {
                  if(!empty($resiver))
                  {
                      $strRec = @implode(',',$resiver);
                      if(!empty($strRec))
                      {
                          $arrUser = $this->User_model->getUserFromIds($strRec);
                          if(!empty($arrUser))
                          {
                              foreach($arrUser as $intKey=>$strValue)
                              {
                                  $strMsg = $this->input->post('Message');
                                  $strMsg .= "<br/>";
                                  $strMsg .= "Event Link : ".  base_url()."Events/".$Subdomain;
                                  $this->email->from($user[0]->Email, $user[0]->Firstname);
                                  $this->email->to($strValue['Email']);
                                  $this->email->subject("Private Message");
                                  $this->email->set_mailtype("html");
                                  $this->email->message($strMsg);    
                                  $this->email->send();
                              }
                          }
                      }
                  }
                  $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                  $view_chats1 = $this->Photos_model->view_hangouts_private_msg($dataevents[0]['Id']);
               }

               $string = '';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;
               echo '<div style="padding-top: 20px;">
                  <div id="messages">';
               if ($this->input->post('ispublic') == '1')
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Feeds <br/></h3>';
               }
               else
               {
                    echo '<h3 style="margin-bottom: 20px;clear: left;display: block;">Private Message <br/></h3>';
               }

               foreach ($view_chats1 as $key => $value)
               {
                    echo "<div class='message_container'>";

                    if ($value['Sender_id'] == $user[0]->Id)
                    {
                         echo "<div class='msg_edit-view-box'>";
                         echo "<div class='msg_edit-arrow' data-id='" . $value['Id'] . "'>";
                         echo "</div>";
                         echo "<div class='msg_edit-view' id='msg_edit-view" . $value['Id'] . "' onclick='removemsg(" . $value['Id'] . ",this);'>";
                         echo "Delete";
                         echo "</div>";
                         echo "</div>";
                    }

                    echo "<div class='msg_main_body'>";
                    echo "<div class='message_img'>";
                    if ($value['Senderlogo'] != "")
                    {
                         echo '<img src="' . base_url() . '/assets/user_files/' . $value['Senderlogo'] . '" >';
                    }
                    else
                    {
                         echo '<img src="' . base_url() . '/assets/images/anonymous.jpg" >';
                    }

                    echo "</div>";
                    echo "<div class='msg_fromname'>";
                    $t=time().$key;
                    echo '<a href="#" class="tooltip_data">';
                    echo ucfirst($value['Sendername']);
                    echo '</a>';
                    echo "</div>";
                    if (!empty($value['Receiver_id']))
                    {
                         echo "<div class='msg_with'>";
                         echo "with";
                         echo "</div>";
                         echo "<div class='msg_toname'>";
                         $t=time().$key;
                         echo '<a href="#" class="tooltip_data">';
                         echo ucfirst($value['Recivername']);
                         echo '</a>';                                               
                         echo "</div>";
                    }
                    echo "</div>";
                    echo "<div class='msg_date'>";
                    echo $this->get_timeago(strtotime($value['Time']));
                    echo "</div>";

                    $img_data = json_decode($value['image']);
                    foreach ($img_data as $kimg => $valimg)
                    {
                         echo "<div class='msg_photo photos_feed'>";
                         echo '<a class="colorbox_' . $value['Id'] . '" href="' . base_url() . 'assets/user_files/' . $valimg . '">';
                         echo '<img src="' . base_url() . 'assets/user_files/' . $valimg . '"  class="' . $value['Id'] . '" >';
                         echo "</a>";
                         echo "</div>";
                    }
                    
                    if($this->input->post('ispublic') == '1')
                    {    
                      $arr=explode(',',$value['user_id']);                  
                      $like=0;
                      $like=count(array_filter($arr));
                      $str="like_".$value['Id'];
                      echo "<div style='clear:both'>";
                      if(in_array($user[0]->Id, $arr))
                      {
                          echo "<div class='like blue_like' id='".$str."'><a href='#'  class='like_btn blue_btn' onclick='unlike(".$value['Sender_id'].",".$value['Id'].",".$str.")' ><i class='fa fa-thumbs-up'></i>Like</a></div>";
                      }
                      else
                      {
                          echo "<div class='like' id='".$str."'><a href='#' class='like_btn'  onclick='like(".$value['Sender_id'].",".$value['Id'].",".$str.")' ><i class='fa fa-thumbs-up'></i>Like</a></div>";
                      }
                      echo "</div>";
                         $post_id=$value['Id'];
                        echo "<div id='".$post_id."'>";
                        echo "&nbsp;comments   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$like." people like this";
                        echo "<span style='float:left;padding:0px;'>".count($view_chats1[$key]['comment'])."  </span>";
                        echo "</div>";
                        echo "<div class='toggle_comment msg_toggle_comment'><a href='javascript: void(0);' class='comment_btn' id='" . $value['Id'] . "'>Comment</a></div>";
                    }
                    else
                    {
                          echo "<div class='toggle_comment'><a href='#' class='comment_btn' id='" . $value['Id'] . "'>Reply</a></div>";
                    }
                    
                    echo "<div class='comment_panel clearfix' id='slidepanel" . $value['Id'] . "'>
                                   <form method='post' name='commentform" . $value['Id'] . "' id='commentform" . $value['Id'] . "' enctype='multipart/form-data' action='" . base_url() . "Speakers/" . $acc_name."/".$Subdomain . "/upload_commentimag/" . $post_id . "'>
                                        <div class='comment_message_img'>";
                    if ($user[0]->Logo!="")
                    {
                         echo "<img src='" . base_url() . "/assets/user_files/" . $user[0]->Logo . "'>";
                    }
                    else
                    {
                         echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                    }
                    echo "</div>";
 
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Comment' name='comment'></textarea>";
                    }
                    else
                    {
                         echo "<textarea class='user_comment_input' id='comment_text" . $value['Id'] . "' placeholder='Reply' name='comment'></textarea>";
                    }
                    
                    
                    
                    echo "<div class='photo_view_icon'>     
                    <div class='camera-icon' id='camera_icon_comment' onclick='javascript: jQuery(\"#cmphto" . $value['Id'] . "\").click();' >&nbsp;</div>
                    <input type='file' capture accept='image/*' class='comment_photoviewer' style='display: none;' name='cmphto[]' id='cmphto" . $value['Id'] . "' onchange='comment_photo(" . $value['Id'] . ")'></div>
                    <input type='hidden' name='msg_id' value='" . $value['Id'] . "'>
                    <ul id='cpreview" . $value['Id'] . "' class='cpreview clearfix'>
                    </ul>
                    <div id='imageloadstatus' style='display:none'><img src='" . base_url() . "assets/images/loading.gif' alt='Uploading....'/></div>";
                    if($this->input->post('ispublic') == '1')
                    {
                           echo "<input type='button' value='Comment'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                    }
                    else
                    {
                          echo "<input type='button' value='Reply'  class='comment_submit' onclick='addcomment(" . $value['Id'] . ",1);' />";
                    }
                                             
                    echo "</form>
                    <div class='comment_data' id='comment_conten" . $value['Id'] . "'>";

                    if (!empty($view_chats1[$key]['comment']))
                    {
                         $view_chats1[$key]['comment'] = array_reverse($view_chats1[$key]['comment']);
                         $i = 0;
                         $flag = false;
                         foreach ($view_chats1[$key]['comment'] as $ckey => $cval)
                         {
                              if ($i > 3)
                              {
                                   $classadded = 'comment_msg_hide';
                              }
                              else
                              {
                                   $classadded = '';
                              }

                              echo "<div class='comment_container " . $classadded . "'><div class='comment_message_img'>";
                              if ($cval['Logo'] != "")
                              {
                                   echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                              }
                              else
                              {
                                   echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                              }
                              echo "</div>
                                        <div class='comment_wrapper'>        
                                        <div class='comment_username'>
                                             " . ucfirst($cval['user_name']) . "
                                        </div>
                                        <div class='comment_text'>
                                             " . $cval['comment'] . "
                                        </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

                              if ($cval['image'] != "")
                              {
                                   $image_comment = json_decode($cval['image']);
                                   echo "<div class='msg_photo photos_feed'>";
                                   echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                                   echo "<img src='" . base_url() . "/assets/user_files/thumbnail/" . $image_comment[0] . "'>";
                                   echo "</a>";
                                   echo "</div>";
                              }
                              if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                              {
                                   echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$post_id.",this)'>&nbsp;</button>";
                              }
                              echo "</div>";


                              if ($i > 3 && $flag == false)
                              {
                                   $flag = true;
                                   echo"<div id='comment_viewmore" . $value['Id'] . "' class='comment_viewmore' onclick='viewmore_comment(" . $value['Id'] . ")'>View more comments</div>";
                              }

                              $i++;
                         }
                    }
                    echo "</div>
                              </div>";
                    echo "</div>";
               }
               echo "</div></div>";
               exit;
          }
     }
     public function commentaddpublic($acc_name = NULL,$Subdomain = NULL, $flag = NULL)
     {
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;
          if ($this->input->post())
          {
               $imgdataarray = $this->input->post('unpublished_commentphoto');
               if (!empty($imgdataarray))
               {
                    $imag_photos = json_encode($imgdataarray);
               }
               else
               {
                    $imag_photos = NULL;
               }

               $array_add['comment'] = trim($this->input->post('comment'));
               $array_add['msg_id'] = $this->input->post('msg_id');
               $array_add['image'] = $imag_photos;
               $array_add['user_id'] = $lid;
               $array_add['Time'] = date("Y-m-d H:i:s");
               $time=$array_add['Time'];
               $msg_id=$this->input->post('msg_id');
               $sender_id=$this->Photos_model->get_senderid($msg_id);
               $current_date=date('Y/m/d');
          
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);     
               $comment_id = $this->Photos_model->add_comment($dataevents[0]['Id'],$array_add);
              
               if ($flag == 1)
               {    
                    $view_chats1 = $this->Photos_model->view_hangouts_public_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }
               else
               {
                    $view_chats1 = $this->Photos_model->view_hangouts_private_msg($dataevents[0]['Id'], 0, 10, $this->input->post('msg_id'));
               }
               if(count($view_chats1) > 1)
               {
                  for ($j=0; $j <=count($view_chats1); $j++) { 
                      if($view_chats1[$j]['Id']==$this->input->post('msg_id'))
                      {
                        $view_comm[0]=$view_chats1[$j];  
                      }
                  }
               }

               if (!empty($view_comm[0]['comment']))
               {
                    $i = 0;
                    $flag = false;
                    $view_comm[0]['comment'] = array_reverse($view_comm[0]['comment']);
                    foreach ($view_comm[0]['comment'] as $ckey => $cval)
                    {
                         if ($i > 3)
                         {
                              $classadded = 'comment_msg_hide';
                         }
                         else
                         {
                              $classadded = '';
                         }

                         echo "<div class='comment_container " . $classadded . "'>  
                         <div class='comment_message_img'>";
                         if ($cval['Logo'] != "")
                         {
                              echo "<img src='" . base_url() . "/assets/user_files/" . $cval['Logo'] . "'>";
                         }
                         else
                         {
                              echo "<img src='" . base_url() . "/assets/images/anonymous.jpg'>";
                         }

                         echo "</div>
                         <div class='comment_wrapper'>        
                         <div class='comment_username'>
                              " . ucfirst($cval['user_name']) . "
                         </div>
                         <div class='comment_text'>
                              " . $cval['comment'] . "
                         </div>" . $this->get_timeago(strtotime($cval['Time'])) . "</div>";

                         if ($cval['image'] != "")
                         {
                              $image_comment = json_decode($cval['image']);
                              echo "<div class='msg_photo photos_feed'>";
                              echo '<a class="colorbox_C_' . $cval['id'] . '" href="' . base_url() . 'assets/user_files/' . $image_comment[0] . '">';
                              echo "<img src='" . base_url() . "assets/user_files/" . $image_comment[0] . "'>";
                              echo "</a>";
                              echo "</div>";
                         }
                         if ($value['Sender_id'] == $user[0]->Id || $cval['user_id'] == $user[0]->Id)
                         {
                              echo "<button class='comment_section_btn' type='button' title='' onclick='removecomment(" . $cval['id'] . ",".$msg_id.",this)'>&nbsp;</button>";
                         }
                         echo "</div>";

                         if ($i > 3 && $flag == false)
                         {
                              $flag = true;
                              echo"<div id='comment_viewmore" . $this->input->post('msg_id') . "' class='comment_viewmore' onclick='viewmore_comment(" . $this->input->post('msg_id') . ")'>View more comments</div>";
                         }
                         $i++;
                    }
               }
          }
          exit;
     }
     function get_timeago($ptime = NULL)
     {
          $estimate_time = time() - $ptime;

          if ($estimate_time < 1)
          {
               return '1 second ago';
          }

          $condition = array(
                  12 * 30 * 24 * 60 * 60 => 'year',
                  30 * 24 * 60 * 60 => 'month',
                  24 * 60 * 60 => 'day',
                  60 * 60 => 'hour',
                  60 => 'minute',
                  1 => 'second'
          );

          foreach ($condition as $secs => $str)
          {
               $d = $estimate_time / $secs;

               if ($d >= 1)
               {
                    $r = round($d);
                    return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
               }
          }
     }

     public function delete_message($acc_name = NULL,$Subdomain = NULL,$id = NULL)
     {
          echo "333";
          $chats = $this->Photos_model->delete_message($id);
          exit;
     }

     public function delete_comment($acc_name = NULL,$Subdomain = NULL,$id = NULL)
     {
          echo "333";
          $chats = $this->Photos_model->delete_comment($id);
          exit;
     }

}