<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class QA extends CI_Controller
{
  function __construct()
  {
    $this->data['pagetitle'] = 'Q&A';
    $this->data['smalltitle'] = 'Q&A';
    $this->data['breadcrumb'] = 'Q&A';
    parent::__construct($this->data);
    $this->load->library('formloader');
    $this->load->library('formloader1');
    $this->load->model('Agenda_model');
    $this->template->set_template('front_template');
    $this->load->model('Event_template_model');
    $this->load->model('Cms_model');
    $this->load->model('Event_model');
    $this->load->model('Setting_model');
    $this->load->model('Speaker_model');
    $this->load->model('Notes_admin_model');
    $this->load->model('Profile_model');
    $this->load->model('Message_model');
    $this->load->model('Qa_model');
    $user = $this->session->userdata('current_user');
    $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
    $eventid = $event_val[0]['Id'];
    $event = $this->Event_model->get_module_event($eventid);
    $menu_list = explode(',', $event[0]['checkbox_values']);
    if(!in_array('50',$menu_list))
    {
      redirect('Forbidden');
    }
    $eventname=$this->Event_model->get_all_event_name();
    if(in_array($this->uri->segment(3),$eventname))
    {
      if ($user != '')
      {
        $logged_in_user_id=$user[0]->Id;
        $parameters = $this->uri->uri_to_assoc(1);
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
        if(!empty($roledata))
        {
          $roleid = $roledata[0]->Role_id;
          $eventid = $event_templates[0]['Id'];
          $rolename = $roledata[0]->Name;
          $cnt = 0;
          $req_mod = ucfirst($this->router->fetch_class());
          $cnt = 1;
        }
        else
        {
          $cnt=0;
        }

        if ($cnt == 1)
        {
          $notes_list = $this->Event_template_model->get_notes($this->uri->segment(2));
          $this->data['notes_list'] = $notes_list;
        }
        else
        {
          $event_type=$event_templates[0]['Event_type'];
          if($event_type==3)
          {
            $this->session->unset_userdata('current_user');
            $this->session->unset_userdata('invalid_cred');
            $this->session->sess_destroy();
          }
          else
          {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain=$this->uri->segment(3);
            $acc_name=$this->uri->segment(2);
            echo '<script>window.location.href="'.base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain.'"</script>';
          }
        }
      }
    }
    else
    {
      $parameters = $this->uri->uri_to_assoc(1);
      redirect('Pageaccess/' . $parameters[$this->data['pagetitle']]);
    }
  }

  public function index($acc_name = NULL,$Subdomain = NULL,$intFormId=NULL)
  {
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $user = $this->session->userdata('current_user');
    if(!empty($user[0]->Id))
    {
      $current_date=date('Y/m/d');
      $this->Event_model->add_view_hit($user[0]->Id,$current_date,'50',$event_templates[0]['Id']);
    }

    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],null);
    $this->data['qa_session']=$qa_session;

    $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
    $this->data['notisetting']=$notificationsetting;

    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
    $this->data['fb_login_data'] = $fb_login_data;
    $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);

    $this->data['sign_form_data']=$res1;

    $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
    $this->data['advertisement_images'] = $advertisement_images;

    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;

    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;

    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;


    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;

    $this->data['Subdomain'] = $Subdomain;

    for($i=0;$i<count($menu_list);$i++)
    {
      if('QA'==$menu_list[$i]['pagetitle'])
      {
        $mid=$menu_list[$i]['id'];
      }
    }
    $this->data['menu_id']=$mid;
    $eid=$event_templates[0]['Id'];
    $res=$this->Agenda_model->getforms($eid,$mid);
    $this->data['form_data']=$res;

    $array_temp_past = $this->input->post();
    if(!empty($array_temp_past))
    {
      $aj=json_encode($this->input->post());
      $formdata=array('f_id' =>$intFormId,
      'm_id'=>$mid,
      'user_id'=>$user[0]->Id,
      'event_id'=>$eid,
      'json_submit_data'=>$aj
      );
      $this->Agenda_model->formsinsert($formdata);
      echo '<script>window.location.href="'.base_url().'QA/'.$acc_name.'/'.$Subdomain.'"</script>';
      exit;
    }

    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] =  $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('50',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/index', $this->data, true);
        $this->template->write_view('footer', 'QA/footer', $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render(); 
  }
  public function view($acc_name = NULL,$Subdomain = NULL,$sid = NULL)
  {
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;

    
    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],$sid);
    $this->data['qa_session']=$qa_session;

    $show_on_screen = $this->Qa_model->getQaSettingShowOnScreen($event_templates[0]['Id']);
    if($show_on_screen == '1')
    {
        $view = $qa_session[0]['is_closed_qa']?'viewsession_new':'viewsession';
        $qa_session_question=$this->Qa_model->get_all_qa_session_question_for_show_on_screen($event_templates[0]['Id'],$sid);
    }
    else
    {
        $qa_session_question=$this->Qa_model->get_all_qa_session_question_by_qa_session_id($event_templates[0]['Id'],$sid);
        $view = 'viewsession';
    }
    $this->data['qa_session_question']=$qa_session_question;

    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $this->data['Subdomain'] = $Subdomain;
    $this->data['sid']=$sid;
    $user = $this->session->userdata('current_user');
    $roleid = $user[0]->Role_id;
    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    if ($this->input->is_ajax_request())
    {   
      if($qa_session[0]['is_closed_qa'] == '1' && $show_on_screen == '1'):
        if(empty($qa_session_question)):?>
          <div class="message_container col-xs-12 col-sm-12 col-md-12 col-lg-12" id="message_container__<?php echo $value['message_id']; ?>" data-sort="<?php echo $value['votes']?: '0'; ?>">
        <div class="msg_main_body_wrap">
          <div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
            <a class="sp-full-screen-button1 sp-fade-full-screen1" style="cursor: pointer;margin-right: 10px;" onclick="test();"><i class="fa fa-expand fa-2x" style="margin-top: 10px;"></i></a>
          </div>
        <div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
            <h3 class="qa_session_name" style="color:<?=$event_templates[0]['Top_text_color']?>;font-size: 60px;"><?php echo ucfirst($qa_session[0]["Session_name"]); ?></h3>
          </div>
        </div> 
      </div>
        <?php   
        endif;
      foreach ($qa_session_question as $key => $value) { ?>
      <div class="message_container col-xs-12 col-sm-12 col-md-12 col-lg-12" id="message_container__<?php echo $value['message_id']; ?>" data-sort="<?php echo $value['votes']?: '0'; ?>">
        <div class="msg_main_body_wrap">
        <div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
          <a class="sp-full-screen-button1 sp-fade-full-screen1" style="cursor: pointer;margin-right: 20px;" onclick="test();"><i class="fa fa-expand fa-2x" style="margin-top: 10px;"></i></a>
        </div>
        <div class="qa_session_dark_grey_box col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="background-color:<?=$event_templates[0]['Top_background_color']?>;">
            <h3 class="qa_session_name" style="color:<?=$event_templates[0]['Top_text_color']?>;font-size: 60px;"><?php echo ucfirst($qa_session[0]["Session_name"]); ?></h3>
          </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="msg_main_body" style="font-size: 35px;">
            <div class="message_img">
              <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?> 
              <img style="" src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
              <?php }else{ ?>
              <img style="" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
              <?php } ?>
            </div>
            <div class="msg_fromname">
                <h1><?=ucfirst($value['user_name']); ?></h1>
                <h2><?=ucfirst($value['Title'])?></h2>
                <h2><?=ucfirst($value['Company_name'])?></h2>
            </div>
          </div>
          <div class="msg_message" style="font-size: 60px;">
            <?php echo $value['Message']; ?>
          </div>
        </div>
        </div>
        
      </div>
    <?php } 
    else:
    foreach ($qa_session_question as $key => $value) { ?>
      <div class="message_container col-xs-12 col-sm-12 col-md-12 col-lg-12" id="message_container__<?php echo $value['message_id']; ?>" data-sort="<?php echo $value['votes']?: '0'; ?>">
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
          <div class="msg_main_body" style="font-size: 35px;">
            <div class="message_img">
              <?php if(!empty($value['Logo']) && file_exists('./assets/user_files/'.$value['Logo'])){ ?> 
              <img style="" src="<?php echo base_url().'assets/user_files/'.$value['Logo']; ?>">
              <?php }else{ ?>
              <img style="" src="<?php echo base_url().'assets/images/anonymous.jpg'; ?>">
              <?php } ?>
            </div>
            <div class="msg_fromname">
              <a style="font-size: 35px;" href="javascript:void(0);"><?php echo ucfirst($value['user_name']); ?></a>
            </div>
          </div>
          <div class="msg_message" style="font-size: 35px;">
            <?php echo $value['Message']; ?>
          </div>
        </div>
        
      </div>
    <?php }
    endif; 
    exit;
    }
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    if ($event_templates[0]['Event_type'] == '1')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/'.$view, $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '2')
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/index', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/'.$view, $this->data, true);
      }
    }
    elseif ($event_templates[0]['Event_type'] == '3') 
    {
      $this->session->unset_userdata('acc_name');
      $acc['acc_name'] =  $acc_name;
      $this->session->set_userdata($acc);
      $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('50',$event_templates[0]['Id']);
      if($isforcelogin['is_force_login']=='1' && empty($user))
      {
        $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/'.$view, $this->data, true);
      }
    }
    else
    {
      if (empty($user))
      {
        $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
      }
      else
      {
        $this->template->write_view('content', 'QA/'.$view, $this->data, true);
      }
    }
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->render();
  }
  public function send_message($acc_name = NULL,$Subdomain = NULL,$sid = NULL)
  {
    $user = $this->session->userdata('current_user');
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $qa_session=$this->Qa_model->get_qa_session_by_event_frontend($event_templates[0]['Id'],$sid);
    $data['qa_session_id']=$sid;
    if($this->input->post('anonymous_user')!='1' && !empty($user[0]->Id))
    {
      $sender_id=$user[0]->Id;
    }
    else
    {
      $sender_id=NULL;
    }
    $modertor_id=$this->Event_model->get_user_all_moderator($event_templates[0]['Id'],$qa_session[0]['Moderator_speaker_id']);
    if(count($modertor_id) > 0)
    {
      $mids=array_column_1($modertor_id,'moderator_id');
    }
    else
    {
      $mids=array($qa_session[0]['Moderator_speaker_id']);
    }
    $resiver=array_unique($mids);
    foreach ($resiver as $key => $value) 
    {
      $data1 = array(
        'Message' => $this->input->post('Message'),
        'Sender_id' => $sender_id,
        'Receiver_id' => $value,
        'Parent' => '0',
        'Event_id' => $event_templates[0]['Id'],
        'Time' => date("Y-m-d H:i:s"),
        'image' => NULL,
        'org_msg_receiver_id'=>$qa_session[0]['Moderator_speaker_id'],
        'ispublic' => '0',
        'qasession_id' => $sid
      );
      $this->Message_model->send_speaker_message($data1);
    }
    echo "success";die;
  }
}
