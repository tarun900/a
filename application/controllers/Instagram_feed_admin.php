<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Instagram_feed_admin extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Instagram Feed';
        $this->data['smalltitle'] = 'Show your Instagram Feed within your App';
        $this->data['breadcrumb'] = 'Instagram Feed';
        $this->data['page_edit_title'] = 'Instagram Feed';
        parent::__construct($this->data);
        $this->load->model('Event_model');
        $this->load->model('Agenda_model');
        $eventid = $this->uri->segment(3);
        $user = $this->session->userdata('current_user');
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('46', $module))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            $cnt = $this->Agenda_model->check_auth("instagram_feed", $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1 && in_array('46', $menu_list))
        {
            $this->load->model('Agenda_model');
            $this->load->model('Setting_model');
            $this->load->model('Map_model');
            $this->load->model('Speaker_model');
            $this->load->model('Event_template_model');
            $this->load->model('Profile_model');
            $this->load->library('upload');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="' . base_url() . 'forbidden/' . '"</script>';
        }
    }
    public function index($id=NULL)

    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 46);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'instagram_feed_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'instagram_feed_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'instagram_feed_admin/js', true);
        $this->template->render();
    }
    public function save_access_token($id=NULL)

    {
        $data['event_array']['Id'] = $id;
        $data['event_array']['insta_access_token'] = $this->input->post('insta_access_token');
        $this->Event_model->update_admin_event($data);
        $this->session->set_flashdata('feed_data', 'Saved');
        redirect(base_url() . "Instagram_feed_admin/index/" . $id);
    }
}
?>