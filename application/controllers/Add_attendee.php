<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Add_attendee extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'User';
        $this->data['smalltitle'] = 'User Details';
        $this->data['page_edit_title'] = 'user';
        $this->data['breadcrumb'] = 'User';
        parent::__construct($this->data);
        $this->load->model('Add_Attendee_model');
        $this->load->model('Setting_model');
        $this->load->model('Profile_model');
        $this->load->model('Event_template_model');
        $this->load->library('session');
    }
    public function index()

    {
        $users = $this->User_model->get_user_list();
        $this->data['users'] = $users;
        // print_r($users); exit();
        $this->template->write_view('css', 'user/css', $this->data, true);
        $this->template->write_view('content', 'user/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'user/js', $this->data, true);
        $this->template->render();
    }
    public function edit($id = '0')

    {
        $this->data['users'] = $this->User_model->get_user_list($id);
        $this->template->write_view('css', 'user/add_css', $this->data, true);
        $this->template->write_view('content', 'user/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'user/add_js', $this->data, true);
        $this->template->render();
    }
    public function add()

    {
        if ($this->input->post())
        {
            if (isset($_FILES) && $_FILES['userfile']['name'] != NULL && $_FILES['userfile']['name'] != '')
            {
                $tempname = explode('.', $_FILES['userfile']['name']);
                $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                $_FILES['userfile']['name'] = $tempname[0] . "." . $tempname[1];
                $_POST['Logo'] = $_FILES['userfile']['name'];
                if ($this->data['user']->Role_name == 'User')
                {
                    $this->data['user']->Logo = $_FILES['userfile']['name'];
                }
                $config['upload_path'] = './assets/user_files';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['max_width'] = '2048';
                $config['max_height'] = '2048';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload())
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                }
                else
                {
                    $data1 = array(
                        'upload_data' => $this->upload->data()
                    );
                }
            }
            foreach($this->input->post() as $k => $v)
            {
                if ($k != "password_again" && $k != "idval")
                {
                    if ($k == "zipcode")
                    {
                        $k = "Postcode";
                    }
                    else if ($k == "Permissions")
                    {
                        $v = implode(',', $v);
                    }
                    $k = ucfirst(strtolower($k));
                    $array_add[$k] = $v;
                }
            }
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $slug = "WELCOME_MSG";
            $em_template = $this->Setting_model->back_email_template($slug);
            $em_template = $em_template[0];
            $new_pass = $this->input->post('password');
            $name = ($this->input->post('Firstname') == NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
            $msg = html_entity_decode($em_template['Content']);
            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $patterns[2] = '/{{email}}/';
            $patterns[3] = '/{{link}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $new_pass;
            $replacements[2] = $this->input->post('email');
            /*$replacements[3] = base_url();*/
            $msg = preg_replace($patterns, $replacements, $msg);
            $msg.= "Email: " . $this->input->post('email') . "<br/>";
            $msg.= "Login Link: " . base_url() . "<br/>";
            $msg.= "Password: " . $new_pass;
            if ($em_template['Subject'] == "")
            {
                $subject = "WELCOME_MSG";
            }
            else
            {
                $subject = $em_template['Subject'];
            }
            $strFrom = EVENT_EMAIL;
            if ($em_template['From'] != "")
            {
                $strFrom = $em_template['From'];
            }
            $this->email->from($strFrom, 'Event Management');
            $this->email->to($this->input->post('email'));
            $this->email->subject($subject); //'User Account'
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            // ///////////////////////////////////////////////////email//////////////////////////////////////////////////////////
            $user = $this->Add_Attendee_model->add_user($array_add);
            $this->session->set_flashdata('user_data', 'Added');
            redirect("Dashboard");
        }
        $statelist = $this->Profile_model->statelist();
        $this->data['Statelist'] = $statelist;
        $this->template->write_view('css', 'user/add_css', $this->data, true);
        $this->template->write_view('content', 'user/add', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'user/add_js', $this->data, true);
        $this->template->render();
    }
    public function delete($id=NULL)

    {
        $user = $this->User_model->delete_user($id);
        $this->session->set_flashdata('user_data', 'Deleted');
        redirect("User");
    }
}