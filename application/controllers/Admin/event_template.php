<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event_template extends FrontendController {
	function __construct() {
		$this->data['pagetitle'] = 'View Your Apps';
		$this->data['smalltitle'] = 'View Your Apps Details';
		$this->data['breadcrumb'] = 'View Your Apps';
		parent::__construct($this->data);
		$this->load->model('event_template_model');
        $this->load->model('setting_model');
        $this->load->model('profile_model');
        $this->load->model('event_model');
	}

	public function index()                 
	{       

		$event_templates = $this->event_template_model->get_event_template_list();
	    $this->data['event_templates'] = $event_templates;

	    $event = $this->event_model->get_event_list();
        $this->data['Event'] = $event;

        $archive_event = $this->event_model->get_archive_event_list();
        $this->data['Archive_event'] = $archive_event;

        $feature_event = $this->event_model->get_feature_event_list();
        $this->data['Feature_event'] = $feature_event;
	    
		$this->template->write_view('css', 'event_template/css', $this->data , true);
		$this->template->write_view('content', 'admin/event_template/index', $this->data , true);
		$this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
		$this->template->write_view('js', 'event_template/js', $this->data , true);
		$this->template->render();
	}

    public function view_template($Subdomain=NULL)
    {
    	
	    	$this->load->model('event_template_model');
	        $this->load->model('setting_model');
	        $this->load->model('profile_model');
	        $event_templates = $this->event_template_model->get_event_template_by_id_list($Subdomain);
	        $this->data['event_templates'] = $event_templates;

	        $this->template->write_view('css', 'events/css', $this->data , true);
	        $this->template->write_view('content', 'admin/events/index', $this->data , true);
	        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
	        $this->template->write_view('js', 'events/js', $this->data , true);
	        $this->template->render();
    }
    
}
