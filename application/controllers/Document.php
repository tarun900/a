<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Document extends FrontendController

{
     function __construct()
     {
          $this->data['pagetitle'] = 'Documents & Folders';
          $this->data['smalltitle'] = 'Add PDFs, Word Documents, Spreadsheets and Text Files into the app.';
          $this->data['breadcrumb'] = 'Documents & Folders';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $user = $this->session->userdata('current_user');
          $eventid = $this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
          $user_events = array_filter(array_column_1($user_events, 'Event_id'));
          if (!in_array($eventid, $user_events))
          {
               redirect('Forbidden');
          }
          $eventmodule = $this->Event_model->geteventmodulues($eventid);
          $module = json_decode($eventmodule[0]['module_list']);
          if (!in_array('16', $module))
          {
               redirect('Forbidden');
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          $roledata = $this->Event_model->getUserRole($eventid);
          if (!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               // $eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Documents & Folders")
               {
                    $title = "Documents";
               }
               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
          }
          else
          {
               $cnt = 0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          if ($cnt == 1 && in_array('16', $menu_list))
          {
               $this->load->model('Document_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               /* $this->load->model('Doccategory_model');*/
               /* $this->load->model('docicon_model');*/
               $this->load->model('Event_template_model');
               $this->load->library('upload');
               $this->load->library('image_lib');
               $roles = $this->Event_model->get_menu_list($roleid, $eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               redirect('Forbidden');
          }
     }
     public function index($id=NULL)

     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
          $this->data['users_role'] = $user_role;
          $event_id = $user[0]->event_id;
          $org = $user[0]->Organisor_id;
          $this->data['org'] = $org;
          /*$docscategory = $this->Doccategory_model->get_doccategory_list(null, $id);
          $this->data['docscategory'] = $docscategory;*/
          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $documents = $this->Document_model->get_document_list($id);
          $this->data['Document'] = $documents;
          $folder_list = $this->Document_model->get_folder_list_event($id);
          $this->data['folder_list'] = $folder_list;
          $Document_files = $this->Document_model->get_document_foldername_list($id);
          $this->data['Document_files'] = $Document_files;
          $menudata = $this->Event_model->geteventmenu($id, 16);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;
          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
          /*$coverimages=$this->Doccategory_model->getcoverimage($id,0);
          $this->data['coverimages'] =$coverimages;*/
          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'document/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'document/js', $this->data, true);
          $this->template->render();
     }
     public function edit($eventid=NULL, $id = '0')

     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          /*$doccategory = $this->Doccategory_model->get_doccategory_list(null, $eventid, '1');
          $this->data['doccategory'] = $doccategory;*/
          $documents = $this->Document_model->get_document_list($eventid, $id);
          $this->data['Document'] = $documents;
          $docfolder = $this->Document_model->get_docfolder_list($eventid, $id);
          $this->data['docfolder'] = $docfolder;
          $documents_files = $this->Document_model->get_document_file_list($id);
          foreach($documents_files as $key => $value)
          {
               if (empty($value['icon']))
               {
                    $exten = explode('.', $value['document_file']);
                    /*$iconname = $this->docicon_model->get_docicon($exten[1]);*/
                    if (!empty($iconname))
                    {
                         $documents_files[$key]['icon'] = $iconname[0]['image'];
                    }
                    else
                    {
                         $documents_files[$key]['icon'] = NULL;
                    }
               }
          }
          $this->data['Document_files'] = $documents_files;
          $this->data['event_id'] = $eventid;
          if ($this->input->post())
          {
               if (!empty($_FILES['userfile']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['userfile']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $_POST['docicon'] = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['coverimages']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['coverimages']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $_POST['coverimages'] = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '100000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("coverimages"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document Cover images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['document']['name'][0]))
               {
                    foreach($_FILES['document']['name'] as $k => $v)
                    {
                         if (!empty($v))
                         {
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $v = str_replace(' ', '', $v);
                              $Documents[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                    }
                    $this->upload->initialize(array(
                         "file_name" => $Documents,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => '*'
                    ));
                    if (!$this->upload->do_multi_upload("document"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document not allow " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['customeicon']['name']))
               {
                    $upload = array();
                    foreach($_FILES['customeicon']['name'] as $k => $v)
                    {
                         if (!empty($v))
                         {
                              $imgesize = getimagesize($_FILES['customeicon']['tmp_name'][$k]);
                              if ($imgesize[0] < 300 || $imgesize[1] < 200)
                              {
                                   $upload[] = 1;
                              }
                              else
                              {
                                   $upload[] = 0;
                              }
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $v = str_replace(' ', '', $v);
                              $customeicon[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                              $customeicon1[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                         else
                         {
                              $customeicon1[] = $v;
                              unset($_FILES['customeicon']['name'][$k]);
                              unset($_FILES['customeicon']['type'][$k]);
                              unset($_FILES['customeicon']['tmp_name'][$k]);
                              unset($_FILES['customeicon']['error'][$k]);
                              unset($_FILES['customeicon']['size'][$k]);
                         }
                    }
                    if (in_array(1, $upload))
                    {
                         $this->session->set_flashdata('error', "Custom icon " . "minimum size should be 200px * 300px");
                         redirect("Document/index/" . $eventid);
                    }
                    else
                    {
                         if (!empty($customeicon[0]))
                         {
                              $this->upload->initialize(array(
                                   "file_name" => $customeicon,
                                   "upload_path" => "./assets/user_documents",
                                   "allowed_types" => '*',
                              ));
                              if (!$this->upload->do_multi_upload("customeicon"))
                              {
                                   $error = array(
                                        'error' => $this->upload->display_errors()
                                   );
                                   $this->session->set_flashdata('error', "Custom icon " . $error['error']);
                                   redirect("Document/index/" . $eventid);
                              }
                         }
                    }
               }
               foreach($this->input->post() as $k => $v)
               {
                    if ($k != "document_view" && $k != "customeicon" && $k != "type" && $k != "link")
                    {
                         $k = (strtolower($k));
                         $array_add[$k] = $v;
                    }
               }
               $array_add['Event_id'] = $eventid;
               $doc_id = $this->Document_model->edit_document($array_add, $id);
               foreach($Documents as $key => $val)
               {
                    $document = array();
                    $document['document_id'] = $id;
                    $document['document_file'] = $val;
                    $document['image_view'] = $_POST['document_view'][$key];
                    if (!empty($customeicon1))
                    {
                         $document['icon'] = $customeicon1[$key];
                    }
                    else
                    {
                         $document['icon'] = $_POST['icon'];
                    }
                    $user = $this->Document_model->add_document_files($document);
               }
               $this->session->set_flashdata('doccategoy_data', 'Added');
               redirect("Document/index/" . $eventid);
          }
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
          $this->data['users_role'] = $user_role;
          $this->template->write_view('css', 'document/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'document/edit', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'document/add_js', $this->data, true);
          $this->template->render();
     }
     public function editdoc($eventid=NULL, $id = '0')

     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          /* $doccategory = $this->Doccategory_model->get_doccategory_list(null, $eventid, '1');
          $this->data['doccategory'] = $doccategory;*/
          $documents = $this->Document_model->get_document_list($eventid, $id);
          $this->data['Document'] = $documents;
          $docfolder = $this->Document_model->get_docfolder_list($eventid, $id);
          $this->data['docfolder'] = $docfolder;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
          $this->data['users_role'] = $user_role;
          $documents_files = $this->Document_model->get_document_file_list($id);
          $this->data['Document_files'] = $documents_files;
          $this->data['event_id'] = $eventid;
          if ($this->input->post())
          {
               if (!empty($_FILES['files_icon_images']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['files_icon_images']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("files_icon_images"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['document']['name'][0]))
               {
                    /*foreach ($_FILES['document']['name'] as $k => $v)
                    {
                    if (!empty($v))
                    {*/
                    $_FILES['document']['name'][0] = preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES['document']['name'][0]);
                    $_FILES['document']['name'][0] = str_replace(' ', '', $_FILES['document']['name'][0]);
                    $Documents_files = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['document']['name'][0];
                    /*}
                    }*/
                    $this->upload->initialize(array(
                         "file_name" => $Documents_files,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => '*'
                    ));
                    if (!$this->upload->do_multi_upload("document"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document not allow " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               foreach($this->input->post() as $k => $v)
               {
                    if ($k != "document_view" && $k != "customeicon" && $k != "icon")
                    {
                         $k = (strtolower($k));
                         $array_add[$k] = $v;
                    }
               }
               if (!empty($images_file))
               {
                    $array_add['docicon'] = $images_file;
               }
               $array_add['Event_id'] = $eventid;
               $doc_id = $this->Document_model->edit_document($array_add, $id);
               /*foreach ($Documents as $key => $val)
               {*/
               if (!empty($Documents_files) || !empty($images_file))
               {
                    $document = array();
                    if (!empty($Documents_files))
                    {
                         $document['document_file'] = $Documents_files;
                    }
                    $document['image_view'] = $_POST['document_view'][$key];
                    if (!empty($images_file))
                    {
                         $document['icon'] = $images_file;
                    }
                    else
                    {
                         $document['icon'] = NULL;
                    }
                    $user = $this->Document_model->edit_document_files($document, $id);
               }
               // $user = $this->Document_model->add_document_files($document);
               /*}*/
               $this->session->set_flashdata('doccategoy_data', 'Added');
               redirect("Document/index/" . $eventid);
          }
          $this->template->write_view('css', 'document/add_css', $this->data, true);
          $this->template->write_view('content', 'document/editdoc', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'document/add_js', $this->data, true);
          $this->template->render();
     }
     public function adddoc($eventid=NULL)

     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          /* $doccategory = $this->Doccategory_model->get_doccategory_list(null, $eventid, '1');
          $this->data['doccategory'] = $doccategory;*/
          $docfolder = $this->Document_model->get_docfolder_list($eventid);
          $this->data['docfolder'] = $docfolder;
          $this->data['event_id'] = $eventid;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
          $this->data['users_role'] = $user_role;
          if ($this->input->post())
          {
               if (!empty($_FILES['files_icon_images']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['files_icon_images']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $_POST['docicon'] = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("files_icon_images"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['document']['name'][0]))
               {
                    foreach($_FILES['document']['name'] as $k => $v)
                    {
                         if (!empty($v))
                         {
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $v = str_replace(' ', '', $v);
                              $Documents[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                    }
                    $this->upload->initialize(array(
                         "file_name" => $Documents,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => '*'
                    ));
                    if (!$this->upload->do_multi_upload("document"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document not allow " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               foreach($this->input->post() as $k => $v)
               {
                    if ($k != "document_view" && $k != "customeicon")
                    {
                         $k = (strtolower($k));
                         $array_add[$k] = $v;
                    }
               }
               if (!empty($images_file))
               {
                    $array_add['docicon'] = $images_file;
               }
               $array_add['Event_id'] = $eventid;
               $doc_id = $this->Document_model->add_document($array_add);
               foreach($Documents as $key => $val)
               {
                    $document = array();
                    $document['document_id'] = $doc_id;
                    $document['document_file'] = $val;
                    $document['image_view'] = $_POST['document_view'][$key];
                    if (!empty($images_file))
                    {
                         $document['icon'] = $images_file;
                    }
                    else
                    {
                         $document['icon'] = NULL;
                    }
                    $user = $this->Document_model->add_document_files($document);
               }
               $this->session->set_flashdata('doccategoy_data', 'Added');
               redirect("Document/index/" . $eventid);
          }
          $this->template->write_view('css', 'document/add_css', $this->data, true);
          $this->template->write_view('content', 'document/adddoc', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'document/add_js', $this->data, true);
          $this->template->render();
     }
     public function add($eventid=NULL)

     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          /* $doccategory = $this->Doccategory_model->get_doccategory_list(null, $eventid, '1');
          $this->data['doccategory'] = $doccategory;*/
          $docfolder = $this->Document_model->get_docfolder_list($eventid);
          $this->data['docfolder'] = $docfolder;
          $this->data['event_id'] = $eventid;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $eventid);
          $this->data['users_role'] = $user_role;
          if ($this->input->post())
          {
               if (!empty($_FILES['userfile']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['userfile']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $_POST['docicon'] = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("userfile"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['coverimages']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['coverimages']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $_POST['coverimages'] = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("coverimages"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document Cover images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['document']['name'][0]))
               {
                    foreach($_FILES['document']['name'] as $k => $v)
                    {
                         if (!empty($v))
                         {
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $v = str_replace(' ', '', $v);
                              $Documents[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                    }
                    $this->upload->initialize(array(
                         "file_name" => $Documents,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => '*'
                    ));
                    if (!$this->upload->do_multi_upload("document"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document not allow " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
               }
               if (!empty($_FILES['customeicon']['name']))
               {
                    $upload = array();
                    foreach($_FILES['customeicon']['name'] as $k => $v)
                    {
                         if (!empty($v))
                         {
                              $imgesize = getimagesize($_FILES['customeicon']['tmp_name'][$k]);
                              if ($imgesize[0] < 300 || $imgesize[1] < 200)
                              {
                                   $upload[] = 1;
                              }
                              else
                              {
                                   $upload[] = 0;
                              }
                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                              $v = str_replace(' ', '', $v);
                              $customeicon[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                              $customeicon1[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                         else
                         {
                              $customeicon1[] = $v;
                              unset($_FILES['customeicon']['name'][$k]);
                              unset($_FILES['customeicon']['type'][$k]);
                              unset($_FILES['customeicon']['tmp_name'][$k]);
                              unset($_FILES['customeicon']['error'][$k]);
                              unset($_FILES['customeicon']['size'][$k]);
                         }
                    }
                    if (in_array(1, $upload))
                    {
                         $this->session->set_flashdata('error', "Custom icon " . "minimum size should be 200px * 300px");
                         redirect("Document/index/" . $eventid);
                    }
                    else
                    {
                         if (!empty($customeicon[0]))
                         {
                              $this->upload->initialize(array(
                                   "file_name" => $customeicon,
                                   "upload_path" => "./assets/user_documents",
                                   "allowed_types" => '*',
                              ));
                              if (!$this->upload->do_multi_upload("customeicon"))
                              {
                                   $error = array(
                                        'error' => $this->upload->display_errors()
                                   );
                                   $this->session->set_flashdata('error', "Custom icon " . $error['error']);
                                   redirect("Document/index/" . $eventid);
                              }
                         }
                    }
               }
               foreach($this->input->post() as $k => $v)
               {
                    if ($k != "document_view" && $k != "customeicon" && $k != "type" && $k != "link")
                    {
                         $k = (strtolower($k));
                         $array_add[$k] = $v;
                    }
               }
               $array_add['Event_id'] = $eventid;
               $doc_id = $this->Document_model->add_document($array_add);
               foreach($Documents as $key => $val)
               {
                    $document = array();
                    $document['document_id'] = $doc_id;
                    $document['document_file'] = $val;
                    $document['image_view'] = $_POST['document_view'][$key];
                    if (!empty($customeicon1))
                    {
                         $document['icon'] = $customeicon1[$key];
                    }
                    else
                    {
                         $document['icon'] = NULL;
                    }
                    $user = $this->Document_model->add_document_files($document);
               }
               $this->session->set_flashdata('doccategoy_data', 'Added');
               redirect("Document/index/" . $eventid);
          }
          $this->template->write_view('css', 'document/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'document/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'document/add_js', $this->data, true);
          $this->template->render();
     }
     public function addfolder($eid=NULL)

     {
          $fdata['Event_id'] = $eid;
          $fdata['title'] = $this->input->post('folder_title');
          $fdata['doc_type'] = '0';
          if (!$this->Document_model->checkfoldername($fdata))
          {
               if (!empty($_FILES['folder_icon_images']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['folder_icon_images']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if ($this->upload->do_multi_upload("folder_icon_images"))
                    {
                         $fdata['docicon'] = $images_file;
                    }
                    else
                    {
                         echo "error###" . $this->upload->display_errors();
                         die;
                    }
               }
               $doc_id = $this->Document_model->add_document($fdata);
               $docfolder = $this->Document_model->get_docfolder_list($eid);
               echo '<option value="0">Select Folder</option>';
               echo '<option onclick="openpop();" style="background-color: #ccc !important;" value="New">Add a new folder</option>';
               foreach($docfolder as $key => $value)
               {
                    if ($value['title'] == $fdata['title'])
                    {
                         echo '<option selected="selected" value="' . $value['id'] . '">' . $value['title'] . '</option>';
                    }
                    else
                    {
                         echo '<option value="' . $value['id'] . '">' . $value['title'] . '</option>';
                    }
               }
               die;
          }
          else
          {
               echo "error###Folder Name Already Exists Plase Enter Another Name";
               die;
          }
     }
     public function checkfoldername($eid=NULL)

     {
          $fdata['Event_id'] = $eid;
          $fdata['title'] = $this->input->post('title');
          $fdata['doc_type'] = '0';
          if ($this->Document_model->checkfoldername($fdata))
          {
               echo "error###Folder Name Already Exists Plase Enter Another Name";
               die;
          }
     }
     public function delete($eventid=NULL, $id=NULL)

     {
          $user = $this->Document_model->delete_document($id);
          $this->session->set_flashdata('doccategoy_data', 'Deleted');
          redirect("Document/index/" . $eventid);
     }
     public function delete_files($eid=NULL, $id=NULL)

     {
          $user = $this->Document_model->delete_document_files($id);
          echo "done";
          exit;
     }
     public function uploadcover($eventid=NULL)

     {
          if (!empty($_FILES))
          {
               if (!empty($_FILES['converimage']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['converimage']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $coverimages = $images_file;
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("converimage"))
                    {
                         $error = array(
                              'error' => $this->upload->display_errors()
                         );
                         $this->session->set_flashdata('error', "Document images added wrong " . $error['error']);
                         redirect("Document/index/" . $eventid);
                    }
                    /*$coverimages=$this->Doccategory_model->uploadcover($eventid,$coverimages);*/
               }
          }
          redirect("Document/index/" . $eventid);
     }
     public function delete_folder($eid=NULL, $fid=NULL)

     {
          $this->Document_model->delete_folder($eid, $fid);
          $this->session->set_flashdata('folder_data', 'Folder Deleted Successfully.');
          redirect("Document/index/" . $eid);
     }
     public function savefoldername($eid=NULL)

     {
          $fdata['Event_id'] = $eid;
          $fdata['title'] = $this->input->post('folder_name');
          $fdata['doc_type'] = '0';
          if ($this->Document_model->checkfoldername($fdata, $this->input->post('folder_id')))
          {
               $this->session->set_flashdata('error', 'Folder Name Already Exists Plase Enter Another Name');
          }
          else
          {
               $images_file = NULL;
               if (!empty($_FILES['folder_icon_images']['name']))
               {
                    $v = str_replace(' ', '', $_FILES['folder_icon_images']['name']);
                    $v = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    $tempname = explode('.', $v);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_documents",
                         "allowed_types" => 'gif|jpg|png|jpeg',
                         "max_size" => '10000000',
                         "max_width" => '3000',
                         "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("folder_icon_images"))
                    {
                         $this->session->set_flashdata('error', $this->upload->display_errors());
                         redirect("Document/index/" . $eid);
                    }
               }
               $this->Document_model->save_folder_name($eid, $this->input->post('folder_name') , $this->input->post('folder_id') , $images_file);
               $this->session->set_flashdata('folder_data', 'Folder Name Save Successfully.');
          }
          redirect("Document/index/" . $eid);
     }
     public function delete_folder_icon($eid=NULL, $did=NULL)

     {
          $documents = $this->Document_model->get_document_list($eid, $did);
          if (!empty($documents[0]['docicon']))
          {
               unlink('./assets/user_documents/' . $documents[0]['docicon']);
          }
          $this->Document_model->remove_icon_images($eid, $did);
          redirect('Document/index/' . $eid);
     }
     public function delete_files_icon($eid=NULL, $did=NULL)

     {
          $documents = $this->Document_model->get_document_list($eid, $did);
          if (!empty($documents[0]['docicon']))
          {
               unlink('./assets/user_documents/' . $documents[0]['docicon']);
          }
          $this->Document_model->remove_icon_images($eid, $did);
          redirect('Document/editdoc/' . $eid . '/' . $did);
     }
     public function deletecover($eventid=NULL)

     {
          /* $coverimages=$this->Doccategory_model->deletecover($eventid);
          redirect("document/index/" . $eventid);*/
     }
}