<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Exhibitors extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Exhibitors';
        $this->data['smalltitle'] = 'Exhibitors';
        $this->data['breadcrumb'] = 'Exhibitors';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->library('formloader1');
        $this->load->model('Agenda_model');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Notes_admin_model');
        // $this->load->model('Exibitor_model');
        $this->load->model('Profile_model');
        $this->load->model('Message_model');
        $user = $this->session->userdata('current_user');
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (!in_array('3', $menu_list) && $eventid != '585')
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $cnt = 0;
        $eventname = $this->Event_model->get_all_event_name();
        if (in_array($this->uri->segment(3) , $eventname))
        {
            if ($user != '')
            {
                $logged_in_user_id = $user[0]->User_id;
                $parameters = $this->uri->uri_to_assoc(1);
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                /*if(!empty($roledata))
                {
                $roleid = $roledata[0]->Role_id;
                $eventid = $event_templates[0]['Id'];
                $rolename = $roledata[0]->Name;
                $cnt = 0;
                $req_mod = ucfirst($this->router->fetch_class());
                // $cnt = $this->Agenda_model->check_auth($this->data['pagetitle'], $roleid, $rolename, $eventid);
                $cnt = 1;
                }
                else
                {
                $cnt=0 ;
                }*/
                $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                if ($cnt == 1)
                {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                    $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type = $event_templates[0]['Event_type'];
                    if ($event_type == 3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain = $this->uri->segment(3);
                        $acc_name = $this->uri->segment(2);
                        echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
                    }
                }
            }
        }
        else
        {
            $parameters = $this->uri->uri_to_assoc(1);
            $Subdomain = $this->uri->segment(3);
            $flag = 1;
            echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
        }
    }
    public function index($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL, $parent_id = NULL)

    {
        // echo $acc_name;
        // echo "<br />";
        //    echo $Subdomain;
        //    echo "<br />";
        //    echo $intFormId;
        //    echo "<br />";
        //    echo $parent;
        //    echo "<br />";
        //    exit();
        $this->data['acc_name'] = $acc_name;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['intFormId'] = $intFormId;
        $this->data['parent_id'] = $parent_id;
        $intFormId = ($intFormId == 'categorywise') ? NULL : $intFormId;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            if ($req_mod == "Exhibitor")
            {
                $req_mod = "Exhibitors";
            }
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $exibitors_data = $this->Event_template_model->get_exibitors_list_by_type($Subdomain, $parent_id);
        $this->data['exibitors_data'] = $exibitors_data;
        $exhibitor_category = $this->Event_template_model->get_exibitor_categories($event_templates[0]['Id'], NULL, $parent_id);
        $this->data['exhibitor_category'] = $exhibitor_category;
        if (empty($parent_id))
        {
            $exhibitor_parent_categories = $this->Event_template_model->get_exibitors_parent_categories($event_templates[0]['Id']);
            $this->data['exhibitor_parent_categories'] = $exhibitor_parent_categories;
        }
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $my_favorites = $this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'], $user[0]->Id, '3');
            $this->data['my_favorites'] = $my_favorites;
        }
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $attendees_metting = $this->Agenda_model->get_all_pending_metting_by_exibitor($event_templates[0]['Id']);
        $this->data['attendees_metting'] = $attendees_metting;
        $this->data['sign_form_data'] = $res1;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Exhibitors' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            echo '<script>window.location.href="' . base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '"</script>';
            exit;
        }
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        if (!empty($parent_id))
        {
            $keywords = array_column_1($exhibitor_category, 'categorie_keywords');
            $keywords = implode(',', $keywords);
            $exibitors_country = $this->Event_template_model->get_exibitor_list_by_category($event_templates[0]['Id'], $keywords, null, null, null, 0, true);
            $this->data['exibitors_country'] = $exibitors_country;
            $exibitors_list = $this->Event_template_model->get_exibitor_list_by_category($event_templates[0]['Id'], $keywords, null, null, null, 0, true);
        }
        else
        {
            $exibitors_list = $this->Event_template_model->get_exibitors_list($Subdomain);
        }
        $this->data['exibitors_list'] = $exibitors_list;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $this->data['Subdomain'] = $Subdomain;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        if ($event_templates[0]['show_topbar_exhi'] == '1')
        {
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $main_contain = ($event_templates[0]['Id'] == '585') ? 'newindex' : 'index';
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if (!empty($exhibitor_parent_categories)) $this->template->write_view('content', 'Exhibitors/newindexparent', $this->data, true);
                else $this->template->write_view('content', 'Exhibitors/' . $main_contain, $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                if (!empty($exhibitor_parent_categories)) $this->template->write_view('content', 'Exhibitors/newindexparent', $this->data, true);
                else $this->template->write_view('content', 'Exhibitors/' . $main_contain, $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('3', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                if (!empty($exhibitor_parent_categories)) $this->template->write_view('content', 'Exhibitors/newindexparent', $this->data, true);
                else $this->template->write_view('content', 'Exhibitors/' . $main_contain, $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user) && $event_templates[0]['Id'] != '585')
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                if (!empty($exhibitor_parent_categories)) $this->template->write_view('content', 'Exhibitors/newindexparent', $this->data, true);
                else $this->template->write_view('content', 'Exhibitors/' . $main_contain, $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        $this->template->render();
    }
    public function load_more($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL, $parent_id = NULL)

    {
        $start = $_GET['start'];
        $this->data['acc_name'] = $acc_name;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['intFormId'] = $intFormId;
        $this->data['parent_id'] = $parent_id;
        $intFormId = ($intFormId == 'categorywise') ? NULL : $intFormId;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        $eid = $event_templates[0]['Id'];
        $array_temp_past = $this->input->post();
        if (!empty($parent_id))
        {
            $exhibitor_category = $this->Event_template_model->get_exibitor_categories($event_templates[0]['Id'], NULL, $parent_id);
            $this->data['exhibitor_category'] = $exhibitor_category;
            $keywords = array_column_1($exhibitor_category, 'categorie_keywords');
            $keywords = implode(',', $keywords);
            $exibitors_list = $this->Event_template_model->get_exibitor_list_by_category($event_templates[0]['Id'], $keywords, null, null, null, $start);
        }
        $this->data['exibitors_list'] = $exibitors_list;
        $data = $this->load->view('Exhibitors/load_more', $this->data, TRUE);
        $this->output->set_output($data);
    }
    public function get_view_by_category_id($acc_name = NULL, $Subdomain = NULL, $id = null, $cid = null)

    {
    }
    public function View($acc_name = NULL, $Subdomain = NULL, $id = null)

    {
        $uid = $this->Event_template_model->get_ex_by_page($id);
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $this->data['subdomain'] = $Subdomain;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['sp_id'] = $uid;
        $this->data['Sid'] = $uid;
        $ex = $this->Event_template_model->get_ex_list($Subdomain);
        $this->data['ex'] = $ex;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        if ($user[0]->Role_id == 4)
        {
            $this->Event_model->add_exhibitor_visit_profile_hit($event_templates[0]['Id'], $id, $user[0]->Id);
            $custom = $this->Event_model->get_user_custom_clounm($user[0]->Id, $event_templates[0]['Id']);
            $this->data['custom'] = $custom;
        }
        $exibitors_data = $this->Event_template_model->get_exibitors_data($Subdomain, $id);
        $this->data['exibitors_data'] = $exibitors_data;
        $this->data['event_settings'] = $this->Event_model->get_event_settings($event_templates[0]['Id']);
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        if ($event_templates[0]['show_topbar_exhi'] == '1')
        {
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $uid);
                $this->data['view_chats1'] = $view_chats1;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;
                $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);
                $this->data['user_url'] = $user_url;
                $this->template->write_view('content', 'Exhibitors/user', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                $this->data['view_chats1'] = $view_chats1;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;
                /*$user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;*/
                $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);
                $this->data['user_url'] = $user_url;
                $this->template->write_view('content', 'Exhibitors/user', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('3', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                if (!empty($user))
                {
                    $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                    $this->data['view_chats1'] = $view_chats1;
                }
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;
                $test = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                /*$user_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                $this->data['user_url'] = $user_url;*/
                $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);
                $this->data['user_url'] = $user_url;
                $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                $this->data['social_url'] = $social_url;
                if (!empty($test))
                {
                    // echo 111; exit();
                    $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;
                }
                else
                {
                    // echo 222; exit();
                    /*$user_url = $this->Event_template_model->get_speaker_user_url($Subdomain, $id);
                    $this->data['user_url'] = $user_url;*/
                    $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);
                    $this->data['user_url'] = $user_url;
                }
                $this->template->write_view('content', 'Exhibitors/user', $this->data, true);
            }
        }
        else
        {
            if (empty($user) && $event_templates[0]['Id'] != '585')
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                $view_chats1 = $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'], 0, 5, null, $uid);
                $this->data['view_chats1'] = $view_chats1;
                $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                $this->data['cms_menu'] = $cmsmenu;
                $user_url = $this->Event_template_model->get_ex_social_url($Subdomain, $uid);
                $this->data['user_url'] = $user_url;
                $this->template->write_view('content', 'Exhibitors/user', $this->data, true);
            }
        }
        $this->template->render();
    }
    public function checpendingkmetting($acc_name= NULL, $Subdomain= NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $attendees_metting = $this->Agenda_model->get_all_pending_metting_by_exibitor($event_templates[0]['Id']);
        $this->data['attendees_metting'] = $attendees_metting;
        $this->data['sign_form_data'] = $res1;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Exhibitors' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $this->data['Subdomain'] = $Subdomain;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/checkmetting_view', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/checkmetting_view', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('3', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/checkmetting_view', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/checkmetting_view', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function request_meeting($acc_name= NULL, $Subdomain= NULL, $exid= NULL)

    {
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $overlapping = $this->Agenda_model->check_overlapping_meeting_with_agenda($user[0]->Id, $event[0]['Id'], $date, $time);
        if (count($overlapping) > 0 && $this->input->post('clash_check') == '1')
        {
            echo "error###This will clash with " . $overlapping[0]['Heading'] . "- would you like to proceed?";
            die;
        }
        else
        {
            $ex_uid = $this->Event_template_model->get_ex_by_page($exid);
            $metting['exhibiotor_id'] = $exid;
            $metting['attendee_id'] = $user[0]->Id;
            $metting['event_id'] = $event[0]['Id'];
            $metting['date'] = $date;
            $metting['time'] = $time;
            $metting['status'] = '0';
            $metting_id = - $this->Agenda_model->add_metting_with_exibitor($metting);
            $url = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
            $Message = "<a href='" . $url . "'>" . ucfirst($user[0]->Firstname) . " " . $user[0]->Lastname . " has requested a Meeting with you at " . $time . " on " . $date . " Tap here to accept the meeting or Tap here to cancel the meeting </a>";
            $data1 = array(
                'Message' => $Message,
                'Sender_id' => $user[0]->Id,
                'Receiver_id' => $ex_uid,
                'Event_id' => $event[0]['Id'],
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user[0]->Id,
                'msg_type' => '2'
            );
            $this->Message_model->send_speaker_message($data1);
            echo "success###";
            die;
        }
    }
    public function changemettingstatus($acc_name= NULL, $Subdomain= NULL, $mid= NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $metting_data = $this->Agenda_model->change_metting_status_by_metting_id($mid, $this->input->post('status'));
        $ex_uid = $this->Event_template_model->get_ex_by_page($metting_data[0]['exhibiotor_id']);
        $exibitors_data = $this->Event_template_model->get_exibitors_data($Subdomain, $metting_data[0]['exhibiotor_id']);
        if ($metting_data[0]['status'] == '2')
        {
            if (empty($this->input->post('reject_msg')))
            {
                $Message = ucfirst($exibitors_data[0]['Heading']) . " has denied your Meeting request due to a clash. Please try another time.";
            }
            else
            {
                $Message = ucfirst($exibitors_data[0]['Heading']) . " has rejected your meeting request - " . $this->input->post('reject_msg');
            }
        }
        else if ($metting_data[0]['status'] == '1')
        {
            $Message = ucfirst($exibitors_data[0]['Heading']) . " Have Accepted Your Meeting Request.";
        }
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $ex_uid,
            'Receiver_id' => $metting_data[0]['attendee_id'],
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $ex_uid
        );
        $this->Message_model->send_speaker_message($data1);
        echo "Success###";
        die;
    }
    public function suggest_new_datetime($acc_name= NULL, $Subdomain= NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $user = $this->session->userdata('current_user');
        $mid = $this->input->post('meeting_id_textbox');
        $meeting_data = $this->Agenda_model->get_meeting_data_by_meeting_id($mid);
        $ex_uid = $this->Event_template_model->get_ex_by_page($meeting_data[0]['exhibiotor_id']);
        $Message = $meeting_data[0]['Heading'] . " is unable to meet with you at the time you specified. " . $meeting_data[0]['Heading'] . " has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";
        $time = $this->input->post('time');
        foreach($this->input->post('date') as $key => $value)
        {
            $this->Agenda_model->add_suggest_new_datetime($event[0]['Id'], $date, $mid, $meeting_data[0]['attendee_id'], $ex_uid, NULL);
            $date = date('Y-m-d H:i', strtotime($value . ' ' . $time[$key]));
            $link = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/changemeetingdate/' . strtotime($date) . '/' . $mid;
            $Message.= "<a href='" . $link . "'>" . $date . "</a><br/>";
        }
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $user[0]->Id,
            'Receiver_id' => $meeting_data[0]['attendee_id'],
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $user[0]->Id,
            'msg_type' => '4'
        );
        $this->Message_model->send_speaker_message($data1);
        redirect(base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting');
    }
    public function changemeetingdate($acc_name= NULL, $Subdomain= NULL, $datetime= NULL, $mid= NULL)

    {
        $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $meeting_data = $this->Agenda_model->get_meeting_data_by_meeting_id($mid);
        $user = $this->session->userdata('current_user');
        $ex_uid = $this->Event_template_model->get_ex_by_page($meeting_data[0]['exhibiotor_id']);
        if ($user[0]->Id == $meeting_data[0]['attendee_id'])
        {
            $m_da['date'] = date('Y-m-d', $datetime);
            $m_da['time'] = date('H:i', $datetime);
            $m_da['status'] = '1';
            $this->Agenda_model->update_datetime_in_meeting_by_meeting_id($m_da, $mid);
            $this->Agenda_model->delete_suggest_meeting_date($event[0]['Id'], $meeting_data[0]['attendee_id'], $ex_uid, null);
        }
        $Message = $meeting_data[0]['Firstname'] . " " . $meeting_data[0]['Lastname'] . " at " . $meeting_data[0]['Company_name'] . " as accepted your new suggested time of " . date('Y-m-d H:i', $datetime) . ". This meeting has now been booked";
        $data1 = array(
            'Message' => $Message,
            'Sender_id' => $user[0]->Id,
            'Receiver_id' => $ex_uid,
            'Event_id' => $event[0]['Id'],
            'Parent' => '0',
            'Time' => date("Y-m-d H:i:s") ,
            'ispublic' => '0',
            'msg_creator_id' => $user[0]->Id
        );
        $this->Message_model->send_speaker_message($data1);
        redirect(base_url() . 'Agenda/' . $acc_name . '/' . $Subdomain . '/view_user_agenda');
    }
    public function get_categorie_exibitorlist($acc_name= NULL, $Subdomain= NULL, $cid = NULL, $cntry_id = 0, $parent_id = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $exhibitor_category = $this->Event_template_model->get_exibitor_categories($event_templates[0]['Id'], $cid, $parent_id);
        if (!empty($parent_id) && (empty($cid) || $cid == 'null'))
        {
            $keywords = array_column_1($exhibitor_category, 'categorie_keywords');
            $keywords = implode(',', $keywords);
        }
        else
        {
            $keywords = $exhibitor_category[0]['categorie_keywords'];
        }
        $exibitors_list = $this->Event_template_model->get_exibitor_list_by_category($event_templates[0]['Id'], $keywords, $cntry_id, null, null, null);
        $user = $this->session->userdata('current_user');
        $my_favorites = $this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'], $user[0]->Id, '3');
        $this->data['my_favorites'] = $my_favorites;
        $favorites_user = array_filter(array_column_1($my_favorites, 'module_id'));
        $active_menu = array_filter(explode(",", $event_templates[0]['checkbox_values']));
        $strhtml = "";
        if (count($exibitors_list) > 0)
        {
            foreach($exibitors_list as $key => $value)
            {
                $editurllink = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/View/' . $value['Id'];
                $strhtml.= "<li><a hrefqq='" . $editurllink . "' class='activity' onclick='openmodel(" . $value['Id'] . ")' id='a" . $value['Id'] . "'>";
                $strhtml.= "<div class='desc'>";
                $strhtml.= "<p class='logo-img' style='width: auto;'>";
                $clogo = json_decode($value['company_logo']);
                if (!empty($clogo[0]))
                {
                    $strhtml.= "<img src='" . base_url() . "assets/user_files/" . $clogo[0] . "' style='width: auto;max-width: 80px;height: auto'/>";
                }
                else
                {
                    $color = $event_templates[0]['Top_background_color'];
                    $strhtml.= "<span style='background:" . $color . "'>" . ucfirst(substr($value['Heading'], 0, 1)) . "</span>";
                }
                $strhtml.= "</p><h4 class='user_container_name'>" . ucfirst($value['Heading']) . "</h4><h4 class='user_container_username' style='display:none;'>" . trim($value['Firstname']) . ' ' . trim($value['Lastname']) . "</h4>";
                $strhtml.= "<div class='Exbitor_keyword' style='display: none;'>" . $value['Short_desc'] . "</div>";
                $strhtml.= "<div class='Exbitor_contry' style='display: none;'>" . $value['country_name'] . "</div>";
                $strhtml.= "</div><div class='time'><i class='fa fa-chevron-right'></i></div>";
                $strhtml.= "</a>";
                if (!empty($user[0]->Id) && in_array('49', $active_menu) && $user[0]->Rid == '4')
                {
                    $strhtml.= "<div class='myfavorites_content_div'>";
                    $funname = 'my_favorites("' . $value['Id'] . '");';
                    $strhtml.= "<a onclick=" . $funname . ">";
                    if (in_array($value['Id'], $favorites_user))
                    {
                        $strstyle = 'style="display:none;"';
                    }
                    else
                    {
                        $strstyle = '';
                    }
                    $strhtml.= "<img width='5%' src='" . base_url() . "assets/images/favorites_not_selected.png' class='pull-right' id='favorites_not_selected_" . $value['Id'] . "' " . $strstyle . ">";
                    if (!in_array($value['Id'], $favorites_user))
                    {
                        $strstyle1 = 'style="display:none;"';
                    }
                    else
                    {
                        $strstyle1 = '';
                    }
                    $strhtml.= "<img width='5%' src='" . base_url() . "assets/images/favorites_selected.png' class='pull-right' id='favorites_selected_" . $value['Id'] . "' " . $strstyle1 . ">";
                    $strhtml.= "</a><a href='javascript:void(0);' style='display: none;'><img width='5%' src='" . base_url() . "assets/images/ajax-loader.gif' id='loder_icone_" . $value['Id'] . "'>";
                    $strhtml.= "</a></div>";
                }
                $strhtml.= "</li>";
            }
        }
        else
        {
            $strhtml = "<h2 style='text-align:center;'>No exhibitors for this category.</h2>";
        }
        echo $strhtml;
        die;
    }
    public function get_categorie_exibitorlist_new($acc_name= NULL, $Subdomain= NULL, $parent_id = NULL)

    {
        if (!empty($this->input->post('category_ids'))) $cid = $this->input->post('category_ids');
        else $cid = NULL;
        if (!empty($this->input->post('country_ids'))) $cntry_id = $this->input->post('country_ids');
        else $cntry_id = NULL;
        if (!empty($this->input->post('halal_check'))) $halal_check = $this->input->post('halal_check');
        else $halal_check = '0';
        $venue = $this->input->post('venue');
        $exhi_ids = $this->input->post('exhi_ids');
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        if ($halal_check == '1' && $cid == NULL)
        {
            $exhibitor_category = [];
        }
        else
        {
            $exhibitor_category = $this->Event_template_model->get_exibitor_categories($event_templates[0]['Id'], $cid, $parent_id);
        }
        $keywords = '';
        if (empty($cid))
        {
            $keywords = '';
        }
        elseif ((!empty($parent_id) && empty($cid)) || (is_array($cid) || empty($_POST['category_ids'])))
        {
            $keywords = array_column_1($exhibitor_category, 'categorie_keywords');
            $keywords = implode(',', $keywords);
        }
        else
        {
            $keywords = $exhibitor_category[0]['categorie_keywords'];
        }
        if ($halal_check)
        {
            $keywords = array_filter(explode(",", $keywords));
            $keywords[] = 'Halal';
            $keywords = implode(',', $keywords);
        }
        $exibitors_list = $this->Event_template_model->get_exibitor_list_by_category($event_templates[0]['Id'], $keywords, $cntry_id, $exhi_ids, $venue, null, true);
        $user = $this->session->userdata('current_user');
        $my_favorites = $this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'], $user[0]->Id, '3');
        $this->data['my_favorites'] = $my_favorites;
        $favorites_user = array_filter(array_column_1($my_favorites, 'module_id'));
        $active_menu = array_filter(explode(",", $event_templates[0]['checkbox_values']));
        $strhtml = "";
        if (count($exibitors_list) > 0)
        {
            foreach($exibitors_list as $key => $value)
            {
                $editurllink = base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '/View/' . $value['Id'];
                $strhtml.= "<li><a hrefqq='" . $editurllink . "' class='activity' onclick='openmodel(" . $value['Id'] . ")' id='a" . $value['Id'] . "'>";
                $strhtml.= "<div class='desc'>";
                $strhtml.= "<p class='logo-img'>";
                $clogo = json_decode($value['company_logo']);
                if (!empty($clogo[0]))
                {
                    $strhtml.= "<img src='" . base_url() . "assets/user_files/" . $clogo[0] . "' style='width: auto;max-width: 80px;height: auto';/>";
                }
                else
                {
                    $color = $event_templates[0]['Top_background_color'];
                    $strhtml.= "<span style='background:" . $color . "'>" . ucfirst(substr($value['Heading'], 0, 1)) . "</span>";
                }
                $strhtml.= "</p><h4 class='user_container_name'>" . ucfirst($value['Heading']) . "</h4><h4 class='user_container_username' style='display:none;'>" . trim($value['Firstname']) . ' ' . trim($value['Lastname']) . "</h4>";
                $strhtml.= "<div class='Exbitor_keyword' style='display: none;'>" . $value['Short_desc'] . "</div>";
                $strhtml.= "<div class='Exbitor_contry' style='display: none;'>" . $value['country_name'] . "</div>";
                $strhtml.= "</div><div class='time'><i class='fa fa-chevron-right'></i></div>";
                $strhtml.= "</a>";
                if (!empty($user[0]->Id) && in_array('49', $active_menu) && $user[0]->Rid == '4')
                {
                    $strhtml.= "<div class='myfavorites_content_div'>";
                    $funname = 'my_favorites("' . $value['Id'] . '");';
                    $strhtml.= "<a onclick=" . $funname . ">";
                    if (in_array($value['Id'], $favorites_user))
                    {
                        $strstyle = 'style="display:none;"';
                    }
                    else
                    {
                        $strstyle = '';
                    }
                    $strhtml.= "<img width='5%' src='" . base_url() . "assets/images/favorites_not_selected.png' class='pull-right' id='favorites_not_selected_" . $value['Id'] . "' " . $strstyle . ">";
                    if (!in_array($value['Id'], $favorites_user))
                    {
                        $strstyle1 = 'style="display:none;"';
                    }
                    else
                    {
                        $strstyle1 = '';
                    }
                    $strhtml.= "<img width='5%' src='" . base_url() . "assets/images/favorites_selected.png' class='pull-right' id='favorites_selected_" . $value['Id'] . "' " . $strstyle1 . ">";
                    $strhtml.= "</a><a href='javascript:void(0);' style='display: none;'><img width='5%' src='" . base_url() . "assets/images/ajax-loader.gif' id='loder_icone_" . $value['Id'] . "'>";
                    $strhtml.= "</a></div>";
                }
                $strhtml.= "</li>";
            }
        }
        else
        {
            $strhtml = "<h2 style='text-align:center;'>No exhibitors for this category.</h2>";
        }
        echo $strhtml;
        die;
    }
    public function index_bkp($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            if ($req_mod == "Exhibitor")
            {
                $req_mod = "Exhibitors";
            }
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $exibitors_data = $this->Event_template_model->get_exibitors_list_by_type($Subdomain);
        $this->data['exibitors_data'] = $exibitors_data;
        $exhibitor_category = $this->Event_template_model->get_exibitor_categories($event_templates[0]['Id']);
        $this->data['exhibitor_category'] = $exhibitor_category;
        $exhibitor_parent_categories = $this->Event_template_model->get_exibitors_parent_categories($event_templates[0]['Id']);
        $this->data['exhibitor_parent_categories'] = $exhibitor_parent_categories;
        // j($exhibitor_parent_categories);
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $my_favorites = $this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'], $user[0]->Id, '3');
            $this->data['my_favorites'] = $my_favorites;
        }
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $attendees_metting = $this->Agenda_model->get_all_pending_metting_by_exibitor($event_templates[0]['Id']);
        $this->data['attendees_metting'] = $attendees_metting;
        $this->data['sign_form_data'] = $res1;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Exhibitors' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_id'] = $mid;
        $this->data['menu_list'] = $menu_list;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'm_id' => $mid,
                'user_id' => $user[0]->Id,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            echo '<script>window.location.href="' . base_url() . 'Exhibitors/' . $acc_name . '/' . $Subdomain . '"</script>';
            exit;
        }
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $exibitors_list = $this->Event_template_model->get_exibitors_list($Subdomain);
        $this->data['exibitors_list'] = $exibitors_list;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $this->data['Subdomain'] = $Subdomain;
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/newindex', $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/newindex', $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('3', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/newindex', $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Exhibitors/newindex', $this->data, true);
                $this->template->write_view('footer', 'Exhibitors/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
}