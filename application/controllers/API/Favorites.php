<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Favorites extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/favorites_model');
    }
    
    public function addOrRemove()
    {
    	$event_id 		= $this->input->post('event_id');
    	$user_id 		= $this->input->post('user_id');
    	$module_id 		= $this->input->post('module_id');
    	// module_type: EX = Exhibitor, SP = Sponser, SK = Speakers, AT = Attendee
    	$module_type 	= $this->input->post('module_type'); 

    	if($event_id!='' && $user_id!='' && $module_id!='' && $module_type!='')
    	{
    		$favorites_data['event_id'] 	= $event_id;
    		$favorites_data['user_id'] 		= $user_id;
    		$favorites_data['module_id'] 	= $module_id;
    		$favorites_data['module_type'] 	= $module_type;

    		$result = $this->favorites_model->addOrRemoveFavorites($favorites_data);

    		$data = array(
              'success' 	=> true,
              'message' 	=> ($result) ? 'Added successfully' : 'Removed successfully',
              'result'		=> $result,
            );
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
    	}

    	echo json_encode($data);
    }

    public function getFavoritesList()
    {
    	$event_id 		= $this->input->post('event_id');
    	$user_id 		= $this->input->post('user_id');
    	
    	if($event_id!='' && $user_id!='')
    	{
    		$where['event_id'] 	= $event_id;
    		$where['user_id'] 	= $user_id;

    		$result = $this->favorites_model->getAllFavorites($where);

    		$data = array(
              'success' 	=> true,
              'data'		=> ($result) ? $result : [],
            );
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
    	}

    	echo json_encode($data);
    }
}
?>   