<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
    function __construct() 
    {
        //error_reporting(1);
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('setting_model');
    }

    public function check() 
    {
        $email=$this->input->post('email');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
        if($email!='' && $password!='' && $event_id!='')
        {
            $user = $this->app_login_model->check_app_login($email,$password,$event_id);
            if($user == 'inactive')
            {
                $data = array(
                   'success' => false,
                   'message' => "You have entered in an incorrect username or password."
                );  
            }else if(empty($user)) 
            {

                $data = array(
                   'success' => false,
                   'message' => "You have entered in an incorrect username or password."
                );   
            } 
            else 
            {
                $user_id=$user[0]->Id;
                if(!file_exists("./assets/user_files/thumbnail/".$user[0]->Logo))
                    copy("./assets/user_files/".$user[0]->Logo,"./assets/user_files/thumbnail/".$user[0]->Logo);
                $token=$user[0]->token;
                $data1['Login_date'] = date('Y-m-d H:i:s');
                $this->app_login_model->update_token($user_id,$data1);
                if(empty($token))
                {
                     $data['token'] = sha1($email.$password.$event_id);
                     $this->app_login_model->update_token($user_id,$data);
                     //print_r($user);exit;
                     $user[0]->token=$data['token'];
                }
                if(!empty($user[0]->Country))
                {
                    $user[0]->country_name=$this->app_login_model->getCountry($data['Country']);
                }
                else
                {
                    $user[0]->country_name="";
                }
                if(!empty($user[0]->State))
                {
                    $user[0]->state_name=$this->app_login_model->getState($data['State']);
                }
                else
                {
                    $user[0]->state_name="";
                }
                $formbullder_data=$this->app_login_model->get_additionalFormData($user_id);
                if(!empty($formbullder_data))
                {
                    $data1=json_decode($formbullder_data[0]['json_data']);
                    $i=0;
                    foreach ($data1 as $key => $value) 
                    {
                        $arr[$i]["key"]=$key;
                        $arr[$i]["value"]=$value;
                        $i++;     
                    }
                }
                else
                {
                    $data1='';
                }
            
                $data = array(
                  'success' => true,
                  'data' => $user[0],
                  'extra_info'=>(!empty($arr)) ? $arr : '',
                );
            }
        }   
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function forgot_password() 
    {
        
        $is_valid_email = $this->app_login_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Invalid Email Address'
               )
            );   
        } 
        else 
        {
            $new_pass = $this->generate_password();

            $slug = "FORGOT_PASSWORD";
            $intEventId = $this->input->post('event_id');
            $em_template = $this->setting_model->front_email_template($slug,$intEventId);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = $em_template['Content'];

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $new_pass;
            $msg = preg_replace($patterns, $replacements, $msg);

            $new_pass_array['email'] = $this->input->post('email');
            $new_pass_array['new_pass'] = $new_pass;
            $this->login_model->change_pas($new_pass_array);
            if($em_template['Subject']=="")
            {
               $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = '=V8h@0rcuh#G';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $this->email->from("invite@allintheloop.com", 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            $this->email->subject($subject); //'Changed Password'
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();

            $data = array(
                'success' => true,
                'data' => array(
                    'msg' => 'Your new password has been sent to your email address.'
               )
            );
        }
        echo json_encode($data);
    }

    public function generate_password() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 9; $i++) 
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
    public function fb_signup()
    {
        $email=$this->input->post('email');
        $facebook_id=$this->input->post('facebook_id');
        $firstname=$this->input->post('first_name');
        $event_id=$this->input->post('event_id');
        $img=$this->input->post('img');
        $device=$this->input->post('device_name');

        $logo="facebook_logo".strtotime(date("Y-m-d H:i:s")).".jpeg";
        copy($img,"./assets/user_files/".$logo);
        copy($img,"./assets/user_files/thumbnail/".$logo);
        
        if($email!='' && $facebook_id!='' && $event_id!='')
        {
            $cnt=$this->app_login_model->checkEmailAlreadyByEvent($email,$event_id);
            /*if($cnt==0)
            {
               */ $data=$this->app_login_model->fb_signup($email,$facebook_id,$event_id,$firstname,$logo,$device);
                $data = array(
                  'success' => true,
                  'data' => $data,
                );
           /* }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "This email is already associated with this event",
                );
            }*/
            

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAllCountryList()
    {

        $data=$this->app_login_model->getAllCountryList();
        if(!empty($data))
        {
            $data = array(
                'success' => true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "No country available",
            );
        }
        echo json_encode($data);
    }
    public function registrationByEvent()
    {
        $email=$this->input->post('email');
        $firstname=$this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
        $country_id=$this->input->post('country');
        $company_name=$this->input->post('cmpy_name');
        $title=$this->input->post('title');
        $device=$this->input->post('device_name');
        $formbiluder_status=$this->input->post('formbiluder_status');

        if($email!='' && $firstname!='' && $event_id!='' && $last_name!='' && $country_id!='' && $password!='' && $formbiluder_status!='')
        {
            $cnt=$this->app_login_model->checkEmailAlreadyByEvent1($email,$event_id);
            if($cnt==0)
            {
                $org_id=$this->app_login_model->getOrganizerByEvent($event_id);
                $user_id=$this->app_login_model->signup($email,$firstname,$last_name,$password,$country_id,$title,$company_name,$event_id,$org_id,$device);
                $this->app_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data=$this->app_login_model->getUserDetailsId($user_id);
                if($formbiluder_status==1)
                {
                    $form_data=$this->input->post('form_data');
                    $this->app_login_model->addFormBuilderData($user_id,$form_data);
                }
                
                $data = array(
                  'success' => true,
                  'data' => $data,
                );
               
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "This email is already associated with this event",
                );
            }
            

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getFormBuillderDataByEvent()
    {
        $event_id=$this->input->post('event_id');
        if($event_id!='')
        {
            $formbullder=$this->app_login_model->getFormBuillderDataByEvent($event_id);
            if(!empty($formbullder))
            {
                $formbullder_arr=json_decode($formbullder[0]['json_data']); 
                $data = array(
                  'success' => true,
                  'status' => 1,
                  'data' => $formbullder_arr,
                );
            }
            else
            {
                $data = array(
                  'success' => true,
                  'status' => 0,
                  'message' => "No Additional Form data",
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function updateUserGCMId()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $token=$this->input->post('token');
        $gcm_id=$this->input->post('gcm_id');
        $device=$this->input->post('device');
        if($event_id!='' && $user_id!='' && $token!='' && $gcm_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_data=$this->app_login_model->updateGCMID($user_id,$gcm_id,$device);
                $data = array(
                'success' => true,
                'data' => $user_data,
                'message'=> "Successfully updated"
            );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function logout()
    {
        $user_id = $this->input->post('user_id');
        if($user_id!='')
        {
            $this->app_login_model->updateGCMID($user_id,'');
            $data = array(
              'success' => true,
              'message' => "You are now logged out"
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function linkedInSignup()
    {
        $email          = $this->input->post('email');
        $firstname      = $this->input->post('first_name');
        $last_name      = $this->input->post('last_name');
        $img            = $this->input->post('img');
        $device         = $this->input->post('device_name');
        $title          = $this->input->post('title');
        $company_name   = $this->input->post('company_name');
        $event_id       = $this->input->post('event_id');

        if($img!='')
        {
            $logo="linkedin_logo".strtotime(date("Y-m-d H:i:s")).".jpeg";
            copy($img,"./assets/user_files/".$logo);
            copy($img,"./assets/user_files/thumbnail/".$logo);
        }
        if($email!='' && $event_id!='')
        {
            $company_title = explode('at', $company_name);
            $data=$this->app_login_model->linkedInSignup($email,$facebook_id,$event_id,$firstname,$last_name,$logo,$device,($company_title[1]) ? $company_title[1] : '',($company_title[0]) ? $company_title[0] : '');
            if(!empty($data['Country']))
            {
                $data['country_name']=$this->app_login_model->getCountry($data['Country']);
            }
            else
            {
                $data['country_name']="";
            }
            if(!empty($data['State']))
            {
                $data['state_name']=$this->app_login_model->getState($data['State']);
            }
            else
            {
                $data['state_name']="";
            }
            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
