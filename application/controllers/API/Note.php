<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Note extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/note_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function note_list()
    {

        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!=''  && $token!='' && $user_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $notes = $this->note_model->getNotesListByEventId($event_id,$user_id);
                $data = array(
                    'note_list' => $notes,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function add_note()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $heading=$this->input->post('heading');
        $description=$this->input->post('description');
        if($event_id!='' && $token!='' && $user_id!='' && $heading!='' && $description!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {
                $add_arr['Heading']=$heading;
                $add_arr['Description']=$description;
                $add_arr['Created_at']=date('Y-m-d H:i:s');
                $add_arr['Event_id']=$event_id;
                $add_arr['User_id']=$user_id;
                $add_arr['Organisor_id']=$this->note_model->getOrganisorIdByUserId($user_id,$event_id);
                $notes = $this->note_model->add_note($add_arr);
                $data = array(
                  'success' => true,
                  'message' => "Successfully added"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function edit_note()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $note_id=$this->input->post('note_id');
        if($event_id!='' && $token!='' && $user_id!='' && $note_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                );   
            } 
            else 
            {

                if($this->input->post('heading'))
                {
                    $update_arr['Heading']=$this->input->post('heading');
                }
                if($this->input->post('description'))
                {
                    $update_arr['Description']=$this->input->post('description');
                }
                if(!empty($update_arr))
                {
                    $notes = $this->note_model->update_note($note_id,$update_arr);
                }
                
                $data = array(
                  'success' => true,
                  'message' => "Successfully updated"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
