<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Document extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/document_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function documents_list()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $folder_list = $this->document_model->getFoldersByEventId($event_id);
                $file_list = $this->document_model->getFilesByEventId($event_id);
                if(empty($folder_list))
                {
                    $folder_list=array([0]=>"");
                }
                if(empty($file_list))
                {
                    $file_list=array([0]=>"");
                }
                $data = array(
                    'folder_list' => $folder_list,
                    'file_list' => $file_list,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function folder_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $folder_id=$this->input->post('folder_id');
        if($event_id!='' && $event_type!='' && $folder_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $file_list = $this->document_model->getFilesByEventId($event_id,$folder_id);
                if(empty($file_list))
                {
                    $file_list=array([0]=>"");
                }
                $data = array(
                    'file_list' => $file_list,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

}
