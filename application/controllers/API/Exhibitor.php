<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exhibitor extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/exhibitor_model');
        $this->load->model('API/message_model');
        // #share_contact
        $this->load->model('API/attendee_model');
        $this->load->library('Gcm');

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function exhibitor_list()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->exhibitor_model->getUserIdByToken($token);
                $exhibitor_list = $this->exhibitor_model->getExhibitorListByEventId($event_id,$user_id);
                $meeting_data = $this->exhibitor_model->getAllMeetingRequest1($event_id,$token);
                $data = array(
                    'exhibitor_list' => $exhibitor_list,
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'menu' => $this->menu_list,

                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function exhibitor_view()
    {
        //error_reporting(E_ALL);
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $exhibitor_page_id=$this->input->post('exhibitor_page_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $exhibitor_id!=''  && $exhibitor_page_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $exhibitor = $this->exhibitor_model->getExhibitorDetails($exhibitor_page_id,$user_id,$event_id);
                $event = $this->event_model->getEventData($event_id);
                if(!empty($exhibitor[0]['company_logo']))
                {
                    $company_logo_decode=json_decode($exhibitor[0]['company_logo']);

                    $exhibitor[0]['company_logo']=($company_logo_decode) ? $company_logo_decode[0] : "" ;
                }

                if(!empty($exhibitor[0]['Images']))
                {
                    if($exhibitor[0]['Images']=="[]")
                    {

                        $exhibitor[0]['Images']=[];
                    }
                    else
                    {
                        $image_decode=json_decode($exhibitor[0]['Images']);
                        
                        $exhibitor[0]['Images']=($image_decode) ? $image_decode : [];
                    }
                    
                }
                // #share_contact
                $where['event_id'] = $event_id;
                $where['from_id'] = $user_id;
                $where['to_id'] = $exhibitor_page_id;
                $exhibitor[0]['approval_status'] = ($exhibitor_page_id != $user_id) ?  $this->attendee_model->getApprovalStatus($where,$event_id) : '';
                $where1['to_id'] = $user_id;
                $exhibitor[0]['share_details'] = ($user_id == $exhibitor_page_id) ? $this->attendee_model->getShareDetails($where1,$event_id) : [] ;
                $exhibitor[0]['contact_details'] = ($exhibitor[0]['approval_status'] == '1') ? $this->attendee_model->getAttendeeConatctDetails($user_id) : [] ;
                // #share_contact
                $limit=10;
                $message = $this->message_model->view_private_msg_coversation($user_id,$exhibitor_id,$event_id,$page_no,$limit);
                $total_pages=$this->message_model->total_pages($user_id,$exhibitor_id,$event_id,$limit);

                $data = array(
                    'exhibitor_details' => $exhibitor[0] ? $exhibitor[0] : new stdClass,
                    'message' => $message,
                    'hide_request_meeting' => $event['hide_request_meeting'],
                    'allow_msg_user_to_exhibitor' => $event['allow_msg_user_to_exhibitor'],
                    'total_pages' => $total_pages,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function sendMessage()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $message=$this->input->post('message');
        if($event_id!='' && $token!='' && $exhibitor_id!='' && $user_id!='' && $message!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data['Message']=$message;
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$exhibitor_id;
                $message_data['Parent']=0;
                $message_data['image']="";
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']='0';
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    $this->message_model->add_msg_hit($user_id,$current_date,$exhibitor_id,$event_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Successfully Saved",
                        'message_id' => $message_id
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function msg_images_request()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $exhibitor_id=$this->input->post('exhibitor_id');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $exhibitor_id!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data1=$this->message_model->getMessageDetails($message_id);

                $newImageName = round(microtime(true) * 1000).".jpeg";
                $target_path= "././assets/user_files/".$newImageName; 
                $target_path1= "././assets/user_files/thumbnail/";                 
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $update_data['image'] =$newImageName;
                if($message_data1[0]['image']!='')
                {
                    $arr=json_decode($message_data1[0]['image']);
                    $arr[]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                else
                {

                    $arr[0]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                $a[0]=$newImageName;
                $message_data['Message']='';
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$exhibitor_id;
                $message_data['Parent']=$message_id;
                $message_data['image']=json_encode($a);
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']='1';
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    //$this->message_model->add_msg_hit($user_id,$current_date,$attendee_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_message()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function make_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($_FILES['image']['name']!='')
                {
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    if($newImageName!='')
                    {
                        $arr[0]=$newImageName;
                        $comment_arr['image']=json_encode($arr);
                    }
                }
                $comment_arr['comment']=$comment;
                $comment_arr['user_id']=$user_id;
                $comment_arr['msg_id']=$message_id;
                $comment_arr['Time']=date("Y-m-d H:i:s");;
                $this->message_model->make_comment($comment_arr,$event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $comment_id=$this->input->post('comment_id');
        if($event_id!='' && $token!='' && $comment_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $exhibitor_id=$this->input->post('exhibitor_id');
        if($event_id!='' && $exhibitor_id!='')
        {
            
                $images = $this->exhibitor_model->getImages($event_id,$exhibitor_id);
                if($images != ''){
                     $data = array(
                    'images' => json_decode($images),
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data
                    );
                }else{
                     $data = array(
                          'success' => false,
                          'message' => "No images found"
                        );
                }
               
         }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function exhibitorSearch()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $keywords=$this->input->post('keywords');
        if($event_id!=''  && $keywords!='')
        {
            $user_id = $this->exhibitor_model->getUserIdByToken($token);
            $data = $this->exhibitor_model->getSerachableRecords($event_id,$keywords,$user_id);
            $meeting_data = $this->exhibitor_model->getAllMeetingRequest1($event_id,$token);
            $data = array(
                'exhibitor_list' => $data,
                'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingDateTime()
    {
        $event_id = $this->input->post('event_id');
        if($event_id!='')
        {
            $data = $this->exhibitor_model->getDateTimeArray($event_id);
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function requestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $exhibitor_id   = $this->input->post('exhibitor_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = $this->input->post('time');

        if($event_id!='' && $exhibitor_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $data['event_id'] = $event_id;
            $data['exhibiotor_id'] = $exhibitor_id;
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $data['date'] = $date->format('Y-m-d');
            $data['time'] = $time;

            $check_session = $this->exhibitor_model->checkSessionClash($attendee_id,$date,$time);
            if($check_session['result'])
            {
                $result = $this->exhibitor_model->saveRequest($data);
                if($result)
                {
                    $url=base_url().$this->exhibitor_model->getUrlData($event_id);
                    $user = $this->exhibitor_model->getUser($exhibitor_id);
                    $Message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".ucfirst($user['Lastname'])." has requested a meeting with you at ".$time." on ".$date." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                    $message1 = ucfirst($user['Firstname'])." ".ucfirst($user['Lastname'])." has requested a meeting with you at ".$time." on ".$date.".";
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $user['Id'],
                        'Event_id'=>$event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s"),
                        'ispublic' => '0',
                        'msg_creator_id'=>$attendee_id,
                        'msg_type'  => '2',
                      );
                    $this->exhibitor_model->saveSpeakerMessage($data1);
                    if($user['gcm_id']!='')
                    {
                        $obj = new Gcm();
                        $extra['message_type'] = 'RequestMeeting';
                        $extra['message_id'] = $user['Id'];
                        $extra['event'] = $this->exhibitor_model->getEventName($event_id);
                        if($user['device'] == 'Iphone')
                        {
                            $msg = $message1;
                            $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        else
                        {
                            $msg['title'] = "Meeting Request";
                            $msg['message'] = $message1;
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }

                    }
                    $data = array(
                      'success' => true,
                      'message' => "Your request was sent successfully",
                      'flag'	=> '1',
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
            }
            else
            {
                $session = $check_session['agenda_name'];
                $data = array(
                  'success' => false,
                  'message' => "This will clash with $session - would you like to proceed?",
                  'flag'	=> '0',
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }
    public function saveRequestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $exhibitor_id   = $this->input->post('exhibitor_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = $this->input->post('time');

        if($event_id!='' && $exhibitor_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $user = $this->exhibitor_model->getUser($exhibitor_id);
            $exibitor = $this->exhibitor_model->getExhibitor($exhibitor_id);

            $data['event_id'] = $event_id;
            $data['exhibiotor_id'] = $exhibitor_id;
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $data['date'] = $date->format('Y-m-d');
            $data['time'] = $time;

            $result = $this->exhibitor_model->saveRequest($data);
            if($result)
            {

            $url=base_url().$this->exhibitor_model->getUrlData($event_id);
                
                $Message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".ucfirst($user['Lastname'])." has requested a meeting with you at ".$time." on ".$date." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                $message1 = ucfirst($user['Firstname'])." ".ucfirst($user['Lastname'])." has requested a meeting with you at ".$time." on ".$date.".";
                $data1 = array(
                    'Message' => $Message,
                    'Sender_id' => $attendee_id,
                    'Receiver_id' => $exibitor['user_id'],
                    'Event_id'=>$event_id,
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s"),
                    'ispublic' => '0',
                    'msg_creator_id'=>$attendee_id,
                    'msg_type'  => '2',
                  );
                    $this->exhibitor_model->saveSpeakerMessage($data1);
                    if($user['gcm_id']!='')
                    {
                        $obj = new Gcm();
                        $extra['message_type'] = 'RequestMeeting';
                        $extra['message_id'] = $user['Id'];
                        $extra['event'] = $this->exhibitor_model->getEventName($event_id);
                        if($user['device'] == 'Iphone')
                        {
                            $msg = $message1;
                            $req = $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        else
                        {
                            $msg['title'] = "Meeting Request";
                            $msg['message'] = $message1;
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $req = $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        //print_r($req);exit;

                    }
                    $data = array(
                      'success' => true,
                      'message' => "Your request was sent successfully",
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }
    public function getAllMeetingRequest()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->exhibitor_model->getAllMeetingRequest($event_id,$user_id);
            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequest()
    {
        $id = $this->input->post('request_id');
        $exhibitor_id = $this->input->post('exhibitor_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id'); 

        if($status!='' && $id!='' && $exhibitor_id!='' && $user_id!='')
        {
            $update_data['status'] = $status;
            $where['Id'] = $id;
            $result_data = $this->exhibitor_model->updateRequest($update_data,$where);
            $exhibitor = $this->exhibitor_model->getExhibitor($exhibitor_id);
            if($status == '2')
            {
                $message=ucfirst($exhibitor['Heading'])." has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message=ucfirst($exhibitor['Heading'])." has accepted your Meeting Request.";
            }
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $result_data['attendee_id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id
            );
            $this->exhibitor_model->saveSpeakerMessage($data1);
            $user = $this->exhibitor_model->getUsersData($result_data['attendee_id']);
            if($user['gcm_id']!='')
            {
                $obj = new Gcm();
                $extra['message_type'] = 'RespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->exhibitor_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $msg = $message;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Meeting Request";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }

            }
            $data = array(
              'success' => true,
              'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully'  ,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function suggestMeetingTime()
    {
        $date = json_decode($this->input->post('date'),true);
        $time = json_decode($this->input->post('time'),true);
        $id = $this->input->post('request_id');
        $exhibitor_id = $this->input->post('exhibitor_id');
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 

        if($date!=''&& $time!='' && $id!='' && $exhibitor_id!='' && $user_id!='' && $event_id!='')
        {
            
            $exhibitor = $this->exhibitor_model->getExhibitor($exhibitor_id);
            $user = $this->exhibitor_model->getAttenee($id);
            $message=$exhibitor['Heading']." is unable to meet with you at the time you specified. ".$exhibitor['Heading']." has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";

            foreach ($date as $key => $value) {
              $date=date('Y-m-d H:i',strtotime($value.' '.$time[$key]));
              $link=base_url().$this->exhibitor_model->getSuggestUrlData($event_id).'changemeetingdate/'.strtotime($date).'/'.$id;
              $message.="<a href='".$link."'>".$date."</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['exhibitor_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->exhibitor_model->saveSuggestedDate($suggest);
            }
            $message1 = $exhibitor['Heading']." is unable to meet with you at this time.Please click here.";
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $user['Id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
              'msg_type'  => '4',
            );
            $this->exhibitor_model->saveSpeakerMessage($data1);

            
            if($user['gcm_id']!='')
            {
                $obj = new Gcm();
                $extra['message_type'] = 'SuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->exhibitor_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $msg = $message1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }

            }
            $data = array(
              'success' => true,
              'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getSuggestedTimings()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $meeting_id = $this->input->post('meeting_id');

        if($user_id!='' && $event_id!='')
        {
            $where['attendee_id'] = $user_id;
            $where['sm.event_id'] = $event_id;
            $where['e.event_id'] = $event_id;
            if($meeting_id!='')
            $where['meeting_id'] = $meeting_id;

            $available_times = $this->exhibitor_model->getAvailableTimes($where);
            foreach ($available_times as $key => $value) {
                $available_times[$key]['date_time'] = date("H:i l, m/d/Y",strtotime($value['date_time']));
                $available_times[$key]['api_date_time'] = date("H:i m/d/Y",strtotime($value['date_time']));
            }
            $data = array(
              'success' => true,
              'data' => $available_times,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function bookSuggestedTime()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $suggested_id = $this->input->post('suggested_id');
        $date_time = $this->input->post('date_time');

        if($user_id!='' && $event_id!='' && $suggested_id!='' && $date_time!='')
        {
            $where['attendee_id'] = $user_id;
            $where['event_id'] = $event_id;
            $where['Id'] = $suggested_id;

            $user = $this->exhibitor_model->bookSuggestedTime($where,$date_time);
            if(!empty($user))
            {
                $attendee = $this->exhibitor_model->getUsersData($user_id);
                if($attendee['title']!='')
                    $message1 = ucfirst($attendee['Firstname']) ." ".ucfirst($attendee['Lastname'])." at ".$attendee['title'] ." has accepted your new suggested time of $date_time. This meeting has now been booked.";
                else
                    $message1 = ucfirst($attendee['Firstname']) ." ".ucfirst($attendee['Lastname'])." has accepted your new suggested time of $date_time. This meeting has now been booked.";

                $data1 = array(
                  'Message' => $message1,
                  'Sender_id' => $user_id,
                  'Receiver_id' => $user['Id'],
                  'Event_id'=>$event_id,
                  'Parent' => '0',
                  'Time' => date("Y-m-d H:i:s"),
                  'ispublic' => '0',
                  'msg_creator_id'=>$user_id
                );
                $this->exhibitor_model->saveSpeakerMessage($data1);

                if($user['gcm_id']!='')
                {
                    $obj = new Gcm();
                    $extra['message_type'] = 'BookedMeeting';
                    $extra['message_id'] = $id;
                    $extra['event'] = $this->exhibitor_model->getEventName($event_id);
                    if($user['device'] == 'Iphone')
                    {
                        $msg = $message1;
                        $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                    }
                    else
                    {
                        $msg['title'] = "Booked Meeting";
                        $msg['message'] = $message1;
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                    }
                }
            }
            $data = array(
              'success' => true,
              'message' => 'Your meeting booked successfully.',
            );

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function shareContactInformation()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $to_user_id=$this->input->post('to_user_id');
        if($event_id!='' && $user_id!='' && $to_user_id!='')
        {
            $where['event_id']         = $event_id;
            $where['from_id']          = $user_id;
            $where['to_id']            = $to_user_id;

            $share_data['event_id']         = $event_id;
            $share_data['from_id']          = $user_id;
            $share_data['to_id']            = $to_user_id;
            $share_data['approval_status']  ='0';
            $share_data['contact_type']  ='1';
            $share_data['share_contact_datetime']  =date('Y-m-d H:i:s');
            
            $id = $this->attendee_model->saveShareContactInformation($where,$share_data);

            $url_data = $this->attendee_model->getUrlData($event_id);
            $url=base_url().$url_data.$id;

            $user = $this->attendee_model->getUser($user_id);

            $message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".$user['Lastname']." has shared their contact details with you. Tap here to share your contact details with ".ucfirst($user['Firstname'])." ".$user['Lastname']." to reveal their details</a>";
           
            $message_data['Message']=$message;
            $message_data['Sender_id']=$user_id;
            $message_data['Receiver_id']=$to_user_id;
            $message_data['Event_id']=$event_id;
            $message_data['Parent']='0';
            $message_data['Time']=date("Y-m-d H:i:s");
            $message_data['ispublic']='0';
            $message_data['msg_creator_id']=$user_id;
            $message_data['msg_type']='1';

            $this->message_model->savePublicMessage($message_data);

            $attendee = $this->attendee_model->getUser($to_user_id);
            if($attendee['gcm_id']!='')
            {
                
                $obj = new Gcm();
                $message1=ucfirst($user['Firstname'])." ".$user['Lastname']." has shared their contact details with you. Tap here to share your contact details with ".ucfirst($user['Firstname'])." ".$user['Lastname']." to reveal their details";
                $extra['message_type'] = 'Attendee';
                $extra['message_id'] = $user_id;
                $extra['event'] = $this->settings_model->event_name($event_id);
                if($attendee['device'] == 'Iphone')
                {
                    $msg =  $message1;
                    $result = $obj->send_notification($attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }   
                else
                {
                    $msg['title'] = "Share Contact";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result = $obj->send_notification($attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }
               // print_r($result);exit;
            }
            $data = array(
              'success' => true,
              'message' => 'You have successfully shared your information.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*public function approveStatus()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $to_user_id=$this->input->post('to_user_id');

        if($event_id!='' && $user_id!='' && $to_user_id!='')
        {
            
            $where['event_id']         = $event_id;
            $where['from_id']          = $to_user_id;
            $where['to_id']            = $user_id;

            $share_data['approval_status']  = '1';
            
            $this->attendee_model->saveShareContactInformation($where,$share_data);

            $data = array(
              'success' => true,
              'message' => 'Successfully approved.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function rejectStatus()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $to_user_id=$this->input->post('to_user_id');

        if($event_id!='' && $user_id!='' && $to_user_id!='')
        {
            
            $where['event_id']         = $event_id;
            $where['from_id']          = $to_user_id;
            $where['to_id']            = $user_id;

            $share_data['approval_status']  = '2';
            
            $this->attendee_model->saveShareContactInformation($where,$share_data);

            $data = array(
              'success' => true,
              'message' => 'Successfully rejected.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
}