<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/event_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/note_model');
        $this->load->model('API/event_template_model');

        $this->events = $this->event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images'] = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        
        foreach ($this->cmsmenu as $key => $values) 
        {
            if(!empty($values['Images']))
            {
                $cmsbannerimage_decode = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Images'] = "";
            }
            if(!empty($values['Logo_images']))
            {
                $cmslogoimage_decode = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
    }

    public function index()
    {

        $user = $this->app_login_model->check_token_with_event($this->input->post('_token'),$this->input->post('event_id'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $fetureproduct = $this->event_model->getFetureProduct($this->input->post('event_id'));

            $data = array(
                'events' => $this->events,
                'menu_list' => $this->menu_list,
                'cmsmenu' => $this->cmsmenu
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function event_id()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
                if(!empty($event_list))
                {
                    //$event_list[0]['checkbox_values'] = explode(',', $event_list[0]['checkbox_values']);
                    if($event_id == '353')
                    {
                        $event_list[0]['Background_color']=='#357055';
                    }
                    if($event_list[0]['fun_background_color']==0)
                    {
                         $event_list[0]['fun_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_text_color']==0)
                    {
                         $event_list[0]['fun_top_text_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                    }
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_block_background_color']==0)
                    {
                         $event_list[0]['fun_block_background_color']="#FFFFFF";
                    }
                    if($event_list[0]['fun_top_background_color']==0)
                    {
                         $event_list[0]['fun_top_background_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                    }
                    if($event_list[0]['theme_color']==0)
                    {
                         $event_list[0]['theme_color1']="#FFFFFF";
                    }
                    else
                    {
                         $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                    }
                    if($event_list[0]['fun_block_text_color']==0)
                    {
                         $event_list[0]['fun_block_text_color']="#FFFFFF";
                    }
                    
                    if($event_list[0]['fun_footer_background_color']==0)
                    {
                         $event_list[0]['fun_footer_background_color']="#FFFFFF";
                    }
                    $img=json_decode($event_list[0]['Images']);
                    $Logo_images=json_decode($event_list[0]['Logo_images']);
                    $Background_img=json_decode($event_list[0]['Background_img']);
                    if(empty($img[0]))
                    {
                        $img[0]="";
                    }
                    if(empty($Logo_images[0]))
                    {
                        $Logo_images[0]="";
                    }
                    if(empty($Background_img[0]))
                    {
                        $Background_img[0]="";
                    }
                    
                    $event_list[0]['Images']=$img[0];
                    $event_list[0]['Logo_images1']=$Logo_images[0];
                    $event_list[0]['Background_img1']=$Background_img[0];
                    $event_list[0]['description1']=$event_list[0]['Description'];
                    $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];
                    
                    $checkbox_values = $event_list[0]['checkbox_values'];
                    $menu_array = explode(',', $checkbox_values);
                   
                    $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                    $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;

                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                        if(count($note_data)>0)
                        {
                             $note_status=1;
                        }
                        else
                        {
                             $note_status=0;
                        }

                    }
                    
                    $data1 = array(
                        'events' => $event_list,
                        'menu_list' => $this->menu_list,
                        'cmsmenu' => $this->cmsmenu,
                        'note_status' => $note_status
                    );
                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   
}
