<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sponsors extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/sponsors_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function sponsors_list()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->sponsors_model->getUserId($token);
                $sponsors_list = $this->sponsors_model->getSponsorsListByEventId($event_id,$user_id);
                $data = array(
                    'sponsors_list' => $sponsors_list,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function sponsors_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $sponsor_id=$this->input->post('sponsor_id');
        if($event_id!='' && $event_type!='' && $sponsor_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $sponsors = $this->sponsors_model->getSponsorsDetails($sponsor_id,$event_id,$token);
                if(!empty($sponsors[0]))
                {
                    if(!empty($sponsors[0]['company_logo']))
                    {
                        $company_logo_decode=json_decode($sponsors[0]['company_logo']);
                        $sponsors[0]['company_logo']=($company_logo_decode[0]) ? $company_logo_decode[0] : '';
                    }

                    if(!empty($sponsors[0]['Images']))
                    {
                        if($sponsors[0]['Images']=="[]")
                        {

                            $sponsors[0]['Images']=[];
                        }
                        else
                        {

                            $image_decode=json_decode($sponsors[0]['Images']);
                            $sponsors[0]['Images']=($image_decode) ? : [] ;
                        }
                        
                    }
                }
                $data = array(
                    'sponsors_details' => ($sponsors[0]) ? $sponsors[0] : new stdClass,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

}
