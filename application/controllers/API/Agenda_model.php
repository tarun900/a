<?php 
class agenda_model extends CI_Model{
    function __construct()
    {
        
            parent::__construct();

    }
    public function checkAgendaByUserId($agenda_id,$user_id)
    {
          $this->db->select('*');
          $this->db->from('users_agenda');
          $this->db->where('user_id', $user_id);
          /*$this->db->where_in('agenda_id', $agenda_id);*/
          $query = $this->db->get();
          $res = $query->result_array();
          if(!empty($res))
          {
                $agenda_id_str=$res[0]['pending_agenda_id'];
                $arr_pagenda_id=explode(',', $agenda_id_str);

                $agenda_id_str=$res[0]['agenda_id'];
                $arr_agenda_id=explode(',', $agenda_id_str);

                $agenda_id_str=$res[0]['check_in_agenda_id'];
                $arr_cagenda_id=explode(',', $agenda_id_str);

                if(in_array($agenda_id,$arr_pagenda_id))
                {
                    return "pending_1";
                }
                elseif(in_array($agenda_id,$arr_agenda_id))
                {
                   return "save_1";
                }
                else
                {
                    return "0";
                }
          }
          else
          {
            return "0";
          }
          exit();
    }
    public function getAgendaByUserId($user_id)
    {
        $this->db->select('agenda_id');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
         
    }
    public function getCheckInAgendaByUserId($user_id){
        $this->db->select('check_in_agenda_id');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
        $this->db->where('check_in_agenda_id !=', "");
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function updateUserAgenda($user_id,$agenda)
    {
        $this->db->where("user_id",$user_id);
        $this->db->update("users_agenda",$agenda);
    }
    public function addUserAgenda($data)
    {
        $this->db->select('*');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->get();
        $res = $query->row();
        if(count($res)){
            $this->db->where("user_id",$data['user_id']);
            $this->db->update("users_agenda",$data);
        }else{
            $this->db->insert("users_agenda",$data);
        }
    }
    public function getAllAgendaByTimeEvent($event_id,$id_arr,$user_id)
    {
        $checkAgneda = $this->checkAgneda($event_id);
        
        if(count($checkAgneda) > 1){
            $agenda_array = $this->getAgendaArray($user_id);
        }else{
           $agenda_array = $checkAgneda[0]['agenda_id']; 
        }
    
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title',false);
        $this->db->from('agenda a');
    if(!empty($agenda_array)){
        $this->db->where_in('a.Id',$agenda_array);
    }
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
    
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            if($res[$i]->Start_date!=$prev)
            {
                $k=0;
                $myarr[$j]["date"]=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j]["data"][$k]['time_zone']= $timezone;
                $j++;
               
            }
            else
            {
               
                $myarr[$j-1]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j-1]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j-1]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j-1]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j-1]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j-1]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j-1]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j-1]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j-1]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j-1]["data"][$k]['time_zone']= $timezone;
            }
            $k++;
            $prev=$res[$i]->Start_date;
            
        }
        
        
        
        return $myarr;
    }
    public function getAllAgendaByTime($event_id,$id_arr,$user_id)
    {
        
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title',false);
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
        $this->db->where('e.Id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $query = $this->db->get();
        $res = $query->result();
        $agendas = array();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
           $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            if($res[$i]->Start_date!=$prev)
            {
                $k=0;
                $myarr[$j]["date"]=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j]["data"][$k]['time_zone']= $timezone;
                $j++;
               
            }
            else
            {
               
                $myarr[$j-1]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j-1]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j-1]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j-1]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j-1]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j-1]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j-1]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j-1]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j-1]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j-1]["data"][$k]['time_zone']= $timezone;
            }
            $k++;
            $prev=$res[$i]->Start_date;
            
        }
        
        
        
        return $myarr;
    }
    public function checkAgneda($event_id){
        $this->db->select('ac.Id,aci.agenda_id');
        $this->db->from('agenda_categories ac');
        $this->db->where('ac.event_id',$event_id);
        $this->db->join('agenda_category_relation aci', 'aci.category_id = ac.Id');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getAgendaArray($user_id){
            $this->db->select('aci.agenda_id,');
            $this->db->from('attendee_agenda_relation ar');
            $this->db->where('ar.attendee_id',$user_id);
            $this->db->join('agenda_category_relation aci', 'aci.category_id = ar.agenda_category_id');
            $query = $this->db->get();
            $res = $query->result_array();
            $agenda_array = array();
            foreach ($res as $key => $value) {
                    $agenda_array[] = $value['agenda_id'];
                }
        return $agenda_array;        
    }
    public function getAgendaById($eid = NULL, $aid = NULL,$user_id = null)
    {
        $this->db->select('a.*,CASE WHEN doc.document_file IS NULL THEN "" ELSE doc.document_file END as document_file,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE m.Map_title END as Map_title,CASE WHEN m.Address IS NULL THEN "" ELSE m.Address END as Map_address,CASE WHEN m.Images IS NULL THEN "" ELSE m.Images END as Images,CASE WHEN a.Start_date IS NULL THEN "" ELSE a.Start_date END as Start_date,CASE WHEN a.Start_time IS NULL THEN "" ELSE a.Start_time END as Start_time,CASE WHEN a.End_date IS NULL THEN "" ELSE a.End_date END as End_date,CASE WHEN a.End_time IS NULL THEN "" ELSE a.End_time END as End_time,CASE WHEN a.checking_datetime IS NULL THEN "" ELSE a.checking_datetime END as checking_datetime,CASE WHEN a.Maximum_People IS NULL THEN "" ELSE a.Maximum_People END as Maximum_People,CASE WHEN a.Heading IS NULL THEN "" ELSE a.Heading END as Heading,CASE WHEN a.Types IS NULL THEN "" ELSE a.Types END as Types,CASE WHEN a.Agenda_status IS NULL THEN "" ELSE a.Agenda_status END as Agenda_status,CASE WHEN a.Speaker_id IS NULL THEN "" ELSE a.Speaker_id END as Speaker_id,CASE WHEN a.document_id IS NULL THEN "" ELSE a.document_id END as document_id,CASE WHEN a.presentation_id IS NULL THEN "" ELSE a.presentation_id END as presentation_id,CASE WHEN a.Address_map IS NULL THEN "" ELSE a.Address_map END as Address_map,CASE WHEN a.other_types IS NULL THEN "" ELSE a.other_types END as other_types,CASE WHEN a.short_desc IS NULL THEN "" ELSE a.short_desc END as short_desc,CASE WHEN a.description IS NULL THEN "" ELSE a.description END as description,CASE WHEN e.Event_show_time_zone IS NULL THEN "" ELSE e.Event_show_time_zone END as Event_show_time_zone',false);
        $this->db->from('agenda a');
        $this->db->join('event e', 'e.Id = a.Event_id');
        //$this->db->join('user_session_rating usr', 'usr.user_id = a.Organisor_id','left');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        $this->db->join('document_files doc', 'doc.document_id = a.document_id','left');
        $this->db->where('e.Id',$eid);
        $this->db->where('a.Id',$aid);
        $query = $this->db->get();
        $res = $query->result_array();
        if(!empty($res)) {
            $show_check_in = $res[0]['show_checking_in'];
            $arr=explode(",",$res[0]['Speaker_id']);
            for($i=0;$i<count($arr);$i++)
            {
                $this->db->select('Id as user_id,Logo,Firstname,Lastname');
                $this->db->from('user');
                $this->db->where('Id',$arr[$i]);
                $q=$this->db->get();
                $result=$q->result_array();
                $info[$i]=$result[0];
            }
            $arr=json_decode($res[0]['Map_image']);
            if(empty($arr))
            {
                $res[0]['Map_image']='';
            }
            else
            {
                $res[0]['Map_image']=$arr;
            }
           
            if(empty($info[0]))
            {

                $res[0]['speaker']=array([0]=>"");
            }
            else
            {
                 $res[0]['speaker']=$info;
            }
           
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
            $res[0]['time_zone']=$timezone;
            
            $this->db->select('*')->from('users_agenda');
            $this->db->where('user_id',$user_id);
            $query=$this->db->get();
            $result=$query->result_array();
            $agenda_ids = explode(',', $result[0]['agenda_id']);
            $check_in = explode(',', $result[0]['check_in_agenda_id']);
            $res[0]['is_agenda_saved'] = in_array($aid, $agenda_ids) ? true : false;
            $res[0]['is_checked_in'] = in_array($aid, $check_in) ? true : false;
                    
            $this->db->select('rating')->from('user_session_rating');
            $this->db->where('user_id',$user_id);
            $this->db->where('session_id',$aid);
            $query=$this->db->get();
            $result=$query->row();
            $res[0]['rating'] = ($result->rating) ? $result->rating : ""; 
        }
        
        
        return $res;
    }
    public function getTimeFormat($eid)
    {
      $this->db->select('format_time')->from('fundraising_setting');
      $this->db->where('Event_id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res;
    }
    public function getTimeZone($eid)
    {
      $this->db->select('Event_time_zone')->from('event');
      $this->db->where('Id',$eid);
      $query=$this->db->get();
      $res=$query->result_array();
      $time = $res[0]['Event_time_zone'];

      $this->db->select('Name')->from('timezone');
      $this->db->where('GMT',$time);
      $query=$this->db->get();
      $res=$query->result_array();
      return $res[0]['Name'];

    }
    public function get_agenda_types($Subdomain=null)
    {
        $this->db->select('Id');
        $this->db->from('event');
        $this->db->where('Subdomain',$Subdomain);
        $query = $this->db->get();
        $res = $query->result_array();

        $user = $this->session->userdata('current_user');
        $orid = $user[0]->Id;
        $this->db->select('Types,other_types');
        $this->db->from('agenda');
        $this->db->where('Event_id',$res[0]['Id']);
        $this->db->where('Organisor_id',$orid);
        $this->db->where("End_date >= DATE_FORMAT(NOW(),'%Y-%m-%d')");
        $this->db->order_by('Start_date asc');
        $this->db->order_by('Start_time asc');
        $this->db->group_by("Types"); 
        $query1 = $this->db->get();
        $res1 = $query1->result_array(); 
        return $res1;

    }

    public function getAllAgendaByTypeEvent($event_id=null,$id=null,$id_arr=null,$user_id)
    {
         $checkAgneda = $this->checkAgneda($event_id);
        if(count($checkAgneda) > 1){
            $agenda_array = $this->getAgendaArray($user_id);
        }else{
           $agenda_array = $checkAgneda[0]['agenda_id']; 
        }
        $id = $this->uri->segment(4);
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title',false);
        $this->db->from('agenda a');
        if(!empty($agenda_array)){
        $this->db->where_in('a.Id',$agenda_array);
    }
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         
        $this->db->where('a.Event_id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           
            if($res[$i]->Types!=$prev)
            {
               
                $k=0;
                if($res1[$i]->Types == 'Other')
                {
                   $myarr[$j]["Types"]=$res[$i]->Types;
                }
                $myarr[$j]["Types"]=$res[$i]->Types;
                $myarr[$j]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j]["data"][$k]['Types']=$res[$i]->Types;
                $myarr[$j]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j]["data"][$k]['time_zone']=$timezone;
                $j++;
               
            }
            else
            {
               
                $myarr[$j-1]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j-1]["data"][$k]['Types']=$res[$i]->Types;
                $myarr[$j-1]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j-1]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j-1]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j-1]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j-1]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j-1]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j-1]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j-1]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j-1]["data"][$k]['time_zone']=$timezone;

            }
            $k++;
            $prev=$res[$i]->Types;
            
        }
        return $myarr;
    }
    public function getAllAgendaByType($event_id=null,$id=null,$id_arr=null,$user_id)
    {
         
        $id = $this->uri->segment(4);
        $this->db->select('a.*,CASE WHEN m.Id IS NULL THEN "" ELSE m.Id END as Address_map_id,CASE WHEN m.Map_title IS NULL THEN "" ELSE Map_title END as Map_title',false);
        $this->db->from('agenda a');
        $this->db->join('map m', 'm.Id = a.Address_map','left');
        if(!empty($id_arr))
        {
            $this->db->where_in('a.Id',$id_arr);
        }
         
        $this->db->where('a.Event_id',$event_id);
        $this->db->where("a.End_date >= ".date('Y-m-d'));
        $this->db->order_by('a.Start_date asc');
        $this->db->order_by('a.Start_time asc');
        $this->db->where('a.Types',$id);
        $query1 = $this->db->get();
        $res = $query1->result();
        $prev="";
        $j=0;
        $k=0;
        for($i=0; $i<count($res); $i++)
        {
            $timezone = ($this->getTimeZone($event_id)) ? $this->getTimeZone($event_id) : "" ;
           
            if($res[$i]->Types!=$prev)
            {
               
                $k=0;
                if($res1[$i]->Types == 'Other')
                {
                   $myarr[$j]["Types"]=$res[$i]->Types;
                }
                $myarr[$j]["Types"]=$res[$i]->Types;
                $myarr[$j]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j]["data"][$k]['Types']=$res[$i]->Types;
                $myarr[$j]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j]["data"][$k]['time_zone']=$timezone;
                $j++;
               
            }
            else
            {
               
                $myarr[$j-1]["data"][$k]['Id']=$res[$i]->Id;
                $myarr[$j-1]["data"][$k]['Types']=$res[$i]->Types;
                $myarr[$j-1]["data"][$k]['Heading']=$res[$i]->Heading;
                $myarr[$j-1]["data"][$k]['Start_date']=$res[$i]->Start_date;
                $myarr[$j-1]["data"][$k]['Start_time']=$res[$i]->Start_time;
                $myarr[$j-1]["data"][$k]['End_date']=$res[$i]->End_date;
                $myarr[$j-1]["data"][$k]['End_time']=$res[$i]->End_time;
                $myarr[$j-1]["data"][$k]['Address_map']=$res[$i]->Address_map_id;
                $myarr[$j-1]["data"][$k]['Map_title']=$res[$i]->Map_title;
                $myarr[$j-1]["data"][$k]['Event_id']=$res[$i]->Event_id;
                $myarr[$j-1]["data"][$k]['time_zone']=$timezone;

            }
            $k++;
            $prev=$res[$i]->Types;
            
        }
        return $myarr;
    }
    public function deleteUserAgenda($user_id)
    {
        $this->db->where("user_id",$user_id);
        $this->db->delete("users_agenda");
    }

    public function getRatingDataByUserId($user_id){
        $this->db->select('session_id')->from('user_session_rating');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
        $result = $query->result_array(); 
        return $result;
    }
    public function getAgendaNameTimeById($id){
        $this->db->select('Heading,Start_time')->from('agenda');
        $this->db->where_in('Id',$id);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }
    public function getRatingData($user_id,$agenda_id){
        $this->db->select('*')->from('user_session_rating');
        $this->db->where('user_id',$user_id);
        $this->db->where('session_id',$agenda_id);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }
    public function updateRating($data,$id){
        $this->db->where('Id',$id);
        $this->db->update('user_session_rating',$data);
    }
    public function insertRating($data){
        $this->db->insert('user_session_rating',$data);
    }
    public function updateAgendaCheckInDateTime($agenda_id){
        $this->db->where('Id',$id);
        $this->db->update('agenda',['checking_datetime'=>$agenda_id]);
    }
     public function getPendingAgendaByUserId($user_id)
    {
        $this->db->select('pending_agenda_id');
        $this->db->from('users_agenda');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
         
    }
    public function savePendingAgenda($user_id)
    {

         $msg=array();
          $this->db->select('*')->from('users_agenda');
          $this->db->where('user_id',$user_id);
          $qu=$this->db->get();
          $res=$qu->result_array();
          $paid=array_filter(explode(",",$res[0]['pending_agenda_id']));
          $pending_id=$paid;
          $agenda_id=array_filter(explode(",",$res[0]['agenda_id']));
          $said=$agenda_id;
          if(count($agenda_id)>0){
               foreach ($paid as $pkey => $pvalue) {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id',$pvalue);
                    $dqu=$this->db->get();
                    $data=$dqu->result_array();
               if(!in_array($pvalue,$agenda_id)){     
               foreach ($agenda_id as $key => $value) {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Start_date',$data[0]['Start_date']);
                    $this->db->where("(Start_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                    $this->db->where("(End_time BETWEEN '".$data[0]['Start_time']."' AND '".$data[0]['End_time']."')");
                    $this->db->where('Id',$value);
                    $qu1=$this->db->get();
                    $res1=$qu1->result_array();
                    if(count($res1)>0)
                    {
                         $msg[$pkey]['Heading']=$data[0]['Heading'];
                         $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                         $msg[$pkey]['book']='0';
                         $msg[$pkey]['resion']="Session Clash";
                         $return=false;
                         break;
                    }
                    else
                    {
                         if(!empty($data[0]['Maximum_People']))
                         {
                              $this->db->select('*')->from('users_agenda');
                              $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                              $this->db->where($where);
                              $tqu=$this->db->get();
                              $total=$tqu->result_array();
                              $placeleft=$data[0]['Maximum_People']-count($total);
                         }
                         else
                         {
                              $placeleft=1;
                         }
                         if($placeleft>0)
                         {
                              $return=true;
                         }
                         else
                         {
                              $msg[$pkey]['Heading']=$data[0]['Heading'];
                              $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                              $msg[$pkey]['book']='0';
                              $msg[$pkey]['resion']="No Spaces Left";
                              $return=false;
                              break;
                         }
                    }
               }
               if($return=="true")
               {
                    array_push($said,$pvalue);          
                    unset($pending_id[array_search($pvalue,$pending_id)]);
                    $msg[$pkey]['Heading']=$data[0]['Heading'];
                    $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                    $msg[$pkey]['book']='1';
                    $msg[$pkey]['resion']="";
               }
               }
               else
               {
                    $msg[$pkey]['Heading']=$data[0]['Heading'];
                    $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                    $msg[$pkey]['book']='0';
                    $msg[$pkey]['resion']="Already Save Session";    
               }
               }
               $ua['agenda_id']=implode(",",$said);
               $ua['pending_agenda_id']=implode(",",$pending_id);
          }
          else
          {
               foreach ($paid as $pkey => $pvalue) {
                    $this->db->select('*')->from('agenda');
                    $this->db->where('Id',$pvalue);
                    $dqu=$this->db->get();
                    $data=$dqu->result_array();
                    if(!empty($data[0]['Maximum_People']))
                    {
                         $this->db->select('*')->from('users_agenda');
                         $where = "FIND_IN_SET($pvalue,`agenda_id`) > 0";
                         $this->db->where($where);
                         $tqu=$this->db->get();
                         $total=$tqu->result_array();
                         $placeleft=$data[0]['Maximum_People']-count($total);
                    }
                    else
                    {
                         $placeleft=1;
                    }
                    if($placeleft>0)
                    {
                         $return=true;
                    }
                    else
                    {
                         $msg[$pkey]['Heading']=$data[0]['Heading'];
                         $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                         $msg[$pkey]['book']='0';
                         $msg[$pkey]['resion']="No Spaces Left";
                         $return=false;
                    }
                    if($return=="true")
                    {
                         array_push($said,$pvalue);          
                         unset($pending_id[array_search($pvalue,$pending_id)]);
                         $msg[$pkey]['Heading']=$data[0]['Heading'];
                         $msg[$pkey]['Start_time']=$data[0]['Start_time'];
                         $msg[$pkey]['book']='1';
                         $msg[$pkey]['resion']="";
                    }
               }
               $ua['agenda_id']=implode(",",$said);
               $ua['pending_agenda_id']=implode(",",$pending_id);    
          }
          $this->db->where('user_id',$user_id);
          $this->db->update('users_agenda',$ua);
          return $msg;
    }

}
        
?>