<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Photo extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        $this->load->model('API/message_model');
        $this->load->model('API/photo_model');
        $this->load->model('API/attendee_model');
        $this->load->model('API/speaker_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function getPublicFeeds()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $page_no=$this->input->post('page_no');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $public_feeds = $this->photo_model->getAllPublicFeedsByEvent($event_id,$page_no,$limit,$user_id);
                $total_pages=$this->photo_model->getPageCountFeedsByEvent($event_id,$limit);
                $data = array(
                    'public_feeds' => $public_feeds,
                    'total_pages' => $total_pages,
                    'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);

    }
    public function UploadPublicFeeds()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $feed_id=$this->input->post('feed_id');
        if($event_id!='' && $token!='' && $user_id!='' && $_FILES['image']['name']!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($feed_id!='')
                {
                    $feed_data=$this->photo_model->getFeedDetails($feed_id);
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $update_data['image'] =$newImageName;
                    if($feed_data[0]['image']!='')
                    {
                        $arr=json_decode($feed_data[0]['image']);
                        $arr[]=$newImageName;
                        $update_arr['image']=json_encode($arr);
                        $this->photo_model->updateFeedImage($feed_id,$update_arr);
                    }
                    else
                    {

                        $arr[0]=$newImageName;
                        $update_arr['image']=json_encode($arr);
                        $this->photo_model->updateFeedImage($feed_id,$update_arr);
                    }
                    $a[0]=$newImageName;
                    $message_data['Message']='';
                    $message_data['Sender_id']=$user_id;
                    $message_data['Event_id']=$event_id;
                    $message_data['Parent']=$feed_id;
                    $message_data['image']=json_encode($a);
                    $message_data['Time']=date("Y-m-d H:i:s");
                    $message_data['ispublic']='1';
                    $feed_id1 = $this->photo_model->saveFeed($message_data);
                }
                else
                {

                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $arr[0]=$newImageName;
                    $message_data['Message']='';
                    $message_data['Sender_id']=$user_id;
                    $message_data['Event_id']=$event_id;
                    $message_data['Parent']=0;
                    $message_data['image']=json_encode($arr);
                    $message_data['Time']=date("Y-m-d H:i:s");
                    $message_data['ispublic']='1';
                    $feed_id1 = $this->photo_model->saveFeed($message_data);
                }
                if($feed_id1!=0)
                {
                    $data = array(
                        'success' => true,
                        'feed_id' => $feed_id1,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_feed()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $feed_id=$this->input->post('feed_id');
        if($event_id!='' && $token!='' && $user_id!='' && $feed_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->photo_model->delete_feed($feed_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function make_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $feed_id=$this->input->post('feed_id');
        if($event_id!='' && $token!='' && $user_id!='' && $feed_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($_FILES['image']['name']!='')
                {
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    if($newImageName!='')
                    {
                        $arr[0]=$newImageName;
                        $comment_arr['image']=json_encode($arr);
                    }
                }
                $comment_arr['comment']=$comment;
                $comment_arr['user_id']=$user_id;
                $comment_arr['msg_id']=$feed_id;
                $comment_arr['Time']=date("Y-m-d H:i:s");;
                $this->photo_model->make_comment($comment_arr,$event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $comment_id=$this->input->post('comment_id');
        if($event_id!='' && $token!='' && $comment_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->photo_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function like_feed()
    {
        $event_id=$this->input->post('event_id');
        $feed_id=$this->input->post('feed_id');
        $user_id=$this->input->post('user_id');
        $token=$this->input->post('token');
        if($event_id!='' && $token!='' && $feed_id!=''  && $user_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $data['post_id']=$feed_id;
                $data['user_id']=$user_id;
                $like_status = $this->photo_model->like_post($data);
                if($like_status!='')
                {
                    $data = array(
                        'success' => true,
                        'message' => "Successfully"
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something want wrong Please try again!"
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function dislike_feed()
    {
        $event_id=$this->input->post('event_id');
        $feed_id=$this->input->post('feed_id');
        $user_id=$this->input->post('user_id');
        $token=$this->input->post('token');
        if($event_id!='' && $token!='' && $feed_id!=''  && $user_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
               
                $like_status = $this->photo_model->dislike_post($user_id,$feed_id);
                if($like_status!='')
                {
                    $data = array(
                        'success' => true,
                        'message' => "Successfully"
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something want wrong Please try again!"
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $feed_id=$this->input->post('feed_id');
        if($event_id!='' && $feed_id!='')
        {
            
                $images = $this->photo_model->getImages($event_id,$feed_id);
                if($images != ''){
                     $data = array(
                    'images' => json_decode($images),
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data
                    );
                }else{
                     $data = array(
                          'success' => false,
                          'message' => "No images found"
                        );
                }
               
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    
}
