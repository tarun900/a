<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('API/app_login_model');
        $this->load->model('API/event_template_model');
        $this->load->model('API/cms_model');
        $this->load->model('API/event_model');
        
        $this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }

    public function index()
    {
        $user = $this->app_login_model->check_token_with_event($this->input->post('_token'),$this->input->post('event_id'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $fetureproduct = $this->event_model->getFetureProduct($this->input->post('event_id'));
            $data = array(
                'event' => $user[0],
                'event_feture_product' => $fetureproduct,
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }
    public function getAllPublicEvents() 
    {
        $gcm_id = $this->input->post('gcm_id');
    	$publicevents = $this->event_model->getPublicEvents($gcm_id);
    	if(!empty($publicevents))
    	{
    		foreach ($publicevents as $key => $value) 
    		{
    		   
    		   $publicevents[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $publicevents[$key]['Event_name'] =ucfirst($value['Event_name']);
	          
    		}
    		$data = array(
              'success' => true,
              'message' => "successfully",
              'data' => $publicevents
        	);
    	}
    	else
    	{
    		$data = array(
              'success' => true,
              'message' => "No Public Events found"
        	);
    	}
		
		/*$data = array(
              'success' => true,
              'message' => "No Public Events found"
        	);*/
			
        echo json_encode($data);
	}
    public function searchEventBySecurekey()
    {
    	$key=$this->input->post('secure_key');
    	if($key!='')
    	{
    		$events = $this->event_model->getPublicPrivateEventByKey($key);
	    	if(!empty($events))
	    	{
	    		
	    		$data = array(
	              'success' => true,
	              'message' => "successfully",
	              'data' => $events
	        	);
	    	}
	    	else
	    	{
	    		$data = array(
	              'success' => true,
	              'message' => "No Events found"
	        	);
	    	}
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
        	);
    	}
        echo json_encode($data);
    }
    public function searchEvent()
    {
		/*$data = array(
                  'success' => false,
                  'message' => "No app found."
                );*/
				
        $event_name = $this->input->post('event_name');
        if($event_name!='')
        {
            $events = $this->event_model->serachEventByName($event_name);
            if(!empty($events))
            {
                $data = array(
                  'success' => true,
                  'data' => $events
                );
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function openApp()
    {
        $event_id = $this->input->post('event_id');
        $gcm_id = $this->input->post('gcm_id');

        if($event_id!='' && $gcm_id!='')
        {
            $data['event_id'] = $event_id;
            $data['gcm_id'] = $gcm_id;
            $this->event_model->saveOpenAppData($data);
        }
    }
}
