<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// ini_set('display_errors', 1); 
// error_reporting(E_ALL);
class Attendee extends CI_Controller

{
    public $menu;

    public $cmsmenu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->model('Native_single_fcm/Attendee_model');
        $this->load->model('Native_single_fcm/Message_model');
        $this->load->model('Native_single_fcm/Settings_model');
        $this->load->model('Native_single_fcm/Gamification_model');
        include ('application/libraries/NativeGcm.php');

        include ('application/libraries/Fcm.php');

        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id') , null, null, $this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach($this->cmsmenu as $key => $values)
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function attendee_list()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $attendee = $this->Attendee_model->getAttendeeListByEventId($event_id, $user_id);
                $contact_attendee = $this->Attendee_model->getContactAttendeeListByEventId($event_id, $user_id);
                $data = array(
                    'attendee_list' => $attendee,
                    'contact_attendee' => $contact_attendee,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_list_native()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        if ($event_id != '' && $event_type != '')
        {
            if ($keyword != '') $where = "u.Lastname like '%$keyword%'";
            $attendee_data = $this->Attendee_model->getAttendeeListByEventId($event_id, ($where) ? $where : '', $page_no, $user_id);
            foreach($attendee_data as $key => $value)
            {
                if (!empty($value['Logo']))
                {
                    $source_url = base_url() . "assets/user_files/" . $value['Logo'];
                    $info = getimagesize($source_url);
                    $new_name = "new_" . $value['Logo'];
                    $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/" . $new_name;
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $attendee_data[$key]['Logo'] = $new_name;
                }
            }
            $data = array(
                'attendee_list' => $attendee_data,
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_contact_list()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $contact_attendee = $this->Attendee_model->getContactAttendeeListByEventId($event_id, $user_id);
                $data = array(
                    'contact_attendee' => $contact_attendee,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_list_native_new()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        // $filter_keywords = str_replace(' ','',$this->input->post('filter_keywords'));
        $filter_keywords = $this->input->post('filter_keywords');
        if ($event_id != '' && $event_type != '')
        {
            if ($keyword != '') $where = "((u.Lastname like '%" . $keyword . "%' OR u.Firstname like '%" . $keyword . "%' OR u.Company_name like '%" . $keyword . "%' OR u.Title like '%" . $keyword . "%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('" . $keyword . "', ' ', '%'), '%'))";
            $attendee_data = $this->Attendee_model->getAttendeeListByEventId_new($event_id, ($where) ? $where : '', $page_no, $user_id, $filter_keywords);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_contact_list_new()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        if ($event_id != '' && $event_type != '')
        {
            if ($keyword != '') $where = "(u.Lastname like '%" . $keyword . "%' OR u.Firstname like '%" . $keyword . "%' OR u.Company_name like '%" . $keyword . "%')";
            $contact_attendee = $this->Attendee_model->get_my_contact($event_id, $user_id, ($where) ? $where : '', $page_no);
            $data = array(
                'contact_attendee' => $contact_attendee['attendees'],
                'list_total_pages' => $contact_attendee['total'],
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_view()

    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        $page_no = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $attendee_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $limit = 10;
                $attendee = $this->Attendee_model->getAttendeeDetails($attendee_id, $event_id);
                if ($attendee)
                {
                    $message = $this->Message_model->view_private_msg_coversation($user_id, $attendee_id, $event_id, $page_no, $limit);
                    foreach($message as $key => $value)
                    {
                        $string = $value['message'];
                        if ($string != strip_tags($string))
                        {
                            $message[$key]['message'] = strip_tags($value['message']);
                            $message[$key]['is_clickable'] = '1';
                        }
                        else
                        {
                            $message[$key]['is_clickable'] = '0';
                        }
                    }
                    $total_pages = $this->Message_model->total_pages($user_id, $attendee_id, $event_id, $limit);
                    $social_links = $this->Attendee_model->getSocialLinks($attendee_id);
                    $attendee[0]->Facebook_url = ($social_links[0]->Facebook_url) ? $social_links[0]->Facebook_url : '';
                    $attendee[0]->Twitter_url = ($social_links[0]->Twitter_url) ? $social_links[0]->Twitter_url : '';
                    $attendee[0]->Linkedin_url = ($social_links[0]->Linkedin_url) ? $social_links[0]->Linkedin_url : '';
                    $where['event_id'] = $event_id;
                    $where['from_id'] = $user_id;
                    $where['to_id'] = $attendee_id;
                    $attendee[0]->approval_status = ($user_id != $attendee_id) ? $this->Attendee_model->getApprovalStatus($where, $event_id) : '';
                    $where1['to_id'] = $user_id;
                    $attendee[0]->share_details = ($user_id == $attendee_id) ? $this->Attendee_model->getShareDetails($where1, $event_id) : [];
                    $attendee[0]->contact_details = ($attendee[0]->approval_status == '1') ? $this->Attendee_model->getAttendeeConatctDetails($attendee_id) : [];
                }
                $allow_ex_contact = $this->Attendee_model->getAllowAttendee($attendee_id, $event_id);
                $data = array(
                    'attendee_details' => ($attendee) ? $attendee : [],
                    'message' => $message,
                    'allowexhcontact' => $allow_ex_contact,
                    'total_pages' => $total_pages,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function sendMessage()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        $message_type = $this->input->post('message_type');
        $message = $this->input->post('message');
        if ($event_id != '' && $token != '' && $attendee_id != '' && $user_id != '' && $message != '' && $message_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $message_data['Message'] = $message;
                $message_data['Sender_id'] = $user_id;
                $message_data['Event_id'] = $event_id;
                $message_data['Receiver_id'] = $attendee_id;
                $message_data['Parent'] = 0;
                $message_data['image'] = "";
                $message_data['Time'] = date("Y-m-d H:i:s");
                $message_data['ispublic'] = $message_type;
                $message_data['msg_creator_id'] = $user_id;
                $message_id = $this->Message_model->saveMessage($message_data);
                if ($message_id != 0)
                {
                    $current_date = date('Y/m/d');
                    $this->Message_model->add_msg_hit($user_id, $current_date, $attendee_id, $event_id);
                    $data = array(
                        'success' => true,
                        'message' => "Successfully Saved",
                        'message_id' => $message_id
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something is wrong.Please try again"
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function msg_images_request()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        $message_type = $this->input->post('message_type');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $attendee_id != '' && $user_id != '' && $message_id != '' && $_FILES['image']['name'] != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $message_data1 = $this->Message_model->getMessageDetails($message_id);
                $newImageName = round(microtime(true) * 1000) . ".jpeg";
                $target_path = "././assets/user_files/" . $newImageName;
                $target_path1 = "././assets/user_files/thumbnail/";
                move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
                copy("./assets/user_files/" . $newImageName, "./assets/user_files/thumbnail/" . $newImageName);
                $update_data['image'] = $newImageName;
                if ($message_data1[0]['image'] != '')
                {
                    $arr = json_decode($message_data1[0]['image']);
                    $arr[] = $newImageName;
                    $update_arr['image'] = json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id, $update_arr);
                }
                else
                {
                    $arr[0] = $newImageName;
                    $update_arr['image'] = json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id, $update_arr);
                }
                $a[0] = $newImageName;
                $message_data['Message'] = '';
                $message_data['Sender_id'] = $user_id;
                $message_data['Event_id'] = $event_id;
                $message_data['Receiver_id'] = $attendee_id;
                $message_data['Parent'] = $message_id;
                $message_data['image'] = json_encode($a);
                $message_data['Time'] = date("Y-m-d H:i:s");
                $message_data['ispublic'] = '1';
                $message_data['msg_creator_id'] = $user_id;
                $message_id = $this->Message_model->saveMessage($message_data);
                if ($message_id != 0)
                {
                    $current_date = date('Y/m/d');
                    // $this->Message_model->add_msg_hit($user_id,$current_date,$attendee_id);
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something is wrong.Please try again"
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_message()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $user_id != '' && $message_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $this->Message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function make_comment()

    {
        // error_reporting(1);
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $comment = $this->input->post('comment');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $user_id != '' && $message_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                if ($_FILES['image']['name'] != '')
                {
                    $newImageName = round(microtime(true) * 1000) . ".jpeg";
                    $target_path = "././assets/user_files/" . $newImageName;
                    $target_path1 = "././assets/user_files/thumbnail/";
                    move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
                    copy("./assets/user_files/" . $newImageName, "./assets/user_files/thumbnail/" . $newImageName);
                    if ($newImageName != '')
                    {
                        $arr[0] = $newImageName;
                        $comment_arr['image'] = json_encode($arr);
                    }
                }
                $comment_arr['comment'] = $comment;
                $comment_arr['user_id'] = $user_id;
                $comment_arr['msg_id'] = $message_id;
                $comment_arr['Time'] = date("Y-m-d H:i:s");
                $this->Message_model->make_comment($comment_arr, $event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_comment()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $comment_id = $this->input->post('comment_id');
        if ($event_id != '' && $token != '' && $comment_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $this->Message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $message_id != '')
        {
            $images = $this->Message_model->getImages($event_id, $message_id);
            if ($images != '')
            {
                $data = array(
                    'images' => json_decode($images) ,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No images found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function shareContactInformation()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $user_id != '' && $attendee_id != '')
        {
            $where['event_id'] = $event_id;
            $where['from_id'] = $user_id;
            $where['to_id'] = $attendee_id;
            $share_data['event_id'] = $event_id;
            $share_data['from_id'] = $user_id;
            $share_data['to_id'] = $attendee_id;
            $share_data['contact_type'] = '0';
            $share_data['approval_status'] = '0';
            $this->add_user_game_point($user_id, $event_id, 4);
            $id = $this->Attendee_model->saveShareContactInformation($where, $share_data);
            $url_data = $this->Attendee_model->getUrlData($event_id);
            $url = base_url() . $url_data . $id;
            $user = $this->Attendee_model->getUser($user_id);
            $default_lang = $this->App_login_model->get_default_lang_label($event_id, $lang_id);
            $msg1 = $default_lang['notifications__share_contact'];
            $patterns = array();
            $patterns[0] = '/{{user_name}}/';
            $replacements = array();
            $replacements[0] = ucfirst($user['Firstname']) . " " . $user['Lastname'];
            $msg1 = preg_replace($patterns, $replacements, $msg1);
            $message = "<a href='" . $url . "'>" . ucfirst($user['Firstname']) . " " . $user['Lastname'] . " " . $msg1 . "</a>";
            $message_data['Message'] = $message;
            $message_data['Sender_id'] = $user_id;
            $message_data['Receiver_id'] = $attendee_id;
            $message_data['Event_id'] = $event_id;
            $message_data['Parent'] = '0';
            $message_data['Time'] = date("Y-m-d H:i:s");
            $message_data['ispublic'] = '0';
            $message_data['msg_creator_id'] = $user_id;
            $message_data['msg_type'] = '1';
            $this->Message_model->savePublicMessage($message_data);
            $attendee = $this->Attendee_model->getUser($attendee_id);
            if ($attendee['gcm_id'] != '')
            {
                $message1 = ucfirst($user['Firstname']) . " has shared their contact details with you.";
                $extra['message_type'] = 'Private';
                $extra['message_id'] = $user_id;
                $extra['event'] = $this->Settings_model->event_name($event_id);
                if ($attendee['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Contact Share";
                    $msg = strip_tags($message); //$message1;//
                    $result = $obj->send(1, $attendee['gcm_id'], $msg, $extra, $attendee['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Contact Share";
                    $msg['message'] = strip_tags($message); //$message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result = $obj->send_notification($attendee['gcm_id'], $msg, $extra, $attendee['device']);
                }
            }
            $message1 = ucfirst($user['Firstname']) . " " . ucfirst($user['Lastname']) . " has shared their contact details with you.";
            $this->Event_model->sendEmailToAttendees($event_id, $message1, $attendee['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => 'You have successfully shared your information.',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function approveStatus()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        if ($event_id != '' && $user_id != '' && $attendee_id != '')
        {
            $where['event_id'] = $event_id;
            $where['from_id'] = $attendee_id;
            $where['to_id'] = $user_id;
            $share_data['approval_status'] = '1';
            $this->Attendee_model->saveShareContactInformation($where, $share_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully approved.',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function rejectStatus()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        if ($event_id != '' && $user_id != '' && $attendee_id != '')
        {
            $where['event_id'] = $event_id;
            $where['from_id'] = $attendee_id;
            $where['to_id'] = $user_id;
            $share_data['approval_status'] = '2';
            $this->Attendee_model->saveShareContactInformation($where, $share_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully rejected.',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingDateTime()

    {
        $event_id = $this->input->post('event_id');
        if ($event_id != '')
        {
            if ($event_id == '479')
            {
                $data = $this->Attendee_model->getDateTimeArray_for_Solar($event_id);
            }
            elseif ($event_id == '992' || $event_id == '1107')
            {
                $data = $this->Attendee_model->getDateTimeArray_for_ipexpo($event_id);
            }
            else
            {
                $data = $this->Attendee_model->getDateTimeArray($event_id);
            }
            $data['location'] = $this->Attendee_model->get_meeting_location($event_id);
            if (empty($data['location'])) $data['location'] = array();
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    // Wednesday 24 May 2017 05:47:24 PM IST comment - use of moderator meeting
    /*    public function requestMeeting()
    {
    $event_id       = $this->input->post('event_id');
    $receiver_id    = $this->input->post('attendee_id');
    $attendee_id    = $this->input->post('user_id');
    $date           = $this->input->post('date');
    $time           = date('H:i:s',strtotime($this->input->post('time')));
    $location       = $this->input->post('location');
    if($event_id!='' && $receiver_id!='' && $attendee_id!=''&& $date!='' && $time!='')
    {
    $attendee = $this->Attendee_model->getUser($attendee_id);
    $data['event_id'] = $event_id;
    $data['recever_attendee_id'] = $receiver_id;
    if($attendee['Role_id'] == '6')
    $data['sender_exhibitor_id'] = $attendee_id;
    else
    $data['attendee_id'] = $attendee_id;
    $date = DateTime::createFromFormat('m-d-Y',$date);
    $date = $date->format('Y-m-d');
    $data['date'] = $date;
    $data['time'] = $time;
    $data['location'] = $location;
    $check_session = $this->Attendee_model->checkSessionClash($attendee_id,$date,$time);
    if($check_session['result'])
    {
    $result = $this->Attendee_model->saveRequest($data);
    if($result)
    {
    $url=base_url().$this->Attendee_model->getUrlData($event_id);
    $user = $this->Attendee_model->getUser($receiver_id);
    if($attendee['Role_id'] != '6')
    {
    $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
    $message1 = ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
    }
    else
    {
    $Message="<a href='".$url."'>".ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
    $message1 = ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
    }
    $data1 = array(
    'Message' => $Message,
    'Sender_id' => $attendee_id,
    'Receiver_id' => $user['Id'],
    'Event_id'=>$event_id,
    'Parent' => '0',
    'Time' => date("Y-m-d H:i:s"),
    'ispublic' => '0',
    'msg_creator_id'=>$attendee_id,
    'msg_type'  => '3',
    );
    $this->Attendee_model->saveSpeakerMessage($data1);
    $template = $this->Message_model->getNotificationTemplate($event_id,'Meeting');
    if($user['gcm_id']!='')
    {
    $obj = new Gcm($event_id);
    $extra['message_type'] = 'AttendeeRequestMeeting';
    $extra['message_id'] = $user['Id'];
    $extra['event'] = $this->Attendee_model->getEventName($event_id);
    if($user['device'] == 'Iphone')
    {
    $extra['title'] = $template['Slug'];
    $msg = $template['Content'];
    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
    }
    else
    {
    $msg['title'] = $template['Slug'];
    $msg['message'] = $template['Content'];
    $msg['vibrate'] = 1;
    $msg['sound'] = 1;
    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
    }
    }
    $this->Event_model->sendEmailToAttendees($event_id,$template['Content'],$user['Email'],"Notification");
    $data = array(
    'success' => true,
    'message' => "Your request was sent successfully",
    'flag'    => '1',
    );
    }
    else
    {
    $data = array(
    'success' => true,
    'message' => "You have already set meeting with this time and date.",
    'flag'    => '0',
    );
    }
    }
    else
    {
    $session = $check_session['agenda_name'];
    $data = array(
    'success' => false,
    'message' => "This will clash with $session - would you like to proceed?",
    'flag'    => '0',
    );
    }
    }
    else
    {
    $data = array(
    'success' => false,
    'message' => "Invalid parameters"
    );
    }
    echo json_encode($data);
    }*/
    public function saveRequestMeeting()

    {
        // error_reporting(E_ALL);
        $event_id = $this->input->get_post('event_id');
        $receiver_id = $this->input->get_post('attendee_id');
        $attendee_id = $this->input->get_post('user_id');
        $date = $this->input->get_post('date');
        // $time           = $this->input->get_post('time');
        $time = date('H:i:s', strtotime($this->input->get_post('time')));
        $location = $this->input->get_post('location');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $receiver_id != '' && $attendee_id != '' && $date != '' && $time != '')
        {
            $user = $this->Attendee_model->getUser($receiver_id);
            $attendee = $this->Attendee_model->getUser($attendee_id);
            $data['event_id'] = $event_id;
            $data['recever_attendee_id'] = $receiver_id;
            if ($attendee['Role_id'] == '6') $data['sender_exhibitor_id'] = $attendee_id;
            else $data['attendee_id'] = $attendee_id;
            $date_format = $this->Attendee_model->checkEventDateFormat($event_id);
            if ($date_format[0]['date_format'] == '0')
            {
                $date = DateTime::createFromFormat('d-m-Y', $date);
            }
            else
            {
                $date = DateTime::createFromFormat('m-d-Y', $date);
            }
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;
            $data['location'] = $location;
            $default_lang = $this->App_login_model->get_default_lang_label($event_id, $lang_id);
            $result = $this->Attendee_model->saveRequest($data);
            if ($result['status'])
            {
                $url = base_url() . $this->Attendee_model->getUrlData($event_id);
                if ($attendee['Role_id'] == '6')
                {
                    $url = base_url() . 'Attendee/' . $acc_name . '/' . $Subdomain . '/checpendingkmetting';
                    $msg1 = $default_lang['notifications__request_meeting_without_modaretor'];
                    $patterns = array();
                    $patterns[0] = '/{{time}}/';
                    $patterns[1] = '/{{date}}/';
                    $replacements = array();
                    $replacements[0] = $time;
                    $replacements[1] = $date;
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " " . $msg1 . "</a>";
                    $message1 = ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                }
                else
                {
                    $msg1 = $default_lang['notifications__request_meeting_with_modaretor'];
                    $patterns = array();
                    $patterns[0] = '/{{user_name}}/';
                    $patterns[1] = '/{{time}}/';
                    $patterns[2] = '/{{date}}/';
                    $replacements = array();
                    $replacements[0] = ucfirst($user['Firstname']) . " " . ucfirst($user['Lastname']);
                    $replacements[1] = $time;
                    $replacements[2] = $date;
                    $msg1 = preg_replace($patterns, $replacements, $msg1);
                    $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " " . $msg1 . "</a>";
                    $message1 = ucfirst($attendee['Company_name']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                }
                $data1 = array(
                    'Message' => $Message,
                    'Sender_id' => $attendee_id,
                    'Receiver_id' => $receiver_id,
                    'Event_id' => $event_id,
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s") ,
                    'ispublic' => '0',
                    'msg_creator_id' => $attendee_id,
                    'msg_type' => '3',
                );
                $this->Attendee_model->saveSpeakerMessage($data1);
                $template = $this->Message_model->getNotificationTemplate($event_id, 'Meeting');
                if ($user['gcm_id'] != '')
                {
                    $extra['message_type'] = 'AttendeeRequestMeeting';
                    $extra['message_id'] = $user['Id'];
                    $extra['event'] = $this->Attendee_model->getEventName($event_id);
                    if ($user['device'] == 'Iphone')
                    {
                        $obj = new Fcm();
                        $extra['title'] = $template['Slug'];
                        $msg = $template['Content'];
                        $req = $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                    }
                    else
                    {
                        $obj = new Gcm($event_id);
                        $msg['title'] = $template['Slug'];
                        $msg['message'] = $template['Content'];
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $req = $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                    }
                }
                if ($template['send_email'] == '1')
                {
                    $this->Event_model->sendEmailToAttendees($event_id, $template['email_content'], $user['Email'], $template['email_subject']);
                }
                $data = array(
                    'success' => true,
                    'message' => "Your request was sent successfully",
                );
            }
            else
            {
                $data = array(
                    'success' => true,
                    'message' => "You have already set meeting with this time and date.",
                    'flag' => '0',
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAllMeetingRequest()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $data = $this->Attendee_model->getAllMeetingRequest($event_id, $user_id);
            $date_format = $this->Attendee_model->checkEventDateFormat($event_id);
            if (!empty($data))
            {
                foreach($data as $key => $value)
                {
                    if ($date_format[0]['date_format'] == 0)
                    {
                        $a = strtotime($value['date']);
                        $date = date("d/m/Y", $a);
                    }
                    else
                    {
                        $a = strtotime($value['date']);
                        $date = date("m/d/Y", $a);
                    }
                    $data[$key]['date'] = $date;
                }
            }
            $data = array(
                'success' => true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequest()

    {
        // error_reporting(E_ALL);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id');
        $lang_id = $this->input->post('lang_id');
        $rejection_msg = $this->input->post('rejection_msg');
        if ($status != '' && $id != '' && $receiver_id != '' && $user_id != '')
        {
            $update_data['status'] = $status;
            $where['Id'] = $id;
            $result_data = $this->Attendee_model->updateRequest($update_data, $where);
            $user = $this->Attendee_model->getUser($user_id);
            $default_lang = $this->App_login_model->get_default_lang_label($event_id, $lang_id);
            if ($status == '2')
            {
                $message = ucfirst($user['Firstname']) . " " . $default_lang['notifications__request_meeting_reject'];
                $message.= !empty($rejection_msg) ? ' - ' . $rejection_msg : '';
            }
            else
            {
                $this->add_user_game_point($user_id, $event_id, 3);
                $message = ucfirst($user['Firstname']) . " " . $default_lang['notifications__request_meeting_accept'];
            }
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => ($result_data['attendee_id']) ? : $result_data['sender_exhibitor_id'],
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            $user = $this->Attendee_model->getUsersData($result_data['attendee_id']);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, $message, $user['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function suggestMeetingTime()

    {
        $date = json_decode($this->input->post('date') , true);
        $time = json_decode($this->input->post('time') , true);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $lang_id = $this->input->post('lang_id');
        if ($date != '' && $time != '' && $id != '' && $receiver_id != '' && $user_id != '' && $event_id != '')
        {
            $exhibitor = $this->Attendee_model->getUser($user_id);
            $user = $this->Attendee_model->getUser($receiver_id);
            $default_lang = $this->App_login_model->get_default_lang_label($event_id, $lang_id);
            $msg1 = $default_lang['notifications__request_meeting_suggested_datetime'];
            $patterns = array();
            $patterns[0] = '/{{user_name}}/';
            $replacements = array();
            $replacements[0] = ucfirst($exhibitor['Firstname']);
            $msg1 = preg_replace($patterns, $replacements, $msg1);
            $message = ucfirst($exhibitor['Firstname']) . " " . $msg1 . "<br/>";
            foreach($date as $key => $value)
            {
                $temp = (strpos($time[$key], "AM")) ? str_replace(' AM', "", $time[$key]) : date("G:i", strtotime($time[$key]));
                $date = date('Y-m-d H:i', strtotime($value . ' ' . $temp));
                $link = base_url() . $this->Attendee_model->getSuggestUrlData($event_id) . 'changemeetingdate/' . strtotime($date) . '/' . $id;
                $message.= "<a href='" . $link . "'>" . $date . "</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['recever_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->Attendee_model->saveSuggestedDate($suggest);
            }
            $message1 = ucfirst($exhibitor['Firstname']) . " " . $msg1;
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $user['Id'],
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
                'msg_type' => '4',
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeSuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Suggest Request Time";
                    $msg = $message1;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, ucfirst($exhibitor['Firstname']) . " " . ucfirst($exhibitor['Lastname']) . " " . $msg1, $user['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getSuggestedTimings()

    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $meeting_id = $this->input->post('meeting_id');
        if ($user_id != '' && $event_id != '')
        {
            $where['attendee_id'] = $user_id;
            $where['sm.event_id'] = $event_id;
            if ($meeting_id != '') $where['meeting_id'] = $meeting_id;
            $available_times = $this->Attendee_model->getAvailableTimes($where);
            foreach($available_times as $key => $value)
            {
                $available_times[$key]['date_time'] = date("H:i l, m/d/Y", strtotime($value['date_time']));
                $available_times[$key]['api_date_time'] = date("H:i m/d/Y", strtotime($value['date_time']));
            }
            $data = array(
                'success' => true,
                'data' => $available_times,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function bookSuggestedTime()

    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $suggested_id = $this->input->post('suggested_id');
        $date_time = $this->input->post('date_time');
        $lang_id = $this->input->post('lang_id');
        if ($user_id != '' && $event_id != '' && $suggested_id != '' && $date_time != '')
        {
            $where['attendee_id'] = $user_id;
            $where['event_id'] = $event_id;
            $where['Id'] = $suggested_id;
            $default_lang = $this->App_login_model->get_default_lang_label($event_id, $lang_id);
            $user = $this->Attendee_model->bookSuggestedTime($where, $date_time);
            if (!empty($user))
            {
                $attendee = $this->Attendee_model->getUsersData($user_id);
                $msg = $default_lang['notifications__request_meeting_accept_suggested_datetime'];
                $patterns = array();
                $patterns[0] = '/{{company_name}}/';
                $patterns[1] = '/{{datetime}}/';
                $replacements = array();
                $replacements[0] = $attendee['Company_name'];
                $replacements[1] = date('Y-m-d H:i', $datetime);
                $msg = preg_replace($patterns, $replacements, $msg);
                $message1 = $attendee['Firstname'] . " " . $attendee['Lastname'] . " " . $msg;
                /*if($attendee['title']!='')
                $message1 = ucfirst($attendee['Firstname']) ." at ".$attendee['title'] ." has accepted your new suggested time of $date_time. This meeting has now been booked.";
                else
                $message1 = ucfirst($attendee['Firstname']) ." has accepted your new suggested time of $date_time. This meeting has now been booked.";*/
                $data1 = array(
                    'Message' => $message1,
                    'Sender_id' => $user_id,
                    'Receiver_id' => $user['Id'],
                    'Event_id' => $event_id,
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s") ,
                    'ispublic' => '0',
                    'msg_creator_id' => $user_id
                );
                $this->Attendee_model->saveSpeakerMessage($data1);
                if ($user['gcm_id'] != '')
                {
                    $extra['message_type'] = 'AttendeeBookedMeeting';
                    $extra['message_id'] = $id;
                    $extra['event'] = $this->Attendee_model->getEventName($event_id);
                    if ($user['device'] == 'Iphone')
                    {
                        $obj = new Fcm();
                        $extra['title'] = "Booked Meeting";
                        $msg = $message1;
                        $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                    }
                    else
                    {
                        $obj = new Gcm($event_id);
                        $msg['title'] = "Booked Meeting";
                        $msg['message'] = $message1;
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                    }
                }
                $this->Event_model->sendEmailToAttendees($event_id, $message1, $user['Email'], "Notification");
            }
            $data = array(
                'success' => true,
                'message' => 'Your meeting booked successfully.',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_view_unread_count()

    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $attendee_id = $this->input->post('attendee_id');
        $page_no = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $attendee_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $limit = 10;
                $attendee = $this->Attendee_model->getAttendeeDetails($attendee_id, $event_id);
                if ($attendee)
                {
                    $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id, $user_id, $page_no, $limit, $attendee_id);
                    $social_links = $this->Attendee_model->getSocialLinks($attendee_id);
                    $attendee[0]->Facebook_url = ($social_links[0]->Facebook_url) ? $social_links[0]->Facebook_url : '';
                    $attendee[0]->Twitter_url = ($social_links[0]->Twitter_url) ? $social_links[0]->Twitter_url : '';
                    $attendee[0]->Linkedin_url = ($social_links[0]->Linkedin_url) ? $social_links[0]->Linkedin_url : '';
                    $where['event_id'] = $event_id;
                    $where['from_id'] = $user_id;
                    $where['to_id'] = $attendee_id;
                    $attendee[0]->approval_status = ($user_id != $attendee_id) ? $this->Attendee_model->getApprovalStatus($where, $event_id) : '';
                    $where1['to_id'] = $user_id;
                    $attendee[0]->share_details = ($user_id == $attendee_id) ? $this->Attendee_model->getShareDetails($where1, $event_id) : [];
                    $attendee[0]->contact_details = ($attendee[0]->approval_status == '1') ? $this->Attendee_model->getAttendeeConatctDetails($attendee_id) : [];
                }
                $attendee[0]->linked_exhibitors = $this->Attendee_model->get_linked_exhibitors($attendee_id);
                $allow_ex_contact = $this->Attendee_model->getAllowAttendee($attendee_id, $event_id);
                $event_data = $this->Attendee_model->getEvenetData($event_id);
                $myfav = $this->Attendee_model->getAttendeemyfav($attendee_id, $event_id, $user_id);
                $game_point = $this->Attendee_model->get_game_point($event_id, $attendee_id);
                if (!empty($myfav))
                {
                    $myfav = '1';
                }
                else
                {
                    $myfav = '0';
                }
                $user = $this->Attendee_model->getUserNew($user_id, $event_id);
                $event_data['allow_meeting_exibitor_to_attendee'] = ($user['Role_id'] == '4') ? '1' : $event_data['allow_meeting_exibitor_to_attendee'];
                $user_group_permmison = $this->Attendee_model->check_group_permissions($attendee_id, $user_id, $event_id);
                $data['user_id_from'] = $user_id;
                $data['user_id_to'] = $attendee_id;
                $data['event_id'] = $event_id;
                $is_blocked = $this->Attendee_model->checkUserBlocked($data);
                $blocked_by_me = $this->Attendee_model->checkBlockedByMe($data);
                $data = array(
                    'attendee_details' => ($attendee) ? $attendee : [],
                    // 'message' => $message,
                    'allowexhcontact' => $allow_ex_contact,
                    'my_faviorite' => $myfav,
                    // 'total_pages' => $total_pages,
                    'unread_count' => $unread_count[0]['unread_count'],
                    'attendee_hide_request_meeting' => $event_data['attendee_hide_request_meeting'],
                    'allow_meeting_exibitor_to_attendee' => $event_data['allow_meeting_exibitor_to_attendee'],
                    'game_is_on' => $game_point['game_is_on'],
                    'game_points' => $game_point['points'],
                    'message_permisson' => $this->Attendee_model->check_message_permisson($attendee_id, $user_id, $event_id) ,
                    'show_send_message' => ($user_group_permmison['send_message']) ? : '0',
                    'show_send_request' => ($user_group_permmison['send_request']) ? : '0',
                    'is_blocked' => $is_blocked,
                    'blocked_by_me' => $blocked_by_me,
                    'enable_block_button' => $event_data['enable_block_button']
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function requestMeeting()

    {
        $event_id = $this->input->post('event_id');
        $receiver_id = $this->input->post('attendee_id');
        $attendee_id = $this->input->post('user_id');
        $date = $this->input->post('date');
        $time = date('H:i:s', strtotime($this->input->post('time')));
        $location = $this->input->post('location');
        if ($event_id != '' && $receiver_id != '' && $attendee_id != '' && $date != '' && $time != '')
        {
            $attendee = $this->Attendee_model->getUser($attendee_id);
            $data['event_id'] = $event_id;
            $data['recever_attendee_id'] = $receiver_id;
            if ($attendee['Role_id'] == '6') $data['sender_exhibitor_id'] = $attendee_id;
            else $data['attendee_id'] = $attendee_id;
            $date_format = $this->Attendee_model->checkEventDateFormat($event_id);
            if ($date_format[0]['date_format'] == '0')
            {
                $date = DateTime::createFromFormat('d-m-Y', $date);
            }
            else
            {
                $date = DateTime::createFromFormat('m-d-Y', $date);
            }
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;
            $data['location'] = $location;
            $check_session = $this->Attendee_model->checkSessionClash($attendee_id, $date, $time);
            if ($check_session['result'])
            {
                $check_moderator = $this->Attendee_model->check_moderator($event_id, $receiver_id);
                if ($check_moderator)
                {
                    $moderator = $this->Attendee_model->getUser($check_moderator['moderator_id']);
                    $data['moderator_id'] = $moderator['Id'];
                    $data['recipient_attendee_id'] = $receiver_id;
                    $data['recever_attendee_id'] = NULL;
                }
                $result = $this->Attendee_model->saveRequest($data);
                if ($result['status'])
                {
                    $url = base_url() . $this->Attendee_model->getUrlData($event_id);
                    $user = $this->Attendee_model->getUser($receiver_id);
                    $this->add_user_game_point($attendee_id, $event_id, 2);
                    if ($attendee['Role_id'] != '6')
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . " Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                    }
                    else
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Company_name']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . " Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Company_name']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                    }
                    if ($check_moderator)
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with " . ucfirst($user['Firstname']) . " " . ucfirst($user['Lastname']) . " at " . $time . " on " . date('d/m/Y', strtotime($date)) . " Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                    }
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $user['Id'],
                        'Event_id' => $event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s") ,
                        'ispublic' => '0',
                        'msg_creator_id' => $attendee_id,
                        'msg_type' => '3',
                    );
                    if ($check_moderator)
                    {
                        $data1['Receiver_id'] = $moderator['Id'];
                        $data1['msg_type'] = '5';
                    }
                    $this->Attendee_model->saveSpeakerMessage($data1);
                    $template = $this->Message_model->getNotificationTemplate($event_id, 'Meeting');
                    if ($check_moderator)
                    {
                        if ($moderator['gcm_id'] != '')
                        {
                            $extra['message_type'] = 'ModeratorRequestMeeting';
                            $extra['message_id'] = $moderator['Id'];
                            $extra['event'] = $this->Attendee_model->getEventName($event_id);
                            if ($moderator['device'] == 'Iphone')
                            {
                                $obj = new Fcm();
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send(1, $moderator['gcm_id'], $msg, $extra, $moderator['device']);
                            }
                            else
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($moderator['gcm_id'], $msg, $extra, $moderator['device']);
                            }
                        }
                    }
                    else
                    {
                        if ($user['gcm_id'] != '')
                        {
                            $extra['message_type'] = 'AttendeeRequestMeeting';
                            $extra['message_id'] = $user['Id'];
                            $extra['event'] = $this->Attendee_model->getEventName($event_id);
                            if ($user['device'] == 'Iphone')
                            {
                                $obj = new Fcm();
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                            }
                            else
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                            }
                        }
                    }
                    if ($template['send_email'] == '1')
                    {
                        $this->Event_model->sendEmailToAttendees($event_id, $template['email_content'], $user['Email'], $template['email_subject']);
                    }
                    $data = array(
                        'success' => true,
                        'message' => "Your request was sent successfully",
                        'flag' => '1',
                    );
                }
                else
                {
                    $data = array(
                        'success' => true,
                        'message' => $result['msg'],
                        'flag' => '0',
                    );
                }
            }
            else
            {
                $session = $check_session['agenda_name'];
                $data = array(
                    'success' => false,
                    'message' => "This will clash with $session - would you like to proceed?",
                    'flag' => '0',
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAllMeetingRequestModerator()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $data = $this->Attendee_model->getAllMeetingRequestModerator($event_id, $user_id);
            $date_format = $this->Attendee_model->checkEventDateFormat($event_id);
            if (!empty($data))
            {
                foreach($data as $key => $value)
                {
                    if ($date_format[0]['date_format'] == 0)
                    {
                        $a = strtotime($value['date']);
                        $date = date("d/m/Y", $a);
                    }
                    else
                    {
                        $a = strtotime($value['date']);
                        $date = date("m/d/Y", $a);
                    }
                    $data[$key]['date'] = $date;
                }
            }
            $data = array(
                'success' => true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequestModerator()

    {
        // error_reporting(E_ALL);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $sender_id = $this->input->post('sender_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id');
        if ($status != '' && $id != '' && $receiver_id != '' && $user_id != '' && $sender_id != '')
        {
            $update_data['status'] = $status;
            $update_data['recever_attendee_id'] = $receiver_id;
            $where['Id'] = $id;
            $result_data = $this->Attendee_model->updateRequestModerator($update_data, $where);
            $user = $this->Attendee_model->getUser($user_id);
            if ($status == '2')
            {
                $message = ucfirst($user['Firstname']) . " has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message = ucfirst($user['Firstname']) . " has accepted your Meeting Request.";
            }
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $result_data['attendee_id'],
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s"),
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            $user = $this->Attendee_model->getUsersData($sender_id);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, $message, $user['Email'], "Notification");
            // Reciver Notifictaion
            $reciver = $this->Attendee_model->getUser($receiver_id);
            if ($status == '2')
            {
                $message = ucfirst($user['Firstname']) . " has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message = "You have a meeting scheduled with " . ucfirst($user['Firstname']) . " " . $user['Lastname'] . " at " . $result_data['time'] . " " . $result_data['date'] . " at " . $result_data['location'];
            }
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $receiver_id,
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            $user = $this->Attendee_model->getUsersData($receiver_id);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, $message, $user['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequestModeratorV2()

    {
        // error_reporting(E_ALL);
        $id = $this->input->get_post('request_id');
        $receiver_id = $this->input->get_post('receiver_id');
        $sender_id = $this->input->get_post('sender_id');
        $user_id = $this->input->get_post('user_id');
        $status = $this->input->get_post('status'); // 3=moderator_approve,4=moderator_reject
        $event_id = $this->input->get_post('event_id');
        if ($status != '' && $id != '' && $receiver_id != '' && $user_id != '' && $sender_id != '')
        {
            $update_data['status'] = $status;
            $update_data['recever_attendee_id'] = $receiver_id;
            $where['Id'] = $id;
            $result_data = $this->Attendee_model->updateRequestModerator($update_data, $where);
            $user = $this->Attendee_model->getUser($user_id);
            if ($status == '4')
            {
                $message = ucfirst($user['Firstname']) . " has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message = ucfirst($user['Firstname']) . " has accepted your Meeting Request.";
            }
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $result_data['attendee_id'],
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s"),
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            $user = $this->Attendee_model->getUsersData($sender_id);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, $message, $user['Email'], "Notification");
            // Reciver Notifictaion
            $reciver = $this->Attendee_model->getUser($receiver_id);
            if ($status == '2')
            {
                $message = ucfirst($user['Firstname']) . " has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message = "You have a meeting scheduled with " . ucfirst($user['Firstname']) . " " . $user['Lastname'] . " at " . $result_data['time'] . " " . $result_data['date'] . " at " . $result_data['location'];
            }
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $receiver_id,
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            $user = $this->Attendee_model->getUsersData($receiver_id);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, $message, $user['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function suggestMeetingTimeModerator()

    {
        $date = json_decode($this->input->post('date') , true);
        $time = json_decode($this->input->post('time') , true);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('sender_id');
        // $user_id = $this->input->post('user_id');
        $user_id = $this->input->post('receiver_id');
        $event_id = $this->input->post('event_id');
        $comment = $this->input->post('comment');
        if ($date != '' && $time != '' && $id != '' && $receiver_id != '' && $user_id != '' && $event_id != '')
        {
            $exhibitor = $this->Attendee_model->getUser($user_id);
            $user = $this->Attendee_model->getUser($receiver_id);
            $data['location'] = $comment;
            $where['Id'] = $id;
            $save_comment = $this->Attendee_model->updateRequestModerator($data, $where);
            $message = ucfirst($exhibitor['Firstname']) . " is unable to meet with you at the time you specified. " . ucfirst($exhibitor['Firstname']) . " has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";
            foreach($date as $key => $value)
            {
                $temp = (strpos($time[$key], "AM")) ? str_replace(' AM', "", $time[$key]) : date("G:i", strtotime($time[$key]));
                $date = date('Y-m-d H:i', strtotime($value . ' ' . $temp));
                $link = base_url() . $this->Attendee_model->getSuggestUrlData($event_id) . 'changemeetingdate/' . strtotime($date) . '/' . $id;
                $message.= "<a href='" . $link . "'>" . $date . "</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['recever_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->Attendee_model->saveSuggestedDate($suggest);
            }
            $message1 = ucfirst($exhibitor['Firstname']) . " is unable to meet with you at this time.Please click here.";
            $data1 = array(
                'Message' => $message,
                'Sender_id' => $user_id,
                'Receiver_id' => $user['Id'],
                'Event_id' => $event_id,
                'Parent' => '0',
                'Time' => date("Y-m-d H:i:s") ,
                'ispublic' => '0',
                'msg_creator_id' => $user_id,
                'msg_type' => '4',
            );
            $this->Attendee_model->saveSpeakerMessage($data1);
            if ($user['gcm_id'] != '')
            {
                $extra['message_type'] = 'AttendeeSuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->Attendee_model->getEventName($event_id);
                if ($user['device'] == 'Iphone')
                {
                    $obj = new Fcm();
                    $extra['title'] = "Suggest Request Time";
                    $msg = $message1;
                    $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                }
                else
                {
                    $obj = new Gcm($event_id);
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                }
            }
            $this->Event_model->sendEmailToAttendees($event_id, ucfirst($exhibitor['Firstname']) . " " . ucfirst($exhibitor['Lastname']) . " is unable to meet with you at this time.", $user['Email'], "Notification");
            $data = array(
                'success' => true,
                'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function SaveCommentModerator()

    {
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('sender_id');
        $user_id = $this->input->post('reciver_id');
        $event_id = $this->input->post('event_id');
        $comment = $this->input->post('comment');
        if ($id != '' && $receiver_id != '' && $user_id != '' && $event_id != '')
        {
            $data['location'] = $comment;
            $where['Id'] = $id;
            $save_comment = $this->Attendee_model->updateRequestModerator($data, $where);
            $data = array(
                'success' => true,
                'message' => "Comment successfully",
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function add_user_game_point($user_id=NULL, $event_id=NULL, $rank_id=NULL)

    {
        $this->Gamification_model->add_user_point($user_id, $event_id, $rank_id);
        return true;
    }
    public function getAllMeetingRequestNew()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $data = $this->Attendee_model->getAllMeetingRequestNew($event_id, $user_id);
            $date_format = $this->Attendee_model->checkEventDateFormat($event_id);
            if (!empty($data))
            {
                foreach($data as $key => $value)
                {
                    if ($date_format[0]['date_format'] == 0)
                    {
                        $a = strtotime($value['date']);
                        $date = date("d/m/Y", $a);
                    }
                    else
                    {
                        $a = strtotime($value['date']);
                        $date = date("m/d/Y", $a);
                    }
                    $data[$key]['date'] = $date;
                }
            }
            $data = array(
                'success' => true,
                'data' => array_values($data) ,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_solar_plaza_meeting_time()

    {
        $event_id = $this->input->post('event_id');
        $date = $this->input->post('date');
        if (!empty($event_id) && !empty($date))
        {
            if ($date == '11-07-2017')
            {
                $time = array(
                    '10:40',
                    '10:55',
                    '12:55',
                    '13:10',
                    '13:25',
                    '13:40',
                    '15:40',
                    '15:55',
                    '17:10',
                    '17:25',
                    '17:40',
                    '17:55',
                    '18:10',
                    '18:25',
                    '18:40',
                    '18:55'
                );
            }
            else
            {
                $time = array(
                    '10:40',
                    '10:55',
                    '12:55',
                    '13:10',
                    '13:25',
                    '13:40',
                    '15:40'
                );
            }
            $data['time'] = $time;
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function get_ipexpo_meeting_time()

    {
        $event_id = $this->input->post('event_id');
        $date = $this->input->post('date');
        if (!empty($event_id) && !empty($date))
        {
            if ($date == '25-04-2018')
            {
                $time = array(
                    "09:30",
                    "09:45",
                    "10:00",
                    "10:15",
                    "10:30",
                    "10:45",
                    "11:00",
                    "11:15",
                    "11:30",
                    "11:45",
                    "12:00",
                    "12:15",
                    "12:30",
                    "12:45",
                    "13:00",
                    "13:15",
                    "13:30",
                    "13:45",
                    "14:00",
                    "14:15",
                    "14:30",
                    "14:45",
                    "15:00",
                    "15:15",
                    "15:30",
                    "15:45",
                    "16:00",
                    "16:15",
                    "16:30",
                    "16:45"
                );
            }
            else
            {
                $time = array(
                    "09:30",
                    "09:45",
                    "10:00",
                    "10:15",
                    "10:30",
                    "10:45",
                    "11:00",
                    "11:15",
                    "11:30",
                    "11:45",
                    "12:00",
                    "12:15",
                    "12:30",
                    "12:45",
                    "13:00",
                    "13:15",
                    "13:30",
                    "13:45",
                    "14:00",
                    "14:15",
                    "14:30",
                    "14:45",
                    "15:00",
                    "15:15",
                    "15:30",
                    "15:45"
                );
            }
            $data['time'] = $time;
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function attendee_list_native_load()

    {
        $this->load->model('Native_single_fcm/App_login_model');
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        // $this->output->enable_profiler(TRUE);
        $starttime = $this->App_login_model->get_endtime_of_last_rec_50($this->input->post('event_id'));
        $diff = microtime(true) - $starttime;
        $sec = intval($diff);
        $micro = $diff - $sec;
        $final = strftime('%T', mktime(0, 0, $sec)) . str_replace('0.', '.', sprintf('%.3f', $micro));
        $mem = $this->getSystemMemInfo();
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        if ($event_id != '' && $event_type != '')
        {
            if ($keyword != '') $where = "(u.Lastname like '%" . $keyword . "%' OR u.Firstname like '%" . $keyword . "%' OR u.Company_name like '%" . $keyword . "%' OR u.Title like '%" . $keyword . "%')";
            $attendee_data = $this->Attendee_model->getAttendeeListByEventId_new($event_id, ($where) ? $where : '', $page_no, $user_id);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start) , 4);
        $insert['start_time'] = $start;
        $insert['end_time'] = $finish;
        $insert['load_time'] = $total_time;
        $insert['used_memory'] = $mem['MemTotal'] - $mem['MemFree'];
        $insert['free_memory'] = $mem['MemFree'];
        $insert['total_memory'] = $mem['MemTotal'];
        $insert['event_id'] = $event_id;
        $insert['type'] = 'Atten.';
        $insert['created'] = date('Y/m/d H:i:s');
        $insert['interval'] = $final;
        $this->App_login_model->add_server_data_50($insert);
    }
    public function getSystemMemInfo()

    {
        $data = explode("\n", file_get_contents("/proc/meminfo"));
        $meminfo = array();
        foreach($data as $line)
        {
            list($key, $val) = array_pad(explode(":", $line, 2) , 2, null);
            $meminfo[$key] = trim($val);
        }
        return $meminfo;
    }
    public function getAllMeetingRequestWithDate() //Friday 09 March 2018 10:25:38 AM IST

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $data = $this->Attendee_model->getAllMeetingRequestWithDate($event_id, $user_id);
            $event = $this->Attendee_model->checkEventDateFormat($event_id);
            $map_meeting = $this->Attendee_model->getMapMeetingLocation($event_id);
            $start = date('Y-m-d', strtotime($event[0]['Start_date']));
            while ($start <= $event[0]['End_date'])
            {
                $data[]['date'] = date('Y-m-d', strtotime($start));
                $start = date('Y-m-d', strtotime($start . '+1 day'));
            }
            foreach($data as $key => $value)
            {
                if (empty($value['request_id']))
                {
                    unset($data[$key]);
                }
                else
                {
                    if ($event[0]['date_format'] == 0)
                    {
                        $a = strtotime($value['date']);
                        $date = date("d/m/Y", $a);
                    }
                    else
                    {
                        $a = strtotime($value['date']);
                        $date = date("m/d/Y", $a);
                    }
                    if (!empty($value['request_id']))
                    {
                        $data[$key]['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $key)));
                        $value['map_location'] = $map_meeting[$value['location']] ? : new stdClass;
                        $value['sender_id'] = '';
                        $value['receiver_id'] = '';
                    }
                    $meeting[$value['date']][] = $value;
                }
            }
            // j($meeting);
            foreach($meeting as $key => $value)
            {
                $data_new[$j]['date'] = $key;
                usort($value, 'sortByTime');
                if (!empty($value[0]['request_id'])) $data_new[$j]['data'] = $value;
                else $data_new[$j]['data'] = [];
                if (count($data_new[$j]['data']) != '1' && $event_id != '1511') unset($data_new[$j]['data'][count($data_new[$j]['data']) - 1]);
                $j++;
            }
            function sortByTime($a, $b)
            {
                $a = strtotime($a['time']);
                $b = strtotime($b['time']);
                if ($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            }
            function sortByDate($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];
                if ($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            }
            usort($data_new, 'sortByDate');
            $data = array(
                'success' => true,
                'data' => $data_new ? : [],
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function GetInvitedAttendee()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $meeting_id = $this->input->post('meeting_id');
        if (!empty($event_id) && !empty($user_id) && !empty($meeting_id))
        {
            $attendees = $this->Attendee_model->GetInvitedAttendee($event_id, $meeting_id, $user_id);
            $data = array(
                'attendees' => $attendees
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAttendeeForInviting()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $meeting_id = $this->input->post('meeting_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        if (!empty($event_id) && !empty($user_id) && !empty($meeting_id))
        {
            if ($keyword != '') $where = "(u.Lastname like '%" . $keyword . "%' OR u.Firstname like '%" . $keyword . "%' OR u.Company_name like '%" . $keyword . "%' OR u.Title like '%" . $keyword . "%')";
            $attendees = $this->Attendee_model->getAttendeeForInviting($event_id, $meeting_id, $user_id, $where);
            $limit = 10;
            $page_no = (!empty($page_no)) ? $page_no : 1;
            $start = ($page_no - 1) * $limit;
            $total = count($attendees);
            $total_page = ceil($total / $limit);
            $attendees = array_slice($attendees, $start, $limit);
            $data = array(
                'attendees' => $attendees,
                'total_page' => $total_page
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function InviteAttendee()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $meeting_id = $this->input->post('meeting_id');
        $invited_ids = json_decode($this->input->post('invited_ids') , true);
        $date = $this->input->post('date');
        $time = date('H:i:s', strtotime($this->input->post('time')));
        $location = $this->input->post('location');
        if (!empty($event_id) && !empty($user_id) && !empty($meeting_id) && !empty($invited_ids))
        {
            $main_meeting = $this->Attendee_model->getMainMeeting($meeting_id);
            foreach($invited_ids as $key => $value)
            {
                $attendee_id = $user_id;
                $receiver_id = $value;
                $attendee = $this->Attendee_model->getUser($attendee_id);
                $data['event_id'] = $event_id;
                $data['recever_attendee_id'] = $receiver_id;
                if ($attendee['Role_id'] == '6') $data['sender_exhibitor_id'] = $attendee_id;
                else $data['attendee_id'] = $attendee_id;
                $data['date'] = $main_meeting['date'];
                $data['time'] = $time;
                $data['location'] = $location;
                $data['is_invited'] = $meeting_id;
                if (!empty($main_meeting['exhibiotor_id']) && !empty($main_meeting['attendee_id']))
                {
                    $data['main_attendee'] = $main_meeting['attendee_id'];
                    $data['main_exhi'] = $main_meeting['exhibiotor_id'];
                }
                elseif (!empty($main_meeting['attendee_id']) && !empty($main_meeting['recever_attendee_id']))
                {
                    $data['main_attendee'] = $main_meeting['attendee_id'] . ',' . $main_meeting['recever_attendee_id'];
                }
                elseif (!empty($main_meeting['sender_exhibitor_id']) && !empty($main_meeting['recever_attendee_id']))
                {
                    $data['main_attendee'] = $main_meeting['recever_attendee_id'];
                    $data['main_exhi'] = $main_meeting['sender_exhibitor_id'];
                }
                elseif (!empty($main_meeting['sender_exhibitor_id']) && !empty($main_meeting['exhibiotor_id']))
                {
                    $data['main_exhi'] = $main_meeting['sender_exhibitor_id'] . ',' . $main_meeting['exhibiotor_id'];
                }
                $check_moderator = $this->Attendee_model->check_moderator($event_id, $receiver_id);
                if ($check_moderator)
                {
                    $moderator = $this->Attendee_model->getUser($check_moderator['moderator_id']);
                    $data['moderator_id'] = $moderator['Id'];
                    $data['recipient_attendee_id'] = $receiver_id;
                    $data['recever_attendee_id'] = NULL;
                }
                $result = $this->Attendee_model->saveRequest($data);
                if ($result['status'])
                {
                    $url = base_url() . $this->Attendee_model->getUrlData($event_id);
                    $user = $this->Attendee_model->getUser($receiver_id);
                    $this->add_user_game_point($attendee_id, $event_id, 2);
                    if ($attendee['Role_id'] != '6')
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has invited you to join a meeting.</a>";
                        $message1 = ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has invited you to join a meeting.";
                    }
                    else
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Company_name']) . "  has invited you to join a meeting.</a>";
                        $message1 = ucfirst($attendee['Company_name']) . " has invited you to join a meeting.";
                    }
                    if ($check_moderator)
                    {
                        $Message = "<a href='" . $url . "'>" . ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with " . ucfirst($user['Firstname']) . " " . ucfirst($user['Lastname']) . " at " . $time . " on " . date('d/m/Y', strtotime($date)) . " Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname']) . " " . ucfirst($attendee['Lastname']) . " has requested a meeting with you at " . $time . " on " . date('d/m/Y', strtotime($date)) . ".";
                    }
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $user['Id'],
                        'Event_id' => $event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s") ,
                        'ispublic' => '0',
                        'msg_creator_id' => $attendee_id,
                        'msg_type' => '3',
                    );
                    if ($check_moderator)
                    {
                        $data1['Receiver_id'] = $moderator['Id'];
                        $data1['msg_type'] = '5';
                    }
                    $this->Attendee_model->saveSpeakerMessage($data1);
                    $template = $this->Message_model->getNotificationTemplate($event_id, 'Meeting');
                    if ($check_moderator)
                    {
                        if ($moderator['gcm_id'] != '')
                        {
                            $extra['message_type'] = 'ModeratorRequestMeeting';
                            $extra['message_id'] = $moderator['Id'];
                            $extra['event'] = $this->Attendee_model->getEventName($event_id);
                            if ($moderator['device'] == 'Iphone')
                            {
                                $obj = new Fcm();
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send(1, $moderator['gcm_id'], $msg, $extra, $moderator['device']);
                            }
                            else
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($moderator['gcm_id'], $msg, $extra, $moderator['device']);
                            }
                        }
                    }
                    else
                    {
                        if ($user['gcm_id'] != '')
                        {
                            $extra['message_type'] = 'AttendeeRequestMeeting';
                            $extra['message_id'] = $user['Id'];
                            $extra['event'] = $this->Attendee_model->getEventName($event_id);
                            if ($user['device'] == 'Iphone')
                            {
                                $obj = new Fcm();
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send(1, $user['gcm_id'], $msg, $extra, $user['device']);
                            }
                            else
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($user['gcm_id'], $msg, $extra, $user['device']);
                            }
                        }
                    }
                    if ($template['send_email'] == '1')
                    {
                        $this->Event_model->sendEmailToAttendees($event_id, $template['email_content'], $user['Email'], $template['email_subject']);
                    }
                }
            }
            $data = array(
                'success' => true,
                'message' => 'Invited Successfully'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAllMeetingRequestWithDateModerator() //Thursday 15 March 2018 12:00:20 PM IST

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '')
        {
            $data = $this->Attendee_model->getAllMeetingRequestModerator($event_id, $user_id);
            $event = $this->Attendee_model->checkEventDateFormat($event_id);
            $map_meeting = $this->Attendee_model->getMapMeetingLocation($event_id);
            if ($event_id != '1511')
            {
                $start = date('Y-m-d', strtotime($event[0]['Start_date']));
                while ($start <= $event[0]['End_date'])
                {
                    $data[]['date'] = date('Y-m-d', strtotime($start));
                    $start = date('Y-m-d', strtotime($start . '+1 day'));
                }
            }
            foreach($data as $key => $value)
            {
                if ($event[0]['date_format'] == 0)
                {
                    $a = strtotime($value['date']);
                    $date = date("d/m/Y", $a);
                }
                else
                {
                    $a = strtotime($value['date']);
                    $date = date("m/d/Y", $a);
                }
                if (!empty($value['request_id']))
                {
                    $data[$key]['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $key)));
                    $value['map_location'] = $map_meeting[$value['location']] ? : new stdClass;
                    /*$value['sender_id'] = '';
                    $value['receiver_id'] = '';*/
                    $value['show_invite_more'] = '0';
                    $value['Logo'] = '';
                }
                $meeting[$value['date']][] = $value;
            }
            foreach($meeting as $key => $value)
            {
                $data_new[$j]['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $key)));
                usort($value, 'sortByTime');
                if (!empty($value[0]['request_id'])) $data_new[$j]['data'] = $value;
                else $data_new[$j]['data'] = [];
                if (count($data_new[$j]['data']) != '1' && $event_id != '259') unset($data_new[$j]['data'][count($data_new[$j]['data']) - 1]);
                $j++;
            }
            function sortByTime($a, $b)
            {
                $a = strtotime($a['time']);
                $b = strtotime($b['time']);
                if ($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            }
            function sortByDate($a, $b)
            {
                $a = $a['date'];
                $b = $b['date'];
                if ($a == $b)
                {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            }
            usort($data_new, 'sortByDate');
            $data = array(
                'success' => true,
                'data' => $data_new ? : [],
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingDate() //Friday 16 March 2018 10:39:44 AM IST 123

    {
        $event_id = $this->input->get_post('event_id');
        $user_id = $this->input->get_post('user_id');
        $loggedin_user_id = $this->input->post('loggedin_user_id');
        if (!empty($user_id) && !empty($event_id))
        {
            $attendee = $this->Attendee_model->getUser($loggedin_user_id);
            if ($attendee['Role_id'] == '4')
            {
                $attendee_R = $this->Attendee_model->getUser($user_id);
                if ($attendee_R['Role_id'] == '4')
                {
                    // $max_limit = $this->Attendee_model->checkMaxMeetingLimit($loggedin_user_id, $event_id);
                    $max_limit = $this->Attendee_model->checkGroupMaxMeetingLimit($loggedin_user_id, $event_id);

                    if ($max_limit)
                    {
                        $data = array(
                            'success' => true,
                            'message' => "You have reached the maximum number of meeting requests you can send.",
                            'flag' => '1',
                        );
                        echo json_encode($data);
                        exit;
                    }
                }
                else
                {
                    $meeting_dates = $this->Attendee_model->getRequestMeetingDate($event_id);
                    $data = array(
                        'success' => true,
                        'meeting_dates' => $meeting_dates,
                        'flag' => '0'
                    );
                }
            }
            $meeting_dates = $this->Attendee_model->getRequestMeetingDate($event_id);
            $data = array(
                'success' => true,
                'meeting_dates' => $meeting_dates,
                'flag' => '0'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingTime() //Friday 16 March 2018 10:39:48 AM IST

    {
        $event_id = $this->input->get_post('event_id');
        $date = $this->input->get_post('date');
        $user_id = $this->input->get_post('user_id');
        $is_exhibitor = $this->input->post('is_exhibitor');
        if (!empty($user_id) && !empty($event_id) && !empty($date))
        {
            $meeting_time = $this->Attendee_model->getRequestMeetingTime($event_id, $user_id, $date, $is_exhibitor);
            $data = array(
                'success' => true,
                'meeting_time' => $meeting_time
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingLocation() //Friday 16 March 2018 10:39:53 AM IST

    {

        $event_id = $this->input->get_post('event_id');
        $date = $this->input->get_post('date');
        $time = $this->input->get_post('time');
        $user_id = $this->input->get_post('user_id');
        if (!empty($user_id) && !empty($event_id) && !empty($date))
        {
            $meeting_location = $this->Attendee_model->getRequestMeetingLocation($event_id, $date, $time);
            /*if(empty($meeting_location))
            {
            $data = array(
            'success' => false,
            'message' => "There is no location available at this time."
            );
            }
            else
            {*/
            $data = array(
                'success' => true,
                'meeting_location' => $meeting_location
            );
            // }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAttendeeCategories() //Tuesday 27 March 2018 02:27:13 PM IST

    {
        $event_id = $this->input->post('event_id');
        if (!empty($event_id))
        {
            $data['attendee_categories'] = $this->Attendee_model->getAttendeeCategories($event_id);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function blockAttendee()

    {
        extract($this->input->post());
        if (!empty($event_id) && !empty($from_id) && !empty($to_id) && !empty($status))
        {
            $data['user_id_from'] = $from_id;
            $data['user_id_to'] = $to_id;
            $data['event_id'] = $event_id;
            $this->Attendee_model->blockAttendee($data, $status);
            $msg = 'Attendee ';
            $msg.= $status == '1' ? 'Blocked' : 'Unblocked';
            $msg.= ' Successfully.';
            $data = array(
                'success' => true,
                'message' => $msg
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function hideMyIdentity()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); //1=hide,0=unhide
        if (!empty($event_id) && !empty($user_id))
        {
            $this->Attendee_model->hideMyIdentity($event_id, $user_id, $status);
            $msg = $status ? 'Identity Hidden Successfully' : 'Identity Unhidden Successfully';
            $data = array(
                'success' => true,
                'message' => $msg
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }

    public function getAttendeeFilters()

    {
        extract($this->input->post());
        if (!empty($event_id))
        {
            $data = $this->Attendee_model->getAttendeeFilters($event_id, $category_id);
            if ($event_id == '1397')
            {
                $remove_keywords = array(
                    'Administrative ',
                    'All of the above',
                    'All relevant to consulting business',
                    'Dietary / Health Supplements ',
                    'Everything',
                    'GENERAL ADMINISTRATION',
                    'Government (not Government)',
                    'Health Supplements ',
                    'Localisation',
                    'Medical',
                    'Partner',
                    'Project Managemment',
                    'Restaurant',
                    'Standards Development',
                    'Strategic Planning',
                    'Supply Chain',
                    'Technical',
                    'Technical Support',
                    'Technical support on Sales and research',
                    'Technology Transfer ',
                    'Trade Promotion',
                    'Trading and manufacture',
                    'Translator',
                    'ee',
                    'founder',
                    'md',
                    'physical therapy/nutrition',
                    'planning and policy',
                    'reporting',
                    'share videos ',
                    'technology transfer / marketin'
                );
                foreach($data as $key => $value)
                {
                    if ($value['column_name'] == 'Job Role')
                    {
                        $data[$key]['keywords'] = array_values(array_diff($value['keywords'], $remove_keywords));
                    }
                }
            }
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function attendee_list_v2() //8/14/2018 2:22:05 PM

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $event_type = $this->input->post('event_type');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');
        // $filter_keywords = str_replace(' ','',$this->input->post('filter_keywords'));
        $filter_keywords = $this->input->post('filter_keywords');
        $category_id = $this->input->post('category_id');
        $advance_filter = json_decode($this->input->post('advance_filter') , true);
        if ($event_id != '')
        {
            $filter_keywords = $this->Attendee_model->getCategoryKeywords($category_id);
            if ($keyword != '') $where = "((u.Lastname like '%" . $keyword . "%' OR u.Firstname like '%" . $keyword . "%' OR u.Company_name like '%" . $keyword . "%' OR u.Title like '%" . $keyword . "%') OR  concat(u.Firstname, ' ', u.Lastname) like concat('%', replace('" . $keyword . "', ' ', '%'), '%'))";
            $attendee_data = $this->Attendee_model->attendee_list_v2($event_id, ($where) ? $where : '', $page_no, $user_id, $filter_keywords, $advance_filter);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
