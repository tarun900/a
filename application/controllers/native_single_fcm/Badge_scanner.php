<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Badge_scanner extends CI_Controller

{
	public function __construct()

	{
		parent::__construct();
		$this->load->model('Native_single_fcm/Gamification_model');
		$this->load->model('Native_single_fcm/Badge_scanner_model');
		$this->load->model('native_single_fcm_v2/Survay_model');
	}
	public function ScanBadge()

	{
		extract($this->input->post());
		if (!empty($user_id) && !empty($code) && !empty($event_id) && !empty($role_id))
		{
			if ($role_id == '4')
			{
				$res = $this->Badge_scanner_model->share_cotact($event_id, $user_id, $code);
			}
			else
			{
				$user_id_1 = $this->Badge_scanner_model->get_user_id_by_code($code);
				$this->Gamification_model->add_user_point($user_id_1, $event_id, 7);
				if ($event_id == '1511' || $event_id == '1381')
				{
					// save exhi lead for podcast
					$data['exhi_user_id'] = $user_id;
					$data['user_id'] = $user_id_1;
					$data['event_id'] = $event_id;
					$this->Badge_scanner_model->save_exhi_lead($data);
				}
				$res['status'] = false;
			}
			$data = array(
				'success' => $res ? $res['status'] : true,
				'message' => ($res['msg']) ? : 'Badge Scanned Successfully.',
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
			);
		}
		echo json_encode($data);
		exit();
	}
	public function scanQr()
	{
		extract($this->input->post());
		if(!empty($user_id) && !empty($code) && !empty($event_id) && !empty($role_id))
		{
			if (filter_var($code, FILTER_VALIDATE_EMAIL))
			{
				if($role_id == '4')
				{	
					$res = $this->Badge_scanner_model->share_cotact($event_id,$user_id,$code);
				}
				else
				{	
					$user_id_1 = $this->Badge_scanner_model->get_user_id_by_code($code);
					$this->Gamification_model->add_user_point($user_id_1,$event_id,7);
					if($event_id == '1511' || $event_id == '1381')
					{
						//save exhi lead for podcast
						$data['exhi_user_id'] = $user_id;
						$data['user_id'] = $user_id_1;
						$data['event_id'] = $event_id;
						$this->Badge_scanner_model->save_exhi_lead($data);
					}
					$res['status'] = false;
				}
				$data = array(
					'success' => $res?$res['status']:true,
					'message' => ($res['msg'])?:'Badge Scanned Successfully.',
					'code_type' => '1',
					);
			}
			else
			{
				//survey module
				$survey_id = explode('-', $code);
	            $survey = $this->Survay_model->check_survey_status($survey_id[1],$event_id);
	            
	            $data = array(
	              'success' => true,
	              'is_survey_avilable' => $survey?'1':'0',
	              'survey_id' => $survey_id[1],
	              'message' => 'No survey available for this QR code.',
	              'code_type' => '0',
	            );
			}
		}	
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid Parameters'
				);
		}
		echo json_encode($data);
	}
}
