<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Photofilter extends CI_Controller

{
    public $menu;

    public $cmsmenu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/Photofilter_model');
        /*$this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values)
        {
        $cmsbannerimage_decode = json_decode($values['Images']);
        $cmslogoimage_decode = json_decode($values['Logo_images']);
        $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
        $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }*/
    }
    /*** Get Image List of Photo Filter ***/
    public function getFiltersByEvent()

    {
        $event_id = $this->input->post('event_id');
        if ($event_id != '')
        {
            $images = $this->Photofilter_model->getFiltersByEvent($event_id);
            $imgdata = [];
            foreach($images as $key => $value)
            {
                $t['image'] = base_url() . "assets/photo_filter/$event_id/" . $value['image'];
                $t['title'] = $value['title'];
                $t['id'] = $value['id'];
                $imgdata[] = $t;
            }
            if ($images != '')
            {
                $data = array(
                    'success' => true,
                    'data' => $imgdata
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No images found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /* Upload photo using photo filter */
    public function upload()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id != '' && !empty($_FILES['image']))
        {
            $ext = end((explode(".", $_FILES["image"]["name"])));
            $image_name = rand() . strtotime(time()) . rand() . "." . $ext;
            if (!is_dir('assets/photo_filter_uploads/' . $event_id))
            {
                mkdir(FCPATH . '/assets/photo_filter_uploads/' . $event_id, 0777, true);
            }
            $target_path = FCPATH . "/assets/photo_filter_uploads/" . $event_id . '/' . $image_name;
            move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
            $saveInfo['event_id'] = $event_id;
            $saveInfo['user_id'] = $user_id;
            $saveInfo['image'] = $image_name;
            $result = $this->Photofilter_model->saveImageInfo($saveInfo);
            if ($result)
            {
                $data = array(
                    'success' => true,
                    'message' => "Image uploaded successfully"
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "Error in uploading file, Please try again"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*** Get uploaded photos by user ***/
    public function getPhotosByUser()

    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $user_id)
        {
            $images = $this->Photofilter_model->getPhotosByUser($event_id, $user_id);
            $imgdata = [];
            foreach($images as $key => $value)
            {
                $t['image'] = base_url() . "assets/photo_filter_uploads/$event_id/" . $value['image'];
                $t['id'] = $value['id'];
                $imgdata[] = $t;
            }
            if ($images != '')
            {
                $data = array(
                    'success' => true,
                    'data' => $imgdata
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No images found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /* copy images from userfiles to event wise folders */
    public function changeImageDir()

    {
        /*$data = $this->db->select('*')->from('photo_filter_image')->get()->result_array();
        foreach ($data as $key => $value) {
        if(!is_dir('assets/photo_filter/'.$value['event_id']))
        {
        mkdir(FCPATH.'/assets/photo_filter/'.$value['event_id'], 0777, true);
        }
        if(copy(FCPATH.'/assets/user_files/'.$value['image'],FCPATH.'/assets/photo_filter/'.$value['event_id'].'/'.$value['image']))
        {
        echo "success-$key<br />";
        }
        else
        {
        echo "failed-$key<br />";
        }
        }
        */
    }
}