<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dims extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/Dims_model');
        $this->load->model('Native_single_fcm/App_login_model');
    }
    public function getDefaultEvent()
    {
        $event = $this->Dims_model->getDimsEvent();

        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
               $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
}
?>   