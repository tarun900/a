<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/App_profile_model');
        $this->load->model('Native_single_fcm/Event_template_model');
        $this->load->model('Native_single_fcm/Agenda_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->library('RC4');
    }
    public function getStateList()
    {
    	$country_id=$this->input->post('country_id');
    	$token=$this->input->post('token');
        $event_id=$this->input->post('event_id');
    	if($country_id!='' && $token!='' && $event_id!='')
    	{
	    	$user = $this->App_login_model->check_token_with_event($token,$event_id,1);
	    	if (empty($user)) 
	        {
	            $data = array(
	                'success' => false,
	                'data' => array(
	                    'msg' => 'Please check token or event.'
	                )
	            );   
	        } 
	        else 
	        {
	          	$statelist = $this->App_profile_model->getStateListByCountry($country_id);
	          	$data = array(
                    'success' => true,
	                'data' => $statelist,
	            );
	        }
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
    	}
        echo json_encode($data);
    }
    public function update_profile()
    {
        
        $firstname =$this->input->post('first_name');
        $lastname =$this->input->post('last_name');
        $event_id =$this->input->post('event_id');
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');
        $salutation =$this->input->post('salutation');
        $title =$this->input->post('title');
        $cmpy_name =$this->input->post('cmpy_name');

        if($user_id!='' && $event_id!='' && $firstname!='' && $lastname!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $update_data['Firstname']=$firstname;
                $update_data['Lastname']=$lastname;
                $update_data['Company_name'] =$cmpy_name;
                $update_data['Title'] =$title;
                $update_data['Salutation'] =$salutation;
                                
                if(!empty(($_FILES['logo']['name'])))
                {
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName;          
                    move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }
                

                if( $this->input->post('logo')!='')
                {
                    $url = $this->input->post('logo');
                    $newImageName = $user_id . "_" . rand().time().".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 

                    file_put_contents($target_path, file_get_contents($url));
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }
                
                if(empty(($_FILES['logo']['name'])) && $this->input->post('logo')=='')
                {
                    $update_data['Logo'] ="";
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }

                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }
    public function update_contact_details()
    {
        $password =$this->input->post('password');
        $linkedin_url =$this->input->post('linkedin_url');
        $facebook_url =$this->input->post('facebook_url');
        $twitter_url =$this->input->post('twitter_url');
        $mobile_no =$this->input->post('mobile_no');
        $country_id =$this->input->post('country_id');
        $user_id =$this->input->post('user_id');

        if($user_id!='')
        {
                $event_id = $this->App_login_model->getEventId($user_id);
                if($password!='' && $event_id == 447)
                {
                    $rc4_obj = new RC4();
                    $update_data['Password']=$rc4_obj->encrypt($password);
                }
                else if($password!='')
                    $update_data['Password']=md5($password);
                $update_data['Mobile']=$mobile_no;
                $update_data['Country'] =$country_id;
                
                $social['Facebook_url'] = $facebook_url;
                $social['Twitter_url'] = $twitter_url;
                $social['Linkedin_url'] = $linkedin_url;

                $this->App_login_model->saveSocialLinks($user_id,$social);
                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                $data['Facebook_url'] = $facebook_url;
                $data['Twitter_url'] = $twitter_url;
                $data['Linkedin_url'] = $linkedin_url;
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }
    public function get_additional_data()
    {   
        $event_id =$this->input->post('event_id');
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');

        if($user_id!='' && $event_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $data = $this->App_profile_model->get_extra_column($user_id,$event_id);
                $social = $this->App_profile_model->get_social_login_status($event_id);
                if($event_id == '1087' || $event_id =='1404' || $event_id =='1413')
                    $social['is_linkedin'] = '1';
                $identityHidden = $this->App_profile_model->getHideenIdentity($event_id,$user_id);

                $data = array(
                    'success' => true,
                    'data' => ($data) ? $data : array(),
                    'social' => $social,
                    'identity_hidden' => $identityHidden,
                    'enable_hide_identity' => $this->App_profile_model->getEvent($event_id)['enable_hide_my_identity'],
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }
    public function update_profile_with_additional_data()
    {
        
        $firstname =$this->input->post('first_name');
        $lastname =$this->input->post('last_name');
        $event_id =$this->input->post('event_id');
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');
        $salutation =$this->input->post('salutation');
        $title =$this->input->post('title');
        $cmpy_name =$this->input->post('cmpy_name');
        $additional_data['json_data'] =  $this->input->post('extra');
        $password =$this->input->post('password');
        if($user_id!='' && $event_id!='' && $firstname!='' && $lastname!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $update_data['Firstname']=$firstname;
                $update_data['Lastname']=$lastname;
                $update_data['Company_name'] =$cmpy_name;
                $update_data['Title'] =$title;
                $update_data['Salutation'] =$salutation;
                
                if($password!='' && $event_id == 447)
                {
                    $rc4_obj = new RC4();
                    $update_data['Password']=$rc4_obj->encrypt($password);
                }
                else if($password!='')
                    $update_data['Password']=md5($password);

                if(!empty(($_FILES['logo']['name'])))
                {
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName;          
                    move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                }
                

                if( $this->input->post('logo')!='')
                {
                    $url = $this->input->post('logo');
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "./assets/user_files/".$newImageName; 
                    file_put_contents($target_path, file_get_contents($url));
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                }
                	
                if(!empty($additional_data))
                {
                    $this->App_profile_model->save_extra_column($user_id,$additional_data,$event_id);
                }
                $update_data['updated_date'] = date('Y-m-d H:i:s');

                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }

    public function update_contact_details_v2()
    {
        $password =$this->input->post('password');
        $linkedin_url =$this->input->post('linkedin_url');
        $facebook_url =$this->input->post('facebook_url');
        $twitter_url =$this->input->post('twitter_url');
        $mobile_no =$this->input->post('mobile_no');
        $country_id =$this->input->post('country_id');
        $user_id =$this->input->post('user_id');

        if($user_id!='')
        {
                $event_id = $this->App_login_model->getEventId($user_id);
                if($password!='' && $event_id == 447)
                {
                    $rc4_obj = new RC4();
                    $update_data['Password']=$rc4_obj->encrypt($password);
                }
                else if($password!='')
                    $update_data['Password']=($password);
                $update_data['Mobile']=$mobile_no;
                $update_data['Country'] =$country_id;
                
                $social['Facebook_url'] = $facebook_url;
                $social['Twitter_url'] = $twitter_url;
                $social['Linkedin_url'] = $linkedin_url;

                $this->App_login_model->saveSocialLinks($user_id,$social);
                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                $data['Facebook_url'] = $facebook_url;
                $data['Twitter_url'] = $twitter_url;
                $data['Linkedin_url'] = $linkedin_url;
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }

    public function update_profile_with_additional_data_v2()
    {
        
        $firstname =$this->input->post('first_name');
        $lastname =$this->input->post('last_name');
        $event_id =$this->input->post('event_id');
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');
        $salutation =$this->input->post('salutation');
        $title =$this->input->post('title');
        $cmpy_name =$this->input->post('cmpy_name');
        $additional_data['json_data'] =  $this->input->post('extra');
        $password =$this->input->post('password');
        if($user_id!='' && $event_id!='' && $firstname!='' && $lastname!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $update_data['Firstname']=$firstname;
                $update_data['Lastname']=$lastname;
                $update_data['Company_name'] =$cmpy_name;
                $update_data['Title'] =$title;
                $update_data['Salutation'] =$salutation;
                
                if($password!='' && $event_id == 447)
                {
                    $rc4_obj = new RC4();
                    $update_data['Password']=$rc4_obj->encrypt($password);
                }
                else if($password!='')
                    $update_data['Password']=$password;

                if(!empty(($_FILES['logo']['name'])))
                {
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName;          
                    move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                }
                

                if( $this->input->post('logo')!='')
                {
                    $url = $this->input->post('logo');
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "./assets/user_files/".$newImageName; 
                    file_put_contents($target_path, file_get_contents($url));
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                }
                    
                if(!empty($additional_data))
                {
                    $this->App_profile_model->save_extra_column($user_id,$additional_data,$event_id);
                }
                $update_data['updated_date'] = date('Y-m-d H:i:s');

                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }

    public function update_profile_v2()
    {
        
        $firstname =$this->input->post('first_name');
        $lastname =$this->input->post('last_name');
        $event_id =$this->input->post('event_id');
        $token =$this->input->post('token');
        $user_id =$this->input->post('user_id');
        $salutation =$this->input->post('salutation');
        $title =$this->input->post('title');
        $cmpy_name =$this->input->post('cmpy_name');

        if($user_id!='' && $event_id!='' && $firstname!='' && $lastname!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $update_data['Firstname']=$firstname;
                $update_data['Lastname']=$lastname;
                $update_data['Company_name'] =$cmpy_name;
                $update_data['Title'] =$title;
                $update_data['Salutation'] =$salutation;
                                
                if(!empty(($_FILES['logo']['name'])))
                {
                    $newImageName = $user_id . "_" . round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName;          
                    move_uploaded_file($_FILES['logo']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }
                

                if( $this->input->post('logo')!='')
                {
                    $url = $this->input->post('logo');
                    $newImageName = $user_id . "_" . rand().time().".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 

                    file_put_contents($target_path, file_get_contents($url));
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    $this->App_profile_model->make_thumbnail($user_id,$newImageName);
                    $update_data['Logo'] =$newImageName;
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }
                
                if(empty(($_FILES['logo']['name'])) && $this->input->post('logo')=='')
                {
                    $update_data['Logo'] ="";
                    $update_data['updated_date'] = date('Y-m-d H:i:s');
                }

                $this->App_profile_model->update_user_profile($user_id,$update_data);
                $data=$this->App_login_model->getUserDetailsId($user_id,$event_id);
                if(!empty($data['Country']))
                {
                    $data['country_name']=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $data['country_name']="";
                }
                if(!empty($data['State']))
                {
                    $data['state_name']=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $data['state_name']="";
                }
                if(strpos($data['Logo'], 'http')!==false) 
                {
                    $data['is_logo_url'] = '1';
                }
                else
                {
                    $data['is_logo_url'] = '0';
                }
                $data = array(
                    'data' => $data,
                    'success' => true,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);  
    }
}
