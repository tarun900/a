    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Maps extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Native_single_fcm/App_login_model');
        $this->load->model('Native_single_fcm/Cms_model');
        $this->load->model('Native_single_fcm/Event_model');
        $this->load->model('Native_single_fcm/Map_model');

        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function map_list()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {   

                $maps = $this->Map_model->getMapsListByEventId($event_id);
                $data = array(
                    'map_list' => $maps,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_details()
    { 
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $map_id=$this->input->post('map_id');
        $lang_id=$this->input->post('lang_id');
        if($event_id == '634' && empty($map_id))
        {
            $map_id = '2129';
        }
        if($event_id!='' && $event_type!='' && $map_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);

            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {   
                
                $user = $this->Map_model->getUser($token);
                $map_details = $this->Map_model->getMapsDetailsByMapId($event_id,$map_id,$lang_id);
                //echo "<pre>"; print_r(json_encode($map_details)); exit();
                $image_mapping_details = $this->Map_model->getImageMappingDetailsByMapId($map_id,$event_id,$user['Id'],$lang_id);
                $map_details[0]['Images'] = ($map_details[0]['Images']) ?: "";
                $map_details[0]['Map_desc'] = html_entity_decode($map_details[0]['Map_desc']);
                $map_details[0]['area'] = $map_details[0]['Map_desc'] == '0' ? "" : $map_details[0]['Map_desc'];
                $data = array(
                    'map_details' => $map_details,
                    'image_mapping_details' => $image_mapping_details,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_details_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $map_id=$this->input->post('map_id');
        if($event_id!='' && $event_type!='' && $map_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $user = $this->Map_model->getUser($token);
                $map_details = $this->Map_model->getMapsDetailsByMapId($event_id,$map_id);
                $image_mapping_details = $this->Map_model->getImageMappingDetailsByMapId($map_id,$event_id,$user['Id']);
                $data = array(
                    'floor_plan_icon' => 'http://www.allintheloop.net/assets/images/Floor_Plan_icon.png',
                    'google_map_icon' => 'http://www.allintheloop.net/assets/images/Google_Maps_icon.jpg',
                    'map_details' => $map_details,
                    'image_mapping_details' => $image_mapping_details,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $dat
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_list_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $lang_id=$this->input->post('lang_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $maps = $this->Map_model->getMapsListByEventIdNew($event_id,$lang_id);
                $data = array(
                    'map_list' => $maps,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function compress_image($data=NULL)
    {   

        $source_url = base_url()."assets/user_files/".$data;
        $info = getimagesize($source_url);
        $new_name = "new_".$data;
        $new_name = $data;
        $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

        if ($info['mime'] == 'image/jpeg')
        {   
            $quality = 50;
            $image = imagecreatefromjpeg($source_url);
            imagejpeg($image, $destination_url, $quality);
        }
        elseif ($info['mime'] == 'image/gif')
        {   
            $quality = 5;
            $image = imagecreatefromgif($source_url);
            imagegif($image, $destination_url, $quality);

        }
        elseif ($info['mime'] == 'image/png')
        {   
            $quality = 5;
            $image = imagecreatefrompng($source_url);
            $background = imagecolorallocatealpha($image,255,0,255,127);
            imagecolortransparent($image, $background);
            imagealphablending($image, false);
            imagesavealpha($image, true);
            imagepng($image, $destination_url, $quality);
        }
        return $new_name;    
    }
    public function get_exi_info_arab()//Arab Health Map
    {   

        $map_id = $this->input->get_post('map_id');

        switch ($map_id) {
            case '2111':
                $file = './assets/geojson/arab_health/Hall_1_4_2111_new.geojson';
                break;
            case '2112':
                $file = './assets/geojson/arab_health/Hall_5_8_2112.geojson';
                break;
            case '2113':
                $file = './assets/geojson/arab_health/Hall_1_7_2113_new.geojson';
                break;
        }
        $data = file_get_contents($file);
        $data = json_decode($data,true);
    
        foreach ($data['features'] as $key => $value)
        {   
            $tmp = $this->App_login_model->get_exhi_info_arab($value['properties']['Text']);
            $data['features'][$key]['properties']['page_id'] = (!empty($tmp['Id'])) ? $tmp['Id'] : '';
            $data['features'][$key]['properties']['exi_id'] = (!empty($tmp['user_id'])) ? $tmp['user_id'] : '';
            $img = (!empty($tmp['company_logo']) && $tmp['company_logo'] != '[null]') ? json_decode($tmp['company_logo']) : [];
            $data['features'][$key]['properties']['company_logo'] = (!empty($img[0])) ? $img[0] : '';
            $data['features'][$key]['properties']['comapany_name'] = (!empty($tmp['Heading'])) ? $tmp['Heading'] : '';
            // $data['features'][$key]['properties']['exhi_desc'] = (!empty($tmp['Description'])) ? strip_tags(substr($tmp['Description'],0,50)) : '';
            $tmp_desc = strip_tags($tmp['Description']);
            $data['features'][$key]['properties']['exhi_desc'] = !empty($temp_desc) ? substr($tmp_desc,0,50) : '';
            
        }
        // j($data);
        $data = json_encode($data);
        //$result=file_put_contents($file,$data);

        /*if($result === false) {
            echo "false";
        } else {
            echo "true";
        }*/
        echo $data;exit;
    }
    public function get_exi_info_medlab()//Arab Health Map
    {   

        $file = './assets/geojson/medlab/Medlab-BoothNumber.geojson';
        
        $data = file_get_contents($file);
        $data = json_decode($data,true);
        $event_id = '1012';
        foreach ($data['features'] as $key => $value)
        {   
            $tmp = $this->App_login_model->get_exhi_info_map($value['properties']['Text'],$event_id);
            $data['features'][$key]['properties']['page_id'] = (!empty($tmp['Id'])) ? $tmp['Id'] : '';
            $data['features'][$key]['properties']['exi_id'] = (!empty($tmp['user_id'])) ? $tmp['user_id'] : '';
            $img = (!empty($tmp['company_logo']) && $tmp['company_logo'] != '[null]') ? json_decode($tmp['company_logo']) : [];
            $data['features'][$key]['properties']['company_logo'] = (!empty($img[0])) ? $img[0] : '';
            $data['features'][$key]['properties']['comapany_name'] = (!empty($tmp['Heading'])) ? $tmp['Heading'] : '';
            // $data['features'][$key]['properties']['exhi_desc'] = (!empty($tmp['Description'])) ? strip_tags(substr($tmp['Description'],0,50)) : '';
            $tmp_desc = strip_tags($tmp['Description']);
            $data['features'][$key]['properties']['exhi_desc'] = !empty($temp_desc) ? substr($tmp_desc,0,50) : '';
            
        }
        // j($data);
        $data = json_encode($data);
        //$result=file_put_contents($file,$data);

        /*if($result === false) {
            echo "false";
        } else {
            echo "true";
        }*/
        echo $data;exit;
    }
    public function exhi_map_relation()
    {
        $map_id = array('2111','2112','2113');

        foreach ($map_id as $key => $value)
        {
            switch ($value)
            {
                case '2111':
                    $file = './assets/geojson/arab_health/Hall_1_4_2111_new.geojson';
                    break;
                case '2112':
                    $file = './assets/geojson/arab_health/Hall_5_8_2112.geojson';
                    break;
                case '2113':
                    $file = './assets/geojson/arab_health/Hall_1_7_2113_new.geojson';
                    break;
            }

            $data = file_get_contents($file);
            $data = json_decode($data,true);


            foreach ($data['features'] as $key1 => $value1)
            {   
                $tmp = $this->App_login_model->get_exhi_info_arab($value1['properties']['Text']);
                if(!empty($tmp['Id']))
                {
                    $insert['exhi_id'] = $tmp['Id'];
                    $insert['map_id'] = $value;
                    $insert['stand_no'] = $value1['properties']['Text'];
                    $insert['event_id'] = '634';
                    $res = $this->db->where('exhi_id',$tmp['Id'])->where('event_id','634')->get('exhi_map_id')->row_array();
                    if(!empty($res))
                    {
                        $this->db->where($res)->update('exhi_map_id',$insert);
                    }
                    else
                    {
                        $this->db->insert('exhi_map_id',$insert);
                    }
                }               
            }
        }
        j('Map Relation Success Fully');
    }
    public function get_geojson()
    {   
        $event_id = $this->input->post('event_id');
        $map_id = $this->input->post('map_id');
        $test = $this->input->post('test');
        if(!empty($event_id))
        {   

            $data = $this->Map_model->get_geojson($event_id,$map_id);
            /*if(!$test)
            {
                $data = $this->Map_model->get_geojson($event_id,$map_id);
            }
            if($test)
            {
                $file = './assets/geojson/fha/test.geojson';
                if(file_exists($file))
                {
                    $data = file_get_contents($file);
                    echo $data;exit();
                }
                else
                {   
                    $data = $this->Map_model->get_geojson($event_id,$map_id);
                    $myfile = fopen($file, "w");
                    fwrite($myfile, json_encode($data));
                    fclose($myfile);
                }
            }*/
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
                );
        }

        echo json_encode($data);exit();
    }
    public function exhi_map_relation_FHA()
    {
        $map_id = array('2097','1565','2098');

        foreach ($map_id as $key => $value)
        {
            switch ($value)
            {
                case '2097':
                    $file = './assets/geojson/fha/Singapore_merged_file.geojson';
                    break;
                case '1565':
                    $file = './assets/geojson/fha/Suntec_Singapore_Level6-2.geojson';
                    break;
                case '2098':
                    $file = './assets/geojson/fha/Suntec_Singapore_level4.geojson';
                    break;
            }

            $data = file_get_contents($file);
            $data = json_decode($data,true);

            foreach ($data['features'] as $key1 => $value1)
            {   
                $tmp = $this->App_login_model->get_exhi_info_map($value1['properties']['Text'],'585');
                if(!empty($tmp['Id']))
                {
                    $insert['exhi_id'] = $tmp['Id'];
                    $insert['map_id'] = $value;
                    $insert['stand_no'] = $value1['properties']['Text'];
                    $insert['event_id'] = '585';
                    $res = $this->db->where('exhi_id',$tmp['Id'])->where('event_id','634')->get('exhi_map_id')->row_array();
                    if(!empty($res))
                    {
                        $this->db->where($res)->update('exhi_map_id',$insert);
                    }
                    else
                    {
                        $this->db->insert('exhi_map_id',$insert);
                    }
                }               
            }
        }
        j('Map Relation Success Fully');
    }
    public function exhi_map_relation_connect_tech()
    {
        $map_id = array('2890','2891','2884','2892','2893','2885');
        $event_id = '870';
        foreach ($map_id as $key => $value)
        {
            $file = './assets/geojson/connect_tech/'.$value.'.geojson';
            $data = file_get_contents($file);
            $data = json_decode($data,true);

            foreach ($data['features'] as $key1 => $value1)
            {   
                $tmp = $this->App_login_model->get_exhi_info_map($value1['properties']['Text'],$event_id);
                if(!empty($tmp['Id']))
                {
                    $insert['exhi_id'] = $tmp['Id'];
                    $insert['map_id'] = $value;
                    $insert['stand_no'] = $value1['properties']['Text'];
                    $insert['event_id'] = $event_id;
                    $res = $this->db->where('exhi_id',$tmp['Id'])->where('event_id',$event_id)->get('exhi_map_id')->row_array();
                    if(!empty($res))
                    {
                        $this->db->where($res)->update('exhi_map_id',$insert);
                    }
                    else
                    {
                        $this->db->insert('exhi_map_id',$insert);
                    }
                }               
            }
        }
        j('Map Relation Success Fully');
    }
}