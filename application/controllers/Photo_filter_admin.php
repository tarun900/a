<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Photo_filter_admin extends FrontendController
{
	function __construct()
	{
		$this->data['pagetitle'] = 'Photo Filter';
		$this->data['smalltitle'] = 'Upload Your Photo.';
		$this->data['page_edit_title'] = 'edit';
		$this->data['breadcrumb'] = 'Photo Filter';
		parent::__construct($this->data);
		$this->load->model('Formbuilder_model');
		$this->load->model('Agenda_model');
		$this->load->model('Event_model');
		$this->load->model('Setting_model');
		$this->load->model('Event_template_model');
		$this->load->library('upload');
		$this->load->library('email');
		$user = $this->session->userdata('current_user');
		$eventid=$this->uri->segment(3);

        $this->load->database();
        $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
        $user_events =  array_filter(array_column_1($user_events,'Event_id'));
        if(!in_array($eventid,$user_events))
        {
             redirect('Forbidden');
        }

		$eventmodule=$this->Event_model->geteventmodulues($eventid);
		$module=json_decode($eventmodule[0]['module_list']);
		if(!in_array('54',$module))
		{
			echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
		}
		$event_templates = $this->Event_model->view_event_by_id($eventid);
		$this->data['Subdomain'] = $event_templates[0]['Subdomain'];

		$event = $this->Event_model->get_module_event($eventid);
		$menu_list = explode(',', $event[0]['checkbox_values']);

		$roledata = $this->Event_model->getUserRole($eventid);
		if(!empty($roledata))
		{
		   $roleid = $roledata[0]->Role_id;
		   $rolename = $roledata[0]->Name;
		   $cnt = 0;
		   $req_mod = ucfirst($this->router->fetch_class());
		   if ($this->data['pagetitle'] == "Photo Filter")
		   {
		        $title = "photo_filter";
		   }
		   $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
		}
		else
		{
		   $cnt=0;
		}
		if (!empty($user[1]['event_id_selected']))
		{
		   $this->data['event_id_selected'] = $user[1]['event_id_selected'];
		}
		if ($cnt == 1)
		{
		   $this->load->model('Attendee_model');
		   $this->load->model('Profile_model');
		   $this->load->model('Photo_filter_model');
		   $this->load->library('session');
		   $roles = $this->Event_model->get_menu_list($roleid,$eventid);
		   $this->data['roles'] = $roles;
		}
		else
		{
		   echo '<script>window.location.href="'.base_url().'forbidden/'.'"</script>'; 
		}
	}
	public function index($id = NULL)
	{	
        if($this->input->post())
        {
        	if(!empty($_POST['photo_filter_image_crop_data']))
        	{
        		if(!empty($event[0]['photo_filter_image']))
	        	{
	        		unlink('./assets/user_files/'.$event[0]['photo_filter_image']);
	        	}
	        	$img=$_POST['photo_filter_image_crop_data'];
	            $filteredData=substr($img, strpos($img, ",")+1); 
	            $unencodedData=base64_decode($filteredData);
	            $images_file = strtotime(date("Y-m-d H:i:s"))."photo_filter_image.png"; 
	            $filepath = "./assets/user_files/".$images_file; 
	            file_put_contents($filepath, $unencodedData); 
	            $photo_data['event_array']['Id']=$id;
	            $photo_data['event_array']['photo_filter_image']=$images_file;
	            $this->Event_model->update_admin_event($photo_data);
	            $this->session->set_flashdata('filter_data',"Photo Filter Images Save SuccessFully...");
	            redirect('Photo_filter_admin/index/'.$id);
	        }
        }
        $this->data['filter'] = $this->Photo_filter_model->getFilters($id);
        $this->data['photos_taken'] = $this->Photo_filter_model->getPhotosTaken($id);
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role; 
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 54);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
		$this->template->write_view('css', 'photo_filter_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'photo_filter_admin/index', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'photo_filter_admin/js', true);
        $this->template->render();
	}
	public function addFilter($id = NULL)
	{
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $event_id = $id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
        $this->data['users_role']=$user_role; 
        $rolename = $user[0]->Role_name;
        $menudata = $this->Event_model->geteventmenu($id, 54);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $event_id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        
        if ($this->input->post())
        {   
            if (!file_exists('./assets/photo_filter/'.$id)) {
            mkdir('./assets/photo_filter/'.$id, 0777, true);
            }
            if(!empty($_FILES['image']['name']))
            {
                $imgname = explode('.', $_FILES['image']['name']);
                $tempname = str_replace(" ","_",$imgname);
                $tempname_imagename = $id.'-photo_filter-'.date('Y-m-d-H-i-s');
                $images_file = $tempname_imagename . "." . $tempname[1];

                $this->upload->initialize(array(
                      "file_name" => $images_file,
                      "upload_path" => "./assets/photo_filter/".$id,
                      "allowed_types" => 'gif|jpg|png|jpeg',
                      "max_size" => '100000'
                ));
                $this->upload->do_upload("image");
                $insert['image'] = $images_file;
            }
            $insert['event_id'] = $id;
            $insert['title'] = $this->input->post('title');
            $this->Photo_filter_model->addFilter($insert);
            redirect(base_url().'photo_filter_admin/index/'.$id.'#filters');
        }
        $this->template->write_view('css', 'admin/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'photo_filter_admin/add_filter', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'survey/js', $this->data, true);
        $this->template->render();
	}
    public function deleteFilter($event_id = NULL,$id = NULL)
    {
        $this->Photo_filter_model->deleteFilter($id,$event_id);
        redirect(base_url().'photo_filter_admin/index/'.$event_id.'#filters');
    }

    public function exportUploadedFiltersPhotos($event_id = NULL)
    {
        $images = $this->Photo_filter_model->getPhotosTaken($event_id);
        $files = array_column_1($images,'image');
        $zipname = 'photo-'.date('d-m-y-H-i-s').'.zip';
        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE);
        foreach ($files as $file) {
          $zip->addFile(FCPATH.'assets/photo_filter_uploads/'.$event_id.'/'.$file,basename($file));
        }
        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zipname);
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);
        unlink($zipname);
    }
}