<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Hub extends CI_Controller
{
	function __construct()
	{
		$this->data['pagetitle'] = 'Hub Events';
      	$this->data['smalltitle'] = 'Events Details';
      	$this->data['breadcrumb'] = 'Events Template';
      	parent::__construct($this->data);
      	$this->template->set_template('front_template');
      	$this->load->model('Event_template_model');
      	$this->load->model('Event_model');
      	$this->load->model('Cms_model');
      	$this->load->library('upload');
      	
      	$org=$this->Event_model->get_organizer_by_acc_name($this->uri->segment(2));
      	$hub_active=$this->Event_model->get_hub_active_by_organizer($org[0]['Id']);
      	if($hub_active!='1')
      	{
      		redirect(base_url().'Forbidden/');
      	}
	}
	public function index($acc_name=NULL)
	{
		$acc['acc_name'] =  $acc_name;
        $this->session->set_userdata($acc);
		$org=$this->Event_model->get_organizer_by_acc_name($acc_name);
		$org_id=$org[0]['Id'];
	   $hubevent=$this->Event_model->get_hub_event_menu_setting_organizerid($org_id);
	   $this->data['hub_event_list']=$hubevent;
	   $cmsmenu = $this->Cms_model->get_hub_cms_page($org_id);
        $this->data['hub_cms_menu'] = $cmsmenu;
	   $hubdesing=$this->Event_model->get_hub_event($org_id);
	   if(count($hubdesing)>0){
	   		$this->data['hubdesing']=$hubdesing;
		   $event_templates[0]['Logo_images']=$hubdesing[0]['hub_logo_images'];
		   $event_templates[0]['attendees_active']=$hubdesing[0]['attendees_active'];
		   $event_templates[0]['public_messages_active']=$hubdesing[0]['public_messages_active'];
		   $event_templates[0]['Img_view']='0';
		   $event_templates[0]['Event_name']=$hubdesing[0]['hub_name'];
		   $event_templates[0]['Subdomain']=$hubevent[0]['Subdomain'];
		   $event_templates[0]['menu_text_color']='#FFFFFF';
		   $event_templates[0]['menu_background_color']='#6b6b6b';
		   $event_templates[0]['menu_hover_background_color']='#b5b5b5';
		   $event_templates[0]['Top_text_color']=$hubdesing[0]['hub_top_bar_text_color'];
		   $event_templates[0]['Top_background_color']=$hubdesing[0]['hub_top_bar_backgroundcolor'];
		   $event_templates[0]['huburl']=base_url().'hub/'.$acc_name;
		   $this->data['event_templates']=$event_templates;
		   $this->template->write_view('css', 'frontend_files/css', $this->data, true);
		   $this->template->write_view('header', 'frontend_files/header', $this->data, true);
		   $this->template->write_view('content', 'events/hub_views', $this->data, true);
		   $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
		   $this->template->write_view('js', 'frontend_files/js', $this->data, true);
		   $this->template->render();
		}
		else
		{
            redirect(base_url().'Pageaccess/');
		}
	}
}
?> 
