<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
class Linkedin_signup extends CI_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Add_Attendee_model');
        $this->load->model('Event_template_model');
    }
    function index()
    {
        echo '<form id="linkedin_connect_form" action="' . base_url() . 'Linkedin_signup/initiate" method="post">';
        echo '<input type="submit" value="Login with LinkedIn" />';
        echo '</form>';
    }
    function initiate($sub=NULL)
    {
        /*$linkedin_config = array(
        'appKey' => '75vd9jtqtfpr2c',
        'appSecret' => '9IocoHLNVgIOgsog',
        'callbackUrl' => base_url().'linkedin_signup/data/'.$sub
        );*/
        $linkedin_config = array(
            'appKey' => '77ueb8xbuq8jl7', //'75vd9jtqtfpr2c',
            'appSecret' => 'gN37KT5tPHHpeSc0', //'9IocoHLNVgIOgsog',
            'callbackUrl' => base_url() . 'linkedin_signup/data/' . $sub
        );
        $this->load->library('linkedin', $linkedin_config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $token = $this->linkedin->retrieveTokenRequest();
        $this->session->set_flashdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
        $this->session->set_flashdata('oauth_request_token', $token['linkedin']['oauth_token']);
        $link = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
        redirect($link);
    }
    function cancel()
    {
        echo 'Linkedin user cancelled login';
    }
    function logout()
    {
        session_unset();
        $_SESSION = array();
        echo "Logout successful";
    }
    function data($sub=NULL)
    {
        /*$linkedin_config = array(
        'appKey' => '75vd9jtqtfpr2c',
        'appSecret' => '9IocoHLNVgIOgsog',
        'callbackUrl' => base_url().'linkedin_signup/data/'.$sub
        );*/
        $linkedin_config = array(
            'appKey' => '77ueb8xbuq8jl7', //'75vd9jtqtfpr2c',
            'appSecret' => 'gN37KT5tPHHpeSc0', //'9IocoHLNVgIOgsog',
            'callbackUrl' => base_url() . 'linkedin_signup/data/' . $sub
        );
        $this->load->library('linkedin', $linkedin_config);
        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $oauth_token = $this->session->flashdata('oauth_request_token');
        $oauth_token_secret = $this->session->flashdata('oauth_request_token_secret');
        $oauth_verifier = $this->input->get('oauth_verifier');
        $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
        if ($response['success'] === TRUE)
        {
            $oauth_expires_in = $response['linkedin']['oauth_expires_in'];
            $oauth_authorization_expires_in = $response['linkedin']['oauth_authorization_expires_in'];
            $response = $this->linkedin->setTokenAccess($response['linkedin']);
            $profile = $this->linkedin->profile('~:(id,first-name,last-name,picture-url,email-address,summary)');
            $profile_connections = $this->linkedin->profile('~/connections:(id,firstName,lastName,headline,email-address,summary,educations,last-modified-timestamp,interests,skills,date-of-birth,positions,picture-url,languages,projects)');
            $user = json_decode($profile['linkedin']);
            $user_array = array(
                'linkedin_id' => $user->id,
                'second_name' => $user->lastName,
                'profile_picture' => $user->pictureUrl,
                'first_name' => $user->firstName,
                'email-address' => $user->emailAddress,
                'summary' => $user->summary
            );
            // $ua = $this->fetch('GET', '/v1/people/~:(firstName,lastName,headline,email-address,summary,educations,last-modified-timestamp,interests,skills,date-of-birth,positions,picture-url,languages,projects)');
            /*print '<pre>';
            print_r($user_array);
            print '</pre>';*/
            $event_templates = $this->Event_template_model->get_event_template_by_id_list($sub);
            $eid = $event_templates[0]['Id'];
            $acc = $this->Event_template_model->getAccname($eid);
            $name = $user_array['first_name'] . " " . $user_array['second_name'];
            $logo = $user_array['first_name'] . "_linkdin_" . strtotime(date("Y-m-d H:i:s")) . ".jpeg";
            copy($user_array['profile_picture'], "./assets/user_files/" . $logo);
            $logindetails['current_user'] = $this->Add_Attendee_model->check_exists_linkdin_users($user_array['email-address'], $name, $logo, $sub, $acc[0]['acc_name']);
            // echo "<pre>";print_r($logindetails);die;
            $logindetails['isLoggedIn'] = true;
            if ($logindetails['current_user'][0]->acc_name == "")
            {
                $logindetails['current_user'][0]->acc_name = $acc[0]['acc_name'];
                $logindetails['acc_name'] = $acc[0]['acc_name'];
                $acc_name = $acc[0]['acc_name'];
            }
            else
            {
                $logindetails['acc_name'] = $logindetails['current_user'][0]->acc_name;
                $acc_name = $logindetails['current_user'][0]->acc_name;
            }
            $this->session->set_userdata($logindetails);
            $user = $this->session->userdata('current_user');
            $company = $this->linkedin->company('1337:(id,name,ticker,description,logo-url,locations:(address,is-headquarters))');
            redirect(base_url() . 'App/' . $acc_name . '/' . $sub);
            // echo 'Company data:';
            // print '<pre>';
            // print_r($company);
            // print '</pre>';
            // echo '<br /><br />';
            // echo 'Logout';
            // echo '<form id="linkedin_connect_form" action="http://192.168.1.123/EventApp/login/logout" method="post">';
            // echo '<input type="submit" value="Logout from LinkedIn" />';
            // echo '</form>';
        }
        else
        {
            echo "Request token retrieval failed:<br /><br />RESPONSE:<br /><br />" . print_r($response, TRUE);
        }
    }
}