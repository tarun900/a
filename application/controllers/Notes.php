<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Notes extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Notes';
        $this->data['smalltitle'] = 'Notes';
        $this->data['breadcrumb'] = 'Notes';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->model('Agenda_model');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->library('formloader1');
        $this->load->model('Notes_admin_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Profile_model');
        $this->load->model('Message_model');
        $user = $this->session->userdata('current_user');
        $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
        $eventid = $event_val[0]['Id'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        if (!in_array('6', $menu_list))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $eventname = $this->Event_model->get_all_event_name();
        if (in_array($this->uri->segment(3) , $eventname))
        {
            if ($user != '')
            {
                $logged_in_user_id = $user[0]->User_id;
                $parameters = $this->uri->uri_to_assoc(1);
                $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                $cnt = $this->Agenda_model->check_access($logged_in_user_id, $event_templates[0]['Id']);
                if ($cnt == 1)
                {
                    $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                    $this->data['notes_list'] = $notes_list;
                }
                else
                {
                    $event_type = $event_templates[0]['Event_type'];
                    if ($event_type == 3)
                    {
                        $this->session->unset_userdata('current_user');
                        $this->session->unset_userdata('invalid_cred');
                        $this->session->sess_destroy();
                    }
                    else
                    {
                        $parameters = $this->uri->uri_to_assoc(1);
                        $Subdomain = $this->uri->segment(3);
                        $acc_name = $this->uri->segment(2);
                        echo '<script>window.location.href="' . base_url() . 'Unauthenticate/' . $acc_name . '/' . $Subdomain . '"</script>';
                    }
                }
            }
        }
        else
        {
            $Subdomain = $this->uri->segment(3);
            $flag = 1;
            echo '<script>window.location.href="' . base_url() . 'Pageaccess/' . $Subdomain . '/' . $flag . '"</script>';
        }
    }
    public function index($acc_name = NULL, $Subdomain = NULL, $intFormId = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $user = $this->session->userdata('current_user');
        if (!empty($user[0]->Id))
        {
            $req_mod = $this->router->fetch_class();
            $menu_id = $this->Event_model->get_menu_id($req_mod);
            $current_date = date('Y/m/d');
            $this->Event_model->add_view_hit($user[0]->Id, $current_date, $menu_id, $event_templates[0]['Id']);
        }
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
        $this->data['fb_login_data'] = $fb_login_data;
        $fundraisingenbled = $this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
        $this->data['linkdin_login'] = $fundraisingenbled[0]['linkedin_login_enabled'];
        $res1 = $this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
        $this->data['sign_form_data'] = $res1;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        for ($i = 0; $i < count($menu_list); $i++)
        {
            if ('Notes' == $menu_list[$i]['pagetitle'])
            {
                $mid = $menu_list[$i]['id'];
            }
        }
        $this->data['menu_list'] = $menu_list;
        $this->data['menu_id'] = $mid;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['acc_name'] = $acc_name;
        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;
        $eid = $event_templates[0]['Id'];
        $res = $this->Agenda_model->getforms($eid, $mid);
        $this->data['form_data'] = $res;
        $this->data['Subdomain'] = $Subdomain;
        $array_temp_past = $this->input->post();
        if (!empty($array_temp_past))
        {
            $aj = json_encode($this->input->post());
            $formdata = array(
                'f_id' => $intFormId,
                'user_id' => $user[0]->Id,
                'm_id' => $mid,
                'event_id' => $eid,
                'json_submit_data' => $aj
            );
            $this->Agenda_model->formsinsert($formdata);
            echo '<script>window.location.href="' . base_url() . 'Notes/' . $acc_name . '/' . $Subdomain . '"</script>';
            exit;
        }
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        if ($event_templates[0]['Event_type'] == '1')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Notes/index', $this->data, true);
                $this->template->write_view('footer', 'Notes/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '2')
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/index', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Notes/index', $this->data, true);
                $this->template->write_view('footer', 'Notes/footer', $this->data, true);
            }
        }
        elseif ($event_templates[0]['Event_type'] == '3')
        {
            $this->session->unset_userdata('acc_name');
            $acc['acc_name'] = $acc_name;
            $this->session->set_userdata($acc);
            $isforcelogin = $this->Event_model->get_force_login_enabled_by_menu_id('6', $event_templates[0]['Id']);
            if ($isforcelogin['is_force_login'] == '1' && empty($user))
            {
                $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Notes/index', $this->data, true);
                $this->template->write_view('footer', 'Notes/footer', $this->data, true);
            }
        }
        else
        {
            if (empty($user))
            {
                $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
            }
            else
            {
                $this->template->write_view('content', 'Notes/index', $this->data, true);
                $this->template->write_view('footer', 'Notes/footer', $this->data, true);
            }
        }
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->render();
    }
    public function add($acc_name=NULL, $Subdomain = NULL)

    {
        $user = $this->session->userdata('current_user');
        if ($user[0]->Role_name == 'Client')
        {
            $logged_in_user_id = $user[0]->Id;
        }
        else
        {
            $logged_in_user_id = $user[0]->Organisor_id;
        }
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $this->data['Subdomain'] = $Subdomain;
        if ($this->input->post('Heading'))
        {
            $data = array(
                'User_id' => $user[0]->Id,
                'Organisor_id' => $logged_in_user_id,
                'Event_id' => $event_templates[0]['Id'],
                'Heading' => $this->input->post('Heading') ,
                'Description' => $this->input->post('Description') ,
                'Created_at' => $this->input->post('Created_at')
            );
            $this->Event_template_model->add_notes($data);
            redirect('Notes/' . $acc_name . '/' . $Subdomain);
            die;
        }
        if (empty($user))
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            $this->load->view('Notes/add', $this->data);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'Notes/js', $this->data, true);
            $this->template->render();
        }
    }
    public function edit($acc_name=NULL, $Subdomain = NULL, $id = NULL)

    {
        if ($this->input->post('curr_url'))
        {
            $this->data['close_flag'] = 1;
        }
        else
        {
            $this->data['close_flag'] = 0;
        }
        $notes_id = $id;
        $user = $this->session->userdata('current_user');
        if ($user[0]->Role_name == 'Client')
        {
            $logged_in_user_id = $user[0]->Id;
        }
        else
        {
            $logged_in_user_id = $user[0]->Organisor_id;
        }
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $notes_by_id = $this->Event_template_model->get_notes_by_id($Subdomain, $id);
        $this->data['notes_by_id'] = $notes_by_id;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $this->data['Subdomain'] = $Subdomain;
        $this->data['id'] = $id;
        $this->data['module'] = $this->data['pagetitle'];
        if ($this->input->post('Heading'))
        {
            $data = array(
                'User_id' => $user[0]->Id,
                'Organisor_id' => $logged_in_user_id,
                'Event_id' => $event_templates[0]['Id'],
                'Heading' => $this->input->post('Heading') ,
                'Description' => $this->input->post('Description')
            );
            $this->Event_template_model->update_notes($data, $id);
            $this->data['status'] = "Updated Successfully";
            redirect("Notes/" . $Subdomain);
        }
        if (empty($user))
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->load->view('Notes/edit', $this->data);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
    }
    public function View($Subdomain = NULL, $id = NULL)

    {
        $notes_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $notificationsetting = $this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting'] = $notificationsetting;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notes_by_id = $this->Event_template_model->get_notes_by_id($Subdomain, $id);
        $this->data['notes_by_id'] = $notes_by_id;
        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $this->data['Subdomain'] = $Subdomain;
        if (empty($user))
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('content', 'registration/index', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
        else
        {
            $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->load->view('Notes/View', $this->data);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            $this->template->render();
        }
    }
    public function delete_notes($id = NULL, $Subdomain = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;
        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;
        $notes_by_id = $this->Event_template_model->get_notes_by_id($Subdomain, $id);
        $this->data['notes_by_id'] = $notes_by_id;
        $notes = $this->Event_template_model->delete_notes($Subdomain, $id);
        redirect("Notes/" . $Subdomain);
    }
    public function submit($acc_name=NULL, $Subdomain = NULL, $id = NULL)

    {
        $user = $this->session->userdata('current_user');
        if ($user[0]->Role_name == 'Client')
        {
            $logged_in_user_id = $user[0]->Id;
        }
        else
        {
            $logged_in_user_id = $user[0]->Organisor_id;
        }
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $data = array(
            'User_id' => $user[0]->Id,
            'Organisor_id' => $logged_in_user_id,
            'Event_id' => $event_templates[0]['Id'],
            'Heading' => $this->input->post('Heading') ,
            'Description' => $this->input->post('Description')
        );
        $this->Event_template_model->update_notes($data, $id);
        $this->data['status'] = "Updated Successfully";
    }
    public function notes_close($Subdomain = null, $id = NULL)

    {
        $flag = $this->input->post('flag');
        if ($flag == 0)
        {
            echo "sucess###1";
            exit;
        }
        else
        {
            echo "error###1";
            exit;
        }
    }
}