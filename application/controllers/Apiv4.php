<?php 
/*error_reporting(E_ALL);
ini_set('display_errors','1');*/
include_once (dirname(__FILE__) . "/Apiv3.php");
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Apiv4 extends Apiv3 {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Speaker_model');
        $this->load->model('Agenda_model');
        $this->load->model('Sponsors_model');
        $this->ncrToken = file_get_contents('./assets/ncrToken/ncr.txt');
    }
    public function index( $offset = 0 )
    {   
        // $this->load->library('../controllers/apiv3');
        die("Direct Access Denied..");
    }
    public function VentureFestaddExhi()
    {   
        extract($this->input->post());
        $event_id = '1802';
        $org_id   = '383336';
        
        /*if($username != 'ppma' || $password != '^D6X/8wNn-`d:?g.' || empty($username)  || empty($password))
        {
            $data = array(
                    'success' => false,
                    'message' => 'API access is not allowed.'
                );
            echo json_encode($data);exit; 
        }*/


        if(!empty($Email) && !empty($Company_name) && !empty($Firstname) && !empty($Lastname))
        {
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $user['Firstname'] = $Firstname;
            $user['Lastname'] = $Lastname;
            $user['Company_name'] = $Company_name;
            $user['Email'] = $Email;
            $user['Password'] = $Password ? md5($Password) : md5('123456');
            $user['Organisor_id'] = $org_id;
            $user['Created_date'] = date('Y-m-d H:i:s');
            $user['Active'] = '1';
            $user['updated_date'] = date('Y-m-d H:i:s');
            $user['is_from_api'] = '1';
            $user['Country'] = $this->getCountryIdFromName($Country);
            $user_id = $this->addExhiUser($user,$event_id);

            $ex_data['Event_id'] = $event_id;
            $ex_data['Heading'] = $Company_name;
            $ex_data['Organisor_id'] = $org_id;
            if(!empty($Type))
            {
                $ex_data['et_id'] = $this->getExhiTypeId($event_id,$Type);
            }
            $ex_data['stand_number'] = $stand_number;
            $ex_data['Short_desc'] = $Short_desc;
            $ex_data['Description'] = htmlentities($Description);
            $ex_data['website_url'] = $website_url;
            $ex_data['facebook_url'] = $facebook_url;
            $ex_data['twitter_url'] = $twitter_url;
            $ex_data['linkedin_url'] = $linkedin_url;
            $ex_data['instagram_url'] = $instagram_url;
            $ex_data['youtube_url'] = $youtube_url;
            $ex_data['country_id'] = $this->getCountryIdFromName($Country);
            $this->load->library('upload');
            if (!empty($_FILES['company_logo']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['company_logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("company_logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['company_logo'] = json_encode(array($images_file));
            }

            if (!empty($_FILES['banner_image']['name'][0]))
            {
                 $tempname = explode('.', $_FILES['banner_image']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("banner_image"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $ex_data['Images'] = json_encode(array($images_file));
            }
            
            $ex_data['user_id'] = $user_id;
            $this->add_exhibitor($ex_data);
            updateModuleDate($event_id,'exhibitor');
            $data = array(
                    'success' => true,
                    'message' => 'Exhibitor Updated Successfully'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                    );
        }
        updateModuleDate($event_id,'exhibitor');   
        echo json_encode($data);exit;
    }
    public function VentureFestaddSpeaker($id)
    {   
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
            
        if(!empty($Firstname) && !empty($Lastname))
        {

            if(empty($Email))
            {
                $Email = $Firstname.'-'.$event_id.'-'.$Organisor_id.'@venturiapps.com';
            }
            else
            {
                if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Email should be valid'
                    );
                    echo json_encode($data);exit;   
                }
            }
            
            $user = $this->getUserByEvent($Email);

            $speaker_data['Salutation'] = $Salutation;
            $speaker_data['Firstname'] = $Firstname;
            $speaker_data['Lastname'] = $Lastname;
            $speaker_data['Email'] = $Email;
            $speaker_data['Password'] =$Password ? md5($Password) : '123456';
            $speaker_data['Title'] = $Title;
            $speaker_data['Company_name'] = $Company_name;
            $speaker_data['Mobile'] = $Mobile;
            $speaker_data['Speaker_desc'] = $Speaker_desc;
                
            $user_link['Website_url'] = $Website_url;
            $user_link['Facebook_url'] = $Facebook_url;
            $user_link['Twitter_url'] = $Twitter_url;
            $user_link['Linkedin_url'] = $Linkedin_url;
            
            if (!empty($_FILES['Logo']['name'][0]))
            {
                $this->load->library('upload');
                 $tempname = explode('.', $_FILES['Logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("Logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $speaker_data['Logo'] = $images_file;
            } 

            if(empty($user))
            {
                $speaker_data['Organisor_id'] = $Organisor_id;
                $speaker_data['Created_date'] = date('Y-m-d H:i:s');
                $speaker_data['Active'] = '1';
                $speaker_data['is_from_api'] = '1';
                $speaker_id = $this->Speaker_model->add_csv_speaker($event_id, $speaker_data, $user_link);
            }
            else
            {
                $speaker_data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Id',$user['Id']);
                $this->db->update('user',$speaker_data);
                $speaker_id =  $user['Id'];
            }
            $this->setUserWithEvent($event_id,$speaker_id,'7',$Organisor_id);
            if(!empty($Type))
            {
                $this->Speaker_model->assign_spear_type($id,$Type,$speaker_id);
            }
            $data = array(
                'success' => true,
                'message' => "Speaker Updated Successfully"
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Paramenters"
            );
        }
        updateModuleDate($event_id,'speaker');   
        echo json_encode($data);exit;
    }
    public function getUserByEvent($email)
    {
        $this->db->where('Email',$email);
        $user = $this->db->get('user')->row_array();
        return $user;
    }
    public function setUserWithEvent($event_id,$user_id,$role_id,$org_id)
    {   
        $where['Event_id'] = $event_id;
        $where['User_id'] = $user_id;
        $this->db->where($where);
        $res = $this->db->get('relation_event_user')->row_array();
        if(empty($res))
        {
            $where['Organisor_id'] = $org_id;
            $this->db->insert('relation_event_user',$where);
        }
    }
    public function VentureFestaddSession()
    {
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
        $cid = '2112';
        if(!empty($start_date) && !empty($start_time) && !empty($end_date) && !empty($end_time) && !empty($heading) && !empty($type))
        {
            if(!validate_date($start_date))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Start date is Invalid'
                );
            }

            if(!validate_time($start_time))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Start time is Invalid'
                );
            }

            if(!validate_date($end_date))
            {
                $data = array(
                    'success' => false,
                    'message' => 'End date is Invalid'
                );
            }

            if(!validate_time($end_time))
            {
                $data = array(
                    'success' => false,
                    'message' => 'End time is Invalid'
                );
            }

            if(!empty($data))
            {
                echo json_encode($data);exit;
            }


            $data['agenda_array']['Organisor_id'] = $Organisor_id;
            $data['agenda_array']['Event_id'] = $event_id;
            $data['agenda_array']['Start_date'] = $start_date;
            $data['agenda_array']['Start_time'] = $start_time;
            $data['agenda_array']['End_date'] = $end_date;
            $data['agenda_array']['End_time'] = $end_time;
            if (!empty($checking_datetime))
            {
                $data['agenda_array']['checking_datetime'] = $checking_datetime;
            }
            $data['agenda_array']['Heading'] = $heading;
            $check = $this->Agenda_model->check_exists_type_name($event_id,$type);

            if (count($check))
            {
                $type_id = $check[0]['type_id'];
            }
            else
            {
                $type_id = $this->Agenda_model->add_session_types($event_id, $type);
            }
            $data['agenda_array']['Types'] = $type_id;
            if (!empty($speaker_email))
            {
                $s_email = explode(",", $speaker_email);
                $s_ids = array();
                foreach($s_email as $key1 => $value1)
                {
                    $s_ids[$key1] = $this->Agenda_model->get_speaker_id_by_email($value1, $event_id);
                }
                $data['agenda_array']['Speaker_id'] = implode(",", $s_ids);
            }
            if (!empty($map_code))
            {
                $data['agenda_array']['Address_map'] = $this->Agenda_model->get_map_id_by_map_code($map_code, $event_id);
            }

            $data['agenda_array']['Agenda_status'] = '1';
            $data['agenda_array']['description'] = $description;
            $data['agenda_array']['Maximum_People'] = $maximum_people;
            $data['agenda_array']['show_places_remaining'] = $show_places_remaining?:0;
            $data['agenda_array']['show_checking_in'] = $show_checking_in?:0;
            $data['agenda_array']['show_rating'] = $show_rating?:0;
            $data['agenda_array']['custom_speaker_name'] = $custom_speaker_name;
            $data['agenda_array']['custom_location'] = $custom_location;
            $data['agenda_array']['allow_clashing'] = $allow_clashing?:0;
            $data['agenda_array']['allow_comments'] = $allow_comments?:0;

            $this->db->where('Heading',$heading);
            $this->db->where('Event_id',$event_id);
            $res = $this->db->get('agenda')->row_array();
            if(empty($res))
            {
                $this->Agenda_model->add_agenda($data, $event_id, $cid);
            }
            else
            {   
                $data['agenda_array']['Id'] = $res['Id'];
                $this->Agenda_model->update_agenda($data,$cid,$event_id);
            }
            $data = array(
                    'success' => true,
                    'message' => 'Agenda Updated Successfully'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                );
        }
        updateModuleDate($event_id,'agenda');   
        echo json_encode($data);
    }

    public function VentureFestaddSponsor()
    {
        extract($this->input->post());
        $event_id = '1802';
        $Organisor_id = '383336';
            
        if (!empty($Sponsors_name))
        {
            $sponsors_data['sponsors_array']['Organisor_id'] = $Organisor_id;
            $sponsors_data['sponsors_array']['Event_id'] = $event_id;
            $sponsors_data['sponsors_array']['Sponsors_name'] = $Sponsors_name;
            if (!empty($Sponsors_type))
            {
                $tid = $this->Sponsors_model->get_sponsors_type_id_by_type_code($Sponsors_type, $event_id);
                $sponsors_data['sponsors_array']['st_id'] = $tid;
            }
            $sponsors_data['sponsors_array']['Company_name'] = $Company_name;
            $sponsors_data['sponsors_array']['Description'] = $Description;
            $sponsors_data['sponsors_array']['website_url'] = $website_url;
            $sponsors_data['sponsors_array']['facebook_url'] = $facebook_url;
            $sponsors_data['sponsors_array']['twitter_url'] = $twitter_url;
            $sponsors_data['sponsors_array']['linkedin_url'] = $linkedin_url;
            $sponsors_data['sponsors_array']['instagram_url'] = $instagram_url;
            $sponsors_data['sponsors_array']['youtube_url'] = $youtube_url;


            if (!empty($_FILES['Logo']['name'][0]))
            {
                $this->load->library('upload');
                 $tempname = explode('.', $_FILES['Logo']['name']);
                 $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                 $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                 $images_file = $tempname_imagename . "." . $tempname[1];
                 $this->upload->initialize(array(
                         "file_name" => $images_file,
                         "upload_path" => "./assets/user_files",
                         "allowed_types" => '*',
                         "max_size" => '100000',
                         "max_width" => '5000',
                         "max_height" => '5000'
                 ));

                 if (!$this->upload->do_upload("Logo"))
                 {
                      $error = array('error' => $this->upload->display_errors());
                 }
                 $sponsors_data['sponsors_array']['company_logo'] = $images_file;
            }

            $this->db->where('Sponsors_name',$Sponsors_name);
            $this->db->where('Event_id',$event_id);
            $res = $this->db->get('sponsors')->row_array();
            if(empty($res))
            {
                $sponsors_id = $this->Sponsors_model->add_user_as_sponsors($sponsors_data);
            }
            else
            {   
                $sponsors_data['sponsors_array']['Id'] = $res['Id'];
                $sponsors_id = $this->Sponsors_model->update_sponsors($sponsors_data);
            }
            $data = array(
                'success' => true,
                'message' => 'Sponsor Updated Successfully'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        updateModuleDate($event_id,'sponsor');   
        echo json_encode($data);
    }

    public function resetNCRToken()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/auth/token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials&scope=c85ea381-8202-4ca1-84b2-ce58e2f5ee74&client_id=E20F3E47-7D82-4C65-B8BB-6DA340FFED1B&client_secret=AITLlearning2019",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 4b813944-8ae5-4485-87c1-de5b19e4264a"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            show_errors();
            $response = json_decode($response,true);
            $file = fopen("./assets/ncrToken/ncr.txt","w");
            echo fwrite($file,$response['access_token']);
            fclose($file);
        }
    }

    public function NCRSessions()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/Sessions?\$expand=TimeSlot,Room,SessionType,Track,PropertyValues/PropertyMetadata",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            $response = filterArray($response,'Enabled','1');
            if($_GET['t'] == '1')
            {	
                j($response[0]);
                j(json_encode($response[0]));
            	echo json_encode($response);exit;
            }
            foreach ($response as $key => $value)
            {   
                if(!empty($value['Code']))
                {   
                    
                    $tmp_desc[] = substr($value['Room']['Name'], 0, strpos($value['Room']['Name'], "("))."<br>";

                    // $tmp_desc[] = $value['Description']."<br>";
                    
                    //Guide
                    $key = array_search('27891', array_column_1($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = $value['PropertyValues'][$key]['Value']."<br>";
                    
                    $tmp_desc[] = "<ul>";
                    //Take-Away 1
                    $key = array_search('27892', array_column_1($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";
                    
                    //Take-Away 2
                    $key = array_search('27893', array_column_1($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";

                    //Take-Away 3
                    $key = array_search('27894', array_column_1($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";

                    $tmp_desc[] = "</ul>";


                    $tmp_desc[] = "<p>Hot Topics</p>";
                    //hot topics
                    $tmp_desc[] = "<ul>";
                    $key = array_search('27890', array_column_1($value['PropertyValues'], 'PropertyMetadataId'));
                    if($value['PropertyValues'][$key]['Value'] != 'Other')
                    $tmp_desc[] = "<li>".$value['PropertyValues'][$key]['Value']."</li>";
                    $tmp_desc[] = "</ul>";
                
                    $agenda['tmp_id'] = 'learn2018-'.$value['Id'];
                    $agenda['StartDate'] = date('Y-m-d',strtotime(date($value['TimeSlot']['StartTime']['EventTime'])));
                    $agenda['EndDate'] = date('Y-m-d',strtotime(date($value['TimeSlot']['EndTime']['EventTime'])));
                    $agenda['StartTime'] = date('H:i:s',strtotime(date($value['TimeSlot']['StartTime']['EventTime'])));
                    $agenda['EndTime'] = date('H:i:s',strtotime(date($value['TimeSlot']['EndTime']['EventTime'])));
                    $agenda['Name'] = $value['Code'].' : '.$value['Title'];
                    $agenda['Type'] = $value['Track']['Title'];
                    $agenda['Location'] = substr($value['Room']['Name'], 0, strpos($value['Room']['Name'], "("));
                    $agenda['Speakers'] = implode(',',$value['SpeakerOrder']);
                    $agenda['Description'] = implode('',$tmp_desc);
                    unset($tmp_desc);
                    
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://ngapi.hubb.me/api/V1/1950/Attachments?sessionId=".$value['Id'],
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "GET",
                      CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer ".$this->ncrToken,
                        "Cache-Control: no-cache",
                        "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
                      ),
                    ));

                    $doc = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                      echo "cURL Error #:" . $err;
                    } else
                    {   
                        $doc = json_decode($doc,true);
                        $allowed_ext = array('.ppt','.pptx','.doc','.docx','.pdf');
                        $doc = filterArray($doc,'Extension',$allowed_ext);
                        foreach ($doc as $dkey => $dvalue)
                        {
                            $insert_doc[$dkey]['Title'] = $dvalue['Title'];
                            $insert_doc[$dkey]['Link'] = $dvalue['Link'];
                            $insert_doc[$dkey]['SessionId'] = 'learn2018-'.$dvalue['SessionId'];
                            $insert_doc[$dkey]['tmp_id'] = $dvalue['Id'];
                            $insert_doc[$dkey]['type'] = $dvalue['Extension'];
                        }
                        if(!empty($insert_doc))
                        {
                            $this->db->insert_batch('Learn_documents',$insert_doc);
                            unset($insert_doc);
                        }
                    }

                    $fin[] = $agenda;
                    $tmp_agenda = $this->db->where('tmp_id','learn2018-'.$value['Id'])->get('Learn_Agenda')->row_array();
                    if(empty($tmp_agenda))
                    {
                        $this->db->insert('Learn_Agenda',$agenda);
                    }
                    else
                    {
                        $this->db->where($tmp_agenda);
                        $this->db->update('Learn_Agenda',$agenda);
                    }
                }
            }
            j('Data Imported');
        }
    }

    public function NCRDocuments()
    {   
        $event_id = '1756';
        $docs = $this->db->get('Learn_documents')->result_array();
        foreach ($docs as $key => $value)
        {   
            $file_name = clean($value['Title']).time().$value['Type'];
            $path = './assets/user_documents/'.$file_name;
            copy($value['Link'],$path);
            $insert['Event_id'] = $event_id;
            $insert['title'] = $value['Title'];
            $insert['doc_type'] = '0';
            $insert['parent'] = '0';
            $res = $this->db->where($insert)->get('documents')->row_array();
            if(empty($res))
            {
                $this->db->insert('documents',$insert);
                $insert_id = $this->db->insert_id();
                $insert_doc['document_id'] = $insert_id;
                $insert_doc['document_file'] = $file_name;
                $this->db->insert('document_files',$insert_doc);
                
                $where['agenda_code'] = $value['SessionId'];
                $where['Event_id'] = $event_id;
                $update['document_id'] = $insert_id;
                $this->db->where($where);
                $this->db->update('agenda',$update);
            }
            else
            {   
                $where['agenda_code'] = $value['SessionId'];
                $where['Event_id'] = $event_id;
                $update['document_id'] = $res['id'];
                $this->db->where($where);
                $this->db->update('agenda',$update);
            }
            $this->db->where($value)->delete('Learn_documents');
        }
        j('documents updated');

    }

    public function NCRAgenda_add()
    {   

        $org_id=33782;
        $Agenda_id=1997;
        $eid=1756;
        $temp_session=$this->db->select('*')->from('Learn_Agenda')->limit(50)->get()->result_array();

        foreach ($temp_session as $key => $value) 
        {   
            if(!empty($value['Speakers']))
            {
                $speaker=array();
                $speaker_name = array();
                $temp_speaker = explode(',',$value['Speakers']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->select('Id,CONCAT_WS(" ",Firstname,Lastname) as Name,Company_name',false)->get_where('user',array('knect_api_user_id'=>'learn2018-'.$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                        $tmp_name[] = $sidarr['Name'];
                        $tmp_name[] = $sidarr['Company_name'];
                        $spr_name = implode(', ',$tmp_name);
                        array_push($speaker_name,$spr_name);
                        unset($tmp_name);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=$value['StartDate'];
            $session_data['Start_time']=$value['StartTime'];
            $session_data['End_date']=$value['EndDate'];
            $session_data['End_time']=$value['EndTime'];
            $session_data['Heading']=trim($value['Name']);
            $session_data['description']=$value['Description'];
            $session_data['custom_location']=$value['Location'];
            $session_data['custom_speaker_name']=implode(',',$speaker_name);
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['Type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
            }
            else
            {
                $type_data['type_name']=trim($value['Type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['tmp_id']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['tmp_id']);
                $session_data['Agenda_status']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Learn_Agenda');
        }
        $total_exhi = $this->db->select()->from('Learn_Agenda')->get()->num_rows();
        updateModuleDate($eid,'agenda');
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function NCRSessionsTypes()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/SessionTypes",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            if($_GET['t'] == '1')
            {
                echo $response;exit;
            }
            else
            {
                $response = json_decode($response,true);
                foreach ($response as $key => $value)
                {
                    $insert['type_name'] = $value['Name'];
                    $insert['event_id'] = '1756';
                    $res = $this->db->where($insert)->get('session_types')->row_array();
                    if(!$res)
                    {
                        $this->db->insert('session_types',$insert);
                    }
                }
            }
            j('Agenda session added Successfully');
        }
    }
    public function NCRSpeakerSessions()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/SpeakerSessions",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            j($response);
            foreach ($response as $key => $value)
            {
                j($value);
            }
        }
    }

    public function NCRUsers()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://ngapi.hubb.me/api/v1/1950/Users",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->ncrToken,
            "Cache-Control: no-cache",
            "Postman-Token: c870af31-571d-443a-9808-1381b2b83783"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else
        {   
            $response = json_decode($response,true);
            if($_GET['t'] == '1')
            {
                j($response);
            }
            foreach ($response as $key => $value)
            {   
                if(in_array('Speaker', $value['Roles']))
                {   
                    $insert[$key]['tmp_id'] = 'learn2018-'.$value['Id'];
                    $insert[$key]['FirstName'] = $value['FirstName'];
                    $insert[$key]['LastName'] = $value['LastName'];
                    $insert[$key]['Email'] = $value['EmailAddress'].'learn2018-'.$value['Id'];
                    $insert[$key]['JobTitle'] = $value['Company'];
                    $insert[$key]['Company'] = '';
                    $insert[$key]['Biography'] = $value['Biography'];
                    $insert[$key]['Image'] = $value['PhotoLink'];
                }
            }
            $this->db->insert_batch('Learn_Speakers',$insert);
            j('Speaker data updated');
        }
    }
    public function NCRUsersAdd()
    {
        $org_id = 33782;
        $eid = 1756;
        $temp_speakers=$this->db->limit(100)->get('Learn_Speakers')->result_array();

        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['tmp_id']),'Email' => $value['Email']))->row_array();
            $user_data['Firstname']=$value['FirstName'];
            $user_data['Lastname']=$value['LastName'];
            $user_data['Email'] =  $value['Email'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['Biography'];
            $user_data['Title']=$value['JobTitle'];
            if(!empty($value['Image']))
            {   
                $logoname="learn2018_spr_logo".uniqid().'.png';
                copy($value['Image'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['tmp_id']);
            $user_data['is_from_api'] = '1';
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id,'Role_id' => '7'))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id',$value['tmp_id'])->delete('Learn_Speakers');
        }
        $total_exhi = $this->db->select()->from('Learn_Speakers')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        updateModuleDate($eid,'speaker');
        die;
    }
    public function assign_arkansas_agenda()
    {    
        // j('hi');
        //error_reporting(E_ALL);
        $this->db->select('u.Id,GROUP_CONCAT(a.Id) as agenda_id,uct.email');
        $this->db->from('user u');
        $this->db->join('temp_user_agenda_1 uct','uct.user_id = u.Id');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->join('agenda a','uct.session_code = a.agenda_code');
        $this->db->where('reu.Role_id','4');
        $this->db->where('reu.Event_id','1856');
        // $this->db->where('uct.user_id','409870');
        $this->db->group_by('u.Id');
        $this->db->limit(100);
        $res = $this->db->get()->result_array();
        foreach ($res as $key => $value)
        {
            $tmp_user_agenda = $this->db->where('user_id',$value['Id'])->get('users_agenda')->row_array();
            if($tmp_user_agenda)
            {
                $tmp_agenda = explode(',',$tmp_user_agenda['agenda_id']);
                $new_agenda  = explode(',',$value['agenda_id']);
                $fin_agenda = array_merge($tmp_agenda,$new_agenda);
                $this->db->where($tmp_user_agenda);
                $update['agenda_id'] = implode(',',array_unique($fin_agenda));
                $this->db->update('users_agenda',$update);
            }   
            else
            {
                $insert['user_id'] = $value['Id'];
                $insert['agenda_id'] = $value['agenda_id'];
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['updated_date'] = date('Y-m-d H:i:s');
                $this->db->insert('users_agenda',$insert);
            }
            $this->db->where('email',$value['email']);
            $this->db->delete('temp_user_agenda_1');
        }
        j('data added successfully');
    }
    public function deeplink()
    {   
        extract($_GET);
        $deeplink = $h."://m.allintheloop.com?node_main=".$node_main."&node_sub=".$node_sub."&nevent=".$nevent;
        ?>
        <script type="text/javascript">
            (function() {
              var app = {
                launchApp: function() {
                  window.location.replace("<?php echo $deeplink; ?>");
                  this.timer = setTimeout(this.openWebApp, 1000);

                },

                openWebApp: function() {
                  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                  if (/android/i.test(userAgent)) {
                        window.location.replace("https://play.google.com/store/apps/details?id=com.allintheloop&hl=en_US");
                    }

                    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                        window.location.replace("https://itunes.apple.com/app/id1200784294");
                    }
                }
              };
              app.launchApp();
            })();
        </script>
        <?php
    }
    public function learn2018SessionSpeaker()
    {   
        $this->db->where('Event_id','1756');
        $this->db->where('Speaker_id IS NOT NULL');
        $res = $this->db->get('agenda')->result_array();
        $user_id =  array_unique(explode(',',implode(',',array_column_1($res,'Speaker_id'))));

        #123
        $tmp_users = $this->db->where_in('Id',$user_id)->get('user')->result_array();
        foreach ($tmp_users as $key => $value) {
            $users[$value['Id']] = $value;
        }

        foreach ($res as $key => $value)
        {   
            $speaker_id = explode(',',$value['Speaker_id']);
            foreach ($speaker_id as $key1 => $value1)
            {
                $tmp_name[] = $users[$value1]['Firstname'];
                $tmp_name[] = $users[$value1]['Lastname'];
                $spr_name[] = implode(' ',$tmp_name).' - '.$users[$value1]['Title'];
                unset($tmp_name);
            }
            $update['custom_speaker_name'] = implode(', ',$spr_name);
            $this->db->where('Id',$value['Id']);
            $this->db->update('agenda',$update);
            unset($spr_name);
        }
        j('Custom Spekaer name updated');
    }
    public function clarionAttendeeTmp()
    {   
        $url1 = "https://www.eventsforce.net/appointmentgroup/api/v2/events/225/attendees.json";
        $url2 = "https://www.eventsforce.net/appointmentgroup/api/v2/events/213/attendees.json";
        $header = array("Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ==");
        $data[] = json_decode(j_curl($url1,$header),true)['data']; 
        $data[] = json_decode(j_curl($url2,$header),true)['data']; 
        $data = array_merge($data[0],$data[1]);
        
        $this->db->select('u.*');
        $this->db->join('relation_event_user reu','reu.User_id = u.Id');
        $this->db->where('reu.Event_id',1825);
        $this->db->where('reu.Role_id',4);
        $res =  $this->db->get('user u')->result_array();
        $uniqid = array_column_1($res,'knect_api_user_id');

        foreach($data as $key => $value)
        {   
            if(!empty($value['email']) && !in_array('clarion2018-'.$value['personID'],$uniqid))
            {
                $insert['tmp_id'] = 'clarion2018-'.$value['personID'];
                $insert['Firstname'] = $value['firstName'];
                $insert['Lastname'] = $value['lastName'];
                $insert['Email'] = $value['email'];
                $insert['Title'] = $value['jobTitle'];
                $insert['Salutation'] = $value['title'];
                $insert['Company_name'] = $value['company'];
                $insert['detailsURL'] = $value['detailsURL'];
                $tmp[] = $insert;
                unset($insert);
            }
        }
        $this->db->insert_batch('clarion_attendee',$tmp);
        j($tmp);
    }
    public function clarionAttendeeExtraColumn($page_no=1)
    {   

        ini_set('max_execution_time', 0);
        switch ($page_no)
        {
            case 1:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50')->get('clarion_attendee')->result_array();
                break;            
            case 2:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','100')->get('clarion_attendee')->result_array();
                break;
            case 3:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','200')->get('clarion_attendee')->result_array();
                break;
            case 4:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','300')->get('clarion_attendee')->result_array();
                break;
            case 5:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','400')->get('clarion_attendee')->result_array();
                break;
            case 6:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','500')->get('clarion_attendee')->result_array();
                break;
            case 7:
                $clarion_attendee = $this->db->where('extra IS NULL')->limit('50','600')->get('clarion_attendee')->result_array();
                break;
        }
        $opts = array(
                      'http'=>array(
                        'method'=>"GET",
                        'header'=>"Authorization: Basic bGF3cmVuY2VAYWxsaW50aGVsb29wLmNvbTpFQTY4MDA0RkEyOTI0MTlBQUI1QkE0NzU1NDFFMjc2RQ=="
                        )
                    );
        $context = stream_context_create($opts);
        foreach ($clarion_attendee as $key => $value)
        {
            $extra_column = json_decode(file_get_contents($value['detailsURL'],false, $context),true)['data'];
            $allKeys = array_column_1($extra_column['customRegistrationData'], 'key');
            $allValues = array_column_1($extra_column['customRegistrationData'], 'value');
            $customData = array_combine($allKeys,$allValues);
            $allKeys = array_column_1($extra_column['customData'], 'key');
            $allValues = array_column_1($extra_column['customData'], 'value');
            $customData2 = array_combine($allKeys,$allValues);
            $tmp['First Name'] = $extra_column['firstName'];
            $tmp['Clarion Barcode'] = $customData['Clarion barcode'];
            $tmp['Address Line 1'] = $extra_column['addressLine1'];
            $tmp['Address Line 2'] = $extra_column['addressLine2'];
            $tmp['Town'] = $extra_column['town'];
            $tmp['Postcode'] = $extra_column['postCode'];
            $tmp['Dietary Requirements'] = $customData2['Dietary Requirements'];
            $tmp['Dietary Requirements Other'] = $customData2['Dietary Requirements Other'];
            $tmp['Special Access Requirements'] = $customData['Special Access Requirements'];
            $tmp['Office location Clarion'] = $customData['Office location Clarion'];
            $tmp['Other office location'] = $customData['Other office location'];
            $tmp['Do you have a question that youd like to ask Ruth'] = $customData['Do you have a question that youd like to ask Ruth'];
            $tmp['What topics would you like to see covered at the Conference'] = $customData['What topics would you like to see covered at the Conference'];
            $tmp['Clarion event date'] = $customData['Clarion event date'];
            $tmp['Clarion workshop session time'] = $customData['Clarion workshop session time'];
            $tmp['Will you be using the coaches provided'] = $customData['Will you be using the coaches provided'];
            $tmp['PersonID'] = $extra_column['PersonID'];
            $tmp['Do you have an email address'] = $customData['Do you have a email address'];
            $tmp['Clarion Event Date'] = $customData['Clarion event date'];
            $tmp['Clarion Workshop Session Time'] = $customData['Clarion workshop session time'];
            $tmp['First Serve'] = $customData['First serve'];
            $tmp['Preferred Workshop'] = $customData['Please select your preferred workshop'];
            $clarion_attendee[$key]['extra'] = json_encode($tmp);
        }
        $this->db->update_batch('clarion_attendee',$clarion_attendee,'tmp_id');
        j($clarion_attendee);
    }
    public function add_attendees_clarion($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100')->get('clarion_attendee')->result_array();
                break;            
            case 2:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','100')->get('clarion_attendee')->result_array();
                break;
            case 3:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','200')->get('clarion_attendee')->result_array();
                break;
            case 4:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','300')->get('clarion_attendee')->result_array();
                break;
            case 5:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','400')->get('clarion_attendee')->result_array();
                break;
            case 6:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','500')->get('clarion_attendee')->result_array();
                break;
            case 7:
                $clarion_attendee = $this->db->where('extra IS NOT NULL')->limit('100','600')->get('clarion_attendee')->result_array();
                break;
        }
        
        $event_id = 1825;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($clarion_attendee as $key => $value)
        {   
            $this->db->select('u.*,reu.Role_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('u.Email',$value['Email']);
            $role = $this->db->get('user u')->row_array();
            if(empty($role) || $role['Role_id'] != '3')
            {
                unset($role['Role_id']);
                $user = $role;
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                $data['knect_api_user_id'] = $value['tmp_id'];
                if(!empty(json_decode($value['extra'],true)['Clarion Barcode']))
                $data['Password'] = md5(json_decode($value['extra'],true)['Clarion Barcode']);
                unset($data['detailsURL']);
                unset($data['tmp_id']);
                unset($data['extra']);
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }
                $event_attendee = $this->db->where('Event_id',$event_id)->where('Attendee_id',$user_id)->get('event_attendee')->row_array();
                if(!empty($event_attendee))
                {
                    $update['extra_column'] = $value['extra'];
                    $this->db->where($event_attendee);
                    $this->db->update('event_attendee',$update);
                }
                $this->db->where($value);
                $this->db->delete('clarion_attendee');
            }
        }
        $clarion_attendee = $this->db->get('clarion_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $clarion_attendee . " Refresh Page -> " . round($clarion_attendee / 50 ) . " Time";
        j($msg);
    }

    public function OEBBerlin_speaker()
    {   
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.oeb.global/speakers/OEB-18",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"event_id\"\r\n\r\n339061\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n322254\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"notification_id\"\r\n\r\n2732\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"flag\"\r\n\r\n2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic dGVzdDpsdWlzZQ==",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "postman-token: 4c64cf80-e06c-330f-7e40-4645e609b1ee"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $idata = [];
            $data = json_decode($response,true);
            foreach($data as $key=>$item)
            {
                $temp['first_name']   = $item['first_name'];
                $temp['last_name']   = $item['surname'];
                $temp['title']   = $item['institution'];
                $temp['email']   = 'oeber2018'.$item['email'];
                $temp['img_url']   = $item['imageCroppedURL'];
                $s_code = [];
                foreach($item['session_code_reference'] as $k=>$v)
                {
                    $s_code[] = 'oeb18-'.$v['session_code'];
                }
                $temp['session_code']   = implode(',',$s_code);
                $temp['desc']   = $item['description'];
                $temp['address_id']   = 'ad-oeb-18'.$item['address_id'];

                $idata[] = $temp;
            }
            if(count($idata))
            $this->db->insert_batch('temp_oebberline_speaker',$idata);
        }
    }

    public function OEBBerlin_session()
    {   
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.oeb.global/sessions/OEB-18",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"event_id\"\r\n\r\n339061\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n322254\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"notification_id\"\r\n\r\n2732\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"flag\"\r\n\r\n2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic dGVzdDpsdWlzZQ==",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "postman-token: 4c64cf80-e06c-330f-7e40-4645e609b1ee"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $idata = [];
            $data = json_decode($response,true);
            if($_GET['test'] == '1')
            {
                j($data);
            }
            foreach($data as $key=>$item)
            {
                $temp['session_name']   = trim($item['title']);
                $temp['session_type']   = $item['session_theme'];
                $temp['start_date']   = $item['date_start'];
                $temp['end_date']   = $item['date_end'];
                $temp['start_time']   = $item['time_start'];
                $temp['end_time']   = $item['time_end'];
                $temp['desc']   = $item['preconf_description'];
                $temp['session_code'] = 'oeb18-'.$item['session_code'];
                $temp['speaker_address_id_reference'] = implode(',',array_filter(explode(',',$item['speaker_address_id_reference'].','.$item['address_id_reference'])));

                $idata[] = $temp;
            }
            if(count($idata))
            $this->db->insert_batch('temp_oebberline_session',$idata);
        }
    }
    public function OEBSpeakersAdd()
    {
        $org_id = 33782;
        $eid = 1757;
        $temp_speakers=$this->db->limit(100)->get('temp_oebberline_speaker')->result_array();

        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['address_id']),'Email' => $value['email']))->row_array();
            $user_data['Firstname']=$value['first_name'];
            $user_data['Lastname']=$value['last_name'];
            $user_data['Email'] =  $value['email'];
            //$user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['desc'];
            $user_data['Title']=$value['title'];
            if(!empty($value['img_url']))
            {   
                $logoname="learn2018_spr_logo".uniqid().'.png';
                copy($value['img_url'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['address_id']);
            $user_data['is_from_api'] = '1';
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id,'Role_id' => '7'))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('address_id',$value['address_id'])->delete('temp_oebberline_speaker');
        }
        $total_exhi = $this->db->select()->from('temp_oebberline_speaker')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        updateModuleDate($eid,'speaker');
        die;
    }
    public function OEBAgenda_add()
    {   

        $org_id=33782;
        $Agenda_id=2146;
        $eid=1757;
        $temp_session=$this->db->select('*')->from('temp_oebberline_session')->limit(50)->get()->result_array();

        foreach ($temp_session as $key => $value) 
        {   
            if(!empty($value['speaker_address_id_reference']))
            {
                $speaker=array();
                $speaker_name = array();
                $temp_speaker = explode(',',$value['speaker_address_id_reference']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->select('Id,CONCAT_WS(" ",Firstname,Lastname) as Name,Company_name',false)->get_where('user',array('knect_api_user_id'=>'ad-oeb-18'.$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                        $tmp_name[] = $sidarr['Name'];
                        $tmp_name[] = $sidarr['Company_name'];
                        $spr_name = implode(' ',$tmp_name);
                        array_push($speaker_name,$spr_name);
                        unset($tmp_name);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
                $session_data['custom_speaker_name']=implode(',',$speaker_name);

            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=date('Y-m-d',strtotime($value['start_date']));
            $session_data['Start_time']=$value['start_time'];
            $session_data['End_date']=date('Y-m-d',strtotime($value['end_date']));
            $session_data['End_time']=$value['end_time'];
            $session_data['Heading']=trim($value['session_name']);
            $session_data['description']=$value['desc'];
            //$session_data['custom_location']=$value['Location'];
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['session_type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
                $this->db->where('type_id',$type_id);
                $typeup['type_name'] = $value['session_type'];
                $this->db->update('session_types',$typeup);
            }
            else
            {
                $type_data['type_name']=trim($value['session_type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['session_code']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['session_code']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['session_code']);
                $session_data['Agenda_status']='1';
                $session_data['show_rating']='1';
                $session_data['allow_clashing']='1';
                $session_data['allow_comments']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('id',$value['id'])->delete('temp_oebberline_session');
        }
        $total_exhi = $this->db->select()->from('temp_oebberline_session')->get()->num_rows();
        updateModuleDate($eid,'agenda');
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function addRegisterdUser()//registrtion sync
    {   
        // show_errors();
        // echo file_get_contents("php://input");exit;
        $this->load->model('Native_single_fcm/App_login_model');
        $input = json_decode(json_decode(file_get_contents("php://input"),true),true);
        // $this->db->insert('webhook',$insert);exit;
        $Firstname = $input['FirstName'];
        $Lastname = $input['LastName'];
        $Email = $input['Email'];
        $input_data = $input['data'];

        foreach ($input_data as $key => $value)
        {   
            $data[ucfirst(substr($value['questionID']['mergefields'],'1','-1'))] =  $value['answer'];
        }
        $event_id = '1892';
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        extract($this->input->post());
        //get event cutom column 
        $custom_column = array_column_1($this->db->where('event_id',$event_id)->get('custom_column')->result_array(),'column_name');
        $recived_custom_column = array_keys($data);
        $new_custom_column = array_diff($recived_custom_column,$custom_column);
        
        foreach ($new_custom_column as $key => $value)
        {
            $tmp['event_id'] = $event_id;
            $tmp['column_name'] = $value;
            $insert[] =  $tmp;
        }
        if(!empty($insert))
            $this->db->insert_batch('custom_column',$insert);

        if(!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {    
            $event_attendee['extra_column'] = json_encode($data);
            unset($data);
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);exit;   
            }
            $this->load->model('Native_single_fcm/App_login_model');
            $user = $this->db->where('Email',$Email)->get('user')->row_array();
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['is_from_api'] = '1';
            if($user)
            {   
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
            //save extra column
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id',$event_id);
            $this->db->where('Attendee_id',$user_id);
            $qu=$this->db->get();
            $extra_info=$qu->row_array();
            if(!empty($extra_info))
            {
                $this->db->where('Event_id',$event_id);
                $this->db->where('Attendee_id',$user_id);
                $this->db->update('event_attendee',$event_attendee);
            }
            else
            {
                $event_attendee['Event_id']=$event_id;
                $event_attendee['Attendee_id']=$user_id;
                $this->db->insert('event_attendee',$event_attendee);
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }
    public function clarion2018Password()
    {   
        show_errors();
        $this->db->where('Event_id',1825);
        $res = $this->db->get('event_attendee')->result_array();
        foreach ($res as $key => $value)
        {
            $tmp_custom = json_decode($value['extra_column'],true);
            if(array_key_exists('Clarion Barcode',$tmp_custom))
            {
                $update[$i]['Id'] = $value['Attendee_id'];
                $update[$i]['Password'] = md5($tmp_custom['Clarion Barcode']);
                $i++;
            }
        }
        // $this->db->update_batch('user',$update,'Id');
    }
    public function test_mail()
    {   
        show_errors();
        // $this->load->config('Mandrill');
        $this->load->library('Mandrill');

        $mandrill_ready = NULL;

        try {

            $this->mandrill->init('6U6JCmoYhLcaKpKb4O6dAw');
            $mandrill_ready = TRUE;
            
        } catch(Mandrill_Exception $e) {

            $mandrill_ready = FALSE;
            
        }

        if( $mandrill_ready ) {

            //Send us some email!
            $email = array(
                'html' => '<p>This is my message<p>', //Consider using a view file
                'text' => 'This is my plaintext message',
                'subject' => 'subject',
                'from_email' => 'invite@allintheloop.com',
                'from_name' => 'ALL IN THE LOOP',
                "track_opens" => true,
                "track_clicks" => true,
                "auto_text" => true,
                'to' => array(array('email' => 'gaurang@xhtmljunkies.com' )),
                'attachments' => array(array('http://www.allintheloop.net/Attendee_admin/index/1511'))
                );

            $result = $this->mandrill->messages_send($email,true);   
            j($result);

        }
    }
    public function test_mail1()
    {   
        show_errors();
        $this->load->library('Mandrill');
        $this->mandrill->setApiKey('6U6JCmoYhLcaKpKb4O6dAw');
        $this->mandrill->setTo('gaurang@xhtmljunkies.com');
        $this->mandrill->setFrom('invite@allintheloop.com');
        $this->mandrill->setSender('Allintheloop');
        $this->mandrill->setSubject('Subject');
        $this->mandrill->setText('text');
        $this->mandrill->addAttachment('https://www.allintheloop.net/assets/images/adminpanellogo.png');
        // $this->mandrill->addAttachment('./assets/badges_files/badges_files10703.pdf');
        j($this->mandrill->send());
    }
    public function updateHomeScreen()
    {   
        $event_id = '1767';
        if (isset($_FILES) && $_FILES['image']['name'] != NULL && $_FILES['image']['name'] != '')
        {   
            $this->load->library('upload');
            $tempname = explode('.', $_FILES['image']['name']);
            $tempname[0] = str_replace(array(
                ' ',
                '%',
                '+',
                '&',
                '-'
            ) , array(
                '_',
                '_',
                '_',
                '_',
                '_'
            ) , $tempname[0]);
            $tempname[0] = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
            $_FILES['image']['name'] = $tempname[0] . "." . $tempname[1];
            $this->upload->initialize(array(
                "file_name" => $_FILES['image']['name'],
                "upload_path" => "./assets/user_files",
                "allowed_types" => 'gif|jpg|png|jpeg'
            ));
            if (!$this->upload->do_upload("image"))
            {
                echo "error###" . $this->upload->display_errors();
                die;
            }
            else
            {
                $data = array(
                    'upload_data' => $this->upload->data()
                );
            }
            $update['Image'] = $_FILES['image']['name'];
            $where['event_id'] = $event_id;
            $where['image_type'] = '0';
            $this->db->where($where);
            $this->db->update('event_banner_image',$update);
        }
        j('Image Uploaded');
    }
    public function OEBBerlin_speakerV2()
    {   
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.oeb.global/LOOPspeakers/OEB-18",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"event_id\"\r\n\r\n339061\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n322254\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"notification_id\"\r\n\r\n2732\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"flag\"\r\n\r\n2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic dGVzdDpsdWlzZQ==",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "postman-token: 4c64cf80-e06c-330f-7e40-4645e609b1ee"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $idata = [];
            $data = json_decode($response,true);
            foreach($data as $key=>$item)
            {
                $temp['first_name']   = $item['Firstname'];
                $temp['last_name']   = $item['Lastname'];
                $temp['title']   = $item['Title'];
                $temp['company']   = $item['Company_name'];
                if(!empty($item['Email']))
                $temp['email']   = 'oeber2018'.$item['Email'];
                else
                $temp['email'] = 'oeber2018-'.$item['Firstname'].'-'.$item['Speaker_code'].'@venturiapps.com'; 
                $temp['img_url']   = $item['Logo_square'];
                $temp['desc']   = $item['Speaker_desc'];
                $temp['address_id']   = 'ad-oeb-18'.$item['Speaker_code'];
                $idata[] = $temp;
            }
            if(count($idata))
            $this->db->insert_batch('temp_oebberline_speaker',$idata);
            j($idata);
        }
    }

    public function OEBBerlin_sessionV2()
    {   
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.oeb.global/LOOPsessions/OEB-18",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"event_id\"\r\n\r\n339061\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"user_id\"\r\n\r\n322254\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"notification_id\"\r\n\r\n2732\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"flag\"\r\n\r\n2\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
        CURLOPT_HTTPHEADER => array(
            "authorization: Basic dGVzdDpsdWlzZQ==",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "postman-token: 4c64cf80-e06c-330f-7e40-4645e609b1ee"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $idata = [];
            $data = json_decode($response,true);
            foreach($data as $key=>$item)
            {
                $temp['session_name']   = trim($item['Heading']);
                $temp['session_type']   = $item['Types'];
                $temp['start_date']   = $item['Start_date'];
                $temp['end_date']   = $item['End_date'];
                $temp['start_time']   = $item['Start_time'];
                $temp['end_time']   = $item['End_time'];
                $temp['desc']   = $item['description'];
                $temp['custom_location']   = $item['custom_location'];
                $temp['session_code'] = 'oeb18-'.$item['session_code'];
                $temp['speaker_address_id_reference'] = $item['speaker_code'];
                $idata[] = $temp;
            }
            if(count($idata))
            $this->db->insert_batch('temp_oebberline_session',$idata);
        j($idata);
        }
    }
    public function OEBSpeakersAddV2()
    {
        $org_id = 33782;
        $eid = 1757;
        $temp_speakers=$this->db->limit(100)->get('temp_oebberline_speaker')->result_array();

        foreach ($temp_speakers as $key => $value) 
        {
            $userdata=$this->db->get_where('user',array('knect_api_user_id'=>trim($value['address_id']),'Email' => $value['email']))->row_array();
            $user_data['Firstname']=$value['first_name'];
            $user_data['Lastname']=$value['last_name'];
            $user_data['Email'] =  $value['email'];
            $user_data['Company_name']=$value['Company'];
            $user_data['Speaker_desc']=$value['desc'];
            $user_data['Title']=$value['title'];
            if(!empty($value['img_url']))
            {   
                $logoname="OEBberlin_spr_logo".uniqid().'.png';
                copy($value['img_url'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;
            $user_data['knect_api_user_id']=trim($value['address_id']);
            $user_data['is_from_api'] = '1';
            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id,'Role_id' => '7'))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('address_id',$value['address_id'])->delete('temp_oebberline_speaker');
        }
        $total_exhi = $this->db->select()->from('temp_oebberline_speaker')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        updateModuleDate($eid,'speaker');
        die;
    }
    public function OEBAgenda_addV2()
    {   

        $org_id=33782;
        $Agenda_id=2146;
        $eid=1757;
        $temp_session=$this->db->select('*')->from('temp_oebberline_session')->limit(50)->get()->result_array();

        foreach ($temp_session as $key => $value) 
        {   
            if(!empty($value['speaker_address_id_reference']))
            {
                $speaker=array();
                $speaker_name = array();
                $temp_speaker = explode(',',$value['speaker_address_id_reference']);
                foreach ($temp_speaker as $skey => $svalue) {
                    $sidarr=$this->db->select('Id,CONCAT_WS(" ",Firstname,Lastname) as Name,Company_name',false)->get_where('user',array('knect_api_user_id'=>'ad-oeb-18'.$svalue))->row_array();
                    if(!empty($sidarr['Id']))
                    {
                        array_push($speaker,$sidarr['Id']);
                        $tmp_name[] = $sidarr['Name'];
                        $tmp_name[] = $sidarr['Company_name'];
                        $spr_name = implode(' ',$tmp_name);
                        array_push($speaker_name,$spr_name);
                        unset($tmp_name);
                    }
                }
                $session_data['Speaker_id']=implode(",", $speaker);
                $session_data['custom_speaker_name']=implode(',',$speaker_name);

            }
            else
            {
                $session_data['Speaker_id']=NULL;
            }
            $session_data['Start_date']=date('Y-m-d',strtotime($value['start_date']));
            $session_data['Start_time']=$value['start_time'];
            $session_data['End_date']=date('Y-m-d',strtotime($value['end_date']));
            $session_data['End_time']=$value['end_time'];
            $session_data['Heading']=trim($value['session_name']);
            $session_data['description']=$value['desc'];
            $session_data['custom_location']=$value['custom_location'];
            //$session_data['custom_location']=$value['Location'];
            $typedata=$this->db->get_where('session_types',array('type_name'=>trim($value['session_type']),'event_id'=>$eid))->row_array();
            if(!empty($typedata))
            {
                $type_id=$typedata['type_id'];
                $this->db->where('type_id',$type_id);
                $typeup['type_name'] = $value['session_type'];
                $this->db->update('session_types',$typeup);
            }
            else
            {
                $type_data['type_name']=trim($value['session_type']);
                $type_data['event_id']=$eid;
                $type_data['created_date']=date('Y-m-d H:i:s'); 
                $this->db->insert('session_types',$type_data);
                $type_id=$this->db->insert_id();
            }
            $session_data['Types']=$type_id;
            $sessiondata=$this->db->select('*')->from('agenda')->where('Event_id',$eid)->where('agenda_code',trim($value['session_code']))->get()->row_array();
            if(!empty($sessiondata))
            {
                $this->db->where('Event_id',$eid)->where('agenda_code',trim($value['session_code']))->update('agenda',$session_data);
                $session_id=$sessiondata['Id'];
            }
            else
            {
                $session_data['Event_id']=$eid;
                $session_data['Organisor_id']=$org_id;
                $session_data['agenda_code']=trim($value['session_code']);
                $session_data['Agenda_status']='1';
                $session_data['show_rating']='1';
                $session_data['allow_clashing']='1';
                $session_data['allow_comments']='1';
                $session_data['created_date']=date('Y-m-d H:i:s');
                $this->db->insert('agenda',$session_data);
                $session_id=$this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);

            $reldata=$this->db->get_where('agenda_category_relation',array('agenda_id'=>$session_id,'category_id'=>$Agenda_id))->row_array();
            if(empty($reldata))
            {
                $rel_data=array('agenda_id'=>$session_id,'category_id'=>$Agenda_id);
                $this->db->insert('agenda_category_relation',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('id',$value['id'])->delete('temp_oebberline_session');
        }
        $total_exhi = $this->db->select()->from('temp_oebberline_session')->get()->num_rows();
        updateModuleDate($eid,'agenda');
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');
    }
    public function reinsider()
    {

		$curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://testsxapi.reinders.com/testsxapi/ServiceSF.svc?wsdl",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:prox=\"http://schemas.datacontract.org/2004/07/ProxyGen.com.infor.sxapi.connection\" xmlns:prox1=\"http://schemas.datacontract.org/2004/07/ProxyGen.com.infor.sxapi.SFOEOrderTotLoadV3\">\r\n    <soapenv:Header />\r\n    <soapenv:Body>\r\n        <tem:SFOEOrderTotLoadV3>\r\n            <tem:callConnection>\r\n                <prox:CompanyNumber>1</prox:CompanyNumber>\r\n                <prox:ConnectionString>Appserver://testsxe:7482/test2sxapiappsrv</prox:ConnectionString>\r\n                <prox:OperatorInitials>webe</prox:OperatorInitials>\r\n                <prox:OperatorPassword>a5P7Hdh2Yi</prox:OperatorPassword>\r\n\t\t\t <prox:SessionModel>0</prox:SessionModel>\r\n\t\t\t <!-- this call connection must be passed with every order! We will change the connection string to our\r\n\t\t\t live environment once we have completed testing -->\r\n            </tem:callConnection>\r\n            <tem:request>\r\n                <prox1:Inheaderextradata>\r\n                    <prox1:Inheaderextradata>\r\n                        <prox1:FieldName>Terms_Type</prox1:FieldName>\r\n                        <prox1:FieldValue>6</prox1:FieldValue>\r\n                        <prox1:SequenceNumber>1</prox1:SequenceNumber>\r\n                    </prox1:Inheaderextradata>\r\n                    <!-- this inheaderextradata array is to be only passed if the customer chooses to pay with\r\n                    Credit Card or PayPal\r\n                    If they choose On Reinders Account then comment this array out-->\r\n                <prox1:Inheaderextradata>\r\n                        <prox1:FieldName>user3</prox1:FieldName>\r\n                        <prox1:FieldValue>jott@reinders.com</prox1:FieldValue>\r\n                        <!-- this is the business email -->\r\n                        <prox1:SequenceNumber>2</prox1:SequenceNumber>\r\n                </prox1:Inheaderextradata>\r\n              </prox1:Inheaderextradata>\r\n                <prox1:Ininheader>\r\n                    <prox1:Ininheader>\r\n                        <prox1:CustomerID>00010000006111</prox1:CustomerID>   \r\n                        <!-- this is linked to the Reinders Customer Number -->\r\n                        <prox1:OrderType>O</prox1:OrderType>           \r\n                        <!-- THIS ALWAYS HAS TO BE PASSED -->\r\n                        <prox1:PoNumber>GIC REGISTRATION</prox1:PoNumber>\r\n                        <prox1:ShipToName>Bluemound Country Club</prox1:ShipToName>   \r\n                        <!-- this is the business name -->\r\n                        <prox1:WarehouseID>00SX</prox1:WarehouseID>\r\n                        <!-- THIS ALWAYS HAS TO BE PASSED -->\r\n                        <prox1:WebTransactionType>LSF</prox1:WebTransactionType>\r\n                        <!-- THIS ALWAYS HAS TO BE PASSED -->\r\n                    </prox1:Ininheader>\r\n                </prox1:Ininheader>\r\n                <prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"Wednesday GIC2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>1</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                      <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGISTHURSDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"Thursday GIC2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>2</prox1:SequenceNumber>                 \t    \r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGIS</prox1:ItemNumber>\r\n                        <!--this is the example for \"Wednesday & Thursday GIC 2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>3</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"3 wednesday RESCON tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>4</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"4 wednesday RESCON tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>2</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>5</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"5 wednesday RESCON tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>3</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>6</prox1:SequenceNumber>\r\n                    </prox1:Ininline>                    \r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>REGISWEDNESDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"On Site Wednesday GIC2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>7</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>REGISTHURSDAY</prox1:ItemNumber>\r\n                        <!--this is the example for \"On Site Thursday GIC2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>8</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                    <prox1:Ininline>\r\n                        <prox1:ItemNumber>REGIS</prox1:ItemNumber>\r\n                        <!--this is the example for \"On Site Wednesday & Thursday GIC2019 tickets !-->\r\n                        <prox1:LineItemType>I</prox1:LineItemType>\r\n                        <prox1:OrderQty>1</prox1:OrderQty>\r\n                 \t    <prox1:SequenceNumber>9</prox1:SequenceNumber>\r\n                    </prox1:Ininline>\r\n                </prox1:Ininline>\r\n                <prox1:Inlineextradata/>\r\n            </tem:request>\r\n        </tem:SFOEOrderTotLoadV3>\r\n    </soapenv:Body>\r\n</soapenv:Envelope>",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/xml",
		    "Postman-Token: 8a9a573a-d2da-4732-a58d-382a8709734f",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
    }
    public function testr()
    {   
        show_errors();  
	    $soapUrl = "http://testsxapi.reinders.com/testsxapi/ServiceSF.svc?wsdl"; 
        
        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:prox="http://schemas.datacontract.org/2004/07/ProxyGen.com.infor.sxapi.connection" xmlns:prox1="http://schemas.datacontract.org/2004/07/ProxyGen.com.infor.sxapi.SFOEOrderTotLoadV3">
            <soapenv:Header />
            <soapenv:Body>
                <tem:SFOEOrderTotLoadV3>
                    <tem:callConnection>
                        <prox:CompanyNumber>1</prox:CompanyNumber>
                        <prox:ConnectionString>Appserver://testsxe:7482/test2sxapiappsrv</prox:ConnectionString>
                        <prox:OperatorInitials>webe</prox:OperatorInitials>
                        <prox:OperatorPassword>a5P7Hdh2Yi</prox:OperatorPassword>
                     <prox:SessionModel>0</prox:SessionModel>
                     <!-- this call connection must be passed with every order! We will change the connection string to our
                     live environment once we have completed testing -->
                    </tem:callConnection>
                    <tem:request>
                        <prox1:Inheaderextradata>
                            <prox1:Inheaderextradata>
                                <prox1:FieldName>Terms_Type</prox1:FieldName>
                                <prox1:FieldValue>6</prox1:FieldValue>
                                <prox1:SequenceNumber>1</prox1:SequenceNumber>
                            </prox1:Inheaderextradata>
                            <!-- this inheaderextradata array is to be only passed if the customer chooses to pay with
                            Credit Card or PayPal
                            If they choose On Reinders Account then comment this array out-->
                        <prox1:Inheaderextradata>
                                <prox1:FieldName>user3</prox1:FieldName>
                                <prox1:FieldValue>jott@reinders.com</prox1:FieldValue>
                                <!-- this is the business email -->
                                <prox1:SequenceNumber>2</prox1:SequenceNumber>
                        </prox1:Inheaderextradata>
                      </prox1:Inheaderextradata>
                        <prox1:Ininheader>
                            <prox1:Ininheader>
                                <prox1:CustomerID>00010000006111</prox1:CustomerID>   
                                <!-- this is linked to the Reinders Customer Number -->
                                <prox1:OrderType>O</prox1:OrderType>           
                                <!-- THIS ALWAYS HAS TO BE PASSED -->
                                <prox1:PoNumber>GIC REGISTRATION</prox1:PoNumber>
                                <prox1:ShipToName>Bluemound Country Club</prox1:ShipToName>   
                                <!-- this is the business name -->
                                <prox1:WarehouseID>00SX</prox1:WarehouseID>
                                <!-- THIS ALWAYS HAS TO BE PASSED -->
                                <prox1:WebTransactionType>LSF</prox1:WebTransactionType>
                                <!-- THIS ALWAYS HAS TO BE PASSED -->
                            </prox1:Ininheader>
                        </prox1:Ininheader>
                        <prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>
                                <!--this is the example for "Wednesday GIC2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>1</prox1:SequenceNumber>
                            </prox1:Ininline>
                              <prox1:Ininline>
                                <prox1:ItemNumber>PREREGISTHURSDAY</prox1:ItemNumber>
                                <!--this is the example for "Thursday GIC2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>2</prox1:SequenceNumber>                      
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>PREREGIS</prox1:ItemNumber>
                                <!--this is the example for "Wednesday & Thursday GIC 2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>3</prox1:SequenceNumber>
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>
                                <!--this is the example for "3 wednesday RESCON tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>4</prox1:SequenceNumber>
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>
                                <!--this is the example for "4 wednesday RESCON tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>2</prox1:OrderQty>
                                <prox1:SequenceNumber>5</prox1:SequenceNumber>
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>PREREGISWEDNESDAY</prox1:ItemNumber>
                                <!--this is the example for "5 wednesday RESCON tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>3</prox1:OrderQty>
                                <prox1:SequenceNumber>6</prox1:SequenceNumber>
                            </prox1:Ininline>                    
                            <prox1:Ininline>
                                <prox1:ItemNumber>REGISWEDNESDAY</prox1:ItemNumber>
                                <!--this is the example for "On Site Wednesday GIC2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>7</prox1:SequenceNumber>
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>REGISTHURSDAY</prox1:ItemNumber>
                                <!--this is the example for "On Site Thursday GIC2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>8</prox1:SequenceNumber>
                            </prox1:Ininline>
                            <prox1:Ininline>
                                <prox1:ItemNumber>REGIS</prox1:ItemNumber>
                                <!--this is the example for "On Site Wednesday & Thursday GIC2019 tickets !-->
                                <prox1:LineItemType>I</prox1:LineItemType>
                                <prox1:OrderQty>1</prox1:OrderQty>
                                <prox1:SequenceNumber>9</prox1:SequenceNumber>
                            </prox1:Ininline>
                        </prox1:Ininline>
                        <prox1:Inlineextradata/>
                    </tem:request>
                </tem:SFOEOrderTotLoadV3>
            </soapenv:Body>
        </soapenv:Envelope>';  

        $headers = array(
                    "Content-type: text/xml;charset=\"utf-8\"",
                    "Accept: text/xml",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    // "SOAPAction: http://schemas.datacontract.org/2004/07/ProxyGen.com.infor.sxapi.connection", 
                    "SOAPAction: http://testsxapi.reinders.com/testsxapi/ServiceSF.svc?wsdl", 
                    "Content-length: ".strlen($xml_post_string),
                ); 

        $url = $soapUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        
        $response = curl_exec($ch); 
        curl_close($ch);

        j($response);
        $response1 = str_replace("<soap:Body>","",$response);
        $response2 = str_replace("</soap:Body>","",$response1);

        
        $parser = simplexml_load_string($response2);
        echo "<pre>";
        print_r($parser);exit;
    }
    public function test2()
    {   
        show_errors();
        ob_start();
        $client = new SoapClient('http://testsxapi.reinders.com/testsxapi/ServiceSF.svc?wsdl'); 
        var_dump($client->__getFunctions()); 
    }

    public function add_GFMF_attendee()

    {
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $Password = $this->input->post('Password');
        $custom_column = json_decode($this->input->post('custom_column') , true);
        if (!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {   
            foreach($custom_column as $key => $value)
            {
                $tmp_custom_column[strip_tags($value['key']) ] = $value['value'];
            }
            $event_attendee['extra_column'] = json_encode($tmp_custom_column);
            if (!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);
                exit;
            }
            $check_unique = $this->db->where('Email', $Email)->get('user')->row_array();
            /*if($check_unique)
            {
            $data = array(
            'success' => false,
            'message' => 'Email is already exists'
            );
            echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 2029; //2029
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email', $Email)->get('user')->row_array();
            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name', $Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id']) ? : $Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if (!empty($Password)) $data['Password'] = md5($Password);
            if ($user)
            {
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email', $Email);
                $this->db->update('user', $data);
                $cnt = $this->App_login_model->checkEmailAlreadyByEvent1($Email, $event_id);
                if ($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'], $event_id, $org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user', $data);
                $user_id = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id, $event_id, $org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
            // save extra column
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id', $event_id);
            $this->db->where('Attendee_id', $user_id);
            $qu = $this->db->get();
            $extra_info = $qu->row_array();
            if (!empty($extra_info))
            {
                $this->db->where('Event_id', $event_id);
                $this->db->where('Attendee_id', $user_id);
                $this->db->update('event_attendee', $event_attendee);
            }
            else
            {
                $event_attendee['Event_id'] = $event_id;
                $event_attendee['Attendee_id'] = $user_id;
                $this->db->insert('event_attendee', $event_attendee);
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);
        exit;
    }

    public function add_Connect_attendee()

    {   
        foreach($custom_column as $key => $value)
        {
            $tmp_custom_column[strip_tags($value['key']) ] = $value['value'];
        }
        $this->load->model('Native_single_fcm/Attendee_model');
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $Password = $this->input->post('Password');
        $custom_column = json_decode($this->input->post('custom_column') , true);
        if (!empty($Email) && !empty($Firstname) && !empty($Lastname))
        {
            $event_attendee['extra_column'] = json_encode($tmp_custom_column);
            if (!filter_var($Email, FILTER_VALIDATE_EMAIL))
            {
                $data = array(
                    'success' => false,
                    'message' => 'Email should be valid'
                );
                echo json_encode($data);
                exit;
            }
            $check_unique = $this->db->where('Email', $Email)->get('user')->row_array();
            /*if($check_unique)
            {
            $data = array(
            'success' => false,
            'message' => 'Email is already exists'
            );
            echo json_encode($data);exit;
            }*/
            $this->load->model('Native_single_fcm/App_login_model');
            $event_id = 1987; //1987
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            $user = $this->db->where('Email', $Email)->get('user')->row_array();
            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name', $Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id']) ? : $Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['is_from_api'] = '1';
            $data['Active'] = '1';
            if (!empty($Password)) $data['Password'] = md5($Password);
            if ($user)
            {
                $user_id = $user['Id'];
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email', $Email);
                $this->db->update('user', $data);
                $cnt = $this->App_login_model->checkEmailAlreadyByEvent1($Email, $event_id);
                if ($cnt == 0)
                {
                    $this->App_login_model->setupUserWithEvent($user['Id'], $event_id, $org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user', $data);
                $user_id = $this->db->insert_id();
                $this->App_login_model->setupUserWithEvent($user_id, $event_id, $org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
            }
            // save extra column
            $this->db->select('*')->from('event_attendee');
            $this->db->where('Event_id', $event_id);
            $this->db->where('Attendee_id', $user_id);
            $qu = $this->db->get();
            $extra_info = $qu->row_array();
            if (!empty($extra_info))
            {
                $this->db->where('Event_id', $event_id);
                $this->db->where('Attendee_id', $user_id);
                $this->db->update('event_attendee', $event_attendee);
            }
            else
            {
                $event_attendee['Event_id'] = $event_id;
                $event_attendee['Attendee_id'] = $user_id;
                $this->db->insert('event_attendee', $event_attendee);
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);
        exit;
    }

    public function add_attendees_2012($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $tmp_2012_attendee = $this->db->limit('50')->get('tmp_2012_attendee')->result_array();
                break;            
            case 2:
                $tmp_2012_attendee = $this->db->limit('50','100')->get('tmp_2012_attendee')->result_array();
                break;
            case 3:
                $tmp_2012_attendee = $this->db->limit('50','200')->get('tmp_2012_attendee')->result_array();
                break;
            case 4:
                $tmp_2012_attendee = $this->db->limit('50','300')->get('tmp_2012_attendee')->result_array();
                break;
            case 5:
                $tmp_2012_attendee = $this->db->limit('50','400')->get('tmp_2012_attendee')->result_array();
                break;
            case 6:
                $tmp_2012_attendee = $this->db->limit('50','500')->get('tmp_2012_attendee')->result_array();
                break;
            case 7:
                $tmp_2012_attendee = $this->db->limit('50','600')->get('tmp_2012_attendee')->result_array();
                break;
            case 8:
                $tmp_2012_attendee = $this->db->limit('50','700')->get('tmp_2012_attendee')->result_array();
                break;
            case 9:
                $tmp_2012_attendee = $this->db->limit('50','800')->get('tmp_2012_attendee')->result_array();
                break;
            case 10:
                $tmp_2012_attendee = $this->db->limit('50','900')->get('tmp_2012_attendee')->result_array();
                break;
        }
        
        $event_id = 2012;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($tmp_2012_attendee as $key => $value)
        {   
            $this->db->select('u.*,reu.Role_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('u.Email',$value['Email']);
            $role = $this->db->get('user u')->row_array();
            if(empty($role) || $role['Role_id'] != '3')
            {   
                unset($role['Role_id']);
                $user = $role;
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }

                $this->db->where($value);
                $this->db->delete('tmp_2012_attendee');
            }
        }
        $tmp_2012_attendee = $this->db->get('tmp_2012_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $tmp_2012_attendee . " Refresh Page -> " . round($tmp_2012_attendee / 50 ) . " Time";
        j($msg);
    }

    public function copyCMS($old_event_id,$event_id)
    {   
        $cms_data = $this->db->where('Event_id',$old_event_id)
                             ->get('cms')->result_array();
        if (count($cms_data) > 0)
        {
            array_walk_recursive($cms_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'Event_id',
                'newval' => $event_id
            ));
            array_walk_recursive($cms_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'Created_date',
                'newval' => date('Y-m-d H:i:s')
            ));
        }
        foreach ($cms_data as $key => $value)
        {
            $tmp_cms[$key]['old']  = $value['Id'];
            unset($cms_data[$key]['Id']);
            $this->db->insert('cms',$cms_data[$key]);
            $tmp_cms[$key]['new'] = $this->db->insert_id();
        }
        // $tmp_cms = array ( 0 => array ( 'old' => '2369', 'new' => 2389));
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','21');
        $group = $this->db->get('modules_group')->result_array();
        array_walk_recursive($group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'created_date',
                'newval' => date('Y-m-d H:i:s')
            ));
        array_walk_recursive($group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));
        foreach ($group as $key => $value)
        {   
            $tmp_group[$key]['old'] = $value['module_group_id'];
            unset($group[$key]['module_group_id']);
            $this->db->insert('modules_group',$group[$key]);
            $tmp_group[$key]['new'] = $this->db->insert_id();    
        }
        // $tmp_group = array (0=>array( 'old' => '564', 'new' => 574));
        foreach ($tmp_group as $key => $value)
        {   
            $this->db->select('menu_id,'.$value['new'].' as group_id,module_id');
            $this->db->where('group_id',$value['old']);
            $rel = $this->db->get('modules_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['module_id'], array_column($tmp_cms, 'old'));
                $rel[$k]['module_id'] = $tmp_cms[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('modules_group_relation',$rel);
        }
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','21');
        $super_group = $this->db->get('modules_super_group')->result_array();
        array_walk_recursive($super_group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'created_date',
                'newval' => date('Y-m-d H:i:s')
            ));
        array_walk_recursive($super_group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));
        foreach ($super_group as $key => $value)
        {   
            $tmp_sgroup[$key]['old'] = $value['id'];
            unset($super_group[$key]['id']);
            $this->db->insert('modules_super_group',$super_group[$key]);
            $tmp_sgroup[$key]['new'] = $this->db->insert_id();    
        }
        // $tmp_sgroup = array(0=>array('old'=>'44','new' => 46));
        foreach ($tmp_sgroup as $key => $value)
        {
            $this->db->select('menu_id,'.$value['new'].' as super_group_id,child_group_id');
            $this->db->where('super_group_id',$value['old']);
            $rel = $this->db->get('super_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['child_group_id'], array_column($tmp_group, 'old'));
                $rel[$k]['child_group_id'] = $tmp_group[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('super_group_relation',$rel);
        }
    }
    public function update_somthing(&$item=NULL, $key=NULL, $arr=NULL)

    {
        if ($key == $arr['matchkey']) $item = $arr['newval'];
    }

    public function copyDocument($old_eid=NULL, $new_eid=NULL)

    {
        $parent_arr = array();
        $this->db->select('*')->from('documents');
        $this->db->where('Event_id', $old_eid);
        $this->db->order_by('parent');
        $que = $this->db->get();
        $res = $que->result_array();
        foreach($res as $key => $value)
        {
            $doc_data = $value;
            $doc_data['Event_id'] = $new_eid;
            $old_document_id = $doc_data['id'];
            $tmp_document[$key]['old'] = $old_document_id;
            unset($doc_data['id']);
            if (!empty($doc_data['parent']))
            {
                $doc_data['parent'] = $parent_arr[$doc_data['parent']];
            }
            $this->db->insert('documents', $doc_data);
            $parent_arr[$old_document_id] = $this->db->insert_id();
            $new_document_id = $this->db->insert_id();
            $tmp_document[$key]['new'] = $new_document_id;
            $this->db->select('*')->from('document_files');
            $this->db->where('document_id', $value['id']);
            $dque = $this->db->get();
            $dres = $dque->result_array();
            foreach($dres as $akey => $avalue)
            {
                $df_data = $avalue;
                unset($df_data['id']);
                $df_data['document_id'] = $new_document_id;
                $this->db->insert('document_files', $df_data);
            }
        }
        // Save Documents New Old Value
        $save['document'] = json_encode($tmp_document);
        $this->saveCopyAppVar($new_eid,$save);
    }
    public function copyQA($old_event_id, $event_id)
    {
        $qa_data = $this->db->where('Event_id',$old_event_id)->get('qa_session')->result_array();
        array_walk_recursive($qa_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'Event_id',
                'newval' => $event_id
            ));

        foreach ($qa_data as $key => $value)
        {
            $tmp_qa[$key]['old']  = $value['Id'];
            unset($qa_data[$key]['Id']);
            $this->db->insert('qa_session',$qa_data[$key]);
            $tmp_qa[$key]['new'] = $this->db->insert_id();
        }

        $save['qa'] = json_encode($tmp_qa);
        $this->saveCopyAppVar($event_id,$save);

        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','50');
        $group = $this->db->get('modules_group')->result_array();
        array_walk_recursive($group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'created_date',
                'newval' => date('Y-m-d H:i:s')
            ));
        array_walk_recursive($group, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));
        foreach ($group as $key => $value)
        {   
            $tmp_group[$key]['old'] = $value['module_group_id'];
            unset($group[$key]['module_group_id']);
            $this->db->insert('modules_group',$group[$key]);
            $tmp_group[$key]['new'] = $this->db->insert_id();    
        }
        // $tmp_group = array (0=>array( 'old' => '564', 'new' => 574));
        foreach ($tmp_group as $key => $value)
        {   
            $this->db->select('menu_id,'.$value['new'].' as group_id,module_id');
            $this->db->where('group_id',$value['old']);
            $rel = $this->db->get('modules_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['module_id'], array_column($tmp_qa, 'old'));
                $rel[$k]['module_id'] = $tmp_qa[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('modules_group_relation',$rel);
        }
    }
    public function copyFav($old_event_id,$event_id)
    {
        $fav_data = $this->db->select('modules_id,color,position,event_id')
                             ->where('event_id',$old_event_id)
                             ->get('favorites_modules_order')->result_array();
        array_walk_recursive($fav_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));          
        if($fav_data) $this->db->insert_batch('favorites_modules_order',$fav_data);
    }
    public function copyPhotoFilter($old_event_id,$event_id)
    {   
        $this->db->select('event_id,title,image,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $res = $this->db->get('photo_filter_image')->result_array();
        if (!file_exists('./assets/photo_filter/'.$event_id)) {
        mkdir('./assets/photo_filter/'.$event_id, 0777, true);
        }
        foreach ($res as $key => $value)
        {
            copy('./assets/photo_filter/'.$old_event_id.'/'.$value['image'],'./assets/photo_filter/'.$event_id.'/'.$value['image']);
        }
        if($res) $this->db->insert_batch('photo_filter_image',$res);

        $this->db->select('user_id,image,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $res = $this->db->get('photo_filter_uploads')->result_array();
        if($res) $this->db->insert_batch('photo_filter_uploads',$res);

        if (!file_exists('./assets/photo_filter_uploads/'.$event_id)) {
        mkdir('./assets/photo_filter_uploads/'.$event_id, 0777, true);
        }
    }
    public function copyBadgeScanner($old_event_id,$event_id)
    {
        $this->db->select('exibitor_scan_attendee_reward,attendee_scan_swap_contact');
        $this->db->where('event_id',$old_event_id);
        $res = $this->db->get('event_settings')->row_array();

        if($this->db->where('event_id',$event_id)->get('event_settings')->row_array())
        {
            $this->db->where('event_id',$event_id)->update('event_settings',$res);
        }
        else
        {   
            $res['event_id'] = $event_id;
            $this->db->insert('event_settings',$res);
        }
    }
    public function copyAttendee($old_event_id,$event_id)
    {
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','2');
        $attendee_category = $this->db->get('attendee_category')->result_array();
        //attendee category is used for fliter
        foreach ($attendee_category as $key => $value)
        {
            $tmp_attendee_category[$key]['old'] = $value['id'];
            unset($attendee_category[$key]['id']);
            $this->db->insert('attendee_category',$attendee_category[$key]);
            $tmp_attendee_category[$key]['new'] = $this->db->insert_id();
        }
        
        //custom column
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $custom_column = $this->db->get('custom_column')->result_array();
        foreach ($custom_column as $key => $value)
        {
            $tmp_custom_column[$key]['old'] = $value['column_id'];
            unset($custom_column[$key]['column_id']);
            $this->db->insert('custom_column',$custom_column[$key]);
            $tmp_custom_column[$key]['new'] = $this->db->insert_id(); 
        }
        
        //attendee_filter_relation
        $this->db->select('custom_column,label,category_id,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $filter = $this->db->get('attendee_filter_relation')->result_array();

        foreach ($filter as $key => $value)
        {
            $k_r = array_search($value['custom_column'], array_column($tmp_custom_column, 'old'));
            $filter[$key]['custom_column'] = $tmp_custom_column[$k_r]['new'];
            $k_r = array_search($value['category_id'], array_column($tmp_attendee_category, 'old'));
            $filter[$key]['category_id'] = $tmp_attendee_category[$k_r]['new'];
        }

        if($filter) $this->db->insert_batch('attendee_filter_relation',$filter);

        //attendee_extra_column
        $this->db->select('user_id,key1,value,'.$event_id.' as event_id');
        $this->db->where('event_id',$event_id);
        $attendee_filter_relation = $this->db->get('attendee_extra_column')->result_array();

        if($attendee_filter_relation) $this->db->insert_batch('attendee_filter_relation',$attendee_filter_relation);

        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $meeting_location = $this->db->get('meeting_location')->result_array();
        foreach ($meeting_location as $key => $value)
        {
            $tmp_meeting_location[$key]['old'] = $value['location_id'];
            unset($meeting_location[$key]['location_id']);
            $this->db->insert('meeting_location',$meeting_location[$key]);
            $tmp_meeting_location[$key]['new'] = $this->db->insert_id();
        }
        $save['meeting_location'] = json_encode($tmp_meeting_location);
        $this->saveCopyAppVar($event_id,$save);
    }

    public function copySpeaker($old_event_id,$event_id)
    {   
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $speaker_type = $this->db->get('speaker_type')->result_array();

        foreach ($speaker_type as $key => $value)
        {
            $tmp_speaker_type[$key]['old'] = $value['type_id'];
            unset($speaker_type[$key]['type_id']);
            $this->db->insert('speaker_type',$speaker_type[$key]);
            $tmp_speaker_type[$key]['new'] = $this->db->insert_id();
        }

        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $speaker_profile = $this->db->get('speaker_profile')->result_array();

        foreach ($speaker_profile as $key => $value)
        {
            $k_r = array_search($value['type'], array_column($tmp_speaker_type, 'old'));
            $speaker_profile[$key]['type'] = $tmp_speaker_type[$k_r]['new'];
            unset($speaker_profile[$key]['id']);
        }
        if($speaker_profile) $this->db->insert_batch('speaker_profile',$speaker_profile);
    }

    public function copySponsor($old_event_id,$event_id)
    {
        //sponsors_type*
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $sponsors_type = $this->db->get('sponsors_type')->result_array();

        foreach ($sponsors_type as $key => $value)
        {
            $tmp_sponsors_type[$key]['old'] = $value['type_id'];
            unset($sponsors_type[$key]['type_id']);
            $this->db->insert('sponsors_type',$sponsors_type[$key]);
            $tmp_sponsors_type[$key]['new'] = $this->db->insert_id();
        }
        
        //sponsors*
        $this->db->select('*,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $sponsors = $this->db->get('sponsors')->result_array();

        foreach ($sponsors as $key => $value)
        {   
            $k_r = array_search($value['st_id'], array_column($tmp_sponsors_type, 'old'));
            $sponsors[$key]['st_id'] = $tmp_sponsors_type[$k_r]['new'];
            $tmp_sponsors[$key]['old'] = $value['Id'];
            unset($sponsors[$key]['Id']);
            $this->db->insert('sponsors',$sponsors[$key]);
            $tmp_sponsors[$key]['new'] = $this->db->insert_id();
        }

        //modules_group*
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','43');
        $group = $this->db->get('modules_group')->result_array();
        
        foreach ($group as $key => $value)
        {   
            $tmp_group[$key]['old'] = $value['module_group_id'];
            unset($group[$key]['module_group_id']);
            $this->db->insert('modules_group',$group[$key]);
            $tmp_group[$key]['new'] = $this->db->insert_id();    
        }
        
        //modules_group_relation*
        foreach ($tmp_group as $key => $value)
        {   
            $this->db->select('menu_id,'.$value['new'].' as group_id,module_id');
            $this->db->where('group_id',$value['old']);
            $rel = $this->db->get('modules_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['module_id'], array_column($tmp_sponsors, 'old'));
                $rel[$k]['module_id'] = $tmp_sponsors[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('modules_group_relation',$rel);
        }
    }

    public function copyExhibitor($old_event_id,$event_id)
    {   
        //exibitor type
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $exhibitor_type = $this->db->get('exhibitor_type')->result_array();

        foreach ($exhibitor_type as $key => $value)
        {
            $tmp_exhibitor_type[$key]['old'] = $value['type_id'];
            unset($exhibitor_type[$key]['type_id']);
            $this->db->insert('exhibitor_type',$exhibitor_type[$key]);
            $tmp_exhibitor_type[$key]['new'] = $this->db->insert_id();
        }
        
        //exibitor
        $this->db->select('*,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $exibitor =  $this->db->get('exibitor')->result_array();

        foreach ($exibitor as $key => $value)
        {
            $k_r = array_search($value['et_id'], array_column($tmp_exhibitor_type, 'old'));
            $exibitor[$key]['et_id'] = $tmp_exhibitor_type[$k_r]['new'];
            $tmp_exibitor[$key]['old'] = $value['Id'];
            unset($exibitor[$key]['Id']);
            $this->db->insert('exibitor',$exibitor[$key]);
            $tmp_exibitor[$key]['new'] = $this->db->insert_id();
        }
        
        //event_countries
        $this->db->select('country_id,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $event_countries = $this->db->get('event_countries')->result_array();

        if($event_countries) $this->db->insert_batch('event_countries',$event_countries);
        
        //exhibitor_category
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $exhibitor_category =  $this->db->get('exhibitor_category')->result_array();

        foreach ($exhibitor_category as $key => $value)
        {
            $tmp_exhibitor_category[$key]['old'] = $value['id'];
            unset($exhibitor_category[$key]['id']);
            $this->db->insert('exhibitor_category',$exhibitor_category[$key]);
            $tmp_exhibitor_category[$key]['new'] = $this->db->insert_id();
        }
        
        foreach ($tmp_exhibitor_category as $key => $value)
        {   
            $this->db->select('parent_category_id,exibitor_category_id');
            $this->db->where('parent_category_id',$value['old']);
            $res = $this->db->get('exibitor_category_relation')->result_array();
            foreach ($res as $k => $v)
            {   
                $k_r = array_search($v['parent_category_id'], array_column($tmp_exhibitor_category, 'old'));
            	$insert['parent_category_id'] = $tmp_exhibitor_category[$k_r]['new'];
                $k_r = array_search($v['exibitor_category_id'], array_column($tmp_exhibitor_category, 'old'));
                $insert['exibitor_category_id'] = $tmp_exhibitor_category[$k_r]['new'];
                $this->db->insert('exibitor_category_relation',$insert);
            }
        }
        //exhi_category_group
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $exhi_category_group = $this->db->get('exhi_category_group')->result_array();
        foreach ($exhi_category_group as $key => $value)
        {
            $tmp_exhi_category_group[$key]['old'] = $value['id'];
            unset($exhi_category_group[$key]['id']);
            $this->db->insert('exhi_category_group',$exhi_category_group[$key]);
            $tmp_exhi_category_group[$key]['new'] = $this->db->insert_id();
        }
        foreach ($tmp_exhi_category_group as $key => $value)
        {
            $this->db->select('p_c_id,c_c_id,'.$event_id.' as event_id,'.$value['new'].' as group_id');
            $this->db->where('group_id',$value['old']);
            $exhi_category_group_relation = $this->db->get('exhi_category_group_relation')->result_array();
            foreach ($exhi_category_group_relation as $k => $v)
            {
                $k_r = array_search($v['p_c_id'], array_column($tmp_exhibitor_category, 'old'));
                $exhi_category_group_relation[$k]['p_c_id'] = $tmp_exhibitor_category[$k_r]['new'];
                $k_r = array_search($v['c_c_id'], array_column($tmp_exhibitor_category, 'old'));
                $exhi_category_group_relation[$k]['c_c_id'] = $tmp_exhibitor_category[$k_r]['new'];
                $this->db->insert('exhi_category_group_relation',$exhi_category_group_relation[$k]);
            }
        }
    }

    public function copyActivity($old_event_id,$event_id)
    {   
        //activity_permission
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $activity_permission = $this->db->get('activity_permission')->row_array();
        unset($activity_permission['id']);
        $this->db->insert('activity_permission',$activity_permission);

        //internal Feeds

        //activity_advert
        $this->db->select('title,image,url,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $activity_advert = $this->db->get('activity_advert')->result_array();
        if($activity_advert) $this->db->insert_batch('activity_advert',$activity_advert);

        if (!file_exists('./assets/activity_advert/'.$event_id)) {
        mkdir('./assets/activity_advert/'.$event_id, 0777, true);
        }
        foreach ($activity_advert as $key => $value)
        {   
            copy('./assets/activity_advert/'.$old_event_id.'/'.$value['image'],'./assets/activity_advert/'.$event_id.'/'.$value['image']);
        }

        //activity_survey
        $this->db->select('*,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $activity_survey = $this->db->get('activity_survey')->result_array();

        foreach ($activity_survey as $key => $value)
        {
            $tmp_activity_survey[$key]['old'] = $value['Id'];
            unset($activity_survey[$key]['Id']);
            $this->db->insert('activity_survey',$activity_survey[$key]);
            $tmp_activity_survey[$key]['new'] = $this->db->insert_id();
        }
        //activity_survey_ans
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $activity_survey_ans = $this->db->get('activity_survey_ans')->result_array();

        foreach ($activity_survey_ans as $key => $value)
        {
            $k_r = array_search($value['question_id'], array_column($tmp_activity_survey, 'old'));
            unset($activity_survey_ans[$key]['id']);
            $activity_survey_ans[$key]['question_id'] = $tmp_activity_survey[$k_r]['new'];
            $this->db->insert('activity_survey_ans',$activity_survey_ans[$key]);
        }


        //event_settings | socialwall_heading
        $this->db->select('socialwall_heading');
        $this->db->where('event_id',$old_event_id);
        $res = $this->db->get('event_settings')->row_array();

        if($this->db->where('event_id',$event_id)->get('event_settings')->row_array())
        {
            $this->db->where('event_id',$event_id)->update('event_settings',$res);
        }
        else
        {   
            $res['event_id'] = $event_id;
            $this->db->insert('event_settings',$res);
        }

        //
    }

    public function copyLeadRetrival($old_event_id,$event_id)
    {
        //role

        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $role = $this->db->get('role')->result_array();
        $role_ids = array_column($role,'Id');

        foreach ($role as $key => $value)
        {
            $tmp_role[$key]['old'] = $value['Id'];
            unset($role[$key]['Id']);
            $this->db->insert('role',$role[$key]);
            $tmp_role[$key]['new'] = $this->db->insert_id();
        }

        //relation_event_user copy based on role change new role id before insert
        $this->db->select('User_id,Organisor_id,Role_id,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $this->db->where_in('Role_id',$role_ids);
        $relation_event_user = $this->db->get('relation_event_user')->result_array();

        foreach ($relation_event_user as $key => $value)
        {
            $k_r = array_search($value['Role_id'], array_column($tmp_role, 'old'));
            $relation_event_user[$key]['Role_id'] = $tmp_role[$k_r]['new'];
            $this->db->insert('relation_event_user',$relation_event_user[$key]);
        }    

        //exibitor_question
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $exibitor_question = $this->db->get('exibitor_question')->result_array();

        foreach ($exibitor_question as $key => $value)
        {
            $tmp_exibitor_question[$key]['old'] = $value['q_id'];
            unset($exibitor_question[$key]['q_id']);
            $this->db->insert('exibitor_question',$exibitor_question[$key]);
            $tmp_exibitor_question[$key]['new'] = $this->db->insert_id();
        }
        // $tmp_exibitor_question = array ( 0 => array ( 'old' => '633', 'new' => 634, ), );
        
        // user_representatives
        $this->db->select('user_id,rep_id,status,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $user_representatives = $this->db->get('user_representatives')->result_array();
        if($user_representatives) $this->db->insert_batch('user_representatives',$user_representatives);
        //exibitor_question_answer

        $this->db->select('exibitor_questionid,exhi_id,rep_id,user_id,Answer,answer_comment,'.$event_id.' as Event_id');
        $this->db->where('event_id',$old_event_id);
        $exibitor_question_answer = $this->db->get('exibitor_question_answer')->result_array();

        foreach ($exibitor_question_answer as $key => $value)
        {
            $k_r = array_search($value['exibitor_questionid'], array_column($tmp_exibitor_question, 'old'));
            $exibitor_question_answer[$key]['exibitor_questionid'] = $tmp_exibitor_question[$k_r]['new'];
            $this->db->insert('exibitor_question_answer',$exibitor_question_answer[$key]);
        }    
        //exibitor_lead
        $this->db->select('exibitor_user_id,rep_id,lead_user_id,firstname,lastname,email,title,company_name,salutation,country,mobile,badgeNumber,custom_column_data,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $exibitor_lead = $this->db->get('exibitor_lead')->result_array();
        if($exibitor_lead) $this->db->insert_batch('exibitor_lead',$exibitor_lead);
    }
    public function copyAgenda($old_event_id=1763,$event_id=1511)
    {     
        //agenda_categories ** //agenda_category
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $agenda_categories = $this->db->get('agenda_categories')->result_array();
        
        foreach ($agenda_categories as $key => $value)
        {
            $tmp_agenda_categories[$key]['old'] = $value['Id'];
            unset($agenda_categories[$key]['Id']);
            $this->db->insert('agenda_categories', $agenda_categories[$key]);
            $tmp_agenda_categories[$key]['new'] = $this->db->insert_id();      
        }
        //var_export($tmp_agenda_categories);
        
        
        //session_types
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $session_types = $this->db->get('session_types')->result_array();
        
        foreach ($session_types as $key => $value)
        {
            $tmp_session_types[$key]['old'] = $value['type_id'];
            unset($session_types[$key]['type_id']);
            $this->db->insert('session_types', $session_types[$key]);
            $tmp_session_types[$key]['new'] = $this->db->insert_id();      
        }
        
        // agenda

        $this->db->select('*,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $agenda = $this->db->get('agenda')->result_array();
        
        //speaker document presenataion qa-session map survey *******************************

        $copyAppData = $this->getCopyAppData($event_id);
        
        //get old new session ids and location ids

        foreach ($agenda as $key => $value)
        {
            $tmp_agenda[$key]['old'] = $value['Id'];
            unset($agenda[$key]['Id']);
            $k_r = array_search($value['Types'], array_column($tmp_session_types, 'old'));
            $agenda[$key]['Types'] = $tmp_session_types[$k_r]['new'];
            
            if(!empty($value['document_id']))
            {
                $k_r = array_search($value['document_id'], array_column($copyAppData['document'], 'old'));
                $agenda[$key]['document_id'] = $copyAppData['document'][$k_r]['new'];
            }

            if(!empty($value['qasession_id']))
            {
                $k_r = array_search($value['qasession_id'], array_column($copyAppData['qa'], 'old'));
                $agenda[$key]['qasession_id'] = $copyAppData['qa'][$k_r]['new'];
            }

            if(!empty($value['survey_id']))
            {
                $k_r = array_search($value['survey_id'], array_column($copyAppData['survey'], 'old'));
                $agenda[$key]['survey_id'] = $copyAppData['qa'][$k_r]['new'];
            }

            $this->db->insert('agenda', $agenda[$key]);
            $tmp_agenda[$key]['new'] = $this->db->insert_id();      
        }
        $save['agenda_category'] = json_encode($tmp_agenda_categories);
        $save['agenda'] = json_encode($tmp_agenda);
        $this->saveCopyAppVar($event_id,$save);

        //agenda_category_relation
        foreach ($tmp_agenda_categories as $key => $value)
        {
            $this->db->select('agenda_id,'.$value['new'].' as category_id');
            $this->db->where('category_id',$value['old']);
            $agenda_category_relation = $this->db->get('agenda_category_relation')->result_array();
            foreach ($agenda_category_relation as $k => $v)
            {
                $k_r = array_search($v['agenda_id'], array_column($tmp_agenda, 'old'));
                $agenda_category_relation[$k]['agenda_id'] = $tmp_agenda[$k_r]['new'];
                $this->db->insert('agenda_category_relation', $agenda_category_relation[$k]);
            }
        }

        //modules_group*
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','1');
        $group = $this->db->get('modules_group')->result_array();
        
        foreach ($group as $key => $value)
        {   
            $tmp_group[$key]['old'] = $value['module_group_id'];
            unset($group[$key]['module_group_id']);
            $this->db->insert('modules_group',$group[$key]);
            $tmp_group[$key]['new'] = $this->db->insert_id();    
        }
        
        //modules_group_relation*
        foreach ($tmp_group as $key => $value)
        {   
            $this->db->select('menu_id,'.$value['new'].' as group_id,module_id');
            $this->db->where('group_id',$value['old']);
            $rel = $this->db->get('modules_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['module_id'], array_column($tmp_agenda_categories, 'old'));
                $rel[$k]['module_id'] = $tmp_agenda_categories[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('modules_group_relation',$rel);
        }
    }
    public function copyMap($old_event_id,$event_id)
    {   
        //map
        $this->db->select('*,'.$event_id.' as Event_id');
        $this->db->where('Event_id',$old_event_id);
        $map = $this->db->get('map')->result_array();
        
        foreach ($map as $key => $value)
        {
            $tmp_map[$key]['old'] = $value['Id'];
            unset($map[$key]['Id']);
            $this->db->insert('map', $map[$key]);
            $tmp_map[$key]['new'] = $this->db->insert_id();      
        }
        //map_image_mapping

        $save['map'] = json_encode($tmp_map);
        $this->saveCopyAppVar($event_id,$save);

        //get old new session ids
        $copyAppData = $this->getCopyAppData($event_id);
        foreach ($tmp_map as $key => $value)
        {
            $this->db->select('coords,user_id,seq_no,session_id,location_id,'.$value['new'].' as map_id');
            $this->db->where('map_id',$value['old']);
            $map_image_mapping = $this->db->get('map_image_mapping')->result_array();
            foreach ($map_image_mapping as $k => $v)
            {   
                //get old new session ids and location ids
                if($v['session_id'])
                {   
                    $k_r = array_search($v['session_id'], array_column($copyAppData['agenda'], 'old'));
                    $map_image_mapping[$k]['session_id'] = $copyAppData['agenda'][$k_r]['new'];            
                }
                elseif ($v['location_id']) {
                    $k_r = array_search($v['location_id'], array_column($copyAppData['meeting_location'], 'old'));
                    $map_image_mapping[$k]['location_id'] = $copyAppData['meeting_location'][$k_r]['new'];   
                }
                $this->db->insert('map_image_mapping', $map_image_mapping[$k]);
            }
        }
        
        //modules_group*
        $this->db->select('*,'.$event_id.' as event_id');
        $this->db->where('event_id',$old_event_id);
        $this->db->where('menu_id','10');
        $group = $this->db->get('modules_group')->result_array();
        
        foreach ($group as $key => $value)
        {   
            $tmp_group[$key]['old'] = $value['module_group_id'];
            unset($group[$key]['module_group_id']);
            $this->db->insert('modules_group',$group[$key]);
            $tmp_group[$key]['new'] = $this->db->insert_id();    
        }
        
        //modules_group_relation*
        foreach ($tmp_group as $key => $value)
        {   
            $this->db->select('menu_id,'.$value['new'].' as group_id,module_id');
            $this->db->where('group_id',$value['old']);
            $rel = $this->db->get('modules_group_relation')->result_array();
            foreach ($rel as $k => $v)
            {   
                $k_r = array_search($v['module_id'], array_column($tmp_map, 'old'));
                $rel[$k]['module_id'] = $tmp_map[$k_r]['new'];
            }
            if($rel) $this->db->insert_batch('modules_group_relation',$rel);
        }
    }
    public function saveCopyAppVar($event_id,$data)
    {   
        $this->db->where('event_id',$event_id);
        $res = $this->db->get('copy_app_tmp_var')->row_array();
        if($res)
        {
            $this->db->where('event_id',$event_id)->update('copy_app_tmp_var',$data);
        }
        else
        {   
            $data['event_id'] = $event_id;
            $this->db->insert('copy_app_tmp_var',$data);
        }
    }
    public function getCopyAppData($event_id)
    {
        $this->db->where('event_id',$event_id);
        $res = $this->db->get('copy_app_tmp_var')->row_array();
        $res['agenda'] = json_decode($res['agenda'],true);
        $res['meeting_location'] = json_decode($res['meeting_location'],true);
        $res['document'] = json_decode($res['document'],true);
        $res['qa'] = json_decode($res['qa'],true);
        $res['map'] = json_decode($res['map'],true);
        $res['survey'] = json_decode($res['survey'],true);
        $res['presentation'] = json_decode($res['presentation'],true);
        $res['agenda_category'] = json_decode($res['agenda_category'],true);
        return $res;
    }

    public function copyMatchmaking($old_event_id,$event_id)
    {
        $this->db->where('event_id',$old_event_id);
        $matchmaking_modules = $this->db->get('matchmaking_modules')->row_array();
        unset($matchmaking_modules['id']);
        $matchmaking_modules['event_id'] = $event_id;
        $this->db->insert('matchmaking_modules',$matchmaking_modules);

        $copyAppData = $this->getCopyAppData($event_id);

        $this->db->where('event_id',$old_event_id);
        $matchmaking_rules = $this->db->get('matchmaking_rules')->result_array();
        foreach ($matchmaking_rules as $key => $value) {
            unset($matchmaking_rules[$key]['id']);
            $matchmaking_rules[$key]['event_id'] = $event_id;
            if($value['profile_type_then'] == '1')
            {
                if ($value['field_then']) {
                    $k_r = array_search($value['field_then'], array_column($copyAppData['agenda_category'], 'old'));
                    $matchmaking_rules[$key]['field_then'] = $copyAppData['agenda_category'][$k_r]['new'];
                }

            }
        }
        if($matchmaking_rules) $this->db->insert_batch('matchmaking_rules',$matchmaking_rules);
    }

    public function copyNotes($old_event_id,$event_id)
    {

        $this->db->where('Event_id', $old_event_id);
        $notes = $this->db->get('notes')->result_array();
        foreach ($notes as $key => $value) {
            unset($notes[$key]['Id']);
            $notes[$key]['Event_id'] = $event_id;
        }
        if ($notes) {
            $this->db->insert_batch('notes', $notes);
        }

    }
    public function tmp_2026_exhi()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.showoff.asp.events/public/exhibitor?limit=500",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic QzU1OTY5NkNDQkYzQjNGQzNCOEJEMzkwNUZFODBCNUE5MEE5ODU3QjpTS2hXaEtkQlFTNjY4R3ZB",
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true)[0]['exhibitor'];
            foreach ($response as $key => $value) {

                $data['tmp_id'] = $value['CompanyId'];
                $data['Heading'] = $value['Company'];
                $data['Description'] = htmlentities($value['description']);
                $data['stand_number'] = $value['stand'];
                $data['company_logo'] = $value['logo'];
                
                $data['Short_desc'] = implode(',',array_column($value['category'],'code'));
                $data['country_id'] = $value['Country'] ? $this->getCountryIdFromName($value['Country']) : ''; 
                
                $data['website_url'] = $value['website'];
                $data['linkedin_url'] = $value['social_linkedin'];
                $data['youtube_url'] = $value['social_youtube'];
                $data['facebook_url'] = $value['social_facebook'];
                $data['twitter_url'] = $value['social_twitter'];
                $data['instagram_url'] = $value['social_instagram'];
                
                $data['Email'] = $value['email'] ?: $value['CompanyId'].'@venturiapps.com';
                $insert[] = $data;
            }
            $this->db->insert_batch('tmp_2026_exhi',$insert);
            echo count($insert).' Exhi. Added To tmp\n';
            j($insert);
        }
    }
    public function add_2026_exhi()
    {   
        $event_id = '2026';
        $org_id = '478777';
        $res = $this->db->where('Email IS NOT NULL')
                        ->limit('50')
                        ->get('tmp_2026_exhi')
                        ->result_array();

        foreach ($res as $key => $value)
        {   
            extract($value);
            if(!empty($Email) && !empty($Heading))
            {
                $user['Company_name'] = $Heading;
                $user['Email'] = $Email;
                $user['Organisor_id'] = $org_id;
                $user['Created_date'] = date('Y-m-d H:i:s');
                $user['Active'] = '1';
                $user['updated_date'] = date('Y-m-d H:i:s');
                $user['is_from_api'] = '1';
                $user['Country'] = $country_id;
                $user_id = $this->addExhiUser($user,$event_id);
    
                $ex_data['Event_id'] = $event_id;
                $ex_data['Organisor_id'] = $org_id;
                
                $ex_data['Heading'] = $Heading;
                $ex_data['Description'] = $Description;
                $ex_data['stand_number'] = $stand_number;
                
                $ex_data['Short_desc'] = $Short_desc;
                $ex_data['country_id'] = $country_id?:NULL;
                
                $ex_data['website_url'] = $website_url;
                $ex_data['facebook_url'] = $facebook_url;
                $ex_data['twitter_url'] = $twitter_url;
                $ex_data['linkedin_url'] = $linkedin_url;
                $ex_data['instagram_url'] = $instagram_url;
                $ex_data['youtube_url'] = $youtube_url;

                if(!empty($value['company_logo'])){   
                    $logoname="2024_exhi_logo".uniqid().'.png';
                    copy($value['company_logo'],"./assets/user_files/".$logoname);
                    $ex_data['company_logo']=json_encode(array($logoname));
                }
                else
                    $ex_data['company_logo']=NULL;
                
                $ex_data['user_id'] = $user_id;
                $this->add_exhibitor($ex_data);
                $this->db->where('tmp_id',$tmp_id)->delete('tmp_2026_exhi');
            }
        }
        updateModuleDate($event_id,'exhibitor');
        $tmp_2026_exhi = $this->db->get('tmp_2026_exhi')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $tmp_2026_exhi . " Refresh Page -> " . round($tmp_2026_exhi / 50) . " Time";
        j($msg);
    }
    public function tmp_2026_spr()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.showoff.asp.events/public/speakers?l=1000",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic QzU1OTY5NkNDQkYzQjNGQzNCOEJEMzkwNUZFODBCNUE5MEE5ODU3QjpTS2hXaEtkQlFTNjY4R3ZB",
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
            foreach ($response as $key => $value)
            {   
                if($value['Sites'][0]['Data']['Uuid'])
                {
                    $data['Email'] = $value['Sites'][0]['Data']['Uuid'].'@venturiapps.com';
                    $data['Firstname'] = $value['Name'];
                    $data['Company_name'] = $value['Company']['Name'];
                    $data['Title'] = $value['JobTitle'];
                    $data['Speaker_desc'] = htmlentities($value['Sites'][0]['Data']['Description']);
                    $data['Logo'] = 'https://internetretailingexpo.com/__resource/peopleProfiles/'.$value['ProfileImg'];
                    $data['knect_api_user_id'] = $value['Sites'][0]['Data']['Uuid'];
                    $insert[] = $data;
                }
            }
            $this->db->insert_batch('tmp_2026_spr',$insert);
        }
    }

    public function add_2026_spr()
    {
        $org_id = 478777;
        $eid = 2026;
        $temp_speakers=$this->db->select('*')->from('tmp_2026_spr')->limit(100)->get()->result_array();
        foreach ($temp_speakers as $key => $value) 
        {   
            $userdata=$this->db->get_where('user',array('Email'=>trim($value['Email'])))->row_array();
            $user_data=$value;
            if(!empty($value['Logo']))
            {   
                $logoname="2026_spr_logo".uniqid().'.png';
                copy($value['Logo'],"./assets/user_files/".$logoname);
                $user_data['Logo']=$logoname;
            }
            else
            {
                $user_data['Logo']=NULL;
            }
            $user_data['Active']='1';
            $user_data['Organisor_id']=$org_id;

            if(!empty($userdata))
            {
                $user_id=$userdata['Id'];
                $user_data['updated_date']=date('Y-m-d H:i:s');
                $this->db->where($userdata);
                $this->db->update('user',$user_data);
            }
            else
            {   
                $user_data['Created_date']=date('Y-m-d H:i:s');
                $this->db->insert('user',$user_data);
                $user_id=$this->db->insert_id();
            }
            unset($userdata);
            unset($user_data);

            $reldata=$this->db->get_where('relation_event_user',array('Event_id'=>$eid,'User_id'=>$user_id,'Role_id' => 7))->row_array();
            if(empty($reldata))
            {
                $rel_data['Event_id']=$eid;
                $rel_data['User_id']=$user_id;
                $rel_data['Organisor_id']=$org_id;
                $rel_data['Role_id']=7;
                $this->db->insert('relation_event_user',$rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('Email',$value['Email'])->delete('tmp_2026_spr');
        }
        $total_exhi = $this->db->select()->from('tmp_2026_spr')->get()->num_rows();
        echo "Success -> Remaning Speakers. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }

    public function tmp_2026_session_1()
    {   
        //https: //api.showoff.asp.events/public/seminars/568522BD-5056-B731-4C7044B3BD558ABF/sessions
        // 2167

        //https: //api.showoff.asp.events/public/seminars/68002A2F-5056-B733-49B9C660CFDF7B70/sessions
        //2166

        show_errors();
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.showoff.asp.events/public/seminars/68002A2F-5056-B733-49B9C660CFDF7B70/sessions?l=500",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic QzU1OTY5NkNDQkYzQjNGQzNCOEJEMzkwNUZFODBCNUE5MEE5ODU3QjpTS2hXaEtkQlFTNjY4R3ZB",
            "cache-control: no-cache"),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response,true);
            foreach($response as $key => $value)
            {
                $agenda['tmp_id'] = '2026-' . $value['Uuid'];
                $agenda['StartDate'] = empty($value['StartDate']) ? '' : date('Y-m-d', strtotime($value['StartDate']));
                $agenda['EndDate'] = empty($value['EndDate']) ? '' : date('Y-m-d', strtotime($value['EndDate']));
                $agenda['StartTime'] = empty($value['LocalStartTime']) ? '' : $value['LocalStartTime'];
                $agenda['EndTime'] = empty($value['LocalEndTime']) ? '' : $value['LocalEndTime'];
                $agenda['Name'] = empty($value['Name']) ? '' : $value['Name'];
                $agenda['Type'] = empty($value['Streams'][0]['Name']) ? 'Sessions' : $value['Streams'][0]['Name'];
                $agenda['Description'] = empty($value['Abstract']) ? '' : $value['Abstract'];
                $agenda['Location'] = empty($value['Location']['Name']) ? '' : $value['Location']['Name'];
                if (!empty($value['Speakers'])) {
                    $temp_speaker = array_filter(array_column_1($value['Speakers'], 'Name'));
                    $agenda['Speakers'] = empty($value['Speakers']) ? '' : implode(',', $temp_speaker);
                    $temp_speaker_ids = array_filter(array_column_1($value['Speakers'], 'Uuid'));
                    if(!empty($temp_speaker_ids))
                    {
                        $this->db->select('group_concat(Id) as user_id');
                        $this->db->where_in('knect_api_user_id',$temp_speaker_ids);
                        $res = $this->db->get('user')->row_array();
                        $agenda['Speaker_id'] = $res['user_id'];
                    }
                } else {
                    $agenda['Speakers'] = '';
                }
                unset($res);
                unset($temp_speaker_ids);
                $this->db->insert('tmp_2026_agenda',$agenda);
                unset($agenda);
            }
        }
    }

    public function add_2026_session()
    {
        $org_id = 478777;
        //2167 - confer
        //2166 - Workshops
        $Agenda_id = 2166;
        $eid = 2026;
        $temp_session = $this->db->select('*')->from('tmp_2026_agenda')->limit(50)->get()->result_array();
        foreach ($temp_session as $key => $value) {
            $session_data['Start_date'] = $value['StartDate'];
            $session_data['Start_time'] = $value['StartTime'];
            $session_data['End_date'] = $value['EndDate'];
            $session_data['End_time'] = $value['EndTime'];
            $session_data['Heading'] = trim($value['Name']);
            $session_data['description'] = $value['Description'];
            $session_data['custom_location'] = $value['Location'];
            $session_data['custom_speaker_name'] = $value['Speakers'];
            $session_data['Speaker_id'] = $value['Speaker_id'];
            $session_data['show_rating'] = '1';
            $session_data['allow_clashing'] = '1';
            $session_data['allow_comments'] = '1';
            $typedata = $this->db->get_where('session_types', array(
                'type_name' => trim($value['Type']),
                'event_id' => $eid,
            ))->row_array();
            if (!empty($typedata)) {
                $type_id = $typedata['type_id'];
            } else {
                $type_data['type_name'] = trim($value['Type']);
                $type_data['event_id'] = $eid;
                $type_data['created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('session_types', $type_data);
                $type_id = $this->db->insert_id();
            }
            $session_data['Types'] = $type_id;
            $sessiondata = $this->db->select('*')->from('agenda')->where('Event_id', $eid)->where('agenda_code', trim($value['tmp_id']))->get()->row_array();
            if (!empty($sessiondata)) {
                $this->db->where('Event_id', $eid)->where('agenda_code', trim($value['tmp_id']))->update('agenda', $session_data);
                $session_id = $sessiondata['Id'];
            } else {
                $session_data['Event_id'] = $eid;
                $session_data['Organisor_id'] = $org_id;
                $session_data['agenda_code'] = trim($value['tmp_id']);
                $session_data['Agenda_status'] = '1';
                $session_data['created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('agenda', $session_data);
                $session_id = $this->db->insert_id();
            }
            unset($sessiondata);
            unset($session_data);
            $reldata = $this->db->get_where('agenda_category_relation', array(
                'agenda_id' => $session_id,
                'category_id' => $Agenda_id,
            ))->row_array();
            if (empty($reldata)) {
                $rel_data = array(
                    'agenda_id' => $session_id,
                    'category_id' => $Agenda_id,
                );
                $this->db->insert('agenda_category_relation', $rel_data);
            }
            unset($reldata);
            unset($rel_data);
            $this->db->where('tmp_id', $value['tmp_id'])->delete('tmp_2026_agenda');
        }
        $total_exhi = $this->db->select()->from('tmp_2026_agenda')->get()->num_rows();
        echo "Success -> Remaning Agenda. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        j('agenda added successfully');

    }

    public function add_attendees_2014($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $tmp_2014_attendee = $this->db->limit('50')->get('tmp_2014_attendee')->result_array();
                break;            
            case 2:
                $tmp_2014_attendee = $this->db->limit('50','100')->get('tmp_2014_attendee')->result_array();
                break;
            case 3:
                $tmp_2014_attendee = $this->db->limit('50','200')->get('tmp_2014_attendee')->result_array();
                break;
            case 4:
                $tmp_2014_attendee = $this->db->limit('50','300')->get('tmp_2014_attendee')->result_array();
                break;
            case 5:
                $tmp_2014_attendee = $this->db->limit('50','400')->get('tmp_2014_attendee')->result_array();
                break;
            case 6:
                $tmp_2014_attendee = $this->db->limit('50','500')->get('tmp_2014_attendee')->result_array();
                break;
            case 7:
                $tmp_2014_attendee = $this->db->limit('50','600')->get('tmp_2014_attendee')->result_array();
                break;
            case 8:
                $tmp_2014_attendee = $this->db->limit('50','700')->get('tmp_2014_attendee')->result_array();
                break;
            case 9:
                $tmp_2014_attendee = $this->db->limit('50','800')->get('tmp_2014_attendee')->result_array();
                break;
            case 10:
                $tmp_2014_attendee = $this->db->limit('50','900')->get('tmp_2014_attendee')->result_array();
                break;
        }
        
        $event_id = 2014;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($tmp_2014_attendee as $key => $value)
        {   
            $this->db->select('u.*,reu.Role_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('u.Email',$value['Email']);
            $role = $this->db->get('user u')->row_array();
            if(empty($role) || $role['Role_id'] != '3')
            {   
                unset($role['Role_id']);
                $user = $role;
                $Email = $value['Email'];
                $data = $value;
                $data['Password'] = md5($value['Password']);
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }

                $this->db->where($value);
                $this->db->delete('tmp_2014_attendee');
            }
        }
        $tmp_2014_attendee = $this->db->get('tmp_2014_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $tmp_2014_attendee . " Refresh Page -> " . round($tmp_2014_attendee / 50 ) . " Time";
        j($msg);
    }
    public function add_attendees_2152($page_no=1)
    {   
        $this->load->model('Native_single_fcm/App_login_model');
        switch ($page_no)
        {
            case 1:
                $tmp_2152_attendee = $this->db->limit('50')->get('tmp_2152_attendee')->result_array();
                break;            
            case 2:
                $tmp_2152_attendee = $this->db->limit('50','100')->get('tmp_2152_attendee')->result_array();
                break;
            case 3:
                $tmp_2152_attendee = $this->db->limit('50','200')->get('tmp_2152_attendee')->result_array();
                break;
            case 4:
                $tmp_2152_attendee = $this->db->limit('50','300')->get('tmp_2152_attendee')->result_array();
                break;
            case 5:
                $tmp_2152_attendee = $this->db->limit('50','400')->get('tmp_2152_attendee')->result_array();
                break;
            case 6:
                $tmp_2152_attendee = $this->db->limit('50','500')->get('tmp_2152_attendee')->result_array();
                break;
            case 7:
                $tmp_2152_attendee = $this->db->limit('50','600')->get('tmp_2152_attendee')->result_array();
                break;
            case 8:
                $tmp_2152_attendee = $this->db->limit('50','700')->get('tmp_2152_attendee')->result_array();
                break;
            case 9:
                $tmp_2152_attendee = $this->db->limit('50','800')->get('tmp_2152_attendee')->result_array();
                break;
            case 10:
                $tmp_2152_attendee = $this->db->limit('50','900')->get('tmp_2152_attendee')->result_array();
                break;
        }
        
        $event_id = 2152;
        $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
        foreach ($tmp_2152_attendee as $key => $value)
        {   
            if(empty($value['Email']))
            {
                $Email = $event_id.'-'.$value['Unique_no'].'@venturiapps.com';
            }
            else
            {
                $Email = $value['Email'];
            }
            $this->db->select('u.*,reu.Role_id');
            $this->db->join('relation_event_user reu','reu.User_id = u.Id');
            $this->db->where('u.Email',$Email);
            $role = $this->db->get('user u')->row_array();
            if(empty($role) || $role['Role_id'] != '3')
            {   
                unset($role['Role_id']);
                $user = $role;
                $data = $value;
                $data['Email'] = $Email;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                }

                $this->db->where($value);
                $this->db->delete('tmp_2152_attendee');
            }
        }
        $tmp_2152_attendee = $this->db->get('tmp_2152_attendee')->num_rows();
        $msg = "Success -> Remaning Users. -> " . $tmp_2152_attendee . " Refresh Page -> " . round($tmp_2152_attendee / 50 ) . " Time";
        j($msg);
    }

    public function addAttendee2047()
    {   
        //AUOPC4681K
        $insert['text'] = file_get_contents('php://input');
        // $insert['text'] = $entityBody;
        $this->db->insert('webhook', $insert);
    }
    public function testj()
    {
        $entityBody = '{"eventType":"ContactCreated","messageTime":"2019-02-26T05:41:14.660Z","messageStub":"2559a80c-8b3d-470c-8600-ed162ac3ee6b","message":[{"contactStub":"7D7BE628-F4F9-49AC-BC34-DBE007DDEFB7","lastName":"Havens","workStateCode":"","contactSourceId":"","gender":"","prefix":"","customFields":[{"id":"773FBFB2-60B5-4C06-AB19-BFCCC7EB13F8","name":"Type of Company","value":""},{"id":"B7B8CD1B-65F2-49F3-BA7F-259094ED0F23","name":"Company Type","value":""},{"id":"F33D5D4C-DFA0-4890-8FE3-AA9322F4387B","name":"Company Type - RAFI","value":"Bio-Technology"},{"id":"0609AF72-FAB5-45A3-BCBF-B0186393DA40","name":"Waste Company Type","value":""},{"id":"9FE24151-3EDF-49CE-9CAD-BAC54511970B","name":"Organisation Type","value":""},{"id":"E4C19C3B-2BC5-46A9-A202-6422E9D2D740","name":"Contact Type","value":""}],"workZipcode":"","contactType":"Sponsor","workCountry":"USA","updatedDate":"2019-02-26","title":"Sustainability & Public Affairs Manager","homeStateCode":"","optedOut":"no","workState":"","workCity":"","homeCity":"","company":"Pivot Bio","accountStub":"7755FDED-FEB1-45DD-B7D3-C91CED313C27","email":"keira@pivotbio.com","homeZipcode":"","homeFax":"","ccEmail":"","nickName":"","homePhone":"","fullName":"Keira Havens","dateOfBirth":"","homeState":"","workFax":"","workAddress1":"","workAddress2":"","firstName":"Keira","homeAddress3":"","workAddress3":"","homeAddress2":"","workCountryCode":"US","homeAddress1":"","mobilePhone":"","middleName":"","workPhone":"877-495-3777","designation":"","homeCountry":""}]}';

        //$entityBody=$this->input->post('entityBody');
        $data = json_decode($entityBody, true);
        j($data);
        if ($data['eventType'] == 'ContactCreated')
        {
            $value['Firstname']=$data['message'][0]['firstName'];
            $value['Lastname']=$data['message'][0]['lastName'];
            $value['Email']=$data['message'][0]['email'];
            $value['workCountry']=$data['message'][0]['workCountry'];
            $value['Company_name']=$data['message'][0]['company'];
            $value['title']=$data['message'][0]['title'];//job title

            $value['State']=$data['message'][0]['workState'];
            $value['Postcode']=$data['message'][0]['homeZipcode'];
            $value['Suburb']=$data['message'][0]['homeAddress1'];
            $value['Mobile']=$data['message'][0]['mobilePhone'];
            $value['Phone_business']=$data['message'][0]['workPhone'];
            
            echo "<pre>";print_r($value);exit;
            

            $getAttendee = file_get_contents($data['api_url'] . '?token=SIHG3UU3LSYDBDZZWVHF');
            $getAttendee = json_decode($getAttendee, true);
            $value['Firstname'] = $getAttendee['answers'][array_search(16240571, array_column_1($getAttendee['answers'], 'question_id')) ]['answer'] ? : $getAttendee['profile']['first_name'];
            $value['Lastname'] = $getAttendee['answers'][array_search(16240572, array_column_1($getAttendee['answers'], 'question_id')) ]['answer'] ? : $getAttendee['profile']['last_name'];
            $value['Title'] = $getAttendee['profile']['job_title'] ? : '';
            $value['Email'] = $getAttendee['profile']['email'];
            $value['Company_name'] = $getAttendee['answers'][array_search(16240573, array_column_1($getAttendee['answers'], 'question_id')) ]['answer'];

            if (!empty($value['Email']) && !empty($value['Firstname']) && !empty($value['Lastname']))
            {
                $user = $this->db->where('Email', $value['Email'])->get('user')->row_array();
                $Email = $value['Email'];
                $data = $value;
                $data['is_from_api'] = '1';
                $data['Active'] = '1';
                if ($user)
                {
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', $Email);
                    $this->db->update('user', $data);
                    $cnt = $this->App_login_model->checkEmailAlreadyByEvent1($Email, $event_id);
                    if ($cnt == 0)
                    {
                        $this->App_login_model->setupUserWithEvent($user['Id'], $event_id, $org_id);
                    }
                    $user_id = $user['Id'];
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user', $data);
                    $user_id = $this->db->insert_id();
                    $this->App_login_model->setupUserWithEvent($user_id, $event_id, $org_id);
                }
            }
            header('Content-Type: application/json');
            $return['success'] = 200;
            $return['message'] = 'Attendee Added Successfully';
            $return['data'] = $value;
            echo json_encode($return, JSON_PRETTY_PRINT);
            exit();
        }
        echo "Bad Request";
        exit;
    }
}
/* End of file apiv4.php */
/* Location: ./application/controllers/apiv4.php */