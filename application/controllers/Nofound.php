<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notfound extends CI_Controller {
	
    public function __construct() 
    {
        parent::__construct(); 
    } 
	function _404()
	{
		echo "<h1>404</h1>";
	}
}