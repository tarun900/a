
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index()
	{
		$this->chk_login();
		$this->data['disp'] = "CodeIgniter";
		$this->template->write('pagetitle', 'Welcome');
		$this->template->write_view('content', 'welcome_message', $this->data , true);
		$this->template->render();
	}
	public function chk_login()
	{
		$logindetails = $this->Login_model->check_login();
		echo $logindetails;
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
