<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Speakers extends CI_Controller
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Speaker';
          $this->data['smalltitle'] = 'Speaker';
          $this->data['breadcrumb'] = 'Speaker';
          parent::__construct($this->data);
          $this->load->library('formloader');
           $this->load->library('formloader1');
          $this->load->model('Agenda_model');
          $this->template->set_template('front_template');
          $this->load->model('Event_template_model');
          $this->load->model('Cms_model');
          $this->load->model('Event_model');
          $this->load->model('Setting_model');
          $this->load->model('Speaker_model');
          $this->load->model('Profile_model');
          $this->load->model('Message_model');
          $user = $this->session->userdata('current_user');
          $eventname=$this->Event_model->get_all_event_name();
          if(!in_array($this->uri->segment(3),$eventname))
          {
              $Subdomain=$this->uri->segment(3);
              $flag=1;
              redirect(base_url().'Pageaccess/'.$Subdomain.'/'.$flag);
              //echo '<script>window.location.href="'.base_url().'Pageaccess/'.$Subdomain.'/'.$flag.'"</script>';
          }
     }

     public function index($acc_name = NULL,$Subdomain = NULL,$intFormId=NULL)
     {
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('7',$menu_list))
          {
            redirect(base_url().'Forbidden/');
            //echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          if ($this->data['pagetitle'] == "Speaker")
          {
              $title = "Speakers";
          }

          if ($user != '')
            {
                 $req_mod = $this->router->fetch_class();
                 if($req_mod=="Speakers")
                 {
                    $req_mod="Speaker";
                 }
                 $menu_id=$this->Event_model->get_menu_id($req_mod);
                 $current_date=date('Y/m/d');
                 $logged_in_user_id=$user[0]->Id;
                 $this->Event_model->add_view_hit($logged_in_user_id,$current_date,$menu_id,$eventid); 
              
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                 
                 $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                 if ($cnt == 1)
                 {
                      $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                      $this->data['notes_list'] = $notes_list;
                 }
                 else
                 {
                      $event_type=$event_templates[0]['Event_type'];
                      if($event_type==3)
                      {
                          $this->session->unset_userdata('current_user');
                          $this->session->unset_userdata('invalid_cred');
                          $this->session->sess_destroy();
                      }
                      else
                      {
                          $parameters = $this->uri->uri_to_assoc(1);
                          $Subdomain=$this->uri->segment(3);
                          $acc_name=$this->uri->segment(2);
                          redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                      }
                 }
            }
          $user = $this->session->userdata('current_user');
          $lid = $user[0]->Id;

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
          $this->data['notisetting']=$notificationsetting;

          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      
          $this->data['sign_form_data']=$res1;
          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;

          



//          if ($user[0]->Name == 'Client')
//          {

               $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
               //echo "<pre>";print_r($menu_list);die;
               for($i=0;$i<count($menu_list);$i++)
               {
                    if('Speakers'==$menu_list[$i]['pagetitle'])
                    {
                         $mid=$menu_list[$i]['id'];
                    }
               }
               $this->data['menu_id']=$mid;
               $this->data['menu_list'] = $menu_list;
               $eid=$event_templates[0]['Id'];
               $res=$this->Agenda_model->getforms($eid,$mid);
               $this->data['form_data']=$res;
//          }
//          else
//          {
//               $menu_list = $this->Event_template_model->get_menu_list($roleid,$event_templates[0]['Id']);
//               $this->data['menu_list'] = $menu_list;
//          }

          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;
          $user = $this->session->userdata('current_user');
          if(!empty($user[0]->Id)){
            $my_favorites=$this->Event_template_model->get_my_favorites_user_by_event_menu_id($event_templates[0]['Id'],$user[0]->Id,'7');
            $this->data['my_favorites']=$my_favorites;
          }
          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;

         if($event_templates[0]['key_people_sort_by']=='1')
          {
            $speakers = $this->Event_template_model->get_speaker_list_index($Subdomain);
          }
          else
          {
            $speakers = $this->Event_template_model->get_speaker_list_index_by_firstname($Subdomain);
          }
          $this->data['speakers'] = $speakers;
         
          $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
          $this->data['advertisement_images'] = $advertisement_images;

          $this->data['Subdomain'] = $Subdomain;
          $intFormBuild = $this->input->post('hdnFormBuild');
          if ($this->input->post())
          {
               if($intFormBuild==1)
               {
                   $array_temp_past = $this->input->post();
                    if(!empty($array_temp_past))
                    {
                     unset($_POST['hdnFormBuild']);
                    $aj=json_encode($this->input->post());
                    $formdata=array('f_id' =>$intFormId,
                    'm_id'=>$mid,
                    'user_id'=>$user[0]->Id,    
                    'event_id'=>$eid,
                    'json_submit_data'=>$aj
                    );
                    $this->Agenda_model->formsinsert($formdata);
                    redirect(base_url().'Speakers/'.$acc_name.'/'.$Subdomain);
                    //echo '<script>window.location.href="'.base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'"</script>';
                    exit;
                    }  
               }
               $data1 = array(
                       'Message' => $this->input->post('Message'),
                       'Sender_id' => $lid,
                       'Receiver_id' => $this->uri->segment(3),
                       'Parent' => '0',
                       'Time' => date("H:i:s"),
                       'ispublic' => $this->input->post('ispublic')
               );
               $this->Message_model->send_speaker_message($data1);

               $Sid = $this->uri->segment(3);
               $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
               $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $Sid);

               $string = '';
               $string.='<span style="font-size: 20px;margin-bottom: 20px;clear: left;display: block;">Messages with ' . $view_chats1[0]['Recivername'] . '<br/></span>';
               $user = $this->session->userdata('current_user');
               $lid = $user[0]->Id;

               foreach ($view_chats1 as $key => $value)
               {
                    if ($value['Sender_id'] == $lid)
                    {
                         echo '<strong>' . $value['Recivername'] . '</strong>';
                         echo '<br/>';
                         echo $value['Message'];
                         echo '<br/>';
                         echo $value['Time'];
                         echo '<br/>';
                         echo '<br/>';
                    }
                    else
                    {
                         echo '<strong>' . $value['Sendername'] . '</strong>';
                         echo '<br/>';
                         echo $value['Message'];
                         echo '<br/>';
                         echo $value['Time'];
                         echo '<br/>';
                         echo '<br/>';
                    }
               }

               echo $string;
               exit;
          }
          
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
          $this->template->write_view('header', 'frontend_files/header', $this->data, true);
          $this->template->write_view('js', 'frontend_files/js', $this->data, true);
          if ($event_templates[0]['Event_type'] == '1')
               {
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                      $this->template->write_view('content', 'Speakers/index', $this->data, true);
                      $this->template->write_view('footer', 'Speakers/footer', $this->data, true);
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '2')
               {
  
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                     $this->template->write_view('content', 'Speakers/index', $this->data, true);
                     $this->template->write_view('footer', 'Speakers/footer', $this->data, true);
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '3') 
               {
                  $this->session->unset_userdata('acc_name');
                  $acc['acc_name'] =  $acc_name;
                  $this->session->set_userdata($acc);
                  $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('7',$event_templates[0]['Id']);
                  if($isforcelogin['is_force_login']=='1' && empty($user))
                  {
                    $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
                  }
                  else
                  {
                    $this->template->write_view('content', 'Speakers/index', $this->data, true);
                    $this->template->write_view('footer', 'Speakers/footer', $this->data, true);
                  }
               }
               else
               {
                  if (empty($user))
                  {
                    $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                  }
                  else
                  {
                    $this->template->write_view('content', 'Speakers/index', $this->data, true);
                    $this->template->write_view('footer', 'Speakers/footer', $this->data, true);
                  }
               }
               $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
               $this->template->render();  
     }

     public function View($acc_name=NULL,$Subdomain = NULL, $id = null)
     {
          $user = $this->session->userdata('current_user');
          $event_val = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
          $eventid = $event_val[0]['Id'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          if(!in_array('7',$menu_list))
          {
            redirect(base_url().'Forbidden/');
            //echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';
          }
          if ($this->data['pagetitle'] == "Speaker")
          {
              $title = "Speakers";
          }

          if ($user != '')
            {
                 $req_mod = $this->router->fetch_class();
                 if($req_mod=="Speakers")
                 {
                      $req_mod="Speaker";
                 }
                 $menu_id=$this->Event_model->get_menu_id($req_mod);
                 $current_date=date('Y/m/d');
                 $logged_in_user_id=$user[0]->User_id;
                 $this->Event_model->add_view_hit($logged_in_user_id,$current_date,$menu_id); 
              
                 $parameters = $this->uri->uri_to_assoc(1);
                 $event_templates = $this->Event_template_model->get_event_template_by_id_list($this->uri->segment(3));
                 $roledata = $this->Event_model->getUserRole($event_templates[0]['Id']);
                 
                 $cnt=$this->Agenda_model->check_access($logged_in_user_id,$event_templates[0]['Id']);
                 if ($cnt == 1)
                 {
                      $notes_list = $this->Event_template_model->get_notes($this->uri->segment(3));
                      $this->data['notes_list'] = $notes_list;
                 }
                 else
                 {
                      $event_type=$event_templates[0]['Event_type'];
                      if($event_type==3)
                      {
                          $this->session->unset_userdata('current_user');
                          $this->session->unset_userdata('invalid_cred');
                          $this->session->sess_destroy();
                      }
                      else
                      {
                          $parameters = $this->uri->uri_to_assoc(1);
                          $Subdomain=$this->uri->segment(3);
                          $acc_name=$this->uri->segment(2);
                          redirect(base_url().'Unauthenticate/'.$acc_name.'/'.$Subdomain);
                      }
                 }
            }
          $notes_list = $this->Event_template_model->get_notes($Subdomain);
          $this->data['notes_list'] = $notes_list;

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;
          $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
          $this->data['fb_login_data'] = $fb_login_data;
          $fundraisingenbled=$this->Event_model->getfundraising_enabled($event_templates[0]['Id']);
               $this->data['linkdin_login']=$fundraisingenbled[0]['linkedin_login_enabled'];
          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
          $this->data['cms_menu'] = $cmsmenu;
          
          $this->data['subdomain'] = $Subdomain;
          $this->data['Subdomain'] = $Subdomain;
          $this->data['sp_id'] = $id;
          $this->data['Sid'] = $id;

          $user = $this->session->userdata('current_user');
          if($user[0]->Role_id==4)
          {
            $custom=$this->Event_model->get_user_custom_clounm($user[0]->Id,$event_templates[0]['Id']);
            $this->data['custom']=$custom;
          }
          
          
          
          $agenda_list=$this->Agenda_model->get_all_agenda_by_speaker_id($event_templates[0]['Id'],$id);
          $this->data['agenda_list']=$agenda_list;
          $this->template->write_view('css', 'frontend_files/css', $this->data, true);
            $this->template->write_view('header', 'frontend_files/header', $this->data, true);
            $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
            $this->template->write_view('js', 'frontend_files/js', $this->data, true);
            if ($event_templates[0]['Event_type'] == '1')
               {
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                        $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                        //$view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                         $view_chats1= $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$id);
                        $this->data['view_chats1'] = $view_chats1;

                        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                        $this->data['cms_menu'] = $cmsmenu;

                        $test = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);

                        $user_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['user_url'] = $user_url;

                        $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['social_url'] = $social_url;

                        if (!empty($test))
                        {
                             //echo 111; exit();
                             $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                             $this->data['social_url'] = $social_url;
                        }
                        else
                        {
                             //echo 222; exit();
                             $user_url = $this->Event_template_model->get_speaker_user_url($Subdomain, $id);
                             $this->data['user_url'] = $user_url;
                        }
                      $this->template->write_view('content', 'Speakers/user', $this->data, true);
                      
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '2')
               {
  
                  if (empty($user))
                  {
                      $this->template->write_view('content', 'registration/index', $this->data, true);
                  }
                  else
                  {
                       $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                       // $view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                        $view_chats1= $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$id);
                        $this->data['view_chats1'] = $view_chats1;

                        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                        $this->data['cms_menu'] = $cmsmenu;

                        $test = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);

                        $user_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['user_url'] = $user_url;

                        $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['social_url'] = $social_url;

                        if (!empty($test))
                        {
                             //echo 111; exit();
                             $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                             $this->data['social_url'] = $social_url;
                        }
                        else
                        {
                             //echo 222; exit();
                             $user_url = $this->Event_template_model->get_speaker_user_url($Subdomain, $id);
                             $this->data['user_url'] = $user_url;
                        }
                      $this->template->write_view('content', 'Speakers/user', $this->data, true);
                  }
               }
               elseif ($event_templates[0]['Event_type'] == '3') 
               {
                  $this->session->unset_userdata('acc_name');
                  $acc['acc_name'] =  $acc_name;
                  $this->session->set_userdata($acc);
                  $isforcelogin=$this->Event_model->get_force_login_enabled_by_menu_id('7',$event_templates[0]['Id']);
                  if($isforcelogin['is_force_login']=='1' && empty($user))
                  {
                    $this->template->write_view('content', 'registration/force_user_login', $this->data, true);
                  }
                  else
                  {
                    $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                    if(!empty($user))
                    {
                        //$view_chats1 = $this->Message_model->view_hangouts($dataevents[0]['Id'], $id);
                       $view_chats1= $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$id);
                        $this->data['view_chats1'] = $view_chats1;
                    }
                    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                    $this->data['cms_menu'] = $cmsmenu;

                    $test = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);

                    $user_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                    $this->data['user_url'] = $user_url;

                    $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                    $this->data['social_url'] = $social_url;

                    if (!empty($test))
                    {
                         //echo 111; exit();
                         $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                         $this->data['social_url'] = $social_url;
                    }
                    else
                    {
                         //echo 222; exit();
                         $user_url = $this->Event_template_model->get_speaker_user_url($Subdomain, $id);
                         $this->data['user_url'] = $user_url;
                    }
                    $this->template->write_view('content', 'Speakers/user', $this->data, true);
                  }
               }
               else
               {
                  if (empty($user))
                  {
                    $this->template->write_view('content', 'registration/authorized_user_login', $this->data, true);
                  }
                  else
                  {
                        $dataevents = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
                         $view_chats1= $this->Message_model->view_hangouts_private_msg1($dataevents[0]['Id'],0,5,null,$id);
                        $this->data['view_chats1'] = $view_chats1;

                        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
                        $this->data['cms_menu'] = $cmsmenu;

                        $test = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);

                        $user_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['user_url'] = $user_url;

                        $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                        $this->data['social_url'] = $social_url;

                        if (!empty($test))
                        {
                             $social_url = $this->Event_template_model->get_speaker_social_url($Subdomain, $id);
                             $this->data['social_url'] = $social_url;
                        }
                        else
                        {
                             $user_url = $this->Event_template_model->get_speaker_user_url($Subdomain, $id);
                             $this->data['user_url'] = $user_url;
                        }
                      $this->template->write_view('content', 'Speakers/user', $this->data, true);
                      
                  }
               }
               $this->template->render();
     }

     public function upload_imag($acc_name = NULL,$Subdomain = NULL, $id = null)
     {

          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          /*$settings= array( 'consumer_key' => 'D76iqflcQRxIjvJGqVJR1Q',
          'consumer_secret'=> 'psPJKrXrSR74gzMLjsx5FEVqIjhlGERk8NmT3gw8bY',
          'oauth_access_token'=> '616244226-G0fQHmVKN5uTPMZvslCNcjnBcg4hpSezXJdMXeAx',
          'oauth_access_token_secret'=> 'RSIhbvZIhfjrSI5drTYPDdhGkhpOAO0zbpoJcA6tzCVm5'); 
          $this->load->library('twitter_feeds');

          if(isset($_POST[('Message')]))
          {
              $url = 'https://api.twitter.com/1.1/search/tweets.json';
              $getfield = '?q='.$_POST['Message'].'&result_type=mixed&count=10';
              $requestmethod = 'GET';
              $twitter = new twitterapiexchange();
              $tweet= $twitter->setGetfield($getfield)
                       ->buildOauth($url, $requestmethod)
                       ->performRequest();
              $tweets= json_decode($tweet);
          }*/

          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {

               // $session_data = $this->session->userdata('logged_in');
               // $data1['email'] = $session_data['email'];
               // $this->load->view('Messages/chatpublic', $data1);
               // $settings= array( 'consumer_key' => 'D76iqflcQRxIjvJGqVJR1Q',
               // 'consumer_secret'=> 'psPJKrXrSR74gzMLjsx5FEVqIjhlGERk8NmT3gw8bY',
               // 'oauth_access_token'=> '616244226-G0fQHmVKN5uTPMZvslCNcjnBcg4hpSezXJdMXeAx', // Optional
               // 'oauth_access_token_secret'=> 'RSIhbvZIhfjrSI5drTYPDdhGkhpOAO0zbpoJcA6tzCVm5'); 
               // $this->load->library('twitter_feeds' );

               // if(isset($_POST[('searchbox')]))
               // {
               //     $url = 'https://api.twitter.com/1.1/search/tweets.json';
               //     $getfield = '?q='.$_POST['searchbox'].'&result_type=mixed&count=10';
               //     $requestmethod = 'GET';
               //     $twitter = new twitterapiexchange();
               //     $tweet= $twitter->setGetfield($getfield)
               //              ->buildOauth($url, $requestmethod)
               //              ->performRequest();
               //     $tweets= json_decode($tweet);
               // }


               define("MAX_SIZE", "200000");
               $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
               //$datafiles=array();
               $uploaddir = "assets/user_files/"; //a directory inside
               $thumbuploaddir = "assets/user_files/thumbnail/";
               foreach ($_FILES['photos']['name'] as $name => $value)
               {

                    $filename = stripslashes($_FILES['photos']['name'][$name]);
                    $size = filesize($_FILES['photos']['tmp_name'][$name]);
                    //get the extension of the file in a lower case format
                    $ext = $this->getExtension($filename);
                    $ext = strtolower($ext);

                    if (in_array($ext, $valid_formats))
                    {
                         if ($size < (MAX_SIZE * 1024))
                         {
                            $fnm=explode(".",$filename);
                              $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $filename=$filename.".".$fnm[1];
                              $image_name = time() . $filename;
                              echo "<li>
                                        <div class='msg_photo'>
                                             <input type='hidden' name='unpublished_photo[]' value='" . $image_name . "'>
                                             <img src='" . base_url() . $thumbuploaddir . $image_name . "' class='imgList'>
                                             <button class='photo_remove_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button>
                                        </div>
                                   </li>";
                                   $fnm=explode(".",$image_name);
                              $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $image_name=$image_name.".".$fnm[1];
                              $newname = $uploaddir . $image_name;
                              $thumbnewname = $thumbuploaddir . $image_name;

                              if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
                              {
                                   $time = time();
                                   $datafiles[] = $image_name;
                                   $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
                              }
                              else
                              {
                                   echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                              }
                         }
                         else
                         {
                              echo '<span class="imgList">You have exceeded the size limit!</span>';
                         }
                    }
                    else
                    {
                         echo '<span class="imgList">Unknown extension!</span>';
                    }
               }
               $string = "";
               $string = json_encode($datafiles);
               //echo '<input type="hidden" value="'.$string.'" name="imgvals" >';
          }
          exit;
     }
     
     
     function createThumbnail($imageDirectory = NULL, $imageName = NULL, $thumbDirectory = NULL, $thumbWidth = NULL, $quality = NULL)
     { 
          $details = getimagesize("$imageDirectory/$imageName") or die('Please only upload images.'); 
          $type = preg_replace('@^.+(?<=/)(.+)$@', '$1', $details['mime']); 
          eval('$srcImg = imagecreatefrom'.$type.'("$imageDirectory/$imageName");'); 
          $thumbHeight = $details[1] * ($thumbWidth / $details[0]); 
          $thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight); 
          imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight,  
          $details[0], $details[1]); 
          eval('image'.$type.'($thumbImg, "$thumbDirectory/$imageName"'. 
          (($type=='jpeg')?', $quality':'').');'); 
          imagedestroy($srcImg); 
          imagedestroy($thumbImg); 
     } 

     public function upload_profile($Subdomain = NULL, $id = null)
     {
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;

          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {
               $ReplaceSpecialCharactersArray = array("'", '"', "/", "!", "@", "#", "$", "%", "^", "&", "(", ")", "<", ">", "{", "}", "[", "]", "?", " ");
               $CurrentDateTime = date("dmYHis");
               $img = $_POST["img_val"];

               $FileName = 'Image_' . $CurrentDateTime . '.jpg';
               $FileName = rand() . "_" . str_replace($ReplaceSpecialCharactersArray, "_", $FileName);
               $CopyImage = copy($img, "assets/user_files/" . $FileName);

               $uploaddir = "assets/user_files/"; //a directory inside

               $image_name = time() . $filename;
               echo "<li>
                    <div class='msg_photo'>
               <input type='hidden' name='unpublished_photo[]' value='" . $FileName . "'>
               <img src='" . base_url() . $uploaddir . $FileName . "' class='imgList'>
               <button class='photo_remove_btn' type='button' title='' id='" . $FileName . "'>&nbsp;</button>
               </div></li>";
          }
          exit;
     }

     public function upload_commentimag($acc_name,$Subdomain = NULL, $id = null)
     {
          $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
          $this->data['event_templates'] = $event_templates;

          $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
          $this->data['menu'] = $menu;

          $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
          $this->data['menu_list'] = $menu_list;

          $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
          $this->data['notify_msg'] = $notifiy_msg;
          if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
          {
               define("MAX_SIZE", "20000000");
               $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
               //$datafiles=array();
               $uploaddir = "assets/user_files/"; //a directory inside
               foreach ($_FILES['cmphto']['name'] as $name => $value)
               {

                    $filename = stripslashes($_FILES['cmphto']['name'][$name]);
                    $size = filesize($_FILES['cmphto']['tmp_name'][$name]);
                    //get the extension of the file in a lower case format
                    $ext = $this->getExtension($filename);
                    $ext = strtolower($ext);

                    if (in_array($ext, $valid_formats))
                    {
                         if ($size < (MAX_SIZE * 1024))
                         {
                              $fnm=explode(".",$filename);
                              $filename=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $filename=$filename.".".$fnm[1];
                              $image_name = time() . $filename;
                              echo "<div class='comment_container_afteruploadimag'><li>
                                   <div class='msg_photo'>
                              <input type='hidden' name='unpublished_commentphoto[]' value='" . $image_name . "'>
                              <img src='" . base_url() . $uploaddir . $image_name . "' class='imgList'>
                              </div></li><button class='comment_section_btn' type='button' title='' id='" . $image_name . "'>&nbsp;</button></div>";
                               $fnm=explode(".",$image_name);
                              $image_name=preg_replace("/[^a-zA-Z0-9.]/", "", $fnm[0]);
                              $image_name=$image_name.".".$fnm[1];
                              $newname = $uploaddir . $image_name;

                              if (move_uploaded_file($_FILES['cmphto']['tmp_name'][$name], $newname))
                              {
                                   $time = time();
                                   $datafiles[] = $image_name;
                                   $this->createThumbnail($uploaddir, $image_name, $thumbuploaddir, 100, 100);
                              }
                              else
                              {
                                   echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>';
                              }
                         }
                         else
                         {
                              echo '<span class="imgList">You have exceeded the size limit!</span>';
                         }
                    }
                    else
                    {
                         echo '<span class="imgList">Unknown extension!</span>';
                    }
               }
               $string = "";
               $string = json_encode($datafiles);
               //echo '<input type="hidden" value="'.$string.'" name="imgvals" >';
          }
          exit;
     }

     function getExtension($str = NULL)
     {
          $i = strrpos($str, ".");
          if (!$i)
          {
               return "";
          }
          $l = strlen($str) - $i;
          $ext = substr($str, $i + 1, $l);
          return $ext;
     }
  public function checpendingkmetting($acc_name = NULL,$Subdomain = NULL)
  {
    $notes_list = $this->Event_template_model->get_notes($Subdomain);
    $this->data['notes_list'] = $notes_list;
    $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $this->data['event_templates'] = $event_templates;
    $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
    $this->data['menu'] = $menu;
    $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
    $this->data['menu_list'] = $menu_list;
    $attendees_metting=$this->Agenda_model->get_all_pending_metting_by_moderator($event_templates[0]['Id']);
    $this->data['attendees_metting']=$attendees_metting;
    $meeting_locations=$this->Event_template_model->get_all_meeting_locations_by_event($event_templates[0]['Id']);
    $this->data['meeting_locations']=$meeting_locations;
    $time_format = $this->Event_model->getTimeFormat($event_templates[0]['Id']);
    $this->data['time_format'] = $time_format;
    $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
    $this->data['notify_msg'] = $notifiy_msg;
    $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
    $this->data['cms_menu'] = $cmsmenu;
    $this->data['Subdomain'] = $Subdomain;
    $this->template->write_view('css', 'frontend_files/css', $this->data, true);
    $this->template->write_view('header', 'frontend_files/header', $this->data, true);
    $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
    $this->template->write_view('js', 'frontend_files/js', $this->data, true);
    $this->template->write_view('content', 'Speakers/checkmetting_view', $this->data, true);
    $this->template->render();
  }   
  public function changemettingstatus($acc_name = NULL,$Subdomain = NULL,$mid = NULL)
  {
    $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $metting_data=$this->Agenda_model->change_metting_status_by_metting_id($mid,$this->input->post('status'));
    $user_url = $this->Event_model->get_user_detail_by_user_id($metting_data[0]['recipient_attendee_id']);
    if($metting_data[0]['status']=='2')
    {
      $Message=ucfirst($user_url[0]['Firstname'])." ".$user_url[0]['Lastname']." has denied your Meeting request due to a clash. Please try another time.";
    }
    else if($metting_data[0]['status']=='1')
    {
      $Message=ucfirst($user_url[0]['Firstname'])." ".$user_url[0]['Lastname']." has accepted your meeting request.";
    }
    $data1 = array(
      'Message' => $Message,
      'Sender_id' => $metting_data[0]['moderator_id'],
      'Receiver_id' => !empty($metting_data[0]['attendee_id']) ? $metting_data[0]['attendee_id'] : $metting_data[0]['sender_exhibitor_id'],
      'Event_id'=>$event[0]['Id'],
      'Parent' => '0',
      'Time' => date("Y-m-d H:i:s"),
      'ispublic' => '0',
      'msg_creator_id'=>$ex_uid
    );
    $this->Message_model->send_speaker_message($data1);
    if($metting_data[0]['status']=='1')
    {
      $user_sender = $this->Event_model->get_user_detail_by_user_id(!empty($metting_data[0]['attendee_id']) ? $metting_data[0]['attendee_id'] : $metting_data[0]['sender_exhibitor_id']);
      $Message1="You have a meeting scheduled with ".ucfirst($user_sender[0]['Firstname'])." ".$user_sender[0]['Lastname']." at ".$metting_data[0]['time']." ".$metting_data[0]['date']." at ".$metting_data[0]['location'];
      $data1 = array(
        'Message' => $Message1,
        'Sender_id' => $metting_data[0]['moderator_id'],
        'Receiver_id' => $metting_data[0]['recipient_attendee_id'],
        'Event_id'=>$event[0]['Id'],
        'Parent' => '0',
        'Time' => date("Y-m-d H:i:s"),
        'ispublic' => '0',
        'msg_creator_id'=>$ex_uid
      );
      $this->Message_model->send_speaker_message($data1);
      $this->Agenda_model->send_recipient_meeting_by_moderator($mid,$metting_data[0]['recipient_attendee_id']);
    }
    echo "Success###";die;
  }   
  public function suggest_new_datetime($acc_name = NULL,$Subdomain = NULL)
  {
    $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $user = $this->session->userdata('current_user');
    $mid=$this->input->post('meeting_id_textbox');
    if(!empty($this->input->post('location')))
    {
      $this->Agenda_model->update_metting_location($mid,$this->input->post('location'));
    }
    $meeting_data=$this->Agenda_model->get_meeting_data_by_meeting_id($mid);
    $user_url = $this->Event_model->get_user_detail_by_user_id($meeting_data[0]['recipient_attendee_id']);
    $Message=ucfirst($user_url[0]['Firstname'])." ".$user_url[0]['Lastname']." is unable to meet with you at the time you specified. ".ucfirst($user_url[0]['Firstname'])." ".$user_url[0]['Lastname']." has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";
    $time=$this->input->post('time');
    foreach ($this->input->post('date') as $key => $value) {
      $date=date('Y-m-d H:i',strtotime($value.' '.$time[$key]));
      $this->Agenda_model->add_suggest_new_datetime($event[0]['Id'],$date,$mid,$meeting_data[0]['attendee_id'],NULL,$meeting_data[0]['recipient_attendee_id']);
      $link=base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/changemeetingdate/'.strtotime($date).'/'.$mid;
      $Message.="<a href='".$link."'>".$date."</a><br/>";
    }
    $data1 = array(
      'Message' => $Message,
      'Sender_id' => $user[0]->Id,
      'Receiver_id' => $meeting_data[0]['attendee_id'],
      'Event_id'=>$event[0]['Id'],
      'Parent' => '0',
      'Time' => date("Y-m-d H:i:s"),
      'ispublic' => '0',
      'msg_creator_id'=>$user[0]->Id
    );
    $this->Message_model->send_speaker_message($data1);
    redirect(base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting');
  }   
  public function changemeetingdate($acc_name = NULL,$Subdomain = NULL,$datetime = NULL,$mid = NULL)
  {
    $event = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
    $meeting_data=$this->Agenda_model->get_meeting_data_by_meeting_id($mid);
    $user = $this->session->userdata('current_user');
    if($user[0]->Id==$meeting_data[0]['attendee_id'])
    {
      $m_da['date']=date('Y-m-d',$datetime);
      $m_da['time']=date('H:i',$datetime);
      $m_da['status']='1';
      $this->Agenda_model->update_datetime_in_meeting_by_meeting_id($m_da,$mid);
      $this->Agenda_model->send_recipient_meeting_by_moderator($mid,$meeting_data[0]['recipient_attendee_id']);
      $this->Agenda_model->delete_suggest_meeting_date($event[0]['Id'],$meeting_data[0]['attendee_id'],null,$meeting_data[0]['recipient_attendee_id']);
      $Message=$meeting_data[0]['Firstname']." ".$meeting_data[0]['Lastname']." at ".$meeting_data[0]['Company_name']." as accepted your new suggested time of ".date('Y-m-d H:i',$datetime).". This meeting has now been booked";
      $data1 = array(
        'Message' => $Message,
        'Sender_id' => $user[0]->Id,
        'Receiver_id' =>$meeting_data[0]['moderator_id'],
        'Event_id'=>$event[0]['Id'],
        'Parent' => '0',
        'Time' => date("Y-m-d H:i:s"),
        'ispublic' => '0',
        'msg_creator_id'=>$user[0]->Id
      );
      $this->Message_model->send_speaker_message($data1);
      $user_sender = $this->Event_model->get_user_detail_by_user_id(!empty($meeting_data[0]['attendee_id']) ? $meeting_data[0]['attendee_id'] : $meeting_data[0]['sender_exhibitor_id']);
      $Message1="You have a meeting scheduled with ".ucfirst($user_sender[0]['Firstname'])." ".$user_sender[0]['Lastname']." at ".$meeting_data[0]['time']." ".$meeting_data[0]['date']." at ".$meeting_data[0]['location'];
      $data2 = array(
        'Message' => $Message1,
        'Sender_id' => $meeting_data[0]['moderator_id'],
        'Receiver_id' => $meeting_data[0]['recipient_attendee_id'],
        'Event_id'=>$event[0]['Id'],
        'Parent' => '0',
        'Time' => date("Y-m-d H:i:s"),
        'ispublic' => '0',
        'msg_creator_id'=>$ex_uid
      );
      $this->Message_model->send_speaker_message($data2);
    }
    redirect(base_url().'Agenda/'.$acc_name.'/'.$Subdomain.'/view_user_agenda');
  }   
  public function save_request_comments($acc_name = NULL,$Subdomain = NULL)
  {
    $this->Agenda_model->update_metting_location($this->input->post('meeting_id'),$this->input->post('comment'));
    redirect(base_url().'Speakers/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting');
  }

}
