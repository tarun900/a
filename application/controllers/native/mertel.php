<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mertel extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_template_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/mertel_model');
		include('application/libraries/nativeGcm.php');
        
        $this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }


	public function getDefaultEvent()
    {
        $event = $this->mertel_model->getDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function getHomePageMenu()
    {
        foreach ($this->menu as $key => $value) {
            if($value['is_feture_products'] == '1')
                $menu[] = $value;
        }
        foreach ($this->cmsmenu as $key => $value) {
            if($value['is_feture_product'] == '1')
            {
                $value['id'] = $value['Id'];
                $menu[] = $value;
            }
        }
        
        $sort_key_val=array("","379","10","271","11","7","12","16","2","15");
        foreach ($menu as $key => $value) {
             $kkey=array_search($value['id'], $sort_key_val);
            
             if($kkey!='')
                $array[$kkey] = $value;
             else
                $array[$key] = $value;
        }
        for ($i = 0 ;$i<=(count($array)); $i++) {
            if($array[$i]!=null)
             $array1[] = $array[$i];
           }
       
        $data['success'] = true;
        $data['data'] = $array1;
        echo json_encode($data);
    }
}