<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message1 extends CI_Controller 
{
	public $menu;
	public $cmsmenu;
	
	function __construct() 
	{
		//error_reporting(1);
		error_reporting(0);
        ini_set('error_reporting', off);
        ini_set('display_errors', off);
        ini_set('log_errors', off);
		parent::__construct();
		$this->load->model('native/app_login_model');
		$this->load->model('native/cms_model');
		$this->load->model('native/event_model');
		$this->load->model('native/message_model');
		$this->load->model('native/attendee_model');
		$this->load->model('native/speaker_model');
		$this->load->model('native/settings_model');
		$this->load->model('native/gamification_model');
		include('application/libraries/nativeGcm.php');
		
		$this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
		$this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

		foreach ($this->cmsmenu as $key => $values) 
		{
			$cmsbannerimage_decode = json_decode($values['Images']);
			$cmslogoimage_decode = json_decode($values['Logo_images']);
			$this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
			$this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
		}
	}
	public function getPublicMessages()
	{
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $event_type!='' && $page_no!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_public_msg_coversation($event_id,$page_no,$limit);
				$total_pages=$this->message_model->public_msg_total_pages($event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);

	}
	public function publicMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		if($event_id!='' && $message!='' && $token!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data['Message']=$message;
				$message_data['Sender_id']=$user_id;
				$message_data['Event_id']=$event_id;
				$message_data['Parent']=0;
				$message_data['image']="";
				$message_data['Time']=date("Y-m-d H:i:s");
				$message_data['ispublic']='1';
				$message_data['msg_creator_id']=$user_id;
				$message_id = $this->message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Successfully Saved",
						'message_id' => $message_id
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function public_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data1 	= $this->message_model->getMessageDetails($message_id);
				$newImageName 	= round(microtime(true) * 1000).".jpeg";
				$target_path 	= "././assets/user_files/".$newImageName; 
				$target_path1 	= "././assets/user_files/thumbnail/";         

				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				
				$update_data['image'] = $newImageName;
				if($message_data1[0]['image']!='')
				{
					$arr=json_decode($message_data1[0]['image']);
					$arr[]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->message_model->updateMessageImage($message_id,$update_arr);
				}
				else
				{

					$arr[0]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->message_model->updateMessageImage($message_id,$update_arr);
				}
				$a[0]                           = $newImageName;
				
				$message_data['Message']        = '';
				$message_data['Sender_id']      = $user_id;
				$message_data['Event_id']       = $event_id;
				$message_data['Parent']         = $message_id;
				$message_data['image']          = json_encode($a);
				$message_data['Time']           = date("Y-m-d H:i:s");
				$message_data['ispublic']       = '1';
				$message_data['msg_creator_id'] = $user_id;

				$message_id = $this->message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_message()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!=''  && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->message_model->delete_message($message_id);
				$data = array(
					'success' => true,
					'message' => "Successfully deleted"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function make_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$comment=$this->input->post('comment');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				if($_FILES['image']['name']!='')
				{
					$newImageName = round(microtime(true) * 1000).".jpeg";
					$target_path= "././assets/user_files/".$newImageName; 
					$target_path1= "././assets/user_files/thumbnail/";                 
					move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
					copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
					if($newImageName!='')
					{
						$arr[0]=$newImageName;
						$comment_arr['image']=json_encode($arr);
					}
				}
				$comment_arr['comment']=$comment;
				$comment_arr['user_id']=$user_id;
				$comment_arr['msg_id']=$message_id;
				$comment_arr['Time']=date("Y-m-d H:i:s");;
				$this->message_model->make_comment($comment_arr,$event_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$comment_id=$this->input->post('comment_id');
		if($event_id!='' && $token!='' && $comment_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->message_model->delete_comment($comment_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getAllSpeakersAttendee()
	{
		//$start_fun_time = microtime(true);
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		if($event_id!='' && $event_type!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$event=$this->event_model->getEvent($event_id);
				$speakers = $this->speaker_model->getSpeakersListByEventId($event_id);
				if($event[0]['show_attendee_menu']=='1'){
					$attendee = $this->attendee_model->getAttendeeListByEventId($event_id);
				}
				else
				{
					$attendee=array();
				}
				$exhibitor = $this->message_model->getExhibitorListByEventId($event_id);

				foreach ($speakers as $key => $value) {
					$speakers[$key]['type'] = "7";
				}
				foreach ($attendee as $key => $value) {
					$attendee[$key]['type'] = "2";
				}
				foreach ($exhibitor as $key => $value) {
					$exhibitor[$key]['type'] = "3";
				}
				$data[0]['type'] = "Speakers";
				$data[0]['data'] = $speakers;

				$data[1]['type'] = "Attendees";
				$data[1]['data'] = $attendee;

				$data[2]['type'] = "Exhibitors";
				$data[2]['data'] = $exhibitor;
				$data = array(
					'success' => true,
					'data'	=> $data,
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		/*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/message/getAllSpeakersAttendee function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
		echo json_encode($data);
	}
	public function getPrivateMessages()
	{
		//$start_fun_time = microtime(true);
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_private_section_msg_coversation($event_id,$user_id,$page_no,$limit);
				foreach ($message as $key => $value) {
					$string = $value['message'];
					if($string != strip_tags($string)) 
					{
						$message[$key]['message'] = strip_tags($value['message']);
						$message[$key]['is_clickable'] = '1';
					}
					else
					{
						$message[$key]['is_clickable'] = '0';
					}	
				}
				$total_pages=$this->message_model->private_msg_total_pages($user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		/*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/message/getPrivateMessages function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
		echo json_encode($data);
	}
	public function privateMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		$receiver_id=json_decode($this->input->post('receiver_id'));

		if($event_id!='' && $message!=''  && $user_id!='' && !empty($receiver_id))
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_id = $this->message_model->savePrivateMessage($this->input->post());

				if(!empty($message_id))
				{
					$moderator = $this->message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->message_model->getGcmIds($receiver_id);
						$device = $this->message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->message_model->getSender($user_id);
					
						
					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm($event_id);
						$template = $this->message_model->getNotificationTemplate($event_id,'Message');
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $template['Content'];
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);
							$extra['title'] = $template['Slug'];

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

                          
							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
							
                        }
                        else
                        {
							$msg['title'] = $template['Slug'];
							$msg['message'] = $template['Content'];
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
						}
						$this->event_model->sendEmailToAttendees($event_id,$template['Content'],$value['Email'],"Notification");
					}
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function private_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=json_decode($this->input->post('message_id'));
		$receiver_id=json_decode($this->input->post('receiver_id'));

		if($event_id!='' && $token!='' && $user_id!='' && !empty($receiver_id) && !empty($message_id) && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
				foreach ($receiver_id as $key => $value) 
				{
					$message_data1=$this->message_model->getMessageDetails($message_id[$key]);
					if($key==0)
					{
						$newImageName = round(microtime(true) * 1000).".jpeg";
						$target_path= "././assets/user_files/".$newImageName; 
						$target_path1= "././assets/user_files/thumbnail/";                 
						move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
						copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
						$update_data['image'] =$newImageName;
						$old_image=$newImageName;
					}
					else
					{
						$update_data['image'] = $old_image;
					}
					if($message_data1[0]['image']!='')
					{
						$arr=json_decode($message_data1[0]['image']);
						$arr[]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					else
					{

						$arr[0]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					$a[0]=$newImageName;
					$message_data['Message']='';
					$message_data['Sender_id']=$user_id;
					$message_data['Receiver_id']=$value;
					$message_data['Event_id']=$event_id;
					$message_data['Parent']=$message_id[$key];
					$message_data['image']=json_encode($a);
					$message_data['Time']=date("Y-m-d H:i:s");
					$message_data['ispublic']='0';
					$message_data['msg_creator_id']=$user_id;
					$this->message_model->savePrivateImageImage($message_data);


				}
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getImageList()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $message_id!='')
		{
			
				$images = $this->message_model->getImages($event_id,$message_id);

				$data = array(
					'images' => json_decode($images),
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	// 16 Aug 2016
	public function notificationCounter()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$token=$this->input->post('token');
		if($event_id == "447")
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		else
		{
			if($event_id!='' && $user_id!='')
			{
				$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
				if (empty($user)) 
				{
					$data = array(
						'success' => false,
						'data' => array(
							'msg' => 'Please check token or event.'
							)
						);   
				} 
				else 
				{
					$notify_data = $this->message_model->getNotificationCounter($event_id,$user_id);
					$data = array(
						'count' => json_decode(count($notify_data)),
						'notify_data'  => $notify_data,
						);
					
					$data = array(
						'success' => true,
						'data' => $data
						);
				}
			}
			else
			{
				$data = array(
					'success' => false,
					'message' => "Invalid parameters"
					);
			}
		}
		echo json_encode($data);
	}
	public function messageRead()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$message_type=$this->input->post('message_type'); // public=1,private=0

		if($event_id!='' && $user_id!=''){
			$result = $this->message_model->updateMessageReadStatus($event_id,$user_id,$message_type);

			if($result){
				$data = array(
					'success' => true,
					);
			}else{
				$data = array(
					'success' => false,
					'message' => "Something went wrong. Please try again.",
					);
			}
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getNotificationListing()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');

		if($event_id!='' && $user_id!=''){
			$result = $this->message_model->getNotificationListing($event_id,$user_id);

			
		$data = array(
			'success' => true,
			'data'	=> $result,
		);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function deleteNotification()
	{
		$notification_id 	= $this->input->post('notification_id');
		$user_id 			= $this->input->post('user_id');

		if($notification_id!='' && $user_id!='')
		{
			$data1['umn_id'] 	= $notification_id;
			$data1['user_id'] 			= $user_id;
			$result = $this->message_model->deleteNotification($data1);

			$data = array(
				'success' 	=> true,
				'message'	=> 'Notification deleted successfully.',
			);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
			);
		}
		echo json_encode($data);
	}

	public function getPrivateMessagesConversation()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		$sender_id=$this->input->post('sender_id');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$sender_id);
				$sender_detail = $this->message_model->getSenderDetails($sender_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					/*if($string != strip_tags($string)) 
					{*/
						$message[$key]['message'] = strip_tags($value['message']);
						//$message[$key]['is_clickable'] = '1';
					/*}
					else
					{
						$message[$key]['is_clickable'] = '0';
					}	*/
				}
				$total_pages=$this->message_model->private_msg_total_pages_list($sender_id,$user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'sender_detail' => $sender_detail,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getPrivateUnreadMessagesList()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->message_model->view_private_unread_message_list($event_id,$user_id,$page_no,$limit);
				
				
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$data = array(
					'messages' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function privateMessageSendImage()
	{

		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		//$receiver_id=json_decode($this->input->post('receiver_id'));
		$receiver_id=$this->input->post('receiver_id');
		$limit = 10; $page_no = 1;
		if($event_id!='' && $token!='' && $user_id!='' && $receiver_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
					
				$newImageName = round(microtime(true) * 1000).".jpeg";
				$target_path= "././assets/user_files/".$newImageName; 
				$target_path1= "././assets/user_files/thumbnail/";                 
				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				$update_data['image'] =$newImageName;
				$old_image=$newImageName;
				
				$message_data['Message']='';
				$message_data['Sender_id']=$user_id;
				$message_data['Receiver_id']=$receiver_id;
				$message_data['Event_id']=$event_id;
				$arr[0] = $newImageName;
				$message_data['image']=json_encode($arr);
				$message_data['Time']=date("Y-m-d H:i:s");
				$message_data['ispublic']='0';
				$message_data['msg_creator_id']=$user_id;
				$message_id = $this->message_model->savePrivateImageImageOnly($message_data);


				$message = $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
				$this->add_user_game_point($user_id,$event_id,1);

				$sender_detail = $this->message_model->getSenderDetails($receiver_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					
					$message[$key]['message'] = strip_tags($value['message']);
					
				}
				$total_pages=$this->message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);
				$array = array(
						'messages' => ($message) ? $message : [],
						'total_pages' => $total_pages,
						'sender_detail' => $sender_detail,
						);
				if($message_id!=0)
				{	
					$moderator = $this->message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->message_model->getGcmIds($receiver_id);
						$device = $this->message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->message_model->getSender($user_id);
					
					$message = $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
					$sender_detail = $this->message_model->getSenderDetails($receiver_id,$event_id);		

					foreach ($message as $key => $value) {
						$string = $value['message'];
						
						$message[$key]['message'] = strip_tags($value['message']);
						
					}
					$total_pages=$this->message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);

					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm($event_id);
						$template = $this->message_model->getNotificationTemplate($event_id,'Message');
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $template['Content'];
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							$extra['title'] = $template['Slug'];

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							
                        }
                        else
                        {
							$msg['title'] = $template['Slug'];
							$msg['message'] = $template['Content'];
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
							
						}
						if($template['send_email'] == '1')
						{
						$this->event_model->sendEmailToAttendees($event_id,$template['email_content'],$value['Email'],$template['email_subject']);
						}
					}
					
					$data = array(
						'success' => true,
						'message' => "Success saved",
						'data' => $array,
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function privateMessageSendText()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		$receiver_id=$this->input->post('receiver_id');
		
		if($event_id!='' && $message!=''  && $user_id!='' && !empty($receiver_id))
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit = 10;
				$_POST['receiver_id'] = $this->message_model->getUserIdByEx($receiver_id,$event_id);
				$message_id = $this->message_model->savePrivateMessageText($this->input->post());

				if(!empty($message_id))
				{
					$moderator = $this->message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->message_model->getGcmIds($receiver_id);
						$device = $this->message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->message_model->getSender($user_id);
					
					$message = $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
					$sender_detail = $this->message_model->getSenderDetails($receiver_id,$event_id);		

					foreach ($message as $key => $value) {
						$string = $value['message'];
						
						$message[$key]['message'] = strip_tags($value['message']);
						
					}
					$total_pages=$this->message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);

					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm($event_id);
						$template = $this->message_model->getNotificationTemplate($event_id,'Message');
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $template['Content'];
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							$extra['title'] = $template['Slug'];

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							
                        }
                        else
                        {
							$msg['title'] = $template['Slug'];
							$msg['message'] = $template['Content'];
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
							
						}
						$this->add_user_game_point($user_id,$event_id,1);

						if($template['send_email'] == '1')
						{
						$this->event_model->sendEmailToAttendees($event_id,$template['email_content'],$value['Email'],$template['email_subject']);
						}
					}
					$array = array(
						'messages' => $message,
						'total_pages' => $total_pages,
						'sender_detail' => $sender_detail,
						);
					$data = array(
						'success' => true,
						'message' => "Successfully Saved",
						'message_id' => $message_id,
						'data' => $array,
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_private_message()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=json_decode($this->input->post('message_id'));
		$receiver_id=json_decode($this->input->post('receiver_id'));
		
		
		if($event_id!='' && $token!='' && $user_id!='' && !empty($receiver_id) && !empty($message_id) && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				

				$message_id = $this->message_model->delete_private_message($message_id);
				
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Message Deleted"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function get_all_contacts()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->message_model->get_all_contacts($event_id,$user_id,$page_no,$limit);
								
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$data = array(
					'user_files' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function get_my_messages()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->message_model->get_my_messages($event_id,$user_id,$page_no,$limit);
				
				
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$data = array(
					'messages' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function delete_converstaion()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$receiver_id=$this->input->post('receiver_id');

		if($event_id!='' && $user_id!='' && $receiver_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->message_model->delete_converstaion($event_id,$user_id,$receiver_id);

				$data = array(
					'success' => true,
					'message' => "Conversation Deleted"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function get_my_messages_test()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->message_model->get_my_messages_test($event_id,$user_id,$page_no,$limit);
				
				
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$data = array(
					'messages' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function add_user_game_point($user_id,$event_id,$rank_id)
    {   
		$this->gamification_model->add_user_point($user_id,$event_id,$rank_id);
/*
        $obj = new Gcm();
        $users = $this->gamification_model->getAllUsersGCM_id_by_event_id($event_id);
        $count = count($users);
        
        if($count > 100)
        {
            $limit = 100;
            for ($i=0;$i<$count;$i++) 
            {
                $page_no        = $i;

                $start          = ($page_no)*$limit;
                $users1         = array_slice($users,$start,$limit);
                foreach ($users1 as $key => $value) 
                {
                    if($value['gcm_id']!='')
                    {
                        $msg =  '';
                        $extra['message_type'] = 'gamification';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        if($value['device'] == "Iphone")
                        {
                            $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                    }
                }
            }
        }
        else
        {
            foreach ($users as $key => $value)
            {
                if($value['gcm_id']!='')
                {
                    $msg = '';
                    $extra['message_type'] = 'gamification';
                    if($value['device'] == "Iphone")
                    {
                        $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                    }
                    else
                    {
                        $msg['title'] = '';
                        $msg['message'] = '';
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
                    } 
                }
            }
        }*/
        return true;
    }
    public function startSocketServer()
    {
    	//error_reporting(E_ALL);
        $address=$this->config->item('ip');
        $port=6788;
        
        $this->sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );

        socket_set_option($this->sock, SOL_SOCKET, SO_REUSEADDR, 1) ;
        
        if(!socket_bind( $this->sock, 0, $port ))
        {
            socket_connect($this->sock, $address, $port) ;
        }
        
        //socket_close($this->sock);
    }
    public function sendmessage()
    {   
    	//error_reporting(E_ALL);
    	//$bse = base64_encode(file_get_contents('application/controllers/native/lion-wild-africa-african.jpg'));
    	//echo "<img src='data:image/jpg;base64,".$bse."' />";exit();
    	//$image = "application/controllers/native/lion-wild-africa-african.jpg";
    	/*$data = fopen ($image, 'rb');
		$size=filesize ($image);
		$contents= fread ($data, $size);
		fclose ($data);*/
        
        $this->startSocketServer();
        $pending_client = $pending_msg = [];
        $msg="200";
        if($this->sock)
        {
            socket_listen( $this->sock );
            while($rand  = socket_accept($this->sock) )
            {
            	socket_getpeername($rand, $address,$port);
            	$data = '';
                while($i = socket_read($rand,10000000,PHP_BINARY_READ))
                {
                	$data .= $i;
                	if($this->isJson($data))
            		{
            			break;
            		}
                	
                }
                if($this->isJson($data))
                {
                	$data = json_decode($data,true);
                	if(!array_key_exists('message', $data) && !array_key_exists('image', $data) )
                	{
                		if(!in_array($data['user_id'], $to_id))
                		{
	                		$to_id[] 		= $data['user_id'];
	                		$connected[] 	= $rand;
                		}
                		continue;
                	}

	                $event_id 		= $data['event_id'];
					$token 			= $data['token'];
					$user_id 		= $data['user_id'];
					$message 		= $data['message'];
					$receiver_id 	= $data['receiver_id'];     
					$limit 			= 10;     
					$page_no 		= 1;
	                socket_getpeername ( $rand , $address ,$port );
	                $flag = 0 ;
	              

					if(array_key_exists('image', $data))
                	{
                		$newImageName = time().date('Ymd').".jpeg";
		                $ifp = fopen( "././assets/user_files/$newImageName", 'wb' ); 
					    fwrite( $ifp, base64_decode( $data['image'] ) );
					    fclose( $ifp ); 
					    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
					    $message_data['Message']='';
						$message_data['Sender_id']=$user_id;
						$message_data['Receiver_id']=$receiver_id;
						$message_data['Event_id']=$event_id;
						$arr[0] = $newImageName;
						$message_data['image']=json_encode($arr);
						$message_data['Time']=date("Y-m-d H:i:s");
						$message_data['ispublic']='0';
						$message_data['msg_creator_id']=$user_id;
						$message_data['is_read'] = (in_array($data['receiver_id'], $to_id)  ) ? '1' : '0';
						$message_id = $this->message_model->savePrivateImageImageOnly($message_data);
                	}
                	else
                	{
                		$post_data['event_id'] 		= $data['event_id'];
		                $post_data['token'] 		= $data['token'];
		                $post_data['user_id'] 		= $data['user_id'];
		                $post_data['message'] 		= $data['message'];
		                $post_data['receiver_id'] 	= $data['receiver_id'];
						$post_data['receiver_id'] 	= $this->message_model->getUserIdByEx($receiver_id,$event_id);
						$post_data['is_read'] = (in_array($data['receiver_id'], $to_id)  ) ? '1' : '0';
						$message_id 	= $this->message_model->savePrivateMessageText($post_data);

                	}
					$m_data 		= $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
					$m_data[0]['image'] = $data['image'];
					if(in_array($data['receiver_id'], $to_id)  )
	                {
	                	$key 		= array_search($data['receiver_id'], $to_id);
	                	
	                    $sent 	= socket_write($connected[$key],json_encode($m_data[0]),strlen(json_encode($m_data[0])));
	                    $flag 	= 1;
	                    socket_close($connected[$key]);
	                    unset($to_id[$key]);
	                    unset($connected[$key]);
	                	
	                }
	                
	                $sent 	= socket_write($rand,json_encode($m_data[0]),strlen(json_encode($m_data[0])));
	                $key 	= array_search($data['user_id'], $to_id);
	                socket_close($rand);
	               
	                unset($to_id[$key]);
	                unset($connected[$key]);
					if(!empty($message_id))
					{
						$moderator = $this->message_model->getModerators($speaker_id,$event_id);
						if(empty($moderator))
						{
							$gcm_ids 	= $this->message_model->getGcmIds($receiver_id);
							$device 	= $this->message_model->getDevice($receiver_id);
						}
						else
						{
							$gcm_ids 	= $this->message_model->getModeratorsGcmIds($receiver_id,$event_id);
							$device 	= $this->message_model->getModeratorDevice($receiver_id,$event_id);
						}
						$sender 		= $this->message_model->getSender($user_id);
						
						$message 		= $this->message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
						$sender_detail 	= $this->message_model->getSenderDetails($receiver_id,$event_id);
						foreach ($message as $key => $value) {
							$string 		= $value['message'];
							$message[$key]['message'] = strip_tags($value['message']);
						}
						$total_pages 	= $this->message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);
						foreach ($gcm_ids as $key => $value) {
							$obj 		= new Gcm($event_id);
							$template 	= $this->message_model->getNotificationTemplate($event_id,'Message');
							if($flag == 0)
							{
								if($device[$key]['device'] == "Iphone")
		                        {
		                            $msg 					=  $template['Content'];
		                            $extra['sender_name'] 	= $sender;
									$extra['message_type'] 	= 'Private';
									$extra['message_id'] 	= $user_id;
									$extra['event'] 		= $this->settings_model->event_name($event_id);
									$extra['title'] 		= $template['Slug'];
		                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
		                        }
		                        else
		                        {
									$msg['title'] 			= $template['Slug'];
									$msg['message'] 		= $template['Content'];
									$extra['sender_name'] 	= $sender;
									$extra['message_type'] 	= 'Private';
									$extra['message_id'] 	= $user_id;
									$extra['event'] 		= $this->settings_model->event_name($event_id);
									$msg['vibrate'] 		= 1;
									$msg['sound'] 			= 1;
									$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
								}
							}
							$this->add_user_game_point($user_id,$event_id,1);
							if($template['send_email'] == '1' && !$flag)
							{
								//$this->event_model->sendEmailToAttendees($event_id,$template['email_content'],$value['Email'],$template['email_subject']);
							}
						}
					}
                }
            }
        }
    }
    public function isJson($string) {
		 json_decode($string);
		 return (json_last_error() == JSON_ERROR_NONE);
	}

	public function getPrivateMessagesConversationRashmi()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		$sender_id=$this->input->post('sender_id');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->message_model->view_private_section_msg_coversation_list_unread($event_id,$user_id,$page_no,$limit,$sender_id);
				$sender_detail = $this->message_model->getSenderDetails($sender_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					$message[$key]['message'] = strip_tags($value['message']);
						
				}
				$total_pages=$this->message_model->private_msg_total_pages_list($sender_id,$user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'sender_detail' => $sender_detail,
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}

}
