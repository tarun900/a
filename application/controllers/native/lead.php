<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lead extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/lead_model');
        $this->load->model('native/app_login_model');
    }
    public function scan_lead()
    {
        $user_id  =  $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $scan_id = $this->input->post('scan_id');
        $scan_data = $this->input->post('scan_data');
        if(!empty($user_id) && !empty($event_id))
        {   
            if(!empty($scan_id))
            {
                $tmp = explode('-',$scan_id);
                $scan_id = $tmp[1];
            }
            else
            {
                $tmp = explode('-',$scan_data);
                $scan_id = $tmp[1];
                /*$scan_id = $scan_data;
                $scan_id = $this->lead_model->get_user_by_email($event_id,$scan_data);*/
            }
            $user_info = $this->lead_model->get_scan_info($event_id,$scan_id);
           
            if(!empty($user_info) && empty($user_info['message']))
            {
                $custom_column = $this->lead_model->get_custom_column($event_id);
                
                $data = array(
                    'user_info' => $user_info,
                    'custom_column' => $custom_column
                ); 
            }
            elseif (!empty($user_info['message']))
            {
                $data = array(
                    'user_info' => new stdClass(),
                    'message' => 'Lead is already scanned'
                    );
            }
            else
            {
                $data = array(
                    'user_info' => new stdClass(),
                    'message' => 'No Lead Found'
                    );
            }

            $data = array(
                'success' =>true,
                'data' => $data
            );

        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function save_scan_lead()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $lead_user_id = $this->input->post('lead_user_id');
        $lead_data['firstname'] = $this->input->post('firstname');
        $lead_data['lastname'] = $this->input->post('lastname');
        $lead_data['email'] = $this->input->post('email');
        $lead_data['company_name'] = $this->input->post('company_name');
        $lead_data['title'] = $this->input->post('title');
        $lead_data['custom_column_data'] = $this->input->post('custom_column_data');

        if(!empty($user_id) && !empty($event_id))
        {   
            $login_user_data=$this->app_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            if($login_user_data['Rid']=='4')
            {
                $exhibitor_user_id=$this->lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
            }

            $this->lead_model->save_scan_lead($lead_data,$event_id,$exhibitor_user_id,$lead_user_id);
            $survey = $this->lead_model->get_all_exibitor_user_questions($event_id,$exhibitor_user_id,$lead_user_id);
            $data = array(
                'success' =>true,
                'survey' => $survey,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function save_questions()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');    
        $lead_user_id = $this->input->post('lead_user_id');
        $survey_data = json_decode($this->input->post('survey_data'),true);

        if(!empty($user_id) && !empty($event_id) && !empty($lead_user_id))
        {	
        	foreach ($survey_data as $key => $value)
        	{
        		$survey_data[$key]['event_id'] = $event_id;
        		$survey_data[$key]['user_id'] = $lead_user_id;
        		$survey_data[$key]['answer_date'] = date('Y-m-d H:i:s');
        	}
        	$this->lead_model->add_question_answer($survey_data);

        	$data = array(
        		'success' => true,
        		'message' => "Lead added Successfully"
        		);
        }   
        else
        {
            $data = array(
                'success' => false ,
                'message' =>'Invalid parameters'
                );
        }
        echo json_encode($data);
    }
    public function get_exhi_leads()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($user_id) && !empty($event_id))
        {   
            $login_user_data=$this->app_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            if($login_user_data['Rid']=='4')
            {
                $exhibitor_user_id=$this->lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
            }
            
            if($keyword!='')
            $where = "(el.lastname like '%".$keyword."%' OR el.firstname like '%".$keyword."%' OR el.company_name like '%".$keyword."%')";

            $leads = $this->lead_model->get_all_my_lead_by_exibitor_user($event_id,$exhibitor_user_id,$where);
           
            
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($leads);
            $total_page     = ceil($total/$limit);
            $leads          = array_slice($leads,$start,$limit);

            $data = array(
                'leads' => $leads,
                'total_page' => $total_page,
            );
            $data = array(
                'success' =>true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function rep_list()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($event_id) && !empty($user_id))
        {
           
            
            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%')";

            $total_rep=$this->lead_model->get_all_my_representative_by_exibitor_user($event_id,$user_id,$where);
            

            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($total_rep);
            $total_page     = ceil($total/$limit);
            $total_rep      = array_slice($total_rep,$start,$limit);

            $data = array(
                'rep_list' => $total_rep,
                'total_page' => $total_page,
                
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);       
    }
    public function add_new_representative()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $rep_id = json_decode($this->input->post('rep_id'),true);

        if(!empty($user_id) && !empty($event_id) && !empty($rep_id))
        {
            if(!empty($rep_id))
            {
                foreach ($rep_id as $value) 
                {   
                    $this->lead_model->add_new_representative($event_id,$user_id,$value); 
                }
            }
            $data = array(
                'success' => true,
                'message' => 'Representative Added Successfully'
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function get_attendee_list_for_rep()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $page_no = $this->input->post('page_no');
        $keyword = $this->input->post('keyword');

        if(!empty($user_id) && !empty($event_id))
        {   
            $max=$this->lead_model->get_exibitor_user_premission($event_id,$user_id);

            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%')";

            $attendee_list = $this->lead_model->get_event_all_attendee_from_assing_resp($event_id,$where);
            
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($attendee_list);
            $total_page     = ceil($total/$limit);
            $attendee_list  = array_slice($attendee_list,$start,$limit);

            $data = array(
                'list' => $attendee_list,
                'total_page' => $total_page,
                'limit' =>$max
                );

            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }
    public function remove_rep()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $rep_id = $this->input->post('rep_id');
        if(!empty($event_id) && !empty($rep_id))
        {   
            $this->lead_model->remove_my_representative($event_id,$rep_id);
            $data = array(
                'message' => 'Representative Removed'
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
        } 
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function export_lead()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $send_email = $this->input->post('send_email');

        if(!empty($event_id) && !empty($user_id))
        {
            $login_user_data=$this->app_login_model->getUserDetailsId($user_id,$event_id);
            $exhibitor_user_id=$user_id;
            if($login_user_data['Rid']=='4')
            {
                $exhibitor_user_id=$this->lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
            }

            $custom_column = $this->lead_model->get_custom_column($event_id);
            $questions=$this->lead_model->get_all_exibitor_user_questions_for_export($event_id,$exhibitor_user_id);
            $filename = "./assets/lead_csv/lead_user_detail.csv";
            $fp = fopen($filename, 'w');
            $header[] = "FirstName";
            $header[] = "LastName";
            $header[] = "Email";
            $header[] = "Title";
            $header[] = "Company Name";
            foreach ($custom_column as $key => $value)
            {
                $header[]=ucfirst($value['column_name']);
            }
            foreach ($questions as $key => $value)
            {
                $header[]=$value['Question'];
            }
            
            fputcsv($fp, $header);
            $my_lead = $this->lead_model->get_all_my_lead_by_exibitor_user($event_id,$exhibitor_user_id);
            foreach ($my_lead as $key => $value) 
            {
                $data['FirstName']=$value['Firstname'];
                $data['LastName']=$value['Lastname'];
                $data['Email']=$value['Email'];
                $data['Title']=$value['Title'];
                $data['Company Name']=$value['Company_name'];
                $custom_column_data=json_decode($value['custom_column_data'],true);
                foreach ($custom_column as $ckey => $cvalue) 
                {
                    $data[ucfirst($cvalue['column_name'])]=$custom_column_data[$cvalue['column_name']];
                }
                foreach ($questions as $qkey => $qvalue) 
                {
                    $ans=$this->lead_model->get_user_question_answer($event_id,$value['Id'],$qvalue['q_id']);
                    $data[]= !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment']; 
                }
                fputcsv($fp, $data);
                unset($data);
            }
            fclose($fp);
            file_put_contents($filename, $fp);
            if($send_email)
            {   
                $event_name = $this->lead_model->get_event_name($event_id);
                $this->load->library('email');
                $config['protocol']   = 'smtp';
                $config['smtp_host']  = 'localhost';
                $config['smtp_port']  = '25';
                $config['smtp_user']  = 'invite@allintheloop.com';
                $config['smtp_pass']  = '=V8h@0rcuh#G';
                $config['_encoding']  = 'base64';
                $config['mailtype']   = 'html';
                $this->email->initialize($config);
                $this->email->from('invite@allintheloop.com',$event_name);
                $this->email->to($login_user_data['Email']);
                //$this->email->to('jagdish@elsner.com.au');
                $this->email->subject("My Lead Csv File");
                $this->email->message("Please find below Attachment");
                $this->email->attach($filename);
                $this->email->send();
                $this->email->clear(TRUE);
                $data['message'] = "CSV email successfully";
            }
            else
            {
            $data = array(
                'url' => base_url().'assets/lead_csv/lead_user_detail.csv',
                'message' => ""
                );    
            }
            

            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function get_offline_data()
    {   
        $event_id = $this->input->post('event_id');
        $exhi_id = $this->input->post('exhi_id');
        
        if(!empty($event_id) && !empty($exhi_id))
        {
            $custom_column = $this->lead_model->get_custom_column($event_id);
            $survey = $this->lead_model->get_all_exibitor_user_questions($event_id,$exhi_id,'1');
            $data = array(
                'success' => true,
                'survey' => $survey,
                'custom_column' => $custom_column,
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
    public function save_scan_upload()
    {   
        
        $event_id = $this->input->post('event_id');
        $user_id  = $this->input->post('user_id');
        $scan_id = $this->input->post('scan_id');
        $scan_data = $this->input->post('scan_data');
        $upload_lead = json_decode($this->input->post('upload_lead'),true);
        $survey_data = json_decode($this->input->post('survey_data'),true);
        
        if(!empty($event_id) && !empty($user_id))
        {   
            
            $tmp = explode('-',$scan_data);
            $scan_id = $tmp[1];
            $scan_id = $this->lead_model->get_user_by_id($event_id,$scan_id);
            
            if(!empty($scan_id))
            {    
                $login_user_data=$this->app_login_model->getUserDetailsId($user_id,$event_id);
                $exhibitor_user_id=$user_id;
                if($login_user_data['Rid']=='4')
                {
                    $exhibitor_user_id=$this->lead_model->get_exhibitor_user_by_representative_id($exhibitor_user_id,$event_id);
                }
                $upload_lead['custom_column_data'] = !empty($upload_lead['custom_column_data']) ? json_encode($upload_lead['custom_column_data']) : NULL;

                $lead_msg = $this->lead_model->save_scan_lead($upload_lead,$event_id,$exhibitor_user_id,$scan_id);
                
                if($lead_msg)
                {   
                    if(!empty($survey_data))
                    {
                        foreach ($survey_data as $key => $value)
                        {
                            $survey_data[$key]['event_id'] = $event_id;
                            $survey_data[$key]['user_id'] = $scan_id;
                            $survey_data[$key]['answer_date'] = date('Y-m-d H:i:s');
                        }
                        $this->lead_model->add_question_answer($survey_data);
                    }
                    $message = "Lead scanned successfully.";
                }
                else
                {
                    $message = "Lead is already scanned.";
                }

            }
            $data = array(
                'success' => true,
                'message'    => $message
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }
}
