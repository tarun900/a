<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Qa extends CI_Controller     
{
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native/qa_model');
        include('application/libraries/nativeGcm.php');

    }

    public function getAllSession()
    {
        $event_id = $this->input->post('event_id');
        $lang_id  = $this->input->post('lang_id');

        if($event_id!='')
        {
            $sessions = $this->qa_model->getAllSesssions($event_id,$lang_id);
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    public function getSessionDetail()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $user_id = $this->input->post('user_id');
        $device_id = $this->input->post('device_id');
        $lang_id  = $this->input->post('lang_id');

        if($event_id!='' && $session_id!='')
        {
            $sessions = $this->qa_model->getSessiondetail($event_id,$session_id,$lang_id);
            if($user_id != 0)
            {
                $messages = $this->qa_model->getMessages($session_id,$user_id);
            }
            else
            {
                $messages = $this->qa_model->getMessages($session_id,$device_id);
            }
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    public function sendMessage()
    {
        $event_id               = $this->input->post('event_id');
        $session_id             = $this->input->post('session_id');
        $user_id                = $this->input->post('user_id');
        $Moderator_speaker_id   = $this->input->post('Moderator_speaker_id');
        $message                = $this->input->post('message');

        if($event_id!='' && $session_id!='' && $message!='' && $Moderator_speaker_id!='')
        {
            $insert_data['event_id']        = $event_id;
            $insert_data['Sender_id']       = ($user_id) ? $user_id : NULL;
            $insert_data['message']         = $message;
            $insert_data['qasession_id']    = $session_id;

            $zone = $this->qa_model->getEventTimeZone($event_id);

            date_default_timezone_set("UTC");
            $cdate=date('Y-m-d H:i:s');
            if(!empty($zone))
            {
                if(strpos($zone,"-")==true)
                { 
                    $arr=explode("-",$zone);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                }
                if(strpos($zone,"+")==true)
                {
                    $arr=explode("+",$zone);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                }
            }

            $insert_data['send_datetime']   = $cdate;
            $this->qa_model->saveMessage($insert_data,$Moderator_speaker_id);

            $data = array(
                  'success'     => true,
                  'message'     => "Your message was sent successfully",
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }
    public function VoteMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');
        $device_id = $this->input->post('device_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            
            if($user_id != 0)
            {
                $this->qa_model->vote($message_id,$user_id);
                $messages = $this->qa_model->getMessages($session_id,$user_id);
            }
            else
            {
                $this->qa_model->voteAnonymous($message_id,$device_id);
                $messages = $this->qa_model->getMessages($session_id,$device_id);   
            }
            
            $sessions = $this->qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }
    public function DeleteMessage()
    {
        $event_id = $this->input->post('event_id');
        $session_id = $this->input->post('session_id');
        $message_id = $this->input->post('message_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $session_id!='' && $message_id!='' &&$user_id!='')
        {   
            
            $this->qa_model->deleteMessage($message_id);
            $obj = new Gcm();
			$users = $this->qa_model->getAllUsersGCM_id_by_event_id($event_id);
			$count = count($users);
			
			if($count > 100)
			{
			    $limit = 100;
			    for ($i=0;$i<$count;$i++) 
			    {
			        $page_no        = $i;

			        $start          = ($page_no)*$limit;
			        $users1         = array_slice($users,$start,$limit);
			        foreach ($users1 as $key => $value) 
			        {
			            if($value['gcm_id']!='')
			            {
			                $msg =  '';
			                $extra['message_type'] = 'QAQusetion';
			                $extra['message_id'] = '';
			                $extra['event'] = $event[0]['Event_name'];
			                if($value['device'] == "Iphone")
			                {
			                    $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			                }
			                else
			                {
			                    $msg['title'] = '';
			                    $msg['message'] = '';
			                    $msg['vibrate'] = 1;
			                    $msg['sound'] = 1;
			                    $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			                } 
			            }
			        }
			    }
			}
			else
			{
			    foreach ($users as $key => $value)
			    {
			        if($value['gcm_id']!='')
			        {
			            $msg = '';
			            $extra['message_type'] = 'QAQusetion';
			            if($value['device'] == "Iphone")
			            {
			                $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			            }
			            else
			            {
			                $msg['title'] = '';
			                $msg['message'] = '';
			                $msg['vibrate'] = 1;
			                $msg['sound'] = 1;
			                $result[] = $obj->send_notification_silent($value['gcm_id'],$msg,$extra,$value['device']);
			            } 
			        }
			    }
			}
            $messages = $this->qa_model->getMessages($session_id,$user_id);
            $sessions = $this->qa_model->getSessiondetail($event_id,$session_id);
            $sessions[0]['messages'] = $messages;
            $data = array(
                  'success'     => true,
                  'data'        => $sessions,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

}
?>   