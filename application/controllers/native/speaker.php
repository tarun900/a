<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Speaker extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/speaker_model');
        $this->load->model('native/message_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function speaker_list()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $lang_id=$this->input->post('lang_id');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {

                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->speaker_model->getUserId($token);
                $speakers = $this->speaker_model->getSpeakersListByEvent($event_id,$user_id,$lang_id);
                foreach ($speakers as $key => $value) 
                {   
                    if(!empty($value['Logo']))
                    {
                        $source_url = base_url()."assets/user_files/".$value['Logo'];
                        $info = getimagesize($source_url);
                        $new_name = "new_".$value['Logo'];
                        $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                        if ($info['mime'] == 'image/jpeg')
                        {   
                            $quality = 50;
                            $image = imagecreatefromjpeg($source_url);
                            imagejpeg($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/gif')
                        {   
                            $quality = 5;
                            $image = imagecreatefromgif($source_url);
                            imagegif($image, $destination_url, $quality);

                        }
                        elseif ($info['mime'] == 'image/png')
                        {   
                            $quality = 5;
                            $image = imagecreatefrompng($source_url);

                            $background = imagecolorallocatealpha($image,255,0,255,127);
                            imagecolortransparent($image, $background);
                            imagealphablending($image, false);
                            imagesavealpha($image, true);
                            imagepng($image, $destination_url, $quality);
                        }
                        $speakers[$key]['Logo'] = $new_name;
                    }
                }
                $data = array(
                    'speaker_list' => $speakers,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function speaker_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $speaker_id=$this->input->post('speaker_id');
        $user_id=$this->input->post('user_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $speaker_id!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $event = $this->event_model->getEventData($event_id);
                $speaker = $this->speaker_model->getSpeakerDetails($speaker_id,$event_id,$user_id);
                $message = $this->message_model->view_private_msg_coversation($user_id,$speaker_id,$event_id,$page_no,$limit);
                $total_pages=$this->message_model->total_pages($user_id,$speaker_id,$event_id,$limit);
                $agenda = $this->speaker_model->getConnectedAgenda($speaker_id,$event_id);
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda'    => ($agenda) ? $agenda : [],
                    'message' => $message,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'total_pages' => $total_pages,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function sendMessage()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $speaker_id=$this->input->post('speaker_id');
        $message_type=$this->input->post('message_type');
        $message=$this->input->post('message');
       
        if($event_id!='' && $token!='' && $speaker_id!='' && $user_id!='' && $message!='' && $message_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $moderator = $this->message_model->getModerators($speaker_id,$event_id);
                if(empty($moderator))
                {   
                    $message_data['Message']=$message;
                    $message_data['Sender_id']=$user_id;
                    $message_data['Event_id']=$event_id;
                    $message_data['Receiver_id']=$speaker_id;
                    $message_data['Parent']=0;
                    $message_data['image']="";
                    $message_data['Time']=date("Y-m-d H:i:s");
                    $message_data['ispublic']=$message_type;
                    $message_data['msg_creator_id']=$user_id;
                    $message_id = $this->message_model->saveMessage($message_data);

                    if($message_id!=0)
                    {
                        $current_date=date('Y/m/d');
                        $this->message_model->add_msg_hit($user_id,$current_date,$speaker_id,$event_id); 
                        $data = array(
                            'success' => true,
                            'message' => "Successfully Saved",
                            'message_id' => $message_id
                        );
                    }
                    else
                    {
                        $data = array(
                        'success' => false,
                        'message' => "Something is wrong.Please try again"
                        );
                    }
                }
                else
                {
                    foreach ($moderator as $key => $value) {
                        $message_data['Message']=$message;
                        $message_data['Sender_id']=$user_id;
                        $message_data['Event_id']=$event_id;
                        $message_data['Receiver_id']=$value;
                        $message_data['Parent']=0;
                        $message_data['image']="";
                        $message_data['Time']=date("Y-m-d H:i:s");
                        $message_data['ispublic']=$message_type;
                        $message_data['msg_creator_id']=$user_id;
                        $message_data['org_msg_receiver_id']=$speaker_id;
                        $message_id[] = $this->message_model->saveMessage($message_data);

                        
                    }
                    if(!empty($message_id))
                    {
                        $current_date=date('Y/m/d');
                        $this->message_model->add_msg_hit($user_id,$current_date,$speaker_id,$event_id); 
                        $data = array(
                            'success' => true,
                            'message' => "Successfully Saved",
                            'message_id' => $message_id
                        );
                    }
                    else
                    {
                        $data = array(
                        'success' => false,
                        'message' => "Something is wrong.Please try again"
                        );
                    }
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function msg_images_request()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $speaker_id=$this->input->post('speaker_id');
        $message_type=$this->input->post('message_type');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $speaker_id!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data1=$this->message_model->getMessageDetails($message_id);

                $newImageName = round(microtime(true) * 1000).".jpeg";
                $target_path= "././assets/user_files/".$newImageName; 
                $target_path1= "././assets/user_files/thumbnail/";                 
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $update_data['image'] =$newImageName;
                if($message_data1[0]['image']!='')
                {
                    $arr=json_decode($message_data1[0]['image']);
                    $arr[]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                else
                {

                    $arr[0]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                $a[0]=$newImageName;
                $message_data['Message']='';
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$speaker_id;
                $message_data['Parent']=$message_id;
                $message_data['image']=json_encode($a);
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']=$message_type;
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    //$this->message_model->add_msg_hit($user_id,$current_date,$attendee_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_message()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function make_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $user_id=$this->input->post('user_id');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($_FILES['image']['name']!='')
                {
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    if($newImageName!='')
                    {
                        $arr[0]=$newImageName;
                        $comment_arr['image']=json_encode($arr);
                    }
                }
                $comment_arr['comment']=$comment;
                $comment_arr['user_id']=$user_id;
                $comment_arr['msg_id']=$message_id;
                $comment_arr['Time']=date("Y-m-d H:i:s");
               
                $this->message_model->make_comment($comment_arr,$event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $comment_id=$this->input->post('comment_id');
        if($event_id!='' && $token!='' && $comment_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $speaker_id=$this->input->post('speaker_id');
        if($event_id!='' && $speaker_id!='')
        {
            
                $images = $this->message_model->getImages($event_id,$speaker_id);
                if($images != ''){
                     $data = array(
                    'images' => json_decode($images),
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data
                    );
                }else{
                     $data = array(
                          'success' => false,
                          'message' => "No images found"
                        );
                }
               
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function speaker_view_unread_count()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $speaker_id=$this->input->post('speaker_id');
        $user_id=$this->input->post('user_id');
        $lang_id=$this->input->post('lang_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $speaker_id!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $event = $this->event_model->getEventData($event_id);
                $speaker = $this->speaker_model->getSpeakerDetails($speaker_id,$event_id,$user_id,$lang_id);
                //$message = $this->message_model->view_private_msg_coversation($user_id,$speaker_id,$event_id,$page_no,$limit);
                $unread_count = $this->message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$speaker_id);
                //$total_pages=$this->message_model->total_pages($user_id,$speaker_id,$event_id,$limit);
                $agenda = $this->speaker_model->getConnectedAgenda($speaker_id,$event_id,$lang_id);
                $document = $this->speaker_model->getSpeakerDocument($speaker_id,$event_id,$lang_id);
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda'    => ($agenda) ? $agenda : [],
                    'document' => $document,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    //'total_pages' => $total_pages,
                    'unread_count' =>$unread_count[0]['unread_count'],
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function speaker_view_new()
    {   
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $speaker_id=$this->input->post('speaker_id');
        $user_id=$this->input->post('user_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $speaker_id!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $event = $this->event_model->getEventData($event_id);
                $speaker = $this->speaker_model->getSpeakerDetailsNew($speaker_id,$event_id,$user_id);
                $message = $this->message_model->view_private_msg_coversation($user_id,$speaker_id,$event_id,$page_no,$limit);
                $total_pages=$this->message_model->total_pages($user_id,$speaker_id,$event_id,$limit);
                $agenda = $this->speaker_model->getConnectedAgenda($speaker_id,$event_id);
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda'    => ($agenda) ? $agenda : [],
                    'message' => $message,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'total_pages' => $total_pages,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}