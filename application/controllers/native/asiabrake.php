<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Asiabrake extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_template_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/asiabrake_model');
		include('application/libraries/nativeGcm.php');
        
        $this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }


	public function getDefaultEvent()
    {
        $event = $this->asiabrake_model->getDefaultEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function exhibitor_list()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
               // $user_id = $this->asiabrake_model->getUserIdByToken($token);
                $exhibitor_list = $this->asiabrake_model->getExhibitorList($event_id);
                $meeting_data = $this->asiabrake_model->getAllMeetingRequest1($event_id,$token);
                $data = array(
                    'exhibitor_list' => $exhibitor_list,
                    'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                    'menu' => $this->menu_list,

                    'cmsmenu' => $this->cmsmenu
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function exhibitorSearch()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $keywords=$this->input->post('keywords');
        if($event_id!=''  && $keywords!='')
        {
            $data = $this->asiabrake_model->getSerachableRecords($event_id,$keywords);
            $meeting_data = $this->asiabrake_model->getAllMeetingRequest1($event_id,$token);
            $data = array(
                'exhibitor_list' => $data,
                'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}