<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sdlexpo extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/sdlexpo_model');
    }
    public function getDefaultEvent()
    {
        $event = $this->sdlexpo_model->getSdlEvent();
        if(!empty($event))
        {
            foreach ($event as $key => $value) 
            {
               
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
}
?>   