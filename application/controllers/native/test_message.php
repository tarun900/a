	<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test_message extends CI_Controller 
{
	public $menu;
	public $cmsmenu;
	
	function __construct() 
	{	
		parent::__construct();
		$this->load->model('native/app_login_model');
		$this->load->model('native/cms_model');
		$this->load->model('native/event_model');
		$this->load->model('native/test_message_model');
		$this->load->model('native/attendee_model');
		$this->load->model('native/speaker_model');
		$this->load->model('native/settings_model');
		$this->load->model('native/gamification_model');
		include('application/libraries/nativeGcm.php');
		
		$this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
		$this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

		foreach ($this->cmsmenu as $key => $values) 
		{
			$cmsbannerimage_decode = json_decode($values['Images']);
			$cmslogoimage_decode = json_decode($values['Logo_images']);
			$this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
			$this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
		}
	}
	public function getPublicMessages()
	{
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $event_type!='' && $page_no!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->test_message_model->view_public_msg_coversation($event_id,$page_no,$limit);
				$total_pages=$this->test_message_model->public_msg_total_pages($event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);

	}
	public function publicMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		if($event_id!='' && $message!='' && $token!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data['Message']=$message;
				$message_data['Sender_id']=$user_id;
				$message_data['Event_id']=$event_id;
				$message_data['Parent']=0;
				$message_data['image']="";
				$message_data['Time']=date("Y-m-d H:i:s");
				$message_data['ispublic']='1';
				$message_data['msg_creator_id']=$user_id;
				$message_id = $this->test_message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Successfully Saved",
						'message_id' => $message_id
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function public_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_data1 	= $this->test_message_model->getMessageDetails($message_id);
				$newImageName 	= round(microtime(true) * 1000).".jpeg";
				$target_path 	= "././assets/user_files/".$newImageName; 
				$target_path1 	= "././assets/user_files/thumbnail/";         

				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				
				$update_data['image'] = $newImageName;
				if($message_data1[0]['image']!='')
				{
					$arr=json_decode($message_data1[0]['image']);
					$arr[]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->test_message_model->updateMessageImage($message_id,$update_arr);
				}
				else
				{

					$arr[0]=$newImageName;
					$update_arr['image'] = json_encode($arr);
					$this->test_message_model->updateMessageImage($message_id,$update_arr);
				}
				$a[0]                           = $newImageName;
				
				$message_data['Message']        = '';
				$message_data['Sender_id']      = $user_id;
				$message_data['Event_id']       = $event_id;
				$message_data['Parent']         = $message_id;
				$message_data['image']          = json_encode($a);
				$message_data['Time']           = date("Y-m-d H:i:s");
				$message_data['ispublic']       = '1';
				$message_data['msg_creator_id'] = $user_id;

				$message_id = $this->test_message_model->savePublicMessage($message_data);
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_message()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=$this->input->post('message_id');
		if($event_id!=''  && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->test_message_model->delete_message($message_id);
				$data = array(
					'success' => true,
					'message' => "Successfully deleted"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function make_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$comment=$this->input->post('comment');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				if($_FILES['image']['name']!='')
				{
					$newImageName = round(microtime(true) * 1000).".jpeg";
					$target_path= "././assets/user_files/".$newImageName; 
					$target_path1= "././assets/user_files/thumbnail/";                 
					move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
					copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
					if($newImageName!='')
					{
						$arr[0]=$newImageName;
						$comment_arr['image']=json_encode($arr);
					}
				}
				$comment_arr['comment']=$comment;
				$comment_arr['user_id']=$user_id;
				$comment_arr['msg_id']=$message_id;
				$comment_arr['Time']=date("Y-m-d H:i:s");;
				$this->test_message_model->make_comment($comment_arr,$event_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_comment()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$comment_id=$this->input->post('comment_id');
		if($event_id!='' && $token!='' && $comment_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$this->test_message_model->delete_comment($comment_id);
				$data = array(
					'success' => true,
					'message' => "Successfully"
					);
			}
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getAllSpeakersAttendee()
	{
		$event_id=$this->input->post('event_id');
		$event_type=$this->input->post('event_type');
		$token=$this->input->post('token');
		if($event_id!='' && $event_type!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$event=$this->event_model->getEvent($event_id);
				$speakers = $this->speaker_model->getSpeakersListByEventId($event_id);
				if($event[0]['show_attendee_menu']=='1'){
					$attendee = $this->attendee_model->getAttendeeListByEventId($event_id);
				}
				else
				{
					$attendee=array();
				}
				$exhibitor = $this->test_message_model->getExhibitorListByEventId($event_id);

				foreach ($speakers as $key => $value) {
					$speakers[$key]['type'] = "7";
				}
				foreach ($attendee as $key => $value) {
					$attendee[$key]['type'] = "2";
				}
				foreach ($exhibitor as $key => $value) {
					$exhibitor[$key]['type'] = "3";
				}
				$data[0]['type'] = "Speakers";
				$data[0]['data'] = $speakers;

				$data[1]['type'] = "Attendees";
				$data[1]['data'] = $attendee;

				$data[2]['type'] = "Exhibitors";
				$data[2]['data'] = $exhibitor;
				$data = array(
					'success' => true,
					'data'	=> $data,
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getPrivateMessages()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->test_message_model->view_private_section_msg_coversation($event_id,$user_id,$page_no,$limit);
				foreach ($message as $key => $value) {
					$string = $value['message'];
					if($string != strip_tags($string)) 
					{
						$message[$key]['message'] = strip_tags($value['message']);
						$message[$key]['is_clickable'] = '1';
					}
					else
					{
						$message[$key]['is_clickable'] = '0';
					}	
				}
				$total_pages=$this->test_message_model->private_msg_total_pages($user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function privateMessageSend()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		$receiver_id=json_decode($this->input->post('receiver_id'));

		if($event_id!='' && $message!=''  && $user_id!='' && !empty($receiver_id))
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$message_id = $this->test_message_model->savePrivateMessage($this->input->post());
				if(!empty($message_id))
				{
					$moderator = $this->test_message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->test_message_model->getGcmIds($receiver_id);
						$device = $this->test_message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->test_message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->test_message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->test_message_model->getSender($user_id);
					
						
					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm($event_id);
						$template = $this->test_message_model->getNotificationTemplate($event_id,'Message');
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $template['Content'];
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);
							$extra['title'] = $template['Slug'];

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

                          
							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
							
                        }
                        else
                        {
							$msg['title'] = $template['Slug'];
							$msg['message'] = $template['Content'];
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = "$message_id[$key]";
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							$data = array(
								'success' => true,
								'message' => "Successfully Saved",
								'message_id' => $message_id,

								);
						}
						$this->event_model->sendEmailToAttendees($event_id,$template['Content'],$value['Email'],"Notification");
					}
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function private_msg_images_request()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=json_decode($this->input->post('message_id'));
		$receiver_id=json_decode($this->input->post('receiver_id'));

		if($event_id!='' && $token!='' && $user_id!='' && !empty($receiver_id) && !empty($message_id) && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
				foreach ($receiver_id as $key => $value) 
				{
					$message_data1=$this->test_message_model->getMessageDetails($message_id[$key]);
					if($key==0)
					{
						$newImageName = round(microtime(true) * 1000).".jpeg";
						$target_path= "././assets/user_files/".$newImageName; 
						$target_path1= "././assets/user_files/thumbnail/";                 
						move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
						copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
						$update_data['image'] =$newImageName;
						$old_image=$newImageName;
					}
					else
					{
						$update_data['image'] = $old_image;
					}
					if($message_data1[0]['image']!='')
					{
						$arr=json_decode($message_data1[0]['image']);
						$arr[]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->test_message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					else
					{

						$arr[0]=$newImageName;
						$update_arr['image']=json_encode($arr);
						$this->test_message_model->updateMessageImage($message_id[$key],$update_arr);
					}
					$a[0]=$newImageName;
					$message_data['Message']='';
					$message_data['Sender_id']=$user_id;
					$message_data['Receiver_id']=$value;
					$message_data['Event_id']=$event_id;
					$message_data['Parent']=$message_id[$key];
					$message_data['image']=json_encode($a);
					$message_data['Time']=date("Y-m-d H:i:s");
					$message_data['ispublic']='0';
					$message_data['msg_creator_id']=$user_id;
					$this->test_message_model->savePrivateImageImage($message_data);


				}
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getImageList()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$message_id=$this->input->post('message_id');
		if($event_id!='' && $message_id!='')
		{
			
				$images = $this->test_message_model->getImages($event_id,$message_id);

				$data = array(
					'images' => json_decode($images),
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	// 16 Aug 2016
	public function notificationCounter()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$token=$this->input->post('token');
		if($event_id == "447")
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		else
		{
			if($event_id!='' && $user_id!='')
			{
				$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
				if (empty($user)) 
				{
					$data = array(
						'success' => false,
						'data' => array(
							'msg' => 'Please check token or event.'
							)
						);   
				} 
				else 
				{
					$notify_data = $this->test_message_model->getNotificationCounter($event_id,$user_id);
					$data = array(
						'count' => json_decode(count($notify_data)),
						'notify_data'  => $notify_data,
						);
					
					$data = array(
						'success' => true,
						'data' => $data
						);
				}
			}
			else
			{
				$data = array(
					'success' => false,
					'message' => "Invalid parameters"
					);
			}
		}
		echo json_encode($data);
	}
	public function messageRead()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');
		$message_type=$this->input->post('message_type'); // public=1,private=0

		if($event_id!='' && $user_id!=''){
			$result = $this->test_message_model->updateMessageReadStatus($event_id,$user_id,$message_type);

			if($result){
				$data = array(
					'success' => true,
					);
			}else{
				$data = array(
					'success' => false,
					'message' => "Something went wrong. Please try again.",
					);
			}
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getNotificationListing()
	{
		$event_id=$this->input->post('event_id');
		$user_id=$this->input->post('user_id');

		if($event_id!='' && $user_id!=''){
			$result = $this->test_message_model->getNotificationListing($event_id,$user_id);

			
		$data = array(
			'success' => true,
			'data'	=> $result,
		);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function deleteNotification()
	{
		$notification_id 	= $this->input->post('notification_id');
		$user_id 			= $this->input->post('user_id');

		if($notification_id!='' && $user_id!='')
		{
			$data1['umn_id'] 	= $notification_id;
			$data1['user_id'] 			= $user_id;
			$result = $this->test_message_model->deleteNotification($data1);

			$data = array(
				'success' 	=> true,
				'message'	=> 'Notification deleted successfully.',
			);
			
		}
		else{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
			);
		}
		echo json_encode($data);
	}

	public function getPrivateMessagesConversation()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		$sender_id=$this->input->post('sender_id');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->test_message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$sender_id);
				$sender_detail = $this->test_message_model->getSenderDetails($sender_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					/*if($string != strip_tags($string)) 
					{*/
						$message[$key]['message'] = strip_tags($value['message']);
						//$message[$key]['is_clickable'] = '1';
					/*}
					else
					{
						$message[$key]['is_clickable'] = '0';
					}	*/
				}
				$total_pages=$this->test_message_model->private_msg_total_pages_list($sender_id,$user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'sender_detail' => $sender_detail,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function getPrivateUnreadMessagesList()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->test_message_model->view_private_unread_message_list($event_id,$user_id,$page_no,$limit);
				
				
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$data = array(
					'messages' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function privateMessageSendImage()
	{

		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		//$receiver_id=json_decode($this->input->post('receiver_id'));
		$receiver_id=$this->input->post('receiver_id');
		$limit = 10; $page_no = 1;
		if($event_id!='' && $token!='' && $user_id!='' && $receiver_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
					
				$newImageName = round(microtime(true) * 1000).".jpeg";
				$target_path= "././assets/user_files/".$newImageName; 
				$target_path1= "././assets/user_files/thumbnail/";                 
				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				$update_data['image'] =$newImageName;
				$old_image=$newImageName;
				
				$message_data['Message']='';
				$message_data['Sender_id']=$user_id;
				$message_data['Receiver_id']=$receiver_id;
				$message_data['Event_id']=$event_id;
				$arr[0] = $newImageName;
				$message_data['image']=json_encode($arr);
				$message_data['Time']=date("Y-m-d H:i:s");
				$message_data['ispublic']='0';
				$message_data['msg_creator_id']=$user_id;
				$message_id = $this->test_message_model->savePrivateImageImageOnly($message_data);


				$message = $this->test_message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
				$sender_detail = $this->test_message_model->getSenderDetails($receiver_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					
					$message[$key]['message'] = strip_tags($value['message']);
					
				}
				$total_pages=$this->test_message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);
				$array = array(
						'messages' => ($message) ? $message : [],
						'total_pages' => $total_pages,
						'sender_detail' => $sender_detail,
						);

				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Success saved",
						'data' => $array,
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function privateMessageSendText()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message=$this->input->post('message');
		$receiver_id=$this->input->post('receiver_id');
		
		if($event_id!='' && $message!=''  && $user_id!='' && !empty($receiver_id))
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit = 10;
				$_POST['receiver_id'] = $this->test_message_model->getUserIdByEx($receiver_id,$event_id);
				$message_id = $this->test_message_model->savePrivateMessageText($this->input->post());

				if(!empty($message_id))
				{
					$moderator = $this->test_message_model->getModerators($speaker_id,$event_id);
					if(empty($moderator))
					{
						$gcm_ids = $this->test_message_model->getGcmIds($receiver_id);
						$device = $this->test_message_model->getDevice($receiver_id);
					}
					else
					{
						$gcm_ids = $this->test_message_model->getModeratorsGcmIds($receiver_id,$event_id);
						$device = $this->test_message_model->getModeratorDevice($receiver_id,$event_id);
					}
					$sender = $this->test_message_model->getSender($user_id);
					
					$message = $this->test_message_model->view_private_section_msg_coversation_list($event_id,$user_id,$page_no,$limit,$receiver_id);
					$sender_detail = $this->test_message_model->getSenderDetails($receiver_id,$event_id);		

					foreach ($message as $key => $value) {
						$string = $value['message'];
						
						$message[$key]['message'] = strip_tags($value['message']);
						
					}
					$total_pages=$this->test_message_model->private_msg_total_pages_list($receiver_id,$user_id,$event_id,$limit);

					foreach ($gcm_ids as $key => $value) {
						$obj = new Gcm($event_id);
						$template = $this->test_message_model->getNotificationTemplate($event_id,'Message');
						if($device[$key]['device'] == "Iphone")
                        {
                            $msg =  $template['Content'];
                            $extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							$extra['title'] = $template['Slug'];

                            $result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);

							
                        }
                        else
                        {
							$msg['title'] = $template['Slug'];
							$msg['message'] = $template['Content'];
							
							$extra['sender_name'] = $sender;
							$extra['message_type'] = 'Private';
							$extra['message_id'] = $user_id;
							$extra['event'] = $this->settings_model->event_name($event_id);
							
							$msg['vibrate'] = 1;
							$msg['sound'] = 1;
							$result = $obj->send_notification($value['gcm_id'],$msg,$extra,$device[$key]['device']);
							
						}
						$this->event_model->sendEmailToAttendees($event_id,$template['Content'],$value['Email'],"Notification");
					}
                    
                    $this->gamification_model->add_user_point($user_id,$event_id,1);
					$array = array(
						'messages' => $message,
						'total_pages' => $total_pages,
						'sender_detail' => $sender_detail,
						);
					$data = array(
						'success' => true,
						'message' => "Successfully Saved",
						'message_id' => $message_id,
						'data' => $array,
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something is wrong.Please try again"
						);
				}
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_private_message()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$message_id=json_decode($this->input->post('message_id'));
		$receiver_id=json_decode($this->input->post('receiver_id'));
		
		
		if($event_id!='' && $token!='' && $user_id!='' && !empty($receiver_id) && !empty($message_id) && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				

				$message_id = $this->test_message_model->delete_private_message($message_id);
				
				if($message_id!=0)
				{
					$data = array(
						'success' => true,
						'message' => "Message Deleted"
						);
				}
				else
				{
					$data = array(
						'success' => false,
						'message' => "Something went wrong.Please try again"
						);
				}
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function get_all_contacts()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->test_message_model->get_all_contacts($event_id,$user_id,$page_no,$limit);
								
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);
       
            	$block_all = $this->test_message_model->is_block_all($user_id,$event_id);
            	$data = array(
            		'is_all_block' => $block_all,
					'user_list' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function get_my_messages()
	{	
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$data = $this->test_message_model->get_my_messages($event_id,$user_id,$page_no,$limit);
            	$groups = $this->test_message_model->get_groups($event_id,$user_id);
				$data = array_merge($data,$groups);
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);


            	foreach ($data as $key => $value)
            	{	
            		if($value['is_group'] == 0)
            		{
	            		$message = $this->test_message_model->get_last_message($event_id,$user_id,$page_no,$limit,$value['Id']);
	            		$data[$key]['message'] = $message['message'];
	            		$data[$key]['image'] = $message['image'];
	            		$data[$key]['time'] = $message['time_stamp'];
	            		$data[$key]['admin_id'] = "";
	            		$data[$key]['members_id'] = "";
	            		$data[$key]['count'] = 0;
            		}
            		else
            		{
	            		$data[$key]['message'] = ($value['message']) ? $value['message']: "";
	            		$data[$key]['image'] = ($value['image']) ? $value['image']: "";
	            		$data[$key]['time'] = ($value['message']) ? $value['time']: "";
            		}
            	}

            	function sortByTime($a, $b) {
            		 if($a['time'] != "" && $b['time'] != "")
   					 return $a['time'] - $b['time'];
   					 else
   					 return 1;
				}
				usort($data, 'sortByTime');

            	$data = array(
					'messages' => $data,
					//'groups' => $groups
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function delete_converstaion()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$receiver_id=$this->input->post('receiver_id');

		if($event_id!='' && $user_id!='' && $receiver_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->test_message_model->delete_converstaion($event_id,$user_id,$receiver_id);

				$data = array(
					'success' => true,
					'message' => "Conversation Deleted"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function block_user()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$block_id = $this->input->post('block_id');

		if($event_id!='' && $block_id!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$data['block_by'] = $user_id;
				$data['block'] = $block_id;
				$data['event_id'] = $event_id;
				$data['date'] = date('Y-m-d H:i:s');
				$data = $this->test_message_model->block_user($data);
				
				$data = array(
					'success' => true,
					'data' => "User Blocked"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function unblock_user()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$block_id = $this->input->post('block_id');

		if($event_id!='' && $block_id!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$data['block_by'] = $user_id;
				$data['block'] = $block_id;
				$data['event_id'] = $event_id;
				$data = $this->test_message_model->unblock_user($data);
				
				$data = array(
					'success' => true,
					'data' => "User Unblocked"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function getPrivateMessagesConversationNew()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		$sender_id=$this->input->post('sender_id');
		if($event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$limit=10;
				$message = $this->test_message_model->view_private_msg_coversation_list_new($event_id,$user_id,$page_no,$limit,$sender_id);
				$sender_detail = $this->test_message_model->getSenderDetails($sender_id,$event_id);		

				foreach ($message as $key => $value) {
					$string = $value['message'];
					/*if($string != strip_tags($string)) 
					{*/
						$message[$key]['message'] = strip_tags($value['message']);
						//$message[$key]['is_clickable'] = '1';
					/*}
					else
					{
						$message[$key]['is_clickable'] = '0';
					}	*/
				}
				$total_pages=$this->test_message_model->private_msg_total_pages_list($sender_id,$user_id,$event_id,$limit);
				$data = array(
					'messages' => $message,
					'total_pages' => $total_pages,
					'sender_detail' => $sender_detail,
					/*'menu' => $this->menu_list,
					'cmsmenu' => $this->cmsmenu*/
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function createGroup()
	{	
		
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$members_id=json_decode($this->input->post('members_id'));
		$group_name = $this->input->post('group_name');
		
		if($event_id!='' && $members_id!='' && $user_id!='' && $group_name!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
			
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{

				$data['admin_id'] = $user_id;
				$data['members_id'] = implode(',',$members_id);
				$data['name'] = $group_name;
				$data['event_id'] = $event_id;
				$data['date'] = date('Y-m-d H:i:s');
				$id = $this->test_message_model->create_group($data);
				$this->test_message_model->add_join_time($data['members_id'],$id);
				$group_details = $this->test_message_model->get_group_details($id,$event_id);

				$data = array(
					'messages' => "Group Create Successfully",
					'group_details' => $group_details
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function updateGroup()
	{			
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$members_id=json_decode($this->input->post('members_id'));
		$group_name = $this->input->post('group_name');
		$group_id = $this->input->post('group_id');
		
		if($event_id!='' && $members_id!='' && $user_id!='' && $group_name!='' && $group_id!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
			
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{

				$data['admin_id'] = $user_id;
				$data['members_id'] = implode(',',$members_id);
				$data['name'] = $group_name;
				$data = $this->test_message_model->update_group($group_id,$data);
				$group_details = $this->test_message_model->get_group_details($group_id,$event_id);

				$data = array(
					'messages' => "Group Updated Successfully",
					'group_details' => $group_details
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function deleteGroup()
	{			
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		
		if($event_id!='' && $user_id!='' && $group_id!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$data = $this->test_message_model->delete_group($group_id);

				$data = array(
					'messages' => "Group Deleted Successfully",
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function send_group_message()
	{			
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		$message = $this->input->post('message');
		
		if($event_id!='' && $user_id!='' && $group_id!='' &&$message!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{	
				$data['group_id'] = $group_id;
				$data['sender_id'] = $user_id;
				$data['message'] = $message;
				$data['event_id'] = $event_id;
				$data['time'] = date('Y-m-d H:i:s');

				$data = $this->test_message_model->send_group_message($data);

				$data = $this->test_message_model->get_group_message($group_id,$event_id,$user_id);
				$group_details = $this->test_message_model->get_group_details($group_id,$event_id);

				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

				$data = array(
					'messages' => $data,
					'total_page' => $total_page,
					'group_details' => $group_details
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function get_group_message()
	{	
		//error_reporting(E_ALL);		
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		$page_no = $this->input->post('page_no');
		
		if($event_id!='' && $user_id!='' && $group_id!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{				
				$data = $this->test_message_model->get_group_message($group_id,$event_id,$user_id);
				$group_details = $this->test_message_model->get_group_details($group_id,$event_id);
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

				$data = array(
					'messages' => $data,
					'total_page' => $total_page,
					'group_details' => $group_details
					);
				$data = array(
					'success' => true,
					'data' => $data
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function delete_group_message()
	{	
		error_reporting(E_ALL);		
		
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		$message_id =  $this->input->post('message_id');
		
		if($event_id!='' && $user_id!='' && $group_id!='' && $message_id!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{	
				$data['group_id'] = $group_id;
				$data['message_id']	= $message_id;
				$data['user_id'] =  $user_id;
				$data = $this->test_message_model->delete_group_message($data);

				$data = array(
					'success' => true,
					'message' => "Message Deleted Successfully"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function left_group()
	{		
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		if($event_id!='' && $user_id!='' && $group_id!='')
		{	
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{	
				$data['group_id'] = $group_id;
				
				$data['user_id'] = $user_id;
				$data = $this->test_message_model->left_group($data);

				$data = array(
					'success' => true,
					'message' => "Group Left Successfully"
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function GroupMessageSendImage()
	{

		$event_id=$this->input->post('event_id');
		$group_id=$this->input->post('group_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');

		$limit = 10; $page_no = 1;
		if($event_id!='' && $token!='' && $user_id!='' && $_FILES['image']['name']!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$old_image="";
					
				$newImageName = round(microtime(true) * 1000).".jpeg";
				$target_path= "././assets/user_files/".$newImageName; 
				$target_path1= "././assets/user_files/thumbnail/";                 
				move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
				copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
				$update_data['image'] =$newImageName;
				$old_image=$newImageName;
				
				$message_data['sender_id']=$user_id;
				$message_data['group_id']=$group_id;
				$message_data['event_id']=$event_id;
				$arr[0] = $newImageName;
				$message_data['image']=json_encode($arr);
				$message_data['time']=date("Y-m-d H:i:s");
				$message_id = $this->test_message_model->saveGroupImageOnly($message_data);


				$data = $this->test_message_model->get_group_message($group_id,$event_id,$user_id);
				$group_details = $this->test_message_model->get_group_details($group_id,$event_id);
				
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

				$data = array(
					'messages' => $data,
					'total_page' => $total_page,
					'group_details' => $group_details
					);
				
				$data = array(
					'success' => true,
					'data' => $data
					);
				
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
	public function block_all()
	{
		$event_id=$this->input->post('event_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');

		if($event_id!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				$data['block_all'] = $user_id;
				$data['event_id'] = $event_id;
				$msg = $this->test_message_model->block_all($data);
				
				$data = array(
					'success' => true,
					'is_block_all' => $msg['state'],
					'data' => $msg['msg']
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
	public function get_selected_contacts()
	{
		$event_id=$this->input->post('event_id');
		$id=$this->input->post('group_id');
		$token=$this->input->post('token');
		$user_id=$this->input->post('user_id');
		$page_no=$this->input->post('page_no');
		if($id!='' && $event_id!='' && $page_no!='' && $user_id!='')
		{
			$user = $this->app_login_model->check_token_with_event($token,$event_id,1);
			if (empty($user)) 
			{
				$data = array(
					'success' => false,
					'data' => array(
						'msg' => 'Please check token or event.'
						)
					);   
			} 
			else 
			{
				
				$data = $this->test_message_model->get_all_contacts($event_id,$user_id,$page_no,$limit);
								
				$limit          = 10;
            	$page_no        = (!empty($page_no))?$page_no:1;
            	$start          = ($page_no-1)*$limit;
            	$total          = count($data);
            	$total_page     = ceil($total/$limit);
            	$data           = array_slice($data,$start,$limit);

            	$block_all = $this->test_message_model->is_block_all($user_id,$event_id);

            	$members_id = $this->test_message_model->get_group_members($id,$event_id);
            	foreach ($data as $key => $value)
            	{
            		if(in_array($value['Id'],$members_id['members_id']))
            		{	
            			$data[$key]['is_selected'] = 1;
            		}
            		else
            		{
            			$data[$key]['is_selected'] = 0;
            		}
            	}
            	$data = array(
            		'is_all_block' => $block_all,
            		'group_name' => $members_id['name'],
					'user_list' => $data,
					);

				$data = array(
					'success' => true,
					'data' => ($data) ? $data : [],
					'total_page' => $total_page
					);
			}
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);		
	}
}