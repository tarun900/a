<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agenda extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
       // error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_template_model');
        $this->load->model('native/agenda_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        date_default_timezone_set("UTC");
        $this->menu = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }

    public function getAgendaByTime()
    {
        //$start_fun_time = microtime(true);
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
              
                $agenda = $this->agenda_model->getAllAgendaByTimeEvent($event_id,null,$user_id,$lang_id,$category_id);
                
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);
						
						if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}	


                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                   /* 'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        /*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/agenda/getAgendaByTime function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
        echo json_encode($data);
    }
    public function getAgendaByType()
    {
        //$start_fun_time = microtime(true);
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('_token');
        $lang_id = $this->input->post('lang_id');
        $category_id = $this->input->post('category_id');

        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda = $this->agenda_model->getAllAgendaByTypeEvent($event_id,null,null,$user_id,$lang_id,$category_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

                        
						if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}	


 						/*$a =  strtotime($value['Start_date']);
	                    $start_date=date("l, M jS, Y",$a);

	                    $a =  strtotime($value['End_date']);
	                    $end_date=date("l, M jS, Y",$a);*/

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                    
                }
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }


                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                   /* 'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        /*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/agenda/getAgendaByType function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
        echo json_encode($data);
    }
    public function getUserAgendaByType()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!=''  && $user_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                $meeting=$this->agenda_model->getAllMeetingsByType($event_id,$user_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                $str=$agenda_data[0]['agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }

                $agenda = $this->agenda_model->getAllAgendaByType($event_id,0,$agenda_id_arr,NULL,$lang_id);
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

						if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}	

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                    
                }
                if(!empty($meeting))
                {
                    foreach ($meeting as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) 
                        {

							if($date_format[0]['date_format'] == 0)
							{	
								$a =  strtotime($value['date']);
			                    $start_date=date("d/m/Y",$a);

							}
							else
							{	
								$a =  strtotime($value['date']);
			                    $start_date=date("m/d/Y",$a);
							}	
	                        $meeting[$key1]['data'][$key]['date']=$start_date;
                    	}
                  	} 
                }

                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->agenda_model->getShowSuggestionButton($user_id,$event_id),
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getUserAgendaByTime()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!='' && $user_id!='' )
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                $str=$agenda_data[0]['agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }

                $agenda = $this->agenda_model->getAllAgendaByTime($event_id,$agenda_id_arr,NULL,$lang_id);
                $meeting = $this->agenda_model->getAllMeetings($event_id,$user_id);

                if(!empty($meeting)){
                    foreach ($meeting as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['time']);
                        if($time_format[0]['time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);

                        $a =  strtotime($value['date']);
                        $start_date=date("l, M jS, Y",$a);

                       

                        $meeting[$key1]['date']=$start_date;
                        $meeting[$key1]['date_time']=$start_date;
                        $meeting[$key1]['data'][$key]['time']=$start_time;
						if($date_format[0]['date_format'] == 0)
							{	
								$a =  strtotime($value['date']);
			                    $start_date=date("d/m/Y",$a);

							}
							else
							{	
								$a =  strtotime($value['date']);
			                    $start_date=date("m/d/Y",$a);
							}	
	                        $meeting[$key1]['data'][$key]['date']=$start_date;
                        
                    }
                   } 
                    
                }
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

						if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}	

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);
                    }
                   } 
                    
                }
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->agenda_model->getShowSuggestionButton($user_id,$event_id),
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getAgendaById()
    {
        
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!='' && $event_type!='' && $agenda_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_value = $this->agenda_model->getAgendaById($event_id,$agenda_id,$user_id,$lang_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);
                if(!empty($agenda_value)){
           			
                    $Start_time =  strtotime($agenda_value[0]['Start_time']);
                    $start_time=date("H:i",$Start_time);
                    $bh_stime = date("H:i:s A",$Start_time);

                    $End_time =  strtotime($agenda_value[0]['End_time']);
                    $end_time=date("H:i",$End_time);

                    
                    $a =  strtotime($agenda_value[0]['Start_date']);
                    // $start_date=date("l, M jS, Y",$a);
                    $start_date_api = str_replace('-', '/', date("d-m-Y",$a));
                    $bh_sdate =date("m-d-Y",$a);

                    $a =  strtotime($agenda_value[0]['End_date']);
                    // $end_date=date("l, M jS, Y",$a);
                    $end_date_api = str_replace('-', '/', date("d-m-Y",$a));

					if($date_format[0]['date_format'] == 0)
					{	
						$a =  strtotime($agenda_value[0]['Start_date']);
	                    $start_date=date("d/m/Y",$a);

	                    $a =  strtotime($agenda_value[0]['End_date']);
	                    $end_date=date("d/m/Y",$a);
						
					}
					else
					{	
						$a =  strtotime($agenda_value[0]['Start_date']);
	                    $start_date=date("m/d/Y",$a);

	                    $a =  strtotime($agenda_value[0]['End_date']);
	                    $end_date=date("m/d/Y",$a);

					}	

                    $agenda_value[0]['Start_date']=$start_date;
                    $agenda_value[0]['Start_date_cal']=$start_date_api;
                    $agenda_value[0]['start_date_time']=$bh_sdate." ".$bh_stime;
                    $agenda_value[0]['End_date_cal']=$end_date_api;
                    $agenda_value[0]['End_date']=$end_date;
                    $agenda_value[0]['Start_time']=$start_time;
                    $agenda_value[0]['End_time']=$end_time;
                    $curr_date =date("Y-m-d");
                    $start1_date =date("Y-m-d",$a);
                    /*if($start1_date > $curr_date)
                        $agenda_value[0]['show_reminder_button']='1';
                    else*/
                        // #agenda_comments
                    $where['event_id'] = $event_id;
                    $where['user_id'] = $user_id;
                    $where['agenda_id'] = $agenda_id;
                    $agenda_value[0]['show_comment_box'] = $this->agenda_model->checkShowCommentBox($where);
                    $agenda_value[0]['show_reminder_button']='0';
                    $agenda_value[0]['session_image'] = $this->compress_image($agenda_value[0]['session_image']);

                }
     
                if($user_id!='')
                {
                    $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                }
                else
                {
                    $flag="login_0";
                }

                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda_value,
                    'reminder' =>  [],
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time'],
                    'user_flag' => $flag,
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
 
     /*public function saveUserAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');

        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $is_pending_agenda = $this->agenda_model->getIsPendingAgenda($event_id);
                $allow_clashing = $this->agenda_model->getAgendaData($agenda_id);
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0][($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if($agenda_id_arr[0]=="")
                    {
                        $agenda_id_arr = "";
                    }
                    if(in_array($agenda_id,$agenda_id_arr))
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"Agenda already saved"
                        );
                    }
                    else
                    {
                        if($allow_clashing[0]['allow_clashing']==0 && $this->agenda_model->get_overlapping_agenda($allow_clashing,$user_id,$is_pending_agenda) == true)
                        {

                            if($user_id!='')
                            {
                                $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                                $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                            }

                            $agenda_id_arr[]=$agenda_id;
                            
                             $agenda_id_str = (count($agenda_id_arr)==1)? $agenda_id : implode(',', $agenda_id_arr);
                           // $data['agenda_id']=$agenda_id_str;
                            $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id_str;
                            $this->agenda_model->updateUserAgenda($user_id,$data);
                            $data = array(
                                'success' => true,
                                'msg'=>"Successfully updated",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0

                            );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"You can not save this agenda. It is overlapping with another",
                        );
                    }
                    }
                }
                else
                {
                    if($allow_clashing[0]['allow_clashing']==0 && $this->agenda_model->get_overlapping_agenda($allow_clashing,$user_id,$is_pending_agenda) == true)
                    {
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                             $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                        }
                        $data['user_id']=$user_id;
                        //$data['agenda_id']=$agenda_id;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id;
                        $this->agenda_model->addUserAgenda($data);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Added.",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0
                        );
                }
                else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"You can not save this agenda. It is overlapping with another.",
                        );
                    }
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
    public function saveUserAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');

        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $is_pending_agenda = $this->agenda_model->getIsPendingAgenda($event_id);
                $allow_clashing = $this->agenda_model->getAgendaData($agenda_id);
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
               
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0][($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                   
                    if(in_array($agenda_id,$agenda_id_arr))
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"Agenda already saved",
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        if($allow_clashing[0]['allow_clashing']==0 && $this->agenda_model->get_overlapping_agenda($allow_clashing,$user_id,$is_pending_agenda) == true)
                        {
                            

                            if($user_id!='')
                            {
                                
                                $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                            }
                           
                            $agenda_id_arr = array_push($agenda_id_arr, $agenda_id);
                            
                             $agenda_id_str = (count($agenda_id_arr)==1)? $agenda_id : implode(',', $agenda_id_arr);
                             
                           // $data['agenda_id']=$agenda_id_str;
                            $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id_str;

                            $this->agenda_model->updateUserAgenda($user_id,$data);
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                            $data = array(
                                'success' => true,
                                'msg'=>"Successfully updated",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,

                            );
                    }
                    elseif($allow_clashing[0]['allow_clashing'] == 1)
                    {
                       
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                             $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                        }
                        $data['user_id']=$user_id;
                        //$data['agenda_id']=$agenda_id;
                        array_push($agenda_id_arr, $agenda_id);
                            
                         $agenda_id_str = (count($agenda_id_arr)==1)? $agenda_id : implode(',', $agenda_id_arr);
                        
                       // $data['agenda_id']=$agenda_id_str;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id_str;
                        $this->agenda_model->addUserAgenda($data);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Added.",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"You can not save this agenda. It is overlapping with another",
                            'agenda_id' => $agenda_id,

                        );
                    }
                    }
                }
                else
                {
                    if($allow_clashing[0]['allow_clashing']==0 && $this->agenda_model->get_overlapping_agenda($allow_clashing,$user_id,$is_pending_agenda) == true)
                    {
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                             $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                        }
                        $data['user_id']=$user_id;
                        //$data['agenda_id']=$agenda_id;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id;
                        $this->agenda_model->addUserAgenda($data);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Added.",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,
                        );
                }
                elseif($allow_clashing[0]['allow_clashing'] == 1)
                    {
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                             $max_people = $this->agenda_model->getMaxPeople($agenda_id,$user_id);
                        }
                        $data['user_id']=$user_id;
                        //$data['agenda_id']=$agenda_id;
                        $data[($is_pending_agenda) ? 'pending_agenda_id' : 'agenda_id']=$agenda_id;
                        $this->agenda_model->addUserAgenda($data);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Added.",
                                'user_flag' => $flag,
                                'Maximum_people' => ($max_people) ? $max_people : 0,
                                'agenda_id' => $agenda_id,
                        );
                    }
                else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"You can not save this agenda. It is overlapping with another.",
                            'agenda_id' => $agenda_id,
                        );
                    }
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*public function deleteUserAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false) 
                    {
                        unset($agenda_id_arr[$key]);
                      
                        if(count($agenda_id_arr)==0)
                        {
                            $this->agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['agenda_id']=$agenda_id_str;
                            $this->agenda_model->updateUserAgenda($user_id,$data);
                        }
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg'=>"Successfully deleted",
                            'user_flag' => $flag,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found."
                        );
                    }
                    
                }
                else
                {
                    $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found"
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
    public function deleteUserAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false) 
                    {
                        unset($agenda_id_arr[$key]);
                      
                        if(count($agenda_id_arr)==0)
                        {
                            $this->agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['agenda_id']=$agenda_id_str;
                            $this->agenda_model->updateUserAgenda($user_id,$data);
                        }
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg'=>"Successfully deleted",
                            'user_flag' => $flag,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found.",
                            'agenda_id' => $agenda_id,
                        );
                    }
                    
                }
                else
                {
                    $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found",
                            'agenda_id' => $agenda_id,
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*public function checkInAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getCheckInAgendaByUserId($user_id);
                $agenda_value = $this->agenda_model->getAgendaById($event_id,$agenda_id,$user_id);
                $enddate=$agenda_value[0]['End_date'].' '.$agenda_value[0]['End_time'];
                $cdate=date('Y-m-d H:i:s');
                if(!empty($agenda_value[0]['checking_datetime']))
                {
                   if(!empty($event_templates[0]['Event_show_time_zone']))
                   {
                        if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
                        { 
                            $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
                            $intoffset=$arr[1]*3600;
                            $intNew = abs($intoffset);
                            $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                        }
                        if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
                        {
                            $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
                            $intoffset=$arr[1]*3600;
                            $intNew = abs($intoffset);
                            $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                        }
                    }
                }
                else
                {
                  $agenda_value[0]['checking_datetime'] = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))-3600);
                }

                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['check_in_agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if(in_array($agenda_id,$agenda_id_arr))
                    {
                        $key = array_search($agenda_id,$agenda_id_arr);
                        unset($agenda_id_arr[$key]);
                        $agenda_id_str = implode(',', $agenda_id_arr);
                        $data['check_in_agenda_id']=$agenda_id_str;
                        $this->agenda_model->updateUserAgenda($user_id,$data);
                        $this->agenda_model->updateCheckIn($user_id,$agenda_id);
                        $data = array(
                            'success' => true,
                            'msg'=>"Checked In removed",
                            'btn_color' => '#428bca',
                        );
                    }
                    else
                    {
                        $rating_data = $this->agenda_model->getRatingDataByUserId($user_id,$event_id);
                        
                            $flag = 0;
                            foreach ($rating_data as $key => $value) {
                                if(!in_array($value['session_id'], $agenda_id_arr) && $value['session_id']!=$agenda_id){
                                    $flag = $value['session_id'];
                                }
                            }
                            if($flag != 0  )
                            {
                                $agenda_data = $this->agenda_model->getAgendaNameTimeById($flag);
                               
                                $data = array(
                                    'success' => false,
                                     'message1' => "You must rate: ",
                                     'message2'   => $agenda_data,
                                     'message3'   => "before you can Check In to this session.",
                                    'flag'=>1,
                                    
                                ); 
                            }
                            elseif(count($rating_data)==0)
                            {
                               
                                 foreach ($agenda_id_arr as $key => $value) {
                                   $agenda_data = $this->agenda_model->getAgendaNameTimeById($value);
                               
                                    $data = array(
                                        'success' => false,
                                         'message1' => "You must rate: ",
                                         'message2'   => $agenda_data,
                                         'message3'   => "before you can Check In to this session.",
                                        'flag'=>1,
                                        
                                    ); 
                                    break;
                                }
                            }
                            else
                            {
                                if($agenda_value[0]['checking_datetime'] <= $cdate  && $enddate >= $cdate)
                                {
                                    $agenda_id_arr[]=$agenda_id;
                                    $agenda_id_str = implode(',', $agenda_id_arr);
                                    $data['check_in_agenda_id']=$agenda_id_str;
                                    $this->agenda_model->updateUserAgenda($user_id,$data);
                                    $this->agenda_model->saveCheckIn($agenda_id,$user_id);
                                   // $this->agenda_model->updateAgendaCheckInDateTime($agenda_id);
                                    $data = array(
                                        'success' => true,
                                        'msg'=>"Successfully Checked In",
                                        'btn_color' => '#5cb85c',
                                    ); 
                                }
                                else
                                {
                                     $data = array(
                                            'success' => false,
                                            'msg'=>"You cannot check in for this session. It is either not started or it was finished.",
                                            'flag'=>0,
                                    );
                                }
                            }
                    }
                }
                else
                {
                    if($agenda_value[0]['checking_datetime'] <= $cdate  && $enddate >= $cdate)
                    {
                        $data['user_id']=$user_id;
                        $data['check_in_agenda_id']=$agenda_id;
                        $this->agenda_model->addUserAgenda($data);
                        $this->agenda_model->saveCheckIn($agenda_id,$user_id);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Checked In",
                                'btn_color' => '#5cb85c',
                        );
                    }
                    else
                    {
                         $data = array(
                                'success' => false,
                                'msg'=>"You cannot check in for this session. It is either not started or it was finished.",
                                'flag'=>0,
                        );
                    }
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters",
              'flag'=>'',
            );
        }
        echo json_encode($data);
    }*/
    public function checkInAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $event_data = $this->event_model->getEvent($event_id);
            $agenda_data=$this->agenda_model->getCheckInAgendaByUserId($user_id);
            $agenda_value = $this->agenda_model->getAgendaById($event_id,$agenda_id,$user_id);
            $cdate=date('Y-m-d H:i:s');
            if(!empty($event_data[0]['checking_datetime']))
            {
               $cdate = $this->getTimeZoneDate($event_data);
            }
            else
            {
              $event_data[0]['checking_datetime'] = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))-3600);
            }
            $str=$agenda_data[0]['check_in_agenda_id'];
            $agenda_id_arr=explode(',',$str);
            
            if(in_array($agenda_id, $agenda_id_arr))
            {
                $key = array_search($agenda_id, $agenda_id_arr);
                unset($agenda_id_arr[$key]);
                $agenda_id_str = implode(',', $agenda_id_arr);
                $data['check_in_agenda_id']=$agenda_id_str;
                $this->agenda_model->updateUserAgenda($user_id,$data);
                $this->agenda_model->updateCheckIn($user_id,$agenda_id);
                $data = array(
                    'success' => true,
                    'msg'=>"Check In removed",
                    'btn_color' => '#428bca',
                );
            }
            else
            {
                if($event_data[0]['checking_datetime'] <= $cdate)
                {
                    $str=$agenda_data[0]['agenda_id'];
                    $agenda_id_arr=explode(',',$str); 
                    $str=$agenda_data[0]['check_in_agenda_id'];
                    $check_in_agenda_id_arr=explode(',',$str); 
                    
                    $key = array_search($agenda_id, $agenda_id_arr);
                    $agenda_ids = array();
                    for($i=0;$i< $key;$i++)
                    {
                        $agenda_ids[] = $agenda_id_arr[$i];
                    }
                    $rating_data = $this->agenda_model->getRatingDataNew($user_id,($key == 0) ? $agenda_id_arr[$key] : $agenda_ids,$event_id);
                    if(empty($rating_data))
                    {
                        $data['user_id']=$user_id;
                        $data['check_in_agenda_id']=($key) ? $agenda_data[0]['check_in_agenda_id'].','.$agenda_id : $agenda_id;
                        $this->agenda_model->addUserAgenda($data);
                        $this->agenda_model->saveCheckIn($agenda_id,$user_id);
                        $data = array(
                                'success' => true,
                                'msg'=>"Successfully Checked In",
                                'btn_color' => '#5cb85c',
                        );
                    }
                    else
                    {
                        $rating_data = $this->agenda_model->getRatingDataNew($user_id,($key == 0) ? $agenda_id_arr[$key] : $agenda_ids,$event_id);
                        $agenda_data1 = $this->agenda_model->getAgendaNameTimeById($rating_data['agenda_id']);
                        $is_checkin = (in_array($rating_data['agenda_id'], $check_in_agenda_id_arr)) ? 0 : 1;
                        $data = array(
                            'success'   => false,
                            'message1'  => "You must rate: ",
                            'message2'  => ($agenda_data1) ? $agenda_data1 : new stdClass,
                            'message3'  => "before you can Check In to this session.",
                            'show_check_in' => $is_checkin,
                            'flag'      =>1,
     
                        ); 
                    }
                }
                else
                {
                    $data = array(
                            'success' => false,
                            'msg'=>"You cannot check in for this session. It is not started.",
                            'flag'=>0,
                    );
                }    
            }

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getTimeZoneDate($event_templates)
    {
        if(!empty($event_templates[0]['Event_show_time_zone']))
           {
                if(strpos($event_templates[0]['Event_show_time_zone'],"-")==true)
                { 
                    $arr=explode("-",$event_templates[0]['Event_show_time_zone']);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
                }
                if(strpos($event_templates[0]['Event_show_time_zone'],"+")==true)
                {
                    $arr=explode("+",$event_templates[0]['Event_show_time_zone']);
                    $intoffset=$arr[1]*3600;
                    $intNew = abs($intoffset);
                    $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
                }
            }
            return $cdate;
    }
   
    public function saveRating()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $rating=$this->input->post('rating');
       /* $agenda_value = $this->agenda_model->getAgendaById($event_id,$agenda_id,$user_id);
        $startdate=$agenda_value[0]['Start_date'].' '.$agenda_value[0]['Start_time'];
        $enddate=$agenda_value[0]['End_date'].' '.$agenda_value[0]['End_time'];
        $cdate=date('Y-m-d H:i:s');
        if(!empty($agenda_value[0]['Event_show_time_zone']))
        {
          if(strpos($agenda_value[0]['Event_show_time_zone'],"-")==true)
          { 
            $arr=explode("-",$agenda_value[0]['Event_show_time_zone']);
            $intoffset=$arr[1]*3600;
            $intNew = abs($intoffset);
            $cdate = date('Y-m-d H:i:s',strtotime($cdate)-$intNew);
          }
          if(strpos($agenda_value[0]['Event_show_time_zone'],"+")==true)
          {
            $arr=explode("+",$agenda_value[0]['Event_show_time_zone']);
            $intoffset=$arr[1]*3600;
            $intNew = abs($intoffset);
            $cdate = date('Y-m-d H:i:s',strtotime($cdate)+$intNew);
          }
        }*/

        if($event_id!='' && $agenda_id!='' && $user_id!='' && $rating!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                /*$agenda_data=$this->agenda_model->getCheckInAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['check_in_agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if(in_array($agenda_id,$agenda_id_arr))
                    {*/
                        $ratings = $this->agenda_model->getRatingData($user_id,$agenda_id);
                        $rating_data['user_id'] = $user_id;
                        $rating_data['session_id'] =$agenda_id;
                        $rating_data['rating'] = $rating;
                        $rating_data['date'] = date('Y-m-d H:i:s');
                        /*if($startdate<=$cdate && $cdate >= $enddate)
                        {*/
                            if(count($ratings))
                            {
                                $this->agenda_model->updateRating($rating_data,$ratings->Id);
                                $data = array(
                                    'success' => true,
                                    'msg'=>"Rating updated successfully",
                                ); 
                            }
                            else
                            {
                                $this->agenda_model->insertRating($rating_data);
                                $data = array(
                                    'success' => true,
                                    'msg'=>"Rating saved successfully", 
                                ); 
                            }
                        /*}
                        else
                        {
                            $data = array(
                                'success' => false,
                                'msg'=>"You can not rate this session. Session either not started or it is not finished.", 
                            ); 
                        }*/
                         
                    /*}else{
                        $data = array(
                            'success' => false,
                            'msg'=>"You must Check In before you can rate this session.",
                        ); 
                    }
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'msg'=>"You must Check In before you can rate this session.",
                    ); 
                }   */     
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getPendingUserAgendaByTime()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!='' && $user_id!='' )
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                $str=$agenda_data[0]['pending_agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);
                $agenda = $this->agenda_model->getAllAgendaByTime($event_id,$agenda_id_arr,NULL,$lang_id);
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

                        if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}

                       /* $a =  strtotime($value['Start_date']);
                        $start_date=date("l, M jS, Y",$a);

                        $a =  strtotime($value['End_date']);
                        $end_date=date("l, M jS, Y",$a);*/


                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);
                        
                    }
                   } 
                    
                }
                
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    //'menu' => $this->menu,
                    //'cmsmenu' => $this->cmsmenu,
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getPendingUserAgendaByType()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!=''  && $user_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                $str=$agenda_data[0]['pending_agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }

                $agenda = $this->agenda_model->getAllAgendaByType($event_id,0,$agenda_id_arr,NULL,$lang_id);
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

                        if($date_format[0]['date_format'] == 0)
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("d/m/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("d/m/Y",$a);
							
						}
						else
						{	
							$a =  strtotime($value['Start_date']);
		                    $start_date=date("m/d/Y",$a);

		                    $a =  strtotime($value['End_date']);
		                    $end_date=date("m/d/Y",$a);

						}
                       /* $a =  strtotime($value['Start_date']);
                        $start_date=date("l, M jS, Y",$a);

                        $a =  strtotime($value['End_date']);
                        $end_date=date("l, M jS, Y",$a);*/

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                    
                }
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    //'menu' => $this->menu,
                    //'cmsmenu' => $this->cmsmenu,
                    'time_format' => $time_format[0]['format_time'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function savePendingUserAgenda()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['pending_agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    $result = $this->agenda_model->savePendingAgenda($user_id);
                   
                    $data = array(
                        'success' => true,
                        'data'=>($result!=null) ? $result : [],
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*public function deleteUserPendingAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['pending_agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false) 
                    {
                        unset($agenda_id_arr[$key]);
                      
                        if(count($agenda_id_arr)==0)
                        {
                            $this->agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['pending_agenda_id']=$agenda_id_str;
                            $this->agenda_model->updateUserAgenda($user_id,$data);
                        }
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg'=>"Successfully deleted",
                            'user_flag' => $flag,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found."
                        );
                    }
                    
                }
                else
                {
                    $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found"
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
    public function deleteUserPendingAgenda()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $agenda_id!='' && $user_id)
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                if(!empty($agenda_data))
                {
                    $str=$agenda_data[0]['pending_agenda_id'];
                    $agenda_id_arr=explode(',',$str);
                    if (($key = array_search($agenda_id, $agenda_id_arr)) !== false) 
                    {
                        unset($agenda_id_arr[$key]);
                      
                        if(count($agenda_id_arr)==0)
                        {
                            $this->agenda_model->deleteUserAgenda($user_id);
                        }
                        else
                        {
                            $agenda_id_str = implode(',', $agenda_id_arr);
                            $data['pending_agenda_id']=$agenda_id_str;
                            $this->agenda_model->updateUserAgenda($user_id,$data);
                        }
                        if($user_id!='')
                        {
                            $flag = $this->agenda_model->checkAgendaByUserId($agenda_id,$user_id);
                        }
                        $data = array(
                            'success' => true,
                            'msg'=>"Successfully deleted",
                            'user_flag' => $flag,
                            'agenda_id' => $agenda_id,
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found.",
                            'agenda_id' => $agenda_id,
                        );
                    }
                    
                }
                else
                {
                    $data = array(
                            'success' => false,
                            'msg'=>"No Agenda found",
                            'agenda_id' => $agenda_id,
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    // #agenda_comments
    public function saveAgendaComments()
    {
        $event_id=$this->input->post('event_id');
        $agenda_id=$this->input->post('agenda_id');
        $comments=$this->input->post('comments');
        $user_id=$this->input->post('user_id');

        if($event_id!='' && $agenda_id!='' && $comments!='' && $user_id!='')
        {
            $insert_data['event_id'] = $event_id; 
            $insert_data['agenda_id'] = $agenda_id; 
            $insert_data['comments'] = $comments; 
            $insert_data['user_id'] = $user_id; 
            $insert_data['created_date'] = date('Y-m-d H:i:s'); 

            $this->agenda_model->saveAgendaComments($insert_data);
            $data = array(
                'success' => true,
                'message'=>"Thank you for your feedback",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    /*public function getAgendaByTimeNew()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda = $this->agenda_model->getAllAgendaByTimeEventNew($event_id,null,$user_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i a",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i a",$End_time);
                        
                        if($date_format[0]['date_format'] == 0)
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("d/m/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("d/m/Y",$a);
                            
                        }
                        else
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("m/d/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("m/d/Y",$a);

                        }   


                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;

                    }
                   } 
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
    /*public function getAgendaByTypeNew()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $user_id = $this->input->post('user_id');
        $token=$this->input->post('_token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda = $this->agenda_model->getAllAgendaByTypeEventNew($event_id,null,null,$user_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i a",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i a",$End_time);

                        
                        if($date_format[0]['date_format'] == 0)
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("d/m/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("d/m/Y",$a);
                            
                        }
                        else
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("m/d/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("m/d/Y",$a);

                        }  

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                    }
                   } 
                    
                }
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }


                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'time_format' => $time_format[0]['format_time'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }*/
    public function getUserAgendaByTimeNew()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $user_id!='' )
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getAgendaByUserId($user_id);
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);

                $str=$agenda_data[0]['agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }
                $agenda = $this->agenda_model->getAllAgendaByTimeNew($event_id,$agenda_id_arr);
                $meeting = $this->agenda_model->getAllMeetings($event_id,$user_id);

                if(!empty($meeting)){
                    foreach ($meeting as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['time']);
                        if($time_format[0]['time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);

                        $a =  strtotime($value['date']);
                        $start_date=date("l, M jS, Y",$a);

                       

                        $meeting[$key1]['date']=$start_date;
                        $meeting[$key1]['date_time']=$start_date;
                        $meeting[$key1]['data'][$key]['time']=$start_time;
                        if($date_format[0]['date_format'] == 0)
                            {   
                                $a =  strtotime($value['date']);
                                $start_date=date("d/m/Y",$a);

                            }
                            else
                            {   
                                $a =  strtotime($value['date']);
                                $start_date=date("m/d/Y",$a);
                            }   
                            $meeting[$key1]['data'][$key]['date']=$start_date;
                        
                    }
                   } 
                    
                }
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

                        if($date_format[0]['date_format'] == 0)
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("d/m/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("d/m/Y",$a);
                            
                        }
                        else
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("m/d/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("m/d/Y",$a);

                        }   

                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                    
                }
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'meeting' => $meeting,
                    'show_suggest_button' => $this->agenda_model->getShowSuggestionButton($user_id,$event_id),
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getPendingUserAgendaByTimeNew()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $user_id!='' )
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->agenda_model->getTimeFormat($event_id);
                $agenda_data=$this->agenda_model->getPendingAgendaByUserId($user_id);
                $str=$agenda_data[0]['pending_agenda_id'];
                if($str!='')
                {
                    $agenda_id_arr=explode(',',$str);
                }
                else
                {
                    $agenda_id_arr[0]=NULL;
                }
                $date_format = $this->agenda_model->checkEventDateFormat($event_id);
                $agenda = $this->agenda_model->getAllAgendaByTimeNew($event_id,$agenda_id_arr);
                if(!empty($agenda)){
                    foreach ($agenda as $key1 => $value) {
                        foreach ($value['data'] as $key => $value) {
                          
                        
                        $Start_time =  strtotime($value['Start_time']);
                        if($time_format[0]['format_time'] == '0')
                            $start_time=date("h:i a",$Start_time);
                        else
                            $start_time=date("H:i",$Start_time);


                        $End_time =  strtotime($value['End_time']);
                        if($time_format[0]['format_time'] == '0')
                            $end_time=date("h:i a",$End_time);
                        else
                            $end_time=date("H:i",$End_time);

                        if($date_format[0]['date_format'] == 0)
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("d/m/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("d/m/Y",$a);
                            
                        }
                        else
                        {   
                            $a =  strtotime($value['Start_date']);
                            $start_date=date("m/d/Y",$a);

                            $a =  strtotime($value['End_date']);
                            $end_date=date("m/d/Y",$a);

                        }

                       /* $a =  strtotime($value['Start_date']);
                        $start_date=date("l, M jS, Y",$a);

                        $a =  strtotime($value['End_date']);
                        $end_date=date("l, M jS, Y",$a);*/


                        $agenda[$key1]['data'][$key]['Start_date']=$start_date;
                        $agenda[$key1]['data'][$key]['End_date']=$end_date;
                        $agenda[$key1]['data'][$key]['Start_time']=$start_time;
                        $agenda[$key1]['data'][$key]['End_time']=$end_time;
                        $agenda[$key1]['data'][$key]['session_image'] = $this->compress_image($value['session_image']);

                    }
                   } 
                    
                }
                
                if(empty($agenda))
                {
                    $agenda=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'agenda' => $agenda,
                    'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function compress_image($data)
    {   

        if(!empty($data))
        {
            $source_url = base_url()."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 30;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);
                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
        	return $new_name;
        }
        else
        {
        	return "";
        }
    }
}
