<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gulfood extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native/event_template_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/gulfood_model');
		/*include('application/libraries/nativeGcm.php');*/
        
        $this->menu = $this->event_template_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
    }
    public function getGulfFoodEvent()
    {
        $event = $this->gulfood_model->getGulfFoodEvent();
        if(!empty($event))
        {
            
            foreach ($event as $key => $value) 
            {
               
               $event[$key]['Logo_images']=($value['Logo_images']) ? $value['Logo_images'] : '';
               
               $event[$key]['Event_name'] =ucfirst($value['Event_name']);
              
            }
            $data = array(
              'success' => true,
              'data' => $event
            );
        }
        else
        {
            $data = array(
              'success' => true,
              'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function GulfoodExhibitorSearch()
    {
        $event_id = $this->input->post('event_id');
        $country_id = $this->input->post('country_id');
        $keyword = $this->input->post('keyword');
        $halal = $this->input->post('halal');
        $category_id = $this->input->post('category_id');
        $sector_id = $this->input->post('sector_id');
        $page_no = $this->input->post('page_no');

        if($event_id!='')
        {
            if($country_id!='')
                $where['e.country_id'] = $country_id;
                
            if($keyword!='')
                $where_keyword = "(e.Heading like '%$keyword%' OR e.Description like '%$keyword%')";
                
            if($halal!='')
                $where['e.halal'] = $halal;
                
            if($category_id!='')
                $category_where['ecr.category_id'] = $category_id;
               
            if($sector_id!='')
                $sector_where['esr.sector_id'] = $sector_id;
                

            $exhibitor_list = $this->gulfood_model->gulfoodExhibitorSearch($where,$event_id,($where_keyword) ? $where_keyword : '',($sector_where) ? $sector_where : '',($category_where) ? $category_where :  '',$page_no);
            
            $meeting_data = $this->gulfood_model->getAllMeetingRequest1($event_id,$token);
            $data = array(
                'exhibitor_list' => $exhibitor_list['exhibitors'],
                'show_meeting_button' => (empty($meeting_data)) ? '0' :'1',
                'total_pages' => $exhibitor_list['total'],
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu,
                );
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getGulfoodExhibitorSector()
    {
        $sector_list = $this->gulfood_model->getExhibitorSectorList();
        $categories = $this->gulfood_model->getGulfoodExhibitorCategories();
        
        $data = array(
          'success' => true,
          'sector_list' => $sector_list,
          'categories' => $categories
        );
        
        echo json_encode($data);
    }

    /*public function getVirtualSuperMarket()
    {
        $event_id = $this->input->post('event_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='')
        {
            $exhibitors = $this->gulfood_model->getVirtualSupermarkertData($event_id,$page_no);
            $data = array(
              'success' => true,
              'data' => $exhibitors['exhibitors'],
              'total_pages' => $exhibitors['total'],
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );
        }
        
        echo json_encode($data);
    }*/

    public function getVirtualSuperMarket()
    {
        $event_id = $this->input->post('event_id');
        $country_id = $this->input->post('country_id');
        $keyword = $this->input->post('keyword');
        $halal = $this->input->post('halal');
        $category_id = $this->input->post('category_id');
        $sector_id = $this->input->post('sector_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='')
        {
            if($country_id!='')
                $where['e.country_id'] = $country_id;
                
            if($keyword!='')
                $where_keyword = "(e.Heading like '%$keyword%' OR e.Description like '%$keyword%')";
                
            if($halal!='')
                $where['e.halal'] = $halal;
                
            if($category_id!='')
                $category_where['ecr.category_id'] = $category_id;
               
            if($sector_id!='')
                $sector_where['esr.sector_id'] = $sector_id;

            $exhibitor_list = $this->gulfood_model->searchVirtualSuperMarket($where,$event_id,($where_keyword) ? $where_keyword : '',($sector_where) ? $sector_where : '',($category_where) ? $category_where :  '',$page_no);
            $data = array(
              'success' => true,
              'data' => $exhibitor_list['exhibitors'],
              'total_pages' => $exhibitor_list['total'],
              'banner'  => "http://www.allintheloop.net/assets/images/gulfood/gulfood_virtual_supermarket_banner.jpg",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );
        }
        
        echo json_encode($data);
    }

    public function getExhibitorProfile()
    {
        $event_id = $this->input->post('event_id');
        $url = $this->input->post('email');
        
        if($event_id!='' && $url != '')
        {
            $data1 = explode('/', $url);
            $count = count($data1);
            $data = array(
              'success' => true,
              'data' => $data1[$count-1],
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );
        }
        
        echo json_encode($data);
    }

    public function getGulfoodAttendee()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        if($event_id!='' && $event_type!='')
        {
            if($keyword!='')
            $where = "u.Lastname like '%$keyword%'";
            $attendee_data = $this->gulfood_model->getAttendeeListByEventId($event_id,($where) ? $where : '',$page_no);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getGulfoodAttendeeWeb()
    {
        $time1 = round(microtime(true) * 1000);
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        $event_id = '447';
        $event_type = '3';
        if($event_id!='' && $event_type!='')
        {

            if($keyword!='')
            $where = "u.Lastname like '%$keyword%'";
            $attendee_data = $this->gulfood_model->getAttendeeListByEventIdTemp($event_id,($where) ? $where : '',$page_no);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        $data =  json_encode($data);
        $time2 = round(microtime(true) * 1000);
        $time = $time2 - $time1;
        echo "time1=".$time1."<br>";
        echo "time2=".$time2."<br>";
        echo "time=".$time."<br>";
        echo $data;
    }

    public function getGulfoodAttendeeContacts()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        if($event_id!='' && $event_type!='')
        {
            if($keyword!='')
            $where = "u.Lastname like '%$keyword%'";
            $contact_attendee = $this->gulfood_model->getContactAttendeeListByEventId($event_id,$user_id,($where) ? $where : '',$page_no);
            $data = array(
                'contact_attendee' => $contact_attendee['attendees'],
                'contact_total_pages' => $contact_attendee['total'],
                'menu' => $this->menu,
                'cmsmenu' => $this->cmsmenu
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getUsefulInfo()
    {
        $data[0]['heading'] = "Accommodation";
        $data[0]['info'] = "<p>To book accommodation, please contact:<br><br>Mr. Anis Aridi<br><br>anisa@alphatoursdubai.com<br><br>Alpha Tours<br>Office: <a href='tel:97147019111'>+971 4 701 9111</a><br>Fax: <a href='tel:+971 4 701 9100'>+971 4 701 9100</a><br>Toll Free: <a href='tel:80025742929'>80025742929</a><br><br>27th Floor, Burlington Tower<br>Business Bay, Dubai<br>(ALPHA WAY)United Arab Emirates<br>P.O. Box 25718<br><br>The Apartments<br><br><br>Comfortably furnished, and recently renovated, these rooms and hotel apartments are equipped with an extensive range of carefully selected amenities and complimentary services. Centrally situated with a welcoming atmosphere, The Apartments Dubai World Trade Center allows visitors to make the most of their visit to Dubai.<br><br><br>Ideally located amongst Dubai’s major landmarks and at the center of both business and leisure districts.  Exhibitors and visitors couldn’t be any closer to events at Dubai International Convention and Exhibition Center. Easily accessible from the Metro station, The Apartments are only 10 km away from Dubai International Airport, 5 km from Jumeirah Beach and only 1 km from the famous Dubai Mall with the world’s largest fountain.<br><br><br>Available in superior, one, two and three-bedrooms, these serviced apartments are comfortably furnished and can be leased on daily and monthly or even yearly basis. The Apartments offer access to The Club with a wide selection of indoor and outdoor recreation facilities including a swimming pool, kid’s play area and kid’s pool, tennis court, gymnasium, steam and sauna rooms and the region’s highest outdoor wall climbing facility. Its dining options comprise a multi-cuisine restaurant, a relaxing lounge and al fresco dining at the Pool Bar. Trade Center Plaza with a wide array of restaurants and cafes is only a short walk away.<br><br><br>Central location, easy access to Dubai World Trade Center and a broad range of accommodation options with impeccable service make The Apartments an ideal choice for Dubai visitors.<br><br>The Apartments DWTC is offering a special discount on bookings for visitors.<br><br><br><br>Tel: +971 4 331 4555<br>Fax: +971 4 331 3800<br>Reservations:  HotelApartments@dwtc.com<br>General Enquires: info@theapartments.ae<br>www.theapartments.ae<br><br>Novotel & Ibis World Trade Centre<br>The Novotel & Ibis World Trade Centre are connected to the Dubai World Trade Centre Complex, which is located just off Sheikh Zayed Road, providing easy access to the business and leisure hub of the city and is minutes away from the Financial District, Emaar Square, Dubai Mall, Burj Khalifa and Jumeirah Public Beach.  The hotels offer a total of 622 modern and stylish bedrooms in the 2 and 4 star categories and 8 flexible function rooms. A total of 7 restaurants and bars are available including an international buffet restaurant at Entre-Nous, Italian restaurant Cubo, Jazz & Blues Bar, Lounge Bar’s Sublime, Coffee Shop and Pool Bar. WIFI access, a full health club and pool facility and complimentary parking are also available.<br><br><br>For more details, see <span style='color:red;'>www.novotel.com / www.ibishotels.com </span> or call <a href='tel:+97143187000'>+971 4 318 7000</a>.</p>";

        $data[1]['heading'] = "Travel";
        $data[1]['info'] = "<p>Alpha Tours Dubai manages all visitor hotel and flight enquiries around Gulfood. As a Gulfood visitor, benefit from special hotel rates, visa assistance, tours and much more...<br>Alpha Tours Dubai will provide the following services:<br>04 Nights accommodation with breakfast.<br>Meet & Assist upon arrival<br>Arrival Transfers<br>Daily scheduled shuttle from/to the exhibition centre during the exhibition days<br>Departure Transfers<br>Follow up contact during your stay<br><br>For all bookings, please contact:<br><br>Mr. Anis Aridi<br>anisa@alphatoursdubai.com<br>Office: +971 4 701 9111<br>Fax: +971 4 701 9100<br>Toll Free: 800 25742 929<br>27th Floor, Burlington Tower<br>Business Bay, Dubai (ALPHA WAY)<br>United Arab Emirates<br>P.O. Box 25718</p>";

        $data[2]["heading"] = "Special Airfares";
        $data[2]["info"] = "<p>Travel on Emirates Airline to Dubai for Gulfood.</p><p>Emirates Airline has proudly been selected as the partner airline for Gulfood. Emirates has excellent connections to all of the six continents via Dubai with more than 140 current destinations worldwide – and it is expanding its route network all the time.</p><p>Use your Promotion Code to get special fares on Emirates.</p><p>Promotion code: EVE6LFD</p><p>Travel Validity: 19 February – 09 March 2017</p><p>Book your travel today at <a target='_blank' href='www.emirates.com'>www.emirates.com</a> and enter the above promotion code when you book.</p><p>Terms and conditions</p><p>Outbound and inbound travel must be within the validity period Destinations: Offer valid ONLY from and to gateway cities in which Emirates operates direct flights. Discount applicable: 10% off on the prevailing lowest online fare on Business Class and on Economy Class return fares. First Class no discount is applicable. Offer valid ONLY for return travel to Emirates gateway cities to Dubai and not from Dubai. Offer extends to companions travelling with attendees.</p><p>The offer may not be used in conjunction with any other promotion, discount or special offer. Regular charges applies for any modification and changes as per the fare rule and conditions. The promotion code should be displayed on a secured website which is accessed by the delegates.</p>";

        $data[3]["heading"] = "Show Timings";
        $data[3]['info'] = "<p>SUNDAY 26 FEBRUARY  11AM-7PM</p><p>MONDAY 27 FEBRUARY  11AM-7PM</p><p>TUESDAY 28 FEBRUARY  11AM-7PM</p><p>WEDNESDAY 1 MARCH  11AM-7PM</p><p>THURSDAY 2 MARCH  11AM-5PM</p>";

        $data[4]['heading'] = "Admission Policy";
        $data[4]['info'] = "<p>Please carefully read the below admission policy before submitting your online pre-registration for Gulfood 2017:</p><p>Gulfood is strictly for Trade and Business Professionals related to the industry only.</p>General public and persons under the age of 21 will not be permitted entry. </p><p>The organisers of Gulfood strive to ensure that only qualified Trade and Business Professionals attend the exhibition to deliver a productive business environment for both visitors and exhibitors. We reserve the right to request further proof of eligibility prior to badge issue, and also to refuse entry if necessary. Badges are non transferable and visitors found to be facilitating this will be not eligible for entry into Gulfood or future exhibitions.</p><p>Please note that Gulfood has a no refund policy on all payments.</p><p>If you do not receive your visitor confirmation in your e-mail, please check your Junk folder (in some cases, your e-mail settings or system firewalls may cause our visitor confirmation e-mails to go to your Junk folder). For online registration support, please contact gulfood@infosalons.ae.</p><p>By completing the online pre-registration form, you agree to comply with all standard terms and conditions set forth by the Dubai Department of Tourism and Commerce Marketing (DTCM). </p><p>Important notice regarding food samples: </p><p>Dubai Municipality's food safety and licensing restrictions strictly prohibit the removal of unlicensed and perishable items (including food and drink samples) from the Exhibition Halls. Thank you in advance for your co-operation.</p>";
        $info['success'] = true;
        $info['data'] = $data;
        echo json_encode($info);
    }
}