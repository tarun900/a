<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Maps extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/map_model');

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function map_list()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $maps = $this->map_model->getMapsListByEventId($event_id);
                $data = array(
                    'map_list' => $maps,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_details()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $map_id=$this->input->post('map_id');
        $lang_id=$this->input->post('lang_id');
        if($event_id!='' && $event_type!='' && $map_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $user = $this->map_model->getUser($token);
                $map_details = $this->map_model->getMapsDetailsByMapId($event_id,$map_id,$lang_id);
                $image_mapping_details = $this->map_model->getImageMappingDetailsByMapId($map_id,$event_id,$user['Id'],$lang_id);
                $data = array(
                    'map_details' => $map_details,
                    'image_mapping_details' => $image_mapping_details,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_details_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $map_id=$this->input->post('map_id');
        if($event_id!='' && $event_type!='' && $map_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $user = $this->map_model->getUser($token);
                $map_details = $this->map_model->getMapsDetailsByMapId($event_id,$map_id);
                $image_mapping_details = $this->map_model->getImageMappingDetailsByMapId($map_id,$event_id,$user['Id']);
                $data = array(
                    'floor_plan_icon' => 'http://www.allintheloop.net/assets/images/Floor_Plan_icon.png',
                    'google_map_icon' => 'http://www.allintheloop.net/assets/images/Google_Maps_icon.jpg',
                    'map_details' => $map_details,
                    'image_mapping_details' => $image_mapping_details,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function map_list_new()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $event_type=$this->input->post('event_type');
        $lang_id=$this->input->post('lang_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'message' => 'Please check token or event.'
                    
                );   
            } 
            else 
            {
                $maps = $this->map_model->getMapsListByEventIdNew($event_id,$lang_id);
                $data = array(
                    'map_list' => $maps,
                    /*'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}