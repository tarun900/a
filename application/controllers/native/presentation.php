
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Presentation extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        error_reporting(0);
        ini_set('error_reporting', off);
        ini_set('display_errors', off);
        ini_set('log_errors', off);
        parent::__construct();
        $this->load->model('native/app_login_model');
        $this->load->model('native/event_template_model');
        $this->load->model('native/presentation_model');
        $this->load->model('native/cms_model');
        $this->load->model('native/event_model');
        $this->load->model('native/settings_model');
        $this->menu = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, 1,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        require './application/libraries/nativeGcm.php';
        $this->startSocketServer();

    }

    public function getPrsentationByTime()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->presentation_model->getTimeFormat($event_id);
                $presentaion = $this->presentation_model->getAllPresentationByTimeEvent($event_id);
                
                if(empty($presentaion))
                {
                    $presentaion=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'presentaion' => $presentaion,
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time']
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getPrsentationByType()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $time_format = $this->presentation_model->getTimeFormat($event_id);
                $presentaion = $this->presentation_model->getAllPresentationByTypeEvent($event_id);
                
                if(empty($presentaion))
                {
                    $presentaion=array([0]=>"");
                }
                $data = array(
                    'event' => $user[0],
                    'presentaion' => $presentaion,
                    /*'menu' => $this->menu,
                    'cmsmenu' => $this->cmsmenu,*/
                    'time_format' => $time_format[0]['format_time'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function getPrsentationById()
    {
    	$event_id=$this->input->post('event_id');
    	$event_type=$this->input->post('event_type');
    	$presentation_id=$this->input->post('presentation_id');
    	$token=$this->input->post('token');
    	if($event_id!='' && $event_type!='' && $presentation_id!='')
    	{
    		$user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
	    	if (empty($user)) 
	        {
	            $data = array(
	                'success' => false,
	                'data' => array(
	                    'msg' => 'Please check token or event.'
	                )
	            );   
	        } 
	        else 
	        {
	          	$presentation_value = $this->presentation_model->getPresentationById($event_id,$presentation_id);
				if(!empty($presentation_value[0]['Images']))
				{
					$Images_arr=json_decode($presentation_value[0]['Images']);
					$presentation_value[0]['Images']=$Images_arr;
					$Images_lock_arr=json_decode($presentation_value[0]['Image_lock']);
					$presentation_value[0]['Image_lock']=$Images_lock_arr;
				}
				$data = array(
					'success' => true,
	                'presentation' => $presentation_value,
	  	            );
	        }
    	}
    	else
    	{
    		$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
    	}
        echo json_encode($data);
    }
    
    public function getPrsentationImagesByIdRefresh()
    {
        $user_id = $this->input->post('user_id');
        $presentation_id = $this->input->post('presentation_id');
        $event_id = $this->input->post('event_id');
        
        if($user_id!='' && $presentation_id!='' && $event_id!='')
        {
            $presentaion = $this->presentation_model->getPresentationById($event_id,$presentation_id);
            if($presentaion)
            {
            $presentaion = $presentaion[0];
            $presenter =   explode(',', $presentaion['user_permissions']);

            $is_presenter = (in_array($user_id, $presenter)) ? true : false;
            
            $images = (json_decode($presentaion['Images'],true)) ? json_decode($presentaion['Images'],true) : [];
            $image_lock = (json_decode($presentaion['Image_lock'],true)) ? json_decode($presentaion['Image_lock'],true) : [];
            array_walk($presentaion,function(&$item){$item=strval($item);});
            
            foreach ($images as $key => $value) 
            {
                $ext = pathinfo($value, PATHINFO_EXTENSION);
                $data=$this->presentation_model->get_edit_survey_in_presentation($event_id,$value);
                if(empty($ext) && !empty($data))
                {
                    $images_data[$key]['type'] = 'survay';

                    $ans=$this->presentation_model->get_survey_and_by_user_id($value,$user_id);

                    
                    $data['Option'] = (json_decode($data['Option'],true)) ? json_decode($data['Option'],true) : [] ;
                    $data['Answer']=($ans[0]['Answer']) ? $ans[0]['Answer'] : '';
                    $ans1=$this->presentation_model->get_survey_and_by_user_id($value);
                    $data['view_result_btn']= ($ans[0]['Answer']=='') ? '1' : '0';
                    $images_data[$key]['value'] = $data;
                }
                else
                {
                    $images_data[$key]['type'] = 'image';
                    $images_data[$key]['value'] = $value;
                }
                if(empty($image_lock))
                {
                    $image_lock_new[$key] = '0';
                }
            }
            $presentaion['Image_lock'] = ($image_lock_new) ? $image_lock_new : $image_lock ;  
            $presentaion['Images'] =($images_data) ? $images_data : [];
            
            $presenter =   explode(',', $presentaion['user_permissions']);
            $is_presenter = (in_array($user_id, $presenter)) ? 'true' : 'false';
         
            $tool = $this->presentation_model->getPresentationTool($presentation_id);
            $presentaion['type'] = '';
            $presentaion['lock_image'] = '';
            $presentaion['push_image'] = '';
            $presentaion['push_result'] = '';
            $presentaion['is_locked_result'] = '0';
            if($tool['push_result']!='')
            {
                $presentaion['type'] = 'result';
                $result[0]['type'] = 'result';
                $presenatation_url = $this->presentation_model->getUrlData($event_id,$tool['chart_type']);
                $result[0]['value'] = $_SERVER['SERVER_NAME'].'/Presentation/'.$presenatation_url.$tool['push_result'];
                $presentaion['push_result'] = $tool['push_result'];
                $final_image = array_merge($result,$images_data);
                $presentaion['Images'] = $final_image;
                array_unshift($presentaion['Image_lock'] , '1');
            }
            if($tool['lock_image']!='')
            {
                $ext = pathinfo($tool['lock_image'], PATHINFO_EXTENSION);
                if(empty($ext))
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] : 'survay';
                else
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] :'image';
                $presentaion['lock_image'] = $tool['lock_image'];
                $presentaion['is_locked_result'] = '1';
            }
            if($tool['push_images']!='')
            {
                $ext = pathinfo($tool['push_images'], PATHINFO_EXTENSION);
                 if(empty($ext))
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] : 'survay';
                else
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] :'image';
                $presentaion['push_image'] = $tool['push_images'];
            }
            $presentation_data=  ($presentaion) ? array($presentaion)  : [] ;  
            }
            $data = array(
                'success' => true,
                'presentation' => ($presentation_data) ? $presentation_data : [],
                'is_presenter' => ($is_presenter) ? $is_presenter : ''
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function viewPresentationByRole()
    {
        $user_id = $this->input->post('user_id');
        $presentation_id = $this->input->post('presentation_id');
        $event_id = $this->input->post('event_id');
        if($user_id!='' && $presentation_id!='' && $event_id!='')
        {
            $presentaion = $this->presentation_model->getPresentationById($event_id,$presentation_id);
            if($presentaion)
            {
            $presentaion = $presentaion[0];
            $images = (json_decode($presentaion['Images'],true)) ? json_decode($presentaion['Images'],true) : [] ;
            $image_lock = (json_decode($presentaion['Image_lock'],true)) ? json_decode($presentaion['Image_lock'],true) : [];
            array_walk($presentaion,function(&$item){$item=strval($item);});
            foreach ($images as $key => $value) 
            {
                $ext = pathinfo($value, PATHINFO_EXTENSION);
                $data=$this->presentation_model->get_edit_survey_in_presentation($event_id,$value);
                if(empty($ext) && !empty($data)) 
                {
                    $images_data[$key]['type'] = 'survay';

                    $ans=$this->presentation_model->get_survey_and_by_user_id($value,$user_id);
                    
                    $data['Option'] = (json_decode($data['Option'],true)) ? json_decode($data['Option'],true) : [] ;
                    $data['Answer']=($ans[0]['Answer']) ? $ans[0]['Answer'] : '';
                    $data['view_result_btn']= ($ans[0]['Answer']=='') ? '1' : '0';
                    $images_data[$key]['value'] = $data;
                }
                else
                {
                    $images_data[$key]['type'] = 'image';
                    $images_data[$key]['value'] = $value;
                }
                if(empty($image_lock))
                {
                    $image_lock_new[$key] = '0';
                }
            }
            $presentaion['Image_lock'] = ($image_lock_new) ? $image_lock_new : $image_lock;
            $presentaion['Images'] = ($images_data) ? $images_data : [];
            $tool = $this->presentation_model->getPresentationTool($presentation_id);
            $presentaion['type'] = '';
            $presentaion['lock_image'] = '';
            $presentaion['push_image'] = '';
            $presentaion['push_result'] = '';
            $presentaion['is_locked_result'] = '0';
            if($tool['push_result']!='')
            {
                $presentaion['type'] = 'result';
                $result[0]['type'] = 'result';
                $presenatation_url = $this->presentation_model->getUrlData($event_id);
                $result[0]['value'] = $_SERVER['SERVER_NAME'].'/Presentation/'.$presenatation_url.$tool['push_result'];
                $presentaion['push_result'] = $tool['push_result'];
                $final_image = array_merge($result,$images_data);
                $presentaion['Images'] = $final_image;
                array_unshift($presentaion['Image_lock'] , '1');

            }
            if($tool['lock_image']!='')
            {
                $ext = pathinfo($tool['lock_image'], PATHINFO_EXTENSION);
                 if(empty($ext))
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] : 'survay';
                else
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] :'image';
                $presentaion['lock_image'] = $tool['lock_image'];
                $presentaion['is_locked_result'] = '1';
            }
            if($tool['push_images']!='')
            {
                $ext = pathinfo($tool['push_images'], PATHINFO_EXTENSION);
                 if(empty($ext))
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] : 'survay';
                else
                    $presentaion['type'] = ($presentaion['type']) ? $presentaion['type'] :'image';
                $presentaion['push_image'] = $tool['push_images'];
            }
            $presentation_data=  ($presentaion) ? array($presentaion)  : [] ;  
            $presenter =   explode(',', $presentaion['user_permissions']);
            $is_presenter = (in_array($user_id, $presenter)) ? 'true' : 'false';
            }
            $data = array(
                'success' => true,
                'presentation' => ($presentation_data) ? $presentation_data : [],
                'is_presenter' => ($is_presenter) ? $is_presenter : ''
            );
        }
        else
        {
            $data = array(
            'success' => false,
            'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function lockUnlockSlides()
    {
        $image_name = $this->input->post('image_name');  // 0 for unlock
        $presentation_id = $this->input->post('presentation_id');
        $event_id = $this->input->post('event_id');

        if($presentation_id!='' && $event_id!='')
        {
            
            if($image_name!='0')
            {
                $lock_data['lock_image']=$image_name;
                $locked = true;
            }
            else
            {
              $lock_data['lock_image']='';
              $locked = false;
            }
            $this->presentation_model->lockUnlockSlides($presentation_id,$lock_data);
            
            $data = array(
                'success' => true,
                'is_locked' => $locked,
            );
        }
        else
        {
            $data = array(
            'success' => false,
            'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
        //$this->sendNotification($event_id);
    }
    public function pushSlide()
    {
        $image_name = $this->input->post('image_name');  
        $presentation_id = $this->input->post('presentation_id');
        $event_id = $this->input->post('event_id');

        if($presentation_id!='' && $event_id!='')
        {
            if($image_name!='0')
            {
                $lock_data['push_images']=$image_name;
                $lock_data['lock_image']='';
                $lock_data['push_result']='';
            }
            else
            {
                $lock_data['push_images']='';
            }
            $this->presentation_model->lockUnlockSlides($presentation_id,$lock_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully pushed',
                'live_mode' => ($image_name!='0') ? '1' : '0',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
        //$this->sendNotification($event_id);
    }
    public function viewChartResult()
    {
       $survay_id = $this->input->post('survay_id');
       $event_id = $this->input->post('event_id');
       $presentation_id = $this->input->post('presentation_id');
       $is_bar_chart = $this->input->post('is_bar_chart');
       
       if($survay_id!='')
       {
            $tool = $this->presentation_model->getPresentationTool($presentation_id);
            
            $presenatation_url = $this->presentation_model->getUrlData($event_id,$is_bar_chart);
            $url = $_SERVER['SERVER_NAME'].'/Presentation/'.$presenatation_url.$survay_id;
       
            $data = array(
                'success' => true,
                'data' => $url,
                'show_remove_button' => ($survay_id == $tool['push_result']) ? '1' : '0',
            );

       }
       else
       {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
       }
       echo json_encode($data);
    }
    public function saveSurveyAnswer()
    {
        $user_id = $this->input->post('user_id');
        $survay_id = $this->input->post('survay_id');
        $ans = $this->input->post('ans');
        if($user_id!='' && $survay_id!='' && $ans!='')
        {
            $save_ans['Question_id']=$survay_id;
            $save_ans['User_id']=$user_id;
            $save_ans['Answer']=$ans;
            $save_ans['answer_date']=date('Y-m-d H:i:s');
            $this->presentation_model->save_ans_in_presentation($save_ans);
            $data = array(
                'success' => true,
                'message' => 'Successfully saved',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function pushResult()
    {
        $presentation_id = $this->input->post('presentation_id');
        $survay_id = $this->input->post('survay_id');
        $is_bar_chart = $this->input->post('is_bar_chart');

        if($presentation_id!='' && $survay_id!='')
        {
            $event_id = $this->presentation_model->getEventId($presentation_id);
            $lock_data['push_result']=$survay_id;
            $lock_data['push_images']='';
            $lock_data['lock_image']='';
            $lock_data['chart_type'] = $is_bar_chart;
            $this->presentation_model->lockUnlockSlides($presentation_id,$lock_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully pushed',
                'show_remove_button' => '1',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);

       // $this->sendNotification($event_id);
    }
    public function removeChartResult()
    {
        $presentation_id = $this->input->post('presentation_id');
        
        if($presentation_id!='')
        {
            $event_id = $this->presentation_model->getEventId($presentation_id);
            $lock_data['push_result']='';
            $lock_data['push_images']='';
            $lock_data['lock_image']='';
            $this->presentation_model->lockUnlockSlides($presentation_id,$lock_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully removed',
                'show_push_button' => '1',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
      //  $this->sendNotification($event_id);
    }
    public function removePushSlide()
    {
        $presentation_id = $this->input->post('presentation_id');
        
        if($presentation_id!='')
        {
            $event_id = $this->presentation_model->getEventId($presentation_id);
            $lock_data['push_images']='';
            $this->presentation_model->lockUnlockSlides($presentation_id,$lock_data);
            $data = array(
                'success' => true,
                'message' => 'Successfully removed',
                'live_mode' => '0',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
       // $this->sendNotification($event_id);
    }
    public function sendNotification($event_id)
    {
        //$this->load->library('nativeGcm');
        $obj = new Gcm();
        $users = $this->settings_model->getAllUsersGCMN($event_id);
        $count = count($users);
        if($count > 100)
        {
           $limit = 100;
            for ($i=0;$i<$count;$i++) 
            {
                $page_no        = $i;
               
                $start          = ($page_no)*$limit;
                $users1         = array_slice($users,$start,$limit);
                foreach ($users1 as $key => $value) 
                {
                    if($value['gcm_id']!='')
                    {
                      $msg =  '';
                      $extra['message_type'] = 'presentation';
                      $extra['message_id'] = '';
                      $extra['event'] = $this->settings_model->event_name($event_id);
                      if($value['device'] == "Iphone")
                      {
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                          $result1[] = $value['Id'];
                      }
                      else
                      {
                          $msg['title'] = '';
                          $msg['message'] = '';
                          $msg['vibrate'] = 1;
                          $msg['sound'] = 1;
                          $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                          $result1[] = $value['Id'];
                      } 
                    }
                }
            }
        }
        else
        {
            foreach ($users as $key => $value) {
                if($value['gcm_id']!='')
                {
                  $msg =  '';
                  $extra['message_type'] = 'presentation';
                  $extra['message_id'] = '';
                  $extra['event'] = $this->settings_model->event_name($event_id);
                  if($value['device'] == "Iphone")
                  {
                      $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      $result1[] = $value['Id'];
                  }
                  else
                  {
                      $msg['title'] = '';
                      $msg['message'] = '';
                      $msg['vibrate'] = 1;
                      $msg['sound'] = 1;
                      $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                      $result1[] = $value['Id'];
                  } 
                }
            }
        }
       // print_r($result1);exit;
    }

    public function startSocketServer()
    {
        $address=$this->config->item('ip');
        $port=$this->config->item('port');
        
        $this->sock = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );

        socket_set_option($this->sock, SOL_SOCKET, SO_REUSEADDR, 1) ;
        
        if(!socket_bind( $this->sock, 0, $port ))
        {
            socket_connect($this->sock, $address, $port) ;
        }
        
        //socket_close($this->sock);
    }
    public function sendmessage()
    {   
        $this->startSocketServer();

        $msg="200";
        if($this->sock)
        {
            socket_listen( $this->sock );
            $addresses = [];
            $repeated = [];
            $rand = rand();
            $i = 0;
            while($rand = socket_accept( $this->sock ) )
            {
                $gcm_id = socket_read($rand,1024);
                
                            
                socket_getpeername ( $rand , $address ,$port );
                
                if(!in_array($gcm_id, $addresses)  )
                {
                    $sent = socket_write($rand,$msg,strlen($msg));

                    $this->presentation_model->saveToDB($gcm_id);
                    $addresses[] = $gcm_id;
                    socket_close($rand);
                }
                else
                {
                    $repeated[] = $gcm_id;
                    if(count($addresses) == count($repeated))
                    {
                        socket_close($rand);
                        break;
                    }
                }
                $i++;
            }
        }
        unset($addresses);
        exit;
       // echo exec("php ".FCPATH."application/controllers/native/server.php");
        

    }

}
