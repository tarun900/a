<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Allapi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Setting_model');
        $this->load->model('Add_Attendee_model');
        $this->load->model('Event_model');
        $this->load->model('Allapi_model');
    }
    public function index()
    {
        die("Direct Access Denied..");
    }
    public function create_token()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Authorize?username=AllInTheLoop&password=Rqeacqf%25401&showCode=GULFOOD17",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QWxsSW5UaGVMb29wOlJxZWFjcWZAMQ==",
                "cache-control: no-cache",
                "postman-token: cea32ba5-edce-589b-77a1-128687c028db"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $token = json_decode($response, true);
        }
        return $token;
    }
    public function insert_temp_exibitor_ids()
    {
        $qu = array(
            'fromDate' => date('Y-m-d H:i', strtotime("-1 day")) ,
            'toDate' => date('Y-m-d H:i')
        );
        $this->load->model('Exibitor_model');
        $token = $this->create_token();
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            /*CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Exhibitors/Modified?".http_build_query($qu),          */
            CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Exhibitors/Modified?fromDate=2017-03-02%2000%3A00&toDate=2017-03-03%2000%3A00",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $token[0]['mysGUID'],
                "cache-control: no-cache",
                "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
            ) ,
        ));
        $response = curl_exec($curl1);
        $err = curl_error($curl1);
        curl_close($curl1);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $all_exibitor = json_decode($response, true);
            $this->Exibitor_model->save_all_temp_exhi_id($all_exibitor[0]['exhibitors']);
        }
        echo "success";
        die;
    }
    public function insert_exibitordata()
    {
        $this->load->model('Exibitor_model');
        $token = $this->create_token();
        $all_exibitorids = $this->Exibitor_model->get_all_temp_exhi_id();
        foreach($all_exibitorids as $key => $value)
        {
            $curl2 = curl_init();
            curl_setopt_array($curl2, array(
                CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Exhibitors?exhid=" . $value['exhid'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 50,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $token[0]['mysGUID'],
                    "cache-control: no-cache",
                    "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                ) ,
            ));
            $exi_response = curl_exec($curl2);
            $err = curl_error($curl2);
            curl_close($curl2);
            if ($err)
            {
                echo "cURL Error #:" . $err;
                $token = $this->create_token();
                $curl23 = curl_init();
                curl_setopt_array($curl23, array(
                    CURLOPT_URL => "https://api.mysstaging.com/mysRest/v1/Exhibitors?exhid=" . $value['exhid'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 50,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer " . $token[0]['mysGUID'],
                        "cache-control: no-cache",
                        "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                    ) ,
                ));
                $exi_response2 = curl_exec($curl23);
                $err1 = curl_error($curl23);
                curl_close($curl23);
                if ($err1)
                {
                }
                else
                {
                    $exibitor_data = json_decode($exi_response2, true);
                }
            }
            else
            {
                $exibitor_data = json_decode($exi_response, true);
            }
            $eid = 447;
            $org_id = 4659;
            $demodata = $exibitor_data[0];
            if (count($demodata['exhibitor']) > 0)
            {
                if (!empty($demodata['exhibitor']['email']))
                {
                    $exuser['Firstname'] = $demodata['exhibitor']['contacts'][0]['fname'];
                    $exuser['Lastname'] = $demodata['exhibitor']['contacts'][0]['lname'];
                    $exuser['Title'] = $demodata['exhibitor']['contacts'][0]['title'];
                    $exuser['Company_name'] = $demodata['exhibitor']['exhname'];
                    $exuser['Email'] = $demodata['exhibitor']['email'];
                    $exuser['Password'] = $demodata['exhibitor']['password'];
                    $exuser['Logo'] = $demodata['exhibitor']['logo'];
                    $exuser['State'] = $demodata['exhibitor']['state'];
                    $exuser['Organisor_id'] = $org_id;
                    $exuser['Postcode'] = $demodata['exhibitor']['zip'];
                    $exuser['Suburb'] = $demodata['exhibitor']['city'];
                    $exuser['Street'] = $demodata['exhibitor']['address1'];
                    $this->load->model('Profile_model');
                    $cid = $this->Profile_model->getstate_by_country_name($demodata['exhibitor']['country']);
                    $exuser['Country'] = $cid[0]['country_id'];
                    $exuser['Created_date'] = date('Y-m-d H:i:s');
                    $exuser['Active'] = '1';
                    $user_id = $this->Exibitor_model->add_user_from_api($exuser, $eid);
                    $exdata['user_id'] = $user_id;
                    $exdata['Event_id'] = $eid;
                    $exdata['Organisor_id'] = $org_id;
                    $exdata['Heading'] = $demodata['exhibitor']['exhname'];
                    $exdata['company_logo'] = json_encode(array(
                        $demodata['exhibitor']['logo']
                    ));
                    if ($demodata['exhibitor']['halal'] == '0')
                    {
                        $exdata['halal'] = '0';
                    }
                    else
                    {
                        $exdata['halal'] = '1';
                    }
                    $exdata['country_id'] = $cid[0]['country_id'];
                    $exdata['Description'] = $demodata['exhibitor']['description'];
                    $exdata['website_url'] = $demodata['exhibitor']['website'];
                    $exdata['facebook_url'] = $demodata['exhibitor']['facebook'];
                    $exdata['linkedin_url'] = $demodata['exhibitor']['linkedin'];
                    $exdata['twitter_url'] = $demodata['exhibitor']['twitter'];
                    $exdata['stand_number'] = $demodata['exhibitor']['booths'][0]['boothnumber'] . ' ' . $demodata['exhibitor']['booths'][0]['hall'];
                    $exid = $this->Exibitor_model->add_exibitor_from_api($exdata);
                    $exsectorid = array();
                    if ($demodata['exhibitor']['beverages'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "1",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['dairy'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "2",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['meat'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "3",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['pulses'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "4",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['fats'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "5",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['world'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "6",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['power'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "7",
                            "exibitor_id" => $exid
                        ));
                    }
                    if ($demodata['exhibitor']['health'] == '1')
                    {
                        array_push($exsectorid, array(
                            "sector_id" => "8",
                            "exibitor_id" => $exid
                        ));
                    }
                    if (count($exsectorid) > 0)
                    {
                        $this->Exibitor_model->add_sector_relation($exsectorid);
                    }
                    foreach($demodata['exhibitor']['productcategories'] as $ckey => $cvalue)
                    {
                        $this->Exibitor_model->add_category_and_relation($cvalue['categoryname'], $exid, $eid);
                    }
                }
                $this->Exibitor_model->remove_temp_exibitor_id($value['exhid']);
            }
        }
        echo "success";
        die;
    }
    
    public function insert_temp_attendee_email()
    {
        $qu = array(
            'fromDate' => date('Y-m-d H:i', strtotime("-1 day")) ,
            'toDate' => date('Y-m-d H:i')
        );
        $this->load->model('Attendee_model');
        $token = $this->create_token();
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            /*CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Users/Modified?".http_build_query($qu),*/
            CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Users/Modified?fromDate=2017-03-02%2000%3A00&toDate=2017-03-03%2000%3A00",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $token[0]['mysGUID'],
                "cache-control: no-cache",
                "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
            ) ,
        ));
        $response = curl_exec($curl1);
        $err = curl_error($curl1);
        curl_close($curl1);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $all_attendee = json_decode($response, true);
            $this->Attendee_model->save_all_temp_attendee_email($all_attendee[0]['users']);
        }
        echo "success";
        die;
    }
    public function insert_attendeedata()
    {
        $this->load->model('Attendee_model');
        $token = $this->create_token();
        $all_attendeeemail = $this->Attendee_model->get_all_temp_attendee_email();
        foreach($all_attendeeemail as $key => $value)
        {
            $curl2 = curl_init();
            curl_setopt_array($curl2, array(
                CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Users/?emailAddress=" . $value['email'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 50,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $token[0]['mysGUID'],
                    "cache-control: no-cache",
                    "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                ) ,
            ));
            $attendee_response = curl_exec($curl2);
            $err = curl_error($curl2);
            curl_close($curl2);
            if ($err)
            {
                echo "cURL Error #:" . $err;
                $token = $this->create_token();
                $curl23 = curl_init();
                curl_setopt_array($curl23, array(
                    CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Users/?emailAddress=" . $value['email'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 50,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer " . $token[0]['mysGUID'],
                        "cache-control: no-cache",
                        "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                    ) ,
                ));
                $attendee_response2 = curl_exec($curl23);
                $err1 = curl_error($curl23);
                curl_close($curl23);
                if ($err1)
                {
                }
                else
                {
                    $attendee_data = json_decode($attendee_response2, true);
                }
            }
            else
            {
                $attendee_data = json_decode($attendee_response, true);
            }
            $eid = 447;
            $org_id = 4659;
            $demodata = $attendee_data[0]['users'];
            if (!empty($demodata['email']))
            {
                $attendee_user['Firstname'] = $demodata['fname'];
                $attendee_user['Lastname'] = $demodata['lname'];
                $attendee_user['Company_name'] = $demodata['company'];
                $attendee_user['Email'] = $demodata['email'];
                $attendee_user['Password'] = $demodata['password'];
                $attendee_user['State'] = $demodata['state'];
                $attendee_user['Organisor_id'] = $org_id;
                $attendee_user['Suburb'] = $demodata['city'];
                $this->load->model('Profile_model');
                $cid = $this->Profile_model->getstate_by_country_name($demodata['country']);
                $attendee_user['Country'] = $cid[0]['country_id'];
                $attendee_user['Created_date'] = date('Y-m-d H:i:s');
                $attendee_user['Active'] = '1';
                if ($demodata['allowexhcontact'] == '1')
                {
                    $attendee_user['allowexhcontact'] = '1';
                }
                else
                {
                    $attendee_user['allowexhcontact'] = '0';
                }
                $this->Attendee_model->add_attendee_from_api($attendee_user, $eid);
                /*metting data*/
                $curl2 = curl_init();
                curl_setopt_array($curl2, array(
                    CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Agendas/Meetings?emailAddress=" . $value['email'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 50,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer " . $token[0]['mysGUID'],
                        "cache-control: no-cache",
                        "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                    ) ,
                ));
                $attendee_response = curl_exec($curl2);
                $err = curl_error($curl2);
                curl_close($curl2);
                if ($err)
                {
                    echo "cURL Error #:" . $err;
                    $token = $this->create_token();
                    $curl23 = curl_init();
                    curl_setopt_array($curl23, array(
                        CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Agendas/Meetings?emailAddress=" . $value['email'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 50,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "authorization: Bearer " . $token[0]['mysGUID'],
                            "cache-control: no-cache",
                            "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                        ) ,
                    ));
                    $attendee_response2 = curl_exec($curl23);
                    $err1 = curl_error($curl23);
                    curl_close($curl23);
                    if ($err1)
                    {
                    }
                    else
                    {
                        $attendee_data = json_decode($attendee_response2, true);
                    }
                }
                else
                {
                    $attendee_data = json_decode($attendee_response, true);
                }
                $eid = 447;
                $org_id = 4659;
                $demodata = $attendee_data[0]['meetings'];
                if (count($demodata) > 0)
                {
                    $attendee_id = $this->Attendee_model->get_user_id_by_email_in_api($value['email']);
                    array_walk_recursive($demodata, array(
                        $this,
                        'update_somthing'
                    ) , array(
                        'matchkey' => 'meetingid',
                        'newval' => $attendee_id
                    ));
                    $this->Attendee_model->insert_temp_metting_data($demodata);
                }
                /*foreach ($demodata as $key1 => $value1) {
                $exi_email=$this->get_exhibitor_email_from_api($value1['exhid']);
                $exibitor_id=$this->Attendee_model->get_exhibitor_id_by_email_in_api($exi_email,$eid);
                $mettingdata['attendee_id']=$attendee_id;
                $mettingdata['exhibiotor_id']=$exibitor_id;
                $mettingdata['event_id']=$eid;
                $date = DateTime::createFromFormat('F, d Y H:i:s',$value1['starttime']);
                $mettingdata['date']=$date->format('Y-m-d');
                $mettingdata['time']=$date->format('H:i:s');
                if(!empty($mettingdata['attendee_id']) && !empty($mettingdata['exhibiotor_id']))
                {
                $metting_id=$this->Attendee_model->add_attendee_exibitor_metting($mettingdata);
                }
                }*/
            }
            $this->Attendee_model->remove_temp_email_address($value['email']);
        }
        echo "Success";
        die;
    }
    public function insert_meting_attendee_data()
    {
        $eid = 447;
        $this->load->model('Attendee_model');
        $metting_data = $this->Attendee_model->get_all_temp_metting_data();
        foreach($metting_data as $key1 => $value1)
        {
            $exi_email = $this->get_exhibitor_email_from_api($value1['exhid']);
            $exibitor_id = $this->Attendee_model->get_exhibitor_id_by_email_in_api($exi_email, $eid);
            $mettingdata['attendee_id'] = $value1['meetingid'];
            $mettingdata['exhibiotor_id'] = $exibitor_id;
            $mettingdata['event_id'] = $eid;
            $date = DateTime::createFromFormat('F, d Y H:i:s', $value1['starttime']);
            $mettingdata['date'] = $date->format('Y-m-d');
            $mettingdata['time'] = $date->format('H:i:s');
            if (!empty($mettingdata['attendee_id']) && !empty($mettingdata['exhibiotor_id']))
            {
                $metting_id = $this->Attendee_model->add_attendee_exibitor_metting($mettingdata);
            }
            $this->Attendee_model->remove_temp_meeting_data($value1['mid']);
        }
        echo "Success";
        die;
    }
    public function insert_attendeee_metting_data()
    {
        $this->load->model('Attendee_model');
        $token = $this->create_token();
        $all_attendeeemail = $this->Attendee_model->get_all_temp_attendee_email();
        foreach($all_attendeeemail as $key => $value)
        {
            $curl2 = curl_init();
            curl_setopt_array($curl2, array(
                CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Agendas/Meetings?emailAddress=" . $value['email'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 50,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $token[0]['mysGUID'],
                    "cache-control: no-cache",
                    "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                ) ,
            ));
            $attendee_response = curl_exec($curl2);
            $err = curl_error($curl2);
            curl_close($curl2);
            if ($err)
            {
                echo "cURL Error #:" . $err;
                $token = $this->create_token();
                $curl23 = curl_init();
                curl_setopt_array($curl23, array(
                    CURLOPT_URL => "http://api.mapyourshow.com/mysRest/v1/Agendas/Meetings?emailAddress=" . $value['email'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 50,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer " . $token[0]['mysGUID'],
                        "cache-control: no-cache",
                        "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                    ) ,
                ));
                $attendee_response2 = curl_exec($curl23);
                $err1 = curl_error($curl23);
                curl_close($curl23);
                if ($err1)
                {
                }
                else
                {
                    $attendee_data = json_decode($attendee_response2, true);
                }
            }
            else
            {
                $attendee_data = json_decode($attendee_response, true);
            }
            $eid = 447;
            $org_id = 4659;
            $demodata = $attendee_data[0]['meetings'];
            $attendee_id = $this->Attendee_model->get_user_id_by_email_in_api($value['email']);
            foreach($demodata as $key1 => $value1)
            {
                $exi_email = $this->get_exhibitor_email_from_api($value1['exhid']);
                $exibitor_id = $this->Attendee_model->get_exhibitor_id_by_email_in_api($exi_email, $eid);
                $mettingdata['attendee_id'] = $attendee_id;
                $mettingdata['exhibiotor_id'] = $exibitor_id;
                $mettingdata['event_id'] = $eid;
                $date = DateTime::createFromFormat('F, d Y H:i:s', $value1['starttime']);
                $mettingdata['date'] = $date->format('Y-m-d');
                $mettingdata['time'] = $date->format('H:i:s');
                if (!empty($mettingdata['attendee_id']) && !empty($mettingdata['exhibiotor_id']))
                {
                    $metting_id = $this->Attendee_model->add_attendee_exibitor_metting($mettingdata);
                }
            }
            $this->Attendee_model->remove_temp_email_address($value['email']);
        }
        echo "Success";
        die;
    }
    public function get_exhibitor_email_from_api($exhid)
    {
        $token = $this->create_token();
        $curl2 = curl_init();
        curl_setopt_array($curl2, array(
            CURLOPT_URL => "https://api.mapyourshow.com/mysRest/v1/Exhibitors?exhid=" . $exhid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $token[0]['mysGUID'],
                "cache-control: no-cache",
                "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
            ) ,
        ));
        $exi_response = curl_exec($curl2);
        $err = curl_error($curl2);
        curl_close($curl2);
        if ($err)
        {
            echo "cURL Error #:" . $err;
            $token = $this->create_token();
            $curl23 = curl_init();
            curl_setopt_array($curl23, array(
                CURLOPT_URL => "https://api.mysstaging.com/mysRest/v1/Exhibitors?exhid=" . $exhid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 50,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $token[0]['mysGUID'],
                    "cache-control: no-cache",
                    "postman-token: 37ea6c7c-939c-a1b0-77a6-87611e604cdd"
                ) ,
            ));
            $exi_response2 = curl_exec($curl23);
            $err1 = curl_error($curl23);
            curl_close($curl23);
            if ($err1)
            {
            }
            else
            {
                $exibitor_data = json_decode($exi_response2, true);
            }
        }
        else
        {
            $exibitor_data = json_decode($exi_response, true);
        }
        return $exibitor_data[0]['exhibitor']['email'];
    }
    public function get_api_agenda_list()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.gisec.ae/wp-json/miramedia/seminars/?filter%5Bposts_per_page%5D=20&page=2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 102948bb-f8df-2286-540a-dd9415535aba"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $apidata = json_decode($response);
        }
        echo "<pre>";
        print_r($apidata);
        die;
    }
    public function get_api_speaker_list()
    {
        $speaker_data = $this->db->get('temp_api_speaker')->result_array();
        $filename = "api_speaker_list.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Salutation";
        $header[] = "Firstname";
        $header[] = "Lastname";
        $header[] = "Company Name";
        $header[] = "Title";
        $header[] = "Logo";
        $header[] = "Speaker Desc";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($speaker_data as $key => $value)
        {
            $sdata['Salutation Name'] = $value['Salutation'];
            $sdata['Firstname'] = ucfirst($value['Firstname']);
            $sdata['Lastname'] = ucfirst($value['Lastname']);
            $sdata['Company Name'] = $value['Company_name'];
            $sdata['Title'] = $value['Title'];
            $sdata['Logo'] = $value['Logo'];
            $sdata['Speaker Desc'] = trim(strip_tags($value['Speaker_desc']));
            fputcsv($fp, $sdata);
        }
    }
    public function add_api_speakers()
    {
        $speaker_data = $this->db->get('temp_api_speaker')->result_array();
        foreach($speaker_data as $key => $value)
        {
            $sdata['Salutation'] = $value['Salutation'];
            $sdata['Firstname'] = ucfirst($value['Firstname']);
            $sdata['Lastname'] = ucfirst($value['Lastname']);
            $sdata['Company_name'] = $value['Company_name'];
            $sdata['Title'] = $value['Title'];
            $sdata['Logo'] = $value['Logo'];
            $sdata['Created_date'] = date('Y-m-d H:i:s');
            $sdata['Active'] = '1';
            $sdata['Organisor_id'] = 14558;
            $sdata['Speaker_desc'] = trim(strip_tags($value['Speaker_desc']));
            $this->db->insert('user', $sdata);
            $reldata['user_id'] = $this->db->insert_id();
            $sdata = array();
            $reldata['Event_id'] = 573;
            $reldata['Organisor_id'] = 14558;
            $reldata['Role_id'] = 7;
            $this->db->insert('relation_event_user', $reldata);
            $reldata = array();
            $this->db->where('sid', $value['sid']);
            $this->db->delete('temp_api_speaker');
        }
        echo "success";
        die;
    }
    public function get_api_sponsors_list()
    {
        $sponsors_data = $this->db->get('temp_api_sponsor')->result_array();
        $filename = "api_sponsors_list.csv";
        $fp = fopen('php://output', 'w');
        $header[] = "Sponsor Name";
        $header[] = "Description";
        $header[] = "Company Logo";
        $header[] = "Sponsor Type";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        fputcsv($fp, array());
        fputcsv($fp, $header);
        foreach($sponsors_data as $key => $value)
        {
            $sdata['Sponsor Name'] = ucfirst($value['Sponsors_name']);
            $sdata['Description'] = trim(strip_tags($value['Description']));
            $sdata['Company Logo'] = $value['company_logo'];
            $sdata['Sponsor Type'] = $value['sponsors_type'];
            fputcsv($fp, $sdata);
        }
    }
    public function get_api_attendee()

    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://ftw.eventoregistrations.com/api/EventoAPI/VisitorList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "Token=UKwD1QKBmxlEdmyF4Id9LKd8ykzA3h2X1yB3olELT0p6nYMrGZBsKOAj9aTcrVatuLwS7bE0c37z1T1ISCaDrKWBUQNmMYPgJm9Q",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: 70f23a15-f446-fb7d-b409-6f032c17acdf"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $final_array = json_decode($response);
        }
        foreach($final_array->objVisitorListDetailsReturn as $key => $value)
        {
            $userdbdata = $this->db->select('*')->from('user')->where('Email', $value->Email)->get()->result_array();
            $user_data['Salutation'] = $value->Title;
            $user_data['FirstName'] = $value->FirstName;
            $user_data['LastName'] = $value->LastName;
            $user_data['Title'] = $value->JobTitle;
            $user_data['Company_name'] = $value->Company;
            $country = $this->db->select('*')->from('country')->where('country_name', $value->Country)->get()->row_array();
            $user_data['Country'] = $country['id'];
            if (count($userdbdata) > 0)
            {
                $user_id = $userdbdata[0]['Id'];
                $this->db->where('Id', $user_id);
                $this->db->update('user', $user_data);
            }
            else
            {
                $user_data['Email'] = $value->Email;
                $user_data['Active'] = '1';
                $user_data['Created_date'] = date('Y-m-d H:i:s');
                $user_data['Organisor_id'] = 14558;
                $this->db->insert('user', $user_data);
                $user_id = $this->db->insert_id();
            }
            $rel_data['Event_id'] = 573;
            $rel_data['User_id'] = $user_id;
            $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
            if (count($reldata) > 0)
            {
                $Role_id = $reldata[0]['Role_id'];
            }
            else
            {
                $rel_data['Organisor_id'] = 14558;
                $rel_data['Role_id'] = 4;
                $this->db->insert('relation_event_user', $rel_data);
                $Role_id = 4;
            }
            if ($Role_id == 4)
            {
                $event_attendee_data['Event_id'] = 573;
                $event_attendee_data['Attendee_id'] = $user_id;
                $attendee_data = $this->db->select('*')->from('event_attendee')->where($event_attendee_data)->get()->result_array();
                if (count($attendee_data) > 0)
                {
                    if ($value->Mode == "Free")
                    {
                        $uevent_attendee_data['views_id'] = 363;
                    }
                    else
                    {
                        $uevent_attendee_data['views_id'] = 289;
                    }
                    $this->db->where($event_attendee_data);
                    $this->db->update('event_attendee', $uevent_attendee_data);
                }
                else
                {
                    if ($value->Mode == "Free")
                    {
                        $event_attendee_data['views_id'] = 363;
                    }
                    else
                    {
                        $event_attendee_data['views_id'] = 289;
                    }
                    $this->db->insert('event_attendee', $event_attendee_data);
                }
                $agenda_rel['attendee_id'] = $user_id;
                $agenda_rel['event_id'] = 573;
                $agendarel = $this->db->select('*')->from('attendee_agenda_relation')->where($agenda_rel)->get()->result_array();
                if (count($agendarel) < 1)
                {
                    $agenda_rel['agenda_category_id'] = 375;
                    $this->db->insert('attendee_agenda_relation', $agenda_rel);
                }
            }
        }
        echo "success";
        die;
    }

    public function cityscape_exibitor()
    {
        die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.cityscapeonline.com/mm/api/v1/user/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"username":"konduko","password":"Konduko.392"}',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/json',
                'Accept: application/json'
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $info = json_decode($response, true);
        }
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            CURLOPT_URL => "http://www.cityscapeonline.com/mm/api/v1/system/connect",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Cookie:' . $info['session_name'] . '=' . $info['sessid'],
                'X-CSRF-Token:' . $info['token'],
                'Content-type: application/json',
                'Accept: application/json'
            ) ,
        ));
        $response1 = curl_exec($curl1);
        $err1 = curl_error($curl1);
        curl_close($curl1);
        if ($err1)
        {
            echo "cURL Error #:" . $err1;
        }
        else
        {
            $connect_info = json_decode($response1, true);
        }
        $curl3 = curl_init();
        curl_setopt_array($curl3, array(
            CURLOPT_URL => "http://www.cityscapeonline.com/mm/api/v1/services-exhibitor-list?show_code=PEX17CS&updated=2017-06-01T00:00:00-06:00",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Cookie:' . $info['session_name'] . '=' . $info['sessid'],
                'X-CSRF-Token:' . $info['token'],
                'Content-type: application/json',
                'Accept: application/json'
            ) ,
        ));
        $response3 = curl_exec($curl3);
        $err2 = curl_error($curl3);
        curl_close($curl3);
        if ($err2)
        {
            echo "cURL Error #:" . $err2;
        }
        else
        {
            $exibitor_info = json_decode($response3, true);
        }
        // echo "<pre>";print_r($exibitor_info);die;
        foreach($exibitor_info as $key => $value)
        {
            $exbitor_data['updated'] = $value['updated'];
            $exbitor_data['show_code'] = $value['show_code'];
            $exbitor_data['first_name'] = $value['first_name'];
            $exbitor_data['last_name'] = $value['last_name'];
            $exbitor_data['email'] = trim($value['email']);
            $exbitor_data['stand'] = $value['stand'];
            $exbitor_data['description'] = is_array($value['description']) ? '' : $value['description'];
            $exbitor_data['title'] = trim($value['title']);
            $exbitor_data['nid'] = $value['nid'];
            if (is_array($value['logo']))
            {
                $logo = NULL;
            }
            else
            {
                $logo = $value['logo'];
            }
            $exbitor_data['logo'] = $logo;
            $exbitor_data['website_url'] = is_array($value['website_url']) ? '' : $value['website_url'];
            $keywords = "";
            if (count($value['nob']) > 0 && count($value['project_category']) > 0)
            {
                $keywords = implode(",", array_merge($value['nob'], $value['project_category']));
            }
            else
            {
                if (count($value['nob']) > 0)
                {
                    $keywords = implode(",", $value['nob']);
                }
                if (count($value['project_category']) > 0)
                {
                    $keywords = implode(",", $value['project_category']);
                }
            }
            $exbitor_data['keywords'] = $keywords;
            $this->db->insert('cityscape_temp_exibitor', $exbitor_data);
        }
        echo "Success<pre>";
        print_r($exibitor_info);
        die;
    }
    public function compress_images($source, $destination)
    {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg')
        {
            $imgesname = $destination . '.jpeg';
            $image = imagecreatefromjpeg($source);
            imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
        }
        elseif ($info['mime'] == 'image/gif')
        {
            $imgesname = $destination . '.gif';
            $image = imagecreatefromgif($source);
            imagegif($image, "./assets/user_files/" . $destination . '.gif');
        }
        elseif ($info['mime'] == 'image/png')
        {
            $imgesname = $destination . '.png';
            $image = imagecreatefrompng($source);
            $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
            imagecolortransparent($image, $background);
            imagealphablending($image, false);
            imagesavealpha($image, true);
            imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
        }
        return $imgesname;
    }
    public function add_cityscape_exibitor()
    {
        die("Direct Access Denied..");
        $oid = '26328';
        $eid = '671';
        $temp_exibitor = $this->db->select('*,group_concat(stand) as stand_number')->from('cityscape_temp_exibitor')->limit('50')->group_by('email,title')->get()->result_array();
        foreach($temp_exibitor as $key => $value)
        {
            $user_data = array();
            $user_data['Company_name'] = $value['title'];
            $user_data['Firstname'] = $value['first_name'];
            $user_data['Lastname'] = $value['last_name'];
            $ures = $this->db->select('*')->from('user')->where('Email', trim($value['email']))->get()->result_array();
            if (!empty($ures))
            {
                $user_data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email', trim($value['email']));
                $this->db->update('user', $user_data);
                $uid = $ures[0]['Id'];
            }
            else
            {
                $user_data['Email'] = trim($value['email']);
                $user_data['Created_date'] = date('Y-m-d H:i:s');
                $user_data['Active'] = '1';
                $user_data['Organisor_id'] = $oid;
                $this->db->insert('user', $user_data);
                $uid = $this->db->insert_id();
            }
            unset($user_data);
            $rel_data = array();
            $rel_data['Event_id'] = $eid;
            $rel_data['User_id'] = $uid;
            $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
            if (!empty($reldata))
            {
                $rid = $reldata[0]['Role_id'];
            }
            else
            {
                $rel_data['Organisor_id'] = $oid;
                $rel_data['Role_id'] = 6;
                $this->db->insert('relation_event_user', $rel_data);
                $rid = 6;
            }
            unset($rel_data);
            $ebi_data = array();
            if ($rid == 6)
            {
                $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['title']))->get()->result_array();
                $ebi_data['Heading'] = trim($value['title']);
                $ebi_data['Description'] = $value['description'];
                $ebi_data['stand_number'] = $value['stand_number'];
                if (!empty($value['logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                }
                $ebi_data['website_url'] = $value['website_url'];
                $ebi_data['Short_desc'] = $value['keywords'];
                if (!empty($eres))
                {
                    $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['title']));
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = $uid;
                    $this->db->insert('exibitor', $ebi_data);
                }
            }
            unset($ebi_data);
            $this->db->where('email', $value['email']);
            $this->db->delete('cityscape_temp_exibitor');
        }
        echo "Success";
        die;
    }
    public function add_dims_temp_exibitor()
    {
        // die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/exhibitor?siteid=7E20C8F3-5056-B733-836332C904B47C05&limit=300",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_exibitor = json_decode($response, true);
        }
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor[0]['exhibitor'] as $key => $value)
        {
            $temp_exibitor_data['FirstName'] = $value['contact'][0]['FirstName'];
            $temp_exibitor_data['Lastname'] = $value['contact'][0]['LastName'];
            $temp_exibitor_data['Email'] = trim($value['contact'][0]['emailAddress']);
            $temp_exibitor_data['Title'] = $value['contact'][0]['JobTitle'];
            $temp_exibitor_data['Mobile'] = $value['contact'][0]['phone'];
            $temp_exibitor_data['Logo'] = $value['logo'];
            if (!empty($value['cover-image']))
            {
                $temp_exibitor_data['Images'] = 'https://www.dubaimotorshow.com' . $value['cover-image'];
            }
            else
            {
                $temp_exibitor_data['Images'] = NULL;
            }
            $temp_exibitor_data['stand_number'] = $value['stand'];
            $carr = $this->db->select('*')->from('country')->where('country_name', trim($value['Country']))->get()->result_array();
            $temp_exibitor_data['Country'] = $carr[0]['id'];
            $temp_exibitor_data['Description'] = $value['description'];
            $temp_exibitor_data['facebook_url'] = $value['social_facebook'];
            $temp_exibitor_data['youtube_url'] = $value['social_youtube'];
            $temp_exibitor_data['linkedin_url'] = $value['social_linkedin'];
            $temp_exibitor_data['instagram_url'] = $value['social_instagram'];
            $temp_exibitor_data['twitter_url'] = $value['social_twitter'];
            $temp_exibitor_data['website_url'] = $value['website'];
            $temp_exibitor_data['Heading'] = $value['title'];
            $temp_exibitor_data['Postcode'] = $value['PostalCode'];
            $temp_exibitor_data['Short_desc'] = implode(',', array_column_1($value['category'], 'Label'));
            $this->db->insert('dims_temp_exibitor', $temp_exibitor_data);
            unset($temp_exibitor_data);
        }
        die("Success");
    }
    public function add_dims_exibitor()
    {
        // die("Direct Access Denied..");
        $oid = '28281';
        $eid = '694';
        $temp_exibitor = $this->db->select('*,group_concat(stand_number) as stand_number')->from('dims_temp_exibitor')->limit('50')->group_by('Email,Heading')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['Lastname'];
                $user_data['Title'] = $value['Title'];
                $user_data['Company_name'] = $value['Heading'];
                $user_data['Postcode'] = $value['Postcode'];
                $user_data['Mobile'] = $value['Mobile'];
                $user_data['Country'] = $value['Country'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Heading'];
                $ebi_data['Description'] = $value['Description'];
                $ebi_data['stand_number'] = $value['stand_number'];
                if (!empty($value['Logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['Logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['website_url'];
                $ebi_data['facebook_url'] = $value['facebook_url'];
                $ebi_data['youtube_url'] = $value['youtube_url'];
                $ebi_data['linkedin_url'] = $value['linkedin_url'];
                $ebi_data['instagram_url'] = $value['instagram_url'];
                $ebi_data['twitter_url'] = $value['twitter_url'];
                $ebi_data['Short_desc'] = $value['Short_desc'];
                $ebi_data['country_id'] = $value['Country'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Heading']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('dims_temp_exibitor');
        }
        echo "Success";
        die;
    }
    public function add_temp_dims_products()
    {
        // die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/entry?libraryId=59708",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_products = json_decode($response, true);
            echo "<pre>";
            print_r($temp_products);
            exit;
            foreach($temp_products as $key => $value)
            {
                $product = $this->db->where('name', $value['title'])->where('event_id', '694')->get('product')->row_array();
                $data['name'] = $value['title'];
                $data['description'] = $value['description'];
                $data['slug'] = str_replace(' ', '_', $value['title']);
                $data['event_id'] = '694';
                $data['price'] = '1';
                $data['auctionType'] = '3';
                $data['status'] = '1';
                $data['style_status'] = '1';
                $data['features_product'] = '1';
                $data['approved_status'] = '1';
                $data['product_preview'] = '1';
                $file_name = time() . 'product_img.jpeg';
                $destination = "./fundraising/assets/images/products/thumb/" . $file_name;
                $destination1 = "./fundraising/assets/images/products/" . $file_name;
                copy($value['Supporting-Image-1'], $destination);
                copy($destination, $destination1);
                $config['image_library'] = 'gd2';
                $config['source_image'] = $destination;
                $config['file_name'] = $file_name;
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 309;
                $config['height'] = 309;
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $new_image_path = str_replace("./", "", $destination);
                $data['thumb'] = "assets/images/products/thumb/" . $file_name;
                if (!empty($product))
                {
                    $this->db->where($product);
                    $this->db->update('product', $data);
                    $this->db->where('product_id', $product['product_id'])->delete('product_image');
                    $p_image['image_url'] = 'assets/images/products/' . $file_name;
                    $p_image['product_id'] = $product['product_id'];
                    $this->db->insert('product_image', $p_image);
                }
                else
                {
                    $this->db->insert('product', $data);
                    $p_image['image_url'] = 'assets/images/products/' . $file_name;
                    $p_image['product_id'] = $this->db->insert_id();
                    $this->db->insert('product_image', $p_image);
                }
                unset($data);
            }
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/Products/" . $temp_products[0]['ProductUuid'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_product = json_decode($response, true);
        }
        echo "<pre>";
        print_r($temp_products);
        die;
    }
    public function add_temp_dims_Speakers()
    {
        die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/speaker?siteid=7E20C8F3-5056-B733-836332C904B47C05",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_speaker = json_decode($response, true);
        }
        echo "<pre>";
        print_r($temp_speaker);
        die;
    }
    public function add_temp_WorldEthanolBiofuels_data()
    {
        die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKB2274/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_WorldEthanolBiofuels_data()
    {
        die("Direct Access Denied..");
        $this->Allapi_model->add_KNECT365_exibitor(638);
        $this->Allapi_model->add_KNECT365_speakers(638);
        $this->Allapi_model->add_KNECT365_session(638, 705);
        echo "SuccessFully.....";
        die;
    }
    public function add_temp_CrewConnect_Global_data()
    {
        // die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3344/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        echo "<pre>";
        print_r($alltempdata['agenda']);
        exit();
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_CrewConnect_Global_data()
    {
        // die("Direct Access Denied..");
        // $this->Allapi_model->add_KNECT365_exibitor(896);
        // $this->Allapi_model->add_KNECT365_speakers(896);
        $this->Allapi_model->add_KNECT365_session(896, 696);
        echo "SuccessFully.....";
        die;
    }
    public function add_temp_GSTShipping2030NorthAmerica_data()
    {
        // die("Direct Access Denied..");
        /*$fulldata=file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3343/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata=json_decode($fulldata,true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        $fulldata1=file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3342/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata1=json_decode($fulldata1,true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata1['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata1['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata1['agenda']);
        echo "SuccessFully.....";die;*/
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3343/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        // $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        // $fulldata1=file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3342/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        // $alltempdata1=json_decode($fulldata1,true);
        // $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata1['organisationCategories']);
        // $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata1['speakers']);
        // $this->Allapi_model->add_temp_KNECT365_session($alltempdata1['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_GSTShipping2030NorthAmerica_data()
    {
        // die("Direct Access Denied..");
        // $this->Allapi_model->add_KNECT365_exibitor(934);
        // $this->Allapi_model->add_KNECT365_speakers(934);
        $this->Allapi_model->add_KNECT365_session(934, 769);
        echo "SuccessFully.....";
        die;
    }
    public function add_temp_SalvageWreck_data()
    {
        // die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3357/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_SalvageWreck_data()
    {
        // die("Direct Access Denied..");
        // $this->Allapi_model->add_KNECT365_exibitor(936);
        // $this->Allapi_model->add_KNECT365_speakers(936);
        $this->Allapi_model->add_KNECT365_session(936, 738);
        echo "SuccessFully.....";
        die;
    }
    public function add_temp_BWMTech_data()
    {
        // die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3384/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_BWMTech_data()
    {
        // die("Direct Access Denied..");
         //$this->Allapi_model->add_KNECT365_exibitor(937);
         //$this->Allapi_model->add_KNECT365_speakers(937);
        $this->Allapi_model->add_KNECT365_session(937, 739);
        echo "SuccessFully.....";
        die;
    }
    public function add_temp_EthanolLatinAmerica_data()
    {
        // die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKB2275/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);

        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_EthanolLatinAmerica_data()
    {
        // die("Direct Access Denied..");
        // $this->Allapi_model->add_KNECT365_exibitor(938);
        // $this->Allapi_model->add_KNECT365_speakers(938);
        $this->Allapi_model->add_KNECT365_session(938, 740);
        echo "SuccessFully.....";
        die;
    }


    public function add_temp_sugar_ethanol_asia()
    {

        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKB2276/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;

    }

    public function add_sugar_ethanol_asia_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1205);
        //$this->Allapi_model->add_KNECT365_speakers(1205);
        $this->Allapi_model->add_KNECT365_session(1205, 1129);
        echo "SuccessFully.....";
        die;

    }


    public function add_temp_CrewConnectEurope()
    {

        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3422/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;

    }

    public function add_CrewConnectEurope_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1468);
        //$this->Allapi_model->add_KNECT365_speakers(1468);
        $this->Allapi_model->add_KNECT365_session(1468, 1420);
        echo "SuccessFully.....";
        die;

    }


    public function add_temp_sugar_ethanol_brazil()
    {

        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKB2277/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;

    }

    public function add_sugar_ethanol_brazil_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1346);
        //$this->Allapi_model->add_KNECT365_speakers(1346);
        //$this->Allapi_model->add_KNECT365_session(1346, 1257);
        echo "SuccessFully.....";
        die;

    }

    public function add_temp_FLAME_knect()
    {

        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKA2659/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;

    }

    public function add_FLAME_knect_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1389);
        //$this->Allapi_model->add_KNECT365_speakers(1389);
        $this->Allapi_model->add_KNECT365_session(1389, 1360);
        echo "SuccessFully.....";
        die;

    }

    public function add_temp_GSTShipping2030Europe()
    {

        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3394/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);

        $this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        $this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        $this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;

    }

    public function add_GSTShipping2030Europe_data()
    {

        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1235);
        //$this->Allapi_model->add_KNECT365_speakers(1235);
        $this->Allapi_model->add_KNECT365_session(1235, 1130);
        echo "SuccessFully.....";
        die;

    }



    public function add_dims_video()
    {
        die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/entry?libraryId=59543",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_video = json_decode($response, true);
        }
        j($temp_video);
        foreach($temp_video as $key => $value)
        {
            if (strpos($value['video-embed-code'], 'iframe'))
            {
                preg_match('/src="([^"]+)"/', $value['video-embed-code'], $match);
                $url = $match[1];
            }
            else
            {
                $url = str_replace('watch?v=', 'embed/', $value['video-embed-code']);
            }
            $data['event_id'] = '694';
            $data['url'] = $url;
            $data['title'] = $value['title'];
            $data['desc'] = $value['abstract'];
            $data['author'] = $value['author'];
            $res = $this->db->where('url', $url)->where('event_id', '694')->get('event_videos')->row_array();
            if (!empty($res))
            {
                $this->db->where('url', $url)->where('event_id', '694')->update('event_videos', $data);
            }
            else
            {
                $this->db->insert('event_videos', $data);
            }
        }
    }
    public function show_dims_videos()
    {
        $this->data['data'] = $this->db->where('event_id', '694')->get('event_videos')->result_array();
        $this->load->view('dims_video', $this->data);
    }
    public function show_dims_news()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/entry?libraryId=59544",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic QTkzMDQ5QzExOTQ5NTY3QzlDQjI2MEMyOTczRjMyNEM3NEM4RkNCMjpkdWJhMW0wdDByc2gwdzE3",
                "cache-control: no-cache",
                "postman-token: 1bfbca27-abf5-36f9-090c-6cbd976d8aec"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_res = json_decode($response, true);
            $this->data['data'] = $temp_res;
        }
        // j($temp_res);
        $this->load->view('dims-cms/dims_news', $this->data);
    }
    public function add_cabsat_temp_exibitor()
    {
        // die("Direct Access Denied..");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.showoff.asp.com/public/exhibitor?limit=1000",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic RkU5MjBCREI2NDMyM0FGNjYxMkQzNzlFREEwMEJGMzRDQjE0N0U0QTpAbGwxbnRoZWwwMHA=",
                "cache-control: no-cache",
                "postman-token: 771b65b1-0d52-30b9-3e02-b603fe77f7d7"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $temp_exibitor = json_decode($response, true);
        }
        // echo $response;exit;
        //echo "<pre>";print_r($temp_exibitor);die;
        //die();
        foreach($temp_exibitor[0]['exhibitor'] as $key => $value)
        {
            $temp_exibitor_data['FirstName'] = $value['contact'][0]['FirstName'];
            $temp_exibitor_data['Lastname'] = $value['contact'][0]['LastName'];
            $temp_exibitor_data['Email'] = trim($value['contact'][0]['emailAddress']);
            $temp_exibitor_data['Title'] = $value['contact'][0]['JobTitle'];
            $temp_exibitor_data['Mobile'] = $value['contact'][0]['phone'];
            $temp_exibitor_data['Logo'] = $value['logo'];
            if (!empty($value['cover-image']))
            {
                $temp_exibitor_data['Images'] = $value['cover-image'];
            }
            else
            {
                $temp_exibitor_data['Images'] = NULL;
            }
            $temp_exibitor_data['stand_number'] = $value['stand'];
            $carr = $this->db->select('*')->from('country')->where('country_name', trim($value['Country']))->get()->result_array();
            $temp_exibitor_data['Country'] = $carr[0]['id'];
            $temp_exibitor_data['Description'] = $value['description'];
            $temp_exibitor_data['facebook_url'] = $value['social_facebook'];
            $temp_exibitor_data['youtube_url'] = $value['social_youtube'];
            $temp_exibitor_data['linkedin_url'] = $value['social_linkedin'];
            $temp_exibitor_data['instagram_url'] = $value['social_instagram'];
            $temp_exibitor_data['twitter_url'] = $value['social_twitter'];
            $temp_exibitor_data['website_url'] = $value['website'];
            $temp_exibitor_data['Heading'] = $value['title'];
            $temp_exibitor_data['Postcode'] = $value['PostalCode'];
            $temp_exibitor_data['Short_desc'] = implode(',', array_column_1($value['category'], 'Label'));
            $this->db->insert('cabsat_temp_exibitor', $temp_exibitor_data);
            unset($temp_exibitor_data);
        }
        die("Success");
    }
    public function add_cabsat_exibitor()
    {
        // die("Direct Access Denied..");
        $oid = '52092';
        $eid = '1020';
        $temp_exibitor = $this->db->select('*,group_concat(stand_number) as stand_number')->from('cabsat_temp_exibitor')->limit('50')->group_by('Email,Heading')->get()->result_array();
        // echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['Email'])))
            {
                $user_data['Firstname'] = $value['FirstName'];
                $user_data['Lastname'] = $value['Lastname'];
                $user_data['Title'] = $value['Title'];
                $user_data['Company_name'] = $value['Heading'];
                $user_data['Postcode'] = $value['Postcode'];
                $user_data['Mobile'] = $value['Mobile'];
                $user_data['Country'] = $value['Country'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['Email']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['Email']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['Email']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['Email'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['Heading'];
                $ebi_data['Description'] = $value['Description'];
                $ebi_data['stand_number'] = $value['stand_number'];
                if (!empty($value['Logo']))
                {
                    $destination = "company_logo_". uniqid();
                    $source = $value['Logo'];
                    //$content = file_get_contents($source);
					//Store in the filesystem.
					//$fp = fopen("./assets/user_files/".$destination.".png", "w");
					//fwrite($fp, $content);
					//fclose($fp);
                    //copy($source,$destination);
                    //$info = getimagesize(str_replace('https','http',$source));
	                //$info= pathinfo($source, PATHINFO_EXTENSION);
                    $info = getimagesize($source);
               
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    //$logo = $destination;
                    $ebi_data['company_logo'] = json_encode(array($logo));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['website_url'];
                $ebi_data['facebook_url'] = $value['facebook_url'];
                $ebi_data['youtube_url'] = $value['youtube_url'];
                $ebi_data['linkedin_url'] = $value['linkedin_url'];
                $ebi_data['instagram_url'] = $value['instagram_url'];
                $ebi_data['twitter_url'] = $value['twitter_url'];
                $ebi_data['Short_desc'] = $value['Short_desc'];
                $ebi_data['country_id'] = $value['Country'];
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['Heading']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['Heading']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('Email', $value['Email']);
            $this->db->delete('cabsat_temp_exibitor');
        }
        $total_exhi = $this->db->select()->from('cabsat_temp_exibitor')->group_by('Email,Heading')->get()->num_rows();
        echo "Success -> Remaning Exhi. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function helth_temp_exhi()
    {	
    	$this->arab_temp_category();
    	$this->add_arab_helth_category();
    	$this->add_arab_helth_country();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/catalogue/web/PEX18AH/MDArY0plam5yd2YvcUFxd29VN1FCeDArTlF3VXVPeWsvVjNGT3ErWjlqWT01/.%2A/all/all/.%2A/all/false",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
                "Postman-Token: 789ab832-f5a2-122a-0d37-b5d9ae10edf8"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $data = json_decode($response,true);
            foreach ($data as $key => $value)
            {
            	$tmp_exhi['type'] = $value['entry'];
            	$tmp_exhi['exhibitorguid'] = $value['exhibitorguid'].'@venturiapps.com';
            	$tmp_exhi['stand'] = $value['stand'];
            	$tmp_exhi['logo'] = $value['logo'];
            	$tmp_exhi['companyname'] = $value['companyname'];
            	$coun_id = $this->db->select('*')->from('country')->where('country_name', trim($value['country']))->get()->row_array();
            	$tmp_exhi['country'] = $coun_id['id'];
            	$tmp_exhi['website'] = $value['website'];
            	
            	$tmp_exhi['products'] = implode(',',$value['products']);
            	$tmp_exhi['email'] = $value['email'];
            	$tmp_exhi['phone'] = $value['phone'];
            	$tmp_exhi['contactperson'] = $value['contactperson'];
            	$tmp_exhi['link_facebook'] = $value['link_facebook'];
            	$tmp_exhi['link_twitter'] = $value['link_twitter'];
            	$tmp_exhi['link_linkedin'] = $value['link_linkedin'];
            	$tmp_exhi['link_youtube'] = $value['link_youtube'];
            	$tmp_exhi['link_instagram'] = $value['link_instagram'];
            	$tmp_exhi['keywords'] = $value['country'].(!empty($value['products']) ? ',' : '').implode(',',$value['products']);
            	if($value['entry'] == 'Bronze')
            	{
                    $tmp_exhi['description'] = !empty($value['country'])?'Country: '.$value['country']:'';
            	}
            	else if($value['entry'] == 'Gold')
            	{	
            		/*$tmp_exhi['description'] = $value['description'].'<br>'.$value['country'].'<br>'.str_replace(',','<br>',$value['email']).'<br>'.$value['phone'];*/
            		$tmp_exhi['description'] = $value['description'].'<br><br>';
            		$tmp_exhi['description'] .= 'Email: '.str_replace(',','<br>',$value['email']).'<br>';
            		$tmp_exhi['description'] .= 'Phone: '.$value['phone'].'<br>';
            		$tmp_exhi['description'] .= 'Country: '.$value['country'];
            	}
            	$this->db->insert('arab_temp_exhi',$tmp_exhi);
            	unset($tmp_exhi);
            	unset($coun_id);
            }
            echo "tmp exhi added";
        }
    }
    public function add_arab_helth_exhi()
    {
        // die("Direct Access Denied..");
        date_default_timezone_set('GMT');
        set_time_limit(0);
        $oid = '16909';
        $eid = '634';
        $temp_exibitor = $this->db->from('arab_temp_exhi')->limit('50')->get()->result_array();
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['exhibitorguid'])))
            {
                $user_data['Firstname'] = $value['contactperson'];
                $user_data['Company_name'] = $value['companyname'];
                $user_data['Mobile'] = $value['phone'];
                $user_data['Country'] = $value['country'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['exhibitorguid']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['exhibitorguid']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['exhibitorguid']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['exhibitorguid'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['companyname'];
                $ebi_data['Description'] = $value['description'];
                $ebi_data['stand_number'] = $value['stand'];
                if (!empty($value['logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['website'];
                $ebi_data['facebook_url'] = $value['link_facebook'];
                $ebi_data['youtube_url'] = $value['link_youtube'];
                $ebi_data['linkedin_url'] = $value['link_linkedin'];
                $ebi_data['instagram_url'] = $value['link_instagram'];
                $ebi_data['twitter_url'] = $value['link_twitter'];
                $ebi_data['Short_desc'] = $value['keywords'];
                $ebi_data['country_id'] = $value['country'];
                $ebi_data['email_address'] = $value['email'];
                if($value['type'] == 'Gold')
                {
                	$ebi_data['et_id'] = '139';
                }
                else if($value['type'] == 'Bronze')
                {
                	$ebi_data['et_id'] = '159';
                	$ebi_data['Description'] = $value['description'];
                }
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['companyname']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('exhibitorguid', $value['exhibitorguid']);
            $this->db->delete('arab_temp_exhi');
        }
        $total_exhi = $this->db->select()->from('arab_temp_exhi')->get()->num_rows();
        echo "Success -> Remaning Exhi. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        /*if(!empty($total_exhi))
        	return $this->add_arab_helth_exhi();
        else
        	echo "all exhi added";*/
        die;
    }
    public function arab_temp_category()
    {
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/system/product-categories/PEX18AH",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Cache-Control: no-cache",
		"Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
		"Postman-Token: edcdf8e9-b16e-fdba-9099-85c9efc08b3c"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		$data = json_decode($response,true);
			foreach ($data as $key => $value)
			{
				$tmp_cat['category_name'] = $value;
				$tmp_cat['keyword'] = $value;
				$this->db->insert('arab_temp_exhi_category',$tmp_cat);
				unset($tmp_cat);
			}
			echo "tmp categories added.";
		}
    }
    public function add_arab_helth_category()
    {	
    	date_default_timezone_set('GMT');
    	$categories = $this->db->get('arab_temp_exhi_category')->result_array();
    	foreach ($categories as $key => $value)
    	{
    		$tmp_cat['category'] = $value['category_name'];
    		$tmp_cat['categorie_keywords'] = $value['keyword'];
    		$tmp_cat['event_id'] = '634';
    		$tmp_cat['category_type'] = '0';

    		$check_cat = $this->db->where($tmp_cat)->get('exhibitor_category')->row_array();
    		if(empty($check_cat))
    		{
    			$tmp_cat['created_date'] = date('Y-m-d H:i:s');
    			$tmp_cat['updated_date'] = date('Y-m-d H:i:s');
    			$this->db->insert('exhibitor_category',$tmp_cat);
    			unset($tmp_cat);
    		}
    		$this->db->where($value);
    		$this->db->delete('arab_temp_exhi_category');
    	}
    	echo "Exhi categories added";
    }
    public function add_arab_helth_country()
    {
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/system/countries/PEX18AH",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
		    "Postman-Token: c1cf606f-0ca4-2875-a38b-42e8b4cc5c80"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		}
		else
		{
		 	$data = json_decode($response);
		 	foreach ($data as $key => $value)
		 	{
		 		$res = $this->db->where('country_name',$value)->get('country')->row_array();
		 		if(empty($res))
		 		{
		 			$insert['country_name'] = $value;
		 			$this->db->insert('country',$insert);
		 			unset($insert);
		 		}
		 	}
		 	echo "country added SuccessFully";
		}
    }
    public function medlab_temp_exhi()
    {	
    	$this->medlab_temp_category();
    	$this->add_medlab_category();
    	$this->add_medlab_country();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/catalogue/web/PEX18ML/OHB6MjdjYU94UlY1eGdDaVZUY0tuN3Y1UDViOWtOOHpTWHBuY1orZTZTOD01/.%2A/all/all/.%2A/all/false",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
                "Postman-Token: 789ab832-f5a2-122a-0d37-b5d9ae10edf8"
            ) ,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            $data = json_decode($response,true);
            /*echo "<pre>";print_r($data);exit;*/
            foreach ($data as $key => $value)
            {
            	$tmp_exhi['type'] = $value['entry'];
            	$tmp_exhi['exhibitorguid'] = $value['exhibitorguid'].'@venturiapps.com';
            	$tmp_exhi['stand'] = $value['stand'];
            	$tmp_exhi['logo'] = $value['logo'];
            	$tmp_exhi['companyname'] = $value['companyname'];
            	$coun_id = $this->db->select('*')->from('country')->where('country_name', trim($value['country']))->get()->row_array();
            	$tmp_exhi['country'] = $coun_id['id'];
            	$tmp_exhi['website'] = $value['website'];
            	
            	$tmp_exhi['products'] = implode(',',$value['products']);
            	$tmp_exhi['email'] = $value['email'];
            	$tmp_exhi['phone'] = $value['phone'];
            	$tmp_exhi['contactperson'] = $value['contactperson'];
            	$tmp_exhi['link_facebook'] = $value['link_facebook'];
            	$tmp_exhi['link_twitter'] = $value['link_twitter'];
            	$tmp_exhi['link_linkedin'] = $value['link_linkedin'];
            	$tmp_exhi['link_youtube'] = $value['link_youtube'];
            	$tmp_exhi['link_instagram'] = $value['link_instagram'];
            	$tmp_exhi['keywords'] = $value['country'].(!empty($value['products']) ? ',' : '').implode(',',$value['products']);
            	if($value['entry'] == 'Bronze')
            	{
            		$tmp_exhi['description'] = !empty($value['country'])?'Country: '.$value['country']:'';
            	}
            	else if($value['entry'] == 'Gold')
            	{	
            		//$tmp_exhi['description'] = $value['description'].','.$value['country'].','.$value['email'].','.$value['phone'];
            		$tmp_exhi['description'] = $value['description'].'<br><br>';
            		$tmp_exhi['description'] .= 'Email: '.str_replace(',','<br>',$value['email']).'<br>';
            		$tmp_exhi['description'] .= 'Phone: '.$value['phone'].'<br>';
            		$tmp_exhi['description'] .= 'Country: '.$value['country'];
            	}
            	$this->db->insert('madlab_temp_exhi',$tmp_exhi);
            	unset($tmp_exhi);
            	unset($coun_id);
            }
            echo "tmp exhi added";
        }
    }
    public function add_medlab_exhi()
    {
        set_time_limit(0);
    	date_default_timezone_set('GMT');
        $oid = '16909';
        $eid = '1012';
        $temp_exibitor = $this->db->from('madlab_temp_exhi')->limit('50')->get()->result_array();
        //echo "<pre>";print_r($temp_exibitor);die;
        foreach($temp_exibitor as $key => $value)
        {
            if (!empty(trim($value['exhibitorguid'])))
            {
                $user_data['Firstname'] = $value['contactperson'];
                $user_data['Company_name'] = $value['companyname'];
                $user_data['Mobile'] = $value['phone'];
                $user_data['Country'] = $value['country'];
                $ures = $this->db->select('*')->from('user')->where('Email', trim($value['exhibitorguid']))->get()->result_array();
                if (count($ures) > 0)
                {
                    $user_data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email', trim($value['exhibitorguid']));
                    $this->db->update('user', $user_data);
                    $uid = $ures[0]['Id'];
                }
                else
                {
                    $user_data['Email'] = trim($value['exhibitorguid']);
                    $user_data['Created_date'] = date('Y-m-d H:i:s');
                    $user_data['Active'] = '1';
                    $user_data['Organisor_id'] = $oid;
                    $this->db->insert('user', $user_data);
                    $uid = $this->db->insert_id();
                }
                unset($user_data);
                $rel_data['Event_id'] = $eid;
                $rel_data['User_id'] = $uid;
                $reldata = $this->db->select('*')->from('relation_event_user')->where($rel_data)->get()->result_array();
                if (count($reldata) > 0)
                {
                    $rid = $reldata[0]['Role_id'];
                }
                else
                {
                    $rel_data['Organisor_id'] = $oid;
                    $rel_data['Role_id'] = 6;
                    $this->db->insert('relation_event_user', $rel_data);
                    $rid = 6;
                }
                unset($rel_data);
            }
            if ($rid == 6 || empty(trim($value['exhibitorguid'])))
            {
                if (!empty($uid))
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                else
                {
                    $eres = $this->db->select('*')->from('exibitor')->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']))->get()->result_array();
                }
                $ebi_data['Heading'] = $value['companyname'];
                $ebi_data['Description'] = $value['description'];
                $ebi_data['stand_number'] = $value['stand'];
                if (!empty($value['logo']))
                {
                    $destination = "company_logo_" . uniqid();
                    $source = $value['logo'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $logo = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $logo = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $logo = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['company_logo'] = json_encode(array(
                        $logo
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($logo);
                    unset($image);
                }
                else
                {
                    $ebi_data['company_logo'] = NULL;
                }
                if (!empty($value['Images']))
                {
                    $destination = "banner_image_" . uniqid();
                    $source = $value['Images'];
                    $info = getimagesize($source);
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $banner = $destination . '.jpeg';
                        $image = imagecreatefromjpeg($source);
                        imagejpeg($image, "./assets/user_files/" . $destination . '.jpeg', 60);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $banner = $destination . '.gif';
                        $image = imagecreatefromgif($source);
                        imagegif($image, "./assets/user_files/" . $destination . '.gif');
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $banner = $destination . '.png';
                        $image = imagecreatefrompng($source);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, "./assets/user_files/" . $destination . '.png', 6);
                    }
                    $ebi_data['Images'] = json_encode(array(
                        $banner
                    ));
                    unset($destination);
                    unset($source);
                    unset($info);
                    unset($banner);
                    unset($image);
                }
                else
                {
                    $ebi_data['Images'] = NULL;
                }
                $ebi_data['website_url'] = $value['website'];
                $ebi_data['facebook_url'] = $value['link_facebook'];
                $ebi_data['youtube_url'] = $value['link_youtube'];
                $ebi_data['linkedin_url'] = $value['link_linkedin'];
                $ebi_data['instagram_url'] = $value['link_instagram'];
                $ebi_data['twitter_url'] = $value['link_twitter'];
                $ebi_data['Short_desc'] = $value['keywords'];
                $ebi_data['country_id'] = $value['country'];
                $ebi_data['email_address'] = $value['email'];
                if($value['type'] == 'Gold')
                {
                    $ebi_data['et_id'] = '172';
                }
                else if($value['type'] == 'Bronze')
                {
                    $ebi_data['et_id'] = '173';
                    $ebi_data['Description'] = $value['description'];
                }
                if (count($eres) > 0)
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    if (!empty($uid))
                    {
                        $this->db->where('Event_id', $eid)->where('user_id', $uid)->where('Heading', trim($value['companyname']));
                    }
                    else
                    {
                        $this->db->where('Event_id', $eid)->where('user_id IS NULL')->where('Heading', trim($value['companyname']));
                    }
                    $this->db->update('exibitor', $ebi_data);
                }
                else
                {
                    $ebi_data['Organisor_id'] = $oid;
                    $ebi_data['Event_id'] = $eid;
                    $ebi_data['user_id'] = !empty($uid) ? $uid : NULL;
                    $this->db->insert('exibitor', $ebi_data);
                }
                unset($eres);
                unset($ebi_data);
            }
            $this->db->where('exhibitorguid', $value['exhibitorguid']);
            $this->db->delete('madlab_temp_exhi');
        }
        $total_exhi = $this->db->select()->from('madlab_temp_exhi')->get()->num_rows();
        echo "Success -> Remaning Exhi. -> " . $total_exhi . " Refresh Page -> " . round($total_exhi / 50 + 1) . " Time";
        die;
    }
    public function medlab_temp_category()
    {
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/system/product-categories/PEX18ML",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
		"Cache-Control: no-cache",
		"Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
		"Postman-Token: edcdf8e9-b16e-fdba-9099-85c9efc08b3c"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		$data = json_decode($response,true);
			foreach ($data as $key => $value)
			{
				$tmp_cat['category_name'] = $value;
				$tmp_cat['keyword'] = $value;
				$this->db->insert('medlab_exhi_category',$tmp_cat);
				unset($tmp_cat);
			}
			echo "tmp categories added.";
		}
    }
    public function add_medlab_category()
    {
    	$categories = $this->db->get('medlab_exhi_category')->result_array();
    	foreach ($categories as $key => $value)
    	{
    		$tmp_cat['category'] = $value['category_name'];
    		$tmp_cat['categorie_keywords'] = $value['keyword'];
    		$tmp_cat['event_id'] = '1012';
    		$tmp_cat['category_type'] = '0';

    		$check_cat = $this->db->where($tmp_cat)->get('exhibitor_category')->row_array();
    		if(empty($check_cat))
    		{
    			$tmp_cat['created_date'] = date('Y-m-d H:i:s');
    			$tmp_cat['updated_date'] = date('Y-m-d H:i:s');
    			$this->db->insert('exhibitor_category',$tmp_cat);
    			unset($tmp_cat);
    		}
    		$this->db->where($value);
    		$this->db->delete('medlab_exhi_category');
    	}
    		echo "Exhi categories added";
    }
    public function add_medlab_country()
    {
    	$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://geukmeexhibitors.azure-api.net/system/countries/PEX18ML",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Cache-Control: no-cache",
		    "Ocp-Apim-Subscription-Key: 8a6da642b5be4acb8e7f9be358c1553e",
		    "Postman-Token: c1cf606f-0ca4-2875-a38b-42e8b4cc5c80"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		}
		else
		{
		 	$data = json_decode($response);
		 	foreach ($data as $key => $value)
		 	{
		 		$res = $this->db->where('country_name',$value)->get('country')->row_array();
		 		if(empty($res))
		 		{
		 			$insert['country_name'] = $value;
		 			$this->db->insert('country',$insert);
		 			unset($insert);
		 		}
		 	}
		 	echo "country added SuccessFully";
		}
    }
    public function add_arab_health_attendee_new()
    {
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $Unique_no = $this->input->post('Unique_no');
        $api_user_id = $this->input->post('api_user_id');
        $api_pass = $this->input->post('api_pass');

        if($api_user_id != 'arabhealth' && $api_pass != 'h@JA^MH6C?')
        {
            $data = array(
                    'success' => false,
                    'message' => 'API Credentials Not Valid'
                );
            echo json_encode($data);exit; 
        }
        if(!empty($Unique_no))
        {   
            /*if(!empty($Email))
            {
                if(strpos($Email, '@') === false)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Email should be valid'
                    );
                    echo json_encode($data);exit;   
                }
            }*/
               
            $check_unique = $this->db->where('Unique_no',$Unique_no)->get('user')->row_array();
            if($check_unique)
            {
                $data = array(
                    'success' => false,
                    'message' => 'Unique no is already exists'
                );
                echo json_encode($data);exit;
            }

            $this->load->model('Native_single_fcm/App_Login_model');
            $event_id = 634;
            $org_id=$this->App_Login_model->getOrganizerByEvent($event_id);
            //$user = $this->db->where('Email',$Email)->get('user')->row_array();

            $data['Company_name'] = $Company_name;
            $data['Firstname'] = $Firstname;
            $data['Lastname'] = $Lastname;
            $data['Email'] = $Email;
            $data['Street'] = $Street;
            $data['Suburb'] = $Suburb;
            $data['State'] = $State;
            $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
            $data['Country'] = ($country_id['id'])?:$Country;
            $data['Postcode'] = $Postcode;
            $data['Mobile'] = $Mobile;
            $data['Phone_business'] = $Phone_business;
            $data['Salutation'] = $Salutation;
            $data['Title'] = $Title;
            $data['Unique_no'] = $Unique_no;
            $data['is_from_api'] = '1';
            
            if($user)
            {   
                $data['updated_date'] = date('Y-m-d H:i:s');
                $this->db->where('Email',$Email);
                $this->db->update('user',$data);
                $cnt=$this->App_Login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                if($cnt == 0)
                {
                    $this->App_Login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                }
                $data = array(
                    'success' => true,
                    'message' => 'User Info updated'
                );
            }
            else
            {
                $data['Organisor_id'] = $org_id;
                $data['Created_date'] = date('Y-m-d H:i:s');
                $this->db->insert('user',$data);
                $user_id=$this->db->insert_id();
                $this->App_Login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data = array(
                    'success' => true,
                    'message' => 'User Info added'
                );
             }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Paramenters'
            );
        }
        echo json_encode($data);exit;
    }

    public function update_firstname()
    {
        $json = file_get_contents('http://fmv.cc/aitl_test.php');

        $user = $this->db->where('`Id` IN (3950,70402,70403,70573,70574,70575,70576,70577,70578,70579,70580,70581,70582,70583,70584,70585,70586,70587,70588,70589,70590,70591,70592,70593,70594,70595,70596,70597,70598,70599,70600,70704,72735,73550)')->get('user_tmp')->result_array();


        // $user = json_decode($json,true);
        
        foreach ($user as $key => $value)
        {
            $update['Firstname'] = $value['Firstname'];
            $update['Lastname'] = $value['Lastname'];
            $update['Company_name'] = $value['Company_name'];
            $update['Title'] = $value['Title'];
            $where['Id'] = $value['Id'];
            $this->db->where($where);
            $this->db->update('user',$update);
            unset($update);
        }
        echo "data updates successfully";
    }
    public function add_users_agenda_grey()
    {
        /*$users_agenda = [];
        foreach ($users_agenda as $key => $value)
        {
            if(!empty($value[0]) && !empty($value[1]))
            {
                $user_id = $this->db->where('Email',$value[0])->get('user')->row_array()['Id'];
                $agenda_id = $this->db->where('agenda_code',$value[1])->get('agenda')->row_array()['Id'];
                if(!empty($user_id) && !empty($agenda_id))
                {
                    $agenda_data = $this->db->where('user_id',$user_id)->get('users_agenda')->row_array();
                    if(!empty($agenda_data))
                    {   

                        if(!in_array($agenda_id,explode(',',$agenda_data['check_in_agenda_id'])) && !in_array($agenda_id,explode(',',$agenda_data['pending_agenda_id'])))
                        {
                            $agenda_data['agenda_id'] = explode(',',$agenda_data['agenda_id']);
                            $agenda_data['agenda_id'][] = $agenda_id;
                            $update['agenda_id'] = implode(',',array_unique(array_filter($agenda_data['agenda_id'])));
                            $where['user_id'] = $user_id;
                            $this->db->where($where)->update('users_agenda',$update);
                        }
                    }
                    else
                    {
                        $insert['user_id'] = $user_id;
                        $insert['agenda_id'] = $agenda_id;
                        $this->db->insert('users_agenda',$insert);
                    }
                }
            }
        }
        j('Users Agenda Assign Successfully');*/
    }
    public function add_medlab_attendee_new()
    {
        $Company_name = $this->input->post('Company_name');
        $Firstname = $this->input->post('Firstname');
        $Lastname = $this->input->post('Lastname');
        $Email = $this->input->post('Email');
        $Street = $this->input->post('Street');
        $Suburb = $this->input->post('Suburb');
        $State = $this->input->post('State');
        $Country = $this->input->post('Country');
        $Postcode = $this->input->post('Postcode');
        $Mobile = $this->input->post('Mobile');
        $Phone_business = $this->input->post('Phone_business');
        $Salutation = $this->input->post('Salutation');
        $Title = $this->input->post('Title');
        $Unique_no = $this->input->post('Unique_no');
        $barcode_list = $this->input->post('barcode_list');
        $api_user_id = $this->input->post('api_user_id');
        $api_pass = $this->input->post('api_pass');

        if($api_user_id == 'medlab' && $api_pass == '$3dR^2Od8X86')
        {   
            if(!empty($Unique_no))
            {   
                /*if(!empty($Email))
                {
                    if(strpos($Email, '@') === false)
                    {
                        $data = array(
                            'success' => false,
                            'message' => 'Email should be valid'
                        );
                        echo json_encode($data);exit;   
                    }
                }*/
                   
                $check_unique = $this->db->where('Unique_no',$Unique_no)->get('user')->row_array();
                if($check_unique)
                {
                    $data = array(
                        'success' => false,
                        'message' => 'Unique no is already exists'
                    );
                    echo json_encode($data);exit;
                }

                $this->load->model('Native_single_fcm/App_Login_model');
                $event_id = 1012;
                $org_id=$this->App_Login_model->getOrganizerByEvent($event_id);
                //$user = $this->db->where('Email',$Email)->get('user')->row_array();

                $data['Company_name'] = $Company_name;
                $data['Firstname'] = $Firstname;
                $data['Lastname'] = $Lastname;
                $data['Email'] = $Email;
                $data['Street'] = $Street;
                $data['Suburb'] = $Suburb;
                $data['State'] = $State;
                $country_id = $this->db->where('country_name',$Country)->get('country')->row_array();
                $data['Country'] = ($country_id['id'])?:$Country;
                $data['Postcode'] = $Postcode;
                $data['Mobile'] = $Mobile;
                $data['Phone_business'] = $Phone_business;
                $data['Salutation'] = $Salutation;
                $data['Title'] = $Title;
                $data['Unique_no'] = $Unique_no;
                $data['barcodes'] = $barcode_list;
                $data['is_from_api'] = '1';
                if($user)
                {   
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $this->db->where('Email',$Email);
                    $this->db->update('user',$data);
                    $cnt=$this->App_Login_model->checkEmailAlreadyByEvent1($Email,$event_id);
                    if($cnt == 0)
                    {
                        $this->App_Login_model->setupUserWithEvent($user['Id'],$event_id,$org_id);
                    }
                    $data = array(
                        'success' => true,
                        'message' => 'User Info updated'
                    );
                }
                else
                {
                    $data['Organisor_id'] = $org_id;
                    $data['Created_date'] = date('Y-m-d H:i:s');
                    $this->db->insert('user',$data);
                    $user_id=$this->db->insert_id();
                    $this->App_Login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                    $data = array(
                        'success' => true,
                        'message' => 'User Info added'
                    );
                 }
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => 'Invalid Paramenters'
                );
            }
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'API Credentials Not Valid'
                );
            echo json_encode($data);exit; 
        }
        echo json_encode($data);exit;
    }



    public function add_temp_offshore_pipeline()
    {
        // die("Direct Access Denied..");
        $fulldata = file_get_contents('https://prd-eventdata-informakn.eu.cloudhub.io/api/event/FKT3394/?client_id=5b8beb939eb9439cbc5d68302e83a90b&client_secret=32036ac1da164abb993db2aBaC15E87F');
        $alltempdata = json_decode($fulldata, true);
    
        //$this->Allapi_model->add_temp_KNECT365_exibitor($alltempdata['organisationCategories']);
        //$this->Allapi_model->add_temp_KNECT365_speakers($alltempdata['speakers']);
        //$this->Allapi_model->add_temp_KNECT365_session($alltempdata['agenda']);
        echo "SuccessFully.....";
        die;
    }
    public function add_offshore_pipeline()
    {
        // die("Direct Access Denied..");
        //$this->Allapi_model->add_KNECT365_exibitor(1235);
        //$this->Allapi_model->add_KNECT365_speakers(1235);
        $this->Allapi_model->add_KNECT365_session(1235, 1130);
        echo "SuccessFully.....";
        die;
    }
}
