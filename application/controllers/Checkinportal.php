<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkinportal extends CI_Controller
{
    function __construct()
    {
    	$this->data['pagetitle'] = 'Check In Portal';
        $this->data['smalltitle'] = 'Check In Portal';
        $this->data['breadcrumb'] = 'Check In Portal';
        parent::__construct($this->data);
        $this->load->library('formloader');
        $this->load->library('formloader1');
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Cms_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Message_model');
        $this->load->model('Notes_admin_model');
        $this->load->model('Profile_model');
        $this->load->model('Agenda_model');
        $this->load->model('Attendee_model');
        $user = $this->session->userdata('current_user');
    }
    public function index($acc_name=NULL,$Subdomain=NULL)
    {
    	$event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;

        $notificationsetting=$this->Setting_model->getnotificationsetting($event_templates[0]['Id']);
        $this->data['notisetting']=$notificationsetting;

        $res1=$this->Event_template_model->get_singup_forms($event_templates[0]['Id']);
      	$this->data['sign_form_data']=$res1;  

	    $fb_login_data = $this->Event_model->getraisedsetting($event_templates[0]['Id']);
	    $this->data['fb_login_data'] = $fb_login_data;

        $advertisement_images = $this->Event_template_model->get_advertising_images($Subdomain);
        $this->data['advertisement_images'] = $advertisement_images;

        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
        $this->data['attendees'] = $attendees;

        $this->data['keysdata']=$this->Attendee_model->get_all_custom_modules($event_templates[0]['Id']);
        $this->data['menu'] = $menu;

        $menu_list = $this->Event_template_model->geteventmenu_list($event_templates[0]['Id'], null, null);
        $this->data['menu_list'] = $menu_list;

        $notifiy_msg = $this->Message_model->msg_notify($event_templates[0]['Id']);
        $this->data['notify_msg'] = $notifiy_msg;

        $cmsmenu = $this->Cms_model->get_cms_page($event_templates[0]['Id']);
        $this->data['cms_menu'] = $cmsmenu;

        $notes_list = $this->Event_template_model->get_notes($Subdomain);
        $this->data['notes_list'] = $notes_list;
        $this->data['Subdomain'] = $Subdomain;
        
        $this->template->write_view('css', 'frontend_files/css', $this->data, true);
        $this->template->write_view('header', 'frontend_files/header', $this->data, true);
        $this->template->write_view('js', 'frontend_files/js', $this->data, true);
        $this->template->write_view('content', 'Attendee/checkinportalview', $this->data, true);
        $this->template->write_view('sidebar', 'frontend_files/sidebar', $this->data, true);
        $this->template->render();
    }
    public function user_check_in($acc_name=NULL,$Subdomain=NULL,$aid=NULL)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->Attendee_model->attendee_check_in_save($event_templates[0]['Id'],$aid);
        $checkin=$this->Attendee_model->attendee_check_in_get($event_templates[0]['Id'],$aid);
        if($checkin[0]['check_in_attendee']=='1')
        {
            echo "success###btn btn-success btn-block###Check Out";
        }
        else
        {
            echo "success###btn btn-green btn-block###Check In";
        }
        die;
    }
    public function get_attendee_user_info($acc_name=NULL,$Subdomain=NULL,$uid=NULL)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $id=$event_templates[0]['Id'];
        $user_info=$this->Attendee_model->get_user_info($uid);
        $extra_info=$this->Attendee_model->get_extra_info_user($id,$uid,'1');
        $keysdata=$this->Attendee_model->get_all_custom_modules($id);
        $einfo=json_decode($extra_info[0]['extra_column'],TRUE);
        $send_array=array();
        for($j=0;$j<count($keysdata);$j++){
           $kid=$keysdata[$j]['column_id'];     
           $keynm=$keysdata[$j]['column_name'];
           $send_array[$kid]=$einfo[$keynm];
        }
        foreach ($user_info as $key => $value) {
            if(!empty($value['Logo']))
            {
                $send_array['user_logo']=base_url().'assets/user_files/'.$value['Logo'];
            }
            else
            {
                $send_array['user_logo']=base_url().'assets/images/anonymous.jpg';   
            }
            $send_array['Firstname']=$value['Firstname'];
            $send_array['Lastname']=$value['Lastname'];
            $send_array['company_name']=$value['Company_name'];
        }
        echo json_encode($send_array);die;
    }
    public function Save_Attendee_informaion($acc_name=NULL,$Subdomain=NULL)
    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $id=$event_templates[0]['Id'];
        $keysdata=$this->Attendee_model->get_all_custom_modules($id);
        $post_arr=$this->input->post();
        $uid=$post_arr['user_id'];
        unset($post_arr['Firstname']);
        unset($post_arr['user_id']);
        unset($post_arr['Lastname']);
        unset($post_arr['attendee_company_name']);
        $key_name=array_column_1($keysdata,'column_name');
        $extra_array=json_encode(array_filter(array_combine(array_values($key_name),array_values($post_arr))));
        $this->Attendee_model->update_extra_information_by_user($id,$uid,$extra_array,'1');
        $user=array();
        if ($_FILES['attendee_profile_photos']['name'] != NULL && $_FILES['attendee_profile_photos']['name'] != '')
        {
            $imgname = explode('.', $_FILES['attendee_profile_photos']['name']);
            $tempname = str_replace(array(' ','%','+','&','-'),array('_','_','_','_','_'),$imgname[0]);
           $tempname = $tempname.'_'. strtotime(date("Y-m-d H:i:s"));
           $_FILES['attendee_profile_photos']['name'] = $tempname . "." . $imgname[1];
           $config['file_name']=$_FILES['attendee_profile_photos']['name'];
           $config['upload_path'] = './assets/user_files';
           $config['allowed_types'] = 'gif|jpg|jpeg|png';
           $config['max_size'] = '100000';
           $config['max_width'] = '2000';
           $config['max_height'] = '2000';
           $this->load->library('upload', $config);
           if ($this->upload->do_upload('attendee_profile_photos')){
            $user['Logo']=$_FILES['attendee_profile_photos']['name'];
           } 
        }
        $user['Company_name']=$this->input->post('attendee_company_name');
        $user['Firstname']=$this->input->post('Firstname');
        $user['Lastname']=$this->input->post('Lastname');
        $user['Lastname']=$this->input->post('Lastname');
        $this->Attendee_model->save_attendee_user_information($uid,$user);
        redirect(base_url().'Checkinportal/'.$acc_name.'/'.$Subdomain);
    }
}
?>