<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('native_single_fcm_v2/Notification_model');
		$this->load->model('Native/Settings_model');
		include('application/libraries/NativeGcm.php');
        include('application/libraries/FcmV2.php');
        show_errors();
	}

	public function getGeoNoti()
	{	
		extract($this->input->post());
		if(!empty($event_id) && !empty($user_id))
		{	
			if($user_id != '387491')
			$data['noti'] = $this->Notification_model->getGeoNoti($event_id,$user_id);
			else
				$data['noti'] = [];
			
			$data = array(
				'success' => true,
				'data' => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid parameters'
			);
		}
		echo json_encode($data);
	}

	public function updateUserNoti()
	{
		extract(array_map('trim',$this->input->post()));
		if(!empty($event_id) && !empty($user_id) && !empty($noti_id) && !empty($gcm) && !empty($device))
		{
			$this->Notification_model->updateUserNoti($user_id,$noti_id);
			$notification = $this->Notification_model->getNoti($noti_id);
			if(!empty($notification['moduleslink']))
            {
                $extra['message_type'] = 'cms';
                $extra['message_id'] = $notification['moduleslink'];
            }
            elseif(!empty($notification['custommoduleslink']))
            {
                $extra['message_type'] = 'custom_page';
                $extra['message_id'] = $notification['custommoduleslink'];
            }
            $extra['event'] = $this->Settings_model->event_name($event_id);
            $extra['title'] =  $notification['title'];

            $user = $this->db->select('*')->from('user')->where('Id',$user_id)->get()->row_array();
            if($user['version_code_id'] == '')
            {

                if($device == "iphone")
                {	
                	$obj1 = new Fcm($event_id);
                    $msg1 =  $notification['content'];
                    $result[] = $obj1->send(1,$gcm,$msg1,$extra,$device,$event_id);
                }
                else
                {   
                    $obj = new Gcm($event_id);
                    $msg['title'] = $notification['title'];
                    $msg['message'] = $notification['content'];
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result[] = $obj->send_notification($gcm,$msg,$extra,$device);
                } 
            }
            else
            {
                include 'application/libraries/Gcmr.php';
                $obj            = new Gcmr($event_id);
                $msg['title'] = $notification['title'];
                $msg['message'] = $notification['content'];
                $msg['vibrate'] = 1;
                $msg['sound'] = 1;
                $result[] = $obj->send_notification($gcm,$msg,$extra,$device);
            }

			$data = array(
				'success' => true,
				'message' => 'Notification Updated Successfully'
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => 'Invalid parameters'
			);

		}
		echo json_encode($data);
	}
	public function updateLogs()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $notification_id = $this->input->post('notification_id');
        $flag = $this->input->post('flag');
        

        if($user_id!='' && $event_id!='' && $notification_id!='' && $flag!='')
        {
            $where['user_id'] = $user_id;
            $where['event_id'] = $event_id;
            $where['notification_id'] = $notification_id;
            switch ($flag) {
                case '1': // received time
                    $update['received_time'] = date('Y-m-d H:i:s');
                    break;
                case '2': // open time
                    $logs = $this->db->select('*')->from('notification_logs')->where($where)->get()->row_array();
                    if($logs['received_time'] == '' || $logs['received_time'] == 'NULL' )
                    {
                        $update['received_time'] = date('Y-m-d H:i:s');
                    }
                    $update['open_time'] = date('Y-m-d H:i:s');
                    break;
            }
            $this->Notification_model->updateNotificationLog($update,$where);
            $data = array(
                'success' => true,
                'message' => 'Notification Updated Successfully'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters'
            );
        }
        echo json_encode($data);
    }
}