<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Survey extends CI_Controller

{
    public $menu;

    public $cmsmenu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Survay_model');
        $this->load->model('native_single_fcm_v2/Gamification_model');
        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id') , null, null, $this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach($this->cmsmenu as $key => $values)
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function get_survey()

    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $survey_screens = $this->Survay_model->get_survey_screens($event_id);
                $survey = $this->Survay_model->get_survey($event_id, $user_id);
                if (empty($survey))
                {
                    $survey = array();
                }
                if (empty($survey_screens))
                {
                    $survey_screens = array();
                }
                $data = array(
                    'survey' => $survey,
                    'survey_screens' => $survey_screens,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function saveSurvey()

    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $survey_json_data = $this->input->post('survey_json_data');
        $survey_data = json_decode($survey_json_data, true);
        $category_id = $this->input->post('category_id');
        for ($i = 0; $i < sizeof($survey_data); $i++)
        {
            $survey_data[$i]['answer_date'] = date('Y-m-d H:i:s');
        }
        if ($event_id != '' && $token != '' && $user_id != '' && !empty($survey_data))
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                if ($event_id == '259' && date('Y-m-d') < "2017-11-22")
                {
                    $status = '1';
                }
                else
                {
                    $status = $this->Survay_model->saveSurvey($survey_data);
                    $this->add_user_game_point($user_id, $event_id, 6);
                }
                if (!empty($category_id))
                {
                    $survey_screens = $this->Survay_model->get_survey_screens($event_id, $category_id);
                    $survey = $this->Survay_model->get_category_wise_survey($event_id, $user_id, $category_id);
                    if (empty($survey))
                    {
                        $survey = array();
                    }
                    if (empty($survey_screens))
                    {
                        $survey_screens = array();
                    }
                    $data = array(
                        'survey' => $survey,
                        'survey_screens' => $survey_screens,
                    );
                }
                if ($status == 1)
                {
                    $data = array(
                        'success' => true,
                        'data' => $data,
                        'message' => "Successfully saved"
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something went wrong Please try again"
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_survey_category()

    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        if ($event_id != '' && $event_type != '')
        {
            /*$user=$this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if(empty($user))
            {
            $data = array(
            'success' => false,
            'data' => array(
            'msg' => 'Please check token or event.'
            )
            );
            }
            else
            {*/
            $survey = $this->Survay_model->get_survey_category($event_id);
            if ($event_id == '1404')
            {
                $zero = $survey[0];
                $one = $survey[1];
                $survey[1] = $zero;
                $survey[0] = $one;
            }
            if (empty($survey))
            {
                $survey = array();
            }
            $data = array(
                'survey' => $survey,
                'message' => (!empty($survey)) ? "" : "There are no available surveys right now.",
                'is_blank' => (!empty($survey)) ? 0 : 1,
            );
            $data = array(
                'success' => true,
                'data' => $data
            );
            // }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_category_wise_survey()

    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        $lang_id = $this->input->post('lang_id');
        if ($event_id != '' && $event_type != '' && $category_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $survey_screens = $this->Survay_model->get_survey_screens($event_id, $category_id);
                $survey = $this->Survay_model->get_category_wise_survey($event_id, $user_id, $category_id, $lang_id);
                $msg = $this->Survay_model->is_survey_avilable($event_id, $category_id);
                if (empty($survey))
                {
                    $survey = array();
                }
                if (empty($survey_screens))
                {
                    $survey_screens = array();
                }
                else
                {
                    $survey_screens[0]['welcome_data'] = html_entity_decode($survey_screens[0]['welcome_data']);
                    $survey_screens[0]['thanku_data'] = html_entity_decode($survey_screens[0]['thanku_data']);
                }
                $data = array(
                    'survey' => $survey,
                    'survey_screens' => $survey_screens,
                    'message' => ($msg['state'] == 1) ? $msg['msg'] : "",
                    'is_blank' => $msg['state'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function add_user_game_point($user_id=NULL, $event_id=NULL, $rank_id=NULL)

    {
        $this->Gamification_model->add_user_point($user_id, $event_id, $rank_id);
        return true;
    }
    public function check_survey_status()

    {
        $survey_id = $this->input->post('survey_id');
        $event_id = $this->input->post('event_id');
        if ($survey_id != '')
        {
            $survey_id = explode('-', $survey_id);
            $survey = $this->Survay_model->check_survey_status($survey_id[1], $event_id);
            $data = array(
                'success' => true,
                'is_survey_avilable' => $survey ? '1' : '0',
                'survey_id' => $survey_id[1],
                'message' => 'No survey available for this QR code.',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}