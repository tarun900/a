<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Native_single_fcm extends CI_Controller

{
  function __construct()
  {
    parent::__construct();
    $this->load->model('native_single_fcm_v2/native_single_fcm_model');
    $this->load->model('native_single_fcm_v2/App_login_model');
    $this->load->model('native_single_fcm_v2/Event_model');
    $this->load->model('native_single_fcm_v2/Settings_model');
  }
  public function getGlidewellDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getGlidewellDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getApexPublicEvents()

  {
    $gcm_id = $this->input->post('gcm_id');
    $publicevents = $this->Event_model->getApexPublicEvents($gcm_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function ApexSearchEvent()

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->ApexSearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function getNHSDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getNHSDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkNHSVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'NHS';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getSIALMEDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getSIALMEDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkSIALMEVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'SIALME';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getCABSATDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getCABSATDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkCABSATVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'CABSAT';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getComicConPublicEvents()

  {
    $gcm_id = $this->input->post('gcm_id');
    $publicevents = $this->Event_model->getComicConPublicEvents($gcm_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkComicConVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'ComicCon';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function ComicconSearchEvent()

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->ComicconSearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function getONTDefaultEvent() //comic con

  {
    $event = $this->native_single_fcm_model->getONTDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getWPBDefaultEvent() //comic con

  {
    $event = $this->native_single_fcm_model->getWPBDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getGamesforumDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getGamesforumEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function KnectSearchEvent()

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->KnectSearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function getArabHelthDefaultEvent() //7-12-2017 10:02

  {
    $event = $this->native_single_fcm_model->getArabHelthDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkArabHelthVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'ArabHelth';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getFHADefaultEvent() //15-12-2017 11:27

  {
    $event = $this->native_single_fcm_model->getFHADefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        $event[$key]['isDataDownload'] = '1'; //quick solution for ios due to rejection from app store
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkFHAVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'FHA2018';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getBondsAndLoansMasterEvent()

  {
    $event = $this->native_single_fcm_model->getBondsAndLoansMasterEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getBondsAndLoansDefaultEvent() //18-12-2017 12:12

  {
    $event = $this->native_single_fcm_model->getBondsAndLoansDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkBondsAndLoansVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'BondsAndLoans';
      $device = strtolower($device);
      // $code = $this->Settings_model->getVersionCode($where,$device);
      $code = $this->Settings_model->getVersionCodeV2($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
        'version_id' => $code['Id']
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getBondsAndLoansPublicEvents()

  {
    extract($this->input->post());
    $publicevents = $this->Event_model->getBondsAndLoansPublicEvents($gcm_id, $user_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents,
        'URL' => 'http://allintheloop.com/privacy-policy.html'
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function getConcreteExpoDefaultEvent() //20-12-2017 15:43

  {
    $event = $this->native_single_fcm_model->getConcreteExpoDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkConcreteExpoVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'ConcreteExpo';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getConnect2bDefaultEvent() //21-12-2017 11:41

  {
    $event = $this->native_single_fcm_model->getConnect2bDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkConnect2bVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Connect2b';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getMjBakerDefaultEvent() //26-12-2017 10:18

  {
    $event = $this->native_single_fcm_model->getMjBakerDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkMjBakerVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'mjbaker';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getAvangridDefaultEvent() //26-12-2017 04:42

  {
    $event = $this->native_single_fcm_model->getAvangridDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkAvangridVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'avangrid';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getMedlabDefaultEvent() //28-12-2017 10:02

  {
    $event = $this->native_single_fcm_model->getMedlabDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        $event[$key]['default_lang']['1__provide_feedback'] = 'Ask a Question';
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkMedlabVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Medlab';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getCoburnsPublicEvents()

  {
    $gcm_id = $this->input->post('gcm_id');
    $publicevents = $this->Event_model->getCoburnsPublicEvents($gcm_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function CoburnsSearchEvent()

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->CoburnsSearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function checkCoburnsVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Coburns';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getGreyDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getGreyDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkGreyVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Grey';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getLTLondonDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getLTLondonDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkLTLondonVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'LTLondon';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getLTFranceDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getLTFranceDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkLTFranceVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'LTFrance';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getDerivConeDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getDerivConeDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkDerivConVersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'DerivCon';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getFixIncome2018DefaultEvent()

  {
    $event = $this->native_single_fcm_model->getFixIncome2018DefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkFixIncome2018VersionCode()

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'FixIncome2018';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getIMEAPublicEvents() //Wednesday 10 January 2018 10:26:55 AM IST

  {
    $gcm_id = $this->input->post('gcm_id');
    $publicevents = $this->Event_model->getIMEAPublicEvents($gcm_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function IMEASearchEvent() //Wednesday 10 January 2018 10:27:00 AM IST

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->IMEASearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function checkIMEAVersionCode() //Wednesday 10 January 2018 10:27:08 AM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Orange';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getUCASPublicEvents() //Thursday 11 January 2018 10:22:08 AM IST

  {
    $gcm_id = $this->input->post('gcm_id');
    $publicevents = $this->Event_model->getUCASPublicEvents($gcm_id);
    if (!empty($publicevents))
    {
      foreach($publicevents as $key => $value)
      {
        $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
      }
      $data = array(
        'success' => true,
        'message' => "successfully",
        'data' => $publicevents
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Public Events found"
      );
    }
    echo json_encode($data);
  }
  public function UCASSearchEvent() //Thursday 11 January 2018 10:22:12 AM IST

  {
    $event_name = $this->input->post('event_name');
    if ($event_name != '')
    {
      $events = $this->native_single_fcm_model->UCASSearchEvent($event_name);
      if (!empty($events))
      {
        $data = array(
          'success' => true,
          'data' => $events
        );
      }
      else
      {
        $data = array(
          'success' => false,
          'message' => "No app found."
        );
      }
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parametrs"
      );
    }
    echo json_encode($data);
  }
  public function checkUCASVersionCode() //Thursday 11 January 2018 10:22:17 AM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'UCAS';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getAICDMemberDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getAICDMemberDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getAICDShow2018DefaultEvent()

  {
    $event = $this->native_single_fcm_model->getAICDShow2018DefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkAICDVersionCode() //Thursday 11 January 2018 10:22:17 AM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'AICD';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getErsteTalentDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getErsteTalentDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkErsteTalentVersionCode() //Thursday 11 January 2018 10:22:17 AM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'ErsteTalent';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getEuromedicomAMWCDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getEuromedicomAMWCDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getEuromedicomFACEDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getEuromedicomFACEDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getEuromedicomAMECLiveVISAGEDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getEuromedicomAMECLiveVISAGEDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkEuromedicomVersionCode() //Thursday 11 January 2018 10:22:17 AM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Euromedicom';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getArlarDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getArlarDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkArlarVersionCode() //Thursday 17 January 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'Arlar';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getIPExpoDefaultEvent()

  {
    $event = $this->native_single_fcm_model->getIPExpoDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        if ($event[$key]['default_lang']['lang_id'] == '651')
        {
          $event[$key]['default_lang']['1__check_in'] = 'Request Slides';
        }
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkIPExpoVersionCode() //Thursday 17 January 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'IPExpo';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getUCExpoDefaultEvent() //Thursday 18 January 2018 10:09:57 AM IST

  {
    $event = $this->native_single_fcm_model->getUCExpoDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        if ($event[$key]['default_lang']['lang_id'] == '772')
        {
          $event[$key]['default_lang']['1__check_in'] = 'Request Slides';
          $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Theatre';
        }
      }
      $data = array(
        'success' => true,
        'data' => $event,
        'URL' => 'http://allintheloop.com/privacy-policy.html'
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkUCExpoVersionCode() //Thursday 17 January 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'UCExpo';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getCityScapeAbuDhabiDefaultEvent() //Thursday 18 January 2018 10:09:57 AM IST

  {
    $event = $this->native_single_fcm_model->getCityScapeAbuDhabiDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
        if ($event[$key]['default_lang']['lang_id'] == '766')
        {
          $event[$key]['default_lang']['1__check_in'] = 'Request Slides';
          $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Theatre';
        }
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkCityScapeAbuDhabiVersionCode() //Thursday 17 January 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'CityScapeAbuDhabi';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getAEI2018DefaultEvent() //Thursday 08 February 2018 04:28:48 PM IST

  {
    $event = $this->native_single_fcm_model->getAEI2018DefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkAEI2018VersionCode() //Thursday 08 February 2018 04:34:35 PM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'AEInstitute2018';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getBBGADefaultEvent() //Thursday 23 February 2018 12:28:48 PM IST

  {
    $event = $this->native_single_fcm_model->getBBGADefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkBBGAVersionCode() //Thursday 23 February 2018 12:29:35 PM IST

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'BBGA';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getBathAndWestDefaultEvent() //Thursday 23 February 2018

  {
    $event = $this->native_single_fcm_model->getBathAndWestDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkBathAndWestVersionCode() //Thursday 23 February 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'BathAndWest';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getTSAMLondonDefaultEvent() //Thursday 06 March 2018

  {
    $event = $this->native_single_fcm_model->getTSAMLondonDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getTSAMTorontoDefaultEvent() //Thursday 06 March 2018

  {
    $event = $this->native_single_fcm_model->getTSAMTorontoDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getTSAMNewYorkDefaultEvent() //Thursday 06 March 2018

  {
    $event = $this->native_single_fcm_model->getTSAMNewYorkDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getTSAMHongKongDefaultEvent() //Thursday 06 March 2018

  {
    $event = $this->native_single_fcm_model->getTSAMHongKongDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function getTSAMBostonDefaultEvent() //Thursday 06 March 2018

  {
    $event = $this->native_single_fcm_model->getTSAMBostonDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkTSAMVersionCode() //Thursday 06 March 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'TSAM';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getAhicDefaultEvent() //Friday 16 March 2018

  {
    $event = $this->native_single_fcm_model->getAhicDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkAhicVersionCode() //Friday 16 March 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'AHIC';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
  public function getAviationMediaDefaultEvent() //Tuesday 20 March 2018

  {
    $event = $this->native_single_fcm_model->getAviationMediaDefaultEvent();
    if (!empty($event))
    {
      foreach($event as $key => $value)
      {
        $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
        $event[$key]['Event_name'] = ucfirst($value['Event_name']);
        $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
      }
      $data = array(
        'success' => true,
        'data' => $event
      );
    }
    else
    {
      $data = array(
        'success' => true,
        'message' => "No Events found"
      );
    }
    echo json_encode($data);
  }
  public function checkAviationMediaVersionCode() //Tuesday 20 March 2018

  {
    $device = $this->input->post('device');
    if ($device != '')
    {
      $where['app'] = 'AviationMedia';
      $device = strtolower($device);
      $code = $this->Settings_model->getVersionCode($where, $device);
      $data = array(
        'success' => true,
        'code' => ($code) ? $code : '',
      );
    }
    else
    {
      $data = array(
        'success' => false,
        'message' => "Invalid parameters",
      );
    }
    echo json_encode($data);
  }
}