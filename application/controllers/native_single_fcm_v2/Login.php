<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('Setting_model');
    }

    /***To Check Login Old**/

    public function check() 
    {
        $email=$this->input->post('email');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
   
        if($email!='' && $password!='' && $event_id!='')
        {
            $user = $this->App_login_model->check_app_login($email,$password,$event_id);
            // echo "<pre>"; print_r($user); exit();
            if($user == 'inactive')
            {
                $data = array(
                   'success' => false,
                   'message' => "You have entered an incorrect username or password."
                );  
            }
            elseif($user == 'inactive1')
            {
                $data = array(
                   'success' => false,
                   'message' => "You have entered an incorrect username or password."
                );
            }
            else if(empty($user)) 
            {

                $data = array(
                   'success' => false,
                   'message' => "You have entered an incorrect username or password."
                );   
            } 
            else 
            {
                $user_id=$user[0]->Id;
                if(!file_exists("./assets/user_files/thumbnail/".$user[0]->Logo))
                    copy("./assets/user_files/".$user[0]->Logo,"./assets/user_files/thumbnail/".$user[0]->Logo);
                $token=$user[0]->token;
                $data1['Login_date'] = date('Y-m-d H:i:s');
                $this->App_login_model->update_token($user_id,$data1);
                /*if(empty($token))
                {*/
                     #pen
                     $data['token'] = sha1($email.$password.$event_id.rand());
                     $data['version_code_id'] = $this->input->post('version_code_id');
                     $this->App_login_model->update_token($user_id,$data);
                     $user[0]->token=$data['token'];
                     #pen
                //}
                if(!empty($user[0]->Country))
                {
                    $user[0]->country_name=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $user[0]->country_name="";
                }
                if(!empty($user[0]->State))
                {
                    $user[0]->state_name=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $user[0]->state_name="";
                }
                $formbullder_data=$this->App_login_model->get_additionalFormData($user_id);
                if(!empty($formbullder_data))
                {
                    $data1=json_decode($formbullder_data[0]['json_data']);
                    $i=0;
                    foreach ($data1 as $key => $value) 
                    {
                        $arr[$i]["key"]=$key;
                        $arr[$i]["value"]=$value;
                        $i++;     
                    }
                }
                else
                {
                    $data1='';
                }
                if(strpos($user[0]->Logo, 'http')!== false) 
                {
                    $user[0]->is_logo_url = '1';
                }
                else
                {
                    $user[0]->is_logo_url = '0';
                }
                $user[0]->is_assigned_master = $this->App_login_model->getAssignedNewsEvent($event_id,$user[0]->Id);
                $user[0]->goal = $this->App_login_model->getAttendeeGoal($event_id,$user[0]->Id);
                $data = array(
                  'success' => true,
                  'data' => $user[0],
                  'extra_info'=>(!empty($arr)) ? $arr : '',
                );
            }
        }   
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***To Forgot Password Old**/

    public function forgot_password() 
    {   
        $is_valid_email = $this->App_login_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Invalid Email Address'
               )
            );   
        } 
        else 
        {
            $new_pass = $this->generate_password();
            $role = $this->App_login_model->getUserRole($this->input->post('email'),$this->input->post('event_id'));
            $slug = "FORGOT_PASSWORD";
            $intEventId = $this->input->post('event_id');
            $em_template = $this->Setting_model->front_email_template($slug,$intEventId);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = html_entity_decode($em_template['Content']);

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = $new_pass;
            $msg = preg_replace($patterns, $replacements, $msg);
            $new_pass_array['email'] = $this->input->post('email');
            if($intEventId == '447' && ($role == 6 || $role == 4))
            {
                $this->load->library('RC4');
                $rc4_obj = new RC4();
                $password = $rc4_obj->encrypt($new_pass);
                $this->App_login_model->change_pas($this->input->post('email'),$password);
            }
            else
            {
                $this->App_login_model->change_pas($this->input->post('email'),md5($new_pass));
            }
            if($em_template['Subject']=="")
            {
               $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = 'xHi$&h9M)x9m';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from('invite@allintheloop.com', 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            // $this->email->to('jagdish@xhtmljunkies.com');
            $this->email->subject($subject);//'User Account'
            $this->email->set_mailtype("html");
            $this->email->message($msg);    
            $this->email->send();

            $data = array(
                'success' => true,
                'data' => array(
                    'msg' => 'Your new password has been sent to your email address.'
               )
            );
        }
        echo json_encode($data);
    }

    public function generate_password()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 9; $i++) 
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /***To Facebook Login**/

    public function fb_signup()
    {
        $email=$this->input->post('email');
        $facebook_id=$this->input->post('facebook_id');
        $firstname=$this->input->post('first_name');
        $event_id=$this->input->post('event_id');
        $img=$this->input->post('img');
        $device=$this->input->post('device_name');

        $logo="facebook_logo".strtotime(date("Y-m-d H:i:s")).".jpeg";
        copy($img,"./assets/user_files/".$logo);
        copy($img,"./assets/user_files/thumbnail/".$logo);
        
        if($email!='' && $facebook_id!='' && $event_id!='')
        {
            $cnt=$this->App_login_model->checkEmailAlreadyByEvent($email,$event_id);
            $data=$this->App_login_model->fb_signup($email,$facebook_id,$event_id,$firstname,$logo,$device);
            $formbuilder = [];
            if($data['status'] == "1")
            {
                $formbuilder = $this->App_login_model->getFormBuillderDataByEvent($event_id);
                
                $formbuilder = json_decode($formbuilder[0]['json_data'],true);
            }
            #pen
            $update_data['token'] = sha1($email.$event_id.rand());
            $update_data['version_code_id'] = $this->input->post('version_code_id');
            $this->App_login_model->update_token($data['data']['Id'],$update_data);
            $data['data']['token']=$update_data['token'];
            $data['data']['password']="";
            $data['data']['is_assigned_master'] = $this->App_login_model->getAssignedNewsEvent($event_id,$data['data']['Id']);
            $data['data']['goal'] = $this->App_login_model->getAttendeeGoal($event_id,$data['data']['Id']);

            #pen
            $data = array(
              'success' => true,
              'data' => $data['data'],
              'status' =>$data['status'], 
              'formbuilder_data' => $formbuilder,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***To give all conutries in sign up**/

    public function getAllCountryList()
    {

        $data=$this->App_login_model->getAllCountryList();
        if(!empty($data))
        {
            $data = array(
                'success' => true,
                'data' => $data,
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "No country available",
            );
        }
        echo json_encode($data);
    }

     /***Registration sign up Old**/

    public function registrationByEvent()
    {
        $email=$this->input->post('email');
        $firstname=$this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
        $country_id=$this->input->post('country');
        $company_name=$this->input->post('cmpy_name');
        $title=$this->input->post('title');
        $device=$this->input->post('device_name');
        $formbiluder_status=$this->input->post('formbiluder_status');

        if($email!='' && $firstname!='' && $event_id!='' && $last_name!=''  && $password!='' && $formbiluder_status!='')
        {
            $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($email,$event_id);
            if($cnt==0)
            {
                $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
                $user_id=$this->App_login_model->signup($email,$firstname,$last_name,$password,$country_id,$title,$company_name,$event_id,$org_id,$device);
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data=$this->App_login_model->getUserDetailsId($user_id);
                if($formbiluder_status==1)
                {
                    $form_data=$this->input->post('form_data');
                    $this->App_login_model->addFormBuilderData($user_id,$form_data);
                }
                
                $data = array(
                  'success' => true,
                  'data' => $data,
                );
               
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "This email is already associated with this event",
                );
            }
            

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get data from form builder from profile for extra fields from users**/

    public function getFormBuillderDataByEvent()
    {
        $event_id=$this->input->post('event_id');
        if($event_id!='')
        {
            $formbullder=$this->App_login_model->getFormBuillderDataByEvent($event_id);
            if(!empty($formbullder))
            {
                $default_lang=$this->App_login_model->get_default_lang_label_for_signup($event_id,$lang_id);
                $formbullder_arr=json_decode($formbullder[0]['json_data'],true);
                foreach ($formbullder_arr['fields'] as $key => $value)
                {   
                    $tmp = $default_lang['sign_up_process_form__'.strtolower(str_replace(" ","_",$value['title']))];
                    $formbullder_arr['fields'][$key]['lang_title'] = ($tmp) ? $tmp : "";

                    if(!empty($value['choices']))
                    {
                        foreach ($value['choices'] as $k => $v)
                        {
                            $tmp = $default_lang['sign_up_process_form__'.strtolower(str_replace(" ","_",$v['title']))];
                            $formbullder_arr['fields'][$key]['choices'][$k]['lang_title'] = ($tmp) ? $tmp : "";
                        }
                    }
                } 
                $data = array(
                  'success' => true,
                  'status' => 1,
                  'data' => $formbullder_arr,
                  'event_name' => $this->App_login_model->getEventName($event_id),
                );
            }
            else
            {
                $data = array(
                  'success' => true,
                  'status' => 0,
                  'message' => "No Additional Form data",
                  'event_name' => $this->App_login_model->getEventName($event_id),
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Update GCM id when user logout and again login**/

    public function updateUserGCMId()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $token=$this->input->post('token');
        $gcm_id=$this->input->post('gcm_id');
        $device=$this->input->post('device');
        $bundleId = $this->input->post('bundleId');

        if($event_id!='' && $user_id!='' && $token!='' && $gcm_id!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {   
                $is_aitl = ($bundleId == 'com.AllInTheLoop' || $bundleId == 'com.allintheloop') ? '1' : '0';
                $user_data=$this->App_login_model->updateGCMID($user_id,$gcm_id,$device,$event_id);
                $data = array(
                    'success' => true,
                    'data' => $user_data,
                    'message'=> "Successfully updated"
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Logout user old**/

    public function logout()
    {
        $user_id = $this->input->post('user_id');
        if($user_id!='')
        {
            $this->App_login_model->updateGCMID($user_id,'');
            $data = array(
              'success' => true,
              'message' => "You are now logged out"
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***To login with Linkedin**/

    public function linkedInSignup()
    {
        $email          = $this->input->post('email');
        $firstname      = $this->input->post('first_name');
        $last_name      = $this->input->post('last_name');
        $img            = $this->input->post('img');
        $device         = $this->input->post('device_name');
        $title          = $this->input->post('title');
        $company_name   = $this->input->post('company_name');
        $event_id       = $this->input->post('event_id');

        if($img!='')
        {
            $logo="linkedin_logo".strtotime(date("Y-m-d H:i:s")).".jpeg";
            copy($img,"./assets/user_files/".$logo);
            copy($img,"./assets/user_files/thumbnail/".$logo);
        }
        if($email!='' && $event_id!='')
        {
            $company_title = explode('at', $company_name);
            $data=$this->App_login_model->linkedInSignup($email,$facebook_id,$event_id,$firstname,$last_name,$logo,$device,($company_title[1]) ? $company_title[1] : '',($company_title[0]) ? $company_title[0] : '');
            $user_data = $data['data'];
            if(!empty($user_data['Country']))
            {
                $user_data['country_name']=$this->App_login_model->getCountry($user_data['Country']);
            }
            else
            {
                $user_data['country_name']="";
            }
            if(!empty($user_data['State']))
            {
                $user_data['state_name']=$this->App_login_model->getState($user_data['State']);
            }
            else
            {
                $user_data['state_name']="";
            }
            $formbuilder_data = [];
            if($data['status'] == "1")
            {
                $formbuilder_data = $this->App_login_model->getFormBuillderDataByEvent($event_id);
                $formbuilder = json_decode($formbuilder_data[0]['json_data']);
            }
            #pen
            $update_data['token'] = sha1($email.$event_id.rand());
            $update_data['version_code_id'] = $this->input->post('version_code_id');
            $this->App_login_model->update_token($user_data['Id'],$update_data);
            $user_data['token']=$update_data['token'];
            $user_data['is_assigned_master'] = $this->App_login_model->getAssignedNewsEvent($event_id,$user_data['Id']);
            $user_data['goal'] = $this->App_login_model->getAttendeeGoal($event_id,$user_data['Id']);

            #pen
            $data = array(
              'success' => true,
              'data' => $user_data,
              'status' => $data['status'],
              'formbuilder_data' => $formbuilder,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Save data from form builder from profile for extra fields from users**/

    public function saveFormBuilderData()
    {
        $form_data=$this->input->post('form_data');
        $user_id=$this->input->post('user_id');
        if($user_id != '' && $form_data!='')
        {
            $this->App_login_model->addFormBuilderData($user_id,$form_data);
            $data = array(
              'success' => true,
              'message' => "Profile Saved",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Login check for Authorized users**/

    public function authorizedLogin() 
    {
        $email=$this->input->post('email');
        $event_id=$this->input->post('event_id');
        if($email!=''  && $event_id!='')
        {   
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            if($org_id == '20152')
            {   
                $email = strtolower($email);
                $domain = explode('@', $email)[1];
                $allowed_domain = $this->App_login_model->CheckDomain($event_id);
                if(!in_array($domain,$allowed_domain))
                {
                   $data = array(
                      'success' => true,
                      'data' => new stdClass(),
                      'login_status'=>false,
                      'status'=>'',
                      'formbuilder_data' =>[], 
                      'message' => (!$data['status']) ? "Your email domain is not allowed." : '',
                    );
                    echo json_encode($data);exit(); 
                }
            }

            $data = $this->App_login_model->checkAuthorizedLogin($event_id,$email);
            $formbullder_data=$this->App_login_model->getFormBuillderDataByEvent($event_id);
            $data1 = [];
            if(!empty($formbullder_data))
            {
                $data1=json_decode($formbullder_data[0]['json_data']);
                $data1 = ($data1) ? $data1 : [];
            }

            if(is_array($data['data']))
            {
                $data['data']['is_assigned_master'] = $this->App_login_model->getAssignedNewsEvent($event_id,$data['data']['Id']);
                $data['data']['goal'] = $this->App_login_model->getAttendeeGoal($event_id,$data['data']['Id']);
            }    

            $data = array(
              'success' => true,
              'data' => $data['data'],
              'login_status'=>$data['status'],
              'status'=>($data1) ? $data['register'] : '0',
              'formbuilder_data' => ($data['register']== '1') ? $data1 : [], 
              'message' => (!$data['status']) ? (isset($data['rep_user']))?"You have entered an incorrect username or password." : "Your email is not registered in authorized email." : '',
            );
        }   
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***To delete user from back button**/

    public function delete_user()
    {
        $user_id = $this->input->post('user_id');
        $role_id = $this->input->post('role_id');

        if(!empty($user_id))
        {   
            if($role_id == '4')
            {
                //$this->App_login_model->delete_user($user_id);
            }
            $data = array(
                'success' => true,
                'message' => 'User Deleted'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid Parameters"
            );
        }
        echo json_encode($data);
    }

    /***Registration sign up Latest**/

    public function registrationByEvent_v2()
    {

        $email=$this->input->post('email');
        $firstname=$this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
        $country_id=$this->input->post('country');
        $company_name=$this->input->post('cmpy_name');
        $title=$this->input->post('title');
        $device=$this->input->post('device_name');
        $formbiluder_status=$this->input->post('formbiluder_status');

        if($email!='' && $firstname!='' && $event_id!='' && $last_name!=''  && $password!='' && $formbiluder_status!='')
        {
            $cnt=$this->App_login_model->checkEmailAlreadyByEvent1($email,$event_id);
            if($cnt==0)
            {
                $org_id=$this->App_login_model->getOrganizerByEvent($event_id);
                $user_id=$this->App_login_model->signup_v2($email,$firstname,$last_name,$password,$country_id,$title,$company_name,$event_id,$org_id,$device);
                $this->App_login_model->setupUserWithEvent($user_id,$event_id,$org_id);
                $data=$this->App_login_model->getUserDetailsId($user_id);
                if($formbiluder_status==1)
                {
                    $form_data=$this->input->post('form_data');
                    $this->App_login_model->addFormBuilderData($user_id,$form_data);
                }
                
                $data = array(
                  'success' => true,
                  'data' => $data,
                );
               
            }
            else
            {
                $data = array(
                  'success' => false,
                  'message' => "This email is already associated with this event",
                );
            }
            

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Check Login Latest**/

    public function check_v2() 
    {
        
        $email=$this->input->post('email');
        $password=$this->input->post('password');
        $event_id=$this->input->post('event_id');
        if($email!='' && $password!='' && $event_id!='')
        {    
            $user = $this->App_login_model->check_app_login_v2($email,$password,$event_id);
            $current_date = date('Y-m-d');
            $user1 = $this->App_login_model->getUser($email);
            $attempts = $this->App_login_model->getUserAttempts($user1['Id'],'login_attempts');
            $event = $this->App_login_model->getEventData($event_id);
            $max_attempts = (int) ($event[0]['no_of_login_attempts']-1);

            if(!empty($attempts) && $user == 'inactive')
            {
                $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                $current_date = date('Y-m-d');
                if( $compare_date == $current_date && $attempts['value'] > $max_attempts)
                {
                   
                   $data = array(
                       'success' => false,
                       'message' => "Your Account has been locked for today. Please try to login by tommorow."
                    ); 
                   echo json_encode($data);exit;
                }
                else
                {   
                    $data = array(
                       'success' => false,
                       'message' => ($attempts['value'] == $max_attempts) ? "This is your last attempt if it is not correct then this time your account will be locked out for 24 hours." : "You have entered in an incorrect username or password."
                    ); 
                    $this->App_login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
                    echo json_encode($data);exit;
                } 
                
            }
            else if(!empty($user1) && empty($attempts) && $user == 'inactive')
            {
               
                $this->App_login_model->insertUserAttempts($user1['Id'],'login_attempts');
                $data = array(
                   'success' => false,
                   'message' => "You have entered in an incorrect username or password."
                ); 
            }
            else if($user == 'inactive')
            {
                
                $data = array(
                   'success' => false,
                   'message' => "You have entered in an incorrect username or password."
                );  
            }
            elseif($user == 'inactive1')
            {
                
                $data = array(
                   'success' => false,
                   'message' => "You have entered an incorrect username or password."
                );
            }
            else if(empty($user)) 
            {
                
                $data = array(
                   'success' => false,
                   'message' => "You have entered in an incorrect username or password."
                );   
            } 
            elseif(!empty($attempts) && ((date('Y-m-d',strtotime($attempts['created_date']))) == $current_date) && $attempts['value'] > 2)
            {
               
                $data = array(
                       'success' => false,
                       'message' => "Your account is locked for 24 hours because of too many wrong attempts."
                    ); 
                   echo json_encode($data);exit;
            }
            else 
            {   
                if(!empty($attempts))
                {
                    $this->App_login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],0);
                }
                $user_id=$user[0]->Id;
                if(!file_exists("./assets/user_files/thumbnail/".$user[0]->Logo))
                    copy("./assets/user_files/".$user[0]->Logo,"./assets/user_files/thumbnail/".$user[0]->Logo);
                $token=$user[0]->token;
                $data1['Login_date'] = date('Y-m-d H:i:s');
                $this->App_login_model->update_token($user_id,$data1);
                /*if(empty($token))
                {*/
                     #pen
                     $data['token'] = sha1($email.$password.$event_id.rand());
                     $data['version_code_id'] = $this->input->post('version_code_id');
                     $this->App_login_model->update_token($user_id,$data);
                     $user[0]->token=$data['token'];
                     #pen
                //}
                if(!empty($user[0]->Country))
                {
                    $user[0]->country_name=$this->App_login_model->getCountry($data['Country']);
                }
                else
                {
                    $user[0]->country_name="";
                }
                if(!empty($user[0]->State))
                {
                    $user[0]->state_name=$this->App_login_model->getState($data['State']);
                }
                else
                {
                    $user[0]->state_name="";
                }
                $formbullder_data=$this->App_login_model->get_additionalFormData($user_id);
                if(!empty($formbullder_data))
                {
                    $data1=json_decode($formbullder_data[0]['json_data']);
                    $i=0;
                    foreach ($data1 as $key => $value) 
                    {
                        $arr[$i]["key"]=$key;
                        $arr[$i]["value"]=$value;
                        $i++;     
                    }
                }
                else
                {
                    $data1='';
                }
                if(strpos($user[0]->Logo, 'http')!== false) 
                {
                    $user[0]->is_logo_url = '1';
                }
                else
                {
                    $user[0]->is_logo_url = '0';
                }
                $user[0]->Password = '';
                $data = array(
                  'success' => true,
                  'data' => $user[0],
                  'extra_info'=>(!empty($arr)) ? $arr : '',
                );
            }
        }   
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Forgot Password Latest**/

    public function forgot_password_v2() 
    {
        $is_valid_email = $this->App_login_model->check_user_email($this->input->post());
        if (empty($is_valid_email)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Invalid Email Address'
               )
            );   
        } 
        else 
        {
            $attempts = $this->App_login_model->getUserAttempts($is_valid_email[0]->Id,'forgot_password');
            if(!empty($attempts))
            {
                $compare_date = date('Y-m-d',strtotime($attempts['created_date']));
                $current_date = date('Y-m-d');
                if( $compare_date == $current_date && $attempts['value'] > 2)
                {
                   $data = array(
                        'success' => true,
                        'data' => array(
                            'msg' => 'You have tried 3 times forgot password. Now you can make forgot password request by tommorow.'
                       )
                    );
                    echo json_encode($data);exit;
                }
            }
            else
            {
                $this->App_login_model->insertUserAttempts($is_valid_email[0]->Id,'forgot_password');
            }
            $event = $this->App_login_model->getEventData($this->input->post('event_id'));

            $acc=$this->App_login_model->getAccname($this->input->post('event_id')); 
            $acc_name=$acc[0]['acc_name'];
            $new_pass = $this->generate_password();
            $role = $this->App_login_model->getUserRole($this->input->post('email'),$this->input->post('event_id'));
            $slug = "FORGOT_PASSWORD";
            $intEventId = $this->input->post('event_id');
            $em_template = $this->setting_model->front_email_template($slug,$intEventId);
            $em_template = $em_template[0];
            $name = ($is_valid_email[0]->Firstname == NULL) ? $is_valid_email[0]->Company_name : $is_valid_email[0]->Firstname;

            $msg = html_entity_decode($em_template['Content']);

            $patterns = array();
            $patterns[0] = '/{{name}}/';
            $patterns[1] = '/{{password}}/';
            $replacements = array();
            $replacements[0] = $name;
            $replacements[1] = "<a href='".base_url()."app/reset_password/".$acc_name."/".$event[0]['Subdomain']."/".$new_pass. "'>Click here to change your password.</a> ";
            $msg = preg_replace($patterns, $replacements, $msg);
            $new_pass_array['email'] = $this->input->post('email');
            $this->App_login_model->insertUserAttempts($is_valid_email[0]->Id,'reset_password',$new_pass);
            /*if($intEventId == '447' && ($role == 6 || $role == 4))
            {
                $this->load->library('RC4');
                $rc4_obj = new RC4();
                $password = $rc4_obj->encrypt($new_pass);
                $this->App_login_model->change_pas($this->input->post('email'),$password);
            }
            else
            {
                $this->App_login_model->change_pas($this->input->post('email'),md5($new_pass));
            }*/
            if($em_template['Subject']=="")
            {
               $subject="FORGOT_PASSWORD";
            }
            else
            {
                $subject=$em_template['Subject'];
            }
            
            $strFrom= EVENT_EMAIL;
            if($em_template['From']!="")
            {
                $strFrom = $em_template['From'];
            }
            $config['protocol']   = 'smtp';
            $config['smtp_host']  = 'localhost';
            $config['smtp_port']  = '25';
            $config['smtp_user']  = 'invite@allintheloop.com';
            $config['smtp_pass']  = 'xHi$&h9M)x9m';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from('invite@allintheloop.com', 'AllInTheLoop');
            $this->email->to($is_valid_email[0]->Email);
            // $this->email->to('jagdish@xhtmljunkies.com');
            $this->email->subject($subject);//'User Account'
            $this->email->set_mailtype("html");
            $this->email->message($msg);    
            $this->email->send();
            if(!empty($attempts))
                $this->App_login_model->updateUserAttempts($attempts['id'],$attempts['created_date'],++$attempts['value']);
            $data = array(
                'success' => true,
                'data' => array(
                    'msg' => 'We have sent password reset link to you email address.'
               )
            );
        }
        echo json_encode($data);
    }

   
}
