<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Product extends CI_Controller
{
    public function __construct()
    {
        //  error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Note_model');
        $this->load->model('native_single_fcm_v2/product_model');
        date_default_timezone_set("UTC");
        include 'application/libraries/NativeGcm.php';
        include 'application/libraries/FcmV2.php';
        $this->load->model('native_single_fcm_v2/Event_template_model');

        $this->events       = $this->Event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode   = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images']      = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) {
            if (!empty($values['Images'])) {
                $cmsbannerimage_decode         = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            } else {
                $this->cmsmenu[$key]['Images'] = "";
            }
            if (!empty($values['Logo_images'])) {
                $cmslogoimage_decode                = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            } else {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }
        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null, $this->input->post('user_id'));
    }
    public function product_listing()
    {
        $event_id     = $this->input->post('event_id');
        $event_type   = $this->input->post('event_type');
        $token        = $this->input->post('token');
        $user_id      = $this->input->post('user_id');
        $auction_type = $this->input->post('auction_type');
        $page_no      = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $auction_type != '' && $page_no != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $products_data = $this->product_model->getProductsByAuctionType($event_id, $auction_type, $page_no);

                if (empty($products_data)) {
                    $products_data['products']   = array();
                    $products_data['total_page'] = 0;
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => $products_data['products'],
                    'total_page'           => $products_data['total_page'],
                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getAuctionWiseProductId()
    {
        $event_id     = $this->input->post('event_id');
        $event_type   = $this->input->post('event_type');
        $token        = $this->input->post('token');
        $auction_type = $this->input->post('auction_type');

        if ($event_id != '' && $event_type != '' && $auction_type != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $products_data = $this->product_model->getProductIdByAuctionType($event_id, $auction_type);
                $data1         = array(
                    'product_id_arr' => $products_data,
                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function slient_products_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($event_id != '' && $event_type != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);

                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }
                }
                $products_data = $this->product_model->getSlientAuctionProductsDetails($product_id, $event_id);
                $cdate         = date('Y-m-d H:i:s');
                $start         = $products_data[0]['auctionStartDateTime'];
                $end           = $products_data[0]['auctionEndDateTime'];
                if (!empty($event_list[0]['Event_show_time_zone'])) {
                    if (strpos($event_list[0]['Event_show_time_zone'], "-") == true) {
                        $arr       = explode("-", $event_list[0]['Event_show_time_zone']);
                        $intoffset = $arr[1] * 3600;
                        $intNew    = abs($intoffset);
                        $cdate     = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - $intNew);
                    }
                    if (strpos($event_list[0]['Event_show_time_zone'], "+") == true) {
                        $arr       = explode("+", $event_list[0]['Event_show_time_zone']);
                        $intoffset = $arr[1] * 3600;
                        $intNew    = abs($intoffset);
                        $cdate     = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) + $intNew);
                    }
                }
                if ($cdate >= $start && $cdate <= $end && $end != '' && $start != '') {
                    $auction_start = '0';
                } elseif ($cdate < $start && $start != '') {
                    $auction_start = '1';
                } else {
                    $auction_start = '';
                }

                $lastbid       = $this->product_model->get_last_bid($product_id);
                $lates_bid_amt = $lastbid[0]['bider'];
                $name          = $lastbid[0]['Firstname'] . ' ' . $lastbid[0]['Lastname'];
                $total_bids    = $this->product_model->count_total_bid($product_id);
                $winner_name   = $this->product_model->check_winner($product_id);
                $auction_arr   = array("lastbid" => ($lates_bid_amt) ? $lates_bid_amt : "", "total_bids" => $total_bids, "name" => $name, "winner_name" => $winner_name);
                $arrAcc        = $this->Event_model->getAccname($event_id);
                $Subdomain     = $this->Event_model->get_subdomain($event_id);
                $url           = base_url() . 'fundraising/' . $arrAcc[0]['acc_name'] . '/' . $Subdomain;
                $productsData  = array();
                foreach ($products_data as $key => $value) {
                    $value['current_date_time'] = ($cdate) ? $cdate : '';
                    $img                        = base_url() . 'fundraising/' . $value['image_arr'][0];
                    $url .= '/product/details/' . $value['product_id'] . '/1';
                    $fb_url                    = "https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=" . $url . "&title=" . urlencode($value['name']) . "&picture=" . $img . "&display=popup&ref=plugin&src=share_button";
                    $twitter_url               = "http://twitter.com/home?status=Support " . ucfirst($arrAcc[0]['acc_name']) . " and checkout their fundraising app!  @allintheloop fundraising " . $url;
                    $value['facebook_url']     = $fb_url;
                    $value['twitter_url']      = $twitter_url;
                    $value['is_auction_start'] = $auction_start;
                    $value['description']      = html_entity_decode($value['description']);
                    $productsData[]            = $value;
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => $productsData,
                    'bid_data'             => $auction_arr,
                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function live_products_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($event_id != '' && $event_type != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    $latest_pleadge_bids = array();
                }

                $products_data = $this->product_model->getLiveAuctionProductsDetails($product_id, $event_id);
                $lastbid       = $this->product_model->get_last_bid($product_id);
                $lates_bid_amt = (count($lastbid[0]['bider'])) ? $lastbid[0]['bider'] : "";
                $name          = $lastbid[0]['Firstname'] . ' ' . $lastbid[0]['Lastname'];
                $total_bids    = $this->product_model->count_total_bid($product_id);
                $winner_name   = $this->product_model->check_winner($product_id);
                $auction_arr   = array("lastbid" => $lates_bid_amt, "total_bids" => $total_bids, "name" => $name, "winner_name" => $winner_name);

                $arrAcc       = $this->Event_model->getAccname($event_id);
                $Subdomain    = $this->Event_model->get_subdomain($event_id);
                $url          = base_url() . 'fundraising/' . $arrAcc[0]['acc_name'] . '/' . $Subdomain;
                $productsData = array();
                foreach ($products_data as $key => $value) {
                    $img = base_url() . 'fundraising/' . $value['image_arr'][0];
                    $url .= '/product/details/' . $value['product_id'] . '/1';
                    $fb_url                = "https://www.facebook.com/sharer/sharer.php?app_id=998839983505890&sdk=joey&u=" . $url . "&title=" . urlencode($value['name']) . "&picture=" . $img . "&display=popup&ref=plugin&src=share_button";
                    $twitter_url           = "http://twitter.com/home?status=Support " . ucfirst($arrAcc[0]['acc_name']) . " and checkout their fundraising app!  @allintheloop fundraising " . $url;
                    $value['facebook_url'] = $fb_url;
                    $value['twitter_url']  = $twitter_url;
                    $value['description']  = html_entity_decode($value['description']);
                    $productsData[]        = $value;
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => $productsData,
                    'bid_data'             => $auction_arr,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function online_shop_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($event_id != '' && $event_type != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);

                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $products_data                   = $this->product_model->getOnlineShopDetails($product_id, $event_id);
                $products_data[0]['description'] = html_entity_decode($products_data[0]['description']);
                $event_list                      = $this->get_event($event_id, $user_id);
                $data1                           = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'currency'             => $currency,
                    'products'             => ($products_data) ? $products_data : [],

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function donation_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($event_id != '' && $event_type != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);

                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $products_data                         = $this->product_model->getDonationDetails($product_id, $event_id);
                $products_data[0]['description']       = html_entity_decode($products_data[0]['description']);
                $products_data[0]['short_description'] = html_entity_decode($products_data[0]['short_description']);
                $data1                                 = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'currency'             => $currency,
                    'products'             => ($products_data) ? $products_data : [],

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function cart_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($event_id != '' && $user_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);

                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $products_data = $this->product_model->getCartDetails($event_id, $user_id);
                $total         = 0;
                foreach ($products_data as $key => $value) {
                    $total += $value['subtotal'];
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => ($products_data) ? $products_data : [],
                    'grand_total'          => $total,
                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function add_to_cart()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $quantity   = $this->input->post('quantity');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        //required when auction type = 4
        $pledge_amount       = $this->input->post('pledge_amount');
        $pledge_other_amount = $this->input->post('pledge_other_amount');
        $comment             = $this->input->post('comment');
        $pledge_visibility   = $this->input->post('pledge_visibility');
        if ($event_id != '' && $product_id != '') {
            $user    = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            $product = $this->product_model->getProductById($product_id);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } elseif ($product[0]['price'] == null && $product[0]['auctionType'] != 4) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Product price is null.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $cart_data['product_id'] = $product_id;
                    $cart_data['user_id']    = $user_id;
                    $cart_data['quantity']   = $quantity;
                    $cart_data['time']       = date('Y-m-d h:i:s');

                    if ($product[0]['auctionType'] == 4) {
                        $data['product_id']        = $product_id;
                        $data['user_id']           = $user_id;
                        $data['pledge_amt']        = $pledge_amount;
                        $data['pledge_othr_amt']   = $pledge_other_amount;
                        $data['created_date']      = date('Y-m-d h:i:s');
                        $data['comment']           = $comment;
                        $data['pledge_visibility'] = $pledge_visibility;
                        $pledge_data               = $this->product_model->addToPledgeAmountDonate($data);

                        $cart_data['product_type'] = $product[0]['auctionType'];
                        $cart_data['price_type']   = ($pledge_amount != '') ? "pledge_amt" : "pledge_othr_amt";
                        $cart_data['visibility']   = $pledge_data[0]['Id'];
                        $cart_data['comment']      = $comment;
                        $cart_data['price']        = ($pledge_amount != '') ? $pledge_data[0]['pledge_amt'] : $pledge_data[0]['pledge_othr_amt'];
                        $cart_data['quantity']     = 1;

                    } else {
                        $order_data['user_id']               = $user_id;
                        $order_data['created_date']          = date('Y-m-d h:i:s');
                        $order_transactin_data['product_id'] = $product_id;
                        $order_transactin_data['qty']        = $quantity;
                        $order_transactin_data['price']      = $product[0]['price'];
                        $order_transactin_data['event_id']   = $event_id;
                        $otid                                = $this->product_model->insertOrderTransaction($order_data, $order_transactin_data);
                        $cart_data['price']                  = $product[0]['price'];
                        $cart_data['visibility']             = $otid;
                    }

                    $this->product_model->insertCartDetails($cart_data);
                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $products_data = $this->product_model->getCartDetailsByProductId($product_id, $event_id, $user_id);
                $total         = 0;
                foreach ($products_data as $key => $value) {
                    $total += $value['subtotal'];
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => ($products_data) ? $products_data : [],
                    'grand_total'          => $total,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function save_bid_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        $bid_amt    = $this->input->post('bid_amt');

        if ($event_id != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $bid_data['product_id'] = $product_id;
                    $bid_data['user_id']    = $user_id;
                    $bid_data['bid_amt']    = $bid_amt;
                    $bid_data['date']       = date('Y-m-d h:i:s');
                    $bid_data['win_status'] = 0;
                    $bid_data['event_id']   = $event_id;
                    $this->product_model->insertBidDetails($bid_data);

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $lastbid = $this->product_model->get_last_bid($product_id);
                $outbid  = $this->product_model->get_outbid($product_id);

                $lates_bid_amt = $lastbid[0]['bider'];
                $name          = $lastbid[0]['Firstname'] . ' ' . $lastbid[0]['Lastname'];
                $total_bids    = $this->product_model->count_total_bid($product_id);
                $winner_name   = $this->product_model->check_winner($product_id);
                $auction_arr   = array("lastbid" => $lates_bid_amt, "total_bids" => $total_bids, "name" => $name, "winner_name" => $winner_name);
                $event_list    = $this->get_event($event_id, $user_id);
                $data1         = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'bid_data'             => $auction_arr,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
        $user = $this->product_model->getUserEmail($user_id);
        if ($outbid[0]['Email'] != $user['Email']) {
            $symbol = $this->getCurrencySymbol($currency);
            // outbid mail
            $slug     = "outbid";
            $template = $this->product_model->getTemplate($slug, $event_id);
            $this->load->library('email');
            $config['mailtype'] = 'html';
            $msg                = "Hello " . $outbid[0]['Firstname'] . " " . $outbid[0]['Lastname'];
            $msg .= $template[0]['Content'];
            $patterns[0]     = '/{currency symbol}/';
            $patterns[1]     = '/{Amount}/';
            $patterns[2]     = '/{Item Name}/';
            $replacements[0] = $symbol;
            $replacements[1] = $outbid[0]['bid_amt'];
            $replacements[2] = $lastbid[0]['product_name'];
            $message         = html_entity_decode(preg_replace($patterns, $replacements, $msg));
            $this->email->initialize($config);
            $this->email->from("info@allintheloop.com", 'Allintheloop');
            $this->email->to([$outbid[0]['Email']]); //
            $this->email->subject($template[0]['Subject']);
            $this->email->message($message);
            $result = $this->email->send();
            // outbid mail

            // In the Bid
            $slug     = "Auction Bid";
            $template = $this->product_model->getTemplate($slug, $event_id);

            $config['mailtype'] = 'html';

            $list = array($user['Email']);

            $message = "Hello " . $user['Firstname'] . " " . $user['Lastname'];
            $message .= $template[0]['Content'];
            $patterns[0]     = '/{currency symbol}/';
            $patterns[1]     = '/{Amount}/';
            $patterns[2]     = '/{Item Name}/';
            $replacements[0] = $symbol;
            $replacements[1] = $bid_amt;
            $replacements[2] = $lastbid[0]['product_name'];
            $message         = html_entity_decode(preg_replace($patterns, $replacements, $message));

            $this->email->from("info@allintheloop.com", 'Allintheloop');
            $this->email->subject($template[0]['Subject']);
            $this->email->message($message);
            $this->email->to($list);
            $this->email->send();
            // In the Bid

            // notificaion outbid
            $gcm_id = $outbid[0]['gcm_id'];
            $device = $outbid[0]['device'];

            $template = $this->product_model->getNotificationTemplate($outbid[0]['event_id'], 'Outbid Alert');
            if ($outbid[0]['version_code_id'] == '') {
                if ($device == "Iphone") {
                    $obj                   = new Fcm();
                    $msg                   = $template['Content'];
                    $extra['message_id']   = $outbid[0]['product_id'];
                    $extra['message_type'] = 'Outbid Auction';
                    $extra['event']        = $this->product_model->getEventName($event_id);
                    $extra['title']        = $template['Slug'];
                    $result                = $obj->send(1, $gcm_id, $msg, $extra, $device);
                } else {
                    $obj            = new Gcm($event_id);
                    $msg['title']   = $template['Slug'];
                    $msg['message'] = $template['Content'];
                    $msg['vibrate'] = 1;
                    $msg['sound']   = 1;

                    $extra['message_id']   = $outbid[0]['product_id'];
                    $extra['message_type'] = 'Outbid Auction';
                    $extra['event']        = $this->product_model->getEventName($event_id);

                    $result = $obj->send_notification($gcm_id, $msg, $extra);
                }
            } else {
                include 'application/libraries/Gcmr.php';
                $obj            = new Gcmr($event_id);
                $msg['title']   = $template['Slug'];
                $msg['message'] = $template['Content'];
                $msg['vibrate'] = 1;
                $msg['sound']   = 1;

                $extra['message_id']   = $outbid[0]['product_id'];
                $extra['message_type'] = 'Outbid Auction';
                $extra['event']        = $this->product_model->getEventName($event_id);

                $result = $obj->send_notification($gcm_id, $msg, $extra);
            }
        }
    }
    public function checkout_details()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');

        if ($event_id != '' && $user_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $billing_information = $this->product_model->getBillingInfomration($user_id);
                    $order_details       = $this->product_model->getProductsByUserId($user_id);
                    $payment_methods     = $this->product_model->getstripeshow($event_id);
                    $checkout_content    = $this->product_model->getCheckoutContent($event_id);
                    $payment_info        = $this->product_model->getPaymentInfo($event_id);

                    $total = 0;
                    foreach ($order_details as $value) {
                        $total += $value['price'] * $value['quantity'];
                    }

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'billing_details'      => $billing_information,
                    'payment_method'       => $payment_methods,
                    'order_details'        => $order_details,
                    'grand_total'          => $total,
                    'checkout_content'     => $checkout_content,
                    'payment_info'         => $payment_info,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function update_cart_quantity()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product    = json_decode($this->input->post('product'), true);
        if ($product != '' && $user_id != '' && $event_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $this->product_model->updateCartDetails($product, $user_id);
                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $products_data = $this->product_model->getCartUpdatedDetails($product_id, $user_id);
                $total         = 0;
                foreach ($products_data as $key => $value) {
                    $total += $value['subtotal'];
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => ($products_data) ? $products_data : [],
                    'grand_total'          => $total,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function delete_cart_product()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if ($product_id != '' && $user_id != '') {
            $user    = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            $product = $this->product_model->getProductById($product_id);
            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } elseif ($product[0]['auctionType'] != 3) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => "Product can't be deleted.",
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $this->product_model->deleteCartDetails($product_id, $user_id);
                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $products_data = $this->Event_model->getcartdata($user_id);
                $total         = 0;
                foreach ($products_data as $key => $value) {
                    $total += $value['price'] * $value['quantity'];
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => ($products_data) ? $products_data : [],
                    'grand_total'          => $total,
                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function get_categories()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');

        if ($event_id != '' && $event_type != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $categories = $this->product_model->get_category_list($event_id);
                $currency   = $this->Event_model->getCurrencyByEvent($event_id);
                $data       = array(
                    'success'    => true,
                    'categories' => $categories,
                    'currency'   => $currency,
                );
            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function add_item()
    {
        $event_id       = $this->input->post('event_id');
        $event_type     = $this->input->post('event_type');
        $token          = $this->input->post('token');
        $auction_type   = $this->input->post('auction_type');
        $category       = $this->input->post('category');
        $user_id        = $this->input->post('user_id');
        $title          = $this->input->post('title');
        $description    = $this->input->post('description');
        $features       = $this->input->post('features');
        $listing_status = $this->input->post('listing_status');
        $image          = $this->input->post('image');
        $product_id     = $this->input->post('product_id');
        // silent : auction type = 1
        $start_price            = $this->input->post('start_price');
        $reserve_price          = $this->input->post('reserve_price');
        $reserve_bid_visible    = $this->input->post('reserve_bid_visible');
        $auction_start_datetime = $this->input->post('auction_start_datetime');
        $auction_end_datetime   = $this->input->post('auction_end_datetime');

        // buy now : auction type = 3
        $price    = $this->input->post('price');
        $quantity = $this->input->post('quantity');

        if ($event_id != '' && $event_type != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {

                    $product_data['name']                 = $title;
                    $product_data['short_description']    = $title;
                    $product_data['description']          = $description;
                    $product_data['slug']                 = $title;
                    $product_data['startPrice']           = $start_price;
                    $product_data['reserveBid']           = $reserve_price;
                    $product_data['reserveBidVisible']    = $reserve_bid_visible;
                    $product_data['price']                = $price;
                    $product_data['event_id']             = $event_id;
                    $product_data['category']             = $category;
                    $product_data['auctionType']          = $auction_type;
                    $product_data['auctionStartDateTime'] = $auction_start_datetime;
                    $product_data['auctionEndDateTime']   = $auction_end_datetime;
                    $product_data['status']               = '1';
                    $product_data['userid']               = $user_id;
                    $product_data['created_date']         = date("Y-m-d h:i:s");
                    $product_data['auction_show']         = $listing_status;
                    $product_data['features_product']     = $features;

                    $products_data = $this->product_model->insertProductDetails($product_data, '', ($product_id > 0) ? $product_id : 0, $quantity);

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'product_id'           => ($products_data) ? "$products_data" : $product_id,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function save_image()
    {
        $product_id = $this->input->post('product_id');
        $user_id    = $this->input->post('user_id');
        if ($product_id != '' && $user_id != '') {
            $newImageName = rand() . time() . ".jpeg";
            $target_path  = "././assets/user_files/" . $newImageName;
            $target_path1 = "././assets/user_files/thumbnail/";
            move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
            copy("./assets/user_files/" . $newImageName, "./assets/user_files/thumbnail/" . $newImageName);
            $images = $newImageName;
            $this->product_model->save_image($images, $product_id, $user_id);

            $data = array(
                'success' => true,
                'message' => 'Successfully uploaded',
            );
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function get_item_list()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');

        if ($event_id != '' && $event_type != '' && $user_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $silent_auction_data = $this->product_model->get_item_list($user_id, 1);
                    $buy_now_data        = $this->product_model->get_item_list($user_id, 3);

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category       = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings  = $this->Event_model->getFundraisingSettings($event_id);
                $currency       = $this->Event_model->getCurrencyByEvent($event_id);
                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }
                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'                  => $event_list,
                    'menu_list'               => $this->menu_list,
                    'cmsmenu'                 => $this->cmsmenu,
                    'fundraising_settings'    => $fund_settings,
                    'latest_pleadge_bids'     => $latest_pleadge_bids,
                    'category'                => $category,
                    'currency'                => $currency,
                    'cart_count'              => $cart_count,
                    'note_status'             => $note_status,
                    'silent_auction_products' => $silent_auction_data,
                    'buy_now_products'        => $buy_now_data,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function get_item_details()
    {
        $event_id     = $this->input->post('event_id');
        $event_type   = $this->input->post('event_type');
        $token        = $this->input->post('token');
        $user_id      = $this->input->post('user_id');
        $product_id   = $this->input->post('product_id');
        $auction_type = $this->input->post('auction_type');

        if ($event_id != '' && $event_type != '' && $user_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                $event_list     = $this->App_login_model->check_event_with_id($event_id);
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);
                if (empty($img[0])) {
                    $img[0] = "";
                }
                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }
                $event_list[0]['Images']           = $img[0];
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];
                $note_status                       = 0;
                $cart_count                        = 0;
                if ($user_id != '') {
                    $products = $this->product_model->get_item_details($user_id, $auction_type, $product_id);

                    $note_data  = $this->Note_model->getNotesListByEventId($event_id, $user_id);
                    $cart_count = count($this->Event_model->getcartdata($user_id));
                    if (count($note_data) > 0) {
                        $note_status = 1;
                    } else {
                        $note_status = 0;
                    }

                }
                $category      = $this->Event_model->getCategoryEvent($event_id);
                $fund_settings = $this->Event_model->getFundraisingSettings($event_id);
                $currency      = $this->Event_model->getCurrencyByEvent($event_id);

                $latest_bids    = $this->Event_model->get_latest_total_bids($event_id, 3);
                $latest_pleadge = $this->Event_model->get_latest_pleadges($event_id, 2);
                if (!empty($latest_bids) && !empty($latest_pleadge)) {
                    $latest_pleadge_bids = array_merge($latest_bids, $latest_pleadge);
                } else {
                    if (empty($latest_bids) && empty($latest_pleadge_bids)) {
                        $latest_pleadge_bids = array();
                    } else {
                        if (empty($latest_pleadge)) {
                            $latest_pleadge_bids = $latest_bids;
                        } else {
                            $latest_pleadge_bids = $latest_pleadge;
                        }
                    }

                }
                $event_list = $this->get_event($event_id, $user_id);
                $data1      = array(
                    'events'               => $event_list,
                    'menu_list'            => $this->menu_list,
                    'cmsmenu'              => $this->cmsmenu,
                    'fundraising_settings' => $fund_settings,
                    'category'             => $category,
                    'currency'             => $currency,
                    'cart_count'           => $cart_count,
                    'note_status'          => $note_status,
                    'products'             => ($products) ? $products : $products,
                    'latest_pleadge_bids'  => $latest_pleadge_bids,

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function product_thumbnail()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        $image_name = $this->input->post('image_name');

        if ($event_id != '' && $event_type != '' && $user_id != '' && $product_id != '' && $image_name != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {
                if ($this->product_model->make_thumbnail($user_id, $image_name, $product_id)) {
                    $data = array(
                        'success' => true,
                        'data'    => array(
                            'msg' => 'Thumbnail created successfully.',
                        ),
                    );
                } else {
                    $data = array(
                        'success' => false,
                        'data'    => array(
                            'msg' => 'Something went wrong. Please try again.',
                        ),
                    );
                }

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function delete_item()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');

        if ($event_id != '' && $event_type != '' && $user_id != '' && $product_id != '') {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);

            if (empty($user)) {
                $data = array(
                    'success' => false,
                    'data'    => array(
                        'msg' => 'Please check token or event.',
                    ),
                );
            } else {

                if ($user_id != '') {
                    $this->product_model->delete_item($user_id, $product_id);
                }
                $data1 = array(
                    'message' => 'Item successfully deleted',

                );

                $data = array(
                    'success' => true,
                    'data'    => $data1,
                );

            }
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function confirmOrder()
    {
        $user_id          = $this->input->post('user_id');
        $transaction_id   = $this->input->post('transaction_id'); //optional
        $order_status     = $this->input->post('order_status');
        $mode             = $this->input->post('mode');
        $event_id         = $this->input->post('event_id');
        $is_shipping      = $this->input->post('is_shipping'); // 0/1
        $shipping_address = json_decode($this->input->post('shipping_address'), true); //optional
        $address          = json_decode($this->input->post('address'), true);

        if ($user_id != '' && $order_status != '' && $mode != '' && $is_shipping != '' && $address != '') {
            if (strtolower($mode) == 'cod') {
                $transaction_id = rand();
            }
            $flag        = 0;
            $product_ids = [];
            $products    = $this->product_model->get_product_data($user_id);
            $cart_data   = $this->product_model->getCartDetails($event_id, $user_id);

            foreach ($cart_data as $key => $value) {
                if ($value['auctionType'] == 4) {
                    $flag          = 1;
                    $product_ids[] = $value['product_id'];
                }
                $visibility_ids[] = $value['visibility'];

            }
            $this->product_model->emptyCartValues($user_id);
            if ($flag == 1) {
                $this->product_model->confirm_pledge_order($user_id, $transaction_id, $order_status, $mode, $product_ids);
            } else {
                $this->product_model->confirm_order($user_id, $transaction_id, $order_status, $mode, $visibility_ids);
            }
            $this->product_model->remove_quantity($products);
            $this->product_model->update_address($address[0], $user_id);

            if ($is_shipping) {
                $this->product_model->save_shipping_address($user_id, $shipping_address[0]);
            }
            $data = array(
                'success' => true,
                'message' => "Your order confirmed successfully.",
            );
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function update_checkout_quantity()
    {
        $event_id   = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        $quantity   = $this->input->post('quantity');
        if ($product_id != '' && $user_id != '' && $event_id != '' && $quantity != '') {
            $product[0]['product_id'] = $product_id;
            $product[0]['qty']        = $quantity;
            $this->product_model->updateCartDetails($product, $user_id);
            $order_details = $this->product_model->getProductsByUserId($user_id);
            $total         = 0;
            foreach ($order_details as $value) {
                $total += $value['price'] * $value['quantity'];
            }
            $data = array(
                'success'       => true,
                'order_details' => $order_details,
                'grand_total'   => $total,
            );
        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function authorize_payment()
    {
        $firstName        = urlencode($this->input->post('firstName'));
        $lastname         = urlencode($this->input->post('lastname'));
        $creditCardNumber = urlencode($this->input->post('creditCardNumber'));
        $date             = $this->input->post('date');
        $amount           = urlencode($this->input->post('amount'));
        $organisor_id     = urlencode($this->input->post('organisor_id'));

        if ($organisor_id != '' && $firstName != '' && $lastname != '' && $creditCardNumber != '' && $date != '' && $amount != '') {
            $userdata = $this->product_model->getpaymentoption($organisor_id);
            $LOGINKEY = $userdata[0]['login_key']; // x_login
            $TRANSKEY = $userdata[0]['transaction_key']; //x_tran_key

            $currencyCode = "USD";
            $paymentType  = "Sale";
            $post_values  = array(
                "x_login"           => "$LOGINKEY",
                "x_tran_key"        => "$TRANSKEY",
                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                //"x_market_type"       => "2",
                "x_device_type"     => "1",
                "x_type"            => "AUTH_CAPTURE",
                "x_method"          => "CC",
                "x_card_num"        => $creditCardNumber,
                //"x_exp_date"      => "0115",
                "x_exp_date"        => $date,
                "x_amount"          => $amount,
                //"x_description"       => "Sample Transaction",
                "x_first_name"      => $firstName,
                "x_last_name"       => $lastName,
                //"x_address"         => $address1,
                //"x_state"           => $state,
                "x_response_format" => "1",
                // "x_zip"             => $zip
            );

            $post_string = "";
            foreach ($post_values as $key => $value) {
                $post_string .= "$key=" . urlencode($value) . "&";
            }

            $post_string = rtrim($post_string, "& ");

            $post_url = "https://test.authorize.net/gateway/transact.dll";

            $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request);
            curl_close($request);

            $response_array = explode($post_values["x_delim_char"], $post_response);

            if ($response_array[0] == 2 || $response_array[0] == 3) {
                $data = array(
                    'success'      => false,
                    'order_status' => "fail",
                    'error'        => $response_array[3],
                );

            } else {
                $data = array(
                    'success'        => true,
                    'order_status'   => "completed",
                    'transaction_id' => $response_array[6],
                );
            }

        } else {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function stripePayment()
    {
        //error_reporting(1);

        $fund_percantage  = $this->input->post('fund_percantage');
        $stripe_show      = $this->input->post('stripe_show');
        $total            = $this->input->post('total');
        $organisor_id     = $this->input->post('organisor_id');
        $stripe_user_id   = $this->input->post('stripe_user_id');
        $stripeToken      = $this->input->post('stripeToken');
        $currency         = $this->input->post('currency');
        $admin_secret_key = $this->input->post('admin_secret_key');

        if ($fund_percantage == '' || $stripe_show == '' || $total == '' || $organisor_id == '' || $stripe_user_id == '' || $stripeToken == '' || $currency == '' || $admin_secret_key == '') {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
            echo json_encode($data);
            exit;
        } else {
            require $_SERVER['DOCUMENT_ROOT'] . '/stripe/init.php';
            try
            {
                \Stripe\Stripe::setApiKey($admin_secret_key);
                //\Stripe\Stripe::$apiBase = "https://api-tls12.stripe.com";

                $application_fee  = $fund_percantage;
                $application_fees = $total * $application_fee / 100;
                $user_key         = $stripe_user_id; // Organiser
                $cus_amount       = $total - $application_fee;
                if ($stripe_show == '1') {
                    $payment = \Stripe\Charge::create(
                        array(
                            'amount'          => $total * 100,
                            'currency'        => strtoupper($currency),
                            'source'          => $stripeToken,
                            'description'     => 'All In The Loop',
                            'application_fee' => $application_fees * 100,
                        ),
                        array("stripe_account" => $user_key)
                    );
                } else {
                    $payment = \Stripe\Charge::create(
                        array(
                            "amount"      => $total * 100,
                            "currency"    => strtoupper($currency),
                            "source"      => $stripeToken,
                            "description" => "All In The Loop",
                        ),
                        array("stripe_account" => $user_key)
                    );
                }

                $data1['transaction_id'] = $stripeToken;
                if ($payment['status'] == "succeeded") {
                    $data1['order_status'] = "completed";
                    $data                  = array(
                        'success'        => true,
                        'message'        => "completed",
                        'transaction_id' => $data1['transaction_id'],
                    );
                } else {
                    $data1['order_status'] = $payment['status'];
                    $data                  = array(
                        'success'        => false,
                        'message'        => "failed",
                        'transaction_id' => $data1['order_status'],
                    );
                }
                echo json_encode($data);
            } catch (Exception $e) {
                $data = array(
                    'success' => false,
                    'message' => "Something went wrong.Please try again.",
                    'error'   => $e,
                );

                echo json_encode($data);
            }
        }
    }
    public function getCurrencySymbol($currency=NULL)
    {
        switch ($currency) {
            case 'usd':
                $return = "$";
                break;

            case 'euro':
                $return = "€";
                break;

            case 'gbp':
                $return = "£";
                break;

            default:
                $return = "$";
                break;
        }
        return $return;
    }
    public function get_event($event_id=NULL, $user_id=NULL)
    {
        if ($event_id != '') {
            $event_list = $this->App_login_model->check_event_with_id($event_id, $user_id);
            $user       = $this->App_login_model->getUserDetailsId($user_id);

            if (!empty($event_list)) {
                $menus                                           = explode(',', $event_list[0]['checkbox_values']);
                $tmp                                             = preg_replace('/[[:^print:]]/', '', trim(strip_tags($event_list[0]['Description'])));
                $event_list[0]['Description']                    = empty($tmp) ? "" : $event_list[0]['Description'];
                $event_list[0]['show_lead_retrival_setting_tab'] = $this->Event_model->checkRepresentative($user_id, $event_id);
                $event_list[0]['is_enabled_favorites']           = (in_array('49', $menus)) ? "1" : "0";

                if ($event_list[0]['fun_background_color'] == 0) {
                    $event_list[0]['fun_background_color'] = "#FFFFFF";
                }
                if ($event_list[0]['fun_top_text_color'] == 0) {
                    $event_list[0]['fun_top_text_color1'] = "#FFFFFF";
                } else {
                    $event_list[0]['fun_top_text_color1'] = $event_list[0]['fun_top_text_color'];
                }
                if ($event_list[0]['fun_footer_background_color'] == 0) {
                    $event_list[0]['fun_footer_background_color'] = "#FFFFFF";
                }
                if ($event_list[0]['fun_block_background_color'] == 0) {
                    $event_list[0]['fun_block_background_color'] = "#FFFFFF";
                }
                if ($event_list[0]['fun_top_background_color'] == 0) {
                    $event_list[0]['fun_top_background_color1'] = "#FFFFFF";
                } else {
                    $event_list[0]['fun_top_background_color1'] = $event_list[0]['fun_top_background_color'];
                }
                if ($event_list[0]['theme_color'] == 0) {
                    $event_list[0]['theme_color1'] = "#FFFFFF";
                } else {
                    $event_list[0]['theme_color1'] = $event_list[0]['theme_color'];
                }
                if ($event_list[0]['fun_block_text_color'] == 0) {
                    $event_list[0]['fun_block_text_color'] = "#FFFFFF";
                }

                if ($event_list[0]['fun_footer_background_color'] == 0) {
                    $event_list[0]['fun_footer_background_color'] = "#FFFFFF";
                }
                $img            = json_decode($event_list[0]['Images']);
                $Logo_images    = json_decode($event_list[0]['Logo_images']);
                $Background_img = json_decode($event_list[0]['Background_img']);

                if (empty($Logo_images[0])) {
                    $Logo_images[0] = "";
                }
                if (empty($Background_img[0])) {
                    $Background_img[0] = "";
                }

                $event_list[0]['Images'] = $img[0];

                //$event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                $event_list[0]['banners']          = $img;
                $event_list[0]['Logo_images1']     = $Logo_images[0];
                $event_list[0]['Background_img1']  = $Background_img[0];
                $event_list[0]['description1']     = $event_list[0]['Description'];
                $event_list[0]['Icon_text_color1'] = $event_list[0]['Icon_text_color'];

                $checkbox_values = $event_list[0]['checkbox_values'];
                $menu_array      = explode(',', $checkbox_values);

                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                $event_list[0]['show_notes_icon']        = (in_array('6', $menu_array)) ? 1 : 0;

                $active_module                           = array_column_1($this->menu_list, 'id');
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;

                $event_list[0]['photo_filter_enabled'] = (in_array('54', array_column_1($this->menu_list, 'id'))) ? '1' : '0';
            }
        }
        return $event_list;
    }
}
