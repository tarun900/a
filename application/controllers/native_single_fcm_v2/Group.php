<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('native_single_fcm_v2/Group_model');
		$this->load->model('native_single_fcm_v2/Qa_model');
	}

	/*** Get Group Data  ***/
	public function get_group_data()
	{
		$event_id = $this->input->post('event_id');
		if(!empty($event_id))
		{
			$group = $this->Group_model->get_groups($event_id);
			$super_group = $this->Group_model->get_super_groups($event_id);
			$group_relation = $this->Group_model->group_relation($event_id);
			$super_group_relation = $this->Group_model->super_group_relation($event_id);
			$data = array(
				'group' => $group,
				'group_relation' => $group_relation,
				'super_group' => $super_group,
				'super_group_relation' => $super_group_relation
			);
			$data = array(
				'success' => true,
				'data' => $data
			);
		}
		else
		{
			$data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
		}
		echo json_encode($data);
	}

	/*** Get Menu Group Data  ***/
	public function get_menu_group()
	{
		$event_id = $this->input->post('event_id');
		$menu_id  = $this->input->post('menu_id');
		if(!empty($event_id) && !empty($menu_id))
		{
			$group = $this->Group_model->get_groups($event_id,$menu_id);

			$data = array(
				'group' => $group,
			);

			$data = array(
				'success' => true,
				'data' => $data
			);
		}
		else
		{
			$data = array(
				'success' => false,
				'message' => "Invalid parameters"
				);
		}
		echo json_encode($data);
	}
}
