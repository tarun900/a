<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Speaker extends CI_Controller

{
    public $menu;

    public $cmsmenu;

    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Speaker_model');
        $this->load->model('native_single_fcm_v2/Message_model');
        $this->load->model('native_single_fcm_v2/favorites_model');
        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id') , null, null, $this->input->post('user_id'));
        $this->cmsmenu = $this->Cms_model->get_cms_page($this->input->post('event_id'));
        foreach($this->cmsmenu as $key => $values)
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    /***Get Speaker List ***/

    public function speaker_list()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $lang_id = $this->input->post('lang_id');
        $token = $this->input->post('token');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                //$user_id = '';
                $user_id = $this->Speaker_model->getUserId($token);
                $speakers = $this->Speaker_model->getSpeakersListByEvent($event_id, $user_id, $lang_id);
                $sort_by = $this->Speaker_model->getSpeakersSortByEvent($event_id);
                $this->load->library('image_lib');
                foreach($speakers as $key => $value)
                {
                    $speakers[$key]['Firstname'] =   trim($value['Firstname']);
                    $speakers[$key]['Lastname'] =   trim($value['Lastname']);
                    $new_name = "new_" . $value['Logo'];
                    $extension_pos = strrpos($new_name, '.');
                    $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                    $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/thumb/" . $new_name;
                    if (!empty($value['Logo']) && !file_exists($destination_url))
                    {
                        $source_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/" . $value['Logo'];
                        $info = getimagesize($source_url);
                        $new_name = "new_" . $value['Logo'];
                        // $new_name = $value['Logo'];
                        $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/thumb/" . $new_name;
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $source_url;
                        $config['new_image'] = $destination_url;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = '110';
                        $config['height'] = '110';
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        $extension_pos = strrpos($new_name, '.');
                        $new_name = substr($new_name, 0, $extension_pos) . '_thumb' . substr($new_name, $extension_pos);
                        /*if ($info['mime'] == 'image/jpeg')
                        {
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/gif')
                        {
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/png')
                        {
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);
                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                        }*/
                        if(file_exists($destination_url))
                        {
                             $speakers[$key]['Logo'] = 'thumb/' . $new_name;
                        }
                    }
                    elseif (file_exists($destination_url))
                    {
                        $speakers[$key]['Logo'] = 'thumb/' . $new_name;
                    }
                }
                $data = array(
                    'speaker_list' => $speakers,
                    'sort_by' => $sort_by
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Speaker View ***/

    public function speaker_view()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $speaker_id = $this->input->post('speaker_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $speaker_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $limit = 10;
                $event = $this->Event_model->getEventData($event_id);
                $speaker = $this->Speaker_model->getSpeakerDetails($speaker_id, $event_id, $user_id);
                $message = $this->Message_model->view_private_msg_coversation($user_id, $speaker_id, $event_id, $page_no, $limit);
                $total_pages = $this->Message_model->total_pages($user_id, $speaker_id, $event_id, $limit);
                $agenda = $this->Speaker_model->getConnectedAgenda($speaker_id, $event_id);
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda' => ($agenda) ? $agenda : [],
                    'message' => $message,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'total_pages' => $total_pages,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Send Message ***/

    public function sendMessage()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $speaker_id = $this->input->post('speaker_id');
        $message_type = $this->input->post('message_type');
        $message = $this->input->post('message');
        if ($event_id != '' && $token != '' && $speaker_id != '' && $user_id != '' && $message != '' && $message_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $moderator = $this->Message_model->getModerators($speaker_id, $event_id);
                if (empty($moderator))
                {
                    $message_data['Message'] = $message;
                    $message_data['Sender_id'] = $user_id;
                    $message_data['Event_id'] = $event_id;
                    $message_data['Receiver_id'] = $speaker_id;
                    $message_data['Parent'] = 0;
                    $message_data['image'] = "";
                    $message_data['Time'] = date("Y-m-d H:i:s");
                    $message_data['ispublic'] = $message_type;
                    $message_data['msg_creator_id'] = $user_id;
                    $message_id = $this->Message_model->saveMessage($message_data);
                    if ($message_id != 0)
                    {
                        $current_date = date('Y/m/d');
                        $this->Message_model->add_msg_hit($user_id, $current_date, $speaker_id, $event_id);
                        $data = array(
                            'success' => true,
                            'message' => "Successfully Saved",
                            'message_id' => $message_id
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'message' => "Something is wrong.Please try again"
                        );
                    }
                }
                else
                {
                    foreach($moderator as $key => $value)
                    {
                        $message_data['Message'] = $message;
                        $message_data['Sender_id'] = $user_id;
                        $message_data['Event_id'] = $event_id;
                        $message_data['Receiver_id'] = $value;
                        $message_data['Parent'] = 0;
                        $message_data['image'] = "";
                        $message_data['Time'] = date("Y-m-d H:i:s");
                        $message_data['ispublic'] = $message_type;
                        $message_data['msg_creator_id'] = $user_id;
                        $message_data['org_msg_receiver_id'] = $speaker_id;
                        $message_id[] = $this->Message_model->saveMessage($message_data);
                    }
                    if (!empty($message_id))
                    {
                        $current_date = date('Y/m/d');
                        $this->Message_model->add_msg_hit($user_id, $current_date, $speaker_id, $event_id);
                        $data = array(
                            'success' => true,
                            'message' => "Successfully Saved",
                            'message_id' => $message_id
                        );
                    }
                    else
                    {
                        $data = array(
                            'success' => false,
                            'message' => "Something is wrong.Please try again"
                        );
                    }
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /**Send Image in Message ***/

    public function msg_images_request()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $speaker_id = $this->input->post('speaker_id');
        $message_type = $this->input->post('message_type');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $speaker_id != '' && $user_id != '' && $message_id != '' && $_FILES['image']['name'] != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $message_data1 = $this->Message_model->getMessageDetails($message_id);
                $newImageName = round(microtime(true) * 1000) . ".jpeg";
                $target_path = "././assets/user_files/" . $newImageName;
                $target_path1 = "././assets/user_files/thumbnail/";
                move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
                copy("./assets/user_files/" . $newImageName, "./assets/user_files/thumbnail/" . $newImageName);
                $update_data['image'] = $newImageName;
                if ($message_data1[0]['image'] != '')
                {
                    $arr = json_decode($message_data1[0]['image']);
                    $arr[] = $newImageName;
                    $update_arr['image'] = json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id, $update_arr);
                }
                else
                {
                    $arr[0] = $newImageName;
                    $update_arr['image'] = json_encode($arr);
                    $this->Message_model->updateMessageImage($message_id, $update_arr);
                }
                $a[0] = $newImageName;
                $message_data['Message'] = '';
                $message_data['Sender_id'] = $user_id;
                $message_data['Event_id'] = $event_id;
                $message_data['Receiver_id'] = $speaker_id;
                $message_data['Parent'] = $message_id;
                $message_data['image'] = json_encode($a);
                $message_data['Time'] = date("Y-m-d H:i:s");
                $message_data['ispublic'] = $message_type;
                $message_data['msg_creator_id'] = $user_id;
                $message_id = $this->Message_model->saveMessage($message_data);
                if ($message_id != 0)
                {
                    $current_date = date('Y/m/d');
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something is wrong.Please try again"
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

     /**Delete Message ***/

    public function delete_message()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $user_id != '' && $message_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $this->Message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   

    /**Make Comment ***/

    public function make_comment()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $comment = $this->input->post('comment');
        $user_id = $this->input->post('user_id');
        $message_id = $this->input->post('message_id');
        if ($event_id != '' && $token != '' && $user_id != '' && $message_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                if ($_FILES['image']['name'] != '')
                {
                    $newImageName = round(microtime(true) * 1000) . ".jpeg";
                    $target_path = "././assets/user_files/" . $newImageName;
                    $target_path1 = "././assets/user_files/thumbnail/";
                    move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
                    copy("./assets/user_files/" . $newImageName, "./assets/user_files/thumbnail/" . $newImageName);
                    if ($newImageName != '')
                    {
                        $arr[0] = $newImageName;
                        $comment_arr['image'] = json_encode($arr);
                    }
                }
                $comment_arr['comment'] = $comment;
                $comment_arr['user_id'] = $user_id;
                $comment_arr['msg_id'] = $message_id;
                $comment_arr['Time'] = date("Y-m-d H:i:s");
                $this->Message_model->make_comment($comment_arr, $event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /**Delete Comment ***/

    public function delete_comment()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $comment_id = $this->input->post('comment_id');
        if ($event_id != '' && $token != '' && $comment_id != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, 1);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $this->Message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /**Get Image List ***/

    public function getImageList()
    {
        $event_id = $this->input->post('event_id');
        $token = $this->input->post('token');
        $speaker_id = $this->input->post('speaker_id');
        if ($event_id != '' && $speaker_id != '')
        {
            $images = $this->Message_model->getImages($event_id, $speaker_id);
            if ($images != '')
            {
                $data = array(
                    'images' => json_decode($images) ,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No images found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /**Get Speaker Details ***/

    public function speaker_view_unread_count()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $speaker_id = $this->input->post('speaker_id');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        $page_no = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $speaker_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $limit = 10;
                $event = $this->Event_model->getEventData($event_id);
                $speaker = $this->Speaker_model->getSpeakerDetails($speaker_id, $event_id, $user_id, $lang_id);
                $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id, $user_id, $page_no, $limit, $speaker_id);
                $agenda = $this->Speaker_model->getConnectedAgenda($speaker_id, $event_id, $lang_id);
                $document = $this->Speaker_model->getSpeakerDocument($speaker_id, $event_id, $lang_id);
                $speaker[0]->Speaker_desc =  html_entity_decode($speaker[0]->Speaker_desc);
                /*if(!empty($speaker[0]->Logo))
                {
                    $source_url = $_SERVER['DOCUMENT_ROOT']."assets/user_files/" . $speaker[0]->Logo;
                    $info = getimagesize($source_url);
                    $new_name = "new_" . $speaker[0]->Logo;
                    $destination_url = $_SERVER['DOCUMENT_ROOT'] . "/assets/user_files/" . $new_name;
                    if ($info['mime'] == 'image/jpeg')
                    {
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/png')
                    {
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);
                        $background = imagecolorallocatealpha($image, 255, 0, 255, 127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $speaker[0]->Logo = $new_name;
                }*/
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda' => ($agenda) ? $agenda : [],
                    'document' => $document,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'unread_count' => $unread_count[0]['unread_count'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /**Get Speaker View Latest ***/

    public function speaker_view_new()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $speaker_id = $this->input->post('speaker_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        if ($event_id != '' && $event_type != '' && $speaker_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $limit = 10;
                $event = $this->Event_model->getEventData($event_id);
                $speaker = $this->Speaker_model->getSpeakerDetailsNew($speaker_id, $event_id, $user_id);
                $message = $this->Message_model->view_private_msg_coversation($user_id, $speaker_id, $event_id, $page_no, $limit);
                $total_pages = $this->Message_model->total_pages($user_id, $speaker_id, $event_id, $limit);
                $agenda = $this->Speaker_model->getConnectedAgenda($speaker_id, $event_id);
                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda' => ($agenda) ? $agenda : [],
                    'message' => $message,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'total_pages' => $total_pages,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function get_favorited_speakers()
    {
        $event_id = $this->input->post('event_id');
        $user_id  = $this->input->post('user_id');
        if(!empty($event_id) && !empty($user_id))
        {   

            $data = $this->Speaker_model->get_favorited_speakers($event_id,$user_id);
            $data = array(
                'success' => true,
                'data' => $data
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function speaker_list_offline()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $lang_id = $this->input->post('lang_id');
        $token = $this->input->post('token');
        if ($event_id != '' && $event_type != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                $user_id = $this->Speaker_model->getUserId($token);
                $speakers = $this->Speaker_model->getSpeakerListOffline($event_id, $user_id, $lang_id);

                $data = array(
                    'speaker_list' => $speakers,
                );
                $data = array(
                    'success' => true,
                    'data' => $data
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function speaker_view_unread_countV2()
    {
        $event_id = $this->input->post('event_id');
        $event_type = $this->input->post('event_type');
        $token = $this->input->post('token');
        $speaker_id = $this->input->post('speaker_id');
        $user_id = $this->input->post('user_id');
        $lang_id = $this->input->post('lang_id');
        $page_no = $this->input->post('page_no');
        $add_fav = $this->input->post('add_fav');

        if ($event_id != '' && $event_type != '' && $speaker_id != '' && $page_no != '')
        {
            $user = $this->App_login_model->check_token_with_event($token, $event_id, $event_type);
            if (empty($user))
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );
            }
            else
            {
                if($add_fav == '1' && $user_id)
                {
                    $favorites_data['event_id'] = $event_id;
                    $favorites_data['user_id'] = $user_id;
                    $favorites_data['module_id'] = $speaker_id;
                    $favorites_data['module_type'] = '7';
                    $result = $this->favorites_model->addOrRemoveFavorites($favorites_data);
                }
                $limit = 10;
                $event = $this->Event_model->getEventData($event_id);
                $speaker = $this->Speaker_model->getSpeakerDetails($speaker_id, $event_id, $user_id, $lang_id);
                $unread_count = $this->Message_model->view_exibitor_private_unread_message_list($event_id, $user_id, $page_no, $limit, $speaker_id);
                $agenda = $this->Speaker_model->getConnectedAgenda($speaker_id, $event_id, $lang_id);
                $document = $this->Speaker_model->getSpeakerDocument($speaker_id, $event_id, $lang_id);
                $speaker[0]->Speaker_desc =  html_entity_decode($speaker[0]->Speaker_desc);


                $data = array(
                    'speaker_details' => ($speaker[0]) ? $speaker[0] : new stdClass,
                    'agenda' => ($agenda) ? $agenda : [],
                    'document' => $document,
                    'allow_msg_keypeople_to_attendee' => $event['allow_msg_keypeople_to_attendee'],
                    'unread_count' => $unread_count[0]['unread_count'],
                );
                $data = array(
                    'success' => true,
                    'data' => $data,
                    'message' => ($result || $speaker[0]->is_favorited) ? 'Added successfully' : 'Removed successfully',

                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}