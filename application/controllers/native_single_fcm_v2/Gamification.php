<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Gamification extends CI_Controller     
{
    function __construct() 
    {
        //error_reporting(E_ALL);
        parent::__construct();
        $this->load->model('native_single_fcm_v2/Gamification_model');
        include('application/libraries/NativeGcm.php');
        include('application/libraries/FcmV2.php');

    }

    /***Get LeaderBoard information**/

    public function get_leaderboard()
    {
        $event_id = $this->input->post('event_id');
        if($event_id!='')
        {
            $data = $this->Gamification_model->get_leaderboard($event_id);
            $data['header'] = html_entity_decode($data['header']);
            $data = array(
                  'success'     => true,
                  'data'        => $data,
            );
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid Parameters',
            );
        }
        echo json_encode($data);
    }

    /***Get Users Game Points to Native**/

    public function add_user_game_point($user_id=NULL,$event_id=NULL,$rank_id=NULL)
    {   
        $obj = new Gcm($event_id);
        $obj1 = new Fcm();
        $users = $this->Gamification_model->getAllUsersGCM_id_by_event_id($event_id);
        $count = count($users);
        
        if($count > 100)
        {
            $limit = 100;
            for ($i=0;$i<$count;$i++) 
            {
                $page_no        = $i;

                $start          = ($page_no)*$limit;
                $users1         = array_slice($users,$start,$limit);
                foreach ($users1 as $key => $value) 
                {
                    if($value['gcm_id']!='')
                    {
                        $msg =  '';
                        $extra['message_type'] = 'gamification';
                        $extra['message_id'] = '';
                        $extra['event'] = $event[0]['Event_name'];
                        if($value['version_code_id'] == '')
                        {
                            if($value['device'] == "Iphone")
                            {
                                $result[] = $obj1->send(1,$value['gcm_id'],$msg,$extra,$value['device'],$event_id);
                            }
                            else
                            {
                                $msg['title'] = '';
                                $msg['message'] = '';
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                            } 
                        }
                        else
                        {
                            include 'application/libraries/Gcmr.php';
                            $obj            = new Gcmr($event_id);
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        }
                    }
                }
            }
        }
        else
        {
            foreach ($users as $key => $value)
            {
                if($value['gcm_id']!='')
                {
                    $msg = '';
                    $extra['message_type'] = 'gamification';
                    if($value['version_code_id'] == '')
                    {
                        if($value['device'] == "Iphone")
                        {
                            $result[] = $obj1->send(1,$value['gcm_id'],$msg,$extra,$value['device'],$event_id);
                        }
                        else
                        {
                            $msg['title'] = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);
                        } 
                    }
                    else
                    {
                        include 'application/libraries/Gcmr.php';
                        $obj            = new Gcmr($event_id);
                        $msg['title'] = '';
                        $msg['message'] = '';
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $result[] = $obj->send_notification($value['gcm_id'],$msg,$extra,$value['device']);

                    }
                }
            }
        }
        return true;
    }
}
?>   