<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Native_single_fcm_v3 extends CI_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/native_single_fcm_v3_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Settings_model');
    }
    public function getL3techDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getL3techDefaultEvent();
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkL3techVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'L3tech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getLearn2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getLearn2019DefaultEvent();
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['3__stand'] = '';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function Learn2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Learn2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getParaxelEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1779');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function getUKCongressDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1740');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Stream';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function UKCongressVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'UKCongress';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getAEI2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1830');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function AEI2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'AEI2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function checkVenturefestVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Venturefest';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function VenturefestSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $events = $this->native_single_fcm_v3_model->VenturefestSearchEvent($event_name);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getVenturefestAllPublicEvents()

    {
        // error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getVenturefestAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
                if ($event[$key]['default_lang']['lang_id'] == '978')
                {
                    $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Stream';
                }
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkRiskUsaVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'RiskEvents';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function RiskUsaSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $events = $this->native_single_fcm_v3_model->RiskUsaSearchEvent($event_name);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getRiskUsaAllPublicEvents()

    {
        // error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getRiskUsaAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function GlobalWealthTechDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1995');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function GlobalWealthTechVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'GlobalWealthTech';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function OlukaiVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Olukai';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function OlukaiDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1809');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function OilCouncilVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'OilCouncil';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function OilCouncilDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1635');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkWatersUsaVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'WatersUSA';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function WatersUsaSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $events = $this->native_single_fcm_v3_model->WatersUsaSearchEvent($event_name);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getWatersUsaAllPublicEvents()

    {
        // error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getWaterskUsaAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function getInnovarioDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1353');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function InnovarioVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Innovario';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function CrugatherSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $events = $this->native_single_fcm_v3_model->CrugatherSearchEvent($event_name);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getCrugatherAllPublicEvents()

    {
        // error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->getCrugatherAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkCrugatherVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Crugather';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );;
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getOffshoreDecommissioningConference2018DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1667');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function OffshoreDecommissioningConference2018VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'OffshoreDecommissioningConference2018';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getTheManufacturingExpoDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1864');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function TheManufacturingExpoVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'TheManufacturingExpo';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getOEBBerlinDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1757');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function OEBBerlinVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'OEBBerlin';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getSolarPowerEuropeDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1808');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function SolarPowerEuropeVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'SolarPowerEurope';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getDelphiEconomicForumDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1888');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Hall';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function DelphiEconomicForumVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'DelphiEconomicForum';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function searchOGCounsilEventBySecurekey()

    {
        $key = $this->input->post('secure_key');
        if ($key != '')
        {
            $events = $this->Event_model->searchOGCounsilEventBySecurekey($key);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'message' => "successfully",
                    'data' => $events,
                    'URL' => 'https://allintheloop.com/privacy-policy.html'
                );
            }
            else
            {
                $data = array(
                    'success' => true,
                    'message' => "No Events found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function InfoProEventsSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $events = $this->native_single_fcm_v3_model->InfoProEventsSearchEvent($event_name);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function InfoProEventsAllPublicEvents()

    {
        // error_reporting(E_ALL);
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->InfoProEventsAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkInfoProEventsVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'InfoProEvents';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCode($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code) ? $code : '',
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function TraffexDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1762');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function TraffexVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Traffex';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function searchDelphiEventBySecurekey()

    {
        $key = $this->input->post('secure_key');
        if ($key != '')
        {
            $org_id = '401513';
            $events = $this->Event_model->searchEventBySecurekey($key, $org_id);
            foreach($events as $key => $value)
            {
                $events[$key]['default_lang']['1__sort_by_track'] = "Sort by Hall";
            }
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'message' => "successfully",
                    'data' => $events,
                    'URL' => 'https://allintheloop.com/privacy-policy.html'
                );
            }
            else
            {
                $data = array(
                    'success' => true,
                    'message' => "No Events found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function TrackjourneyDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1865');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function TrackjourneyVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Trackjourney';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function BBGA2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1992');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function BBGA2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'BBGA2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getEyeforpharmaBarcelonaDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1564');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkEyeforpharmaBarcelonaVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'EyeforpharmaBarcelona';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getEyeforpharmaPhiladelphiaDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2053');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Interest';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkEyeforpharmaPhiladelphiaVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'EyeforpharmaPhiladelphia';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getLTFrance2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1764');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkLTFrance2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'LTFrance2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function searchRethinkEventBySecurekey()

    {
        $key = $this->input->post('secure_key');
        if ($key != '')
        {
            $org_id = '479096';
            $events = $this->Event_model->searchEventBySecurekey($key, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'message' => "successfully",
                    'data' => $events,
                    'URL' => 'https://allintheloop.com/privacy-policy.html'
                );
            }
            else
            {
                $data = array(
                    'success' => true,
                    'message' => "No Events found"
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function RethinkAllPublicEvents()

    {
        $gcm_id = $this->input->post('gcm_id');
        $publicevents = $this->native_single_fcm_v3_model->RethinkAllPublicEvents($gcm_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkRethinkVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Rethink';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function RethinkSearchEvent()

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '479096';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
    public function getLTLondon2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1763');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I agree to the Terms and the Privacy Policy';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkLTLondon2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'LTLondon2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getGFMF2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2029');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__save_session'] = 'Register';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkLTGFMF2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'GFMF2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getEWMA2019DefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2055');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__save_session'] = 'Register';
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkEWMA2019VersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'EWMA2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getConnectXDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1987');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkConnectXVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'ConnectX';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getEuroBrakeDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1873');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Type';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkEuroBrakeVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'EuroBrake';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getASPConferenceDefaultEvent()

    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1539');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkASPConferenceVersionCode()

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'ASPConference';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function IBISAllPublicEvents()//multi event

    {
        $gcm_id = $this->input->post('gcm_id');
        $org_id = '466063';
        $publicevents = $this->native_single_fcm_v3_model->MultiAllPublicEvents($gcm_id,$org_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
                $publicevents[$key]['default_lang']['49__you_have_not_saved_any_favorites_yet'] = "You have not saved any favourites yet.";
                $publicevents[$key]['default_lang']['1__sort_by_time'] = "Sort by Day";
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkIBISVersionCode()//multi event

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'IBIS';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function IBISSearchEvent()//multi event

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '466063';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            foreach ($events as $key => $value) {
                $events[$key]['default_lang']['1__sort_by_time'] = "Sort by Day";
            }
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function InvestorConferencesAllPublicEvents()//multi event

    {
        $gcm_id = $this->input->post('gcm_id');
        $org_id = '340998';
        $publicevents = $this->native_single_fcm_v3_model->MultiAllPublicEvents($gcm_id,$org_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    public function checkInvestorConferencesVersionCode()//multi event

    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'InvestorConferences';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function InvestorConferencesSearchEvent()//multi event

    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '340998';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
}