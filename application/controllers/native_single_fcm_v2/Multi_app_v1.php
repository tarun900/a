<?php
/*
Created Date 23-Jan-19 3:12:27 PM
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Multi_app_v1 extends CI_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/native_single_fcm_v3_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Settings_model');
    }

    public function WATS2019AllPublicEvents()//multi event
    {
        $gcm_id = $this->input->post('gcm_id');
        $org_id = '494824';
        $publicevents = $this->native_single_fcm_v3_model->MultiAllPublicEvents($gcm_id,$org_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkWATS2019VersionCode()//multi event
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'WATS2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function WATS2019SearchEvent()//multi event
    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '494824';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function HFTP2019AllPublicEvents()//multi event
    {
        $gcm_id = $this->input->post('gcm_id');
        $org_id = '55497';
        $publicevents = $this->native_single_fcm_v3_model->MultiAllPublicEvents($gcm_id,$org_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkHFTP2019VersionCode()//multi event
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'HFTP2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function HFTP2019SearchEvent()//multi event
    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '55497';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }

    public function Obforum2019AllPublicEvents()//multi event Obforum2019
    {
        $gcm_id = $this->input->post('gcm_id');
        $org_id = '506762';
        $publicevents = $this->native_single_fcm_v3_model->MultiAllPublicEvents($gcm_id,$org_id);
        if (!empty($publicevents))
        {
            foreach($publicevents as $key => $value)
            {
                $publicevents[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $publicevents[$key]['Event_name'] = ucfirst($value['Event_name']);
            }
            $data = array(
                'success' => true,
                'message' => "successfully",
                'data' => $publicevents
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Public Events found"
            );
        }
        echo json_encode($data);
    }
    
    public function checkObforum2019VersionCode()//multi event Obforum2019
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Obforum2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function Obforum2019SearchEvent()//multi event Obforum2019
    {
        $event_name = $this->input->post('event_name');
        if ($event_name != '')
        {
            $org_id = '506762';
            $events = $this->native_single_fcm_v3_model->MultiSearchEvent($event_name, $org_id);
            if (!empty($events))
            {
                $data = array(
                    'success' => true,
                    'data' => $events
                );
            }
            else
            {
                $data = array(
                    'success' => false,
                    'message' => "No app found."
                );
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parametrs"
            );
        }
        echo json_encode($data);
    }
}