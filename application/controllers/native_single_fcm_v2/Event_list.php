<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Event_list extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('Event_model');

    }

    /*public function index()
    {
        $event_list = $this->Login_model->check_token_with_event($this->input->post('_token'));
        foreach ($event_list as $key => $values) 
        {
            $bannerimage_decode = json_decode($values['Images']);
            $logoimage_decode = json_decode($event_list['Logo_images']);
            $bgimage_decode = json_decode($event_list['Background_img']);
            $event_list[$key]['Images'] = $bannerimage_decode[0];
            $event_list[$key]['Logo_images'] = $logoimage_decode[0];
            $event_list[$key]['Background_img'] = $bgimage_decode[0];
        }

        if (empty($event_list)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $data = array(
                'events' => $event_list
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }*/

    public function index2()
    {
        $event_list = $this->Login_model->check_event_with_token($this->input->post('_token'));
        foreach ($event_list as $key => $values) 
        {
            $bannerimage_decode = json_decode($values['Images']);
            $logoimage_decode = json_decode($values['Logo_images1']);
            $bgimage_decode = json_decode($values['Background_img1']);
            $fun_logo_images_decode = $values['fun_logo_images1'];
            $contact_us = $values['Contact_us1'];
            $Icon_text_color = $values['Icon_text_color1'];
            $fun_top_background_color = $values['fun_top_background_color1'];
            $fun_top_text_color = $values['fun_top_text_color1'];
            $fun_footer_background_color = $values['fun_footer_background_color1'];
            $fun_block_background_color = $values['fun_block_background_color1'];
            $fun_block_text_color = $values['fun_block_text_color1'];
            $fun_header_status = $values['fun_header_status1'];
            $theme_color = $values['theme_color1'];
            $description = $values['description1'];
        
            if($bgimage_decode == NULL)
            {
                $event_list[$key]['Background_img1'] = "";
            } 
            else
            {
                $event_list[$key]['Background_img1'] = $bgimage_decode[0];
            }

            if($logoimage_decode == NULL)
            {
                $event_list[$key]['Logo_images1'] = "";
            } 
            else
            {
                $event_list[$key]['Logo_images1'] = $logoimage_decode[0];
            }

            if($fun_logo_images_decode == NULL)
            {
                $event_list[$key]['fun_logo_images1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_logo_images1'] = $fun_logo_images_decode;
            }

            if($contact_us == NULL)
            {
                $event_list[$key]['contact_us1'] = "";
            } 
            else
            {
                $event_list[$key]['contact_us1'] = $contact_us;
            }

            if($Icon_text_color == NULL)
            {
                $event_list[$key]['Icon_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['Icon_text_color1'] = $Icon_text_color;
            }

            if($fun_top_background_color == NULL)
            {
                $event_list[$key]['fun_top_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_top_background_color1'] = $fun_top_background_color;
            }

            if($fun_top_text_color == NULL)
            {
                $event_list[$key]['fun_top_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_top_text_color1'] = $fun_top_text_color;
            }

            if($fun_footer_background_color == NULL)
            {
                $event_list[$key]['fun_footer_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_footer_background_color1'] = $fun_footer_background_color;
            }

            if($fun_block_background_color == NULL)
            {
                $event_list[$key]['fun_block_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_block_background_color1'] = $fun_block_background_color;
            }

            if($fun_block_text_color == NULL)
            {
                $event_list[$key]['fun_block_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_block_text_color1'] = $fun_block_text_color;
            }

            if($theme_color == NULL)
            {
                $event_list[$key]['theme_color1'] = "";
            } 
            else
            {
                $event_list[$key]['theme_color1'] = $theme_color;
            }

            if($description == NULL)
            {
                $event_list[$key]['description1'] = "";
            } 
            else
            {
                $event_list[$key]['description1'] = $description;
            }

            if($fun_header_status == NULL)
            {
                $event_list[$key]['fun_header_status1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_header_status1'] = $fun_header_status;
            }
            

        } 

        if (empty($event_list)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $data = array(
                'events' => $event_list
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function check_email_with_event()
    {
        $event_list = $this->Login_model->check_email_with_event($this->input->post('_token'),$this->input->post('email'));
        foreach ($event_list as $key => $values) 
        {
            $bannerimage_decode = json_decode($values['Images']);
            $logoimage_decode = json_decode($values['Logo_images1']);
            $bgimage_decode = json_decode($values['Background_img1']);
            $fun_logo_images_decode = $values['fun_logo_images1'];
            $contact_us = $values['Contact_us1'];
            $Icon_text_color = $values['Icon_text_color1'];
            $fun_top_background_color = $values['fun_top_background_color1'];
            $fun_top_text_color = $values['fun_top_text_color1'];
            $fun_footer_background_color = $values['fun_footer_background_color1'];
            $fun_block_background_color = $values['fun_block_background_color1'];
            $fun_block_text_color = $values['fun_block_text_color1'];
            $fun_header_status = $values['fun_header_status1'];
            $theme_color = $values['theme_color1'];
            $description = $values['description1'];
        
            if($bgimage_decode == NULL)
            {
                $event_list[$key]['Background_img1'] = "";
            } 
            else
            {
                $event_list[$key]['Background_img1'] = $bgimage_decode[0];
            }

            if($logoimage_decode == NULL)
            {
                $event_list[$key]['Logo_images1'] = "";
            } 
            else
            {
                $event_list[$key]['Logo_images1'] = $logoimage_decode[0];
            }

            if($fun_logo_images_decode == NULL)
            {
                $event_list[$key]['fun_logo_images1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_logo_images1'] = $fun_logo_images_decode;
            }

            if($contact_us == NULL)
            {
                $event_list[$key]['contact_us1'] = "";
            } 
            else
            {
                $event_list[$key]['contact_us1'] = $contact_us;
            }

            if($Icon_text_color == NULL)
            {
                $event_list[$key]['Icon_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['Icon_text_color1'] = $Icon_text_color;
            }

            if($fun_top_background_color == NULL)
            {
                $event_list[$key]['fun_top_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_top_background_color1'] = $fun_top_background_color;
            }

            if($fun_top_text_color == NULL)
            {
                $event_list[$key]['fun_top_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_top_text_color1'] = $fun_top_text_color;
            }

            if($fun_footer_background_color == NULL)
            {
                $event_list[$key]['fun_footer_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_footer_background_color1'] = $fun_footer_background_color;
            }

            if($fun_block_background_color == NULL)
            {
                $event_list[$key]['fun_block_background_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_block_background_color1'] = $fun_block_background_color;
            }

            if($fun_block_text_color == NULL)
            {
                $event_list[$key]['fun_block_text_color1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_block_text_color1'] = $fun_block_text_color;
            }

            if($theme_color == NULL)
            {
                $event_list[$key]['theme_color1'] = "";
            } 
            else
            {
                $event_list[$key]['theme_color1'] = $theme_color;
            }

            if($description == NULL)
            {
                $event_list[$key]['description1'] = "";
            } 
            else
            {
                $event_list[$key]['description1'] = $description;
            }

            if($fun_header_status == NULL)
            {
                $event_list[$key]['fun_header_status1'] = "";
            } 
            else
            {
                $event_list[$key]['fun_header_status1'] = $fun_header_status;
            }
            

        } 

        if (empty($event_list)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $data = array(
                'events' => $event_list
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }
}
