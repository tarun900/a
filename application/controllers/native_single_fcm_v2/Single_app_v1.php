<?php
/*
Created Date 23-Jan-19 3:12:27 PM
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Single_app_v1 extends CI_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/native_single_fcm_v3_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Settings_model');
    }
    
    public function getSolarPowerSummitDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2098');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkSolarPowerSummitVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'SolarPowerSummit';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }

    public function getManufacturingBelfastDefaultEvent()
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2153');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => "No Events found"
            );
        }
        echo json_encode($data);
    }

    public function checkManufacturingBelfastVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'ManufacturingBelfast';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters",
            );
        }
        echo json_encode($data);
    }
    public function getGlobalInsurTechSummitDefaultEvent() // 28-Jan-2019 Jagdish
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent(2061);
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    
    public function checkGlobalInsurTechSummitVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'GlobalInsurTechSummit';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getGFMF2019DefaultEvent() // 31-Jan-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2029');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkGFMF2019VersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'GFMF2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getReindersDefaultEvent() // 01-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1868');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
                $event[$key]['default_lang']['1__sort_by_track'] = 'Sort by Room';
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkReindersVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'Reinders';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }

    public function getACI2019DefaultEvent() // 06-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2177');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkACI2019VersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'ACI2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getAvangridRenewables2019DefaultEvent() // 13-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1526');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkAvangridRenewables2019VersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'AvangridRenewables2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getNCVOAnnualConferenceDefaultEvent() // 14-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('1814');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkNCVOAnnualConferenceVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'NCVOAnnualConference';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getIRMSConference2019DefaultEvent() // 15-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2158');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https://allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkIRMSConference2019VersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'IRMSConference2019';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getIRXExpoDefaultEvent() // 26-Feb-2019
    {
        $event = $this->native_single_fcm_v3_model->getDefaultEvent('2026');
        if (!empty($event))
        {
            foreach($event as $key => $value)
            {
                $event[$key]['Logo_images'] = ($value['Logo_images']) ? $value['Logo_images'] : '';
                $event[$key]['Event_name'] = ucfirst($value['Event_name']);
                $event[$key]['default_lang'] = $this->App_login_model->get_default_lang_label($value['event_id']);
            }
            $data = array(
                'success' => true,
                'data' => $event,
                'URL' => 'https: //allintheloop.com/privacy-policy.html'
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'No Events found'
            );
        }
        echo json_encode($data);
    }
    public function checkIRXExpoVersionCode()
    {
        $device = $this->input->post('device');
        if ($device != '')
        {
            $where['app'] = 'IRXExpo';
            $device = strtolower($device);
            $code = $this->Settings_model->getVersionCodeV2($where, $device);
            $data = array(
                'success' => true,
                'code' => ($code[$device . '_code']) ? $code[$device . '_code'] : '',
                'version_id' => $code['Id']
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
}