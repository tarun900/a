<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller     
{
    function __construct() 
    {   
        parent::__construct();
        $this->load->model('native_single_fcm_v2/App_login_model');
        $this->load->model('native_single_fcm_v2/Event_model');
        $this->load->model('native_single_fcm_v2/Cms_model');
        $this->load->model('native_single_fcm_v2/Note_model');
        $this->load->model('native_single_fcm_v2/Event_template_model');
        $this->load->model('native_single_fcm_v2/Attendee_model');
        $this->events = $this->Event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images'] = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        $this->cmsmenu = $this->Cms_model->get_cms_page_new($this->input->get_post('event_id'),NULL,NULL,$this->input->post('
            user_id'),$this->input->post('lang_id'));
        $this->cmsmenu1 = $this->Cms_model->get_cms_page_new1($this->input->get_post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            if(!empty($values['Images']))
            {
                $cmsbannerimage_decode = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Images'] = "";
            }
            if(!empty($values['Logo_images']))
            {
                $cmslogoimage_decode = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }

        $this->menu_list = $this->Event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->menu_list1 = $this->Event_model->geteventmenu_list1($this->input->post('event_id'), null, null,$this->input->post('user_id'));
    }

    public function index()
    {

        $user = $this->App_login_model->check_token_with_event($this->input->post('_token'),$this->input->post('event_id'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $fetureproduct = $this->Event_model->getFetureProduct($this->input->post('event_id'));

            $data = array(
                'events' => $this->events,
                'menu_list' => $this->menu_list,
                'cmsmenu' => $this->cmsmenu
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    /***To give Homescreen data for adavnce design features **/

    public function event_id_advance_design()
    {   
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->App_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {   
                $event_list = $this->get_event($event_id,$user_id);
                //
                $user = $this->App_login_model->getUserDetailsId($user_id,$event_id);
                if($user_id)
                $this->App_login_model->saveUserLoginEvent($event_id,$user_id);
                if(!empty($event_list))
                {
                    $event_list[0]['show_lead_retrival_setting_tab'] = '1';
                    $event_list[0]['description1'] = html_entity_decode($event_list[0]['description1']);
                    $event_list[0]['show_attendee_category'] = $this->App_login_model->getAttendeeCategoryStatus($event_id);
                    // $this->Event_model->checkRepresentative($user_id,$event_id);
                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->Note_model->getNotesListByEventId($event_id,$user_id);
                        $note_status= (count($note_data)>0) ? 1 : 0;
                    }
                    
                        
                    foreach ($this->menu_list as $key => $value)
                    {
                        if($value['is_feture_products'] == '1')
                        {
                            $value['is_cms'] = '0';
                            $value['my_id'] = $value['id'];
                            $menu[] = $value;
                        }
                        $module_relation = $this->App_login_model->get_modules_relation($event_id,$value['id']);
                        if($value['id'] == '1' && !empty($value['category_list']) && $value['is_feture_products'] == '1')
                        {   
                            foreach ($value['category_list'] as $k => $v)
                            {   
                                $v['is_cms'] = '0';
                                $menu[] = $v;
                            }
                        }
                        if($value['id'] == '1' && !empty($value['category_list']))
                        {
                            foreach ($value['category_list'] as $k => $v)
                            {   
                                if(in_array($v['category_id'],$module_relation))
                                {
                                    unset($this->menu_list[$key]['category_list'][$k]);
                                }
                            }   
                        }
                        $this->menu_list[$key]['group'] = $this->App_login_model->get_modules_group($event_id,$value['id']);
                        $this->menu_list[$key]['super_group'] = $this->App_login_model->get_modules_super_group($event_id,$value['id']);
                        $this->menu_list[$key]['category_list'] = array_values($this->menu_list[$key]['category_list']);

                    }
                    foreach ($this->cmsmenu as $key => $value)
                    {
                        if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                        {
                            $value['my_id'] = $value['Id'];
                            $value['is_cms'] = '1';
                            $value['category_id'] = "";
                            $menu[] = $value;
                        }
                        $module_relation = $this->App_login_model->get_modules_relation($event_id,'21');
                        if(in_array($value['Id'],$module_relation))
                        {
                            unset($this->cmsmenu[$key]);
                        }
                    }
                    $this->cmsmenu = array_values($this->cmsmenu);
                    $this->menu_list = array_values($this->menu_list);
                    
                    foreach ($menu as $key => $value) 
                    {
                        $menu[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                        $menu[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                        $menu[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                        $menu[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                        if($value['is_cms'] == '1')
                        $menu[$key]['is_force_login'] = 0;
                    }

                    function sortByOrder($a, $b)
                    {
                        return $a['sort_order'] - $b['sort_order'];
                    }

                    usort($menu, 'sortByOrder');

                    #cache
                    /*$this->load->library('memcached_library');
                    $banner_images_coords = $this->memcached_library->get("banner_images_coords$event_id");
                    if(!$banner_images_coords)
                    {*/
                        $banner_images_coords = $this->App_login_model->banner_images_and_coords($event_id,$user_id);
                        /*$this->memcached_library->add("banner_images_coords$event_id",$banner_images_coords); */
                    //}
                    #cache

                    /*$no_compress_image =  array('992','750','1032','1320','1319','1318','1317','1307','1169','1312');
                    if(in_array($event_id,$no_compress_image))
                    {
                        $is_compress_image = '0'; 
                    }
                    else
                    {
                        $is_compress_image = '1';
                    }*/
                    // error_reporting(E_ALL);
                    /*if($event_id == '259')
                    {
                        foreach ($banner_images_coords['images'] as $key => $value) 
                        {   
                            if(!empty($value['Image']))
                            {
                                $source_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$value['Image'];
                                $info = getimagesize($source_url);
                                $new_name = "new_".$value['Image'];
                                $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $source_url;
                                $config['new_image'] = $destination_url;
                                $config['create_thumb'] = TRUE;
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = '110';
                                $config['height'] = '110';
                                $this->load->library('image_lib', $config);
                                $this->image_lib->resize();

                                
                                if ($info['mime'] == 'image/jpeg')
                                {   
                                    $quality = 50;
                                    $image = imagecreatefromjpeg($source_url);
                                    imagejpeg($image, $destination_url, $quality);
                                }
                                elseif ($info['mime'] == 'image/gif')
                                {   
                                    $new_name = $value['Image'];
                                }
                                elseif ($info['mime'] == 'image/png')
                                {   
                                    $quality = 9;
                                    $image = imagecreatefrompng($source_url);
                                    $background = imagecolorallocatealpha($image,255,0,255,127);
                                    imagecolortransparent($image, $background);
                                    imagealphablending($image, false);
                                    imagesavealpha($image, true);
                                    imagepng($image, $destination_url, $quality);
                                }

                                $banner_images_coords['images'][$key]['Image'] = $new_name;
                            }
                        }
                    }*/

                    $lang_list = $this->App_login_model->get_event_lang_list($event_id);
                    
                    if($event_id == '1069' || $event_id == '1353')
                    {
                        unset($lang_list[0]);
                        $lang_list = array_values($lang_list);
                    }

                    $this->load->model('native_single_fcm_v2/Settings_model');
                    $o_screen = $this->Settings_model->get_onboarding_settings($event_id);
                    $event_list[0]['o_screen']= $o_screen;

                    if(!$event_list[0]['auto_allocate_meeting_location']){
                        $this->load->model('native_single_fcm_v2/Attendee_model');
                        $meeting_location = $this->Attendee_model->get_meeting_location($event_id);
                    }

                    $activity_share_icon_setting = $this->Event_model->get_activity_share_icon_setting($event_id);
                    

                    $advertise = $this->App_login_model->getAdvertise($event_id);
                    $data1 = array(
                        'events' => $event_list,
                        'banner_images' => $banner_images_coords,
                        'menu_list' => $this->menu_list,
                        'cmsmenu' => $this->cmsmenu,
                        'ersite_home_page_menu' =>($event_id == '455') ?  $array1 : [],
                        'aitl_home_page_menu' => ($menu != '') ?  $menu : [],
                        'note_status' => $note_status,
                        'lang_list' => $lang_list,
                        'advertise' => $advertise,
                        'host_name' => 'server.allintheloop.net',
                        'show_meeting_location' => ($meeting_location) ? '1' : '0'
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***To give default language for Native **/

    public function get_default_lang()
    {
        $event_id=$this->input->post('event_id');
        $lang_id=$this->input->post('lang_id');
        if(!empty($event_id))
        {
            $default_lang=$this->App_login_model->get_default_lang_label($event_id,$lang_id);
            if(($event_id == '992' && $default_lang['lang_id'] == '651') || ($event_id == '1107' && $default_lang['lang_id'] == '772') || ($event_id == '1682'))
            {
                $default_lang['1__check_in'] = 'Request Slides';
                $default_lang['1__sort_by_track'] = 'Sort by Theatre';
            }
            if($event_id == '1012' && $default_lang['lang_id'] == '673')
            {
                $default_lang['1__provide_feedback'] = 'Ask a Question';
            }
            if($event_id == '799' || $event_id == '1044' || $event_id == '1045' || $event_id == '1564' || $event_id == '1868' || $event_id == '2055')
            {
                $default_lang['1__sort_by_track'] = 'Sort by Room';
            }

            if($event_id == '2053')
            {
                $default_lang['1__sort_by_track'] = 'Sort by Interest';
            }

            if($event_id == '2029')
            {
                $default_lang['1__save_session'] = 'Register';
            }

            if($event_id == '1761' || $event_id == '1873')
            {
                $default_lang['1__sort_by_track'] = 'Sort by Type';
            }

            if($event_id == '1124')
            {
                $default_lang['1__view_my_agenda'] = 'My Agenda';
                $default_lang['1__sort_by_time'] = 'Conference Agenda';
            }

            if($event_id == '870')
            {
                $default_lang['my_profile__login'] = 'Login/Sign Up';
                $default_lang['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';
            }

            if($event_id == '1203')
            {
                $default_lang['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';
            }
            
            //UHG 
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            if($org_id == '20152')
            {
                $default_lang['1__save_session'] = 'Reserve my Seat';
                $default_lang['3__stand'] = 'Location';
                $default_lang['1__saved'] = 'Reserved';
            }

            if($org_id == '381895' || $org_id == '410111')
            {
                $default_lang['1__sort_by_track'] = 'Sort By Stream';
            }

            if($org_id == '466063')
            {
                $default_lang['1__sort_by_time'] = 'Sort By Day';
            }

            if($org_id == '39756')
            {
                $default_lang['sign_up_process__i_agree_to_the_terms_&_conditions'] = 'I Agree to the Terms & Conditions';
            }

            if($event_id == '1397' || $event_id == '1740')
            {
                $default_lang['1__sort_by_track'] = 'Sort By Stream';
            }

            if($event_id == '1888')
            {
                $default_lang['1__sort_by_track'] = 'Sort by Hall';
            }

            $lang_list = $this->App_login_model->get_event_lang_list($event_id);
            if($event_id == '1353')
            {
                unset($lang_list[0]);
                $lang_list = array_values($lang_list);
            }

            $data = array(
                'success' => true,
                'default_lang' => $default_lang,
                'lang_list' => $lang_list
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Compress image of homescreen **/

    public function compress_image($data=NULL)
    {   
        if(is_array($data))
        {
            foreach ($data as $key => $value) 
            {   
                if(!empty($value))
                {
                    $source_url = base_url()."assets/user_files/".$value;
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value;
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                    if ($info['mime'] == 'image/jpeg')
                    {   
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {   
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);

                    }
                    elseif ($info['mime'] == 'image/png')
                    {   
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);

                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $new_data[] = $new_name;
                }
            }
            return $new_data;
        }
        elseif(!empty($data))
        {
            $source_url = base_url()."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 50;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);
                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
            return $new_name;
        }
    }

    /***Get Data of event **/

    public function get_event($event_id=NULL,$user_id=NULL)
    {   
        if($event_id!='')
        {
            $event_list = $this->App_login_model->check_event_with_id($event_id,$user_id);
            $user = $this->App_login_model->getUserDetailsId($user_id);

            if(!empty($event_list))
            {
                $menus = explode(',', $event_list[0]['checkbox_values']);
                $tmp = preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_list[0]['Description'])));
                $event_list[0]['Description'] = empty($tmp) ?  "" : $event_list[0]['Description'];

                $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";

                if($event_list[0]['fun_background_color']==0)
                {
                    $event_list[0]['fun_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_text_color']==0)
                {
                    $event_list[0]['fun_top_text_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                }
                if($event_list[0]['fun_footer_background_color']==0)
                {
                    $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_block_background_color']==0)
                {
                    $event_list[0]['fun_block_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_background_color']==0)
                {
                    $event_list[0]['fun_top_background_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                }
                if($event_list[0]['theme_color']==0)
                {
                    $event_list[0]['theme_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                }
                if($event_list[0]['fun_block_text_color']==0)
                {
                    $event_list[0]['fun_block_text_color']="#FFFFFF";
                }

                if($event_list[0]['fun_footer_background_color']==0)
                {
                    $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                $img=json_decode($event_list[0]['Images']);
                $Logo_images=json_decode($event_list[0]['Logo_images']);
                $Background_img=json_decode($event_list[0]['Background_img']);

                if(empty($Logo_images[0]))
                {
                    $Logo_images[0]="";
                }
                if(empty($Background_img[0]))
                {
                    $Background_img[0]="";
                }

                $event_list[0]['Images']=$img[0];

                //$event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                $event_list[0]['banners'] = $img;

                $banner_url = json_decode($event_list[0]['banner_url'],true);
                foreach ($img as $key => $value)
                {
                    $b_tmp[$key]['url'] = !empty($banner_url[$value])? $banner_url[$value] :'';
                    //$b_tmp[$key]['image'] = "new_".$value;
                    $b_tmp[$key]['image'] = $value;
                }
                
                $event_list[0]['banners_url'] = !empty($b_tmp)?$b_tmp:[];
                $event_list[0]['Logo_images1']=$Logo_images[0];
                $event_list[0]['Background_img1']=$Background_img[0];
                $event_list[0]['description1']=$event_list[0]['Description'];
                $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];

                $checkbox_values = $event_list[0]['checkbox_values'];
                $menu_array = explode(',', $checkbox_values);

                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                
                $active_module = array_column_1($this->menu_list, 'id');
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;
                $event_list[0]['photo_filter_enabled'] = (in_array('54',array_column_1($this->menu_list,'id'))) ? '1' : '0';

                $event_list[0]['time_format'] = $this->App_login_model->getTimeFormat($event_id)[0]['format_time'];
               /* $event_list[0]['users_agenda'] = $this->App_login_model->get_all_users_agenda($user_id);*/
                $event_list[0]['attendee_agenda'] = $this->App_login_model->get_attendee_agenda($event_id,$user_id);
                $this->load->model('native_single_fcm_v2/Exhibitor_model');
                $meeting_data = $this->Exhibitor_model->getAllMeetingRequest1($event_id,$user_id);
                $event_list[0]['show_meeting_button'] = (empty($meeting_data)) ? '0' :'1';

                $all_menu_id = array_column_1($this->menu_list,'id');
                $event_list[0]['lead_retrival_enabled'] = in_array('53',$all_menu_id)? '1':'0'; 
                $event_list[0]['private_message_enabled'] = in_array('12',$all_menu_id)? '1':'0'; 
                $event_list[0]['public_message_enabled'] = in_array('13',$all_menu_id)? '1':'0'; 

            }
        }
        return $event_list;                   
    }   

    /***Get Updated module list for Native **/

    public function getModulesUpdatedLogs()
    {
        $event_id = $this->input->get_post('event_id');

        if($event_id == '')
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );

            echo json_encode($data);exit;
        }
        $date_array[] = $this->App_login_model->getLatestUpdatedDate('exibitor','Event_id',$event_id);
        $date_array[] = $this->App_login_model->getLatestUpdatedDate('exhibitor_category','event_id',$event_id);
        $date_array[] = $this->App_login_model->getLatestUpdatedDate('user','Event_id',$event_id);
        $date_array[] = $this->App_login_model->getLatestUpdatedDate('event_countries','event_id',$event_id);
        $date_array[] = $this->App_login_model->getLatestUpdatedDate('exhi_category_group','event_id',$event_id);
        $sponsors_date[] = $this->App_login_model->getLatestUpdatedDate('sponsors','Event_id',$event_id);
        $sponsors_date[] = $this->App_login_model->getLatestUpdatedDate('sponsors_type','event_id',$event_id);
        
        $agenda_date[] = $this->App_login_model->getLatestUpdatedDate('agenda','Event_id',$event_id);
        $agenda_date[] = $this->App_login_model->getLatestUpdatedDate('agenda_categories','event_id',$event_id);
        $agenda_date[] = $this->App_login_model->getLatestUpdatedDate('session_types','event_id',$event_id);
        // $d = $this->App_login_model->getLatestUpdatedDate('modules_group','event_id',$event_id);
        rsort($date_array);
        rsort($sponsors_date);
        rsort($agenda_date);

        $group[] = ($this->App_login_model->getLatestUpdatedDate('modules_group','event_id',$event_id))?:'';
        $group[] = ($this->App_login_model->getLatestUpdatedDate('modules_super_group','event_id',$event_id))?:'';
        rsort($group);
        $group = ($group[0])?:'';
        $data = array(
          'success' => true,
          'updated_date' => ($date_array[0])?:'',//exhi updated date
          'exhibitor' => ($date_array[0])?:'',
          'group' => $group,
          'cms' => ($this->App_login_model->getLatestUpdatedDate('cms','Event_id',$event_id))?:'',
          'super_group' => $group,
          'map' => ($this->App_login_model->getLatestUpdatedDate('map','Event_id',$event_id))?:'',
          'qa' => ($this->App_login_model->getLatestUpdatedDate('qa_session','Event_id',$event_id))?:'',
          'sponsor' => ($sponsors_date[0])?:'',
          'agenda'  => ($agenda_date[0])?:'',
          'speaker' => ($this->App_login_model->getLatestSpeakerUpdateDate($event_id))?:''
        );
        echo json_encode($data);
    }

    /***Get Favourite Exhibitors **/

    public function getFavouritedExhibitors()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id=='' && $user_id =='')
        {
             $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );
            echo json_encode($data);exit;
        }

        $ex_data = $this->App_login_model->getFavouritedExhibitors($event_id,$user_id);

         $data = array(
          'success' => true,
          'data' => $ex_data,
        );
        echo json_encode($data);
    }

    /***Not usefull **/

    public function test_arab_token()
    {
         $user = $this->App_login_model->check_token_with_event_test($this->input->post('_token'),$this->input->post('event_id'));
         //lq();
         j($user);
    }

    // Get login screen settings
    public function get_login_screen_settings()
    {
        $event_id = $this->input->post('event_id');
        if(!empty($event_id))
        {

            $data = $this->App_login_model->get_login_screen_settings($event_id);
            
            $data_n['login_screen_image'] = ($data['login_screen_image'])?'/login_screen/'.$event_id.'/'.$data['login_screen_image']:'';
            $data_n['login_screen_text'] = nl2br(stripslashes($data['login_screen_text']));
            
            $data = array(
                'success' => true,
                'data' => ($data_n)?:new stdClass(),
                'URL'  => 'http://allintheloop.com/privacy-policy.html'
                );
        }
        else
        {
            $data = array(
                    'success' => false,
                    'message' => 'Invalid parameters'
                    );
        }
        echo json_encode($data);
    }
    public function getUpdatedByModules()
    {
        extract($this->input->post());
        
        if(empty($event_id) || empty($module))
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );

            echo json_encode($data);exit;
        }

        switch ($module)
        {
        	case 'exhibitor':
        		$date_array[] = $this->App_login_model->getLatestUpdatedDate('exibitor','Event_id',$event_id);
        		$date_array[] = $this->App_login_model->getLatestUpdatedDate('exhibitor_category','event_id',$event_id);
        		$date_array[] = $this->App_login_model->getLatestUpdatedDate('user','Event_id',$event_id);
        		$date_array[] = $this->App_login_model->getLatestUpdatedDate('event_countries','event_id',$event_id);
        		$date_array[] = $this->App_login_model->getLatestUpdatedDate('exhi_category_group','event_id',$event_id);
        		rsort($date_array);
        		$date = ($date_array[0])?:'';
        		break;
        	
        	case 'cms':
        		$date = ($this->App_login_model->getLatestUpdatedDate('cms','Event_id',$event_id))?:'';
        		break;

        	case 'map':
        		$date = ($this->App_login_model->getLatestUpdatedDate('map','Event_id',$event_id))?:'';
        		break;

        	case 'qa':
        		$date = ($this->App_login_model->getLatestUpdatedDate('qa_session','Event_id',$event_id))?:'';
        		break;

        	case 'sponsor':
        		$sponsors_date[] = $this->App_login_model->getLatestUpdatedDate('sponsors','Event_id',$event_id);
        		$sponsors_date[] = $this->App_login_model->getLatestUpdatedDate('sponsors_type','event_id',$event_id);
        		rsort($sponsors_date);
        		$date = ($sponsors_date[0])?:'';
        		break;

        	case 'agenda':
        		$agenda_date[] = $this->App_login_model->getLatestUpdatedDate('agenda','Event_id',$event_id);		
		        $agenda_date[] = $this->App_login_model->getLatestUpdatedDate('agenda_categories','event_id',$event_id);
		        $agenda_date[] = $this->App_login_model->getLatestUpdatedDate('session_types','event_id',$event_id);
		        rsort($agenda_date);
		        $date = ($agenda_date[0])?:'';
        		break;
        	case 'speaker':
        		$date = ($this->App_login_model->getLatestSpeakerUpdateDate($event_id))?:'';
        		break;
        }

        $group[] = ($this->App_login_model->getLatestUpdatedDate('modules_group','event_id',$event_id))?:'';
        $group[] = ($this->App_login_model->getLatestUpdatedDate('modules_super_group','event_id',$event_id))?:'';
        rsort($group);
        $group = ($group[0])?:'';

        $data = array(
          'success' => true,
          'date' => ($date)?:$group,
          'module' => $module,
          'group' => $group
          );
        echo json_encode($data);
    }
    public function getModuleUpdateDate()
    {   
        $event_id = $this->input->post('event_id');
        if($event_id)
        {
            $this->db->where('event_id',$event_id);
            $res = $this->db->get('modules_update_log')->result_array();
            foreach ($res as $key => $value)
            {
                $data[$value['module_name']] = ($value['updated_date'])?:'';
            }

            $update_new = array('exhibitor','cms','map','qa','sponsor','agenda','speaker','group'); 

            $result=array_diff($update_new,array_keys($data));
            $date = date('Y-m-d H:i:s');
            foreach ($result as $key => $value)
            {
                $insert[$key]['module_name'] = $value;
                $insert[$key]['updated_date'] = $date;
                $insert[$key]['event_id'] = $event_id;
            }
            if(!empty($insert))
            {
                $this->db->insert_batch('modules_update_log',$insert);
            }

            $data['exhibitor'] = array_key_exists('exhibitor',$data) ? $data['exhibitor']: $date;
            $data['cms'] = array_key_exists('cms',$data) ? $data['cms']: $date;
            $data['map'] = array_key_exists('map',$data) ? $data['map']: $date;
            $data['qa'] = array_key_exists('qa',$data) ? $data['qa']: $date;
            $data['sponsor'] = array_key_exists('sponsor',$data) ? $data['sponsor']: $date;
            $data['agenda'] = array_key_exists('agenda',$data) ? $data['agenda']: $date;
            $data['speaker'] = array_key_exists('speaker',$data) ? $data['speaker']: $date;
            $data['group'] = array_key_exists('group',$data) ? $data['group']: $date;
            $data['success'] = true;
            echo json_encode($data);exit;
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
            echo json_encode($data);exit;
        }
    }
    public function getUpdatedByModulesNew()
    {
        extract($this->input->post());
        
        if(empty($event_id) || empty($module))
        {
            $data = array(
              'success' => false,
              'message' => 'Invalid parameters',
            );

            echo json_encode($data);exit;
        }

        $this->db->where('module_name',$module);
        $this->db->where('event_id',$event_id);
        $date = $this->db->get('modules_update_log')->row_array()['updated_date'];

        $this->db->where('module_name','group');
        $this->db->where('event_id',$event_id);
        $group = $this->db->get('modules_update_log')->row_array()['updated_date'];
        $group = $group?:'';

        $data = array(
          'success' => true,
          'date' => ($date)?:$group,
          'module' => $module,
          'group' => $group
          );
        echo json_encode($data);
    }
}