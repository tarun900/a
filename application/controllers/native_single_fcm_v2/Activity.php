<?php 
require_once( APPPATH . "third_party/TwitterAPIExchange.php" ); 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single_fcm_v2/Activity_model');
        $this->load->model('native_single_fcm_v2/Social_model');
        $this->load->model('native_single_fcm_v2/App_login_model');
         $this->i = 0;  
    }

    /***Get Feeds Data **/

    public function getFeeds()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id'); // optional
        $page_no =$this->input->post('page_no');
        
        if($event_id!= '' && $page_no!= '')
        {
            $public_message_feeds = $this->Activity_model->getPublicMessageFeeds($event_id,$user_id);
            $photo_feed     = $this->Activity_model->getPhotoFeeds($event_id,$user_id);
            $check_in_feed  = $this->Activity_model->getCheckInFeeds($event_id,$user_id);
            $rating_feed    = $this->Activity_model->getRatingFeeds($event_id,$user_id);
            $user_feed      = $this->Activity_model->getUserFeeds($event_id,$user_id);
            $activity_feeds = $this->Activity_model->getActivityFeeds($event_id,$user_id);
            
            $data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds);

            function sortByTime($a, $b)
            {
                $a = $a['Time'];
                $b = $b['Time'];

                if ($a == $b)
                {
                    return 0;
                }
                return ($a > $b) ? -1 : 1;
            } 
            if(!empty($data))
            {
                usort($data, 'sortByTime');
                foreach ($data as $key => $value)
                {
                    $data[$key]['Time'] = $this->Activity_model->get_timeago(strtotime($value['Time']));
                }
            }  
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($data);
            $total_page     = ceil($total/$limit);
            $data           = array_slice($data,$start,$limit);
            foreach ($data as $key => $value)
            {
                $comments = $this->Activity_model->getComments($value['id'],$value['type'],$user_id); 
                $data[$key]['total_comments'] = count($comments);
                $data[$key]['comments'] = $comments;
            }
            $data = array(
                  'success'     => true,
                  'data'        => $data,
                  'total_page'  => $total_page,
                  'total'       =>$total,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }  

    /***Get Details of feeds **/

    public function getFeedDetails()
    {
        $type   = $this->input->post('type');
        $id     = $this->input->post('id');
        
        if($id!= '' && $type!= '')
        {
            if($type == "activity")
            {
                $feed_details           = $this->Activity_model->getFeedDetails($id);
                $image_array            = $feed_details->image;
                $feed_details->image    = json_decode($image_array);
            }

            $data = array(
                  'success' => true,
                  'data'    => ($feed_details) ? $feed_details : "",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }   

    /***Post update of feeds **/

    public function postUpdate()
    {
        $event_id   = $this->input->post('event_id');
        $token      = $this->input->post('token');
        $user_id    = $this->input->post('user_id');
        $message    = $this->input->post('message');

        if($event_id!='' && $message!='' && $token!='' && $user_id!='')
        {
            $this->load->model('DufourCoProductions/App_login_model');
            $user = $this->App_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                        )
                    );   
            } 
            else 
            {
                $message_data['message']    = $message;
                $message_data['sender_id']  = $user_id;
                $message_data['event_id']   = $event_id;
                $message_data['image']      = "";
                $message_data['time']       = date("Y-m-d H:i:s");

                $message_id = $this->Activity_model->savePublicPost($message_data);

                if($message_id!=0)
                {
                    $data = array(
                        'success'       => true,
                        'message'       => "Successfully Saved",
                        'message_id'    => $message_id,
                    );
                }
                else
                {
                    $data = array(
                        'success' => false,
                        'message' => "Something went wrong. Please try again.",
                    );
                }
            }
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***For Save Acitivity Images from feeds **/

    public function saveActivityImages()
    {
        $activity_id    = $this->input->post('activity_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            //copy("./assets/user_files/".$new_image_name,"./assets/user_files/thumbnail/".$new_image_name);
            $images         = $new_image_name;
            $message_data   = $this->Activity_model->getMessageDetails($activity_id);
            
            $update_data['image'] = $new_image_name;
            if($message_data->image!='')
            {
                $arr                    = json_decode($message_data->image);
                $arr[]                  = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->Activity_model->updateMessageImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['image']    = json_encode($arr);
                $this->Activity_model->updateMessageImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***For get Images List for feeds **/

    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $activity_id=$this->input->post('activity_id');
        if($event_id!='' && $activity_id!='')
        {
            $images = $this->Activity_model->getImages($event_id,$activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

     /***For get Feed Likes **/

    public function feedLike()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $where = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                );
            $like = $this->Activity_model->getFeedLike($where);
            if($like!='')
            {
                $update_data = array(
                    'like' => ($like == '1') ? '0'  :  '1',
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->Activity_model->updateFeedLike($update_data,$where);
            }
            else{
                $data = array(
                    'module_type' => $module_type,
                    'module_primary_id' => $module_primary_id,
                    'user_id' => $user_id,
                    'like' => ($like) ? 0  :  1,
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->Activity_model->makeFeedLike($data);
            }    
            $data = array(
                'success' => true,
                'message' => 'Successfully done.',
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***For get Feed comments **/

    public function feedComment()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
       
        if($module_type!='' && $module_primary_id!='' && $user_id!='' && $comment!='')
        {
            $data = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                );
            $id = $this->Activity_model->makeFeedComment($data);
              
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => $id,
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***For save Image feeeds comment **/

    public function saveCommentImages()
    {
        $activity_id    = $this->input->post('comment_id');

        if($activity_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
           
            $images         = $target_path;
            $message_data   = $this->Activity_model->getCommentDetails($activity_id);
            
            if($message_data->comment_image!='')
            {
                $arr                    = json_decode($message_data->comment_image);
                $arr[]                  = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->Activity_model->updateCommentImage($activity_id,$update_arr);
            }
            else
            {
                $arr[0]                 = $new_image_name;
                $update_arr['comment_image']    = json_encode($arr);
                $update_arr['datetime']    = date('Y:m:d H:i:s');
                $this->Activity_model->updateCommentImage($activity_id,$update_arr);
            }
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***For get Image feeeds comment **/

    public function getCommentImageList()
    {
        $activity_id=$this->input->post('comment_id');
        if($activity_id!='')
        {
            $images = $this->Activity_model->getCommentImages($activity_id);

            $data = array(
                'images' => (json_decode($images)) ? json_decode($images) : [],
                );
            $data = array(
                'success' => true,
                'data' => $data,
                );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***For delete feeeds comment **/

    public function deleteFeedComment()
    {
        $comment_id = $this->input->post('comment_id');

        if($comment_id!='')
        {
            $this->Activity_model->deleteComment($comment_id);
            $data = array(
                'success' => true,
                'message' => 'Comment deleted successfully',
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid parameters',
                );
        }
        echo json_encode($data);
    }

    /***Get Internal data in feeds Function  **/

    public function get_internal_data($event_id=NULL,$user_id=NULL)
    {
        $per = $this->Activity_model->getPermission($event_id);
        if(!empty($per))
        {
            $public_message_feeds = ($per['public']) ? $this->Activity_model->getPublicMessageFeeds_new($event_id,$user_id) : [];
            
            $photo_feed = ($per['photo']) ? $this->Activity_model->getPhotoFeeds_new($event_id,$user_id) : [];

            $check_in_feed = ($per['check_in']) ? $this->Activity_model->getCheckInFeeds_new($event_id,$user_id) : [];
            
            $rating_feed = ($per['rating']) ? $this->Activity_model->getRatingFeeds_new($event_id,$user_id) : [];
            
            $user_feed = ($per['update_profile']) ? $this->Activity_model->getUserFeeds_new($event_id,$user_id) : [];
            
            $arrSaveSession = ($per['show_session']) ? $this->Activity_model->getSaveSessionFeed($event_id,$user_id) : [];
           
            $arrauctionbid = ($per['auction_bids']) ? $this->Activity_model->getauctionbidsFeed($event_id,$user_id) : [];
            
            $arrcomments = ($per['comments']) ? $this->Activity_model->getcommentfeed($event_id,$user_id) : [];
            
            $activity_feeds = $this->Activity_model->getActivityFeeds_new($event_id,$user_id);
            $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
            
            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($activity_data, 'sortByOrder');
            $acc = $this->Activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];
            foreach ($activity_data as $key => $value)
            {
                $activity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$user_id);
                 $activity_data[$key]['likes']=$this->Activity_model->getLikes($value['id'],$value['type'],$user_id);
      
                $activity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $activity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $activity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $activity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $activity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $activity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $activity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $activity_data[$key]['timestamp'] = (strtotime($value['Time'])) ?: time();

                $activity_data[$key]['f_share_url'] = '';
                $activity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;;
                $activity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $activity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($activity_data[$key]['image']))
                {
                    $activity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $activity_data[$key]['is_alert'] = 0;
            }
            return $activity_data;
        }
        else
        {
            $activity_data = [];
            return $activity_data;
        }
    }

    /***Get Alert data in feeds Function old  **/

    public function get_alert_data($event_id=NULL,$user_id=NULL)
    {
        $per = $this->Activity_model->getPermission($event_id);
        if(!empty($per))
        {

            $orgpublic_message_feed = ($per['public']) ? $this->Activity_model->getPublicMessageFeeds_by_org($event_id,$user_id) : [];

            $orgphoto_feed = ($per['photo']) ? $this->Activity_model->getPhotoFeeds_by_org($event_id,$user_id) : [];
            
            $orgcheck_in_feed = ($per['check_in']) ? $this->Activity_model->getCheckInFeeds_by_org($event_id,$user_id) : [];

            $orgrating_feed = ($per['rating']) ? $this->Activity_model->getRatingFeeds_by_org($event_id,$user_id) : [];
            
            $orguser_feed = ($per['update_profile']) ? $this->Activity_model->getUserFeeds_by_org($event_id,$user_id) : [];
            
            $orgarrSaveSession = ($per['show_session']) ? $this->Activity_model->getSaveSessionFeed_by_org($event_id,$user_id) : [];
            
            $orgarrauctionbid = ($per['auction_bids']) ? $this->Activity_model->getauctionbidsFeed_by_org($event_id,$user_id) : [];

            $orgarrcomments = ($per['comments']) ? $this->Activity_model->getcommentfeed_by_org($event_id,$user_id) : [];
            
            $orgactivity_feeds = $this->Activity_model->getActivityFeeds_by_org($event_id,$user_id);
            $notify=$this->Activity_model->get_all_notification_by_event($event_id,$user_id);
            $orgactivity_data = array_merge($orgpublic_message_feed,$orgphoto_feed,$orgcheck_in_feed,$orgrating_feed,$orguser_feed,$orgactivity_feeds,$orgarrSaveSession,$orgarrauctionbid,$orgarrcomments,$notify);

            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($orgactivity_data, 'sortByOrder');            

            $acc = $this->Activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];

            foreach ($orgactivity_data as $key => $value) 
            {
                $orgactivity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$user_id);
                 $orgactivity_data[$key]['likes']=$this->Activity_model->getLikes($value['id'],$value['type'],$user_id);
                $orgactivity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $orgactivity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $orgactivity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $orgactivity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $orgactivity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $orgactivity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $orgactivity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $orgactivity_data[$key]['timestamp'] = (strtotime($value['Time']))?: time();
                $orgactivity_data[$key]['f_share_url'] = '';
                $orgactivity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;
                $orgactivity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $orgactivity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($orgactivity_data[$key]['image']))
                {
                    $orgactivity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $orgactivity_data[$key]['is_alert'] = 1;
            }
            return $orgactivity_data;
        }
        else
        {
            $orgactivity_data = [];
            return $orgactivity_data;
        }
    }

    /***Get Twitter data in feeds Function  **/

    public function get_twitter_data($event_id=NULL,$tw_next_url=NULL)
    {
        $per = $this->Activity_model->getPermission($event_id);    
        if(!empty($per['hashtag']))
        {
            $settings = array(
            'oauth_access_token' => "719145514269184002-S9X64BWU3vkyMARPsG22gnHTHQlQZmz",
            'oauth_access_token_secret' => "TfoPnJUm597UR26Tv4gCSXgx7AOKbyrsHQUuuuaOUoH0a",
            'consumer_key' => "nnUEflY3VSEErY225YWxAN5K0",
            'consumer_secret' => "ZyFJkhzEIhnicJq8CIS4aIwoH42JQHTO7oWyV4kYwzfW4dsdT2"
            );
            $url = 'https://api.twitter.com/1.1/search/tweets.json';
            if(!empty($tw_next_url))
            {   
                $getfield = $tw_next_url;
            }
            else
            {
                $getfield = '?q=#'.$per['hashtag'].'&count=5';
            }

            if($tw_next_url == '1')
            {
                $tweets['statuses'] = [];
            }
            else
            {
                $requestMethod = 'GET';
                $twitter = new TwitterAPIExchange($settings);
                $tweest = $twitter->setGetfield($getfield)
                                 ->buildOauth($url, $requestMethod)
                                 ->performRequest();
                $tweets = json_decode($tweest,true);
            } 
            foreach ($tweets['statuses'] as $key => $value)
            {   
                $tweets['statuses'][$key]['text'] = htmlspecialchars_decode($value['text']);
                $tweets['statuses'][$key]['created_at'] = $this->get_timeago($value['created_at']);
                $tweets['statuses'][$key]['timestamp'] = strtotime($value['created_at']);
                $tweets['statuses'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['t_share_url'] = "http://twitter.com/home?status=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                $tweets['statuses'][$key]['url'] = "https://twitter.com/".$value['user']['screen_name']."/status/".$value['id_str'];
                if(array_key_exists('extended_entities',$value))
                {
                    foreach($value['extended_entities']['media'] as $k => $v)
                    {
                        $tweets['statuses'][$key]['media'][] = $v['media_url'];
                    }
                    unset($tweets['statuses'][$key]['extended_entities']);
                }
                else
                {
                    $tweets['statuses'][$key]['media'] = [];
                }
            }
        }
        if(!empty($tweets['statuses']))
        {
            $twitter = $tweets;
        }
        else
        {
            $twitter = new stdClass();
            $twitter->statuses = [];
            $twitter->search_metadata = new stdClass();
        }
        return $twitter;
    }

    /***Get Facebook data in feeds Function  **/

    public function get_facebook_data($event_id=NULL,$fb_next_url=NULL)
    {   
        $per = $this->Activity_model->getPermission($event_id);
        if(!empty($per['fbpage_name']))
        {
            $profile_id=$per['fbpage_name'];
            $app_id = "206032402939985";
            $app_secret = "cca7ba36c196a2c377e1e481f84e8309";
            $authToken = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
            if(!empty($fb_next_url))
            {
                if($fb_next_url == '1')
                {
                    $facebook_feeds['data'] = [];                    
                }
                else
                {
                    $json_object = file_get_contents($fb_next_url); 
                    $facebook_feeds=json_decode($json_object,true);
                }
            }
            else
            {   
                $json_object = file_get_contents("https://graph.facebook.com/{$profile_id}/feed?".http_build_query(json_decode($authToken))."&summary=1&fields=full_picture,message,link,name,likes,comments,description,from,caption,attachments,created_time&limit=5");
                $facebook_feeds=json_decode($json_object,true);
            }
            foreach ($facebook_feeds['data'] as $key => $value) 
            {   
                if(!empty($value['from']))
                {   
                    $facebook_feeds['data'][$key]['logo'] = "https://graph.facebook.com/$profile_id/picture?type=large";
                    $facebook_feeds['data'][$key]['created_time'] = $this->get_timeago($value['created_time']);
                    $facebook_feeds['data'][$key]['timestamp'] = strtotime($value['created_time']);
                    $tmp = explode('_',$value['id']);
                    
                    $facebook_feeds['data'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                    $facebook_feeds['data'][$key]['t_share_url'] = "http://twitter.com/home?status=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                    $facebook_feeds['data'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                    $facebook_feeds['data'][$key]['url'] = "https://www.facebook.com/".str_replace('_','/posts/',$value['id']);
                    if(array_key_exists('attachments', $value))
                    {
                        if(array_key_exists('subattachments',$value['attachments']['data'][0]))
                        {
                            foreach ($value['attachments']['data'][0]['subattachments']['data'] as $key1 => $value1) 
                            {
                                foreach ($value1['media'] as $key11 => $value11)
                                {
                                    $facebook_feeds['data'][$key]['attachments']['media'][] = $value11;
                                }
                                unset($facebook_feeds['data'][$key]['attachments']['data']);
                            }
                        }
                        else
                        {
                            if(array_key_exists('image',$value['attachments']['data'][0]['media']))
                            {
                                $facebook_feeds['data'][$key]['attachments']['media'][] = $value['attachments']['data'][0]['media']['image'];
                                unset($facebook_feeds['data'][$key]['attachments']['data']);
                            }
                            else
                            {
                                $facebook_feeds['data'][$key]['attachments']['media'] = [];
                                unset($facebook_feeds['data'][$key]['attachments']['data']);
                            }
                        }
                    }
                    else
                    {
                        $facebook_feeds['data'][$key]['attachments']['media'] = [];
                    }
                    if(array_key_exists('comments',$value))
                    {
                        foreach ($value['comments']['data'] as $k => $v)
                        {
                            $facebook_feeds['data'][$key]['comments']['data'][$k]['from']['logo'] = "https://graph.facebook.com/".$v['from']['id']."/picture?type=large";
                        }
                    }
                    else
                    {
                        $facebook_feeds['data'][$key]['comments'] = new stdClass();
                    }
                }
                else
                {
                    unset($facebook_feeds['data'][$key]);
                }
            }
        }

        
        if(!empty($facebook_feeds['data']))
        {   
            $facebook['data'] = array_values($facebook_feeds['data']);
            $facebook['paging'] = $facebook_feeds['paging'];
        }
        else
        {
            $facebook = new stdClass();
        }
        return $facebook;
    }

    /***Get Instagram data in feeds Function  **/

    public function get_instagram_data($event_id=NULL,$insta_next_url=NULL)
    {
        $access_token=$this->Activity_model->get_instagram_access_token($event_id);
        if(!empty($access_token))
        {
            $photo_count=5;     
            if(!empty($insta_next_url))
            {
                if($insta_next_url == '1')
                {
                    $instagram_feed['data'] = [];                    
                }
                else
                {
                    $json_link=$insta_next_url;
                }
            }
            else
            {
                $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
                $json_link.="access_token={$access_token}&count={$photo_count}";
            }
            $json = file_get_contents($json_link);
            $instagram_feed = json_decode($json,TRUE);
            foreach ($instagram_feed['data'] as $key => $value) {
                $instagram_feed['data'][$key]['created_time'] = $this->get_timeago(date("Y-m-d H:i:s",$value['created_time']));
                $instagram_feed['data'][$key]['timestamp'] = strtotime(date("Y-m-d H:i:s",$value['created_time']));
                $instagram_feed['data'][$key]['f_share_url'] = "https://www.facebook.com/sharer/sharer.php?u=".$value['link'];
                $instagram_feed['data'][$key]['t_share_url'] = "http://twitter.com/home?status=".$value['link'];
                $instagram_feed['data'][$key]['l_share_url'] = "https://www.linkedin.com/shareArticle?mini=true&url=".$value['link'];
                $instagram_feed['pagination'] = ($instagram_feed['pagination'])?: new stdClass();
            }
        }
        return ($instagram_feed)?: new stdClass();
    }

    /***Get All data in feeds Function  **/

    public function get_all()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        $fb_next_url = $this->input->post('fb_next_url');
        $tw_next_url = $this->input->post('tw_next_url');
        $insta_next_url = $this->input->post('insta_next_url');

        if($event_id!='' && $user_id!='')
        {
            $per = $this->Activity_model->getPermission($event_id);
            
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            if($this->input->post('test'))
                $activity_data = $this->get_internal_data_UHG_test($event_id,$user_id,$org_id,1);
            elseif($org_id == '20152')
                $activity_data = $this->get_internal_data_UHG($event_id,$user_id,$org_id);
            else
                $activity_data = $this->get_internal_data($event_id,$user_id);

            $orgactivity_data = $this->get_alert_data($event_id,$user_id);
            
            
            
            $limit          = $org_id == '20152'? 20:5;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $activity_data  = array_slice($activity_data,$start,$limit);
            $orgactivity_data  = array_slice($orgactivity_data,$start,$limit);

            if($org_id == '20152')
            {
                $twitter = new stdClass();
                $twitter->statuses = [];
                $twitter->search_metadata = new stdClass();
                $facebook = new stdClass();
                $instagram =  new stdClass();
            }
            else
            {
                $twitter = $this->get_twitter_data($event_id,$tw_next_url);
                $facebook = $this->get_facebook_data($event_id,$fb_next_url);
                $instagram =$this->get_instagram_data($event_id,$insta_next_url);
            }


            
            foreach ($activity_data as $key => $value)
            {
                if(!empty($value['image']))
                {   
                    $activity_data[$key]['image'] = array_values($this->compress_image($value['image'],$event_id));
                }
                if(!empty($value['comments']))
                {
                    foreach ($value['comments'] as $key1 => $value1)
                    {
                        if(!empty($value1['comment_image']))
                        {
                            $activity_data[$key]['comments'][$key1]['comment_image'] = array_values($this->compress_image($value1['comment_image'],$event_id));
                        }
                    }
                }
            }
            

            $data = array(
                    'all'      => $activity_data,
                    'alerts'   => $orgactivity_data,
                    'facebook' => $facebook,
                    'twitter'  => $twitter,
                    'instagram'=> $instagram
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Internal data in feeds Function  **/

    public function get_internal()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='' && $user_id!='')
        {
            $activity_data = $this->get_internal_data($event_id,$user_id);

            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($activity_data);
            $total_page     = ceil($total/$limit);
            $activity_data  = array_slice($activity_data,$start,$limit);

            
            foreach ($activity_data as $key => $value)
            {
                if(!empty($value['image']))
                {   
                    $activity_data[$key]['image'] = array_values($this->compress_image($value['image'],$event_id));
                }
                if(!empty($value['comments']))
                {
                    foreach ($value['comments'] as $key1 => $value1)
                    {
                        if(!empty($value1['comment_image']))
                        {
                            $activity_data[$key]['comments'][$key1]['comment_image'] = array_values($this->compress_image($value1['comment_image'],$event_id));
                        }
                    }
                }
            }

            
            $data = array(
                    'internal'      => $activity_data,
                    'total_page' => $total_page,
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Alert data in feeds Function  **/

    public function get_alert()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $page_no = $this->input->post('page_no');
        
        if($event_id!='' && $user_id!='')
        {   
            $orgactivity_data = $this->get_alert_data($event_id,$user_id);
            $limit          = 10;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $total          = count($orgactivity_data);
            $total_page     = ceil($total/$limit);
            $orgactivity_data  = array_slice($orgactivity_data,$start,$limit);

            foreach ($orgactivity_data as $key => $value)
            {
                if(!empty($value['image']))
                {   
                    $orgactivity_data[$key]['image'] = array_values($this->compress_image($value['image'],$event_id));
                }
                if(!empty($value['comments']))
                {
                    foreach ($value['comments'] as $key1 => $value1)
                    {
                        if(!empty($value1['comment_image']))
                        {
                            $activity_data[$key]['comments'][$key1]['comment_image'] = array_values($this->compress_image($value1['comment_image'],$event_id));
                        }
                    }
                }
            }

            $data = array(
                    'internal'   => $orgactivity_data,
                    'total_page' => $total_page,
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Social feeds  **/

    public function get_social()
    {   
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $fb_next_url = $this->input->post('fb_next_url');
        $tw_next_url = $this->input->post('tw_next_url');

        if($event_id!='' && $user_id!='')
        {
            $twitter = $this->get_twitter_data($event_id,$tw_next_url);
            $facebook = $this->get_facebook_data($event_id,$fb_next_url);
            $instagram =$this->get_instagram_data($event_id,$insta_next_url);
            $data = array(
                    'facebook' => $facebook,
                    'twitter'  => $twitter,
                    'instagram'=> $instagram
                    );

            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Time of feeds Function  **/

    public function get_timeago($ptime=NULL)
    {    
        $estimate_time = time() - strtotime($ptime);
        if ($estimate_time < 1)
        {
           return '1 second ago';
        }
        $condition = array(
              12 * 30 * 24 * 60 * 60 => 'year',
              30 * 24 * 60 * 60 => 'month',
              24 * 60 * 60 => 'day',
              60 * 60 => 'hour',
              60 => 'minute',
              1 => 'second'
        );
        foreach ($condition as $secs => $str)
        {
            $d = $estimate_time / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
        $user_id=$this->input->post('user_id');
    }

    /***Get Feed Likes Latest Function  **/

    public function feedLikeNew()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $event_id=$this->input->post('event_id');
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $where = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'event_id' => $event_id,
                );
            $like = $this->Activity_model->getFeedLike($where);
            if($like!='')
            {
                $update_data = array(
                    'like' => ($like == '1') ? '0'  :  '1',
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->Activity_model->updateFeedLike($update_data,$where);
            }
            else
            {
                $data = array(
                    'module_type' => $module_type,
                    'module_primary_id' => $module_primary_id,
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'like' => ($like) ? 0  :  1,
                    'datetime' => date('Y-m-d H:i:s'),
                    );
                $this->Activity_model->makeFeedLike($data);
            }    
            $data = array(
                'success' => true,
                'message' => 'Successfully done.',
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***Feed commenrs Latest Function  **/

    public function feedCommentNew()
    {
        $module_type=$this->input->post('module_type');
        $module_primary_id=$this->input->post('id');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $event_id = $this->input->post('event_id');
        
        if($module_type!='' && $module_primary_id!='' && $user_id!='')
        {
            $data = array(
                'module_type' => $module_type,
                'module_primary_id' => $module_primary_id,
                'user_id' => $user_id,
                'comment' => $comment,
                'datetime' => date('Y-m-d H:i:s'),
                'event_id' => $event_id,
                );
            if(!empty($_FILES['image']))
            {
                $new_image_name = round(microtime(true) * 1000).".jpeg";
                $target_path    = "././assets/user_files/".$new_image_name;              
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                $images         = $target_path;
                $arr[0]                 = $new_image_name;
                $data['comment_image']    = json_encode($arr);
            }

            $id = $this->Activity_model->makeFeedComment($data);
            $data = array(
                'success' => true,
                'message' => 'Comment added successfully.',
                'comment_id' => $id,
            );  
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
                );
        }
        echo json_encode($data);
    }

    /***Add Photo Filter Image  **/

    public function add_photo_filter_image()
    {
        $event_id = $this->input->post('event_id');
        $sender_id = $this->input->post('user_id');

        if($event_id!='' && $sender_id!='')
        {
            $new_image_name = round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/user_files/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            
            $arr[0]              = $new_image_name;
            $insert['image']     = json_encode($arr);
            $insert['message']   = "";
            $insert['event_id']  = $event_id;
            $insert['sender_id'] = $sender_id;
            $insert['time']      = date('Y-m-d H:i:s'); 
            $this->Activity_model->savePublicPost($insert);
            
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }


     public function add_photo_filter_multi_image()
    {
        $event_id = $this->input->post('event_id');
        $sender_id = $this->input->post('user_id');
        $images = json_decode($this->input->post('image'));
                        
        if($event_id!='' && $sender_id!='' && $images!='')
        {
            foreach ($images as $key => $file) {
                $new_image_name = rand().round(microtime(true) * 1000).".jpeg";
                $target_path    = "././assets/user_files/".$new_image_name;   
                copy($file,$target_path);
                $arr[]              = $new_image_name;
            }
            $insert['image']     = json_encode($arr);
            $insert['message']   = "";
            $insert['event_id']  = $event_id;
            $insert['sender_id'] = $sender_id;
            $insert['time']      = date('Y-m-d H:i:s'); 
            $this->Activity_model->savePublicPost($insert);
            
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
            );
        }else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Photo Filter URL  **/

    public function get_photofilter_url()
    {
        $event_id = $this->input->post('event_id');
        $sender_id = $this->input->post('user_id');

        if($event_id!='' && $sender_id!='')
        {
            $new_image_name = $event_id.'-'.$sender_id.'-'.round(microtime(true) * 1000).".jpeg";
            $target_path    = "././assets/images/linkedin_share/".$new_image_name;              
            move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
            $data = array(
                  'success' => true,
                  'message' => 'Successfully uploaded',
                  'url'     => base_url().'assets/images/linkedin_share/'.$new_image_name 
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    /***Get Internal feeds for UHG Native App **/

    public function get_internal_data_UHG($event_id=NULL,$user_id=NULL,$org_id=NULL)
    {
        $per = $this->Activity_model->getPermission($event_id);
        $event_ids = $this->App_login_model->getEventsByOrg($org_id);

        if(!empty($per))
        {
            $public_message_feeds = ($per['public']) ? $this->Activity_model->getPublicMessageFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $photo_feed = ($per['photo']) ? $this->Activity_model->getPhotoFeeds_UHG($event_ids,$user_id,$org_id) : [];

            $check_in_feed = ($per['check_in']) ? $this->Activity_model->getCheckInFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $rating_feed = ($per['rating']) ? $this->Activity_model->getRatingFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $user_feed = ($per['update_profile']) ? $this->Activity_model->getUserFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $arrSaveSession = ($per['show_session']) ? $this->Activity_model->getSaveSessionFeedUHG($event_ids,$user_id,$org_id) : [];
           
            $arrauctionbid = ($per['auction_bids']) ? $this->Activity_model->getauctionbidsFeedUHG($event_ids,$user_id,$org_id) : [];
            
            $arrcomments = ($per['comments']) ? $this->Activity_model->getcommentfeedUHG($event_ids,$user_id,$org_id) : [];
            
            $activity_feeds = $this->Activity_model->getActivityFeeds_UHG($event_ids,$user_id,$org_id);
            $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
            
            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($activity_data, 'sortByOrder');
            $acc = $this->Activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];
            foreach ($activity_data as $key => $value)
            {
                $activity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$user_id);
                $activity_data[$key]['likes']=$this->Activity_model->getLikes($value['id'],$value['type'],$user_id);
                $activity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $activity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $activity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $activity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $activity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $activity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $activity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $activity_data[$key]['timestamp'] = (strtotime($value['Time'])) ?: time();

                $activity_data[$key]['f_share_url'] = '';
                $activity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;;
                $activity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $activity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($activity_data[$key]['image']))
                {
                    $activity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $activity_data[$key]['is_alert'] = 0;
            }
            return $activity_data;
        }
        else
        {
            $activity_data = [];
            return $activity_data;
        }
    }

     /***To Compress Images of feeds **/

    public function compress_image($data=NULL,$event_id=NULL)
    {   
        if(is_array($data))
        {
            foreach ($data as $key => $value) 
            {   
                if(!empty($value))
                {
                    $source_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$value;
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value;
                    if (!file_exists($_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id)) {
                        mkdir($_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id, 0777, true);
                    }
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id.'/'.$new_name;

                    if(!file_exists($destination_url))
                    {
                        if ($info['mime'] == 'image/jpeg')
                        {   
                            $quality = 50;
                            $image = imagecreatefromjpeg($source_url);
                            imagejpeg($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/gif')
                        {   
                            $quality = 5;
                            $image = imagecreatefromgif($source_url);
                            imagegif($image, $destination_url, $quality);

                        }
                        elseif ($info['mime'] == 'image/png')
                        {   
                            $quality = 5;
                            $image = imagecreatefrompng($source_url);
                            $background = imagecolorallocatealpha($image,255,0,255,127);
                            imagecolortransparent($image, $background);
                            imagealphablending($image, false);
                            imagesavealpha($image, true);
                            imagepng($image, $destination_url, $quality);
                        }
                    }
                    $new_data[] = 'activity/'.$event_id.'/'.$new_name;
                }
            }
            return $new_data;
        }
        elseif(!empty($data))
        {
            $source_url = $_SERVER['DOCUMENT_ROOT']."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 50;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);
                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
            return $new_name;
        }
    }
    public function get_internal_data_UHG_test($event_id=NULL,$user_id=NULL,$org_id=NULL,$test=0)
    {
        $per = $this->Activity_model->getPermission($event_id);
        $event_ids = $this->App_login_model->getEventsByOrg($org_id);

        if(!empty($per))
        {
            $public_message_feeds = ($per['public']) ? $this->Activity_model->getPublicMessageFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $photo_feed = ($per['photo']) ? $this->Activity_model->getPhotoFeeds_UHG($event_ids,$user_id,$org_id) : [];

            $check_in_feed = ($per['check_in']) ? $this->Activity_model->getCheckInFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $rating_feed = ($per['rating']) ? $this->Activity_model->getRatingFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $user_feed = ($per['update_profile']) ? $this->Activity_model->getUserFeeds_UHG($event_ids,$user_id,$org_id) : [];
            
            $arrSaveSession = ($per['show_session']) ? $this->Activity_model->getSaveSessionFeedUHG($event_ids,$user_id,$org_id) : [];
           
            $arrauctionbid = ($per['auction_bids']) ? $this->Activity_model->getauctionbidsFeedUHG($event_ids,$user_id,$org_id) : [];
            
            $arrcomments = ($per['comments']) ? $this->Activity_model->getcommentfeedUHG($event_ids,$user_id,$org_id) : [];
            
            $activity_feeds = $this->Activity_model->getActivityFeeds_UHG($event_ids,$user_id,$org_id);
            $activity_data = array_merge($public_message_feeds,$photo_feed,$check_in_feed,$rating_feed,$user_feed,$activity_feeds,$arrSaveSession,$arrauctionbid,$arrcomments);
            
            if(!function_exists('sortByOrder'))
            {
                function sortByOrder($a, $b)
                {
                    return strtotime($b['Time']) - strtotime($a['Time']);
                }
            }
            usort($activity_data, 'sortByOrder');
            $acc = $this->Activity_model->get_acc_name($event_id);
            $acc_name = $acc['acc_name'];
            $subdomain = $acc['Subdomain'];
            foreach ($activity_data as $key => $value)
            {
                $activity_data[$key]['comments']=$this->Activity_model->getComments($value['id'],$value['type'],$user_id);
                $activity_data[$key]['likes']=$this->Activity_model->getLikes($value['id'],$value['type'],$user_id);
                $activity_data[$key]['image'] = (array_key_exists('image',$value) && !empty($value['image'])) ? json_decode($value['image']) : [];
                $activity_data[$key]['agenda_id'] = (array_key_exists('agenda_id',$value)) ? $value['agenda_id'] : '';
                $activity_data[$key]['rating'] = (array_key_exists('rating',$value) && !empty($value['rating'])) ? $value['rating'] : '';
                $activity_data[$key]['like_count'] = (array_key_exists('like_count',$value) && !empty($value['like_count'])) ? $value['like_count'] : '0';
                $activity_data[$key]['is_like'] = (array_key_exists('is_like',$value) && !empty($value['is_like'])) ? $value['is_like'] : '0';
                $activity_data[$key]['agenda_time'] = (array_key_exists('agenda_time',$value) && !empty($value['agenda_time'])) ? $value['agenda_time'] : '';
                $activity_data[$key]['Time'] = $this->get_timeago($value['Time']);
                $activity_data[$key]['timestamp'] = (strtotime($value['Time'])) ?: time();

                $activity_data[$key]['f_share_url'] = '';
                $activity_data[$key]['url'] = base_url().'activity/'.$acc_name.'/'.$subdomain;
                $tmp['status'] = "“".$value['Message']."” ".base_url()."activity/".$acc_name."/".$subdomain;;
                $activity_data[$key]['t_share_url'] = "http://twitter.com/home?".http_build_query($tmp);
                $tmpp['summary'] = $value['Message'];
                $activity_data[$key]['l_share_url'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.base_url().'activity/'.$acc_name.'/'.$subdomain.'&title='.ucfirst($value['activity']).'&'.http_build_query($tmpp);
                if(!empty($activity_data[$key]['image']))
                {
                    $activity_data[$key]['l_share_url'] .= '&source='.base_url().'assets/user_files/'.$orgactivity_data[$key]['image'][0];
                }
                $activity_data[$key]['is_alert'] = 0;
                $activity_data[$key]['date_time'] = date('Y-m-d h:i:s',strtotime($value['Time']));
            }
            // if($test)
            //     j($activity_data);
            // else
            return $activity_data;
        }
        else
        {
            $activity_data = [];
            return $activity_data;
        }
    }
   

    public function deletePost()
    {
        extract($this->input->post());
        if(!empty($activity_id) && !empty($user_id) && !empty($event_id) && !empty($activity_no))
        {
            $this->Activity_model->deletePost($activity_id,$activity_no);
            $data = array(
                'success' => true,
                'message' => 'Post deleted successfully.'
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
    public function getLikes()
    {
        extract($this->input->post());
        if(!empty($event_id) && !empty($id) && !empty($module_type))
        {
            $data['likes'] = $this->Activity_model->getLikes($id,$module_type);
            $data['comments'] = $this->Activity_model->getComments($id,$module_type);
            $data = array(
                'success' => true,
                'data' => $data
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }

    public function get_all_v2()
    {   
        extract($this->input->post());

        if($event_id!='' && $user_id!='')
        {
            $per = $this->Activity_model->getPermission($event_id);
            
            $org_id = $this->App_login_model->getOrganizerByEvent($event_id);
            if($this->input->post('test'))
                $activity_data = $this->get_internal_data_UHG_test($event_id,$user_id,$org_id,1);
            elseif($org_id == '20152')
                $activity_data = $this->get_internal_data_UHG($event_id,$user_id,$org_id);
            else
                $activity_data = $this->get_internal_data($event_id,$user_id);

            $orgactivity_data = $this->get_alert_data($event_id,$user_id);
            
            
            
            $limit          = $org_id == '20152' ? 20:5;
            $page_no        = (!empty($page_no))?$page_no:1;
            $start          = ($page_no-1)*$limit;

            $activity_data  = array_slice($activity_data,$start,$limit);
            $orgactivity_data  = array_slice($orgactivity_data,$start,$limit);

            if($org_id == '20152')
            {
                $twitter = new stdClass();
                $twitter->statuses = [];
                $twitter->search_metadata = new stdClass();
                $facebook = new stdClass();
                $instagram =  new stdClass();
            }
            else
            {
                $twitter = $this->get_twitter_data($event_id,$tw_next_url);
                $facebook = $this->get_facebook_data($event_id,$fb_next_url);
                $instagram =$this->get_instagram_data($event_id,$insta_next_url);
            }

            foreach ($activity_data as $key => $value)
            {   
                $new['id'] = $value['id'];
                $new['type'] = $value['type'];
                $new['post_type'] = '1';//internal
                $new['activity_no'] = $value['activity_no'];
                $new['logo'] = $value['Logo'] ? base_url().'assets/user_files/'.$value['Logo'] : '';

                $tmp1[] = $value['Firstname'];
                $tmp1[] = $value['Lastname'];
                $new['name'] = implode(' ',$tmp1);
                unset($tmp1);

                $tmp2[] = $value['Title'];
                $tmp2[] = $value['Company_name'];
                $new['sub_title'] = implode(', ',array_filter($tmp2));
                unset($tmp2);

                $new['time'] = $value['Time'];
                $new['message'] = $value['Message']?:$value['activity'];


                if(!empty($value['image']))
                {   
                    $new['image'] = array_values($this->compress_image_v2($value['image'],$event_id));
                }
                else
                {
                    $new['image'] = [];
                }

                $new['is_like'] = $value['is_like'];
                $new['likes'] = $value['likes'];
                foreach ($new['likes'] as $key1 => $value1)
                {
                    $new['likes'][$key1]['logo'] = $value1['Logo'] ? base_url().'assets/user_files/'.$value1['Logo'] : '';
                }

                $new['like_count'] = $value['like_count'];

                if(!empty($value['comments']))
                {   
                    $new['comments'] = $value['comments'];
                    foreach ($new['comments'] as $key1 => $value1)
                    {   
                        if(!empty($value1['logo']))
                            $new['comments'][$key1]['logo'] = base_url().'assets/user_files/'.$value1['logo'];
                        if(!empty($value1['comment_image']))
                        {   
                            $new['comments'][$key1]['comment_image'] = array_values($this->compress_image_v2($value1['comment_image'],$event_id));
                        }
                    }
                }
                else
                {
                    $new['comments'] = [];
                }
                $new['user_id'] = $value['user_id'];
                $new['role_id'] = $value['Role_id'];

                $new['advert_image'] = "";
                $new['advert_url'] = "";

                $new['survey_question'] = "";
                $new['survey_options'] = [];
                $new['survey_result'] = [];
                $new['show_result'] = "";
                $new['ans_submitted'] = '0';
                $new['result_message'] = 'Thanks';

                $tmp[] = $new;
                unset($new);
            }

            foreach ($orgactivity_data as $key => $value)
            {   
                $new['id'] = $value['id'];
                $new['type'] = $value['type'];
                $new['post_type'] = '2';//alert
                $new['activity_no'] = $value['activity_no'];
                $new['logo'] = $value['Logo'] ? base_url().'assets/user_files/'.$value['Logo'] : '';


                $tmp1[] = $value['Firstname'];
                $tmp1[] = $value['Lastname'];
                $new['name'] = implode(' ',$tmp1);
                unset($tmp1);

                $tmp2[] = $value['Title'];
                $tmp2[] = $value['Company_name'];
                $new['sub_title'] = implode(', ',array_filter($tmp2));
                unset($tmp2);

                $new['time'] = $value['Time'];
                $new['message'] = $value['Message']?:$value['activity'];


                if(!empty($value['image']))
                {   
                    $new['image'] = array_values($this->compress_image_v2($value['image'],$event_id));
                }
                else
                {
                    $new['image'] = [];
                }

                $new['is_like'] = $value['is_like'];
                $new['likes'] = $value['likes'];
                foreach ($new['likes'] as $key1 => $value1)
                {
                    $new['likes'][$key1]['logo'] = $value1['Logo'] ? base_url().'assets/user_files/'.$value1['Logo'] : '';
                }
                $new['like_count'] = $value['like_count'];

                if(!empty($value['comments']))
                {   
                    $new['comments'] = $value['comments'];
                    foreach ($new['comments'] as $key1 => $value1)
                    {   
                        if(!empty($value1['logo']))
                            $new['comments'][$key1]['logo'] = base_url().'assets/user_files/'.$value1['logo'];
                        if(!empty($value1['comment_image']))
                        {   
                            $new['comments'][$key1]['comment_image'] = array_values($this->compress_image_v2($value1['comment_image'],$event_id));
                        }
                    }
                }
                else
                {
                    $new['comments'] = [];
                }
                $new['user_id'] = $value['user_id'];
                $new['role_id'] = $value['Role_id'];

                $new['advert_image'] = "";
                $new['advert_url'] = "";

                $new['survey_question'] = "";
                $new['survey_options'] = [];
                $new['survey_result'] = [];
                $new['show_result'] = "";
                $new['ans_submitted'] = '0';
                $new['result_message'] = 'Thanks';


                $tmp[] = $new;
                unset($new);
            }

            
            if(array_key_exists('data',$facebook))
            {
                foreach ($facebook['data'] as $key => $value)
                {   
                    $new['id'] = $value['id'];
                    $new['type'] = '';
                    $new['post_type'] = '3';//facebook
                    $new['activity_no'] = '';
                    $new['logo'] = $value['logo'];
                    $new['name'] = $value['from']['name'];
                    $new['sub_title'] = '';
                    $new['time'] = $value['created_time'];
                    $new['message'] = $value['message'];
                    $new['image'] = array_column_1($value['attachments']['media'],'src');
                    $new['is_like'] = '';
                    $new['like_count'] = count($value['likes']['data']);
                    $new['likes'] = [];
                    $new['comments'] = [];
                    $new['user_id'] = '';
                    $new['role_id'] = '';

                    $new['advert_image'] = '';
                    $new['advert_url'] = '';

                    $new['survey_question'] = "";
                    $new['survey_options'] = [];
                    $new['survey_result'] = [];
                    $new['show_result'] = "";
                    $new['ans_submitted'] = '0';
                    $new['result_message'] = 'Thanks';

                    $tmp[] = $new;
                    unset($new);
                }
            }

            if(!is_object($twitter))
            {   
                foreach ($twitter['statuses'] as $key => $value)
                {   
                    $new['id'] = $value['id'];
                    $new['type'] = '';
                    $new['post_type'] = '4';//twitter
                    $new['activity_no'] = '';
                    $new['logo'] = $value['user']['profile_image_url'];
                    $new['name'] = $value['user']['name'];
                    $new['sub_title'] = '';
                    $new['time'] = $value['created_at'];
                    $new['message'] = $value['text'];
                    $new['image'] = $value['media'];
                    $new['is_like'] = '';
                    $new['like_count'] = $value['favorite_count'];
                    $new['likes'] = [];
                    $new['comments'] = [];
                    $new['user_id'] = '';
                    $new['role_id'] = '';

                    $new['advert_image'] = "";
                    $new['advert_url'] = "";

                    $new['survey_question'] = "";
                    $new['survey_options'] = [];
                    $new['survey_result'] = [];
                    $new['show_result'] = "";

                    $tmp[] = $new;
                    unset($new);
                }
            }
            $advert_tmp = $this->Activity_model->getAdevert($event_id);
            foreach ($advert_tmp as $key => $value)
            {   
                $new['id'] = 'adv-'.$value['id'];
                $new['type'] = '';
                $new['post_type'] = '5';//advert
                $new['activity_no'] = '';
                $new['logo'] = '';
                $new['name'] = '';
                $new['sub_title'] = '';
                $new['time'] = '';
                $new['message'] = '';
                $new['image'] = [];
                $new['is_like'] = '';
                $new['like_count'] = '';
                $new['likes'] = [];
                $new['comments'] = [];
                $new['user_id'] = '';
                $new['role_id'] = '';

                $new['advert_image'] = base_url().'assets/activity_advert/'.$event_id.'/'.$value['image'];
                $new['advert_url'] = $value['url'];

                $new['survey_question'] = "";
                $new['survey_options'] = [];
                $new['survey_result'] = [];
                $new['show_result'] = "";
                $new['ans_submitted'] = '0';
                $new['result_message'] = 'Thanks';

                $advert[] = $new;
                unset($new);
            }

            $survey_tmp = $this->Activity_model->getSurvey($event_id);
            foreach ($survey_tmp as $key => $value)
            {   
                $new['id'] = 'sur-'.$value['Id'];
                $new['type'] = '';
                $new['post_type'] = '6';//surevy
                $new['activity_no'] = '';
                $new['logo'] = '';
                $new['name'] = '';
                $new['sub_title'] = '';
                $new['time'] = '';
                $new['message'] = '';
                $new['image'] = [];
                $new['is_like'] = '';
                $new['like_count'] = '';
                $new['likes'] = [];
                $new['comments'] = [];
                $new['user_id'] = '';
                $new['role_id'] = '';

                $new['advert_image'] = '';
                $new['advert_url'] = '';

                $new['survey_question'] = $value['Question'];
                $new['survey_options'] = json_decode($value['Option'],true);
                $new['show_result'] = "";

                $survey_answer_chart = $this->Activity_model->getSurveyAns($value['Id']);
                $new['ans_submitted'] = in_array($user_id,array_column_1($survey_answer_chart,'puserid')) ? '1' :'0';
                $new['result_message'] = 'Thanks'; 
                if($survey_answer_chart[0]['publish_result'] == '1' || $survey_answer_chart['publish_date'] >= date('Y-m-d H:i:s'))
                {
                    $final_Array = array();
                    foreach($survey_answer_chart as $key => $value)
                    {
                        $final_Array[$value['Question']][$value['panswer']][] = array(
                            'id' => $value['puserid'],
                            'Firstname' => $value['Firstname'],
                            'Lastname' => $value['Lastname'],
                            'Email' => $value['Email']
                        );
                    }
                    foreach($final_Array as $key => $value)
                    {
                        foreach($value as $i => $j)
                        {
                            $final_Array[$key][$i] = sizeof($j);
                        }
                    }
                    $chart_result = [];
                    $color_arr = array('#109618','#ff9900','#3366cc','#dc3912','#800000','#FF0000','#C14C33','#593027','#96944C','#156A15','#3B2129','#54073E','#5711A7','#093447','#0C3F33');
                    
                    foreach($final_Array as $key => $value)
                    {
                        $chart['question'] = $key;
                        $i = 0;
                        foreach($value as $key1 => $value1)
                        {
                            $ans['answer_key'] = (string)$key1;
                            $ans['answer_value'] = (string)$value1;
                            $ans['color'] = $color_arr[$i];
                            $chart['answer'][] = $ans;
                            $i++;
                        }
                        $chart_result = $chart;
                    }
                }
                $new['survey_result'] = $chart_result['answer']?:[];
                unset($chart_result['answer']);
                $survey[] = $new;
                unset($new);
            }
            if(!is_object($instagram))
            {
                foreach ($instagram['data'] as $key => $value)
                {   
                    $new['id'] = $value['id'];
                    $new['type'] = '';
                    $new['post_type'] = '7';//Instagram
                    $new['activity_no'] = '';
                    $new['logo'] = $value['user']['profile_picture'];
                    $new['name'] = $value['user']['full_name'];
                    $new['sub_title'] = $value['user']['username'];
                    $new['time'] = $value['created_time'];
                    $new['message'] = $value['caption']['text'];
                    $new['image'] = array($value['images']['low_resolution']['url']);
                    $new['is_like'] = '';
                    $new['like_count'] = $value['likes']['count'];
                    $new['likes'] = [];
                    $new['comments'] = [];
                    $new['user_id'] = '';
                    $new['role_id'] = '';

                    $new['advert_image'] = "";
                    $new['advert_url'] = "";

                    $new['survey_question'] = "";
                    $new['survey_options'] = [];
                    $new['survey_result'] = [];
                    $new['show_result'] = "";

                    $tmp[] = $new;
                    unset($new);
                }
            }

            $tmp = array_values(array_filter($tmp));


            if($page_no != '1')
            {
                $i = 0;
                $queue = "";
                for ($i=0; $i < $prev_post_after_ad; $i++) { 
                    array_unshift($tmp,$queue);
                }

                if(!empty($last_adv_id))
                $advert = array_values(array_slice($advert,array_search($last_adv_id, array_column_1($advert, 'id'))+1));
                else
                    unset($advert);

                if(!empty($last_survey_id))
                $survey = array_values(array_slice($survey,array_search($last_survey_id, array_column_1($survey, 'id'))+1));
                else
                    unset($survey);
            }

            $result = call_user_func_array('array_merge', array_map(function($v) use ($advert,$survey)
                            {   
                                $data = $v + [7 => $advert[$this->i]]+[8 => $survey[$this->i]];
                                $this->i++;
                                return $data;
                            },array_chunk($tmp, 6))); array_pop($result);

            if(is_object($facebook))
                $pagination['fb'] = "";
            else 
                $pagination['fb'] = $facebook['paging']['next'];

            if(is_object($twitter))
                $pagination['tw'] = "";
            else 
                $pagination['tw'] = $twitter['search_metadata']['next_results'] ?: '';
                
            $pagination['ig'] = is_array($instagram)?$instagram['pagination']['next_url']:'';
            $result = array_values(array_filter($result));
            #123
            $data = array(
                    'success'=> true,
                    'data' => $result ?: [],
                    'pagination' => $pagination
                );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function compress_image_v2($data=NULL,$event_id=NULL)
    {   
        if(is_array($data))
        {
            foreach ($data as $key => $value) 
            {   
                if(!empty($value))
                {
                    $source_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$value;
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value;
                    if (!file_exists($_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id)) {
                        mkdir($_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id, 0777, true);
                    }
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/activity/".$event_id.'/'.$new_name;

                    if(!file_exists($destination_url))
                    {
                        if ($info['mime'] == 'image/jpeg')
                        {   
                            $quality = 50;
                            $image = imagecreatefromjpeg($source_url);
                            imagejpeg($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/gif')
                        {   
                            $quality = 5;
                            $image = imagecreatefromgif($source_url);
                            imagegif($image, $destination_url, $quality);
                        }
                        elseif ($info['mime'] == 'image/png')
                        {   
                            $quality = 5;
                            $image = imagecreatefrompng($source_url);
                            $background = imagecolorallocatealpha($image,255,0,255,127);
                            imagecolortransparent($image, $background);
                            imagealphablending($image, false);
                            imagesavealpha($image, true);
                            imagepng($image, $destination_url, $quality);
                        }
                    }
                    $new_data[] = base_url().'assets/user_files/activity/'.$event_id.'/'.$new_name;
                }
            }
            return $new_data;
        }
        elseif(!empty($data))
        {
            $source_url = $_SERVER['DOCUMENT_ROOT']."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 50;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);
                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
            return base_url().'assets/user_files/'.$new_name;
        }
    }
    public function ansSurvey()
    {
        extract($this->input->post());
        if(!empty($event_id) && !empty($user_id) && !empty($survey_id) && !empty($answer))
        {
            $data['question_id'] = str_replace('sur-','',$survey_id);
            $data['user_id'] = $user_id;
            $data['event_id'] = $event_id;
            $data['answer'] = $answer;
            $this->Activity_model->ansSurvey($data);
            //check result publish date
            $survey_answer_chart = $this->Activity_model->getSurveyAns($survey_id);
            if($survey_answer_chart[0]['publish_result'] == '1')
            {
                $final_Array = array();
                foreach($survey_answer_chart as $key => $value)
                {
                    $final_Array[$value['Question']][$value['panswer']][] = array(
                        'id' => $value['puserid'],
                        'Firstname' => $value['Firstname'],
                        'Lastname' => $value['Lastname'],
                        'Email' => $value['Email']
                    );
                }
                foreach($final_Array as $key => $value)
                {
                    foreach($value as $i => $j)
                    {
                        $final_Array[$key][$i] = sizeof($j);
                    }
                }
                $chart_result = [];
                $color_arr = array('#109618','#ff9900','#3366cc','#dc3912','#800000','#FF0000','#C14C33','#593027','#96944C','#156A15','#3B2129','#54073E','#5711A7','#093447','#0C3F33');
                
                foreach($final_Array as $key => $value)
                {
                    $chart['question'] = $key;
                    $i = 0;
                    foreach($value as $key1 => $value1)
                    {
                        $ans['answer_key'] = (string)$key1;
                        $ans['answer_value'] = (string)$value1;
                        $ans['color'] = $color_arr[$i];
                        $chart['answer'][] = $ans;
                        $i++;
                    }
                    $chart_result = $chart;
                }
            }
            $new['survey_result'] = $chart_result['answer']?:[];
            $new['result_message'] = 'Thanks';
            $data = array(
                'success' => true,
                'data' => $new
            );
        }
        else
        {
            $data = array(
                'success' => true,
                'message' => 'Invalid Parameters'
            );
        }
        echo json_encode($data);
    }
}

?>