<?php
if (!defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class Notification extends FrontendController

{
    public function __construct()

    {
        // error_reporting(E_ALL);
        $this->data['pagetitle'] = 'Push Notifications';
        $this->data['smalltitle'] = 'Send instantly or schedule Push Notifications from the App.';
        $this->data['breadcrumb'] = 'Push Notifications';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Notification_model');
        $this->load->model('Event_model');
        $this->load->model('Agenda_model');
        $this->load->model('Appnotitemplate_model');
        $this->load->model('Attendee_model');
        $this->load->model('Native_single_fcm/Qa_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column_1($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $eventmodule = $this->Event_model->geteventmodulues($eventid);
        $module = json_decode($eventmodule[0]['module_list']);
        if (!in_array('27', $module))
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $event = $this->Event_model->get_module_event($eventid);
        $menu_list = explode(',', $event[0]['checkbox_values']);
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            // $eventid = $user[0]->Event_id;
            $rolename = $roledata[0]->Name;
            if ($this->data['pagetitle'] == "Notifications")
            {
                $title = "Push Notification";
            }
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            $cnt = $this->Agenda_model->check_auth("Push Notifications", $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        // if ($cnt == 1 && in_array('27',$menu_list))
        if ($cnt == 1)
        {
            $this->load->model('Setting_model');
            $this->load->model('Speaker_model');
            $this->load->model('Profile_model');
            $this->load->model('Agenda_model');
            $this->load->model('Event_template_model');
            $this->load->model('Cms_model');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
        // if ($eventid == '1511') include 'application/libraries/Gcmr.php';

        // else include 'application/libraries/NativeGcm.php';

        include 'application/libraries/FcmV2.php';

        include 'application/libraries/Fcmr.php';

        include 'application/libraries/Gcmr.php';

    }
    public function index($id=NULL)

    {
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Formbuilder_model->get_permission_list($id);
            $this->data['total_permission'] = $total_permission;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $menudata = $this->Event_model->geteventmenu($id, 27);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        /*$arrSchedule = $this->Notification_model->get_scedulednotification_list($id);
        $this->data['arrSchedule'] = $arrSchedule;
        $arrSent = $this->Notification_model->get_sentnotification_list($id);
        $this->data['arrSent'] = $arrSent;*/
        $arrnotification = $this->Notification_model->get_all_notification_by_event_id($id);
        $this->data['arrnotification'] = $arrnotification;
        $this->data['geo_noti'] = $this->Notification_model->getGeoNoti($id);
        $this->data['noti_analytics'] = $this->Notification_model->getAnalytics($id);
        $arrPushTemplate = $this->Appnotitemplate_model->getAppPushTemplate($id);
        $this->data['arrPushTemplate'] = $arrPushTemplate;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'notification/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $news_event = $this->Event_model->getNewsMasterEvent();
        $is_news_event = $news_event[0]['Id'] == $id ? '1' : '0';
        if($is_news_event)
        {   
            $this->data['rss_noti'] = $this->Notification_model->getRssNoti($id);
            $this->template->write_view('content', 'notification/index_news', $this->data, true);
        }
        else
        $this->template->write_view('content', 'notification/index_new', $this->data, true);

        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'notification/js', $this->data, true);
        $this->template->render();
    }
    public function addnotification($id=NULL)

    {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        if ($id == '634' || $id == '1012')
        {
            $attendee_data = $this->Notification_model->get_all_attendees_by_event_new($id);
            $this->data['attendee_data'] = $attendee_data;
        }
        else
        {
            $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
            $this->data['attendee_data'] = $attendee_data;
        }
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);
        if (!empty($user))
        {
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = null;
                    $data['custommoduleslink'] = $tmp[1];
                }
                $data['title'] = $this->input->post('title');
                if ($this->input->post('noti_type') == '1')
                {
                    $data['notification_type'] = '1';
                }
                else
                {
                    $data['notification_type'] = '2';
                }
                if ($this->input->post('datetime') != '')
                {
                    $data['datetime'] = $this->input->post('datetime');
                }
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $this->input->post('user_ids')))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['content'] = $this->input->post('Description');
                $data['event_id'] = $id;
                $data['ttl'] = $this->input->post('ttl');
                $data['user_groups'] = implode(',', $group_ids);
                $add_id = $this->Notification_model->add_notification($data);
                if ($data['notification_type'] == '1')
                {
                    // if ($id == '1511') {
                    $this->sendPushNotificationNew($add_id, $id, $this->input->post('ttl'));
                    /*} else {
                    $this->sendPushNotification($add_id, $id, $this->input->post('ttl'));
                    }*/
                }
                $this->session->set_flashdata('notification_data', 'Added');
                redirect("Notification/index/" . $id);
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/addnoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function editnotification($id=NULL, $nid=NULL)

    {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
        $this->data['attendee_data'] = $attendee_data;
        $notification = $this->Notification_model->get_edit_notification_data_by_id($id, $nid);
        $this->data['notification'] = $notification[0];
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);
        $user_groups = explode(',', $notification[0]['user_groups']);
        $this->data['groups_user_ids'] = $this->Attendee_model->get_attendee_id_by_group_id($id, $user_groups);
        if (!empty($user))
        {
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = null;
                    $data['custommoduleslink'] = $tmp[1];
                }
                $data['title'] = $this->input->post('title');
                if ($this->input->post('noti_type') == '1')
                {
                    $data['notification_type'] = '1';
                }
                else
                {
                    $data['notification_type'] = '2';
                }
                if ($this->input->post('datetime') != '')
                {
                    $data['datetime'] = $this->input->post('datetime');
                }
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $post_user_ids))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['event_id'] = $id;
                $data['user_groups'] = implode(',', $group_ids);
                $data['content'] = $this->input->post('Description');
                $data['ttl'] = $this->input->post('ttl');
                $this->Notification_model->update_notification($id, $nid, $data);
                $this->session->set_flashdata('notification_data', 'Updated');
                redirect("Notification/index/" . $id);
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/editnoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function pushedit($intEventId = null, $intTempId = null)

    {
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $intEventId);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
        $this->data['cms_list'] = $cms_list;
        $event = $this->Event_model->get_admin_event($intEventId);
        $this->data['event'] = $event[0];
        $arrForm = $this->Appnotitemplate_model->getAppPushTemplate($intEventId, $intTempId);
        $this->data['arrTemplate'] = $arrForm;
        if ($this->data['page_edit_title'] == 'edit')
        {
            if ($intEventId == null || $intEventId == '')
            {
                redirect('Event');
            }
            $temppost = $this->input->post();
            if (!empty($temppost))
            {
                $arrUpdate = array();
                $arrUpdate['event_id'] = $intEventId;
                $arrUpdate['id'] = $intTempId;
                $arrUpdate['Content'] = $this->input->post('Content');
                if (array_key_exists('email_subject', $this->input->post()))
                {
                    $arrUpdate['email_subject'] = $this->input->post('email_subject');
                }
                if (array_key_exists('email_content', $this->input->post()))
                {
                    $arrUpdate['email_content'] = $this->input->post('email_content');
                }
                $advertising_id = $this->Appnotitemplate_model->update_pushtemplate($arrUpdate);
                $this->session->set_flashdata('notification_data', 'Updated');
                redirect("Notification/index/" . $intEventId . "#default_push");
            }
            $this->session->set_userdata($data);
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('content', 'notification/pushedit', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $total_permission = $this->advertising_model->get_permission_list();
                $this->data['total_permission'] = $total_permission;
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/edit_js', $this->data, true);
            $this->template->render();
        }
    }
    public function delete($Event_id=NULL, $id=NULL, $is_geo = 0)

    {
        $this->Notification_model->delete_notification($id);
        if ($is_geo)
        {
            $this->sendUpdateGeoNoti($Event_id);
        }
        $this->session->set_flashdata('notification_data', 'Deleted');
        redirect("Notification/index/" . $Event_id);
    }
    public function sendnotiemail()

    {
        $arrNotification = $this->Notification_model->pushwEmail();
        if (!empty($arrNotification))
        {
            foreach($arrNotification as $intKey => $strVal)
            {
                $arrUser = $this->Event_model->getEventUser($strVal['event_id']);
                if (!empty($arrUser))
                {
                    foreach($arrUser as $intK => $strValue)
                    {
                        $msg = $strVal['content'];
                        $this->email->from(EVENT_EMAIL, 'AllInTheLoop');
                        $this->email->to($strValue['Email']);
                        $this->email->subject("Notification");
                        $this->email->set_mailtype("html");
                        $this->email->message(html_entity_decode($msg));
                        $this->email->send();
                    }
                }
            }
        }
    }
    public function sendPushNotification($add_id=NULL, $event_id=NULL, $ttl = - 1)

    {
        $this->load->model('Native/Settings_model');
        $users = $this->Settings_model->getAllUsersGCM($add_id);
        $event_id_arr = array(
            '619'
        );
        $obj1 = new Fcm();
        if ($event_id == '1511')
        {
            $obj1 = new Fcmr();
        }
        if (in_array($event_id, $event_id_arr))
        {
            $obj = new Gcm($event_id);
        }
        if ($event_id == '634' || $event_id == '1012')
        {
            $users_ios = $this->Settings_model->getAllUsersGCM_ios($add_id);
            $users_android = $this->Settings_model->getAllUsersGCM_android($add_id);
            $device_ids_ios = array_column_1($users_ios, 'gcm_id');
            $device_ids_android = array_column_1($users_android, 'gcm_id');
            $notification = $this->Settings_model->getNotification($add_id, $users_ios[0]['Id'], $users_ios[0]['gcm_id']);
            if (!empty($notification))
            {
                if (!empty($notification['moduleslink']))
                {
                    $extra['message_type'] = 'cms';
                    $extra['message_id'] = $notification['moduleslink'];
                }
                elseif (!empty($notification['custommoduleslink']))
                {
                    $extra['message_type'] = 'custom_page';
                    $extra['message_id'] = $notification['custommoduleslink'];
                }
                $extra['event'] = $this->Settings_model->event_name($event_id);
                $extra['title'] = $notification['title'];
                $msg1 = $notification['content'];
                $tmp_ios = array_chunk($device_ids_ios, 12);
                $obj = new Gcm($event_id);
                $msg['title'] = $notification['title'];
                $msg['message'] = $notification['content'];
                $msg['vibrate'] = 1;
                $msg['sound'] = 1;
                $tmp_and = array_chunk($device_ids_android, 12);
                for ($i = 0; $i < 12; $i++)
                {
                    if (!empty($tmp_ios[$i]))
                    {
                        $obj1->send_arab(1, $tmp_ios[$i], $msg1, $extra, "Iphone");
                    }
                    if (!empty($tmp_and[$i]))
                    {
                        $obj = new Gcm($event_id);
                    }
                    $msg['title'] = $notification['title'];
                    $msg['message'] = $notification['content'];
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result[] = $obj->send_notification($value['gcm_id'], $msg, $extra, $value['device']);
                    {
                        $obj->send_notification_arab($tmp_and[$i], $msg, $extra, "Android");
                    }
                }
            }
        }
        else
        {
            foreach($users as $key => $value)
            {
                if ($value['gcm_id'] != '')
                {
                    $notification = $this->Settings_model->getNotification($add_id, $value['Id'], $value['gcm_id']);
                    if (!empty($notification))
                    {
                        if (!empty($notification['moduleslink']))
                        {
                            $extra['message_type'] = 'cms';
                            $extra['message_id'] = $notification['moduleslink'];
                        }
                        elseif (!empty($notification['custommoduleslink']))
                        {
                            $extra['message_type'] = 'custom_page';
                            $extra['message_id'] = $notification['custommoduleslink'];
                        }
                        $extra['event'] = $this->Settings_model->event_name($event_id);
                        if ($value['device'] == "Iphone")
                        {
                            $extra['title'] = $notification['title'];
                            $msg1 = $notification['content'];
                            if (in_array($event_id, $event_id_arr))
                            {
                                $result[] = $obj->send_notification($value['gcm_id'], $msg1, $extra, $value['device']);
                            }
                            else
                            {
                                if ($event_id == '1511')
                                {
                                    $extra['notification_id'] = $add_id;
                                    $result[] = $obj1->send(1, [$value['gcm_id']], $msg1, $extra, $value['device'], $event_id, $ttl);
                                }
                                else
                                {
                                    $result[] = $obj1->send(1, $value['gcm_id'], $msg1, $extra, $value['device'], $event_id);
                                }
                            }
                        }
                        else
                        {
                            $extra['title'] = $notification['title'];
                            $msg1 = $notification['content'];
                            $extra['notification_id'] = $add_id;
                            if ($event_id == '1511')
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $notification['title'];
                                $msg['message'] = $notification['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification([$value['gcm_id']], $msg1, $extra, $value['device'], $ttl);
                            }
                            else
                            {
                                $obj = new Gcm($event_id);
                                $msg['title'] = $notification['title'];
                                $msg['message'] = $notification['content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $result[] = $obj->send_notification([$value['gcm_id']], $msg, $extra, $value['device']);
                            }
                        }
                    }
                }
            }
            $noti_setting = $this->Event_model->getnotificationsetting($event_id);
            if ($noti_setting[0]['email_display'] == '1')
            {
                $emailuser = $this->Notification_model->get_asign_notification_user_list($event_id, $add_id);
                foreach($emailuser as $key => $value)
                {
                    $this->Event_model->sendEmailToAttendees($event_id, $value['content'], $value['Email'], $value['title']);
                }
            }
        }
    }
    public function sendPushNotificationNew($add_id=NULL, $event_id=NULL, $ttl = - 1)

    {
        $this->load->model('Native/Settings_model');
        $users = $this->Settings_model->getAllUsersGCM($add_id);
        $event_id_arr = array(
            '619'
        );
        $obj1 = new Gcmr();
        $and_device = $ios_device = $old_devices = [];
        $extra['event'] = $this->Settings_model->event_name($event_id);
        foreach($users as $key => $value)
        {
            if ($value['gcm_id'] != '')
            {
                if ($value['version_code_id'] == '')
                {
                    $old_devices[] = $value;
                }
                else
                {
                    if ($value['device'] == 'Android') $and_device[] = $value['gcm_id'];
                    else $ios_device[] = $value['gcm_id'];
                    $notification = $this->Settings_model->getNotification($add_id, $value['Id'], $value['gcm_id']);
                    if (!empty($notification))
                    {   
                        if (!empty($notification['moduleslink']))
                        {
                            $extra['message_type'] = 'cms';
                            $extra['message_id'] = $notification['moduleslink'];
                        }
                        elseif (!empty($notification['custommoduleslink']))
                        {
                            $extra['message_type'] = 'custom_page';
                            $extra['message_id'] = $notification['custommoduleslink'];
                        }
                        if($notification['notification_type'] == '4')
                            $extra['message_type'] = $notification['is_survey'] ? 'rssSurvey' : 'rssNews';
                    }
                }
            }
        }
        $extra['title'] = $notification['title'];
        $msg1 = $notification['content'];
        $extra['notification_id'] = $add_id;
        $obj = new Gcmr($event_id);
        $msg['title'] = $notification['title'];
        $msg['message'] = $notification['content'];
        $msg['vibrate'] = 1;
        $msg['sound'] = 1;
        $result[] = $obj->send_notification($and_device, $msg1, $extra, 'Android', $ttl);
        $result[] = $obj->send_notification($ios_device, $msg1, $extra, 'Iphone', $ttl);
        $result = [];
        $obj = new Fcm($event_id);
        foreach($old_devices as $key => $value)
        {
            if ($value['gcm_id'] != '')
            {
                $notification = $this->Settings_model->getNotification($add_id, $value['Id'], $value['gcm_id']);
                if (!empty($notification))
                {
                    if (!empty($notification['moduleslink']))
                    {
                        $extra['message_type'] = 'cms';
                        $extra['message_id'] = $notification['moduleslink'];
                    }
                    elseif (!empty($notification['custommoduleslink']))
                    {
                        $extra['message_type'] = 'custom_page';
                        $extra['message_id'] = $notification['custommoduleslink'];
                    }
                    $extra['event'] = $this->Settings_model->event_name($event_id);
                    if ($value['device'] == "Iphone")
                    {
                        $extra['title'] = $notification['title'];
                        $msg1 = $notification['content'];
                        $extra['notification_id'] = $add_id;
                        $result[] = $obj->send(1, $value['gcm_id'], $msg1, $extra, $value['device'], $event_id);
                    }
                    else
                    {
                        $extra['title'] = $notification['title'];
                        $msg1 = $notification['content'];
                        $extra['notification_id'] = $add_id;
                        $obj = new Gcm($event_id);
                        $msg['title'] = $notification['title'];
                        $msg['message'] = $notification['content'];
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $result[] = $obj->send_notification($value['gcm_id'], $msg, $extra, $value['device']);
                    }
                }
            }
        }
        $noti_setting = $this->Event_model->getnotificationsetting($event_id);
        if ($noti_setting[0]['email_display'] == '1')
        {
            $emailuser = $this->Notification_model->get_asign_notification_user_list($event_id, $add_id);
            foreach($emailuser as $key => $value)
            {
                $this->Event_model->sendEmailToAttendees($event_id, $value['content'], $value['Email'], $value['title']);
            }
        }
    }
    public function save_only_notify_login_user_status($eid=NULL)

    {
        $data['event_array']['Id'] = $eid;
        if ($this->input->post('only_notify_login_user') == '1')
        {
            $data['event_array']['only_notify_login_user'] = '1';
        }
        else
        {
            $data['event_array']['only_notify_login_user'] = '0';
        }
        $this->Event_model->update_admin_event($data);
        redirect(base_url() . "Notification/index/" . $eid);
    }
    public function active_mail_notify($edi=NULL, $nid=NULL)

    {
        $pushtemplate = $this->Appnotitemplate_model->getAppPushTemplate($edi, $nid);
        $arrUpdate['send_email'] = $pushtemplate[0]['send_email'] == '0' ? '1' : '0';
        $advertising_id = $this->Appnotitemplate_model->active_email_notify($arrUpdate, $edi, $nid);
        echo "success###";
    }
    public function addGeoNoti($id=NULL)

    {
        
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        if ($id == '634' || $id == '1012')
        {
            $attendee_data = $this->Notification_model->get_all_attendees_by_event_new($id);
            $this->data['attendee_data'] = $attendee_data;
        }
        else
        {
            $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
            $this->data['attendee_data'] = $attendee_data;
        }
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);
        if (!empty($user))
        {
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = null;
                    $data['custommoduleslink'] = $tmp[1];
                }
                $data['title'] = $this->input->post('title');
                $data['notification_type'] = '3';
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $this->input->post('user_ids')))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['content'] = $this->input->post('Description');
                $data['event_id'] = $id;
                $data['user_groups'] = implode(',', $group_ids);
                $data['lat'] = $this->input->post('lat');
                $data['long'] = $this->input->post('long');
                $data['address'] = $this->input->post('address');
                $data['radius'] = $this->input->post('radius');
                $add_id = $this->Notification_model->add_notification($data);
                $this->sendUpdateGeoNoti($id);
                $this->session->set_flashdata('notification_data', 'Added');
                redirect("Notification/index/" . $id . "#geo_noti");
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/addgeonoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function editGeoNoti($id=NULL, $nid=NULL)

    {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
        $this->data['attendee_data'] = $attendee_data;
        $notification = $this->Notification_model->get_edit_notification_data_by_id($id, $nid);
        $this->data['notification'] = $notification[0];
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);
        $user_groups = explode(',', $notification[0]['user_groups']);
        $this->data['groups_user_ids'] = $this->Attendee_model->get_attendee_id_by_group_id($id, $user_groups);
        if (!empty($user))
        {
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = null;
                    $data['custommoduleslink'] = $tmp[1];
                }
                $data['title'] = $this->input->post('title');
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $post_user_ids))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['event_id'] = $id;
                $data['user_groups'] = implode(',', $group_ids);
                $data['content'] = $this->input->post('Description');
                $data['lat'] = $this->input->post('lat');
                $data['long'] = $this->input->post('long');
                $data['address'] = $this->input->post('address');
                $data['radius'] = $this->input->post('radius');
                $this->Notification_model->update_notification($id, $nid, $data);
                $this->session->set_flashdata('notification_data', 'Updated');
                $this->sendUpdateGeoNoti($id);
                redirect("Notification/index/" . $id . '#geo_noti');
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/editgeonoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
    /* old one as on date 20-feb-2019 */
    // public function sendUpdateGeoNoti($event_id=NULL)

    // {
    //     $obj = new Gcm($event_id);
    //     $obj1 = new Fcm();
    //     $users = $this->Qa_model->getAllUsersGCM_id_by_event_id($event_id);
    //     $count = count($users);
    //     if ($count > 100)
    //     {
    //         $limit = 100;
    //         for ($i = 0; $i < $count; $i++)
    //         {
    //             $page_no = $i;
    //             $start = ($page_no) * $limit;
    //             $users1 = array_slice($users, $start, $limit);
    //             foreach($users1 as $key => $value)
    //             {
    //                 if ($value['gcm_id'] != '')
    //                 {
    //                     $msg = '';
    //                     $extra['message_type'] = 'updateGeoNoti';
    //                     $extra['message_id'] = '';
    //                     $extra['event'] = $event[0]['Event_name'];
    //                     $extra['title'] = '';
    //                     if ($value['device'] == "Iphone")
    //                     {
    //                         $result[] = $obj1->send_silent(1, $value['gcm_id'], $msg, $extra, $value['device'], $event_id);
    //                     }
    //                     else
    //                     {
    //                         $msg['title'] = '';
    //                         $msg['message'] = '';
    //                         $msg['vibrate'] = 1;
    //                         $msg['sound'] = 1;
    //                         $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     else
    //     {
    //         foreach($users as $key => $value)
    //         {
    //             if ($value['gcm_id'] != '')
    //             {
    //                 $msg = '';
    //                 $extra['message_type'] = 'updateGeoNoti';
    //                 $extra['message_id'] = '';
    //                 $extra['event'] = $event[0]['Event_name'];
    //                 $extra['title'] = '';
    //                 if ($value['device'] == "Iphone")
    //                 {
    //                     $result[] = $obj1->send_silent(1, $value['gcm_id'], $msg, $extra, $value['device'], $event_id);
    //                 }
    //                 else
    //                 {
    //                     $msg['title'] = '';
    //                     $msg['message'] = '';
    //                     $msg['vibrate'] = 1;
    //                     $msg['sound'] = 1;
    //                     $result[] = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
    //                 }
    //             }
    //         }
    //     }
    // }
      /* make new as on date 20-feb-2019 */
   function sendUpdateGeoNoti($event_id=NULL)

    {
       $obj   = new Gcm($event_id);
        $obj1  = new Fcm();
        $users = $this->Qa_model->getAllUsersGCM_id_by_event_id($event_id);
        
        
        $count = count($users);
        if ($count > 100) {
            $limit = 100;
            for ($i = 0; $i < $count; $i++) {
                $page_no = $i;

                $start  = ($page_no) * $limit;
                $users1 = array_slice($users, $start, $limit);
                foreach ($users1 as $key => $value) {
                    if ($value['version_code_id'] == '') 
                    {

                        if ($value['gcm_id'] != '') {
                            $msg                   = '';
                            $extra['message_type'] = 'updateGeoNoti';
                            $extra['message_id']   = '';
                            $extra['event']        = $event[0]['Event_name'];
                            $extra['title']        = '';
                            if ($value['device'] == "Iphone") {
                                $result[] = $obj1->send_silent(1, $value['gcm_id'], $msg, $extra, $value['device'], $event_id);
                            } else {
                                $msg['title']   = '';
                                $msg['message'] = '';
                                $msg['vibrate'] = 1;
                                $msg['sound']   = 1;
                                $result[]       = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                            }

                        }
                    }else{
                        
                           $obj2            = new Gcmr($event_id);
                            $msg['title']   = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound']   = 1;
                            $extra['message_type'] = 'updateGeoNoti';
                            $extra['message_id']   = '';
                            $extra['event']        = $event[0]['Event_name'];
                            $extra['title']        = '';
                           $obj2->send_notification($value['gcm_id'], $msg, $extra, $value['device'], $ttl);

                        }

                    }//foreach
                }//for
            }//counter
       
         else 
         {

            
            foreach ($users as $key => $value) {

             if ($value['version_code_id'] == '') 
            {
                if ($value['gcm_id'] != '') 
                {
                    $msg                   = '';
                    $extra['message_type'] = 'updateGeoNoti';
                    $extra['message_id']   = '';
                    $extra['event']        = $event[0]['Event_name'];
                    $extra['title']        = '';
                    if ($value['device'] == "Iphone") {
                        $result[] = $obj1->send_silent(1, $value['gcm_id'], $msg, $extra, $value['device'], $event_id);
                    } else {
                        $msg['title']   = '';
                        $msg['message'] = '';
                        $msg['vibrate'] = 1;
                        $msg['sound']   = 1;
                        $result[]       = $obj->send_notification_silent($value['gcm_id'], $msg, $extra, $value['device']);
                    }
                }

            }else{
                          
                          
                           $obj2            = new Gcmr($event_id);
                            
                            $msg['title']   = '';
                            $msg['message'] = '';
                            $msg['vibrate'] = 1;
                            $msg['sound']   = 1;
                            $extra['message_type'] = 'updateGeoNoti';
                            $extra['message_id']   = '';
                            $extra['event']        = $event[0]['Event_name'];
                            $extra['title']        = '';
                           $obj2->send_notification($value['gcm_id'], $msg, $extra, $value['device'], $ttl);

                 }
        }//foreah
    }//else
    }
    public function analytics($id=NULL, $nid=NULL)

    {
        if ($this->data['user']->Role_name == 'User')
        {
            $total_permission = $this->Formbuilder_model->get_permission_list($id);
            $this->data['total_permission'] = $total_permission;
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $menudata = $this->Event_model->geteventmenu($id, 27);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        /*$arrSchedule = $this->Notification_model->get_scedulednotification_list($id);
        $this->data['arrSchedule'] = $arrSchedule;
        $arrSent = $this->Notification_model->get_sentnotification_list($id);
        $this->data['arrSent'] = $arrSent;*/
        $this->data['noti_analytics'] = $this->Notification_model->getAnalyticsById($nid);
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        $this->template->write_view('css', 'notification/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'notification/analytics_details', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'notification/js', $this->data, true);
        $this->template->render();
    }
    public function addNewsNoti($id=NULL)

    {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
        $this->data['attendee_data'] = $attendee_data;
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);

        $this->data['rss'] = $this->Notification_model->getRss($event[0]['Organisor_id']);
        $this->data['survey'] = $this->Notification_model->getSurvey($id);

        if (!empty($user))
        {   
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                    $data['is_survey'] = '1';
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                    $data['is_survey'] = '0';
                }
                $data['title'] = $this->input->post('title');
                $data['notification_type'] = '4';
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $this->input->post('user_ids')))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['content'] = $this->input->post('Description');
                $data['event_id'] = $id;
                $data['user_groups'] = implode(',', $group_ids);
                $add_id = $this->Notification_model->add_notification($data);
                $this->sendPushNotificationNew($add_id,$id);
                $this->session->set_flashdata('notification_data', 'Added');
                redirect("Notification/index/" . $id);
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/addNewsnoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
    public function editNewsNoti($id=NULL, $nid=NULL)

    {
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu = $this->Event_model->get_cms_menu($id);
        $this->data['cms_menu'] = $cms_menu;
        $attendee_data = $this->Notification_model->get_all_attendees_by_event($id);
        $this->data['attendee_data'] = $attendee_data;
        $notification = $this->Notification_model->get_edit_notification_data_by_id($id, $nid);
        $this->data['notification'] = $notification[0];
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $event_templates = $this->Event_model->get_event_template_by_id_list($id);
        $this->data['event_templates'] = $event_templates;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $logged_in_user_id = $user[0]->Id;
        $this->data['user_group'] = $this->Attendee_model->get_user_group($id);
        $user_groups = explode(',', $notification[0]['user_groups']);
        $this->data['groups_user_ids'] = $this->Attendee_model->get_attendee_id_by_group_id($id, $user_groups);

        $this->data['rss'] = $this->Notification_model->getRss($event[0]['Organisor_id']);
        $this->data['survey'] = $this->Notification_model->getSurvey($id);

        if (!empty($user))
        {
            if ($this->input->post())
            {
                $sdom = $event[0]['Subdomain'];
                $tmp = explode('_', $this->input->post('Menu_id'));
                if ($tmp[0] == 'M')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                    $data['is_survey'] = '1';
                }
                elseif ($tmp[0] == 'C')
                {
                    $data['moduleslink'] = $tmp[1];
                    $data['custommoduleslink'] = null;
                    $data['is_survey'] = '0';
                }
                $data['title'] = $this->input->post('title');
                $post_group_ids = ($this->input->post('user_group')) ? : array();
                $post_user_ids = ($this->input->post('user_ids')) ? : array();
                $post_user_ids = array_merge($post_group_ids, $post_user_ids);
                foreach($post_user_ids as $key => $value)
                {
                    if (strpos($value, 'usergroup') !== false)
                    {
                        $group_ids[] = explode('_', $value) [1];
                        unset($post_user_ids[$key]);
                    }
                }
                $group_user_ids = ($group_ids) ? $this->Attendee_model->get_attendee_id_by_group_id($event[0]['Id'], $group_ids) : array();
                if (in_array('All', $post_user_ids))
                {
                    $data['user_ids'] = array_filter(array_column_1($attendee_data, 'Id'));
                    $data['send_to_all'] = '1';
                }
                else
                {
                    $data['user_ids'] = array_filter($post_user_ids);
                }
                $data['user_ids'] = array_unique(array_merge($data['user_ids'], $group_user_ids));
                $data['user_ids'] = implode(',', $data['user_ids']);
                $data['event_id'] = $id;
                $data['user_groups'] = implode(',', $group_ids);
                $data['content'] = $this->input->post('Description');
                $this->Notification_model->update_notification($id, $nid, $data);
                $this->session->set_flashdata('notification_data', 'Updated');
                redirect("Notification/index/".$id);
            }
            $this->template->write_view('css', 'notification/add_css', $this->data, true);
            $this->template->write_view('header', 'common/header', $this->data, true);
            $this->template->write_view('content', 'notification/editNewsnoti', $this->data, true);
            if ($this->data['user']->Role_name == 'User')
            {
                $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
            }
            else
            {
                $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            }
            $this->template->write_view('js', 'notification/add_js', $this->data, true);
            $this->template->render();
        }
    }
}