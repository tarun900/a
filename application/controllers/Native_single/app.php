<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class App extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/note_model');
        $this->load->model('native_single/event_template_model');
        $this->load->model('native_single/attendee_model');
        $this->events = $this->event_template_model->get_event_template_by_id_list($this->input->post('subdomain'));
        $bannerimage_decode = json_decode($this->events[0]['Images']);
        $logoimage_decode = json_decode($this->events[0]['Logo_images']);

        $this->events[0]['Images'] = $bannerimage_decode[0];
        $this->events[0]['Logo_images'] = $logoimage_decode[0];

        $this->cmsmenu = $this->cms_model->get_cms_page_new($this->input->post('event_id'),NULL,NULL,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->cmsmenu1 = $this->cms_model->get_cms_page_new1($this->input->post('event_id'));
        
        foreach ($this->cmsmenu as $key => $values) 
        {
            if(!empty($values['Images']))
            {
                $cmsbannerimage_decode = json_decode($values['Images']);
                $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Images'] = "";
            }
            if(!empty($values['Logo_images']))
            {
                $cmslogoimage_decode = json_decode($values['Logo_images']);
                $this->cmsmenu[$key]['Logo_images'] = $cmslogoimage_decode[0];
            }
            else
            {
                $this->cmsmenu[$key]['Logo_images'] = "";
            }
        }

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->menu_list1 = $this->event_model->geteventmenu_list1($this->input->post('event_id'), null, null,$this->input->post('user_id'));
    }

    public function index()
    {

        $user = $this->app_login_model->check_token_with_event($this->input->post('_token'),$this->input->post('event_id'));
        if (empty($user)) 
        {
            $data = array(
                'success' => false,
                'data' => array(
                    'msg' => 'Please check token or event.'
                )
            );   
        } 
        else 
        {                        
            $fetureproduct = $this->event_model->getFetureProduct($this->input->post('event_id'));

            $data = array(
                'events' => $this->events,
                'menu_list' => $this->menu_list,
                'cmsmenu' => $this->cmsmenu
            );

            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        
        echo json_encode($data);
    }

    public function event_id_advance_design()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('_token');
        $user_id=$this->input->post('user_id');
        $test = $this->input->post('test');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $event_list = $this->get_event($event_id,$user_id);
                $user = $this->app_login_model->getUserDetailsId($user_id);
                
                if(!empty($event_list))
                {
                    $note_status=0;
                    if($user_id!='')
                    {
                        $note_data = $this->note_model->getNotesListByEventId($event_id,$user_id);
                        $note_status= (count($note_data)>0) ? 1 : 0;
                    }
                    
                    foreach ($this->menu_list as $key => $value)
                    {
                        if($value['is_feture_products'] == '1')
                        {
                            $value['is_cms'] = '0';
                            $value['my_id'] = $value['id'];
                            $menu[] = $value;
                        }
                        if($value['id'] == '1' && !empty($value['category_list']) && $value['is_feture_products'] == '1')
                        {
                            foreach ($value['category_list'] as $k => $v)
                            {   
                                $v['is_cms'] = '0';
                                $menu[] = $v;
                            }
                        }
                    }
                    foreach ($this->cmsmenu as $key => $value)
                    {
                        if($value['is_feture_product'] == '1' && $value['show_in_app'] == '1')
                        {
                            $value['my_id'] = $value['Id'];
                            $value['is_cms'] = '1';
                            $value['category_id'] = "";
                            $menu[] = $value;
                        }
                    }
            
                    foreach ($menu as $key => $value) 
                    {
                        $menu[$key]['is_feture_products'] = ($value['is_cms'] == '1') ? $value['is_feture_product'] : $value['is_feture_products'] ;
                        $menu[$key]['id'] = ($value['is_cms'] == '1') ? $value['Id'] : $value['id'] ;
                        $menu[$key]['Menu_name'] = ($value['is_cms'] == '1') ? $value['Menu_name'] : $value['menuname'] ;
                        $menu[$key]['Images'] = ($value['is_cms'] == '1') ? $value['Images'] : $value['img'] ;
                        if($value['is_cms'] == '1')
                        $menu[$key]['is_force_login'] = 0;
                    }

                    function sortByOrder($a, $b)
                    {
                        return $a['sort_order'] - $b['sort_order'];
                    }

                    usort($menu, 'sortByOrder');

                    $banner_images_coords = $this->app_login_model->banner_images_and_coords($event_id,$user_id);

                    foreach ($banner_images_coords['images'] as $key => $value) 
                    {   
                        if(!empty($value['Image']))
                        {
                            $source_url = base_url()."assets/user_files/".$value['Image'];
                            $info = getimagesize($source_url);
                            $new_name = "new_".$value['Image'];
                            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                            if ($info['mime'] == 'image/jpeg')
                            {   
                                $quality = 50;
                                $image = imagecreatefromjpeg($source_url);
                                imagejpeg($image, $destination_url, $quality);
                            }
                            elseif ($info['mime'] == 'image/gif')
                            {   
                                $new_name = $value['Image'];
                            }
                            elseif ($info['mime'] == 'image/png')
                            {   
                                $quality = 5;
                                $image = imagecreatefrompng($source_url);
                                $background = imagecolorallocatealpha($image,255,0,255,127);
                                imagecolortransparent($image, $background);
                                imagealphablending($image, false);
                                imagesavealpha($image, true);
                                imagepng($image, $destination_url, $quality);
                            }
                            $banner_images_coords['images'][$key]['Image'] = $new_name;
                        }
                    }
                    $lang_list = $this->app_login_model->get_event_lang_list($event_id);
                    
                    $data1 = array(
                        'events' => $event_list,
                        'banner_images' => $banner_images_coords,
                        'menu_list' => $this->menu_list,
                        'cmsmenu' => $this->cmsmenu,
                        'ersite_home_page_menu' =>($event_id == '455') ?  $array1 : [],
                        'aitl_home_page_menu' => ($menu != '') ?  $menu : [],
                        'note_status' => $note_status,
                        'lang_list' => $lang_list
                    );
                    
                    $data = array(
                      'success' => true,
                      'data' => $data1
                    );
                }
                else{
                    $data = array(
                      'success' => false,
                      'message' => 'This event is not assigned to this user.',
                    );
                }
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function get_default_lang()
    {
        $event_id=$this->input->post('event_id');
        $lang_id=$this->input->post('lang_id');
        if(!empty($event_id))
        {
            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
            $lang_list = $this->app_login_model->get_event_lang_list($event_id);
            $data = array(
                'success' => true,
                'default_lang' => $default_lang,
                'lang_list' => $lang_list
            );
        }
        else
        {
            $data = array(
                'success' => false,
                'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function compress_image($data)
    {   
        if(is_array($data))
        {
            foreach ($data as $key => $value) 
            {   
                if(!empty($value))
                {
                    $source_url = base_url()."assets/user_files/".$value;
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value;
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                    if ($info['mime'] == 'image/jpeg')
                    {   
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {   
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);

                    }
                    elseif ($info['mime'] == 'image/png')
                    {   
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);

                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $new_data[] = $new_name;
                }
            }
            return $new_data;
        }
        elseif(!empty($data))
        {
            $source_url = base_url()."assets/user_files/".$data;
            $info = getimagesize($source_url);
            $new_name = "new_".$data;
            $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

            if ($info['mime'] == 'image/jpeg')
            {   
                $quality = 50;
                $image = imagecreatefromjpeg($source_url);
                imagejpeg($image, $destination_url, $quality);
            }
            elseif ($info['mime'] == 'image/gif')
            {   
                $quality = 5;
                $image = imagecreatefromgif($source_url);
                imagegif($image, $destination_url, $quality);

            }
            elseif ($info['mime'] == 'image/png')
            {   
                $quality = 5;
                $image = imagecreatefrompng($source_url);
                $background = imagecolorallocatealpha($image,255,0,255,127);
                imagecolortransparent($image, $background);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                imagepng($image, $destination_url, $quality);
            }
            return $new_name;
        }
    }
    public function get_event($event_id,$user_id)
    {   
        if($event_id!='')
        {
            $event_list = $this->app_login_model->check_event_with_id($event_id,$user_id);
            $user = $this->app_login_model->getUserDetailsId($user_id);

            if(!empty($event_list))
            {
                $menus = explode(',', $event_list[0]['checkbox_values']);
                $tmp = preg_replace('/[[:^print:]]/', '',trim(strip_tags($event_list[0]['Description'])));
                $event_list[0]['Description'] = empty($tmp) ?  "" : $event_list[0]['Description'];

                $event_list[0]['is_enabled_favorites']= (in_array('49', $menus)) ? "1" : "0";

                if($event_list[0]['fun_background_color']==0)
                {
                    $event_list[0]['fun_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_text_color']==0)
                {
                    $event_list[0]['fun_top_text_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['fun_top_text_color1']= $event_list[0]['fun_top_text_color'];
                }
                if($event_list[0]['fun_footer_background_color']==0)
                {
                    $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_block_background_color']==0)
                {
                    $event_list[0]['fun_block_background_color']="#FFFFFF";
                }
                if($event_list[0]['fun_top_background_color']==0)
                {
                    $event_list[0]['fun_top_background_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['fun_top_background_color1']=  $event_list[0]['fun_top_background_color'];
                }
                if($event_list[0]['theme_color']==0)
                {
                    $event_list[0]['theme_color1']="#FFFFFF";
                }
                else
                {
                    $event_list[0]['theme_color1']=$event_list[0]['theme_color'];
                }
                if($event_list[0]['fun_block_text_color']==0)
                {
                    $event_list[0]['fun_block_text_color']="#FFFFFF";
                }

                if($event_list[0]['fun_footer_background_color']==0)
                {
                    $event_list[0]['fun_footer_background_color']="#FFFFFF";
                }
                $img=json_decode($event_list[0]['Images']);
                $Logo_images=json_decode($event_list[0]['Logo_images']);
                $Background_img=json_decode($event_list[0]['Background_img']);

                if(empty($Logo_images[0]))
                {
                    $Logo_images[0]="";
                }
                if(empty($Background_img[0]))
                {
                    $Background_img[0]="";
                }

                $event_list[0]['Images']=$img[0];

                $event_list[0]['banners'] = ($img) ? $this->compress_image($img) : [];
                $event_list[0]['Logo_images1']=$Logo_images[0];
                $event_list[0]['Background_img1']=$Background_img[0];
                $event_list[0]['description1']=$event_list[0]['Description'];
                $event_list[0]['Icon_text_color1']=$event_list[0]['Icon_text_color'];

                $checkbox_values = $event_list[0]['checkbox_values'];
                $menu_array = explode(',', $checkbox_values);

                $event_list[0]['show_message_bell_icon'] = (in_array('12', $menu_array) || in_array('13', $menu_array)) ? 1 : 0;
                $event_list[0]['show_notes_icon'] = (in_array('6', $menu_array)) ? 1 : 0;
                
                $active_module = array_column_1($this->menu_list, 'id');
                $event_list[0]['show_message_bell_icon'] = (in_array('12', $active_module) || in_array('13', $active_module)) ? 1 : 0;
                $event_list[0]['photo_filter_enabled'] = (in_array('54',array_column_1($this->menu_list,'id'))) ? '1' : '0';
            }
        }
        return $event_list;                   
    }   
}