<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Attendee extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/attendee_model');
        $this->load->model('native_single/message_model');
        $this->load->model('native_single/settings_model');
        $this->load->model('native_single/gamification_model');
        
        include('application/libraries/nativeGcm.php');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function attendee_list()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $attendee = $this->attendee_model->getAttendeeListByEventId($event_id,$user_id);
                $contact_attendee = $this->attendee_model->getContactAttendeeListByEventId($event_id,$user_id);
                $data = array(
                    'attendee_list' => $attendee,
                    'contact_attendee' => $contact_attendee,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function attendee_list_native()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        if($event_id!='' && $event_type!='')
        {
            if($keyword!='')
            $where = "u.Lastname like '%$keyword%'";
            $attendee_data = $this->attendee_model->getAttendeeListByEventId($event_id,($where) ? $where : '',$page_no,$user_id);
            
            foreach ($attendee_data as $key => $value) 
            {   
                if(!empty($value['Logo']))
                {
                    $source_url = base_url()."assets/user_files/".$value['Logo'];
                    $info = getimagesize($source_url);
                    $new_name = "new_".$value['Logo'];
                    $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                    if ($info['mime'] == 'image/jpeg')
                    {   
                        $quality = 50;
                        $image = imagecreatefromjpeg($source_url);
                        imagejpeg($image, $destination_url, $quality);
                    }
                    elseif ($info['mime'] == 'image/gif')
                    {   
                        $quality = 5;
                        $image = imagecreatefromgif($source_url);
                        imagegif($image, $destination_url, $quality);

                    }
                    elseif ($info['mime'] == 'image/png')
                    {   
                        $quality = 5;
                        $image = imagecreatefrompng($source_url);

                        $background = imagecolorallocatealpha($image,255,0,255,127);
                        imagecolortransparent($image, $background);
                        imagealphablending($image, false);
                        imagesavealpha($image, true);
                        imagepng($image, $destination_url, $quality);
                    }
                    $attendee_data[$key]['Logo'] = $new_name;
                }
            }
            $data = array(
                'attendee_list' => $attendee_data,
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_contact_list()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $contact_attendee = $this->attendee_model->getContactAttendeeListByEventId($event_id,$user_id);
                $data = array(
                    'contact_attendee' => $contact_attendee,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function attendee_list_native_new()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        if($event_id!='' && $event_type!='')
        {
            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%' OR u.Title like '%".$keyword."%')";
            $attendee_data = $this->attendee_model->getAttendeeListByEventId_new($event_id,($where) ? $where : '',$page_no,$user_id);
            $data = array(
                'attendee_list' => $attendee_data['attendees'],
                'list_total_pages' => $attendee_data['total'],
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_contact_list_new()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $event_type=$this->input->post('event_type');
        $page_no=$this->input->post('page_no');
        $keyword=$this->input->post('keyword');
        if($event_id!='' && $event_type!='')
        {
            if($keyword!='')
            $where = "(u.Lastname like '%".$keyword."%' OR u.Firstname like '%".$keyword."%' OR u.Company_name like '%".$keyword."%')";
            $contact_attendee = $this->attendee_model->get_my_contact($event_id,$user_id,($where) ? $where : '',$page_no);

            $data = array(
                'contact_attendee' => $contact_attendee['attendees'],
                'list_total_pages' => $contact_attendee['total'],
            );
            
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }

    public function attendee_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $attendee_id!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $attendee = $this->attendee_model->getAttendeeDetails($attendee_id,$user_id);
                if($attendee)
                {
                    $message = $this->message_model->view_private_msg_coversation($user_id,$attendee_id,$event_id,$page_no,$limit);
                    foreach ($message as $key => $value)
                    {
                        $string = $value['message'];
                        if($string != strip_tags($string)) 
                        {
                            $message[$key]['message'] = strip_tags($value['message']);
                            $message[$key]['is_clickable'] = '1';
                        }
                        else
                        {
                            $message[$key]['is_clickable'] = '0';
                        }   
                    }
                    $total_pages=$this->message_model->total_pages($user_id,$attendee_id,$event_id,$limit);
                    $social_links = $this->attendee_model->getSocialLinks($attendee_id);
                    $attendee[0]->Facebook_url = ($social_links[0]->Facebook_url) ? $social_links[0]->Facebook_url : '';
                    $attendee[0]->Twitter_url =  ($social_links[0]->Twitter_url) ? $social_links[0]->Twitter_url : '';
                    $attendee[0]->Linkedin_url = ($social_links[0]->Linkedin_url) ? $social_links[0]->Linkedin_url : '';
                    $where['event_id'] = $event_id;
                    $where['from_id'] = $user_id;
                    $where['to_id'] = $attendee_id;
                    $attendee[0]->approval_status = ($user_id != $attendee_id) ?  $this->attendee_model->getApprovalStatus($where,$event_id) : '';
                    $where1['to_id'] = $user_id;
                    $attendee[0]->share_details = ($user_id == $attendee_id) ? $this->attendee_model->getShareDetails($where1,$event_id) : [] ;
                    $attendee[0]->contact_details = ($attendee[0]->approval_status == '1') ? $this->attendee_model->getAttendeeConatctDetails($attendee_id) : [] ;
                }
                $allow_ex_contact = $this->attendee_model->getAllowAttendee($attendee_id,$event_id);
                $data = array(
                    'attendee_details' => ($attendee) ? $attendee : [],
                    'message' => $message,
                    'allowexhcontact' => $allow_ex_contact,
                    'total_pages' => $total_pages,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function sendMessage()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');
        $message_type=$this->input->post('message_type');
        $message=$this->input->post('message');
       
        if($event_id!='' && $token!='' && $attendee_id!='' && $user_id!='' && $message!='' && $message_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data['Message']=$message;
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$attendee_id;
                $message_data['Parent']=0;
                $message_data['image']="";
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']=$message_type;
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    $this->message_model->add_msg_hit($user_id,$current_date,$attendee_id,$event_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Successfully Saved",
                        'message_id' => $message_id
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function msg_images_request()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');
        $message_type=$this->input->post('message_type');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $attendee_id!='' && $user_id!='' && $message_id!='' && $_FILES['image']['name']!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $message_data1=$this->message_model->getMessageDetails($message_id);

                $newImageName = round(microtime(true) * 1000).".jpeg";
                $target_path= "././assets/user_files/".$newImageName; 
                $target_path1= "././assets/user_files/thumbnail/";                 
                move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                $update_data['image'] =$newImageName;
                if($message_data1[0]['image']!='')
                {
                    $arr=json_decode($message_data1[0]['image']);
                    $arr[]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                else
                {

                    $arr[0]=$newImageName;
                    $update_arr['image']=json_encode($arr);
                    $this->message_model->updateMessageImage($message_id,$update_arr);
                }
                $a[0]=$newImageName;
                $message_data['Message']='';
                $message_data['Sender_id']=$user_id;
                $message_data['Event_id']=$event_id;
                $message_data['Receiver_id']=$attendee_id;
                $message_data['Parent']=$message_id;
                $message_data['image']=json_encode($a);
                $message_data['Time']=date("Y-m-d H:i:s");
                $message_data['ispublic']='1';
                $message_data['msg_creator_id']=$user_id;
                $message_id = $this->message_model->saveMessage($message_data);
                if($message_id!=0)
                {
                    $current_date=date('Y/m/d');
                    //$this->message_model->add_msg_hit($user_id,$current_date,$attendee_id); 
                    $data = array(
                        'success' => true,
                        'message' => "Success saved"
                    );
                }
                else
                {
                    $data = array(
                    'success' => false,
                    'message' => "Something is wrong.Please try again"
                    );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_message()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $message_id=$this->input->post('message_id');

        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_message($message_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully deleted"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function make_comment()
    {   
        //error_reporting(1);
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $comment=$this->input->post('comment');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $token!='' && $user_id!='' && $message_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                if($_FILES['image']['name']!='')
                {
                    $newImageName = round(microtime(true) * 1000).".jpeg";
                    $target_path= "././assets/user_files/".$newImageName; 
                    $target_path1= "././assets/user_files/thumbnail/";                 
                    move_uploaded_file($_FILES['image']['tmp_name'],$target_path);
                    copy("./assets/user_files/".$newImageName,"./assets/user_files/thumbnail/".$newImageName);
                    if($newImageName!='')
                    {
                        $arr[0]=$newImageName;
                        $comment_arr['image']=json_encode($arr);
                    }
                }
                $comment_arr['comment']=$comment;
                $comment_arr['user_id']=$user_id;
                $comment_arr['msg_id']=$message_id;
                $comment_arr['Time']=date("Y-m-d H:i:s");
               
                $this->message_model->make_comment($comment_arr,$event_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function delete_comment()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $comment_id=$this->input->post('comment_id');
        if($event_id!='' && $token!='' && $comment_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $this->message_model->delete_comment($comment_id);
                $data = array(
                    'success' => true,
                    'message' => "Successfully"
                    );
            }
            
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getImageList()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $message_id=$this->input->post('message_id');
        if($event_id!='' && $message_id!='')
        {
            
            $images = $this->message_model->getImages($event_id,$message_id);
            if($images!=''){
                $data = array(
                'images' => json_decode($images),
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }else{
                $data = array(
                  'success' => false,
                  'message' => "No images found"
                );
            }
                
           
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function shareContactInformation()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');
        $lang_id = $this->input->post('lang_id');

        if($event_id!='' && $user_id!='' && $attendee_id!='')
        {
            $where['event_id']         = $event_id;
            $where['from_id']          = $user_id;
            $where['to_id']            = $attendee_id;

            $share_data['event_id']         = $event_id;
            $share_data['from_id']          = $user_id;
            $share_data['to_id']            = $attendee_id;
            $share_data['contact_type']     = '0';
            $share_data['approval_status']  ='0';
            
            $this->add_user_game_point($user_id,$event_id,4);
            $id = $this->attendee_model->saveShareContactInformation($where,$share_data);

            $url_data = $this->attendee_model->getUrlData($event_id);
            $url=base_url().$url_data.$id;

            $user = $this->attendee_model->getUser($user_id);

            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
            
            $msg1 = $default_lang['notifications__share_contact'];
            $patterns = array();
            $patterns[0] = '/{{user_name}}/';
            $replacements = array();
            $replacements[0] = ucfirst($user['Firstname'])." ".$user['Lastname'];
            $msg1 = preg_replace($patterns, $replacements, $msg1);

            $message="<a href='".$url."'>".ucfirst($user['Firstname'])." ".$user['Lastname']." ".$msg1."</a>";
            
            $message_data['Message']=$message;
            $message_data['Sender_id']=$user_id;
            $message_data['Receiver_id']=$attendee_id;
            $message_data['Event_id']=$event_id;
            $message_data['Parent']='0';
            $message_data['Time']=date("Y-m-d H:i:s");
            $message_data['ispublic']='0';
            $message_data['msg_creator_id']=$user_id;
            $message_data['msg_type']='1';

            $this->message_model->savePublicMessage($message_data);

            $attendee = $this->attendee_model->getUser($attendee_id);
            if($attendee['gcm_id']!='')
            {
                
                $obj = new Gcm($event_id);
                $message1=ucfirst($user['Firstname'])." has shared their contact details with you.";

                
                $extra['message_type'] = 'Private';
                $extra['message_id'] = $user_id;
                $extra['event'] = $this->settings_model->event_name($event_id);
                if($attendee['device'] == 'Iphone')
                {
                    $extra['title'] = "Contact Share";
                    $msg =  substr(strip_tags($message),0,45);//$message1;//
                    $result = $obj->send_notification($attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }   
                else
                {
                    $msg['title'] = "Contact Share";
                    $msg['message'] = substr(strip_tags($message),0,45);//$message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $result = $obj->send_notification($attendee['gcm_id'],$msg,$extra,$attendee['device']);
                }
            }
            $this->event_model->sendEmailToAttendees($event_id,strip_tags($message),$attendee['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => 'You have successfully shared your information.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function approveStatus()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');

        if($event_id!='' && $user_id!='' && $attendee_id!='')
        {
            
            $where['event_id']         = $event_id;
            $where['from_id']          = $attendee_id;
            $where['to_id']            = $user_id;

            $share_data['approval_status']  = '1';
            
            $this->attendee_model->saveShareContactInformation($where,$share_data);

            $data = array(
              'success' => true,
              'message' => 'Successfully approved.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function rejectStatus()
    {
        $event_id=$this->input->post('event_id');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');

        if($event_id!='' && $user_id!='' && $attendee_id!='')
        {
            
            $where['event_id']         = $event_id;
            $where['from_id']          = $attendee_id;
            $where['to_id']            = $user_id;

            $share_data['approval_status']  = '2';
            
            $this->attendee_model->saveShareContactInformation($where,$share_data);

            $data = array(
              'success' => true,
              'message' => 'Successfully rejected.',
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getRequestMeetingDateTime()
    {
        $event_id = $this->input->post('event_id');
        if($event_id!='')
        {
            $data = $this->attendee_model->getDateTimeArray($event_id);
        	$data['location'] = $this->attendee_model->get_meeting_location($event_id);
            
            if(empty($data['location']))
                $data['location'] = array();
            $data = array(
              'success' => true,
              'data' => $data
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    //Wednesday 24 May 2017 05:47:24 PM IST comment - use of moderator meeting
/*    public function requestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $receiver_id    = $this->input->post('attendee_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = date('H:i:s',strtotime($this->input->post('time')));
        $location       = $this->input->post('location');

        if($event_id!='' && $receiver_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $attendee = $this->attendee_model->getUser($attendee_id);

            $data['event_id'] = $event_id;
            $data['recever_attendee_id'] = $receiver_id;
            if($attendee['Role_id'] == '6')
                $data['sender_exhibitor_id'] = $attendee_id;
            else
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;
            $data['location'] = $location;

            $check_session = $this->attendee_model->checkSessionClash($attendee_id,$date,$time);
            if($check_session['result'])
            {
                $result = $this->attendee_model->saveRequest($data);
                if($result)
                {
                    $url=base_url().$this->attendee_model->getUrlData($event_id);
                    $user = $this->attendee_model->getUser($receiver_id);
                    if($attendee['Role_id'] != '6')
                    {
                        $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    else
                    {
                        $Message="<a href='".$url."'>".ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $user['Id'],
                        'Event_id'=>$event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s"),
                        'ispublic' => '0',
                        'msg_creator_id'=>$attendee_id,
                        'msg_type'  => '3',
                      );
                    $this->attendee_model->saveSpeakerMessage($data1);
                    $template = $this->message_model->getNotificationTemplate($event_id,'Meeting');
                    if($user['gcm_id']!='')
                    {
                        $obj = new Gcm($event_id);
                        $extra['message_type'] = 'AttendeeRequestMeeting';
                        $extra['message_id'] = $user['Id'];
                        $extra['event'] = $this->attendee_model->getEventName($event_id);
                        
                        if($user['device'] == 'Iphone')
                        {
                            $extra['title'] = $template['Slug'];
                            $msg = $template['Content'];
                            $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        else
                        {
                            $msg['title'] = $template['Slug'];
                            $msg['message'] = $template['Content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        
                    }
                    $this->event_model->sendEmailToAttendees($event_id,$template['Content'],$user['Email'],"Notification");
                    $data = array(
                      'success' => true,
                      'message' => "Your request was sent successfully",
                      'flag'    => '1',
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
            }
            else
            {
                $session = $check_session['agenda_name'];
                $data = array(
                  'success' => false,
                  'message' => "This will clash with $session - would you like to proceed?",
                  'flag'    => '0',
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }*/
    public function saveRequestMeeting()
    {
        //error_reporting(E_ALL);
        $event_id       = $this->input->post('event_id');
        $receiver_id    = $this->input->post('attendee_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = $this->input->post('time');
        $location       = $this->input->post('location');
        $lang_id        = $this->input->post('lang_id');

        if($event_id!='' && $receiver_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $user = $this->attendee_model->getUser($receiver_id);
            $attendee = $this->attendee_model->getUser($attendee_id);
            $data['event_id'] = $event_id;
            $data['recever_attendee_id'] = $receiver_id;
            if($attendee['Role_id'] == '6')
                $data['sender_exhibitor_id'] = $attendee_id;
            else
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;
            $data['location'] = $location;

            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
            $result = $this->attendee_model->saveRequest($data);
            if($result['status'])
            {
            $url=base_url().$this->attendee_model->getUrlData($event_id);
            
                if($attendee['Role_id'] == '6')
                    {   
                        $url=base_url().'Attendee/'.$acc_name.'/'.$Subdomain.'/checpendingkmetting';

                        $msg1 = $default_lang['notifications__request_meeting_without_modaretor'];
                        $patterns = array();
                        $patterns[0] = '/{{time}}/';
                        $patterns[1] = '/{{date}}/';
                        $replacements = array();
                        $replacements[0] = $time;
                        $replacements[1] = $date;
                        $msg1 = preg_replace($patterns, $replacements, $msg1);

                        $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." ".$msg1."</a>";
                        $message1 = ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    else
                    {   
                        $msg1 = $default_lang['notifications__request_meeting_with_modaretor'];
                        $patterns = array();
                        $patterns[0] = '/{{user_name}}/';
                        $patterns[1] = '/{{time}}/';
                        $patterns[2] = '/{{date}}/';
                        $replacements = array();
                        $replacements[0] = ucfirst($user['Firstname'])." ".ucfirst($user['Lastname']);
                        $replacements[1] = $time;
                        $replacements[2] = $date;
                        $msg1 = preg_replace($patterns, $replacements, $msg1);
                        $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." ".$msg1."</a>";
                        $message1 = ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                $data1 = array(
                    'Message' => $Message,
                    'Sender_id' => $attendee_id,
                    'Receiver_id' => $receiver_id,
                    'Event_id'=>$event_id,
                    'Parent' => '0',
                    'Time' => date("Y-m-d H:i:s"),
                    'ispublic' => '0',
                    'msg_creator_id'=>$attendee_id,
                    'msg_type'  => '3',
                  );
                    $this->attendee_model->saveSpeakerMessage($data1);
                    $template = $this->message_model->getNotificationTemplate($event_id,'Meeting');
                    if($user['gcm_id']!='')
                    {
                        $obj = new Gcm($event_id);
                        $extra['message_type'] = 'AttendeeRequestMeeting';
                        $extra['message_id'] = $user['Id'];
                        $extra['event'] = $this->attendee_model->getEventName($event_id);
                        
                        if($user['device'] == 'Iphone')
                        {
                            $extra['title'] = $template['Slug'];
                            $msg = $template['Content'];
                            $req = $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        else
                        {
                            $msg['title'] = $template['Slug'];
                            $msg['message'] = $template['Content'];
                            $msg['vibrate'] = 1;
                            $msg['sound'] = 1;
                            $req = $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                        }
                        

                    }
                    if($template['send_email'] == '1')
                    {
                    $this->event_model->sendEmailToAttendees($event_id,$template['email_content'],$user['Email'],$template['email_subject']);
                    }
                    $data = array(
                      'success' => true,
                      'message' => "Your request was sent successfully",
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => "You have already set meeting with this time and date.",
                      'flag'    => '0',
                    );
                }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }
    public function getAllMeetingRequest()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->attendee_model->getAllMeetingRequest($event_id,$user_id);
            $date_format = $this->attendee_model->checkEventDateFormat($event_id);

            if(!empty($data))
            {
                foreach ($data as $key => $value)
                {
                    if($date_format[0]['date_format'] == 0)
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("d/m/Y",$a);
                        
                    }
                    else
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("m/d/Y",$a);
                    }
                    $data[$key]['date'] = $date;    
                }
            }

            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequest()
    {
        //error_reporting(E_ALL);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id');
        $lang_id = $this->input->post('lang_id'); 

        if($status!='' && $id!='' && $receiver_id!='' && $user_id!='')
        {
            $update_data['status'] = $status;
            $where['Id'] = $id;
            $result_data = $this->attendee_model->updateRequest($update_data,$where);
            $user = $this->attendee_model->getUser($user_id);
            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);

            if($status == '2')
            {   
                $message=ucfirst($user['Firstname'])." ".$default_lang['notifications__request_meeting_reject'];
            }
            else
            {   
                $this->add_user_game_point($user_id,$event_id,3);
                $message=ucfirst($user['Firstname'])." ".$default_lang['notifications__request_meeting_accept'];
            }

            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $result_data['attendee_id'],
              'Event_id' => $event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
            );
            $this->attendee_model->saveSpeakerMessage($data1);
            $user = $this->attendee_model->getUsersData($result_data['attendee_id']);
            if($user['gcm_id']!='')
            {
                $obj = new Gcm($event_id);
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->attendee_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $extra['title'] = "Meeting Request Response";
                    $msg = substr($message,0,35);
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                
            }
            $this->event_model->sendEmailToAttendees($event_id,$message,$user['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully'  ,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function suggestMeetingTime()
    {
        $date = json_decode($this->input->post('date'),true);
        $time = json_decode($this->input->post('time'),true);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $lang_id = $this->input->post('lang_id'); 

        if($date!=''&& $time!='' && $id!='' && $receiver_id!='' && $user_id!='' && $event_id!='')
        {
            $exhibitor = $this->attendee_model->getUser($user_id);
            $user = $this->attendee_model->getUser($receiver_id);
            
            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);
            $msg1 = $default_lang['notifications__request_meeting_suggested_datetime'];
            $patterns = array();
            $patterns[0] = '/{{user_name}}/';
            $replacements = array();
            $replacements[0] = ucfirst($exhibitor['Firstname']);
            $msg1 = preg_replace($patterns, $replacements, $msg1);
            
            $message=ucfirst($exhibitor['Firstname'])." ".$msg1."<br/>";

            foreach ($date as $key => $value) {
              $temp = (strpos($time[$key], "AM")) ? str_replace(' AM', "", $time[$key]) : date("G:i", strtotime($time[$key]));
              $date=date('Y-m-d H:i',strtotime($value.' '.$temp));
              $link=base_url().$this->attendee_model->getSuggestUrlData($event_id).'changemeetingdate/'.strtotime($date).'/'.$id;
                $message.="<a href='".$link."'>".$date."</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['recever_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->attendee_model->saveSuggestedDate($suggest);
            }
            $message1 = ucfirst($exhibitor['Firstname'])." ".$msg1;
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $user['Id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
              'msg_type'  => '4',
            );
            $this->attendee_model->saveSpeakerMessage($data1);

            
            if($user['gcm_id']!='')
            {
                $obj = new Gcm($event_id);
                $extra['message_type'] = 'AttendeeSuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->attendee_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $extra['title'] = "Suggest Request Time";
                    $msg = $message1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
            }
            $this->event_model->sendEmailToAttendees($event_id,ucfirst($exhibitor['Firstname'])." ".ucfirst($exhibitor['Lastname'])." ".$msg1,$user['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function getSuggestedTimings()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $meeting_id = $this->input->post('meeting_id');

        if($user_id!='' && $event_id!='')
        {
            $where['attendee_id'] = $user_id;
            $where['sm.event_id'] = $event_id;
            if($meeting_id!='')
            $where['meeting_id'] = $meeting_id;

            $available_times = $this->attendee_model->getAvailableTimes($where);
            foreach ($available_times as $key => $value) {
                $available_times[$key]['date_time'] = date("H:i l, m/d/Y",strtotime($value['date_time']));
                $available_times[$key]['api_date_time'] = date("H:i m/d/Y",strtotime($value['date_time']));
            }
            $data = array(
              'success' => true,
              'data' => $available_times,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function bookSuggestedTime()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id'); 
        $suggested_id = $this->input->post('suggested_id');
        $date_time = $this->input->post('date_time');
        $lang_id = $this->input->post('lang_id');

        if($user_id!='' && $event_id!='' && $suggested_id!='' && $date_time!='')
        {
            $where['attendee_id'] = $user_id;
            $where['event_id'] = $event_id;
            $where['Id'] = $suggested_id;
            $default_lang=$this->app_login_model->get_default_lang_label($event_id,$lang_id);   
            $user = $this->attendee_model->bookSuggestedTime($where,$date_time);
            if(!empty($user))
            {
                $attendee = $this->attendee_model->getUsersData($user_id);

                $msg = $default_lang['notifications__request_meeting_accept_suggested_datetime'];
                $patterns = array();
                $patterns[0] = '/{{company_name}}/';
                $patterns[1] = '/{{datetime}}/';
                $replacements = array();
                $replacements[0] = $attendee['Company_name'];
                $replacements[1] = date('Y-m-d H:i',$datetime);
                $msg = preg_replace($patterns, $replacements, $msg);

                $message1=$attendee['Firstname']." ".$attendee['Lastname']." ".$msg;

                /*if($attendee['title']!='')
                    $message1 = ucfirst($attendee['Firstname']) ." at ".$attendee['title'] ." has accepted your new suggested time of $date_time. This meeting has now been booked.";
                else
                    $message1 = ucfirst($attendee['Firstname']) ." has accepted your new suggested time of $date_time. This meeting has now been booked.";*/

                $data1 = array(
                  'Message' => $message1,
                  'Sender_id' => $user_id,
                  'Receiver_id' => $user['Id'],
                  'Event_id'=>$event_id,
                  'Parent' => '0',
                  'Time' => date("Y-m-d H:i:s"),
                  'ispublic' => '0',
                  'msg_creator_id'=>$user_id
                );
                $this->attendee_model->saveSpeakerMessage($data1);

                if($user['gcm_id']!='')
                {
                    $obj = new Gcm($event_id);
                    $extra['message_type'] = 'AttendeeBookedMeeting';
                    $extra['message_id'] = $id;
                    $extra['event'] = $this->attendee_model->getEventName($event_id);
                    if($user['device'] == 'Iphone')
                    {
                        $extra['title'] = "Booked Meeting";
                        $msg = $message1;
                        $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                    }
                    else
                    {
                        $msg['title'] = "Booked Meeting";
                        $msg['message'] = $message1;
                        $msg['vibrate'] = 1;
                        $msg['sound'] = 1;
                        $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                    }
                    
                }
                $this->event_model->sendEmailToAttendees($event_id,$message1,$user['Email'],"Notification");
            }
            $data = array(
              'success' => true,
              'message' => 'Your meeting booked successfully.',
            );

        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function attendee_view_unread_count()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $attendee_id=$this->input->post('attendee_id');
        $page_no=$this->input->post('page_no');
        if($event_id!='' && $event_type!='' && $attendee_id!='' && $page_no!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $limit=10;
                $attendee = $this->attendee_model->getAttendeeDetails($attendee_id,$user_id);
                if($attendee)
                {
                $unread_count = $this->message_model->view_exibitor_private_unread_message_list($event_id,$user_id,$page_no,$limit,$attendee_id);
               
                $social_links = $this->attendee_model->getSocialLinks($attendee_id);
                $attendee[0]->Facebook_url = ($social_links[0]->Facebook_url) ? $social_links[0]->Facebook_url : '';
                $attendee[0]->Twitter_url =  ($social_links[0]->Twitter_url) ? $social_links[0]->Twitter_url : '';
                $attendee[0]->Linkedin_url = ($social_links[0]->Linkedin_url) ? $social_links[0]->Linkedin_url : '';
                $where['event_id'] = $event_id;
                $where['from_id'] = $user_id;
                $where['to_id'] = $attendee_id;
                $attendee[0]->approval_status = ($user_id != $attendee_id) ?  $this->attendee_model->getApprovalStatus($where,$event_id) : '';
                $where1['to_id'] = $user_id;
                $attendee[0]->share_details = ($user_id == $attendee_id) ? $this->attendee_model->getShareDetails($where1,$event_id) : [] ;
                $attendee[0]->contact_details = ($attendee[0]->approval_status == '1') ? $this->attendee_model->getAttendeeConatctDetails($attendee_id) : [] ;
                }
                $attendee[0]->linked_exhibitors = $this->attendee_model->get_linked_exhibitors($attendee_id);

                $allow_ex_contact = $this->attendee_model->getAllowAttendee($attendee_id,$event_id);
                $event_data = $this->attendee_model->getEvenetData($event_id);
                $myfav = $this->attendee_model->getAttendeemyfav($attendee_id,$event_id,$user_id);
                $game_point = $this->attendee_model->get_game_point($event_id,$attendee_id);

                if(!empty($myfav))
                {
                    $myfav = '1';
                }
                else
                {
                    $myfav = '0';
                }
                $data = array(
                    'attendee_details' => ($attendee) ? $attendee : [],
                    //'message' => $message,
                    'allowexhcontact' => $allow_ex_contact,
                    'my_faviorite' => $myfav,
                    //'total_pages' => $total_pages,
                    'unread_count' =>$unread_count[0]['unread_count'],
                    'attendee_hide_request_meeting' => $event_data['attendee_hide_request_meeting'],
                    'allow_meeting_exibitor_to_attendee' => $event_data['allow_meeting_exibitor_to_attendee'],
                    'game_is_on'  => $game_point['game_is_on'],
                    'game_points' => $game_point['points'],
                    'message_permisson' =>  $this->attendee_model->check_message_permisson($attendee_id,$user_id,$event_id),
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function requestMeeting()
    {
        $event_id       = $this->input->post('event_id');
        $receiver_id    = $this->input->post('attendee_id');
        $attendee_id    = $this->input->post('user_id');
        $date           = $this->input->post('date');
        $time           = date('H:i:s',strtotime($this->input->post('time')));
        $location       = $this->input->post('location');

        if($event_id!='' && $receiver_id!='' && $attendee_id!=''&& $date!='' && $time!='')
        {
            $attendee = $this->attendee_model->getUser($attendee_id);

            $data['event_id'] = $event_id;
            $data['recever_attendee_id'] = $receiver_id;
            if($attendee['Role_id'] == '6')
                $data['sender_exhibitor_id'] = $attendee_id;
            else
            $data['attendee_id'] = $attendee_id;
            $date = DateTime::createFromFormat('m-d-Y',$date);
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['time'] = $time;
            $data['location'] = $location;


            
            $check_session = $this->attendee_model->checkSessionClash($attendee_id,$date,$time);
            if($check_session['result'])
            {   
                $check_moderator = $this->attendee_model->check_moderator($event_id,$receiver_id);
                if($check_moderator)
                {
                    $moderator = $this->attendee_model->getUser($check_moderator['moderator_id']);
                    $data['moderator_id'] =  $moderator['Id'];
                    $data['recipient_attendee_id'] = $receiver_id;
                    $data['recever_attendee_id'] = NULL;
                }

                $result = $this->attendee_model->saveRequest($data);
                
                if($result['status'])
                {   
                    $url=base_url().$this->attendee_model->getUrlData($event_id);
                    $user = $this->attendee_model->getUser($receiver_id);
                    $this->add_user_game_point($attendee_id,$event_id,2);

                    if($attendee['Role_id'] != '6')
                    {
                        $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    else
                    {
                        $Message="<a href='".$url."'>".ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Company_name'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    
                    if($check_moderator)
                    {
                        $Message="<a href='".$url."'>".ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with ".ucfirst($user['Firstname'])." ".ucfirst($user['Lastname'])." at ".$time." on ".date('d/m/Y',strtotime($date))." Tap here to accept the meeting or Tap here to cancel the meeting </a>";
                        $message1 = ucfirst($attendee['Firstname'])." ".ucfirst($attendee['Lastname'])." has requested a meeting with you at ".$time." on ".date('d/m/Y',strtotime($date)).".";
                    }
                    $data1 = array(
                        'Message' => $Message,
                        'Sender_id' => $attendee_id,
                        'Receiver_id' => $user['Id'],
                        'Event_id'=>$event_id,
                        'Parent' => '0',
                        'Time' => date("Y-m-d H:i:s"),
                        'ispublic' => '0',
                        'msg_creator_id'=>$attendee_id,
                        'msg_type'  => '3',
                      );
                    if($check_moderator)
                    {
                        $data1['Receiver_id'] = $moderator['Id'];
                        $data1['msg_type'] = '5';
                    }

                    $this->attendee_model->saveSpeakerMessage($data1);
                    $template = $this->message_model->getNotificationTemplate($event_id,'Meeting');
                    if($check_moderator)
                    {
                        if($moderator['gcm_id']!='')
                        {
                            $obj = new Gcm($event_id);
                            $extra['message_type'] = 'ModeratorRequestMeeting';
                            $extra['message_id'] = $moderator['Id'];
                            $extra['event'] = $this->attendee_model->getEventName($event_id);
                            
                            if($moderator['device'] == 'Iphone')
                            {
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send_notification($moderator['gcm_id'],$msg,$extra,$moderator['device']);
                            }
                            else
                            {
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($moderator['gcm_id'],$msg,$extra,$moderator['device']);
                            }
                            
                        } 
                    }
                    else
                    {
                        if($user['gcm_id']!='')
                        {
                            $obj = new Gcm($event_id);
                            $extra['message_type'] = 'AttendeeRequestMeeting';
                            $extra['message_id'] = $user['Id'];
                            $extra['event'] = $this->attendee_model->getEventName($event_id);
                            
                            if($user['device'] == 'Iphone')
                            {
                                $extra['title'] = $template['Slug'];
                                $msg = $template['Content'];
                                $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                            }
                            else
                            {
                                $msg['title'] = $template['Slug'];
                                $msg['message'] = $template['Content'];
                                $msg['vibrate'] = 1;
                                $msg['sound'] = 1;
                                $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                            }
                            
                        }  
                    }

                    if($template['send_email'] == '1')
                    {
                    $this->event_model->sendEmailToAttendees($event_id,$template['email_content'],$user['Email'],$template['email_subject']);
                    }
                    $data = array(
                      'success' => true,
                      'message' => "Your request was sent successfully",
                      'flag'    => '1',
                    );
                }
                else
                {
                    $data = array(
                      'success' => true,
                      'message' => $result['msg'],
                      'flag'    => '0',
                    );
                }
            }
            else
            {
                $session = $check_session['agenda_name'];
                $data = array(
                  'success' => false,
                  'message' => "This will clash with $session - would you like to proceed?",
                  'flag'    => '0',
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }

        echo json_encode($data);
    }
    public function getAllMeetingRequestModerator()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if($event_id!='' && $user_id!='')
        {
            $data = $this->attendee_model->getAllMeetingRequestModerator($event_id,$user_id);
            $date_format = $this->attendee_model->checkEventDateFormat($event_id);

            if(!empty($data))
            {
                foreach ($data as $key => $value)
                {
                    if($date_format[0]['date_format'] == 0)
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("d/m/Y",$a);
                        
                    }
                    else
                    {   
                        $a =  strtotime($value['date']);
                        $date=date("m/d/Y",$a);
                    }
                    $data[$key]['date'] = $date;    
                }
            }

            $data = array(
              'success' => true,
              'data' => $data,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function respondRequestModerator()
    {
        //error_reporting(E_ALL);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('receiver_id');
        $sender_id = $this->input->post('sender_id');
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status'); // 1 / 2
        $event_id = $this->input->post('event_id');

        if($status!='' && $id!='' && $receiver_id!='' && $user_id!='' && $sender_id!='' )
        {
            $update_data['status'] = $status;
            $update_data['recever_attendee_id'] =  $receiver_id;
            $where['Id'] = $id;
            $result_data = $this->attendee_model->updateRequestModerator($update_data,$where);
            $user = $this->attendee_model->getUser($user_id);
            if($status == '2')
            {
                $message=ucfirst($user['Firstname'])." has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message=ucfirst($user['Firstname'])." has accepted your Meeting Request.";
            }
            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $result_data['attendee_id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
            );
            $this->attendee_model->saveSpeakerMessage($data1);
            $user = $this->attendee_model->getUsersData($sender_id);
            
            if($user['gcm_id']!='')
            {
                $obj = new Gcm($event_id);
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->attendee_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                
            }
            $this->event_model->sendEmailToAttendees($event_id,$message,$user['Email'],"Notification");

            //Reciver Notifictaion
            $reciver = $this->attendee_model->getUser($receiver_id);
            if($status == '2')
            {
                $message=ucfirst($user['Firstname'])." has denied your meeting request due to a clash. Please try another time.";
            }
            else
            {
                $message="You have a meeting scheduled with ".ucfirst($user['Firstname'])." ".$user['Lastname']." at ".$result_data['time']." ".$result_data['date']." at ".$result_data['location'];

            }

            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $receiver_id,
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
            );
            $this->attendee_model->saveSpeakerMessage($data1);
            $user = $this->attendee_model->getUsersData($receiver_id);
            if($user['gcm_id']!='')
            {
                $obj = new Gcm($event_id);
                $extra['message_type'] = 'AttendeeRespondRequest';
                $extra['message_id'] = $user['Id'];
                $extra['event'] = $this->attendee_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $extra['title'] = "Meeting Request Response";
                    $msg = $message;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Meeting Request Response";
                    $msg['message'] = $message;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                
            }
            $this->event_model->sendEmailToAttendees($event_id,$message,$user['Email'],"Notification");

            $data = array(
              'success' => true,
              'message' => ($status == '1') ? 'Request accepted successfully' : 'Request rejected successfully'  ,
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function suggestMeetingTimeModerator()
    {
        $date = json_decode($this->input->post('date'),true);
        $time = json_decode($this->input->post('time'),true);
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('sender_id');
        //$user_id = $this->input->post('user_id');
        $user_id = $this->input->post('receiver_id');
        $event_id = $this->input->post('event_id');
        $comment = $this->input->post('comment'); 

        if($date!=''&& $time!='' && $id!='' && $receiver_id!='' && $user_id!='' && $event_id!='')
        {
            
            $exhibitor = $this->attendee_model->getUser($user_id);
            $user = $this->attendee_model->getUser($receiver_id);
            $data['location'] = $comment;
            $where['Id'] = $id;
            $save_comment = $this->attendee_model->updateRequestModerator($data,$where);

            $message=ucfirst($exhibitor['Firstname'])." is unable to meet with you at the time you specified. ".ucfirst($exhibitor['Firstname'])." has suggested the following times to meet with you instead. Please click on one of the following times to confirm your meeting:<br/>";

            foreach ($date as $key => $value) 
            {
              $temp = (strpos($time[$key], "AM")) ? str_replace(' AM', "", $time[$key]) : date("G:i", strtotime($time[$key]));
              $date=date('Y-m-d H:i',strtotime($value.' '.$temp));
              $link=base_url().$this->attendee_model->getSuggestUrlData($event_id).'changemeetingdate/'.strtotime($date).'/'.$id;
              $message.="<a href='".$link."'>".$date."</a><br/>";
                $suggest['meeting_id'] = $id;
                $suggest['recever_user_id'] = $user_id;
                $suggest['attendee_id'] = $user['Id'];
                $suggest['event_id'] = $event_id;
                $suggest['date_time'] = $date;
                $this->attendee_model->saveSuggestedDate($suggest);
            }

            $message1 = ucfirst($exhibitor['Firstname'])." is unable to meet with you at this time.Please click here.";

            $data1 = array(
              'Message' => $message,
              'Sender_id' => $user_id,
              'Receiver_id' => $user['Id'],
              'Event_id'=>$event_id,
              'Parent' => '0',
              'Time' => date("Y-m-d H:i:s"),
              'ispublic' => '0',
              'msg_creator_id'=>$user_id,
              'msg_type'  => '4',
            );

            $this->attendee_model->saveSpeakerMessage($data1);

            if($user['gcm_id']!='')
            {
                $obj = new Gcm($event_id);
                $extra['message_type'] = 'AttendeeSuggestRequestTime';
                $extra['message_id'] = $id;
                $extra['event'] = $this->attendee_model->getEventName($event_id);
                if($user['device'] == 'Iphone')
                {
                    $extra['title'] = "Suggest Request Time";
                    $msg = $message1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                else
                {
                    $msg['title'] = "Suggest Request Time";
                    $msg['message'] = $message1;
                    $msg['vibrate'] = 1;
                    $msg['sound'] = 1;
                    $obj->send_notification($user['gcm_id'],$msg,$extra,$user['device']);
                }
                
            }
            $this->event_model->sendEmailToAttendees($event_id,ucfirst($exhibitor['Firstname'])." ".ucfirst($exhibitor['Lastname'])." is unable to meet with you at this time.",$user['Email'],"Notification");
            $data = array(
              'success' => true,
              'message' => "You have successfully suggested another time.",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function SaveCommentModerator()
    {
        $id = $this->input->post('request_id');
        $receiver_id = $this->input->post('sender_id');
        $user_id = $this->input->post('reciver_id');
        $event_id = $this->input->post('event_id');
        $comment = $this->input->post('comment'); 

        if($id!='' && $receiver_id!='' && $user_id!='' && $event_id!='')
        {
            
            $data['location'] = $comment;
            $where['Id'] = $id;
            $save_comment = $this->attendee_model->updateRequestModerator($data,$where);

        
            $data = array(
              'success' => true,
              'message' => "Comment successfully",
            );
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function add_user_game_point($user_id,$event_id,$rank_id)
    {   
        $this->gamification_model->add_user_point($user_id,$event_id,$rank_id);
        return true;
    }
}