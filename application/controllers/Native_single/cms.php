<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/attendee_model');

        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'),$this->input->post('lang_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'),$this->input->post('lang_id'));

        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function get_cms_page()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $cms_id=$this->input->post('cms_id');
        $token=$this->input->post('token');
        $lang_id = $this->input->post('lang_id');

        if($event_id!='' && $event_type!='' && $cms_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $cms_page=$this->cms_model->get_cms_page($event_id,$cms_id,$token,$lang_id);
               
                if(!empty($cms_page[0]['Images']))
                {
                    $cmsbannerimage_decode = json_decode($cms_page[0]['Images']);
                    $cms_page[0]['Images'] = $cmsbannerimage_decode[0];
                }
                if(!empty($cms_page[0]['Logo_images']))
                {
                    $cmslogoimage_decode = json_decode($cms_page[0]['Logo_images']);
                    $cms_page[0]['Logo_images'] = $cmslogoimage_decode[0];
                }

                $data = array(
                    'cms_details' => $cms_page
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
