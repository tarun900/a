<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Qr_code extends CI_Controller     
{
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/qr_code_model');
    }

    public function getQrCodeOfLoggedInAttendee()
    {
        $event_id = $this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $attendee_id = $this->input->post('user_id');
        $token=$this->input->post('token');

        if($event_id !='' && $event_type !='' && $attendee_id !='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                    $data = array(
                        'success' => false,
                        'data' => array(
                            'msg' => 'You must first log in or register to create a QR Code.'
                            )
                        ); 
            } 
            else 
            {
                    $qr_codes = $this->qr_code_model->getQrCodeOfLoggedInAttendee($event_id,$attendee_id);
                    $data = array(
                          'success'     => true,
                          'data'        => $qr_codes,
                    );
            }
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function getQrCodeOfLoggedInAttendeeNew()
    {
        $event_id = $this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $attendee_id = $this->input->post('user_id');
        $token=$this->input->post('token');

        if($event_id !='' && $event_type !='' && $attendee_id !='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                    $data = array(
                        'success' => false,
                        'data' => array(
                            'msg' => 'You must first log in or register to create a QR Code.'
                            )
                        ); 
            } 
            else 
            {       
                    $agenda = $this->qr_code_model->getAgendaByUserId($attendee_id,$event_id);
                    
                    $this->load->library('ciqrcode');
                    $data['event_id'] = $event_id;
                    $data['user_id'] = $attendee_id;
                    $data['agenda'] =  $agenda;  
                    $params['data'] = json_encode($data);
                    $params['level'] = 'H';
                    $params['size'] = 10;
                    $params['savename'] = FCPATH.'assets/user_files/qr_code/'.$attendee_id.'.png';
                    $this->ciqrcode->generate($params);

                    $data = array(
                          'success'     => true,
                          'data'        => base_url('assets/user_files/qr_code').'/'.$attendee_id.'.png',
                    );
            }
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
    public function checkAgendaByUserId()
    {
        $event_id = $this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $attendee_id = $this->input->post('user_id');
        $agenda_id = $this->input->post('agenda_id');
        $token=$this->input->post('token');

        if($event_id !='' && $event_type !='' && $attendee_id !='' && $agenda_id !='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                    $data = array(
                        'success' => false,
                        'data' => array(
                            'msg' => 'You must first log in or register to create a QR Code.'
                            )
                        ); 
            } 
            else 
            {       
                    $agenda = $this->qr_code_model->getAgendaByUserId($attendee_id,$event_id);
                    if(in_array($agenda_id,$agenda))
                    {
                        $data = array(
                              'success'     => true,
                              'data'        => "Allow Entry",
                        );   
                    }
                    else
                    {
                        $data = array(
                              'success'     => true,
                              'data'        => "Deny Entry",
                        );
                    }
            }
        }
        else
        {
            $data = array(
                  'success'     => false,
                  'message'     => 'Invalid parameters',
            );
        }
        echo json_encode($data);
    }
}
?>