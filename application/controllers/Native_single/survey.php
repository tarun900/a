<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Survey extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/survay_model');
        $this->load->model('native_single/gamification_model');
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }
    public function get_survey()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user=$this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $survey_screens = $this->survay_model->get_survey_screens($event_id);
                $survey = $this->survay_model->get_survey($event_id,$user_id);
               
                if(empty($survey))
                {
                   
                    $survey=array([0]=>"");
                }
                if(empty($survey_screens))
                {
                    $survey_screens=array([0]=>"");
                }
                $data = array(
                    'survey' => $survey,
                    'survey_screens' => $survey_screens,
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function saveSurvey()
    {
        $event_id=$this->input->post('event_id');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $survey_json_data=$this->input->post('survey_json_data');
        $survey_data=json_decode($survey_json_data,true);
        $category_id = $this->input->post('category_id');

        for($i=0;$i<sizeof($survey_data);$i++)
        {
             $survey_data[$i]['answer_date'] = date('Y-m-d H:i:s');
        }

        if($event_id!='' && $token!=''  && $user_id!='' && !empty($survey_data))
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,1);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {   
                $status=$this->survay_model->saveSurvey($survey_data);
                $this->add_user_game_point($user_id,$event_id,6);

                if(!empty($category_id))
                {
                    $survey_screens = $this->survay_model->get_survey_screens($event_id,$category_id);
                    $survey = $this->survay_model->get_category_wise_survey($event_id,$user_id,$category_id);
                   
                    if(empty($survey))
                    {
                        $survey=array([0]=>"");
                    }
                    if(empty($survey_screens))
                    {
                        $survey_screens=array([0]=>"");
                    }
                    $data = array(
                        'survey' => $survey,
                        'survey_screens' => $survey_screens,
                    );
                }

                if($status==1)
                {
                    $data = array(
                      'success' => true,
                      'data' =>$data,
                      'message' => "Successfully saved"
                     );
                }
                else
                {
                    $data = array(
                      'success' => false,
                      'message' => "Something went wrong Please try again"
                     );
                }
                
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_survey_category()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        if($event_id!='' && $event_type!='')
        {
            $user=$this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $survey = $this->survay_model->get_survey_category($event_id);

                if(empty($survey))
                {
                    $survey=array([0]=>"");

                }
                
                $data = array(
                    'survey' => $survey,
                    'message' => (!empty($survey)) ? "" : "There are no available surveys right now.",
                    'is_blank' => (!empty($survey)) ? 0 : 1,
                  
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function get_category_wise_survey()
    {

        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $user_id=$this->input->post('user_id');
        $category_id = $this->input->post('category_id');
        $lang_id=$this->input->post('lang_id');

        if($event_id!='' && $event_type!='' && $category_id!='')
        {
            $user=$this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if(empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $survey_screens = $this->survay_model->get_survey_screens($event_id,$category_id);
                $survey = $this->survay_model->get_category_wise_survey($event_id,$user_id,$category_id,$lang_id);
                $msg = $this->survay_model->is_survey_avilable($event_id,$category_id);

                if(empty($survey))
                {
                   
                    $survey=array([0]=>"");
                }
                if(empty($survey_screens))
                {
                    $survey_screens=array([0]=>"");
                }

                $data = array(
                    'survey' => $survey,
                    'survey_screens' => $survey_screens,
                    'message' => ($msg['state'] == 1) ? $msg['msg'] : "",
                    'is_blank' => $msg['state'],
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
    public function add_user_game_point($user_id,$event_id,$rank_id)
    {   
        $this->gamification_model->add_user_point($user_id,$event_id,$rank_id);
        return true;
    }
}
