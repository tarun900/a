<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sponsors extends CI_Controller 
{
    public $menu;
    public $cmsmenu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model('native_single/app_login_model');
        $this->load->model('native_single/cms_model');
        $this->load->model('native_single/event_model');
        $this->load->model('native_single/sponsors_model');
        
        $this->menu_list = $this->event_model->geteventmenu_list($this->input->post('event_id'), null, null,$this->input->post('user_id'));
        $this->cmsmenu = $this->cms_model->get_cms_page($this->input->post('event_id'));
        foreach ($this->cmsmenu as $key => $values) 
        {
            $cmsbannerimage_decode = json_decode($values['Images']);
            $cmslogoimage_decode = json_decode($values['Logo_images']);
            $this->cmsmenu[$key]['Images'] = $cmsbannerimage_decode[0];
            $this->cmsmenu[$key]['Logo_images'] = $cmsbannerimage_decode[0];
        }
    }

    public function sponsors_list()
    {
        //$start_fun_time = microtime(true);
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        if($event_id!='' && $event_type!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $user_id = $this->sponsors_model->getUserId($token);
                $sponsors_list = $this->sponsors_model->getSponsorsListByEventId($event_id,$user_id);
                foreach ($sponsors_list as $key1 => $value1)
                {   
                   
                    foreach ($value1['data'] as $key => $value) 
                        {    
                            
                            if(!empty($value['company_logo']))
                            {
                                $source_url = base_url()."assets/user_files/".$value['company_logo'];
                                $info = getimagesize($source_url);
                                $new_name = "new_".$value['company_logo'];
                                $destination_url = $_SERVER['DOCUMENT_ROOT']."/assets/user_files/".$new_name;

                                if ($info['mime'] == 'image/jpeg')
                                {   
                                    $quality = 50;
                                    $image = imagecreatefromjpeg($source_url);
                                    imagejpeg($image, $destination_url, $quality);
                                }
                                elseif ($info['mime'] == 'image/gif')
                                {   
                                    $quality = 5;
                                    $image = imagecreatefromgif($source_url);
                                    imagegif($image, $destination_url, $quality);

                                }
                                elseif ($info['mime'] == 'image/png')
                                {   
                                    $quality = 5;
                                    $image = imagecreatefrompng($source_url);

                                    $background = imagecolorallocatealpha($image,255,0,255,127);
                                    imagecolortransparent($image, $background);
                                    imagealphablending($image, false);
                                    imagesavealpha($image, true);
                                    imagepng($image, $destination_url, $quality);
                                }
                                
                                $sponsors_list[$key1]['data'][$key]['company_logo'] = $new_name;
                            }
                        }
                }
                $data = array(
                    'sponsors_list' => $sponsors_list,
                   /* 'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        /*$end_fun_time = microtime(true);
        $file_handle = fopen(dirname(__FILE__).'/function_log_files.txt','a'); 
        fwrite($file_handle,"\n ".date('Y-m-d H:i:s')." On This date Execute This Function native/sponsors/sponsors_list function take time To Respond Is  : ".($end_fun_time -  $start_fun_time)." seconds. ");
        fclose($file_handle);*/
        echo json_encode($data);
    }
    public function sponsors_view()
    {
        $event_id=$this->input->post('event_id');
        $event_type=$this->input->post('event_type');
        $token=$this->input->post('token');
        $sponsor_id=$this->input->post('sponsor_id');
        $lang_id = $this->input->post('lang_id');
        if($event_id!='' && $event_type!='' && $sponsor_id!='')
        {
            $user = $this->app_login_model->check_token_with_event($token,$event_id,$event_type);
            if (empty($user)) 
            {
                $data = array(
                    'success' => false,
                    'data' => array(
                        'msg' => 'Please check token or event.'
                    )
                );   
            } 
            else 
            {
                $sponsors = $this->sponsors_model->getSponsorsDetails($sponsor_id,$event_id,$token,$lang_id);
                if(!empty($sponsors[0]))
                {
                    if(!empty($sponsors[0]['company_logo']))
                    {
                        $company_logo_decode=json_decode($sponsors[0]['company_logo']);
                        $sponsors[0]['company_logo']=($company_logo_decode[0]) ? $company_logo_decode[0] : '';
                    }

                    if(!empty($sponsors[0]['Images']))
                    {
                        if($sponsors[0]['Images']=="[]")
                        {

                            $sponsors[0]['Images']=[];
                        }
                        else
                        {

                            $image_decode=json_decode($sponsors[0]['Images']);
                            $sponsors[0]['Images']=($image_decode) ? : [] ;
                        }
                        
                    }
                }
                $data = array(
                    'sponsors_details' => ($sponsors[0]) ? $sponsors[0] : new stdClass,
                   /* 'menu' => $this->menu_list,
                    'cmsmenu' => $this->cmsmenu*/
                );
                
                $data = array(
                  'success' => true,
                  'data' => $data
                );
            }
        }
        else
        {
            $data = array(
              'success' => false,
              'message' => "Invalid parameters"
            );
        }
        echo json_encode($data);
    }
}
