<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Gamification_admin extends FrontendController

{
     function __construct()
     {
          $this->data['pagetitle'] = 'Gamification';
          $this->data['smalltitle'] = 'Gamification';
          $this->data['breadcrumb'] = 'Gamification';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $user = $this->session->userdata('current_user');
          $eventid = $this->uri->segment(3);
          $this->load->database();
          $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
          $user_events = array_filter(array_column_1($user_events, 'Event_id'));
          if (!in_array($eventid, $user_events))
          {
               redirect('Forbidden');
          }
          $eventmodule = $this->Event_model->geteventmodulues($eventid);
          $module = json_decode($eventmodule[0]['module_list']);
          if (!in_array('52', $module))
          {
               redirect('Forbidden');
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          $roledata = $this->Event_model->getUserRole($eventid);
          if (!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Gamification")
               {
                    $title = "Gamification";
               }
               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
          }
          else
          {
               $cnt = 0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          if ($cnt == 1 && in_array('52', $menu_list))
          {
               $this->load->model('Gamification_model');
               $this->load->model('Agenda_model');
               $this->load->model('Event_template_model');
               $roles = $this->Event_model->get_menu_list($roleid, $eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               redirect('Forbidden');
          }
     }
     public function index($id=NULL)

     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
          $this->data['users_role'] = $user_role;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $menudata = $this->Event_model->geteventmenu($id, 52);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $rank = $this->Gamification_model->get_all_rank_type();
          $this->data['rank'] = $rank;
          $rank_point = $this->Gamification_model->get_all_rank_point_by_event($id);
          $this->data['rank_point'] = $rank_point;
          $rank_color = $this->Gamification_model->get_all_rank_color_by_event($id);
          $this->data['rank_color'] = $rank_color;
          $game_header = $this->Gamification_model->get_game_header($id);
          $this->data['game_header'] = $game_header;
          $this->data['menu_toal_data'] = $menu_toal_data;
          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
          $this->template->write_view('css', 'admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'gamification_admin/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'gamification_admin/js', $this->data, true);
          $this->template->render();
     }
     public function save_gamification_setting($event_id=NULL)

     {
          $all_rank = $this->Gamification_model->get_all_rank_type();
          $this->data['all_rank'] = $all_rank;
          foreach($all_rank as $key => $value)
          {
               $val = ($this->input->post($value['id'])) ? $this->input->post($value['id']) : 0;
               $this->Gamification_model->save_game_rank_point($val, $value['id'], $event_id);
          }
          $this->session->set_flashdata('gamification_data', 'Gamification Point Saved SuccessFully.');
          redirect(base_url() . 'Gamification_admin/index/' . $event_id);
     }
     public function save_rank_color($event_id=NULL)

     {
          for ($i = 1; $i <= 5; $i++)
          {
               $this->Gamification_model->save_game_rank_color($this->input->post($i) , $i, $event_id);
          }
          $this->session->set_flashdata('gamification_data', 'Gamification Rank Color Saved SuccessFully.');
          redirect(base_url() . 'Gamification_admin/index/' . $event_id);
     }
     public function save_header($event_id=NULL)

     {
          $this->Gamification_model->save_game_header($this->input->post('header_content') , $event_id);
          $this->session->set_flashdata('gamification_data', 'Gamification Header Saved SuccessFully.');
          redirect(base_url() . 'Gamification_admin/index/' . $event_id);
     }
}