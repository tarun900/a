<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Sponsors_user extends FrontendController
{

     function __construct()
     {  
          $this->data['pagetitle'] = 'Your Sponsors';
          $this->data['smalltitle'] = 'Organise your Sponsors lists and user portals.';
          $this->data['breadcrumb'] = 'Your Sponsors';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Exibitor_model');
          $this->load->model('Sponsors_model');
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $this->load->model('Map_model');
          $this->load->model('Event_template_model');    
          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }


          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());
               if ($this->data['pagetitle'] == "Your Sponsors")
               {
                    $title = "Exhibitors";
               }
               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1)
          {
               $this->load->model('Exibitor_model');
               $this->load->model('Sponsors_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->model('Event_template_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               $this->load->model('Exibitor_model');
               $this->load->model('Sponsors_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');
               $this->load->model('Profile_model');
               $this->load->library('upload');
          }
     }

     public function index($id = NULL)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $users = $this->Sponsors_model->get_sponsors_list_byevent($id,$orid);
          $this->data['users']=$users;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;

          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $sponsors_list = $this->Sponsors_model->get_users_sponsors_list($orid);
          $this->data['sponsors_list'] = $sponsors_list;

          $menudata = $this->Event_model->geteventmenu($id, 43);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $this->template->write_view('css', 'sponsors_user/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'sponsors_user/index', $this->data, true);
          if ($this->data['user']->Role_name == 'User')
          {

               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'sponsors_user/js', $this->data, true);
          $this->template->render();
     }

     public function add($id = NULL)
     {

          $Event_id = $id;
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          if ($user[0]->Role_name == 'Client')
          {

               $Organisor_id = $user[0]->Id;
          }
          else
          {
               $Organisor_id = $this->Event_model->get_org_id_by_event($id);
          }
          $logged_in_user_id = $user[0]->Id;
       
          if ($this->input->post())
          {

               if (!empty($_FILES['company_logo']['name'][0]))
               {

                    $tempname = explode('.', $_FILES['company_logo']['name']);
                    $tempname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $tempname[0]);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];

                    $this->upload->initialize(array(
                            "file_name" => $images_file,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                    ));

                    if (!$this->upload->do_multi_upload("company_logo"))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error', $error['error']);
                    }
               }

               if (!empty($_FILES['Images']['name'][0]))
               {
                    foreach ($_FILES['Images']['name'] as $k => $v)
                    {
                         $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);
                         if (file_exists("./assets/user_files" . $v))
                              $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         else
                              $Images[] = $v;
                    }

                    $this->upload->initialize(array(
                            "file_name" => $Images,
                            "upload_path" => "./assets/user_files",
                            "allowed_types" => '*',
                            "max_size" => '100000',
                            "max_width" => '5000',
                            "max_height" => '5000'
                    ));

                    if (!$this->upload->do_multi_upload("Images"))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error', "" . $error['error']);
                    }
               }

               $data['sponsors_array']['Organisor_id'] = $Organisor_id;
               $data['sponsors_array']['user_id']=$logged_in_user_id;
               $data['sponsors_array']['Event_id'] = $Event_id;
               $data['sponsors_array']['Company_name'] = $this->input->post('Company_name');
               $data['sponsors_array']['Description'] = $this->input->post('Description');
               $data['sponsors_array']['Images'] = $Images;
               $data['sponsors_array']['website_url'] = $this->input->post('website_url');
               $data['sponsors_array']['facebook_url'] = $this->input->post('facebook_url');
               $data['sponsors_array']['twitter_url'] = $this->input->post('twitter_url');
               $data['sponsors_array']['linkedin_url'] = $this->input->post('linkedin_url');
               $data['sponsors_array']['company_logo'] = $images_file;

               $sponsors_id = $this->Sponsors_model->add_user_as_sponsors($data);
               $this->session->set_flashdata('exibitor_data', 'Added');
               redirect("Sponsors_user/index/" . $id);
          }
          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;

          $statelist = $this->Profile_model->statelist();
          $this->data['Statelist'] = $statelist;

          $countrylist = $this->Profile_model->countrylist();
          $this->data['countrylist'] = $countrylist;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $agenda_list = $this->Agenda_model->get_agenda_list($id);
          $this->data['agenda_list'] = $agenda_list;

          $this->template->write_view('css', 'sponsors_user/add_css', $this->data, true);
          $this->template->write_view('content', 'sponsors_user/add', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'sponsors_user/add_js', $this->data, true);
          $this->template->render();

     }

     public function edit($eid = NULL,$id = NULL)
     {
          $Event_id = $eid;
          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $event_id = $id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$eid);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $event = $this->Event_model->get_admin_event($eid);
          $this->data['event'] = $event[0];

          $sponsors_list = $this->Sponsors_model->get_sponsors_list($id);
          $this->data['sponsors_list'] = $sponsors_list;

          $sponsors_by_id = $this->Sponsors_model->get_sponsors_by_id($eid,$id);
          $this->data['sponsors_by_id'] = $sponsors_by_id;

          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Exibitor');
               }

               if ($this->input->post())
               {
                    if (!empty($_FILES['company_logo']['name'][0]))
                    {

                         $imgname = explode('.', $_FILES['company_logo']['name']);
                         $imgname[0] = preg_replace("/[^a-zA-Z0-9.]/", "", $imgname[0]);
                         $tempname_imagename = $imgname[0] . strtotime(date("Y-m-d H:i:s"));
                         $images_file = $tempname_imagename . "." . $imgname[1];

                         $this->upload->initialize(array(
                                 "file_name" => $images_file,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => 'gif|jpg|png|jpeg',
                                 "max_size" => '100000',
                                 "max_width" => '5000',
                                 "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("company_logo"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', $error['error']);
                         }
                    }

                    if (!empty($_FILES['Images']['name'][0]))
                    {
                         foreach ($_FILES['Images']['name'] as $k => $v)
                         {

                              $v = preg_replace("/[^a-zA-Z0-9.]/", "", $v);

                              if (file_exists("./assets/user_files" . $v))
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                              else
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                         }
                         $this->upload->initialize(array(
                                 "file_name" => $Images,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => 'gif|jpg|png|jpeg',
                                 "max_size" => '100000',
                                 "max_width" => '5000',
                                 "max_height" => '5000'
                         ));

                         if (!$this->upload->do_multi_upload("Images"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                         }
                    }

                    $data['sponsors_array']['Id'] = $this->uri->segment(4);
                    $data['sponsors_array']['Organisor_id'] = $logged_in_user_id;
                    $data['sponsors_array']['Event_id'] = $Event_id;
                    $data['sponsors_array']['Company_name'] = $this->input->post('Company_name');
                    $data['sponsors_array']['Description'] = $this->input->post('Description');
                    $data['sponsors_array']['website_url'] = $this->input->post('website_url');
                    $data['sponsors_array']['facebook_url'] = $this->input->post('facebook_url');
                    $data['sponsors_array']['twitter_url'] = $this->input->post('twitter_url');
                    $data['sponsors_array']['linkedin_url'] = $this->input->post('linkedin_url');
                    $data['sponsors_array']['Images'] = $Images;
                    $data['sponsors_array']['old_images'] = $this->input->post('old_images');
                    $data['sponsors_array']['company_logo'] = $images_file;
                    $data['sponsors_array']['old_company_logo'] = $this->input->post('old_company_logo');
                    $this->Sponsors_model->update_sponsors($data);
                    $this->session->set_flashdata('sponsors_data', 'Updated');
                    redirect("Sponsors_user/index/" . $id);
               }

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'sponsors_user/add_css', $this->data, true);
               $this->template->write_view('content', 'sponsors_user/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Sponsors_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'sponsors_user/add_js', $this->data, true);
               $this->template->render();
          }
     }

     public function delete($id = NULL, $Event_id = NULL)
     {

          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $sponsors_list = $this->Sponsors_model->get_exibitor_sponsors($id);
               $this->data['sponsors_list'] = $sponsors_list[0];
          }

          $sponsors = $this->Sponsors_model->delete_sponsors($id,$Event_id);
          $this->session->set_flashdata('sponsors_data', 'Deleted');
          redirect("Sponsors_user/index/" . $Event_id);
     }
}