<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends FrontendController {
	function __construct() {
          $this->data['pagetitle'] = 'Email Templates';
          $this->data['smalltitle'] = 'Organize and edit the Email Templates that you send to your App users';
          $this->data['breadcrumb'] = 'Email Templates';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Setting_model');
          $this->load->library('upload');
          $eventid=$this->uri->segment(3);
          $user = $this->session->userdata('current_user');
          
          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $user = $this->session->userdata('current_user');
          
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
              if ($this->data['pagetitle'] == "Settings")
               {
                    $title = "Email Templates";
               }
               $req_mod = ucfirst($this->router->fetch_class());

               $cnt = $this->Agenda_model->check_auth("Email Templates", $roleid, $rolename,$eventid);
              
          }
          else
          {
               $cnt=0;
          }
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }
          if ($cnt == 1)
          {
               $this->load->model('Setting_model');
               $this->load->model('Speaker_model');
               $this->load->model('Profile_model');
               $this->load->model('Agenda_model');
               $this->load->model('Event_template_model');
               $this->load->model('Cms_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
            
            
	}

	public function index()                 
	{
    redirect('Forbidden');
	}
  public function fbloginsetting($id = NULL)
  {
      if($this->input->post())
      {
        if(isset($_POST['displaybtn']) && $_POST['displaybtn']=="Update")
        {
          $eid=$id;
          if($this->input->post('both')=='yes')
          {
            $email_display='1';
            $pushnoti_display='1';
          }
          else
          {
            if($this->input->post('emailsend')=='yes')
            {
              $email_display='1';
            }
            else
            {
              $email_display='0';
            }

            if($this->input->post('pushnoti')=='yes')
            {
              $pushnoti_display='1';
            }
            else
            {
              $pushnoti_display='0';
            }
          }
          $arr=array('email_display'=>$email_display,
                'pushnoti_display'=>$pushnoti_display,
                'Event_id'=>$eid);
          $this->Setting_model->addnotificationsetting($arr);
        }
        if(isset($_POST['facebook_login_btn']) && $_POST['facebook_login_btn']!="")
        {
          $raised=0;
          if($_POST['facebook_login']==1)
          {
            $facebook_login="1";
          }
          else
          {
            $facebook_login="0";
          }
          $this->Event_model->add_facebook_login($facebook_login,$id);
        }
      }
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $event_id = $id;
          $this->data['notificationsetting']=$this->Event_model->getnotificationsetting($id);
          $this->template->write_view('css', 'setting/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'setting/fbloginform', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'setting/js', true);
          $this->template->render();
  }
  public function email_templates($id = NULL)
	{       
    $eventmodule=$this->Event_model->geteventmodulues($id);
    $this->data['eventmodule']=$eventmodule;
    $event = $this->Event_model->get_module_event($id);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('28',$module))
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

    if ($this->data['user']->Role_name == 'User')
    {
         $total_permission = $this->Formbuilder_model->get_permission_list();
         $this->data['total_permission'] = $total_permission;
    }

    $event = $this->Event_model->get_admin_event($id);
    $this->data['event'] = $event[0];

    $menudata = $this->Event_model->geteventmenu($id,28);
    $menu_toal_data = $this->Event_model->get_total_menu($id);
    $this->data['menu_toal_data'] = $menu_toal_data;
    
    $this->data['event_id'] = $id;
    $this->data['menu_id'] = $menudata[0]->id;
    $this->data['title'] = $menudata[0]->menuname;
    $this->data['img'] = $menudata[0]->img;
    $this->data['img_view'] = $menudata[0]->img_view;
    $this->data['is_feture_product'] = $menudata[0]->is_feture_product;


    $this->data['email_template'] = $this->Setting_model->email_template($id);
    
    $this->template->write_view('css', 'setting/css', $this->data , true);
    $this->template->write_view('header', 'common/header', $this->data, true);
    $this->template->write_view('content', 'setting/index', $this->data , true);
    if ($this->data['user']->Role_name == 'User')
    {
         $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
    }
    else
    {
         $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
    }
    $this->template->write_view('js', 'setting/js', $this->data , true);
    $this->template->render();
	}

  public function edit($intEventId = null, $intTempId = null)
  {
    $eventmodule=$this->Event_model->geteventmodulues($intEventId);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('28',$module))
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
        $user = $this->session->userdata('current_user');

        $roleid = $user[0]->Role_id;
        $user_role=$this->Event_template_model->get_menu_list($roleid,$intEventId);
        $this->data['users_role']=$user_role;

        $logged_in_user_id = $user[0]->Id;

        $menu_toal_data = $this->Event_model->get_total_menu($intEventId);
        $this->data['menu_toal_data'] = $menu_toal_data;
        
        $cms_list = $this->Cms_model->get_all_cms_list($intEventId);
        $this->data['cms_list'] = $cms_list;

        $event = $this->Event_model->get_admin_event($intEventId);
        $this->data['event'] = $event[0];
      
      
      $temppost = $this->input->post();
      if(!empty($temppost))
      {
          $arrUpdate = array();
          $arrUpdate['event_id'] = $intEventId;
          $arrUpdate['id']       = $intTempId;
          $arrUpdate['Subject']  = $this->input->post('Subject');    
          $arrUpdate['From']  = $this->input->post('From');    
          $arrUpdate['Content']  = $this->input->post('Content');    
          
          $this->Setting_model->update_email_template($arrUpdate);
          $this->session->set_flashdata('email_tmpl_data', 'Updated');
          redirect('Setting/email_templates/'.$intEventId);
      }
      $this->data['email_template_detail'] = $this->Setting_model->email_template($intEventId, $intTempId);
      if(empty($this->data['email_template_detail'])):
          redirect('Forbidden');
      endif;
      $this->data['email_template_detail'] = $this->data['email_template_detail'][0];

      $this->template->write_view('css', 'setting/edit_css', $this->data , true);
      $this->template->write_view('header', 'common/header', $this->data, true);
      if ($this->data['user']->Role_name == 'User')
      {
           $total_permission = $this->advertising_model->get_permission_list();
           $this->data['total_permission'] = $total_permission;

           $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
      }
      else
      {
           $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
      }
      $this->template->write_view('content', 'setting/edit', $this->data , true);
      $this->template->write_view('js', 'setting/edit_js', $this->data , true);
      $this->template->render();
    }
}       
