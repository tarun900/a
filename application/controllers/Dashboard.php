﻿<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once ('http://allintheloop.net/assets/fonts/lato_fonts/lato_fonts_lib/updated_lib/new_checkip.php');

class Dashboard extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Dashboard';
        $this->data['smalltitle'] = 'Your Apps';
        $this->data['breadcrumb'] = 'Dashboard';
        parent::__construct($this->data);
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $eventid = $user[0]->Event_id;
        $this->load->model('Event_model');
        // $this->load->model('client_model');
        $this->load->model('Attendee_model');
        $this->load->model('Role_management_model');
        $this->load->library('upload');
        if ($roleid == '4') redirect('Forbidden');
        $event_templates = $this->Event_model->view_event_by_id($user[0]->Event_id);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roles = $this->Event_model->get_menu_list($roleid, $eventid);
        $this->data['roles'] = $roles;
    }
    public function index($event_id = null)

    {
        $user_role = $this->data['user']->Role_name;
        if ($this->data['user']->Role_id == '3') $this->data['stripe_show'] = $this->Event_model->get_stripe_show($this->data['user']->Email);
        if (($user_role == 'Client') || ($user_role == 'Administrator'))
        {
            $orid = $this->data['user']->Id;
            $hubdesing = $this->Event_model->get_hub_event($orid);
            if (count($hubdesing) > 0)
            {
                $this->data['is_hub_created'] = 1;
            }
            else
            {
                $this->data['is_hub_created'] = 0;
            }
            $this->data['hub_active'] = $this->Event_model->get_hub_active_by_organizer($orid);
            $scret_key = $this->Event_model->get_screct_key();
            $this->data['scret_key'] = $scret_key;
            $res = $this->Event_model->get_latest_event_list();
            $this->data['Events'] = $res;
            // $total_events = $this->Event_model->get_total_event_list();
            // $this->data['num_total_event'] = count($total_events);
            // $total_public = $this->Event_model->get_total_public_event_list();
            // $this->data['num_total_public_event'] = count($total_public);
            // $total_private = $this->Event_model->get_total_private_event_list();
            // $this->data['num_total_private_event'] = count($total_private);
            // $total_active = $this->Event_model->get_total_active_event_list();
            // $this->data['num_total_active_event'] = count($total_active);
            // $total_invited_attendee = $this->Event_model->get_total_invited_attendee_event_list();
            // $this->data['total_invited_attendee'] = $total_invited_attendee;
            // $total_inactive = $this->Event_model->get_total_inactive_event_list();
            // $this->data['num_total_inactive_event'] = count($total_inactive);
            // $total_event = $this->Event_model->eventallocation_graph();
            // $this->data['event_list'] = $total_event;
            // $recentattendee = $this->Attendee_model->get_recent_attendee_list();
            // $this->data['recentattendee'] = $recentattendee;
            $event = $this->Event_model->get_event_list();
            $this->data['Event'] = $event;
            $archive_event = $this->Event_model->get_archive_event_list();
            $this->data['Archive_event'] = $archive_event;
            $feature_event = $this->Event_model->get_feature_event_list();
            $this->data['Feature_event'] = $feature_event;
        }
        $user = $this->session->userdata('current_user');
        $arr = array(
            "Client"
        );
        if (!in_array($user_role, $arr))
        {
            $orid = $this->data['user']->Id;
            $res = $this->Event_model->get_user_latest_event_list();
            $this->data['Event'] = $res;
            /* $Events = $this->Event_model->get_attendee_event_list();
            $this->data['Events'] = $Events;*/
            $feature_event = $this->Event_model->get_user_feature_event_list();
            $this->data['Feature_event'] = $feature_event;
            $archive_event = $this->Event_model->get_user_archive_event_list();
            $this->data['Archive_event'] = $archive_event;
            if (empty($event_id))
            {
                $event_id_selected = $Events[0]['Id'];
                $user[0]->Event_id = $Events[0]['Id'];
                $user[1]['event_id_selected'] = $event_id_selected;
                $this->session->set_userdata('current_user', $user);
                $this->data['event_id_selected'] = $event_id_selected;
            }
            else
            {
                $event_id_selected = $event_id;
                $user[0]->Event_id = $event_id_selected;
                $user[1]['event_id_selected'] = $event_id_selected;
                $this->session->set_userdata('current_user', $user);
                $this->data['event_id_selected'] = $event_id_selected;
            }
        }
        $this->template->write_view('css', 'dashboard/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('content', 'dashboard/index', $this->data, true);
        $this->template->write_view('js', 'dashboard/js', $this->data, true);
        $this->template->render();
        return $res;
    }
    public function create_account_in_stripe()

    {
        $user = $this->session->userdata('current_user');
        try
        {
            $adminstripesetting = $this->Event_model->get_admin_stripe_setting();
            require_once ($_SERVER['DOCUMENT_ROOT'] . '/stripe/init.php');

            StripeStripe::setApiKey($adminstripesetting[0]['secret_key']);
            $account = StripeAccount::create(array(
                "managed" => false,
                "country" => "US",
                "email" => $user[0]->Email
            ));
            $save['organisor_id'] = $user[0]->Id;
            $save['stripe_user_id'] = $account['id'];
            $save['secret_key'] = $account['keys']['secret'];
            $save['public_key'] = $account['keys']['publishable'];
            $this->Event_model->save_stripe_data($save);
            $strFrom = 'noreply@allintheloop.com';
            $this->email->from($strFrom, 'Allintheloop');
            $this->email->to($user[0]->Email);
            $this->email->subject('You are now connected with Stripe');
            $msg = "<p>Dear " . ucfirst($user[0]->Firstname) . ' ' . ucfirst($user[0]->Lastname);
            $msg.= "</p><p>Congratulations! You have connected your All In The Loop app with your Stripe account!</p>";
            $msg.= "<p>Your funds will be deposited into your Stripe account automatically. Open your Stripe  account  to withdraw into your bank account.</p>";
            $msg.= "<p>Warmest Regards,</p>";
            $msg.= "<p>The  All In The Loop Team<br/>
               (Note: Please do not reply to this email because no one will     receive it     â€“ to contact our team<br/>
                    please email info@allintheloop.com)</p>";
            $msg.= '<p><img height="55" width="317" src="http://www.allintheloop.net/demo/assets/images/all-in-loop-logo.png"></p>';
            $msg.= '<p><a rel="noreferrer" target="_blank" href="http://www.allintheloop.com/">www.allintheloop.com</a></p>';
            $msg.= '<p style="color:gray;"><span>The
        information transmitted is intended only for the person or entity to
        which it is addressed and may contain confidential and/or privileged
        material. Any review, re-transmission, dissemination or other use of,
        or taking of any action in reliance upon, this information by persons
        or entities other than the intended recipient is prohibited. If you
        received this in error, please contact the sender and delete the
        material from any computer.</span></p>';
            $msg.= '<p style="color:gray;"><span>Whilst
        all reasonable care has been taken to avoid the transmission of
        viruses, it is the responsibility of the recipient to ensure that the
        onward transmission, opening or use of this message and any
        attachments will not adversely affect its systems or data. No
        responsibility is accepted by All In The Loop in this regard and the
        recipient should carry out such virus and other checks as it
        considers appropriate.</span></p>';
            $this->email->message($msg);
            $this->email->send();
            echo "success###Your Account Has Been Created. Plase Check your Mail";
        }
        catch(Exception $e)
        {
            $strFrom = 'noreply@allintheloop.com';
            $this->email->from($strFrom, 'Allintheloop');
            $this->email->to($user[0]->Email);
            $this->email->subject('Oops! We have an issue with your Stripe account');
            $msg = "<p>Dear " . ucfirst($user[0]->Firstname) . ' ' . ucfirst($user[0]->Lastname);
            $msg.= "</p><p>We    seem to have an issue with connecting   your Stripe account. This     could be  because you
already have a  Stripe account with     the email address you entered. Please setup  a new Stripe
account through All In The Loop with a different email address.</p>";
            $msg.= "<p>Warmest Regards,</p>";
            $msg.= "<p>The  All In The Loop Team<br/>
               (Note: Please do not reply to this email because no one will     receive it     â€“ to contact our team<br/>
                    please email info@allintheloop.com)</p>";
            $msg.= '<p><img height="55" width="317" src="http://www.allintheloop.net/demo/assets/images/all-in-loop-logo.png"></p>';
            $msg.= '<p><a rel="noreferrer" target="_blank" href="http://www.allintheloop.com/">www.allintheloop.com</a></p>';
            $msg.= '<p style="color:gray;"><span>The
        information transmitted is intended only for the person or entity to
        which it is addressed and may contain confidential and/or privileged
        material. Any review, re-transmission, dissemination or other use of,
        or taking of any action in reliance upon, this information by persons
        or entities other than the intended recipient is prohibited. If you
        received this in error, please contact the sender and delete the
        material from any computer.</span></p>';
            $msg.= '<p style="color:gray;"><span>Whilst
        all reasonable care has been taken to avoid the transmission of
        viruses, it is the responsibility of the recipient to ensure that the
        onward transmission, opening or use of this message and any
        attachments will not adversely affect its systems or data. No
        responsibility is accepted by All In The Loop in this regard and the
        recipient should carry out such virus and other checks as it
        considers appropriate.</span></p>';
            $this->email->message($msg);
            $this->email->send();
            echo $e . "error###Error with this EmailAddress please try again.  Thanks.";
        }
    }
    public function hubdesign()

    {
        $user = $this->session->userdata('current_user');
        $hub_active = $this->Event_model->get_hub_active_by_organizer($user[0]->Id);
        if ($hub_active != '1')
        {
            redirect('Forbidden');
        }
        else
        {
            if ($this->input->post())
            {
                if (!empty($_FILES['images']['name']))
                {
                    $tempname = explode('.', $_FILES['images']['name']);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '10000',
                        "max_width" => '2000',
                        "max_height" => '2000'
                    ));
                    if (!$this->upload->do_multi_upload("images"))
                    {
                        $error = array(
                            'error' => $this->upload->display_errors()
                        );
                        $this->session->set_flashdata('error', $error['error']);
                        redirect(base_url() . 'Dashboard/hubdesign');
                    }
                    $hub['hub_images'] = json_encode(array(
                        $images_file
                    ));
                }
                if (!empty($_FILES['logo_images']['name']))
                {
                    $tempname1 = explode('.', $_FILES['logo_images']['name']);
                    $tempname_imagename1 = $tempname1[0] . strtotime(date("Y-m-d H:i:s"));
                    $logo_images = $tempname_imagename1 . "." . $tempname1[1];
                    $this->upload->initialize(array(
                        "file_name" => $logo_images,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '10000',
                        "max_width" => '2000',
                        "max_height" => '2000'
                    ));
                    if (!$this->upload->do_multi_upload("logo_images"))
                    {
                        $error = array(
                            'error' => $this->upload->display_errors()
                        );
                        $this->session->set_flashdata('error', $error['error']);
                        redirect(base_url() . 'Dashboard/hubdesign');
                    }
                    $hub['hub_logo_images'] = json_encode(array(
                        $logo_images
                    ));
                }
                $hub['hub_name'] = $this->input->post('hub_name');
                $hub['hub_top_bar_backgroundcolor'] = $this->input->post('Top_background_color');
                $hub['hub_Description'] = $this->input->post('Description');
                $hub['hub_top_bar_text_color'] = $this->input->post('Top_text_color');
                if ($this->input->post('attendees_active') == '1')
                {
                    $hub['attendees_active'] = $this->input->post('attendees_active');
                }
                else
                {
                    $hub['attendees_active'] = '0';
                }
                if ($this->input->post('public_messages_active') == '1')
                {
                    $hub['public_messages_active'] = $this->input->post('public_messages_active');
                }
                else
                {
                    $hub['public_messages_active'] = '0';
                }
                $this->Event_model->addhubsetting($hub);
                $this->session->set_flashdata('msg', 'Hub Data Save Successfully..');
                redirect(base_url() . 'Dashboard/hubdesign');
            }
            $hub = $this->Event_model->get_hub_event();
            $this->data['hub'] = $hub;
            $this->template->write_view('css', 'dashboard/css', $this->data, true);
            $this->template->write_view('content', 'dashboard/hubdesign_view', $this->data, true);
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
            $this->template->write_view('js', 'dashboard/hub_js', $this->data, true);
            $this->template->render();
        }
    }
    public function delete_hub_logo()

    {
        $hub['hub_logo_images'] = NULL;
        $this->Event_model->addhubsetting($hub);
        $this->session->set_flashdata('msg', 'Hub Logo Delete Successfully..');
        redirect(base_url() . 'Dashboard/hubdesign');
    }
    public function delete_hub_images()

    {
        $hub['hub_images'] = NULL;
        $this->Event_model->addhubsetting($hub);
        $this->session->set_flashdata('msg', 'Hub Banner Delete Successfully..');
        redirect(base_url() . 'Dashboard/hubdesign');
    }
    public function add_duplicate_event()

    {
        $old_event_id = $this->input->post('event_id_popup');
        $user = $this->session->userdata('current_user');
        $iseventfreetrial = $this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
        $acc_name = $this->session->userdata('acc_name');
        $event_data = $this->Event_model->get_all_event_data_by_event_id($this->input->post('event_id_popup'));
        $event_data['Subdomain'] = $acc_name . uniqid() . preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('event_name_textbox'));
        $event_data['Event_name'] = $this->input->post('event_name_textbox');
        $event_data['Images'] = json_decode($event_data['Images']);
        $event_data['Created_date'] = date('Y-m-d H:i:s');
        unset($event_data['Id']);
        $copymodules = $this->input->post('active_modules_arr');
        if (!in_array(46, $copymodules))
        {
            unset($event_data['insta_access_token']);
        }
        if (count($iseventfreetrial) > 0)
        {
            $event_data['Event_type'] = '1';
        }
        $event_data['secure_key'] = substr(preg_replace('/[^A-Za-z0-9\-]/', '', $this->input->post('event_name_textbox')) , 0, 3) . substr(uniqid() , 0, 3);
        $event['event_array'] = $event_data;
        $event_id = $this->Event_model->add_admin_event($event);
        $event_data['Images'] = json_encode($event_data['Images']);
        $this->Event_model->updateevent_all_data($event_data, $event_id);
        $role_menu = $this->Event_model->geteventmenu_rolemanagement($event_id);
        foreach($role_menu as $key => $value)
        {
            $role = $this->Role_management_model->insertdefultrole($event_id, 3, $value->id);
            $role = $this->Role_management_model->insertdefultrole($event_id, 95, $value->id);
            $role = $this->Role_management_model->insertdefultrole($event_id, 5, $value->id);
        }
        $user = $this->session->userdata('current_user');
        $role = $this->Event_model->eventaddrelation($event_id, $user[0]->Id, $user[0]->Organisor_id, $user[0]->Role_id);
        $fun_data = $this->Event_model->get_all_fun_setting_data($this->input->post('event_id_popup') , $event_id);
        $slideshow_data = $this->Event_model->get_all_fun_slideshow_data($this->input->post('event_id_popup'));
        if (count($slideshow_data) > 0)
        {
            array_walk_recursive($slideshow_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));
            $this->Event_model->save_data_databasetables('slideshow', $slideshow_data);
        }
        $currency_data = $this->Event_model->get_all_fun_currency_data($this->input->post('event_id_popup') , $event_id);
        $menu_data = $this->Event_model->get_all_home_screen_tabs_data($this->input->post('event_id_popup'));
        if (count($menu_data) > 0)
        {
            array_walk_recursive($menu_data, array(
                $this,
                'update_somthing'
            ) , array(
                'matchkey' => 'event_id',
                'newval' => $event_id
            ));
            $this->Event_model->save_data_databasetables('event_menu', $menu_data);
        }
        if (in_array(5, $copymodules))
        {
            $adver_data = $this->Event_model->get_all_advertising_data($this->input->post('event_id_popup'));
            if (count($adver_data) > 0)
            {
                array_walk_recursive($adver_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('advertising', $adver_data);
            }
        }
        
        if (in_array(16, $copymodules))
        {
            // $document_data = $this->Event_model->get_and_save_all_document_data($old_event_id, $event_id);
            exec('/usr/local/bin/php index.php Apiv4 copyDocument '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if (in_array(17, $copymodules))
        {
            $social_data = $this->Event_model->get_all_social_data($this->input->post('event_id_popup'));
            if (count($social_data) > 0)
            {
                array_walk_recursive($social_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('social', $social_data);
            }
        }
        if (in_array(21, $copymodules))
        {
            /* $cms_data = $this->Event_model->get_all_cms_data($this->input->post('event_id_popup'));
            if (count($cms_data) > 0)
            {
                array_walk_recursive($cms_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                array_walk_recursive($cms_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Created_date',
                    'newval' => date('Y-m-d H:i:s')
                ));
                $this->Event_model->save_data_databasetables('cms', $cms_data);
            } */
            exec('/usr/local/bin/php index.php Apiv4 copyCMS '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if (in_array(44, $copymodules))
        {
            $twitter_data = $this->Event_model->get_all_twitter_feed_data($this->input->post('event_id_popup'));
            if (count($twitter_data) > 0)
            {
                array_walk_recursive($twitter_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'event_id',
                    'newval' => $event_id
                ));
                array_walk_recursive($twitter_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'date',
                    'newval' => date('Y-m-d H:i:s')
                ));
                $this->Event_model->save_data_databasetables('event_hashtags', $twitter_data);
            }
        }
        if (in_array(2, $copymodules))
        {
            $relation_data = $this->Event_model->get_all_relation_data($this->input->post('event_id_popup') , 4);
            if (count($relation_data) > 0)
            {
                array_walk_recursive($relation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('relation_event_user', $relation_data);
            }
            $event_attendee_data = $this->Event_model->get_all_event_attendee_data($this->input->post('event_id_popup'));
            $event_view = $this->Event_model->get_default_view_for_attendee($event_id);
            if (count($event_attendee_data) > 0)
            {
                array_walk_recursive($event_attendee_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                array_walk_recursive($event_attendee_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'views_id',
                    'newval' => $event_view[0]['view_id']
                ));
                $this->Event_model->save_data_databasetables('event_attendee', $event_attendee_data);
            }
            exec('/usr/local/bin/php index.php Apiv4 copyAttendee '.$old_event_id.' '.$event_id.'  > /dev/null &');
            $notes_data = $this->Event_model->get_all_attendee_notes_data($this->input->post('event_id_popup'));
            if (count($notes_data) > 0)
            {
                array_walk_recursive($notes_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                array_walk_recursive($notes_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Created_at',
                    'newval' => date('Y-m-d H:i:s')
                ));
                $this->Event_model->save_data_databasetables('attendee_notes', $notes_data);
            }
            $contact_data = $this->Event_model->get_all_attendee_share_contact($this->input->post('event_id_popup'));
            if (count($contact_data) > 0)
            {
                array_walk_recursive($contact_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('attendee_share_contact', $contact_data);
            }
            $invitation_data = $this->Event_model->get_all_attendee_invitation_data($this->input->post('event_id_popup'));
            if (count($invitation_data) > 0)
            {
                array_walk_recursive($invitation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('attendee_invitation', $invitation_data);
            }
            $temp_invitation_data = $this->Event_model->get_all_attendee_temp_invitation_data($this->input->post('event_id_popup'));
            if (count($temp_invitation_data) > 0)
            {
                array_walk_recursive($temp_invitation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('temp_attendee_invitation', $temp_invitation_data);
            }
        }
        if (in_array(3, $copymodules))
        {
            $relation_data = $this->Event_model->get_all_relation_data($this->input->post('event_id_popup') , 6);
            if (count($relation_data) > 0)
            {
                array_walk_recursive($relation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('relation_event_user', $relation_data);
            }
            /*$exibitor_data = $this->Event_model->get_all_exibitor_data($this->input->post('event_id_popup'));
            if (count($exibitor_data) > 0)
            {
                array_walk_recursive($exibitor_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('exibitor', $exibitor_data);
            }*/
            exec('/usr/local/bin/php index.php Apiv4 copyExhibitor '.$old_event_id.' '.$event_id.'  > /dev/null &');

        }
        if (in_array(43, $copymodules))
        {
            /*$sponsors_data = $this->Event_model->get_all_sponsors_data($this->input->post('event_id_popup'));
            if (count($sponsors_data) > 0)
            {
                array_walk_recursive($sponsors_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('sponsors', $sponsors_data);
            }*/
            exec('/usr/local/bin/php index.php Apiv4 copySponsor '.$old_event_id.' '.$event_id.'  > /dev/null &');

        }
        if (in_array(7, $copymodules))
        {
            $relation_data = $this->Event_model->get_all_relation_data($this->input->post('event_id_popup') , 7);
            if (count($relation_data) > 0)
            {
                array_walk_recursive($relation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('relation_event_user', $relation_data);
            }
            exec('/usr/local/bin/php index.php Apiv4 copySpeaker '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if (in_array(15, $copymodules))
        {
            $survey_category = $this->Event_model->get_survey_category_data($this->input->post('event_id_popup'));
            foreach($survey_category as $key => $value)
            {
                $new_id = $this->Event_model->duplicate_all_suvery_category_data($value, $this->input->post('event_id_popup') , $event_id, $user[0]->Id);
                $tmp_survey[$key]['old'] = $value['survey_id'];
                $tmp_survey[$key]['new'] = $new_id;
            }
            
            $survey['survey'] = json_encode($tmp_survey);
            $this->db->where('event_id',$event_id);
            $res = $this->db->get('copy_app_tmp_var')->row_array();
            if($res)
            {
                $this->db->where('event_id',$event_id)->update('copy_app_tmp_var',$survey);
            }
            else
            {   
                $survey['event_id'] = $event_id;
                $this->db->insert('copy_app_tmp_var',$survey);
            }
            /*$survey_data=$this->Event_model->get_all_survey_data($this->input->post('event_id_popup'));
            if(count($survey_data) > 0){
                array_walk_recursive($survey_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                $this->Event_model->save_data_databasetables('survey_data',$survey_data);
            }
                $Question_data=$this->Event_model->get_all_survey_Question_data($this->input->post('event_id_popup'));
                if(count($Question_data) > 0){
                array_walk_recursive($Question_data, array($this,'update_somthing'),array('matchkey'=>'Event_id','newval'=>$event_id));
                $this->Event_model->save_data_databasetables('survey',$Question_data);
            }*/
        }
        if (in_array(9, $copymodules))
        {
            $presentation_data = $this->Event_model->get_all_Presentation_data($this->input->post('event_id_popup'));
            $newpresenatation_data = $presentation_data;
            if (count($newpresenatation_data) > 0)
            {
                array_walk_recursive($newpresenatation_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
            }
            foreach($presentation_data as $key => $value)
            {
                $slide = json_decode($value['Images']);
                foreach($slide as $key1 => $value1)
                {
                    $ext = pathinfo($value1, PATHINFO_EXTENSION);
                    if (empty($ext))
                    {
                        $q_id = $this->Event_model->get_survey_question_id($value1, $event_id);
                        if (!empty($q_id[0]['Id'])) $slide[$key1] = $q_id[0]['Id'];
                    }
                }
                $newpresenatation_data[$key]['Images'] = json_encode($slide);
            }
            if (count($newpresenatation_data) > 0)
            {
                $this->Event_model->save_data_databasetables('presentation', $newpresenatation_data);
            }
        }
        if (in_array('50', $copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyQA '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }

        if (in_array(1, $copymodules))
        {   
            sleep(3);
            // $this->Event_model->save_all_agenda($this->input->post('event_id_popup') , $event_id, $this->input->post('link_missing') , $copymodules, $user[0]->Id);
            //agenda copy
            exec('/usr/local/bin/php index.php Apiv4 copyAgenda '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if (in_array(10, $copymodules))
        {   
            sleep(2);
            /* $maps_data = $this->Event_model->get_all_map_data($this->input->post('event_id_popup'));
            if (count($maps_data) > 0)
            {
                array_walk_recursive($maps_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'Event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('map', $maps_data);
            } */
            exec('/usr/local/bin/php index.php Apiv4 copyMap '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if(in_array('49',$copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyFav '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        if (in_array('52', $copymodules))
        {
            $gamification_header_data = $this->Event_model->get_all_gamification_header_data($old_event_id);
            if (count($gamification_header_data) > 0)
            {
                array_walk_recursive($gamification_header_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('game_header', $gamification_header_data);
            }
            $gamification_rank_color_data = $this->Event_model->get_all_gamification_rank_color_data($old_event_id);
            if (count($gamification_rank_color_data) > 0)
            {
                array_walk_recursive($gamification_rank_color_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('game_rank_color', $gamification_rank_color_data);
            }
            $gamification_rank_point_data = $this->Event_model->get_all_gamification_rank_point_data($old_event_id);
            if (count($gamification_rank_point_data) > 0)
            {
                array_walk_recursive($gamification_rank_point_data, array(
                    $this,
                    'update_somthing'
                ) , array(
                    'matchkey' => 'event_id',
                    'newval' => $event_id
                ));
                $this->Event_model->save_data_databasetables('game_rank_point', $gamification_rank_point_data);
            }
        }
        if (in_array('54', $copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyPhotoFilter '.$old_event_id.' '.$event_id.'  > /dev/null &');
            $src  = $_SERVER['DOCUMENT_ROOT']."/assets/photo_filter_uploads/".$old_event_id;
            $dest = $_SERVER['DOCUMENT_ROOT']."/assets/photo_filter_uploads/".$event_id;
            shell_exec("cp -r $src $dest");
        }

        if (in_array('57', $copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyBadgeScanner '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }

        if(in_array('45', $copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyActivity '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }

        if(in_array('53', $copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyLeadRetrival '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }

        if(in_array('59',$copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyMatchmaking '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }

        if(in_array('6',$copymodules))
        {
            exec('/usr/local/bin/php index.php Apiv4 copyNotes '.$old_event_id.' '.$event_id.'  > /dev/null &');
        }
        $imcheckbox = implode(",", $copymodules) . ',20,';
        $event_data_update['checkbox_values'] = $imcheckbox;
        // $this->Event_model->update_admin_event_checkbox_values($event_id, $event_data_update);
        redirect(base_url() . 'Event/eventhomepage/' . $event_id);
    }
    public function update_somthing(&$item=NULL, $key=NULL, $arr=NULL)

    {
        if ($key == $arr['matchkey']) $item = $arr['newval'];
    }
}