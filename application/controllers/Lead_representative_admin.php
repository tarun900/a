<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Lead_representative_admin extends FrontendController

{
	function __construct()
	{
		$this->data['pagetitle'] = 'Lead Representatives';
		$this->data['smalltitle'] = 'Upload or individually add your Lead representative list on this page.';
		$this->data['page_edit_title'] = 'edit';
		ini_set('memory_limit', '-1');
		$this->data['breadcrumb'] = 'Lead Representative';
		parent::__construct($this->data);
		$this->load->model('Formbuilder_model');
		$this->load->model('Agenda_model');
		$this->load->model('Event_model');
		$this->load->model('Setting_model');
		$this->load->model('Event_template_model');
		$this->load->model('lead_representative_model');
		$this->load->model('Lead_retrieval_model');
		$this->load->library('upload');
		$this->load->library('email');
		$user = $this->session->userdata('current_user');
		$eventid = $this->uri->segment(3);
		$this->load->database();
		$user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
		$user_events = array_filter(array_column_1($user_events, 'Event_id'));
		if (!in_array($eventid, $user_events))
		{
			redirect('Forbidden');
		}
		$eventmodule = $this->Event_model->geteventmodulues($eventid);
		$module = json_decode($eventmodule[0]['module_list']);
		$event_templates = $this->Event_model->view_event_by_id($eventid);
		$this->data['Subdomain'] = $event_templates[0]['Subdomain'];
		$event = $this->Event_model->get_module_event($eventid);
		$menu_list = explode(',', $event[0]['checkbox_values']);
		$roledata = $this->Event_model->getUserRole($eventid);
		if (!empty($roledata))
		{
			$roleid = $roledata[0]->Role_id;
			$rolename = $roledata[0]->Name;
			$cnt = 0;
			$req_mod = ucfirst($this->router->fetch_class());
			if ($this->data['pagetitle'] == "Your Lead")
			{
				$title = "lead_retrieval";
			}
			$cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
		}
		else
		{
			$cnt = 0;
		}
		if (!empty($user[1]['event_id_selected']))
		{
			$this->data['event_id_selected'] = $user[1]['event_id_selected'];
		}
	}
	public function index($id=NULL)

	{
		$event = $this->Event_model->get_admin_event($id);
		$this->data['event'] = $event[0];
		$user = $this->session->userdata('current_user');
		$roleid = $user[0]->Role_id;
		$event_id = $id;
		$user_role = $this->Event_template_model->get_menu_list($roleid, $id);
		$this->data['users_role'] = $user_role;
		$rolename = $user[0]->Role_name;
		$menudata = $this->Event_model->geteventmenu($id, 53);
		$menu_toal_data = $this->Event_model->get_total_menu($id);
		$this->data['menu_toal_data'] = $menu_toal_data;
		$this->data['event_id'] = $event_id;
		$this->data['menu_id'] = $menudata[0]->id;
		$this->data['title'] = $menudata[0]->menuname;
		$this->data['img'] = $menudata[0]->img;
		$this->data['img_view'] = $menudata[0]->img_view;
		$this->data['is_feture_product'] = $menudata[0]->is_feture_product;
		$this->data['lead_role'] = $this->lead_representative_model->get_lead_role($id);
		$this->data['lead_users'] = $this->lead_representative_model->getLeadUsers($id, $user[0]->Id);
		$this->data['my_lead'] = $this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($id, $user[0]->Id);
		$this->template->write_view('css', 'lead_representative_admin/css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'lead_representative_admin/index', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'lead_representative_admin/js', true);
		$this->template->render();
	}
	public function save_lead_setting($id=NULL)

	{
		$data['event_array']['Id'] = $id;
		if ($this->input->post('allow_export_lead_data') == '1')
		{
			$data['event_array']['allow_export_lead_data'] = '1';
		}
		else
		{
			$data['event_array']['allow_export_lead_data'] = '0';
		}
		if ($this->input->post('allow_custom_survey_lead') == '1')
		{
			$data['event_array']['allow_custom_survey_lead'] = '1';
		}
		else
		{
			$data['event_array']['allow_custom_survey_lead'] = '0';
		}
		$this->Event_model->update_admin_event($data);
		$this->session->set_flashdata('survey_data', 'Lead Retrieval Setting Saved Successfully.');
		redirect('Lead_representative_admin/index/' . $id);
	}
	public function store($event_id=NULL)

	{
		show_errors();
		$user = $this->session->userdata('current_user');
		if ($user[0]->Email == $this->input->post('Email'))
		{
			$this->session->set_flashdata('survey_data_error', 'Please use an alternative email to your admin email.');
		}
		else
		{
			$count = count($this->lead_representative_model->getLeadUsers($event_id, $user[0]->Id));
			if ($count < (int)$user[0]->no_of_reps)
			{
				$event = $this->Event_model->get_admin_event($event_id);
				$data['Firstname'] = $this->input->post('Firstname');
				$data['Lastname'] = $this->input->post('Lastname');
				$data['Email'] = $this->input->post('Email');
				$data['Title'] = $this->input->post('Title');
				$data['Company_name'] = $this->input->post('Company_name');
				$data['Password'] = (md5($this->input->post('Password'))) ? : md5('123456');
				$result = $this->lead_representative_model->add($data, $event_id, $event[0]['Organisor_id'], $user[0]->Id, $user[0]->Role_id);
				if ($result) $this->session->set_flashdata('survey_data', 'Representative created Successfully.');
				else $this->session->set_flashdata('survey_data_error', 'Organisor can not be add as representative.');
			}
			else
			{
				$this->session->set_flashdata('setting_error_data', 'â€œYou cannot add any more representatives to your account.');
			}
		}
		redirect('Event/eventhomepage/' . $event_id);
	}
	public function update($event_id=NULL)

	{
		$update_data['ask_survey'] = ($this->input->post('ask_survey') == "on") ? '1' : '0';
		$update_data['add_survey'] = ($this->input->post('add_survey') == "on") ? '1' : '0';
		$update_data['Name'] = $this->input->post('role');
		$where['Id'] = $this->input->post('id');
		$this->lead_representative_model->updateRole($update_data, $where);
		$this->session->set_flashdata('setting_data', 'Role updated Successfully.');
		redirect('Event/eventhomepage/' . $event_id);
	}
	public function delete($event_id=NULL, $item_id=NULL)

	{
		$where['Id'] = $item_id;
		$this->lead_representative_model->deleteRole($where);
		$this->session->set_flashdata('setting_data', 'Role deleted Successfully.');
		redirect('Event/eventhomepage/' . $event_id);
	}
	public function delete_rep($event_id=NULL, $user_id=NULL)

	{
		$user = $this->session->userdata('current_user');
		$this->lead_representative_model->delete_rep($event_id, $user_id, $user[0]->Id, $user[0]->Rid);
		$this->session->set_flashdata('survey_data', 'Representative deleted successfully');
		redirect('Event/eventhomepage/' . $event_id);
	}
	public function get_user_data($event_id=NULL, $user_id=NULL)

	{
		echo json_encode($this->db->select('*')->from('user')->where('Id', $user_id)->get()->row_array());
		exit;
	}
	public function edit_user($event_id=NULL)

	{
		$where['Id'] = $this->input->post('user_id');
		$data['Firstname'] = $this->input->post('Firstname');
		$data['Lastname'] = $this->input->post('Lastname');
		$data['Title'] = $this->input->post('Title');
		$data['Company_name'] = $this->input->post('Company_name');
		if ($this->input->post('Password')) $data['Password'] = md5($this->input->post('Password'));
		$this->db->where($where);
		$this->db->update('user', $data);
		$this->session->set_flashdata('survey_data', 'Representative updated successfully');
		redirect('Event/eventhomepage/' . $event_id);
	}
	public function change_rep_status($event_id=NULL, $rep_id=NULL, $status=NULL)

	{
		$user = $this->session->userdata('current_user');
		$where['event_id'] = $event_id;
		$where['rep_id'] = $rep_id;
		$where['user_id'] = $user[0]->Id;
		$update['status'] = $status;
		$this->db->where($where)->update('user_representatives', $update);
		$this->session->set_flashdata('survey_data', ($status == '0') ? 'Representative disabled successfully' : 'Representative enabled successfully');
		redirect('Event/eventhomepage/' . $event_id);
	}
}