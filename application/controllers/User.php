<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class User extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Users';
          $this->data['smalltitle'] = 'Users Details';
          $this->data['page_edit_title'] = 'users';
          $this->data['breadcrumb'] = 'Users';
          parent::__construct($this->data);
          $this->load->model('Event_model');
          $this->load->model('User_model');
          $this->load->model('Setting_model');
          $this->load->model('Profile_model');
          $this->load->model('Speaker_model');

          $user = $this->session->userdata('current_user');
          $eventid=$this->uri->segment(3);

          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
               redirect('Forbidden');
          }
        
          $this->load->library('session');
     }

     public function index($id = NULL)
     {
          $this->data['event_id11'] = $id;
          $this->data['event_id'] = $id;
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];
          $user = $this->session->userdata('current_user');
        
          $users = $this->User_model->get_user_list_byevent($id);

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $event_id = $user[0]->Event_id;
          $org = $user[0]->Organisor_id;
          $this->data['org'] = $org;

          if ($this->input->post())
          {
               $att_user_id=$this->input->post('attendee');
               
               //$this->User_model->add_user_as_speaker_inv($inv_user_id);
               
              /* foreach ($inv_user_id as $key => $value) 
               {
                    $speakers_flag=urlencode(base64_encode("1"));
                    $user_details=$this->User_model->get_user_details($value);
                    $event_link = base_url() . 'Events/' . $event[0]['Subdomain'] . '';
                    $tes = urlencode(base64_encode($user_details[0]['Email']));
                    $msg1 = 'Hi ' . $user_details[0]['Firstname'] . " " . $user_details[0]['Lastname']. ', <br/> <br/> ';
                    $msg1 .= $msg . ' <br/><br/> ' . 'Invitation Link:  ' . $event_link . '/' . $tes . '/'.$speakers_flag.' ';
                    $sub = ucfirst($event[0]['Subdomain'] . ' ' . 'Event Invitation');
                    $sent_from = $this->input->post('sent_from');
                    $this->load->library('email');
                    /*$config['protocol'] = "smtp";
                    $config['smtp_host'] = "ssl://smtp.gmail.com";
                    $config['smtp_port'] = "465";
                    $config['smtp_user'] = "nakranichetan32@gmail.com"; 
                    $config['smtp_pass'] = "chetan@9099118056";
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "text/html";
                    $config['newline'] = "\r\n";
                    $this->email->initialize($config);
                    $this->email->from($sent_from, 'Event Management');
                    $this->email->to($user_details[0]['Email']);
                    $this->email->subject($sub);
                    $this->email->message($msg1);
                    $this->email->send();
               }*/
               $this->User_model->update_user_as_attendee($att_user_id);
               $this->session->set_flashdata('user_data', 'Move to attendee list');
               redirect("Role_management/index/" . $id);
             
          }

          if ($event_id > 0)
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          else
          {
               $user_list = $this->User_model->get_all_users($event_id);
               $this->data['user_list'] = $user_list;
          }
          $this->data['users'] = $users;
         // $speakers = $this->Speaker_model->get_speaker_list($id);
          $inv_status=array();
         /* foreach ($speakers as $key => $value) 
          {
             
             $inv_status[]=$this->Speaker_model->get_in_status($value['User_id']);
          }*/
          $attendee_user_list=$this->User_model->get_attendee_user_list($id,$user[0]->Organisor_id);
          $this->data['attendee_user_list']=$attendee_user_list;
        
          $this->template->write_view('css', 'user/css', $this->data, true);
          $this->template->write_view('content', 'user/index', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'user/js', $this->data, true);
          $this->template->render();
     }

     public function exist($id = NULL)
     {
          $Event_id = $id;
          $user = $this->session->userdata('current_user');
          $logged_in_user_id = $user[0]->Id;

          $user_list = $this->User_model->get_all_users($id);
          $this->data['user_list'] = $user_list;
          $this->data['Event_id'] = $Event_id;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          if ($this->input->post())
          {
               $data['user_array']['Event_id'] = $this->input->post('Event_id');
               $data['user_array']['User_id'] = $this->input->post('User_id');
               $data['user_array']['Role_id'] = $this->input->post('rolename');
               $user_id = $this->User_model->add_exist_user($data);
               $this->session->set_flashdata('user_data', 'Added');
               redirect("Role_management/index/" . $Event_id);
              
          }

          $this->template->write_view('css', 'user/add_css', $this->data, true);
          $this->template->write_view('content', 'user/exist', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'user/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = '0')
     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          $this->data['users'] = $this->User_model->get_user_list($id);
          $this->template->write_view('css', 'user/add_css', $this->data, true);
          $this->template->write_view('content', 'user/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'user/add_js', $this->data, true);
          $this->template->render();
     }

     public function add($eventid = NULL)
     {
          $event = $this->Event_model->get_admin_event($eventid);
          $this->data['event'] = $event[0];
          if ($this->input->post())
          {
               foreach ($this->input->post() as $k => $v)
               {
                    if ($k == "email")
                    {
                         $k = "Email";
                         $array_add[$k] = $v;
                    }
                    else if ($k == "password")
                    {
                         $k = "Password";
                         $v = md5($v);
                         $array_add[$k] = $v;
                    }
                    else if ($k == "password_again")
                    {
                         
                    }
                    else
                    {
                         $array_add[$k] = $v;
                    }
               }

               /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////
               /* $slug = "WELCOME_MSG";                
                 $em_template = $this->Setting_model->email_template($slug);
                 $em_template = $em_template[0];
                 $new_pass = $this->input->post('password');
                 $name = ($this->input->post('Firstname')==NULL) ? $this->input->post('Company_name') : $this->input->post('Firstname');
                 $msg = $em_template['Content'];
                 $patterns = array();
                 $patterns[0] = '/{{name}}/';
                 $patterns[1] = '/{{password}}/';
                 $patterns[2] = '/{{email}}/';
                 $patterns[3] = '/{{link}}/';
                 $replacements = array();
                 $replacements[0] = $name;
                 $replacements[1] = $new_pass;
                 $replacements[2] = $this->input->post('email');
                 $replacements[3] = base_url();
                 $msg = preg_replace($patterns, $replacements, $msg);
                 $msg .= "Email: ".$this->input->post('email')."<br/>";
                 $msg .= "Login Link: ".base_url()."<br/>";
                 $msg .= "Password: ".$new_pass;

                 $this->email->from('info@evntapp.com', 'Event Management');
                 $this->email->to($this->input->post('email'));
                 $this->email->subject('User Account');
                 $this->email->set_mailtype("html");
                 $this->email->message($msg);
                 $this->email->send();
                */
               /////////////////////////////////////////////////////email//////////////////////////////////////////////////////////

               $user = $this->User_model->add_user($array_add, $eventid);
               $this->session->set_flashdata('user_data', 'Added');
               redirect("Role_management/index/" . $eventid);
              
          }
          $role_id = $user[0]->Role_id;
          $this->data['roles'] = $this->User_model->get_role_list_version2($eventid);
          $this->template->write_view('css', 'user/add_css', $this->data, true);
          $this->template->write_view('content', 'user/add', $this->data, true);
          $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          $this->template->write_view('js', 'user/add_js', $this->data, true);
          $this->template->render();
     }

     public function delete($Event_id = NULL, $id = NULL)
     {
          $user = $this->User_model->delete_user($id,$Event_id);
          $this->session->set_flashdata('user_data', 'Deleted');
          redirect("Role_management/index/" . $Event_id);
         
     }

}
