<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Event_template extends CI_Controller

{
    function __construct()
    {
        $this->data['pagetitle'] = 'View Your Apps';
        $this->data['smalltitle'] = 'View Your Apps Details';
        $this->data['breadcrumb'] = 'View Your Apps';
        parent::__construct($this->data);
        $this->template->set_template('front_template');
        $this->load->model('Event_template_model');
        $this->load->model('Event_model');
        $this->load->model('Setting_model');
        $this->load->model('Speaker_model');
        $this->load->model('Profile_model');
    }
    public function index()

    {
        /*$event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $this->template->write_view('css', 'events/css', $this->data , true);
        $this->template->write_view('content', 'events/index', $this->data , true);
        $this->template->write_view('js', 'events/js', $this->data , true);
        $this->template->render();*/
        $event_templates = $this->Event_template_model->get_event_template_list();
        $this->data['event_templates'] = $event_templates;
        // echo'<pre>'; print_r($event_templates); exit();
        $this->template->write_view('css', 'event_template/css', $this->data, true);
        $this->template->write_view('content', 'event_template/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'events/add_js', $this->data, true);
        $this->template->render();
    }
    public function view_template($Subdomain = NULL)

    {
        $event_templates = $this->Event_template_model->get_event_template_by_id_list($Subdomain);
        $this->data['event_templates'] = $event_templates;
        $menu = $this->Event_template_model->geteventmenu($event_templates[0]['Id'], null, 1);
        $this->data['menu'] = $menu;
        // echo'<pre>'; print_r($event_templates); exit;
        $speakers = $this->Event_template_model->get_speaker_list($Subdomain);
        $this->data['speakers'] = $speakers;
        $agendas = $this->Event_template_model->get_agenda_list($Subdomain);
        $this->data['agendas'] = $agendas;
        $attendees = $this->Event_template_model->get_attendee_list($Subdomain);
        $this->data['attendees'] = $attendees;
        $this->template->write_view('css', 'events/css', $this->data, true);
        $this->template->write_view('content', 'events/index', $this->data, true);
        // $this->template->write_view('content', 'template/front_template', $this->data , true);
        $this->template->write_view('js', 'events/js', $this->data, true);
        $this->template->render();
    }
}