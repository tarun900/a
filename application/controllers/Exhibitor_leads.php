<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Exhibitor_leads extends FrontendController

{
	function __construct()
	{
		$this->data['pagetitle'] = 'My Lead';
		$this->data['smalltitle'] = 'Lising My Lead';
		$this->data['breadcrumb'] = 'Lead';
		$this->data['page_edit_title'] = 'edit';
		parent::__construct($this->data);
		ini_set('auto_detect_line_endings', true);
		$this->load->model('Agenda_model');
		$this->load->model('Event_model');
		$this->load->model('Event_template_model');
		$this->load->model('Exibitor_model');
		$this->load->model('Lead_retrieval_model');
		$user = $this->session->userdata('current_user');
		$eventid = $this->uri->segment(3);
		$user = $this->session->userdata('current_user');
		$eventid = $this->uri->segment(3);
		$this->load->database();
		$user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
		$user_events = array_filter(array_column_1($user_events, 'Event_id'));
		if (!in_array($eventid, $user_events))
		{
			redirect('Forbidden');
		}
		$event = $this->Event_model->get_admin_event($eventid);
		$this->data['Subdomain'] = $event[0]['Subdomain'];
		$premission = $this->Exibitor_model->get_exibitor_user_premission($eventid, $user[0]->Id);
		$this->data['exibitor_premission'] = $premission;
		$event = $this->Event_model->get_admin_event($eventid);
		$this->data['event'] = $event[0];
		$user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $eventid);
		$this->data['users_role'] = $user_role;
		$this->data['event_id'] = $eventid;
	}
	public function index($eid=NULL)

	{
		$user_id = $this->data['user']->Id;
		$my_lead = $this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($eid, $user_id);
		$this->data['my_lead'] = $my_lead;
		$custom_column = $this->Lead_retrieval_model->get_all_custom_column_data($eid);
		$this->data['custom_column'] = $custom_column;
		$this->template->write_view('css', 'admin/css', $this->data, true);
		$this->template->write_view('header', 'common/header', $this->data, true);
		$this->template->write_view('content', 'exibitor_lead/index', $this->data, true);
		if ($this->data['user']->Role_name == 'User')
		{
			$this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
		}
		else
		{
			$this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
		}
		$this->template->write_view('js', 'exibitor_lead/js', $this->data, true);
		$this->template->render();
	}
	public function export_my_lead($event_id=NULL)

	{
		$user = $this->session->userdata('current_user');
		$custom_column = $this->Lead_retrieval_model->get_all_custom_column_data($event_id);
		$this->data['custom_column'] = $custom_column;
		$questions = $this->Lead_retrieval_model->get_all_exibitor_user_questions_for_export($event_id, $user[0]->Id);
		$this->data['questions'] = $questions;
		$filename = "lead_user_detail.csv";
		$fp = fopen('php://output', 'w');
		$header[] = "FirstName";
		$header[] = "LastName";
		$header[] = "Email";
		$header[] = "Title";
		$header[] = "Company Name";
		foreach($custom_column as $key => $value)
		{
			$header[] = ucfirst($value['column_name']);
		}
		foreach($questions as $key => $value)
		{
			$header[] = $value['Question'];
		}
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename=' . $filename);
		fputcsv($fp, $header);
		$my_lead = $this->Lead_retrieval_model->get_all_my_lead_by_exibitor_user($event_id, $user[0]->Id);
		foreach($my_lead as $key => $value)
		{
			$data['FirstName'] = $value['Firstname'];
			$data['LastName'] = $value['Lastname'];
			$data['Email'] = $value['Email'];
			$data['Title'] = $value['Title'];
			$data['Company Name'] = $value['Company_name'];
			$custom_column_data = json_decode($value['custom_column_data'], true);
			foreach($custom_column as $ckey => $cvalue)
			{
				$data[ucfirst($cvalue['column_name']) ] = $custom_column_data[$cvalue['column_name']];
			}
			foreach($questions as $qkey => $qvalue)
			{
				$ans = $this->Lead_retrieval_model->get_user_question_answer($event_id, $value['Id'], $qvalue['q_id']);
				$data[] = !empty($ans[0]['Answer']) ? $ans[0]['Answer'] : $ans[0]['answer_comment'];
			}
			fputcsv($fp, $data);
			unset($data);
		}
	}
}