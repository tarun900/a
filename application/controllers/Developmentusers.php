<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Developmentusers extends CI_Controller {
// class Developmentusers extends FrontendController {

  
    function __construct() {

        if(!in_array("access_setting",explode("/",$_SERVER['REQUEST_URI'])))
        {
          $this->data['pagetitle'] = 'Services Access Management ';
          $this->data['smalltitle'] = 'Edit Accessability.';
        }
        else
        {
          $this->data['pagetitle'] = 'App Settings';
          $this->data['smalltitle'] = 'Set the privacy of your app.';
        }
        $this->data['breadcrumb'] = 'Services Access Management';
        parent::__construct($this->data);


        // $user = $this->session->userdata('current_user');
        // $eventid=$this->uri->segment(3);

        //  $this->load->database();
        //   $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
        //   $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          
        //   if(!in_array($eventid,$user_events))
        //   {
        //      redirect('Forbidden');
        //   }
 
       /* for template start */

       $user = $this->session->userdata('current_user');  
       $this->data['user'] = $user[0];       

        $this->load->model('Event_model');
        $scret_key = $this->Event_model->get_screct_key();
        $this->data['scret_key'] = $scret_key;
        $this->data['iseventfreetrial']=$this->Event_model->check_event_free_trial_account_by_org_email($user[0]->Email);
       $id=$this->uri->segment(3);
       // echo $id ;exit();
       $event = $this->Event_model->get_admin_event($id);
       $this->data['event'] = $event[0];

        ///////////////Site Setting///////////////////////
        $site_setting = $this->Login_model->get_site_setting();
        $site_setting = $site_setting[0];
        $this->data['site_title'] = $site_setting['Site_title'];
        $this->data['site_logo'] = $site_setting['Site_logo'];
        $this->data['reserved_right'] = $site_setting['Reserved_right'];
                
        $this->template->set_template('home');
        $this->template->write('pagetitle', $data['pagetitle']);
          $this->template->write('page_edit_title', $data['page_edit_title']);    
        $this->template->write_view('header', 'common/header', $this->data , true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data , true);
        $this->template->write_view('right_sidebar', 'common/right_sidebar', $this->data , true);
        $this->template->write_view('inner_toolbar', 'common/inner_toolbar', $this->data , true);
        $this->template->write_view('breadcrumb', 'common/breadcrumb', $this->data , true);
        $this->template->write_view('footer', 'common/footer', $this->data , true);

         /* for template end*/

       
           $this->load->model('Developmentuser_model');

         
          
    }

      public function index($id)                 
    {  
         
        $event_id=$this->uri->segment(3);
          
        $data['userdata']=$this->Developmentuser_model->getusersByEventid($event_id);
        
        $data['roledata']=$this->Developmentuser_model->getAllRoles($event_id);
    
        $data['event_id']=$event_id;

      $data['pagetitle'] = 'Users';
      $data['smalltitle'] = 'Manage Your Users and representatives.';
        
      

      $this->template->write_view('content', 'development_user/index',$data);
      $this->template->render();

        
    }

     public function roleassignment()                 
    {  
        
        $roles=$this->input->post('role');
        $id=$this->input->post('id');
        $event_id=$this->input->post('event_id');
        if(count($roles)>0){
          $rolearray=array();
          for ($i=0; $i <count($roles) ; $i++) {
                
                $newArry=explode("::",$roles[$i]);
                array_push($rolearray,$newArry[0]);

          }

          $assignroles=implode(",",$rolearray);
          
          $data['userdata']=$this->Developmentuser_model->assignRoles($id[0],$assignroles);
          $this->session->set_flashdata('success', 'Role Assign successfully.');
          redirect("Developmentusers/index/".$event_id,'refresh');

       }else{

          $this->session->set_flashdata('error', 'plese Select the role for assign');
          redirect("Developmentusers/index/".$event_id,'refresh');
       }
        
    }

     public function softdelete($event_id,$id)                 
    {  
        $data['userdata']=$this->Developmentuser_model->softdeleteUser($id);
        $this->session->set_flashdata('success', 'Deleted successfully.');
        redirect("Developmentusers/index/".$event_id,'refresh');
    }


    public function add($event_id)                 
    {  
        if(!empty($_POST)){

           
              $uniqueid = time().uniqid();
              $date = date('Y-m-d H:i:s');
              $fname=$this->input->post('fname');
              $lname=$this->input->post('lname');
              $email=$this->input->post('email');

             $is_present=$this->Developmentuser_model->check_email($email);
            
             if($is_present){

                  $this->session->set_flashdata('error', 'Email Already Exists.');
                  redirect("Developmentusers/add/".$event_id,'refresh');

             }else{


                      $user_data=array(
                                'fname'=>$fname,
                                'lname'=>$lname,
                                'email'=>$email,
                                'role_id'=>'',
                                'event_id'=>$event_id,
                                'uniq_id'=>$uniqueid,
                                'created_at'=>$date
                              );
                    
                  $this->Developmentuser_model->insert_user($user_data);

                  $this->session->set_flashdata('success', 'user Created successfully.');
                  redirect("Developmentusers/index/".$event_id,'refresh');
              }

        }else{

              $data['event_id']=$event_id;


              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/add',$data);
              
              $this->template->render();

        }

    }

 
  public function add_multiple($event_id)                 
  {
     
     $date = date('Y-m-d H:i:s');
     $uniqueid = time().uniqid();

       //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes))
    {
        if(is_uploaded_file($_FILES['file']['tmp_name']))
        {
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE)
            {

              
               $is_present=$this->Developmentuser_model->check_email_InCsv($line[2]);
               if($is_present){

                     /* if same email found then update data */
                     $id=$is_present[0]['id']; 
                     $user_data=array(
                                'fname'=>$line[0],
                                'lname'=>$line[1],
                                'email'=>$line[2],
                                'updated_at'=>$date
                              );
                    
                  $this->Developmentuser_model->update_user($id,$user_data);

                } 
                else{
                     /* if not same email then inserted new data */

                      $user_data=array(
                                'fname'=>$line[0],
                                'lname'=>$line[1],
                                'email'=>$line[2],
                                'role_id'=>'',
                                'event_id'=>$event_id,
                                'uniq_id'=>$uniqueid,
                                'created_at'=>$date
                              );
                    
                     $this->Developmentuser_model->insert_user($user_data);

                 
               }
               
            } //eof while
            
            //close opened csv file
            fclose($csvFile);
             
             echo "success";

        }else{
            echo "fail";
        }
    }
    else{
        echo  "invalid";
    }

  }


    public function edit($event_id,$id)                 
    {  
        if(!empty($_POST)){

          
              $date = date('Y-m-d H:i:s');
              $fname=$this->input->post('fname');
              $lname=$this->input->post('lname');
              $email=$this->input->post('email');

             $is_present=$this->Developmentuser_model->check_email_forEdit($email,$id);
            
             if($is_present){

                  $this->session->set_flashdata('error', 'Email Already Exists.');
                  redirect("Developmentusers/edit/".$event_id."/".$id,'refresh');

             }else{


                      $user_data=array(
                                'fname'=>$fname,
                                'lname'=>$lname,
                                'email'=>$email,
                                'updated_at'=>$date
                              );
                    
                  $this->Developmentuser_model->update_user($id,$user_data);

                  $this->session->set_flashdata('success', 'user Updated successfully.');
                  redirect("Developmentusers/index/".$event_id,'refresh');
              }

        }else{

              $data['event_id']=$event_id;
              $data['user_data']=$this->Developmentuser_model->getUserByid($id);


              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/edit',$data);
              
              $this->template->render();

        }

    }



    
     public function permisionlist($event_id)                 
    {  
         
        $event_id=$this->uri->segment(3);
         
        $data['permisiondata']=$this->Developmentuser_model->getDataForPermisionListing($event_id);

        $functionalitydata=$this->Developmentuser_model->getAllFunctionality($event_id);

        
        foreach ($functionalitydata as $key => $value) {

          $data['functionality_array'][$value['id']]=$value['functinality_name'];
        
         }

    
        $data['event_id']=$event_id;
    
        $this->data['pagetitle'] = 'Users';
        $this->data['smalltitle'] = 'Manage Your Users and representatives.';
        $this->template->write_view('content', 'development_user/permision_lisitng',$data);
        
        $this->template->render();
    }


    public function permisiondelete($event_id,$id)                 
    {  
        $data=$this->Developmentuser_model->deletePermision($id);
        if($data){
          $this->session->set_flashdata('success', 'Deleted successfully.');
          redirect("Developmentusers/permisionlist/".$event_id,'refresh');
        }else{
          $this->session->set_flashdata('error', '!Oops.. Not Deleted Some error occure.');
          redirect("Developmentusers/permisionlist/".$event_id,'refresh');
        }
    }


   public function permisionadd($event_id)                 
    {  
        if(!empty($_POST)){

            $date = date('Y-m-d H:i:s');
            $menu_id=$this->input->post('menu_id');
           // $functionality_id=$this->input->post('functionality_id');
            $role_id=$this->input->post('role_id');
             

            // $is_present=$this->Developmentuser_model->multiple_insert_permision($event_id,$menu_id,$role_id,$functionality_id);
            $is_present=$this->Developmentuser_model->multiple_insert_permision($event_id,$menu_id,$role_id);

            
            $this->session->set_flashdata('success', 'Permision added successfully.');
            redirect("Developmentusers/permisionlist/".$event_id,'refresh');
              

        }else{

              $data['event_id']=$event_id;

              $data['roledata']=$this->Developmentuser_model->getAllRoles($event_id);
            
              $data['menu']=$this->Developmentuser_model->getAllMenu($event_id);




              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/permisionadd',$data);
              
              $this->template->render();

        }

    }



    public function permisionedit($event_id,$id)                 
    {  
        if(!empty($_POST)){
        
          
            $date = date('Y-m-d H:i:s');
            $menu_id=$this->input->post('menu_id');
            $functionality_id=$this->input->post('functionality_id');
            $role_id=$this->input->post('role_id');
             

            $is_present=$this->Developmentuser_model->multiple_update_permision($event_id,$menu_id,$role_id,$id,$functionality_id);
            
            $this->session->set_flashdata('success', 'Permision Updated successfully.');
            redirect("Developmentusers/permisionlist/".$event_id,'refresh');


        }else{

              $data['event_id']=$event_id;
              $data['permision_data']=$this->Developmentuser_model->getpermisionByid($id);
             
              $data['roledata']=$this->Developmentuser_model->getAllRoles($event_id);
            
              $data['menu']=$this->Developmentuser_model->getAllMenu($event_id);

              $data['functionalitydata']=$this->Developmentuser_model->getAllFunctionality($event_id);



               
              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/permisionedit',$data);
             
              $this->template->render();

        }

    }



     public function functionalitylist($event_id)                 
    {  
         
        $event_id=$this->uri->segment(3);
         
        $data['functinalitydata']=$this->Developmentuser_model->getDataForFunctinalityListing($event_id);

    
        $data['event_id']=$event_id;
        $this->data['pagetitle'] = 'Users';
        $this->data['smalltitle'] = 'Manage Your Users and representatives.';
        $this->template->write_view('content', 'development_user/functionality_lisitng',$data);

        $this->template->render();
    }


    public function functionalitydelete($event_id,$id)                 
    {  
        $data=$this->Developmentuser_model->deletefunctionality($id);
        if($data){
          $this->session->set_flashdata('success', 'Deleted successfully.');
          redirect("Developmentusers/functionalitylist/".$event_id,'refresh');
        }else{
          $this->session->set_flashdata('error', '!Oops.. Not Deleted Some error occure.');
          redirect("Developmentusers/functionalitylist/".$event_id,'refresh');
        }
    }



   public function functionalityadd($event_id)                 
    {  
        if(!empty($_POST)){

          
            $date = date('Y-m-d H:i:s');
            $menu_id=$this->input->post('menu_id');
            $functionality_name=$this->input->post('functionality_name');
             

            $is_present=$this->Developmentuser_model->insert_funtionality($event_id,$menu_id,$functionality_name);
            
            $this->session->set_flashdata('success', 'Added successfully.');
            redirect("Developmentusers/functionalitylist/".$event_id,'refresh');
              

        }else{

              $data['event_id']=$event_id;

              $data['menu']=$this->Developmentuser_model->getAllMenu($event_id);
          


              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/functionalityadd',$data);
             
              $this->template->render();

        }

    }

   

    public function functionalityedit($event_id,$id)                 
    {  
        if(!empty($_POST)){
          
            $date = date('Y-m-d H:i:s');
            $menu_id=$this->input->post('menu_id');
            $functionality_name=$this->input->post('functionality_name');
             

            $is_present=$this->Developmentuser_model->multiple_update_functionality($event_id,$menu_id,$functionality_name,$id);
            
            $this->session->set_flashdata('success', 'Updated successfully.');
            redirect("Developmentusers/functionalitylist/".$event_id,'refresh');


        }else{

               $data['event_id']=$event_id;
               $data['functionality_data']=$this->Developmentuser_model->getfunctionalityByid($id);
               $data['menu']=$this->Developmentuser_model->getAllMenu($event_id);
   

               
              $this->data['pagetitle'] = 'Users';
              $this->data['smalltitle'] = 'Manage Your Users and representatives.';
              $this->template->write_view('content', 'development_user/functionalityedit',$data);
             
              $this->template->render();

        }

    }


    public function getfunctionalityByAjax()                 
    { 
      $menu_id=$this->input->post('menu_id');
      $event_id=$this->input->post('event_id');

      $functionality=$this->Developmentuser_model->getfunctionalityBymenuid($menu_id,$event_id);
      if($functionality){

        echo json_encode($functionality);
        
      }else{
        echo "0";
      }
   }


} //end of class      
