<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');

class Qr_scanner extends FrontendController
{

     function __construct()
     {
          $this->data['pagetitle'] = 'Qr Scanner';
          $this->data['smalltitle'] = 'Qr Scanner Details';
          $this->data['breadcrumb'] = 'Qr Scanner';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          
          $user = $this->session->userdata('current_user');
          
          $event_templates = $this->Event_model->view_event_by_id($user[0]->Event_id);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
          
          $roledata = $this->Event_model->getUserRole($user[0]->Event_id);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
               $eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 0;
               $req_mod = ucfirst($this->router->fetch_class());

               if ($this->data['pagetitle'] == "Qr Scanner")
               {
                    $title = "Qr_scanner";
               }

               $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }

          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1)
          {
               $this->load->model('Qr_scanner_model');
               $this->load->model('Agenda_model');
               $this->load->model('Setting_model');               
               $this->load->model('Profile_model');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id = NULL)
     {

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $orid = $this->data['user']->Id;
          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Agenda_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }
          if ($this->data['user']->Role_name == 'Client')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];
          }

          //$qr_scanners = $this->Qr_scanner_model->get_qr_scanner_list();
          //$this->data['Qr_scanner'] = $advertisings;

          $menudata = $this->Event_model->geteventmenu($id, 18);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          //echo '<pre>'; print_r($menu_toal_data); exit();
          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;


          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;

          $rolename = $user[0]->Role_name;
          //$roles = $this->advertising_model->get_login_role($roleid);


          $this->data['roles'] = $roles;
          $req_mod = ucfirst($this->router->fetch_class());


          if ($req_mod == 'Qr_scanner')
          {
               $req_mod = "QR Scanner";
          }
          $check_client = 0;

          if ($rolename == 'Client')
          {
               $this->template->write_view('css', 'qr_scanner/css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'qr_scanner/index', $this->data, true);
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               $this->template->write_view('js', 'qr_scanner/js', $this->data, true);
               $this->template->render();
          }
          else
          {
               static $fin_ele = 0;
               static $mis_ele = 0;

               foreach ($roles as $value)
               {
                    $fin_ele++;
                    //echo $value['menuname']."==".$req_mod;
                    if ($value['menuname'] == $req_mod)
                    {
                         $this->template->write_view('css', 'qr_scanner/css', $this->data, true);
                         $this->template->write_view('header', 'common/header', $this->data, true);
                         $this->template->write_view('content', 'qr_scanner/index', $this->data, true);

                         if ($this->data['user']->Role_name == 'User')
                         {
                              $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
                         }
                         else
                         {
                              $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
                         }

                         $this->template->write_view('js', 'qr_scanner/js', $this->data, true);
                         $this->template->render();
                    }
                    else
                    {
                         $mis_ele++;
                         //redirect ('forbidden');
                    }
               }

               if ($mis_ele == $fin_ele)
               {
                    redirect('Forbidden');
               }
          }
     }

}
