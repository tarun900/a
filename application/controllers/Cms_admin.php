<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cms_admin extends FrontendController

{
    function __construct()
    {
        $this->data['pagetitle'] = 'Custom Homescreens';
        $this->data['smalltitle'] = 'Organise and group your tabs and features into separate home screens.';
        $this->data['breadcrumb'] = 'Custom Homescreens';
        $this->data['page_edit_title'] = 'edit';
        $this->data['page_edit_title'] = 'edit';
        parent::__construct($this->data);
        $this->load->model('Agenda_model');
        $this->load->model('Event_model');
        $this->load->model('Event_template_model');
        $user = $this->session->userdata('current_user');
        $eventid = $this->uri->segment(3);
        $delete_sec = $this->uri->segment(1) . '/' . $this->uri->segment(2);
        if ($delete_sec == 'Cms_admin/delete') $eventid = $this->uri->segment(4);
        $this->load->database();
        $user_events = $this->db->where('User_id', $user[0]->Id)->get('relation_event_user')->result_array();
        $user_events = array_filter(array_column_1($user_events, 'Event_id'));
        if (!in_array($eventid, $user_events))
        {
            redirect('Forbidden');
        }
        $event_templates = $this->Event_model->view_event_by_id($eventid);
        $this->data['Subdomain'] = $event_templates[0]['Subdomain'];
        $roledata = $this->Event_model->getUserRole($eventid);
        if (!empty($roledata))
        {
            $roleid = $roledata[0]->Role_id;
            $eventid = $eventid;
            $rolename = $roledata[0]->Name;
            $cnt = 0;
            $req_mod = ucfirst($this->router->fetch_class());
            if ($this->data['pagetitle'] == 'Custom Homescreens')
            {
                $title = "Custom Homescreens";
            }
            $cnt = $this->Agenda_model->check_auth($title, $roleid, $rolename, $eventid);
        }
        else
        {
            $cnt = 0;
        }
        if (!empty($user[1]['event_id_selected']))
        {
            $this->data['event_id_selected'] = $user[1]['event_id_selected'];
        }
        if ($cnt == 1)
        {
            $this->load->model('Setting_model');
            $this->load->model('Menu_model');
            $this->load->model('Cms_model');
            $this->load->library('upload');
            $roles = $this->Event_model->get_menu_list($roleid, $eventid);
            $this->data['roles'] = $roles;
        }
        else
        {
            echo '<script>window.location.href="' . base_url() . 'Forbidden/' . '"</script>';
        }
    }
    public function index($id=NULL)

    {
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $orid = $this->data['user']->Id;
        $hubdesing = $this->Event_model->get_hub_event($orid);
        if (count($hubdesing) > 0)
        {
            $this->data['is_hub_created'] = 1;
        }
        else
        {
            $this->data['is_hub_created'] = 0;
        }
        $eventinhub = $this->Event_model->event_in_hub_by_event_id($id);
        if (count($eventinhub) > 0)
        {
            $this->data['event_in_hub'] = 1;
        }
        else
        {
            $this->data['event_in_hub'] = 0;
        }
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 21);
        $this->data['module_group_list'] = $module_group_list;
        $super_group_list = $this->Event_template_model->get_all_super_group($id, 21);
        $this->data['super_group_list'] = $super_group_list;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $this->data['event_id'] = $id;
        $this->template->write_view('css', 'cms_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/index', $this->data, true);
        $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        $this->template->write_view('js', 'cms_admin/js', true);
        $this->template->render();
    }
    public function add($id=NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        if ($id == NULL || $id == '')
        {
            redirect("Cms_admin/index/" . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        // print_r($event); exit();
        $menu_toal_data = $this->Event_template_model->geteventmenu_list($id, null, null);
        $this->data['menu_toal_data'] = $menu_toal_data;
        if ($this->input->post())
        {
            if (empty($this->input->post('company_banner_crop_data_textbox')))
            {
                if (!empty($_FILES['images']['name']))
                {
                    $imgname = explode('.', $_FILES['images']['name']);
                    $tempname = str_replace(" ", "_", $imgname);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '1000000',
                        "max_width" => '3000',
                        "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("images"))
                    {
                        echo "error###banner upload problem<br/>" . $this->upload->display_errors();
                        die;
                    }
                }
            }
            else
            {
                $img = $_POST['company_banner_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_crop_banner_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
            }
            if (empty($this->input->post('company_logo_crop_data_textbox')))
            {
                if (!empty($_FILES['logo_images']['name']))
                {
                    $imgname = explode('.', $_FILES['logo_images']['name']);
                    $tempname = str_replace(" ", "_", $imgname);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $filenames = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $filenames,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png',
                        "max_size" => '1000000',
                        "max_width" => '3000',
                        "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("logo_images"))
                    {
                        echo "error###logo upload problem<br/>" . $this->upload->display_errors();
                        die;
                    }
                }
            }
            else
            {
                $img = $_POST['company_logo_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $filenames = strtotime(date("Y-m-d H:i:s")) . "_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $filenames;
                file_put_contents($filepath, $unencodedData);
            }
            $data['cms_array']['Organisor_id'] = $logged_in_user_id;
            $data['cms_array']['Event_id'] = $id;
            $data['cms_array']['Description'] = $this->input->post('Description');
            $data['cms_array']['Menu_name'] = $this->input->post('Menu_name');
            if (!empty($this->input->post('exi_page_id')))
            {
                $data['cms_array']['exi_page_id'] = $this->input->post('exi_page_id');
            }
            else
            {
                $data['cms_array']['exi_page_id'] = NULL;
            }
            $data['cms_array']['Images'] = $images_file;
            $data['cms_array']['Logo_images'] = $filenames;
            $data['cms_array']['fecture_module'] = implode(',', $this->input->post('feature_menu'));
            $data['cms_array']['Status'] = '1';
            $cms_id = $this->Cms_model->add_admin_cms($data);
            if (!empty($filenames))
            {
                $data1['agenda_array']['img'] = $filenames;
            }
            $data1['agenda_array']['is_feture_product'] = '1';
            $data1['agenda_array']['cms_id'] = $cms_id;
            $data1['agenda_array']['event_id'] = $id;
            $data1['agenda_array']['title'] = $this->input->post('Menu_name');
            $data1['agenda_array']['Redirect_url'] = NULL;
            $ecms_id = $this->Menu_model->add_menu($data1);
            echo "success###Custom screen is Added succesfully.";
            die;
        }
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $exibitor_page = $this->Cms_model->get_all_exibitor_page_by_event($id);
        $this->data['exibitor_page'] = $exibitor_page;
        $this->data['event_id'] = $id;
        $this->template->write_view('css', 'cms_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/add', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'cms_admin/js', $this->data, true);
        $this->template->render();
    }
    public function edit($id=NULL, $cms_id=NULL)

    {
        $Event_id = $id;
        $user = $this->session->userdata('current_user');
        $logged_in_user_id = $user[0]->Id;
        if ($cms_id == NULL || $cms_id == '')
        {
            redirect("Cms_admin/index/" . $id);
        }
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $user = $this->session->userdata('current_user');
        $roleid = $user[0]->Role_id;
        $user_role = $this->Event_template_model->get_menu_list($roleid, $id);
        $this->data['users_role'] = $user_role;
        $menudata = $this->Event_model->geteventcmsmenu($id, $cms_id);
        $menu_toal_data = $this->Event_model->get_total_menu($id);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $this->data['event_id'] = $id;
        $this->data['menu_id'] = $menudata[0]->id;
        $this->data['cms_id'] = $cms_id;
        $this->data['title'] = $menudata[0]->menuname;
        $this->data['img'] = $menudata[0]->img;
        $this->data['img_view'] = $menudata[0]->img_view;
        $this->data['is_feture_product'] = $menudata[0]->is_feture_product;
        //          $this->data['cms_id'] = $cms_id;
        //          $this->data['event_id'] = $id;
        $menu_toal_data = $this->Event_template_model->geteventmenu_list($id, null, null);
        $this->data['menu_toal_data'] = $menu_toal_data;
        $cms_menu_list = $this->Event_model->geteventcmsmenu_fecture($id, $cms_id, 1);
        $this->data['cms_menu_list'] = $cms_menu_list;
        if ($this->input->post())
        {
            if (empty($this->input->post('company_banner_crop_data_textbox')))
            {
                if (!empty($_FILES['images']['name']))
                {
                    $imgname = explode('.', $_FILES['images']['name']);
                    $tempname = str_replace(" ", "_", $imgname);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $images_file = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $images_file,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png|jpeg',
                        "max_size" => '1000000',
                        "max_width" => '3000',
                        "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("images"))
                    {
                        echo "error###banner upload problem<br/>" . $this->upload->display_errors();
                        die;
                    }
                }
            }
            else
            {
                $img = $_POST['company_banner_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_crop_banner_image.png";
                $filepath = "./assets/user_files/" . $images_file;
                file_put_contents($filepath, $unencodedData);
            }
            if (empty($this->input->post('company_logo_crop_data_textbox')))
            {
                if (!empty($_FILES['logo_images']['name']))
                {
                    $imgname = explode('.', $_FILES['logo_images']['name']);
                    $tempname = str_replace(" ", "_", $imgname);
                    $tempname_imagename = $tempname[0] . strtotime(date("Y-m-d H:i:s"));
                    $filenames = $tempname_imagename . "." . $tempname[1];
                    $this->upload->initialize(array(
                        "file_name" => $filenames,
                        "upload_path" => "./assets/user_files",
                        "allowed_types" => 'gif|jpg|png',
                        "max_size" => '1000000',
                        "max_width" => '3000',
                        "max_height" => '3000'
                    ));
                    if (!$this->upload->do_multi_upload("logo_images"))
                    {
                        echo "error###logo upload problem<br/>" . $this->upload->display_errors();
                        die;
                    }
                }
            }
            else
            {
                $img = $_POST['company_logo_crop_data_textbox'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $filenames = strtotime(date("Y-m-d H:i:s")) . "_crop_logo_image.png";
                $filepath = "./assets/user_files/" . $filenames;
                file_put_contents($filepath, $unencodedData);
            }
            $data['cms_array']['Organisor_id'] = $logged_in_user_id;
            $data['cms_array']['Event_id'] = $id;
            //               $data['cms_array']['Slug'] = $this->input->post('Slug');
            $data['cms_array']['Description'] = $this->input->post('Description');
            $data['cms_array']['Menu_name'] = $this->input->post('Menu_name');
            if ($images_file != "")
            {
                $data['cms_array']['Images'] = $images_file;
            }
            if ($filenames != "")
            {
                $data['cms_array']['Logo_images'] = $filenames;
            }
            $data['cms_array']['Img_view'] = $this->input->post('img_view');
            if (!empty($this->input->post('exi_page_id')))
            {
                $data['cms_array']['exi_page_id'] = $this->input->post('exi_page_id');
            }
            else
            {
                $data['cms_array']['exi_page_id'] = NULL;
            }
            $data['cms_array']['Background_color'] = $this->input->post('Background_color');
            if ($this->input->post('feature_menu') == NULL)
            {
                $data['cms_array']['fecture_module'] = NULL;
            }
            else
            {
                $data['cms_array']['fecture_module'] = implode(',', $this->input->post('feature_menu'));
            }
            if ($this->input->post('cms_feature_menu') == NULL)
            {
                $data['cms_array']['cms_fecture_module'] = NULL;
            }
            else
            {
                $data['cms_array']['cms_fecture_module'] = implode(',', $this->input->post('cms_feature_menu'));
            }
            $data['cms_array']['Status'] = $this->input->post('Status');
            $event_id = $this->Cms_model->edit_admin_cms($data, $cms_id);
            /*if (!empty($_FILES['Images']['name'][0]))
            {
            foreach ($_FILES['Images']['name'] as $k => $v)
            {
            $v = str_replace(' ', '', $v);
            if (file_exists("./assets/user_files/" . $v))
            $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
            else
            $Images[] = $v;
            }
            $this->upload->initialize(array(
            "file_name" => $Images,
            "upload_path" => "./assets/user_files",
            "allowed_types" => '*',
            "max_size" => '1000000',
            "max_width" => '3000',
            "max_height" => '3000'
            ));
            if (!$this->upload->do_multi_upload("Images"))
            {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', "For Menu Images, " . $error['error']);
            // return false;
            }
            }
            $menudata = $this->Event_model->geteventmenu($this->input->post('event_id'),$this->input->post('menu_id'));
            if(!empty($Images))
            {
            $data1['agenda_array']['img'] = $Images[0];
            }
            if($this->input->post('is_feture_product') != '')
            {
            $data1['agenda_array']['is_feture_product'] = '1';
            }
            else
            {
            $data1['agenda_array']['is_feture_product'] = '0';
            }
            $data1['agenda_array']['cms_id'] = $this->input->post('cms_id');
            $data1['agenda_array']['event_id'] = $this->input->post('event_id');
            $data1['agenda_array']['title'] = $this->input->post('Menu_name');
            // $data1['agenda_array']['img_view'] = $this->input->post('img_view_cms');
            $data1['agenda_array']['Redirect_url'] =NULL;
            // echo '<pre>'; print_r($data1); exit();
            $agenda_id = $this->Menu_model->add_menu($data1); */
            // $this->session->set_flashdata('event_data', 'Added');
            // redirect("cms/index/" . $id."/".$cms_id);
            echo "success###Custom screen is updated succesfully.";
            die;
        }
        $cms_data = $this->Cms_model->get_all_cms_list($id, $cms_id);
        $this->data['cms_data'] = $cms_data;
        $exibitor_page = $this->Cms_model->get_all_exibitor_page_by_event($id);
        $this->data['exibitor_page'] = $exibitor_page;
        $this->template->write_view('css', 'cms_admin/css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/edit', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'cms_admin/js', $this->data, true);
        $this->template->render();
    }
    public function menusave($eid=NULL, $cid=NULL)

    {
        if ($this->input->post())
        {
            // echo'<pre>'; print_r($_FILES); exit();
            if (!empty($_FILES['Images']['name'][0]))
            {
                foreach($_FILES['Images']['name'] as $k => $v)
                {
                    $v = str_replace(' ', '', $v);
                    if (file_exists("./assets/user_files/" . $v)) $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                    else $Images[] = $v;
                }
                $this->upload->initialize(array(
                    "file_name" => $Images,
                    "upload_path" => "./assets/user_files",
                    "allowed_types" => '*',
                    "max_size" => '10000',
                    "max_width" => '3000',
                    "max_height" => '3000'
                ));
                if (!$this->upload->do_multi_upload("Images"))
                {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
                    $this->session->set_flashdata('error', "For Menu Images, " . $error['error']);
                    // return false;
                }
            }
            $menudata = $this->Event_model->geteventmenu($this->input->post('event_id') , $this->input->post('menu_id'));
            if (!empty($Images))
            {
                $data['agenda_array']['img'] = $Images[0];
            }
            if ($this->input->post('is_feture_product') != '')
            {
                $data['agenda_array']['is_feture_product'] = '1';
            }
            else
            {
                $data['agenda_array']['is_feture_product'] = '0';
            }
            $data['agenda_array']['menu_id'] = $this->input->post('menu_id');
            $data['agenda_array']['cms_id'] = $this->input->post('cms_id');
            $data['agenda_array']['event_id'] = $this->input->post('event_id');
            $data['agenda_array']['title'] = $this->input->post('title');
            $data['agenda_array']['img_view'] = $this->input->post('img_view');
            $data['agenda_array']['Redirect_url'] = $this->input->post('Redirect_url');
            $agenda_id = $this->Menu_model->add_menu($data);
            redirect(base_url() . 'Cms_admin/edit/' . $eid . '/' . $cid);
        }
    }
    public function delete($id=NULL, $Event_id=NULL)

    {
        $agenda = $this->Cms_model->delete_cmspage($id);
        $this->session->set_flashdata('event_data', 'Deleted');
        redirect("Cms_admin/index/" . $Event_id);
    }
    public function cms_page_add_in_hub($eventid=NULL, $cid=NULL)

    {
        echo $this->Cms_model->cms_page_add_in_hub_cms_id($cid);
        die;
    }
    public function cms_page_show_in_app($eventid=NULL, $cid=NULL)

    {
        echo $this->Cms_model->cms_page_show_in_app_cms_id($cid);
        die;
    }
    public function saveeditorfiles($eid=NULL)

    {
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $Images = uniqid() . '_' . strtotime(time()) . '.' . $ext;
        $this->upload->initialize(array(
            "file_name" => $Images,
            "upload_path" => "./assets/user_files",
            "allowed_types" => '*',
            "max_size" => '0',
            "max_width" => '0',
            "max_height" => '0'
        ));
        if ($this->upload->do_multi_upload("file"))
        {
            echo base_url() . 'assets/user_files/' . $Images;
        }
        else
        {
            echo "";
        }
        die;
    }
    public function delete_image($eid=NULL, $cms_id=NULL, $img_type=NULL)

    {
        $cms_data = $this->Cms_model->get_all_cms_list($eid, $cms_id);
        if ($img_type == '0')
        {
            $Logo_array = json_decode($cms_data[0]['Logo_images']);
            unlink('./assets/user_files/' . $Logo_array[0]);
            $array_cms['Logo_images'] = NULL;
        }
        else
        {
            $images_array = json_decode($cms_data[0]['Images']);
            unlink('./assets/user_files/' . $images_array[0]);
            $array_cms['Images'] = NULL;
        }
        $this->Cms_model->delete_cms_images($cms_id, $array_cms);
        redirect(base_url() . 'Cms_admin/edit/' . $eid . '/' . $cms_id);
    }
    public function add_group($id=NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 21;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, NULL);
            redirect(base_url() . 'Cms_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/add_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_group($id=NULL, $mgid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_group_data($mgid, $id, 21);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $cms_list = $this->Cms_model->get_all_cms_list($id);
        $this->data['cms_list'] = $cms_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 21;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_modules_group($id, $group_data, $mgid);
            $this->session->set_flashdata('cms_data', 'Group Updated');
            redirect(base_url() . 'Cms_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/edit_group_view', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_group($event_id=NULL, $group_id=NULL)

    {
        $this->Event_template_model->delete_group($group_id, $event_id);
        $this->session->set_flashdata('cms_data', 'Group Deleted');
        redirect(base_url() . 'Cms_admin/index/' . $event_id);
    }
    public function add_super_group($id=NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 21);
        $this->data['module_group_list'] = $module_group_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            else
            {
                $group_data['group_image'] = NULL;
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 21;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_super_group($id, $group_data, NULL);
            redirect(base_url() . 'Cms_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/add_super_group', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function edit_super_group($id=NULL, $mgid=NULL)

    {
        $user = $this->session->userdata('current_user');
        $user_role = $this->Event_template_model->get_menu_list($user[0]->Role_id, $id);
        $this->data['users_role'] = $user_role;
        $edit_group = $this->Event_template_model->get_edit_super_group_data($mgid, $id, 21);
        $this->data['group_data'] = $edit_group;
        $event = $this->Event_model->get_admin_event($id);
        $this->data['event'] = $event[0];
        $module_group_list = $this->Event_template_model->get_all_modules_group($id, 21);
        $this->data['module_group_list'] = $module_group_list;
        if ($this->input->post())
        {
            if (!empty($this->input->post('group_icons_crop_data')))
            {
                $img = $_POST['group_icons_crop_data'];
                $filteredData = substr($img, strpos($img, ",") + 1);
                $unencodedData = base64_decode($filteredData);
                $images_file = strtotime(date("Y-m-d H:i:s")) . "_group_crop_logo_image.png";
                $filepath = "./assets/group_icon/" . $images_file;
                file_put_contents($filepath, $unencodedData);
                $group_data['group_image'] = $images_file;
            }
            if (!empty($this->input->post('delete_image')))
            {
                unlink('./assets/group_icon/' . $edit_group['group_image']);
                $group_data['group_image'] = "";
            }
            $group_data['event_id'] = $id;
            $group_data['menu_id'] = 21;
            $group_data['group_name'] = $this->input->post('group_name');
            $group_data['group_relation'] = $this->input->post('group_maps');
            $this->Event_template_model->save_super_group($id, $group_data, $mgid);
            redirect(base_url() . 'Cms_admin/index/' . $id);
        }
        $this->template->write_view('css', 'map/add_css', $this->data, true);
        $this->template->write_view('header', 'common/header', $this->data, true);
        $this->template->write_view('content', 'cms_admin/edit_super_group', $this->data, true);
        if ($this->data['user']->Role_name == 'User')
        {
            $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
        }
        else
        {
            $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
        }
        $this->template->write_view('js', 'map/add_js', $this->data, true);
        $this->template->write_view('js', 'map/js', $this->data, true);
        $this->template->render();
    }
    public function delete_super_group($event_id=NULL, $group_id=NULL)

    {
        $this->Event_template_model->delete_super_group($group_id, $event_id);
        $this->session->set_flashdata('cms_data', 'Group Deleted');
        redirect(base_url() . 'Cms_admin/index/' . $event_id);
    }
}