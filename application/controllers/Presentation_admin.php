<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Presentation_admin extends FrontendController
{
     function __construct()
     {
          $this->data['pagetitle'] = 'Presentations & Slideshows';//'Presenters & Speakers';
          $this->data['smalltitle'] = 'Add presentations and slide shows to be included in the app.';//'Create media-rich speaker biographies with images and descriptions.';
          $this->data['breadcrumb'] = 'Presentations & Slideshows';//'Presenters & Speakers';
          $this->data['page_edit_title'] = 'edit';
          parent::__construct($this->data);
          $this->load->model('Agenda_model');
          $this->load->model('Event_model');
          $eventid=$this->uri->segment(3);
          $user = $this->session->userdata('current_user');


          $this->load->database();
          $user_events = $this->db->where('User_id',$user[0]->Id)->get('relation_event_user')->result_array();
          $user_events =  array_filter(array_column_1($user_events,'Event_id'));
          if(!in_array($eventid,$user_events))
          {
             redirect('Forbidden');
          }

          $eventmodule=$this->Event_model->geteventmodulues($eventid);
          $module=json_decode($eventmodule[0]['module_list']);
          if(!in_array('9',$module))
          {
              echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>';  
          }
          $event_templates = $this->Event_model->view_event_by_id($eventid);
          $this->data['Subdomain'] = $event_templates[0]['Subdomain'];

          $event = $this->Event_model->get_module_event($eventid);
          $menu_list = explode(',', $event[0]['checkbox_values']);
          
          $roledata = $this->Event_model->getUserRole($eventid);
          if(!empty($roledata))
          {
               $roleid = $roledata[0]->Role_id;
              // $eventid = $user[0]->Event_id;
               $rolename = $roledata[0]->Name;
               $cnt = 1;
               $req_mod = ucfirst($this->router->fetch_class());

               $cnt = $this->Agenda_model->check_auth("Presentation", $roleid, $rolename,$eventid);
          }
          else
          {
               $cnt=0;
          }
          
          if (!empty($user[1]['event_id_selected']))
          {
               $this->data['event_id_selected'] = $user[1]['event_id_selected'];
          }

          if ($cnt == 1 && in_array('9',$menu_list))
          {
               $this->load->model('Presentation_model');
               
               $this->load->model('Setting_model');
               $this->load->model('Agenda_model');
               $this->load->model('Map_model');
               $this->load->model('Event_template_model');
               $this->load->model('Speaker_model');
               $this->load->model('Profile_model');
               $this->load->model('Survey_model');
               $this->load->library('upload');
               $roles = $this->Event_model->get_menu_list($roleid,$eventid);
               $this->data['roles'] = $roles;
          }
          else
          {
               echo '<script>window.location.href="'.base_url().'Forbidden/'.'"</script>'; 
          }
     }

     public function index($id = NULL)
     {

          if ($this->data['user']->Role_name == 'User')
          {
               $total_permission = $this->Presentation_model->get_permission_list();
               $this->data['total_permission'] = $total_permission;
          }

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $time_format = $this->Event_model->getTimeFormat($id);
          $this->data['time_format'] = $time_format;
          
          $presentation_list = $this->Presentation_model->get_all_presentation_list($id);
          $this->data['presentation_list'] = $presentation_list;

          if ($this->data['user']->Role_name == 'Speaker')
          {
               $event = $this->Event_model->get_admin_event($id);
               $this->data['event'] = $event[0];

               $presentation_list = $this->Presentation_model->get_speaker_presentation_list($id);
               $this->data['presentation_list'] = $presentation_list;
          }

          if ($this->data['user']->Role_name == 'Attendee' || $this->data['user']->Role_name == 'Speaker')
          {
               $Events = $this->Event_model->get_attendee_event_list();
               $this->data['Events'] = $Events;
          }

          $menudata = $this->Event_model->geteventmenu($id, 9);
          $menu_toal_data = $this->Event_model->get_total_menu($id);
          $this->data['menu_toal_data'] = $menu_toal_data;

          $this->data['event_id'] = $id;
          $this->data['menu_id'] = $menudata[0]->id;
          $this->data['title'] = $menudata[0]->menuname;
          $this->data['img'] = $menudata[0]->img;
          $this->data['img_view'] = $menudata[0]->img_view;
          $this->data['is_feture_product'] = $menudata[0]->is_feture_product;

          $time_format = $this->Event_model->getTimeFormat($id);
          $this->data['time_format'] = $time_format;

          $user = $this->session->userdata('current_user');
          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;

          $this->template->write_view('css', 'presentation_admin/css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'presentation_admin/index', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }

          $this->template->write_view('js', 'presentation_admin/js', true);
          $this->template->render();
     }

     public function add($id = NULL)
     {

          $Event_id = $id;
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $speakers = $this->Presentation_model->get_all_speaker_list($id);
          $this->data['key_people'] = $speakers;

          $Surveys = $this->Survey_model->get_survey_list_in_presentation($id);
          $this->data['Surveys'] = $Surveys;

          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;
     
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $event_templates = $this->Event_model->get_event_template_by_id_list($id);
          $this->data['event_templates'] = $event_templates;

          if ($this->input->post())
          {
               
               if($this->input->post('presentation_file_type')==0){
                    if (!empty($_FILES['Images']['name'][0]))
                    {
                         $Image_lock = array();

                         foreach ($_FILES['Images']['name'] as $k => $v)
                         {
                              $Image_lock[] = "0";

                              $v = str_replace(' ', '', $v);
                              if (file_exists("./assets/user_files/" . $v))
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                              else
                                   $Images[] = $v;
                         }

                         $this->upload->initialize(array(
                                 "file_name" => $Images,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => 'gif|jpg|jpeg|png',
                                 "max_size" => '1000000',
                                 "max_width" => '3000',
                                 "max_height" => '3000'
                         ));

                         if (!$this->upload->do_multi_upload("Images"))
                         {
                              $error = array('error' => $this->upload->display_errors());
                              $this->session->set_flashdata('error', "" . $error['error']);
                              redirect("Presentation_admin/index/" . $id);
                              //return false;
                         }
                    }
               }
               else
               {   
                    if(!empty($_FILES['PPT']['name'])){ 
                    $_FILES['sliedfile']['name'] = str_replace(' ', '', $_FILES['sliedfile']['name']);
                    if (file_exists("./assets/user_files/" . $_FILES['sliedfile']['name']))
                    {
                         $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['sliedfile']['name'];
                         $_FILES['sliedfile']['name']          = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['sliedfile']['name'];    
                    }
                    else
                    {
                         $Images[] = $_FILES['sliedfile']['name'];
                    }
                    $this->upload->initialize(array(
                       "file_name" => $Images,
                       "upload_path" => "./assets/user_files",
                       "allowed_types" => 'ppt|pot|pps|ppa|pptx|potx|ppsx|ppam|pptm|potm|ppsm'
                    ));

                    if (!$this->upload->do_multi_upload("sliedfile"))
                    {
                         $error = array('error' => $this->upload->display_errors());
                         $this->session->set_flashdata('error', "" . $error['error']);
                         redirect("Presentation_admin/index/" . $id);
                    }
                    }
               }
               $data['presentation_array']['presentation_file_type'] = $this->input->post('presentation_file_type');
               $data['presentation_array']['Organisor_id'] = $this->input->post('Organisor_id');
               $data['presentation_array']['Event_id'] = $this->input->post('Event_id');
               $data['presentation_array']['Start_date'] = $this->input->post('Start_date');
               $Start_time = $this->input->post('Start_time');
               $Start_time = date('H:i:s', strtotime($Start_time));
               $data['presentation_array']['Start_time'] = $Start_time;
               $data['presentation_array']['End_date'] = $this->input->post('End_date');
               $End_time = $this->input->post('End_time');
               $End_time = date('H:i:s', strtotime($End_time));
               $data['presentation_array']['End_time'] = $End_time;
               if(!empty($event[0]['Event_time_zone']))
               {
                    if(strpos($event[0]['Event_time_zone'],"-")==true)
                    { 
                         $arr=explode("-", $event[0]['Event_time_zone']);
                         $offset=explode(":",$arr[1]);
                         if(!in_array("30",$offset))
                         {
                              $intoffset=$offset[0]*3600;
                         }
                         else
                         {
                              $intoffset=$offset[0].'.5';
                              $intoffset=$intoffset*3600;
                         }
                        $intNew = abs($intoffset);
                        $StartDateTime = date('Y-m-d H:i:s',strtotime($data['presentation_array']['Start_date']." ".$data['presentation_array']['Start_time']) - $intNew);
                        $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['presentation_array']['End_date']." ".$data['presentation_array']['End_time']) - $intNew);
                    }
                    else
                    {
                        $arr=explode("+", $event[0]['Event_time_zone']);
                        $offset=explode(":",$arr[1]);
                         if(!in_array("30",$offset))
                         {
                              $intoffset=$offset[0]*3600;
                         }
                         else
                         {
                              $intoffset=$offset[0].'.5';
                              $intoffset=$intoffset*3600;
                         }
                        $intNew = abs($intoffset);
                        $StartDateTime = date('Y-m-d H:i:s',strtotime($data['presentation_array']['Start_date']." ".$data['presentation_array']['Start_time']) + $intNew);
                        $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['presentation_array']['End_date']." ".$data['presentation_array']['End_time']) + $intNew);
                    }
                    $data['presentation_array']['Start_date']=date('Y-m-d', strtotime($StartDateTime));
                    $data['presentation_array']['Start_time'] = date('H:i:s', strtotime($StartDateTime));
                    $data['presentation_array']['End_time']= date('H:i:s', strtotime($EndDateTime));
                    $data['presentation_array']['End_date']=date('Y-m-d', strtotime($EndDateTime));
               }
               $data['presentation_array']['Status'] = $this->input->post('Status');
               $data['presentation_array']['Thumbnail_status'] = $this->input->post('Thumbnail_status');
               $data['presentation_array']['Auto_slide_status'] = $this->input->post('Auto_slide_status');
               $data['presentation_array']['Heading'] = $this->input->post('Heading');
               $data['presentation_array']['Types'] = $this->input->post('Types');
               $data['presentation_array']['Address_map'] = $this->input->post('Address_map');
               if($this->input->post('presentation_file_type')==0){
                    $data['presentation_array']['user_permissions']=implode(",",$this->input->post('user_permissions'));
                    $Images=array_merge($Images,$this->input->post('add_survey'));
               }
               $data['presentation_array']['Images'] = $Images;

                foreach ($Images as $key => $value)
               {
                    $data['presentation_array']['Image_lock'][] = "1"; 
               }
               $presentation_id = $this->Presentation_model->add_presentation($data);
               $this->session->set_flashdata('presentation_data', 'Added');
               redirect("Presentation_admin/index/" . $id);
          }

          $this->template->write_view('css', 'presentation_admin/add_css', $this->data, true);
          $this->template->write_view('header', 'common/header', $this->data, true);
          $this->template->write_view('content', 'presentation_admin/add', $this->data, true);

          if ($this->data['user']->Role_name == 'User')
          {
               $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
          }
          else
          {
               $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
          }
          $this->template->write_view('js', 'presentation_admin/add_js', $this->data, true);
          $this->template->render();
     }

     public function edit($id = NULL)
     {

          $Event_id = $id;
          $user = $this->session->userdata('current_user');

          $roleid = $user[0]->Role_id;
          $user_role=$this->Event_template_model->get_menu_list($roleid,$id);
          $this->data['users_role']=$user_role;
          $logged_in_user_id = $user[0]->Id;

          $speakers = $this->Presentation_model->get_all_speaker_list($id);
          $this->data['key_people'] = $speakers;

          $Surveys = $this->Survey_model->get_survey_list_in_presentation($id);
          $this->data['Surveys'] = $Surveys;

          $map_list = $this->Map_model->get_maps($id);
          $this->data['map_list'] = $map_list;

          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $presentation_list = $this->Presentation_model->get_presentation_list($id);
          $this->data['presentation_list'] = $presentation_list;
          $img=json_decode($presentation_list[0]['Images'],TRUE);
          foreach ($img as $key => $value) {
               $ext = pathinfo($value, PATHINFO_EXTENSION);
               if(empty($ext))
               {
                    $img[$key]=$this->Presentation_model->get_edit_survey_in_presentation($id,$value);
               }
          }
          $presentation_list[0]['Imagesno']=$presentation_list[0]['Images'];
          $presentation_list[0]['Images']=json_encode($img);
          $this->data['presentation_list'] = $presentation_list;
          if ($this->data['page_edit_title'] = 'edit')
          {
               if ($id == NULL || $id == '')
               {
                    redirect('Event');
               }

               if ($this->input->post())
               {

                    if($this->input->post('presentation_file_type')=='0'){
                         if (!empty($_FILES['Images']['name'][0]))
                         {
                              $Image_lock = array();

                              foreach ($_FILES['Images']['name'] as $k => $v)
                              {
                                   $Image_lock[] = "0";

                                   $v = str_replace(' ', '', $v);
                                   if (file_exists("./assets/user_files/" . $v))
                                        $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $v;
                                   else
                                        $Images[] = $v;
                              }

                              $this->upload->initialize(array(
                                      "file_name" => $Images,
                                      "upload_path" => "./assets/user_files",
                                      "allowed_types" => 'gif|jpg|jpeg|png',
                                      "max_size" => '1000000',
                                      "max_width" => '3000',
                                      "max_height" => '2000'
                              ));

                              if (!$this->upload->do_multi_upload("Images"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', "" . $error['error']);
                                   redirect("Presentation_admin/index/" . $id);
                                   //return false;
                              }
                         }
                    }
                    else
                    {    
                         if(!empty($_FILES['PPT']['name'])){
                              $_FILES['PPT']['name'] = str_replace(' ', '', $_FILES['PPT']['name']);
                              if (file_exists("./assets/user_files/" . $_FILES['PPT']['name']))
                              {
                                   $Images[] = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['PPT']['name'];
                                   $_FILES['PPT']['name']          = strtotime(date("Y-m-d H:i:s")) . '_' . $_FILES['PPT']['name'];    
                              }
                              else
                              {
                                   $Images[] = $_FILES['PPT']['name'];
                              }

                              $this->upload->initialize(array(
                                 "file_name" => $Images,
                                 "upload_path" => "./assets/user_files",
                                 "allowed_types" => 'ppt|pot|pps|ppa|pptx|potx|ppsx|ppam|pptm|potm|ppsm'
                              ));

                              if (!$this->upload->do_multi_upload("PPT"))
                              {
                                   $error = array('error' => $this->upload->display_errors());
                                   $this->session->set_flashdata('error', "" . $error['error']);
                                   redirect("Presentation_admin/index/" . $id);
                              }
                         }

                    }

                    $data['presentation_array']['Id'] = $this->uri->segment(4);
                    $data['presentation_array']['Organisor_id'] = $logged_in_user_id;
                    $data['presentation_array']['Event_id'] = $Event_id;
                    $data['presentation_array']['presentation_file_type'] = $this->input->post('presentation_file_type');

                    $data['presentation_array']['Start_date'] = $this->input->post('Start_date');
                    $Start_time = $this->input->post('Start_time');
                    $Start_time = date('H:i:s', strtotime($Start_time));
                    $data['presentation_array']['Start_time'] = $Start_time;

                    $data['presentation_array']['End_date'] = $this->input->post('End_date');
                    $End_time = $this->input->post('End_time');
                    $End_time = date('H:i:s', strtotime($End_time));
                    $data['presentation_array']['End_time'] = $End_time;
                    if(!empty($event[0]['Event_time_zone'])){
                         $presentation_list = $this->Presentation_model->get_presentation_list($id);
                   $inputsdate=date('Y-m-d H:i:s',strtotime($data['presentation_array']['Start_date']." ".$data['presentation_array']['Start_time']));
                   $savesdate=date('Y-m-d H:i:s',strtotime($presentation_list[0]['Start_date']." ".$presentation_list[0]['Start_time']));
                    $inputedate=date('Y-m-d H:i:s',strtotime($data['presentation_array']['End_date']." ".$data['presentation_array']['End_time']));
                    $saveedate=date('Y-m-d H:i:s',strtotime($presentation_list[0]['End_date']." ".$presentation_list[0]['End_time']));    
                    if(strpos($event[0]['Event_time_zone'],"-")==true)
                    { 
                         $arr=explode("-", $event[0]['Event_time_zone']);
                         $offset=explode(":",$arr[1]);
                         if(!in_array("30",$offset))
                         {
                              $intoffset=$offset[0]*3600;
                         }
                         else
                         {
                              $intoffset=$offset[0].'.5';
                              $intoffset=$intoffset*3600;
                         }
                        $intNew = abs($intoffset);
                        if($inputsdate!=$savesdate){
                        $StartDateTime = date('Y-m-d H:i:s',strtotime($data['presentation_array']['Start_date']." ".$data['presentation_array']['Start_time']) - $intNew);
                         }
                         if($inputedate!=$saveedate){
                         $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['presentation_array']['End_date']." ".$data['presentation_array']['End_time']) - $intNew);
                         }
                    }
                    else
                    {
                        $arr=explode("+", $event[0]['Event_time_zone']);
                        $offset=explode(":",$arr[1]);
                         if(!in_array("30",$offset))
                         {
                              $intoffset=$offset[0]*3600;
                         }
                         else
                         {
                              $intoffset=$offset[0].'.5';
                              $intoffset=$intoffset*3600;
                         }
                        $intNew = abs($intoffset);
                        if($inputsdate!=$savesdate){
                         $StartDateTime = date('Y-m-d H:i:s',strtotime($data['presentation_array']['Start_date']." ".$data['presentation_array']['Start_time']) + $intNew);
                         }
                         if($inputedate!=$saveedate){
                         $EndDateTime =  date('Y-m-d H:i:s',strtotime($data['presentation_array']['End_date']." ".$data['presentation_array']['End_time']) + $intNew);
                         }
                    }
                         if($inputsdate!=$savesdate){
                         $data['presentation_array']['Start_date']=date('Y-m-d', strtotime($StartDateTime));
                         $data['presentation_array']['Start_time'] = date('H:i:s', strtotime($StartDateTime));
                         }
                         if($inputedate!=$saveedate){
                         $data['presentation_array']['End_time']= date('H:i:s', strtotime($EndDateTime));
                         $data['presentation_array']['End_date']=date('Y-m-d', strtotime($EndDateTime));
                         }
                    }
                    $data['presentation_array']['Auto_slide_status'] = $this->input->post('Auto_slide_status');
                    $data['presentation_array']['Status'] = $this->input->post('Status');
                    $data['presentation_array']['Thumbnail_status'] = $this->input->post('Thumbnail_status');
                    $data['presentation_array']['Heading'] = $this->input->post('Heading');
                    $data['presentation_array']['Types'] = $this->input->post('Types');
                    $data['presentation_array']['Address_map'] = $this->input->post('Address_map');
                    $data['presentation_array']['Image_lock'] = $this->input->post('Image_lock');
                    $data['presentation_array']['Image_current'] = $this->input->post('Image_current');
                    $data['presentation_array']['old_image_lock'] = $this->input->post('old_image_lock');


                    foreach ($Images as $key => $value)
                    {
                         $dataimage_lock['presentation_array']['old_image_lock'][] = "1";
                    }

                    foreach ($this->input->post('old_images') as $y => $z)
                    {
                         $key = array_search($z, $data['presentation_array']['Image_lock']);
                         if ($key !== false)
                         {

                              $data['presentation_array']['old_image_lock'][$y] = "1";
                         }
                         else
                         {    
                              $data['presentation_array']['old_image_lock'][$y] = "0";
                         }
                    }

                    unset($data['presentation_array']['Image_lock']);
                    if (!empty($dataimage_lock['presentation_array']['old_image_lock']))
                    {
                         $data['presentation_array']['Image_lock'] = array_merge($dataimage_lock['presentation_array']['old_image_lock'], $data['presentation_array']['old_image_lock']);
                    }
                    else
                    {
                         $data['presentation_array']['Image_lock'] = $data['presentation_array']['old_image_lock'];
                    }

                    unset($data['presentation_array']['old_image_lock']);             

                    $data['presentation_array']['Images'] = $Images;
                    $data['presentation_array']['old_images'] = $this->input->post('old_images');
                    if($this->input->post('presentation_file_type')=='0'){
                         $data['presentation_array']['user_permissions']=implode(",",$this->input->post('user_permissions'));
                         foreach ($this->input->post('add_survey') as $key => $value) {
                              if(!in_array($value,$data['presentation_array']['old_images']))
                              {
                                   array_push($data['presentation_array']['old_images'],$value);
                                   array_push($data['presentation_array']['Image_lock'],"1");
                              }
                         }
                    }
                    foreach ($Images as $y => $z)
                    {
                         $key = array_search($z, $this->input->post('Image_lock'));
                         if ($key !== false)
                         {

                              $data['presentation_array']['Image_lock'][$y] = "1";
                         }
                         else
                         {    
                              $data['presentation_array']['Image_lock'][$y] = "0";
                         }
                    }
                    $this->Presentation_model->update_presentation($data);
                    $this->session->set_flashdata('presentation_data', 'Updated');
                    redirect("Presentation_admin/index/" . $id);
               }

               $this->session->set_userdata($data);
               $this->template->write_view('css', 'presentation_admin/add_css', $this->data, true);
               $this->template->write_view('header', 'common/header', $this->data, true);
               $this->template->write_view('content', 'presentation_admin/edit', $this->data, true);

               if ($this->data['user']->Role_name == 'User')
               {
                    $total_permission = $this->Presentation_model->get_permission_list();
                    $this->data['total_permission'] = $total_permission;

                    $this->template->write_view('sidebar', 'common/user_sidebar', $this->data, true);
               }
               else
               {
                    $this->template->write_view('sidebar', 'common/sidebar', $this->data, true);
               }

               $this->template->write_view('js', 'presentation_admin/add_js', $this->data, true);
               $this->template->render();
          }
     }
     public function delete_pptfile($eid = NULL,$pid = NULL)
     {
          $this->Event_template_model->delete_pptfile($eid,$pid);
          echo "done";die;
     }
     public function delete($Event_id = NULL,$id = NULL)
     {
          $event = $this->Event_model->get_admin_event($id);
          $this->data['event'] = $event[0];

          $presentation_list = $this->Presentation_model->get_presentation_list($id);
          $this->data['presentation_list'] = $presentation_list[0];

          $presentation = $this->Presentation_model->delete_presentation($id);
          $this->session->set_flashdata('presentation_data', 'Deleted');
          redirect("Presentation_admin/index/" . $Event_id);
     }

}