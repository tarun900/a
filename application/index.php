
<div class='popup responsive-pop'>
    <div class='cnt223'>
           <!-- start: LOGIN BOX -->
    <div class="main-login">
            <!-- start: LOGIN BOX -->
            <div class="box-login">
                <h3>Sign in to your account</h3>
                <p>
                    Enter your e-mail and password to login in your account.
                </p>
                <form method="POST" class="form-login" action="<?php echo base_url();?>Attendee_login/check/<?php echo $Subdomain; ?>">
                    

                    <?php if($this->session->flashdata('invlaidlogin_data')){ ?>
                    <div class="errorHandler alert alert-danger no-display" style="display: block;">
                        <i class="fa fa-remove-sign"></i> <?php echo $this->session->flashdata('invlaidlogin_data'); ?> 
                    </div>
                    <?php } ?>

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                    </div>
                    <fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="text" class="form-control" id="username" name="username" placeholder="Email">
                                <i class="fa fa-user"></i> </span>
                        </div>
                        <div class="form-group form-actions">
                            <span class="input-icon">
                                <input type="password" class="form-control password" name="password" placeholder="Password">
                                <i class="fa fa-lock"></i>
                                <a class="forgot" href="#">
                                    I forgot my password
                                </a>
                                <a class="register pull-right" href="#">
                                    Sign up
                                </a> </span>
                        </div>
                        <?php for($i=0;$i<count($event_templates);$i++) { ?>
                        <input type="hidden" id="Event_id" name="Event_id" value="<?php echo $event_templates[$i]['Id']; ?>">
                        <?php } ?>
                        <div class="form-actions">
                            <button type="submit" id="login" class="btn btn-green pull-right">
                                Login <i class="fa fa-arrow-circle-right"></i>
                            </button>
                            <!-- <button id="facebook" onclick="loginwithfb()">Sign in with Facebook</button>
 -->
                        </div>
                    </fieldset>
                </form>
<!-- 
                   <?php if (@$user_profile): ?>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <img class="img-thumbnail" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?=$user_profile['id']?>/picture?type=large" style="width: 140px; height: 140px;">
                                <h2><?php echo $user_profile['session_name() ']; ?></h2>
                                <a href="<?php echo $user_profile['link']; ?>" class="btn btn-lg btn-default btn-block" role="button">View Profile</a>
                                <a href="<?php echo $logout_url; ?>" class="btn btn-lg btn-primary btn-block" role="button">Logout</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <h2 class="form-signin-heading">Login with Facebook</h2>
                        <input type="button" class="btn btn-lg btn-primary btn-block" role="button" onclick="login_fb()" value="Login">
                    <?php endif; ?>
 -->
        
        


    </div> <!-- /container -->
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
                <!-- start: COPYRIGHT -->
                <div class="copyright">
                    <?php echo $reserved_right; ?>
                </div>
                <!-- end: COPYRIGHT -->
            </div>
            <!-- end: LOGIN BOX -->
            <!-- start: FORGOT BOX -->
            <div class="box-forgot" style="display:none;">
                <h3>Forget Password?</h3>
                <p>
                    Enter your e-mail address below to reset your password.
                </p>
    
                <form method="POST" class="form-forgot" action="return false;">
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                    </div>
                    <fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="email" class="form-control" name="email" placeholder="Email" id="forgotemail">
                                <i class="fa fa-envelope"></i> </span>
                        </div>
                        <div class="form-actions">
                            <a class="btn btn-light-grey go-back">
                                <i class="fa fa-chevron-circle-left"></i> Log-In
                            </a>
                            <button type="submit" class="btn btn-green pull-right" onclick="sendforgotmail(); return false;">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </fieldset>
                </form>
                <!-- start: COPYRIGHT -->
                <div class="copyright">
                    <?php echo $reserved_right; ?>
                </div>
                <!-- end: COPYRIGHT -->
            </div>
            <div class="box-register" style="display:none;">
                <h3>Sign Up</h3>
                <p>
                    Enter your personal details below:
                </p>

                <form method="POST" action="<?php echo base_url();?>Attendee_login/add/<?php echo $Subdomain; ?>" id="form">
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                    </div>
                   
                    <fieldset>
                        <div class="form-group">
                            <span class="input-icon">
                            <?php if($Email!=""){ ?>      
                            <input type="email1" class="form-control"  name="email1" id="email1" placeholder="Email" <?php if($Email!=""){ ?> disabled <?php } ?>  value="<?php if($Email!=""){ echo base64_decode(urldecode($Email)); } ?>">
                             <?php } ?>
                            <input type="hidden" name="speaker_flag" value="<?php echo base64_decode(urldecode($speaker_flag)); ?>">
                            <input type="email" class="form-control"  name="email" id="email" placeholder="Email" <?php if($Email!=""){ ?> style="visibility:hidden;width: 0px; height: 0px;" <?php } ?> onblur="checkemail();" value="<?php if($Email!=""){ echo base64_decode(urldecode($Email)); } ?>">
                                <i class="fa fa-envelope"></i> </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                <i class="fa fa-lock"></i> </span>
                        </div>
                        <div class="form-group">
                            <span class="input-icon">
                                <input type="password" class="form-control" name="password_again" placeholder="Password Again">
                                <i class="fa fa-lock"></i> </span>
                        </div>
                        <?php for($i=0;$i<count($event_templates);$i++) { ?>
                        <input type="hidden" name="Event_id" value="<?php echo $event_templates[$i]['Id']; ?>">
                        <input type="hidden" name="Organisor_id" value="<?php echo $event_templates[$i]['Organisor_id']; ?>">
                        <input type="hidden" name="Active" value="1">
                        <?php } ?>
                        <div class="form-actions">
                            Already have an account?
                            <a href="#" class="go-back">Log-in</a>
                            <button class="btn btn-yellow btn-block" type="submit">
                                Submit <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </fieldset>
                </form>
                <!-- start: COPYRIGHT -->
                <!-- end: COPYRIGHT -->
            </div>
            <!-- end: FORGOT BOX -->
        </div>
    <!-- end: LOGIN BOX -->
    </div>
</div>
<div class="overlay" style="background:rgba(0, 0, 0, 0.7) none repeat scroll 0 0;height:100%;left:0;position:fixed;top:0;width:100%;z-index: 9999;
"></div>

<div class="slider-banner">
    <div class="row">
        <?php for($i=0;$i<count($event_templates);$i++) { ?>
        <?php $image_array = json_decode($event_templates[$i]['Images']); ?>
        <?php if(!empty($image_array))   { ?>
          <img title="" alt="" src="<?php echo base_url(); ?>assets/user_files/<?php echo $image_array[0]; ?>">
        <?php } else{ ?>
        <img "title="" alt="" src="<?php echo base_url(); ?>assets/images/events_medium.jpg">
        <?php } } ?>
    </div>
</div>
<div class="event-list">
    <div class="row">
        <?php foreach ($menu as $key => $value) 
        {
           $img = $value['img'];
           $url = base_url();
           $bimg = $url."/assets/user_files/".$img; 
           $dimg = $url."/assets/images/defult-menuimg.png";
                for($i=0;$i<count($event_templates);$i++) 
                {
                    $url = base_url().$value['pagetitle'].'/'.''.$event_templates[$i]['Subdomain'];
           ?>
            <div class="col-md-6 col-lg-4 col-sm-6">
                <a href="<?php echo $url; ?>"> 
                    <?php } ?>
                    <?php if($img !='') { ?>
                    <div style="background:url(<?php echo $bimg ?>) no-repeat left top; background-size: cover;" class="event-img">
                        &nbsp;
                    </div>
                    <?php } else { ?>
                    <div style="background:url(<?php echo $dimg ?>) no-repeat left top; background-size: cover;" class="event-img">
                        &nbsp;
                    </div>
                    <?php } ?>
                    <div class="event-tile">
                        <h3><?php echo $value['menuname']; ?></h3>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row" style="margin-left: 0px; padding-left: 0;">
    <div class="event-timer">
        <div class="row">
            <?php for($i=0;$i<count($event_templates);$i++) 
                { 
                    $event = $event_templates[$i]['Start_date'];
                    $end_event = $event_templates[$i]['End_date'];
                    $today = date("Y-m-d");
                    if($event >= $today)
                    {
                       echo '<h3>Event starts in</h3>';
                       echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event >= $today || $end_event > $today)
                    {
                        echo '<h3>Event end in</h3>';
                        echo'<ul id="countdown">
                                <li>
                                    <span class="days">00</span>
                                    <p class="timeRefDays">
                                        days
                                    </p>
                                </li>
                                <li>
                                    <span class="hours">00</span>
                                    <p class="timeRefHours">
                                        hours
                                    </p>
                                </li>
                                <li>
                                    <span class="minutes">00</span>
                                    <p class="timeRefMinutes">
                                        minutes
                                    </p>
                                </li>
                                <li>
                                    <span class="seconds">00</span>
                                    <p class="timeRefSeconds">
                                        seconds
                                    </p>
                                </li>
                            </ul>';
                    }
                    elseif($event < $today)
                    {
                        echo '<h3>Event have end</h3>';
                    }
                    else
                    {

                    }
                }
                ?>
        </div>
    </div>
</div>
<!-- end: PAGE -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script> 
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="<?php echo base_url() ?>assets/js/registration.js"></script>
<script>
    jQuery(document).ready(function() {
        Main.init();
        Registration.init();
         <?php
        if($Email!="")
        {
        echo 'jQuery(".register").click();';
        }
        ?>
          FormValidator.init();
       
    });
    function login_fb()
    {
        var name="";
        fbAsyncInit();
        FB.login(
            function(response) {
                 if (response.status== 'connected') {
                     FB.api('/me', function(response) {
                     name=response.name;

                     
                });
 
            FB.api("/me/picture?width=30&redirect=0&type=normal&height=30", function (response) {
                    var img=response.data.url;
                     $.ajax({
                        url : '<?php echo base_url(); ?>Attendee_login/fblogin/<?php echo $Subdomain; ?>',
                        data :'name='+name+'&img='+img,
                        type: "POST",           
                        success : function(data)
                        {
                            return false;
                            
                        }
                        });
            });
           
            }
        }
        ,{
             scope: "email,public_profile"
        }
      );



    }
    function fbAsyncInit() 
    {
      FB.init({
       appId      : '1468562826783502',
       status     : true, // check login status
       cookie     : true, // enable cookies to allow the server to access the session
       xfbml      : true  // parse XFBML
      });
    }
</script>
<script type="text/javascript"> 
function sendforgotmail()
{           
    $.ajax({
    url : '<?php echo base_url(); ?>Attendee_login/forgot_pas',
    data :'email='+$("#forgotemail").val(),
    type: "POST",           
    success : function(data)
    {
        var values=data.split('###');
        if(values[0]=="error")
        {   
            $('#forgotemail').parent().parent().removeClass('has-success').addClass('has-error');
            $('#forgotemail').parent().find('.control-label span').removeClass('valid');
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display');
            $("#forgot_msg").removeClass('alert-success');
            $("#forgot_msg").fadeIn();
        }
        else
        {
            $('#forgot_msg').html(values[1]);
            $("#forgot_msg").removeClass('no-display').addClass('alert-success');
            $("#forgot_msg").removeClass('alert-danger');
            $("#forgot_msg").fadeOut(3000);
        }
        
    }
    });
}
</script>
<script type="text/javascript">
function checkemail()
{      
    

    var sendflag="";
    $.ajax({
        url : '<?php echo base_url(); ?>Attendee_login/checkemail',
        data :'email='+$("#email").val()+'&idval='+$('#idval').val()+'&event_id='+$("#Event_id").val(),
        type: "POST",  
        async: false,
        success : function(data)
        {
            var values=data.split('###');
            if(values[0]=="error")
            {   
                $('#email').parent().removeClass('has-success').addClass('has-error');
                $('#email').parent().find('.control-label span').removeClass('ok').addClass('required');
                $('#email').parent().find('.help-block').removeClass('valid').html(values[1]);
                sendflag=false;               
            }
            else
            {
                $('#email').parent().removeClass('has-error').addClass('has-success');
                $('#email').parent().find('.control-label span').removeClass('required').addClass('ok');
                $('#email').parent().find('.help-block').addClass('valid').html(''); 
                sendflag=true;               
            }
        }
    });
    return sendflag;
}
</script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility-coming-soon.js"></script>
<script type="text/javascript">
        jQuery("#countdown").countdown({
            <?php for($i=0;$i<count($event_templates);$i++) { 
            $event = $event_templates[$i]['Start_date'];
            $end_event = $event_templates[$i]['End_date'];
            $end_event_date = date('Y/m/d H:i:s', strtotime($end_event));
            $edate = date('Y/m/d H:i:s', strtotime($event));
            $today = date('Y/m/d H:i:s');
            if($edate >= $today)
            {
    ?>
            date : '<?php echo $edate; ?>',
            format: "on"
    <?php }  else
    {?>
            date : '<?php echo $end_event_date; ?>',
            format: "on"

    <?php } }?>
        });
</script>

<script type="text/javascript">
   jQuery(document).ready(function() 
   {
        Main.init();
        ComingSoon.init();
    });
</script>

<style type="text/css">
@import "compass/css3";

@import url(http://fonts.googleapis.com/css?family=Merriweather+Sans);
 div.social-wrap button {
    padding-right: 45px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 35px 35px;
    background-position: right center;
    background-repeat: no-repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .5) -1px -1px 0;
}
button#facebook {
    border-color: #2d5073;
    background-color: #3b5998;
    background-image: url(http://icons.iconarchive.com/icons/danleech/simple/512/facebook-icon.png);
}
button#twitter {
    border-color: #007aa6;
    background-color: #008cbf;
    background-image: url(https://twitter.com/images/resources/twitter-bird-white-on-blue.png);
}
button#googleplus {
    border-color: #111;
    background-color: #222;
    text-shadow: #333 -1px -1px 0;
    background-image:url(http://i.tinyuploads.com/KBZc6n.png);
}
div.social-wrap button:active {
    background-color: #222;
}
div.social-wrap.b button {
    padding-right: 45px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 25px 25px, cover;
    background-position: 170px center, center center;
    background-repeat: no-repeat, repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .4) -1px -1px 0;
}
div.social-wrap.b {
}
div.social-wrap.b > .googleplus {
    background-size: 30px 30px, cover;
    background-image: url(http://dl.dropbox.com/u/109135764/gplus.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #353233), color-stop(100%, #1d1b1c));
    ;
}
div.social-wrap.b > .facebook {
    background: url(http://dl.dropbox.com/u/109135764/facebookf.png), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #4c74c4), color-stop(100%, #3b5998));
    background-size: 25px 25px, cover;
    background-position: 170px center, center center;
    background-repeat: no-repeat, repeat;
}
div.social-wrap.b > .twitter {
    background-image: url(http://dl.dropbox.com/u/109135764/twitterbird.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #00aced), color-stop(99%, #00a9ff));
    ;
}
div.social-wrap.b button em {
    font-size: 18px;
    letter-spacing: 1px;
    font-family:"Times New Roman";
    margin-right: 4px;
    margin-left: 4px;
}
div.social-wrap.c button {
    padding-left: 35px;
    padding-right: 0px;
    height: 35px;
    background: none;
    border: none;
    display: block;
    background-size: 25px 25px, cover;
    background-position: 10px center, center center;
    background-repeat: no-repeat, repeat;
    border-radius: 4px;
    color: white;
    font-family:"Merriweather Sans", sans-serif;
    font-size: 14px;
    margin-bottom: 15px;
    width: 205px;
    border-bottom: 2px solid transparent;
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
    box-shadow: 0 4px 2px -2px gray;
    text-shadow: rgba(0, 0, 0, .4) -1px -1px 0;
}
div.social-wrap.c {
}
div.social-wrap.c > .googleplus {
    background-size: 30px 30px, cover;
    background-image: url(http://dl.dropbox.com/u/109135764/gplus.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #353233), color-stop(100%, #1d1b1c));
    ;
}
div.social-wrap.c > .facebook {
    background: url(http://dl.dropbox.com/u/109135764/facebookf.png), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #4c74c4), color-stop(100%, #3b5998));
    background-size: 25px 25px, cover;
    background-position: 10px center, center center;
    background-repeat: no-repeat, repeat;
}
div.social-wrap.c > .twitter {
    background-image: url(http://dl.dropbox.com/u/109135764/twitterbird.png), -webkit-gradient(linear, left top, left bottom, color-stop(1%, #00aced), color-stop(99%, #00a9ff));
    ;
}
div.social-wrap.c button em {
    font-size: 18px;
    letter-spacing: 1px;
    font-family:"Times New Roman";
    margin-right: 4px;
    margin-left: 4px;
}
ul.sm-wrap {
    list-style-type: none;
    padding: 0px;
}
ul.sm-wrap li {
    font-family:"Helvetica", sans-serif;
    font-size: 11px;
    margin-bottom: 10px;
    padding-left: 20px;
    background-size: 15px 15px;
    background-repeat: no-repeat;
}
ul.sm-wrap li#facebook-sm {
    background-image: url(http://icons.iconarchive.com/icons/danleech/simple/512/facebook-icon.png);
}
ul.sm-wrap li#twitter-sm {
    background-image:url(https://twitter.com/images/resources/twitter-bird-white-on-blue.png);
}
ul.sm-wrap li#gplus-sm {
    background-image: url(http://i.tinyuploads.com/KBZc6n.png);
}

ul.sm-wrap a{
    text-decoration: none;    
}
#overlaybox {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    filter:alpha(opacity=70);
    -moz-opacity:0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
    z-index: 1500;
    display: block;
}
.cnt223 a{
    text-decoration: none;
}
.popup{
    width: 65%;
    margin: 0 auto;
    position: fixed;
    z-index: 99999;
}
.cnt223{
    min-width: 70%;
    width: 70%;
    min-height: 150px;
    margin: 100px auto;
    background: #f3f3f3;
    position: relative;
    z-index: 103;
    padding: 10px;
    border-radius: 5px;
    box-shadow: 0 2px 5px #000;
}
.cnt223 p{
    clear: both;
    color: #555555;
    text-align: justify;
}
.cnt223 p a{
    color: #d91900;
    font-weight: bold;
}
.cnt223 .x{
    float: right;
    height: 35px;
    left: 22px;
    position: relative;
    top: -25px;
    width: 34px;
}
.cnt223 .x:hover{
    cursor: pointer;
}
</style>
<script type='text/javascript'>
    $(function(){
        var overlay = $('<div id="overlaybox"></div>');
        overlay.show();
        overlay.appendTo(document.body);
        $('.popup').show();
        $('.close').click(function(){
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
    });
    $('.x').click(function(){
       
        $('.popup').hide();
        overlay.appendTo(document.body).remove();
        return false;
        });
    });
</script>